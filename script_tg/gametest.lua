--msk.require("gamestart")
--目标 到达10级

local g_global = g_globalt
local sleep = mskco.Sleep
g_global.testmode = true

hooked_t = hooked_t or {}
local function IsSkipFun(f)
	if f == print or f == mprint or f == pairs then
		return true
	end
	return false
end

local function UnHookTbFun(t)
	t = t or _G
	if hooked_t[t] == nil then return end
	local k,v
	print('now bg')
	for _,v in pairs(hooked_t[t]) do
		print('unhook ',k)
		k,v = v[2],v[3]
		t[k] = v
	end
	hooked_t[t] = nil
end

local function HookTbFun(t)
	t = t or _G
	if hooked_t[t] then 
		UnHookTbFun(t)
	end
	local nt = {}
	hooked_t[t] = nt
	local newFun
	for k,v in pairs(t) do
		if type(v) == 'function' and false == IsSkipFun(v) and nil == nt[f] then
			local prtmsg = true
			if type(k) == 'string' and k:sub(-8,-1) == '_OnEvent' then
				newFun = function(event,...)
					if g_filterhook then
						prtmsg = false
					end
					if prtmsg then
						print(k,'->()',event,arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9,arg10)
					end
					return v(event,...)
				end
			else
				newFun = function(...)
					if g_filterhook then
						prtmsg = false
					end
					if prtmsg then
						print(k,'->()',...)
					end
					return v(...)
				end
			end
			t[k] = newFun
			nt[newFun] = {t,k,v}
		end
	end
end

----[[
local function PrintVisWindow(wnd,wn)
	wn = wn or ""
	if not wnd or wnd:GetProperty("Visible") == "false" then
		return
	end
	local cname = wn..wnd:GetProperty("WindowName")
	local tr = cname.." = "..wnd:GetProperty("Text")
	print(tr)
	local childIndex = 0
	local childWnd
	while true do
		childWnd = wnd:GetChildByIndex(childIndex)
		if not childWnd then return end
		PrintVisWindow(childWnd,cname..".")
		childIndex = childIndex + 1
	end
end

lhooked_f = lhooked_f or {}
local function HookFunction(fname,t,f)
	t = t or _G
	local fun = t[fname]
	if type(fun) ~= 'function' then
		print(fname,' is not a function')
	end
	if fname:sub(-8,-1) == '_OnEvent' then
		f = f or function(event,...)
			print(fname,'->()',event,arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9,arg10)
		end
	end
	f = f or function(...) print(fname,...) end
	local newfun = function (...) f(...) return fun(...) end
	lhooked_f[newfun] = t[fname]
	t[fname] = newfun
	
end

local function UnHookFunction(fname,t)
	t = t or _G
	t[fname] = lhooked_f[t[fname]]
	lhooked_f[t[fname]] = nil
end

local LuaUnit = require('luaunit')

TestToto = {} --class

    function TestToto:setUp()
    end
	
	function TestToto:test_collect()
		assert(nil ~= collect(0))
	end
	
function test_ReloadFile()
	msk.require 'gamestart'
	FileLoadCallBack('reload')
end

function test_Fight()
	while true do
		local ch = GetEnemy()
		while ch do
			FightMonster(ch.id)
			ch = GetEnemy()
		end
		sleep(2)
	end
end

function test_Updata()
	dofile([[G:/bitbuket/tzj\aide\SigSearch.lua]])
end

local function PrintSlotBlend(bagType, itemSlot)
	local rateList = GetBlendAttr(itemSlot, bagType, 0)
	local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffectTime, sellPrice, _, equipKind, offlineTime, isTimeLimit, EquipPoint = GetItemInfoBySlot(bagType, itemSlot, 0)
	if itemType ~= 2 then return end
	local _, coefMain, kindMain, _, _ = GetBlendEquipInfo(itemSlot)
	local index = 0
	for i = 0, table.maxn(rateList) do
		if rateList[0] == nil then
		  return
		end
		local GuidList = GetBlendCommonInfo(rateList[i].typeId, rateList[i].value, EquipPoint, minLevel, coefMain)
		local count = 0
		local BlendName, newObj, Gb1, Gb2, Gb3
		if GuidList ~= nil then
			for j = 0, table.maxn(GuidList) do
				local CREATEFLAG = false
				local itemGuid = GuidList[j].ItemGuid
				local bagType1, itemSlot1 = GetItemPosByGuid(itemGuid)
				if itemSlot1 ~= -1 then
					local iname = GetItemInfoBySlot(bagType1, itemSlot1, 0)
					print(name..'-'..bagType..'|'..itemSlot,'->',iname..'-'..bagType1..'|'..itemSlot1,' -->',rateList[i].name )
				end
			end
		end
	end
end

function test_PrintAllGlendEquip()
	local bagnum = s_bagnum[1] or 0
	bagnum = bagnum + gUI.Bag.BagExtNumCur*10
	for i=0,bagnum-1 do
		PrintSlotBlend(1,i)
	end
	bagnum = s_bagnum[1] or 0
	for i=0,bagnum-1 do
		PrintSlotBlend(4,i)
	end
end


--SendMail('米莉亚', '哈哈我是测试的标题', '哈哈我是测试的内容', 0, 0, 30, -1, -1, -1)
function test_Bug()
	local tid = 6019
	g_global.notmove = true
	local bag,slot
	repeat
		--sleep(0.5)
		slot,bag = GetItemByName('虎啸项链')
		if slot then SellItem(bag, slot) end
		if GetQuestState(tid) == QUEST_STATUS_AVAILABLE then
			AcceptTask(-1,tid)
			sleep(2,'utask')
		else
			AbandonQuest(tid)
			sleep(2,'utask')
		end
	until false
end

function test_PickItem(wnd,skip)
	wnd = wnd or WindowSys_Instance:GetWindow('NameBoardItemBox')
	skip = skip or {}
	local childIndex = 0
	local childWnd
	local group_id
	local item_index
	local itemname
	while true do
		childWnd = wnd:GetChildByIndex(childIndex)
		if not childWnd then return end
		itemname = childWnd:GetProperty('Text')
		group_id = childWnd:GetCustomUserData(0)
		item_index = childWnd:GetCustomUserData(1)
		if childWnd:IsVisible() and (not skip[group_id] or not skip[group_id][item_index]) then
			print('尝试拾取:',itemname,group_id,item_index)
			skip[group_id] = skip[group_id] or {}
			skip[group_id][item_index] = true
			PickUpItem(group_id, item_index)
			sleep(5,'item_picked')
			return test_PickItem(wnd,skip)
		end
		--print('childWnd:IsVisible()',childWnd:IsVisible())
		childIndex = childIndex + 1
	end
end

local msk_event = {
	['您没有拾取这个道具的权限'] = 'item_picked',
	['离掉落包裹距离过远'] = 'item_picked',
}
function test_ShowErrorMessage(msg)
	local event = msk_event[msg]
	return event and mskco.HandleEvent(event) and false
end


function test_PickItemByName(name,wnd,skip)
	wnd = wnd or WindowSys_Instance:GetWindow('NameBoardItemBox')
	skip = skip or {}
	local childIndex = 0
	local childWnd
	local group_id
	local item_index
	local itemname
	while true do
		childWnd = wnd:GetChildByIndex(childIndex)
		if not childWnd then return end
		itemname = childWnd:GetProperty('Text')
		group_id = childWnd:GetCustomUserData(0)
		item_index = childWnd:GetCustomUserData(1)
		if childWnd:IsVisible() and (name == nil or name == itemname) and (not skip[group_id] or not skip[group_id][item_index]) then
			print('尝试拾取:',itemname,group_id,item_index)
			skip[group_id] = skip[group_id] or {}
			skip[group_id][item_index] = true
			PickUpItem(group_id, item_index)
			sleep(5,'item_picked')
			return test_PickItemByName(name,wnd,skip)
		end
		--print('childWnd:IsVisible()',childWnd:IsVisible())
		childIndex = childIndex + 1
	end
end


function test_Home(guid)
	g_global.farmguid = guid
	local id,dx,dy,ch
	for i=1,60 do
		PathTo(174, 136, 140)
		ch = GetCharByName("枫叶精",false,false,true)
		if ch then
			FightChar(ch)
		else
			id,dx,dy = GetGameObjectByName('神灵草')
			if not id then
				sleep(2)
				id,dx,dy = GetGameObjectByName('神灵草')
			end
			if not id then
				break
			end
			if id then
				CollectGameObj(id,dx,dy)
			end
		end
		sleep(2)
	end
	trypick('庄园徽章',5)
	trypick('枫叶精内丹（一）',20)
	g_global.farmguid = oldfguid
	g_global.xcjtaskstart = false
	CreateTaskObj(8129):Accept()
	sleep(2,'utask')
	CreateTaskObj(8129):Accept()
end
function print_hex(var)
	return print(string.format('%x',var))
end

function test_AddGuild()
	g_global.script_config = mc_tools.GetData('script_config')
	local cfg = g_global.script_config
	local tguildName = cfg['加入帮派'] or 'o水晶宫o'
	if tguildName == '不加入' then return end
	--[[准备加帮]]
	if gUI_INVALIDE == GetGuildBaseInfo() then
		print('尝试加入帮派:',tguildName)
		local tguild = nil
		mskco.SetFunctionHandler("_OnGuildC_GuildListPage",
				function (pageNum, memNum, guildID, guildName, leaderName, level, population, isRequested, maxPop) 
					--print('_OnGuildC_GuildListPage->',pageNum, memNum, guildID, guildName, leaderName, level, population, isRequested, maxPop)
					if guildName == tguildName  then
						tguild = guildID
						mskco.HandleEvent('guild_get')
					end
				end)
		for i=1,3 do
			GuildSearchRequestGuild(tguildName)
			sleep(3,'guild_get')
			if tguild then break end
		end
		if tguild == nil then
			print('获取帮派失败:',tguildName)
			local logger = g_global.logger
			logger:error('加入帮派:%s 失败，无法获取帮派ID',tguildName)
			return
		end
		mskco.SetEventProducer('_OnGuildC_JoinSucc','guild_join',nil,true)
		for i=1,3 do
			ApplyJoinGuild(tguild)
			sleep(3,'guild_join')
			if  gUI_INVALIDE ~= GetGuildBaseInfo() then break end
		end
		if gUI_INVALIDE == GetGuildBaseInfo() then
			print('加入帮派失败 申请超时:',tguildName)
			local logger = g_global.logger
			logger:error('加入帮派:%s 申请超时',tguildName)
			return
		end
	end

	print('帮派任务结束 离开帮派')
	QuitGuildOrganization(false)
	sleep(1)
	DealBag()
end


function myprint(...)
	--local sprint = "<--"
	local sprint = ""
	for _,v in ipairs{...} do
		sprint = sprint..tostring(v).."  "
	end
	if ver_release and g_global.ShowErrorMessage then
		for v in sprint:gmatch("[^\n]+") do
			ShowErrorMessage(v)
		end
	end
end

function allcodes()
	local u1,u2,u3
	function f1(a1,a2,...)
		local l0 = a1; -- move
		local l1 = 1 -- loadk
		local l2 = true -- loadbool
		local l3 = nil -- loadnil
		local l4 = u1[g1] -- gettupval, getglobal, gettable
		g1 = l1 -- setglobal
		u2 = l2 -- setupval
		l3[l2] = l1 -- settable
		local l5 = { -- newtable
			l1, l2; -- move, setlist
			x = l2 -- settable
		}
		local l6 = l5:x() -- self, call
		local l7 = -((l0+l1-l2)*l3/l4%l5)^l6 -- add, sub, mul, div, mod, pow, unm
		local l8 = #(not l7) -- not, len
		local l9 = l7..l8 -- concat
		if l1==l2 and l2<l3 or l3<=l4 then -- eq, lt, le, jmp
			for i = 1, 10, 2 do -- forprep
				l0 = l0 and l2 -- test
			end -- forloop
		else -- jmp
			for k,v in ipairs(l5) do
				l4 = l5 or l6 -- testset
			end -- tforloop
		end
		do
			local l21, l22 = ... -- vararg
			local function f2() -- closure
				return l21, l22
			end
			f2(k,v) -- call
		end --close
		return f1() -- return, tailcall
	end
end

function test_dec()
	local f = loadfile([[allopcodes.lua]])
	--allcodes
	local str = string.dump(f)
	local f = io.open([[allopcodes.luac]],'w')
	f:write(str)
	f:close()
end


function hooklog()
	local f = debug.getinfo(2,'f').func
	if Counters[f] == nil then
		Counters[f] = 1
		Names[f] = debug.getinfo(2,'Sn')
	else
		Counters[f] = Counters[f] + 1
	end
end
function gethookname(func)
	local n = Names[func]
	if n.what == 'C' then
		return n.name
	end
	local lc  = string.format("[%s]:%s",n.short_src,n.linedefined)
	if n.namewhat ~= "" then
		return string.format("%s (%s)",lc,n.name)
	else
		return lc
	end
end
function log_call_num()
	g_logbegin = not g_logbegin
	if g_logbegin then
		Counters = {}
		Names = {}
		debug.sethook (hooklog,'c')
	else
		debug.sethook ()
		for f,n in pairs(Counters) do
			print(gethookname(f),n)
		end
	end
end

function RunAllTest()
	PrintAllChar()
	do return end
	do return MoveToTargetLocation(g_player.x+math.random(-3,3), g_player.y+math.random(-3,3), GetMPlayerMapId()) end
	do return MoveToTargetLocation(64, 62, 112) end
	local call_findPath = msk.getcalladdress("TG_Engine.dll","?findPath@TerrainPathManager@CrossEngine@@UAE_NABVVector3@Ogre@@0H_N@Z")
	print_hex(call_findPath)
	do return end
	local dataaddr = msk.new(24)
	local data = string.pack('f6',244,0,81,g_player.x,g_player.z,g_player.y)
	msk.sdata(dataaddr,data)
	--msk.delete(dataaddr)
	local bok = msk.call(0x0051FC20,0x07A0F088,0,1,0x2710,dataaddr,dataaddr+0xc)
	msk.delete(dataaddr)
	do return end
	local at = g_global.accountdata
	at.teamnun = 3
	mc_tools.SendAndWait('nteam',pickle{tar = 'jjj' ,num =at.teamnun})
	g_global.teammemernames = {}
	for i=0,at.teamnun-1 do
		local _, name = GetTeamMemberInfo(i,false)
		g_global.teammemernames[i+1] = name
	end
	if at.teamnun < 2 then
		g_global.teammemernames[1] = g_player.name
	end
	test_Home()
	do return end
	g_TestRun = not g_TestRun
	if not g_TestRun then
		return mskco.Clear()
	end
	--msk.require("task_1_35")
	if unittest then
		LuaUnit:run( 'TestToto' )
	else
		repeat
			FightPursuers()
			sleep(1)
		until false
		--msk.require'task'
		--HookFunction('CastSkill')
		--CreateTaskObj(4502):Accept()
		--HookFunction('CastSkill')
		--FileLoadCallBack('reload')
		--test_Fight()
		--test_ReloadFile()
	end
end
RunAllTest()
print("--------------------------------------ok-----------------------------------------")
