require "Init"
require "lfs"
require "AccountManager"
require "Tools"
require "Team"
require "mskco"
require "EventHandle"
http = require("socket.http")


local TerminateProcess = Kernel32.TerminateProcess
local WaitForSingleObject = Kernel32.WaitForSingleObject
local OpenProcess = Kernel32.OpenProcess
local CloseHandle = Kernel32.CloseHandle
local ShowWindow = User32.ShowWindow

local lfs = lfs
local CAccountManager = CAccountManager
local CTools = CTools
local listAccount = g_listAccount
local cbArea = g_cbArea
local cbServer = g_cbServer
local cbScript = g_cbScript
local uiConfig = g_uiConfig
local gameConfig
local teamArrays = {
	["仙草精"] = {
		count = 2,
		newTeam = {},
	},
}

--增加列表框函数
function listAccount:GetSelectItems()
    local pos,nItem
    pos = listAccount:GetFirstSelectedItemPosition()
    if 0 == pos then
        return
    end
    local t = {}
    while true do
        nItem = listAccount:GetNextSelectedItem()
        if nItem == -1 then break end
        t[#t+1] = nItem
    end
    return t
end

---网络 回调函数
function _Net_Accept(s,ip,port)
	print("_Net_Accept")
    local at  = CAccountManager.GetIdleAccount(s)
    if not at then 
		print("not get idle account")
		NetSend(s,"exit#ov")
		return 
	end
	--uiConfig.curclientnum = uiConfig.curclientnum + 1
	CAccountManager.SetExtern(at.index,'socket',s)
    local str = "at#"..at.user..","..at.pwd..","..at.areaidx..","..at.serveridx..","..at.charidx..","..at.specia..","..at.script
	NetSend(s,str)
	_UI_Event("evCreateGame")
	--if CAccountManager.HasIdleAccount() then g_etCreateGame:SetEvent() end
end

function _Net_Read(s,data,len)
	if not data or len <=1 then return end
	local bg,ed = data:find("&")
	if bg then
		local first,second = data:sub(1,bg-1),data:sub(ed+1,-1)
		_Net_Read(s,first,bg-1)
		_Net_Read(s,second,len-ed)
		return
	end
	print("_Net_Read:",data )
	local at = CAccountManager.GetAccount(nil,s)
	if not at then 
		print("not get account")
		return 
	end
	local cmd
	cmd,data = data:match("(%w+)#(.+)")
	if mskco.HandleEvent(cmd,data,at,cmd) then return end
	if cmd == "extern" then
		local guidchange = false
		local oldguid = at.guid
		for key,var in data:gmatch("(%w+)=([^,]+)") do
			if key == "guid" then
				guidchange = true
			end
			CAccountManager.SetExtern(at.index,key,var)
		end
		if at.storedata.charidx ~= at.charidx then
			at.storedata = {charidx = at.charidx}
			at.nteam = nil
		end
		if at.team == nil then return end
		if guidchange and oldguid ~= at.guid then
			do return end
			local tready
			at.team,tready = CTeam:CreateTeam(at,uiConfig.teamnum)
			if tready then
				at.team:RunStep() 
			end
		elseif guidchange then
			at.team:CurStepFun(at) 
		end
	elseif cmd == 'tradename' then
		g_scriptConfig['交易对象'] = data
		Ui_SetConfig('交易对象',data)
	elseif cmd == 'pid' then
		local pid = tonumber(data)
		print('pid get',pid)
		if uiConfig['启动后最小化'] == 1 then
			local hwnd = MiniSizeProcessWindows(pid,'TGClientWnd')
			CAccountManager.SetExtern(at.index,'hwnd',hwnd)
		end
		if at.phandle == nil then
			--uiConfig.curclientnum = uiConfig.curclientnum+1
		end
		CAccountManager.SetExtern(at.index,'phandle',OpenProcess(0x1fffff,0,pid))
	elseif cmd == 'tmpteam' then
		if nil == at.tmpteam then
			at.tmpteam = {}
			at.tmpteam.num = #at.team
			at.tmpteam.nstep = tonumber(data)
			at.tmpteam.cstep = 0
			at.tmpteam.readystep = 0
		end
		CTeam.RunTmpTeam(at)
	elseif cmd == "tdata" then
		if at.team  == nil then return end
		local key,val = data:match("(.+)=(.+)")
		at.team[key] = val
	elseif cmd == "atdata" then
		local val = at.team and at.team[data] or "nil"
		NetSend(at.socket,"tdata#"..val)
	elseif cmd == "exit" then
		if data == '永久封闭' and uiConfig['封号后警告'] == 1 then
			print('播放封号警告')
			g_etPlayerSound:SetEvent()
		end
		CAccountManager.SetExtern(at.index,"status",data)
		CAccountManager.SetExtern(at.index,"socket",nil)
	elseif cmd == "tskteam" then
		local tnum = tonumber(data)
		local tready = false
		if at.team then
			local st = g_savedTeam[at]
			st[#st+1] = at.team
		end
		at.team,tready = CTeam:CreateTeam(at,tnum,'normal')
		local team = at.team
		--CAccountManager.SetExtern(at.index,"specia","xcj")
		if tready then 
			team.StepFun = CTeam.RunTskStep
			team.CurStepFun = CTeam.RunCurTskStep
			team:RunStep() 
		end
	elseif cmd == "xcjteam" then
		local tnum = tonumber(data)
		local tready = false
		if at.team then
			local st = g_savedTeam[at]
			st[#st+1] = at.team
		end
		at.team,tready = CTeam:CreateTeam(at,tnum,"xcj")
		local team = at.team
		--CAccountManager.SetExtern(at.index,"specia","xcj")
		if tready then 
			team.StepFun = CTeam.RunXcjStep
			team.CurStepFun = CTeam.RunCurXcjStep
			team:RunStep() 
		end
	elseif cmd == "byteam" then
		local tnum = tonumber(data)
		local tready = false
		if at.team then
			local st = g_savedTeam[at]
			st[#st+1] = at.team
		end
		at.team,tready = CTeam:CreateTeam(at,tnum,'by')
		local team = at.team
		--CAccountManager.SetExtern(at.index,"specia","by")
		if tready then 
			team.StepFun = CTeam.RunByStep
			team.CurStepFun = CTeam.RunCurByStep
			team:RunStep() 
		end
	elseif cmd == "stepok" then
		local team = at.team
		if team == nil then return end
		CAccountManager.SetExtern(at.index,"ready",true)
		if team:IsStepOk() then team:RunStep() end
	elseif cmd == "teamok" then
		CAccountManager.SetExtern(at.index,"ready",true)
		local team = at.team
		assert(team[1].guid == data)
		if team:IsStepOk() then 
			team.teamready = true
			team:RunStep() 
		end
	elseif cmd == "leaderok" then
		local team = at.team
		if team.teamleaderready then return end
		CAccountManager.SetExtern(at.index,"ready",true)
		assert(team[1].name == data)
		if team:IsStepOk() then 
			team.teamleaderready = true
			team:RunStep() 
		end
	elseif cmd == "friendok" then
		CAccountManager.SetExtern(at.index,"ready",true)
		local team = at.team
		if team:IsStepOk() then 
			team.friendready = true
			team:RunStep() 
		end
	elseif cmd == "inteam" then
		at = CAccountManager.GetAccountByKey("guid",data)
		if at then
			CAccountManager.SetExtern(at.index,"ready",true)
			local team = at.team
			if team:IsStepOk() then 
				team.teamready = true
				team:RunStep() 
			end
		else
			print("Cant find account by guid",data)
		end
	elseif cmd == "nfriend" then
		CAccountManager.SendData("ifriend2#"..data.."|"..at.name)
	elseif cmd == "result" then
		CAccountManager.LogData(at.index,data)
	elseif cmd == "getdata" then
		NetSend(at.socket,'getdata#'..pickle(at.storedata[data]))
	elseif cmd == "setdata" then
		at.storedata = table.merge(at.storedata,eval(data))
	else
		print("match failed",cmd,data)
	end
    --NetSend(s,"you send"..data)
end

function _Net_Close(s)
    print("some one close:")
	local at = CAccountManager.GetAccount(nil,s)
	if not at then return end
	--uiConfig.curclientnum = uiConfig.curclientnum-1
	CAccountManager.SetExtern(at.index,"socket",nil)
end

--界面回调函数--
function _UI_Event(event,arg1,arg2,arg3)
	--print("_UI_Event ",event)
	if event == "evInit" then
		local sdir,zdir = "脚本","帐号"
		print('CFG_DEF',CFG_DEF)
		if CFG_DEF == 'release' then
			sdir = [[G:\bitbuket\tzj\ver_release_tg\]]..sdir
			zdir = [[G:\bitbuket\tzj\ver_release_tg\]]..zdir
		end
		lfs.mkdir(sdir)
		lfs.mkdir(zdir)
		lfs.mkdir('日志')
		--print(sdir,zdir)
		local fileFind = {}
		CTools.FindInDir(sdir, "%.lua", fileFind, false,true)--查找txt文件
		for i=1,#fileFind do
			cbScript:AddString(fileFind[i])--:sub(1,-5)
		end
		cbScript:SetCurSel(0)
		uiConfig['多开数'] = uiConfig['多开数'] or 3
		uiConfig.teamnum = uiConfig.teamnum or 1
		--uiConfig.curclientnum = 0
		print('uiConfig',uiConfig)
	elseif event == "evCreateGame" then
		--do g_etCreateGame:SetEvent()  end
		local curclientnum = GetProceesNum('Game.exe')
		print('多开数 限制',uiConfig['多开数'],curclientnum)
		if uiConfig['多开数'] <= curclientnum then 
			return
		end
		if false == CAccountManager.HasIdleAccount() then
			return
		end
		g_etCreateGame:SetEvent() 
    elseif event == "evCheckChange" then
		if arg2 ~= 0 then return end
        local index= arg1
        if listAccount:GetItemState(index,2) == 0 then
            --print("not select")
            return
        end
        local bCheck = listAccount:GetCheck(index)
        if bCheck == 0 then 
            bCheck = 1
        else
            bCheck = 0
        end
        local t = listAccount:GetSelectItems()
        if not t then return end
		local idx,att
        for i=1,#t do
			idx = t[i]
			att = CAccountManager.GetAccount(idx)
            if index ~= t[i] then
                listAccount:SetCheck(t[i],bCheck)
            end
			if bCheck ~= 1 then
				CAccountManager.ExitAccount(att)
			end
        end
	elseif event == 'evCloseAll' then
		CAccountManager.ExitAll()
    elseif event == "evListDClik" then
		local index,col = arg1,arg2
        if col == 0 then 
			local text = UiGetInput("file#gametest")
			if not text then return end
			local t = CAccountManager.GetAccount(index)
			NetSend(t.socket,text)
			return 
		end
        local text = CAccountManager.GetText(index,col)
		if not text then return end
        text = UiGetInput(text)
        if not text then return end
        print("SetText",index,col,text,type(text))
        CAccountManager.SetText(index,col,text)
    elseif event == "evAreaChange" then
		for i=cbServer:GetCount()-1,0,-1 do
			cbServer:DeleteString(i)
		end
		
		SetGlobal("servergrp",gvServerIdx)
		SetGlobal("servername",gvServerName)
		local areaidx = gvServerIdx
	
		--SetGlobal("servergrp",cbArea:GetCurSel())
		--SetGlobal("servername",cbArea:GetText())
		--local var = gameConfig and gameConfig.Telecom and gameConfig.Telecom[cbArea:GetText()]
		--if not var then return end
		--local areaidx = tonumber(var:match("(%d+),.+"))
		--if not areaidx then return end
		local fname = 'Server_'..areaidx..'.txt'
		print('try get file:',fname)
		local data,err = http.request('http://launcher.tzj.iwgame.com/'..fname)
		if data then 
			local servers =  data:split('\n')
			local servername
			for i=1,#servers do
				servername = servers[i]:match("%d+~(.-)~.+")
				if servername then
					cbServer:AddString(servername)
				end
			end
		else
			print('net server get failed:',err)
			local f = io.open(uiConfig.gamedir.."\\server_"..areaidx..".txt")
			if not f then return end
			local l = f:read()
			local servername
			while l do
				servername = l:match("%d+~([^~]+)~.+")
				if servername then
					cbServer:AddString(servername)
				end
				l = f:read()
			end
			f:close()
		end
		cbServer:SetCurSel(0)
    elseif event == "evFileOpen" then
		--uiConfig.curclientnum = 0
		CAccountManager.LoadFromFile(arg1)
		mskco.HandleEvent('loadfile',arg1,at,event)
	elseif event == "evGameDirChage" then
		uiConfig.gamedir = tostring(arg1)
		for i=cbArea:GetCount()-1,0,-1 do
			cbArea:DeleteString(i)
		end
		print('serverinfo:',gvServerIdx,gvServerName,gvServerName,gvGameVer)
		--gvServerIdx= 1--测试新区
		cbArea:AddString(gvServerName)	
		uiConfig.areaidx = gvServerIdx
		cbArea:SetCurSel(0)
		_UI_Event("evAreaChange",0)
		CopyFile(uiConfig.gamedir.."\\ClientConfig.cfg","说明\\ClientConfig_old.cfg")
		CopyFile("说明/ClientConfig.cfg",uiConfig.gamedir.."\ClientConfig.cfg")
		do return end
		gameConfig = CTools.ReadConfig(uiConfig.gamedir.."\\..\\ServerList.cfg")
		if not gameConfig.Telecom then return end
		local tt = {}
		local idx
		local snum = 0
		for k,v in pairs(gameConfig.Telecom) do
			idx = v:find(',')
			idx = v:sub(1,idx-1)
			idx = tonumber(idx)
			tt[idx] = k
			snum = snum + 1
		end
		for i=0,snum-1 do
			cbArea:AddString(tt[i])
		end
		cbArea:SetCurSel(0)
		_UI_Event("evAreaChange",0)
	elseif event == "evExit" then
		if uiConfig and uiConfig.gamedir and uiConfig.gamedir ~= "" then
			os.rename(uiConfig.gamedir.."\\BugReport_back.exe",uiConfig.gamedir.."\\BugReport.exe")
			os.remove(uiConfig.gamedir.."\\msvcp120.dll")
			os.remove(uiConfig.gamedir.."\\msvcr120.dll")
			os.remove(uiConfig.gamedir.."\\BugReport_back.exe")
			CopyFile("说明\\ClientConfig_old.cfg",uiConfig.gamedir.."\\ClientConfig.cfg")
		end
		os.remove("C:\\mskd")
	elseif event == "evTimer" then
		local num_close = CAccountManager.StatusCheck()
		_UI_Event("evCreateGame")
	elseif event == 'evShwoWindow' then
		local t = listAccount:GetSelectItems()
        if not t then return end
		local at
        for i=1,#t do
			at = CAccountManager.GetAccount(t[i])
			if at and at.hwnd then return ShowWindow(at.hwnd,1) end
        end
	elseif event == 'evRestart' then
		if not CAccountManager.Restart(arg1) then return end
		g_etCreateGame:SetEvent() 
	elseif event == 'evShowDebugInfo' then
		local t = listAccount:GetSelectItems()
        if not t then return end
		local at
        for i=1,#t do
			at = CAccountManager.GetAccount(t[i])
			if at and at.socket then 
				NetSend(at.socket,'printlv#1')
			end
		end
	elseif event == 'evStopDebugInfo' then
		local t = listAccount:GetSelectItems()
        if not t then return end
		local at
        for i=1,#t do
			at = CAccountManager.GetAccount(t[i])
			if at and at.socket then 
				NetSend(at.socket,'printlv#2')
			end
		end
    end
end

function _UI_AccountListDoubleClick(index,col,text)
    print("_UI_AccountListDoubleClick",text)
end

--初始化帐号界面
CAccountManager.AccountListInit()
print("Start ok")
--初始化大区和服务器
--[[
cbArea:AddString("内测一区")
cbArea:SetCurSel(0)
cbServer:AddString("盘古")
cbServer:AddString("乾坤")
cbServer:AddString("轩辕")
cbServer:SetCurSel(0)
cbScript:AddString("test.lua")
cbScript:SetCurSel(0)
--]]