require "mskco"

local _curteams = {}
function _CreateTeam(at,ti)
	--assert(ti.num > 1)
	local t= _curteams[ti.tar] and _curteams[ti.tar][ti.num]
	if nil == t then
		t = ti
		t.tready = mskco.NewMap()
		t.cstep = '组建队伍'--这个仅仅是调试用
		t.nstep = 0
	end
	t[#t+1] = at
	CAccountManager.SetExtern(at.index,"teamleader",t[1].name)
	if #t >= t.num then
		print('team ready',t.tar,t.num,#t)
		for i=1,#t do
			t[i].nteam = t
		end
		if _curteams[t.tar] then _curteams[t.tar][t.num] = nil end
		return t
	else
		print('team no ready',t.tar,t.num,#t)
		_curteams[t.tar] = _curteams[t.tar] or {}
		_curteams[t.tar][t.num] = t
	end
	
end
function Handler_LoadFile(data,at,ev)
	_curteams = {}
end


local newteam_steps = {
	function (at,t)
		NetSend(at.socket,"tleader#"..t[1].name)
	end,
	function (at,t)
		NetSend(t[1].socket,"iteam#"..at.guid..","..t.name)
	end,
	function (at,t)
		local i = -1
		for m=1,#t do
			if t[m] == at then
				i=m
				break
			end
		end
		assert(i>0)
		local friends = ""
		for j=i+1,#t do
			friends = friends..t[j].guid.."|"..t[j].name..","
		end
		NetSend(at.socket,"ifriend#"..friends)
	end,
}
function Handler_NewTeam(data,at,ev)
	print("Handler_NewTeam",data)
	if at.nteam and at.nteam.ready then return NetSend(at.socket,'nteam#ok') end
	if at.nteam then --掉线重上的
		local atstep = at.tstep or 0
		print('掉线重上 ',#at.nteam,at.nteam.cstep,at.nteam.nstep,atstep)
		if atstep ~= at.nteam.nstep then
			newteam_steps[at.nteam.nstep](at,at.nteam)
		end
		return
	end
	local teaminfo = eval(data)
	--检查是否已经成功组过队
	--检查队伍是否已经创建成功
	at.nteam = _CreateTeam(at,teaminfo)
	if at.nteam == nil then return end
	local nReady
	local t = at.nteam
	local wat
	--一个人的队伍
	if t.num == 1 then
		t.ready = true
		for i=1,#t do
			NetSend(t[i].socket,'nteam#ok')
		end
		return
	end
	
	--通知队长信息
	if at.nteam.nstep < 1 then
		for i=2,#t do
			NetSend(t[i].socket,"tleader#"..t[1].name)
		end
		t[1].tstep = 1
		t.cstep = '通知队长名字'
		t.nstep = 1
		nReady = 1
		while nReady < #t do
			_,wat = mskco.Wait("leaderok")
			if wat.nteam == t then
				nReady = nReady + 1
				wat.tstep = 1
			else--不是自己的消息 抛出去
				mskco.HandleEvent('leaderok',data,wat)
			end
		end
	end
	--开始组队
	if at.nteam.nstep < 2 then
		for i=2,#t do
			NetSend(t[1].socket,"iteam#"..t[i].guid..","..t[i].name)
		end
		nReady = 1
		t[1].tstep = 2
		t.cstep = '正在组队'
		t.nstep = 2
		while nReady < #t do
			_,wat = mskco.Wait("teamok")
			if wat.nteam == t then
				nReady = nReady + 1
				wat.tstep = 2
			else--不是自己的消息 抛出去
				mskco.HandleEvent('teamok',data,wat)
			end
		end
		at.nteam.itready = true
	end
	--开始加好友
	if at.nteam.nstep < 3 then
		for i=1,#t-1 do--测试警告->i=1
			friends = ""
			for j=i+1,#t do
				friends = friends..t[j].guid.."|"..t[j].name..","
			end
			NetSend(t[i].socket,"ifriend#"..friends)
		end
		t[#t].tstep = 3
		nReady = 1
		t.cstep = '互加好友'
		t.nstep = 3
		while nReady < #t do
			_,wat = mskco.Wait("friendok")
			if wat.nteam == t then
				nReady = nReady + 1
				wat.tstep = 3
			else--不是自己的消息 抛出去
				mskco.HandleEvent('friendok',data,wat)
			end
		end
		at.nteam.ifready = true
	end
	--全部完成了
	at.nteam.ready = true
	for i=1,#t do
		NetSend(t[i].socket,'nteam#ok')
	end
end

--------------------------------------
--
--------------------------------------
local function _TryNextTeamStep(t,nteam)
	if #t >= #nteam then
		for i=1,#nteam do
			t:remove(nteam[i])
			NetSend(nteam[i].socket,'tstep#'..t.nready)
		end
		t.nready = t.nready + 1
	end
end

function Handler_TeamStep(data,at,ev)
	local idx
	repeat
		data,idx = data:match("(.+)_(%d+)")
		idx = tonumber(idx)
		local nteam = at.nteam
		assert(nteam and nteam.ready)
		local t = nteam.tready:pop(data) or mskco.NewSet{nready = 0}
		if idx == 9999 then--已经做完了 不等其他人了
			--删除自己的队伍信息 删除队伍里自己的信息
			at.nteam = nil
			nteam.num = nteam.num -1
			for i=1,#nteam do
				if nteam[i] == at then
					table.remove(nteam,i)
				end
			end
			
			--如果剩下的人数已经够了 那么继续下一步
			_TryNextTeamStep(t,nteam)
		elseif idx ~= t.nready then--掉线了 步骤不对
			NetSend(at.socket,'tstep#'..(t.nready-1))
		else
			t:insert(at)
			_TryNextTeamStep(t,nteam)
		end
		nteam.tready:push(data,t)
		data,at = mskco.Wait(ev)
	until false
end

function Handler_GetTeamData()
	local sdata
	repeat
		if data == 'xcjat' then
			assert(nil ~= at.nteam)
			local tat
			for i=1,#at.nteam do
				if at.nteam[i].storedata['selfwater'] == true then
					tat = at.nteam[i]
					break
				end
			end
			if tat then
				sdata = pickle{guid = tat.guid,name = tat.name}
			else
				sdata = pickle'end'
			end
		elseif data == 'script_config' then
			sdata = pickle(g_scriptConfig)
		else
			sdata = pickle(at.storedata[data])
		end
		print("getdata try send",sdata)
		NetSend(at.socket,'getdata#'..sdata)
		data,at = mskco.Wait(ev)
	until false
end

function Handler_SetTeamData(data,at,ev)
	local sdata
	repeat
		if data == 'xcjat' then
			assert(nil ~= at.nteam)
			local tat
			for i=1,#at.nteam do
				if at.nteam[i].storedata['selfwater'] == true then
					tat = at.nteam[i]
					break
				end
			end
			if tat then
				sdata = pickle{guid = tat.guid,name = tat.name}
			else
				sdata = pickle'end'
			end
		elseif data == 'script_config' then
			sdata = pickle(g_scriptConfig)
		else
			sdata = pickle(at.storedata[data])
		end
		print("getdata try send",sdata)
		NetSend(at.socket,'getdata#'..sdata)
		data,at = mskco.Wait(ev)
	until false
end

function Handler_GetData(data,at,ev)
	local sdata
	repeat
		if data == 'xcjat' then
			assert(nil ~= at.nteam)
			local tat
			for i=1,#at.nteam do
				if at.nteam[i].storedata['selfwater'] == true then
					tat = at.nteam[i]
					break
				end
			end
			if tat then
				sdata = pickle{guid = tat.guid,name = tat.name}
			else
				sdata = pickle'end'
			end
		elseif data == 'script_config' then
			sdata = pickle(g_scriptConfig)
		else
			sdata = pickle(at.storedata[data])
		end
		print("getdata try send",sdata)
		NetSend(at.socket,'getdata#'..sdata)
		data,at = mskco.Wait(ev)
	until false
end

function Handler_SetData(data,at,ev)
	repeat
		at.storedata = table.merge(at.storedata,eval(data))
		data,at = mskco.Wait(ev)
	until false
end

function Handler_GetAcData(data,at,ev)
	repeat
		NetSend(at.socket,'getacdata#'..pickle(at[data]))
		data,at = mskco.Wait(ev)
	until false
end

function Handler_SetAcData(data,at,ev)
	repeat
		table.merge(at,eval(data))
		data,at = mskco.Wait(ev)
	until false
end

function Handler_GetGlobalData(data,at,ev)
	repeat
		local tname,data = data:match('(.-)|(.+)')
		local t = _G[tname]
		NetSend(at.socket,'getgldata#'..pickle(t[data]))
		data,at = mskco.Wait(ev)
	until false
end

function Handler_SetGlobalData(data,at,ev)
	repeat
		local tname,data = data:match('(.-)|(.+)')
		local t = _G[tname]
		table.merge(t,eval(data))
		data,at = mskco.Wait(ev)
	until false
end


mskco.SetEventHandler('getteamdata',Handler_GetTeamData)
mskco.SetEventHandler('setteamdata',Handler_SetTeamData)
mskco.SetEventHandler('loadfile',Handler_LoadFile)
mskco.SetEventHandler('nteam',Handler_NewTeam)
mskco.SetEventHandler('tstep',Handler_TeamStep)
mskco.SetEventHandler('setdata',Handler_SetData)
mskco.SetEventHandler('getdata',Handler_GetData)
mskco.SetEventHandler('getacdata',Handler_GetAcData)
mskco.SetEventHandler('setacdata',Handler_SetAcData)