local io = io
local pairs = pairs
local print = print
local type = type
local string = string
local tostring = tostring
local lfs = lfs
local table = table
local assert = assert
module "CTools"

function CopyFile(src,dest)
    local f = io.open(src,"rb")
    if not f then 
        print("copyfile failed file not exit",src)
        return 
    end
    local data = f:read("*all")
    f:close()
    f = io.open(dest,"wb")
    if not f then 
        print("copyfile failed file cant create",dest)
        return 
    end
    f:write(data)
    f:close()
    return
end

function ClearDir(path)
     for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            os.remove(path..'/'..file)
        end
    end
end

function CopyDir(src,dest,wefind)
    for file in lfs.dir(src) do
        if file ~= "." and file ~= ".." then
            local f = src..'\\'..file
            local desf = dest.."\\"..file
            --print ("/t "..f)
            if string.find(f, wefind) ~= nil then
                --print("/t "..f)
                copyfile(f,desf)
            end
            local attr = lfs.attributes (f)
            assert (type(attr) == "table")
            if attr.mode == "directory" and intofolder then
                lfs.mkdir(desf)
                CopyDir (f,desf, wefind)
            else
                
            end
        end
    end
end

function FindInDir(path, wefind, r_table, intofolder,shortname)
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            local f = path..'\\'..file
            if string.find(f, wefind) ~= nil then
                --print("/t "..f)
                if shortname then 
					table.insert(r_table, file) 
				else
					table.insert(r_table, f)
				end
            end
			if intofolder then
				local attr = lfs.attributes (f)
				assert (type(attr) == "table")
				if attr.mode == "directory" then
					FindInDir (f, wefind, r_table, intofolder)
				end
			end
		end
    end
	print("FindInDir ok")
end

function ReadConfig(fname)
	local block,key,value,source
	local t = {}
	for line in io.lines(fname) do
		block = line:match'%[(.+)%]'
		if block then
			source = block
			t[block] = {}
		elseif line ~= "" then
			key,value = line:match("(%P+)%s*=%s*(.*)")
			if key then t[source][key] = value end
		end
	end
	return t
end
