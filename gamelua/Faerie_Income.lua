if gUI and not gUI.Faerie_Income then
  gUI.Faerie_Income = {}
end
Faerie_IncomeShowType = {Refining = 0, Reduce = 1}
local _MAX_SLOTNEW = 10
local IS_FLASFLAG = true
local Faerie_IncomeInterface = Faerie_IncomeShowType.Refining
local FaerieRefiningIndex = 0
Faerie_Resource = {
  [0] = "白虎魂力",
  [1] = "玄武魂力",
  [2] = "朱雀魂力",
  [3] = "青龙魂力"
}
function Faerie_Income_OpenPanel(ShowType, isFlaush)
end
function Faerie_Income_ShowInterfaceByType()
end
function Faerie_Income_InitData()
  if Faerie_IncomeInterface == Faerie_IncomeShowType.Refining then
    Faerie_Income_InitRefiData()
  elseif Faerie_IncomeInterface == Faerie_IncomeShowType.Reduce then
    Faerie_Income_InitReduData()
  end
end
function Faerie_Income_InitRefiData()
  Faerie_HandLogic(0)
end
function Faerie_Income_ClosePanel()
  gUI.Faerie_Income.Reduce:SetVisible(false)
end
function UI_Faerie_Income_TableCtrlClick(msg)
end
function UI_EvolveBtn_Click(msg)
  local wnd = WindowToButton(msg:get_window())
  local Index = wnd:GetProperty("CustomUserData")
  Faerie_HandLogic(tonumber(Index))
end
function Faerie_HandLogic(Index)
  Faerie_UpdateAttrByIndex(Index)
  FaerieRefiningIndex = Index
  local text = Faerie_Resource[Index]
  gUI.Faerie_Income.ShowLab:SetProperty("Text", text)
end
function Faerie_UpdateAttrByIndex(Index)
  gUI.Faerie_Income.DbMain:ClearAllContent()
  local Level, CurrProName, CurrProValue, NextProValue, PlayLev, NeedLev = GetFaerieLevelInfo(Index)
  local ItemId1, ItemValue1, ItemId2, ItemValue2, nMoney = GetFaerieMaterInfo(Index)
  if Level == -1 then
    gUI.Faerie_Income.DbMain:InsertBack("炼化等级已满", 4294903313, 0, 0)
    gUI.Faerie_Income.CofimBtn:SetProperty("Enable", "false")
  elseif Level == -2 then
    gUI.Faerie_Income.DbMain:InsertBack("", 4294967295, 0, 0)
    gUI.Faerie_Income.CofimBtn:SetProperty("Enable", "true")
  else
    gUI.Faerie_Income.DbMain:InsertBack("当前属性", 4294638330, 0, 0)
    local strCurr = string.format("%s 加成 %d%%", CurrProName, CurrProValue)
    local strNext = string.format("%s 加成 %d%%", CurrProName, NextProValue)
    gUI.Faerie_Income.DbMain:InsertBack(strCurr, 4294638330, 0, 0)
    gUI.Faerie_Income.DbMain:InsertBack("   ", 4294638330, 0, 0)
    local curColor = 4278255360
    if PlayLev == 0 then
      curColor = 4294903313
    end
    gUI.Faerie_Income.DbMain:InsertBack("炼化后属性", curColor, 0, 0)
    gUI.Faerie_Income.DbMain:InsertBack(strNext, curColor, 0, 0)
    gUI.Faerie_Income.DbMain:InsertBack("需求玩家等级 " .. tostring(NeedLev) .. "级", curColor, 0, 0)
    gUI.Faerie_Income.CofimBtn:SetProperty("Enable", "true")
  end
  if ItemId1 ~= nil then
    gUI.Faerie_Income.MoneyLab:SetVisible(true)
    if ItemId1 > 0 then
      gUI.Faerie_Income.NeedGb:SetVisible(true)
      gUI.Faerie_Income.CountLa1:SetVisible(true)
    else
      gUI.Faerie_Income.CountLa1:SetVisible(false)
      gUI.Faerie_Income.NeedGb:SetVisible(false)
    end
    if ItemId2 > 0 then
      gUI.Faerie_Income.NeedGb1:SetVisible(true)
      gUI.Faerie_Income.CountLa2:SetVisible(true)
    else
      gUI.Faerie_Income.NeedGb1:SetVisible(false)
      gUI.Faerie_Income.CountLa2:SetVisible(false)
    end
    local icon1, count1, name1, quilty1 = GetBaseInfoByItemId(ItemId1)
    local icon2, count2, name2, quilty2 = GetBaseInfoByItemId(ItemId2)
    if icon1 then
      gUI.Faerie_Income.NeedGb:SetItemGoods(0, icon1, quilty1)
      gUI.Faerie_Income.NeedGb:SetCustomUserData(0, ItemId1)
    end
    if icon2 then
      gUI.Faerie_Income.NeedGb1:SetItemGoods(0, icon2, quilty2)
      gUI.Faerie_Income.NeedGb1:SetCustomUserData(0, ItemId2)
    end
    local color1 = colorGreen
    local color2 = colorGreen
    if ItemValue1 > count1 then
      color1 = colorRed
    end
    if count1 >= 1000 then
      count1 = "*"
    end
    gUI.Faerie_Income.CountLa1:SetProperty("Text", "{#C^" .. color1 .. "^" .. tostring(count1) .. "/" .. tostring(ItemValue1) .. "}")
    if ItemValue2 > count2 then
      color2 = colorRed
    end
    if count2 >= 1000 then
      count2 = "*"
    end
    gUI.Faerie_Income.CountLa2:SetProperty("Text", "{#C^" .. color2 .. "^" .. tostring(count2) .. "/" .. tostring(ItemValue2) .. "}")
    Faerie_Income_SetMoney(nMoney)
  else
    gUI.Faerie_Income.NeedGb1:SetVisible(false)
    gUI.Faerie_Income.NeedGb:SetVisible(false)
    gUI.Faerie_Income.NeedGb1:ClearGoodsItem(0)
    gUI.Faerie_Income.NeedGb:ClearGoodsItem(0)
    gUI.Faerie_Income.CountLa1:SetVisible(false)
    gUI.Faerie_Income.CountLa2:SetVisible(false)
    gUI.Faerie_Income.MoneyLab:SetVisible(false)
  end
end
function Faerie_Income_SetMoney(money)
  gUI.Faerie_Income.MoneyLab:SetProperty("Text", Lua_UI_Money2String(money, false, false, false, true))
  gUI.Faerie_Income.MoneyLab:SetCustomUserData(0, money)
end
function Faerie_SetMoney(newMoney, changeMoney, event)
  if gUI.Faerie_Income.Refining:IsVisible() then
    local itemMoney = tonumber(gUI.Faerie_Income.MoneyLab:GetCustomUserData(0))
    gUI.Faerie_Income.MoneyLab:SetProperty("Text", Lua_UI_Money2String(itemMoney, false, false, false, true, newMoney, event))
    gUI.Faerie_Income.MoneyLab:SetCustomUserData(0, itemMoney)
  end
end
function Faerie_DoActionRefining(msg)
  AskFusionLevelUp(FaerieRefiningIndex)
end
function Faerie_Income_Succ()
  if gUI.Faerie_Income.Refining:IsVisible() then
    Faerie_UpdateAttrByIndex(FaerieRefiningIndex)
  end
  if gUI.Faerie.wndRoot:IsVisible() then
    _Faerie_BaseInfo()
  end
end
function Faerie_Income_BeginInfo(arg1, arg2, arg3)
  gUI.Faerie_Income.GbSub1:ResetAllGoods(true)
  gUI.Faerie_Income.GbSub1:SetItemColumn(5)
  gUI.Faerie_Income.GbSub1:SetItemCount(arg2)
end
function Faerie_Income_Info(iconName, guid, nsel, i, quilty)
  gUI.Faerie_Income.GbSub1:SetItemGoods(i, iconName, quilty)
  gUI.Faerie_Income.GbSub1:SetItemStarState(i, 0)
  if nsel == 1 then
    gUI.Faerie_Income.GbSub1:SetItemEnable(i, false)
  else
    gUI.Faerie_Income.GbSub1:SetItemEnable(i, true)
  end
end
function Faerie_Income_EndInfo(arg1)
end
function UI_Faerie_RClick(msg)
  local win = msg:get_window()
  local from_off = msg:get_lparam()
  local bag, slot = GetResolveItemInfo(from_off)
  local guid = GetItemGUIDByPos(bag, slot, 0)
  ResolveItemAddOrDel(guid)
end
function UI_FaerieSlot_RClick(msg)
  local win = msg:get_window()
  local from_off = msg:get_lparam()
  local bag, slot = GetResolveInfo(from_off, NpcFunction.NPC_FUNC_FUSION_DECOMPOSE)
  local guid = GetItemGUIDByPos(bag, slot, 0)
  ResolveItemAddOrDel(guid)
end
function UI_LeftItemTip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  local from_off = msg:get_wparam()
  local bag, slot = GetResolveInfo(from_off, NpcFunction.NPC_FUNC_FUSION_DECOMPOSE)
  if bag then
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
  end
end
function UI_RightItemTip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  local from_off = msg:get_wparam()
  local bag, slot = GetResolveItemInfo(from_off)
  if bag then
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
  end
end
function UI_Faerie_OnDraged(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local itemId = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local form_type = from_win:GetProperty("BoxType")
  local bagType = gUI_GOODSBOX_BAG[form_type]
  local boxName = to_win:GetProperty("WindowName")
  if from_win:GetProperty("GoodBoxIndex") == "GB_ItemMainBag" then
    local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.backpack0, itemId, 0)
    ResolveItemAddOrDel(guid)
  end
end
function Faerie_Income_InitReduData()
  gUI.Faerie_Income.GbSub:ResetAllGoods(true)
  gUI.Faerie_Income.GbSub1:ResetAllGoods(true)
  gUI.Faerie_Income.DbSub:ClearAllContent()
  ClearResolveItemSlot()
  ResolveItemRefushInfos()
end
function Faerie_AddSlot(guid, index, bag, slot)
  local name, icon, quality, minLevel, job = GetItemInfoBySlot(bag, slot, -1)
  gUI.Faerie_Income.GbSub:SetItemGoods(index, icon, quality)
  local RigIndex = GetPosByItemGuidInRes(guid)
  if RigIndex then
    gUI.Faerie_Income.GbSub1:SetItemEnable(RigIndex, false)
  end
end
function Faerie_DelSlot(index, guid)
  if index < _MAX_SLOTNEW then
    gUI.Faerie_Income.GbSub:ClearGoodsItem(index)
    local RigIndex = GetPosByItemGuidInRes(guid)
    if RigIndex then
      gUI.Faerie_Income.GbSub1:SetItemEnable(RigIndex, true)
    end
  end
end
function Faerie_Refall()
  if gUI.Faerie_Income.Reduce:IsVisible() then
    if IS_FLASFLAG then
      IS_FLASFLAG = false
      gUI.Faerie_Income.FaerieTime:Restart()
    else
      gUI.Faerie_Income.FaerieTime:Restart()
    end
  end
end
function UI_Faerie_DesClick(msg)
  DisassComfimNew()
end
function Faerie_Not_En(arg1)
  if arg1 == 0 then
    Lua_Chat_ShowOSD("DISASSEMBLE_NOEN")
  elseif arg1 == 1 then
    Lua_Chat_ShowOSD("DISASSEMBLE_CANNOTITEM")
  end
end
function Faerie_Reslut(arg1)
  if gUI.Faerie_Income.Reduce:IsVisible() then
    gUI.Faerie_Income.DbSub:InsertBack(arg1, 4294967295, 0, 0)
  end
end
function Faerie_Suess(arg1)
  if gUI.Faerie_Income.Reduce:IsVisible() then
    gUI.Faerie_Income.GbSub:ClearGoodsItem(arg1)
  end
end
function Faerie_IntensifyOpen(index)
  if not gUI.Faerie_Income.Refining:IsVisible() then
    gUI.Faerie_Income.Refining:SetVisible(true)
  end
  Faerie_HandLogic(tonumber(index))
end
function Faerie_IncomeOpen()
  if not gUI.Faerie_Income.Reduce:IsVisible() then
    gUI.Faerie_Income.Reduce:SetVisible(true)
    Faerie_Income_InitReduData()
  else
    gUI.Faerie_Income.Reduce:SetVisible(false)
  end
end
function UI_Faerie_IncomeCloseClick(msg)
  gUI.Faerie_Income.Reduce:SetVisible(false)
end
function UI_Faerie_IntensifyCloseClick(msg)
  gUI.Faerie_Income.Refining:SetVisible(false)
end
function Script_Faerie_Income_OnEvent(event)
  if event == "PLAYER_MONEY_CHANGED" then
    Faerie_SetMoney(arg1, arg2, event)
  elseif event == "PLAYER_BIND_MONEY_CHANGED" then
    Faerie_SetMoney(arg1, arg2, event)
  elseif event == "ITEM_ADD_TO_BAG" then
    Faerie_UpdateList()
  elseif event == "ITEM_DEL_FROM_BAG" then
    Faerie_UpdateList()
  elseif event == "FAERIE_INCOME_SUCC" then
    Faerie_Income_Succ()
  elseif event == "RESOLVEITEM_UPDATEINFOBEGIN" then
    Faerie_Income_BeginInfo(arg1, arg2, arg3)
  elseif event == "RESOLVEITEM_UPDATEINFOS" then
    Faerie_Income_Info(arg1, arg2, arg3, arg4, arg5)
  elseif event == "RESOLVEITEM_UPDATEINFOEND" then
    Faerie_Income_EndInfo(arg1)
  elseif event == "RESOLVE_BAG_ENOUGHFAERI" then
    Faerie_Not_En(arg1)
  elseif event == "RESOLVEITEM_NOTIFTY_DELSLOT" then
    Faerie_DelSlot(arg1, arg2)
  elseif event == "RESOLVEITEM_NOTIFTY_ADDSLOT" then
    Faerie_AddSlot(arg1, arg2, arg3, arg4)
  elseif event == "RESOLVE_REF_ALLNEW" then
    Faerie_Refall()
  elseif event == "DISASSEMBLE_RESLUTCONTENTNEW" then
    Faerie_Reslut(arg1)
  elseif event == "DISASSEMBLE_SUESSNEW" then
    Faerie_Suess(arg1)
  end
end
function Faerie_UpdateList()
  if gUI.Faerie_Income.Reduce:IsVisible() then
    if IS_FLASFLAG then
      IS_FLASFLAG = false
      gUI.Faerie_Income.FaerieTime:Restart()
    else
      gUI.Faerie_Income.FaerieTime:Restart()
    end
  end
end
function _Faerie_FaerieTime(timer)
  if gUI.Faerie_Income.Refining:IsVisible() then
    Faerie_UpdateAttrByIndex(FaerieRefiningIndex)
  end
  if gUI.Faerie_Income.Reduce:IsVisible() then
    ResolveItemRefushInfos()
  end
  IS_FLASFLAG = true
  gUI.Faerie_Income.FaerieTime:Stop()
end
function Script_Faerie_Income_OnHide()
  ClearResolveItemSlot()
end
function Script_Faerie_Income_OnLoad()
  gUI.Faerie_Income.Reduce = WindowSys_Instance:GetWindow("Faerie_Income")
  gUI.Faerie_Income.GbSub = WindowToGoodsBox(WindowSys_Instance:GetWindow("Faerie_Income.Faerie_bg.Image_bg.Disassemble_bg.Top_bg.Disassemble_gbox"))
  gUI.Faerie_Income.DbSub = WindowToDisplayBox(WindowSys_Instance:GetWindow("Faerie_Income.Faerie_bg.Image_bg.Disassemble_bg.Down_bg.Note_dbox"))
  gUI.Faerie_Income.GbSub1 = WindowToGoodsBox(WindowSys_Instance:GetWindow("Faerie_Income.Faerie_bg.Image_bg.Disassemble_bg.Right_bg.Down_bg.Disassemble_cnt.Disassemble_gbox"))
  gUI.Faerie_Income.FaerieTime = TimerSys_Instance:CreateTimerObject("FaerieTime", 1, -1, "_Faerie_FaerieTime", false, false)
end
function Script_Faerie_Intensify_OnLoad()
  gUI.Faerie_Income.Refining = WindowSys_Instance:GetWindow("Faerie_Intensify")
  gUI.Faerie_Income.ShowLab = WindowToLabel(WindowSys_Instance:GetWindow("Faerie_Intensify.Faerie_bg.Image_bg.Evolve_bg.Top_bg.Name_dlab"))
  gUI.Faerie_Income.CofimBtn = WindowToButton(WindowSys_Instance:GetWindow("Faerie_Intensify.Faerie_bg.Image_bg.Evolve_bg.Evolve_btn"))
  gUI.Faerie_Income.DbMain = WindowToDisplayBox(WindowSys_Instance:GetWindow("Faerie_Intensify.Faerie_bg.Image_bg.Evolve_bg.Top_bg.Evolve_Dbox"))
  gUI.Faerie_Income.MoneyLab = WindowToLabel(WindowSys_Instance:GetWindow("Faerie_Intensify.Faerie_bg.Image_bg.Evolve_bg.Down_bg.Send_dlab"))
  gUI.Faerie_Income.NeedGb = WindowToGoodsBox(WindowSys_Instance:GetWindow("Faerie_Intensify.Faerie_bg.Image_bg.Evolve_bg.Down_bg.Need_gbox"))
  gUI.Faerie_Income.NeedGb1 = WindowToGoodsBox(WindowSys_Instance:GetWindow("Faerie_Intensify.Faerie_bg.Image_bg.Evolve_bg.Down_bg.Need1_gbox"))
  gUI.Faerie_Income.CountLa1 = WindowToLabel(WindowSys_Instance:GetWindow("Faerie_Intensify.Faerie_bg.Image_bg.Evolve_bg.Down_bg.NeedCount_dlab"))
  gUI.Faerie_Income.CountLa2 = WindowToLabel(WindowSys_Instance:GetWindow("Faerie_Intensify.Faerie_bg.Image_bg.Evolve_bg.Down_bg.NeedCount1_dlab"))
end
function Script_Faerie_Intensify_OnHide()
  ClearResolveItemSlot()
end
function Script_Faerie_Intensify_OnEvent(event)
end
 WindowToLabel(WindowSys_Instance:GetWindow("Faerie_Intensify.Faerie_bg.Image_bg.Evolve_bg.Down_bg.NeedCount1_dlab"))
end
function Script_Faerie_Intensify_OnHide()
  ClearResolveItemSlot()
end
function Script_Faerie_Intensify_OnEvent(event)
end
