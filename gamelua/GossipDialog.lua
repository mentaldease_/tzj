if gUI and not gUI.Gossip then
  gUI.Gossip = {}
end
local gSelectState = -1
local CURRENTSELECTINDEX = -1
local MAXLENGTH = 16
local gWndDisplayColor = 0
local gFuncItemCount = 0
local gTaskItemCount = 0
local gCurShowTaskId = 0
local gCurShowTaskStatus = 0
local gTransitionFlag = false
local gIsNeedChoiceReward = false
local _Gossip_CurTaskName = ""
local _GossipMapName = {}
_Gossip_MapBtn_List = {}
BtnTypeDig = {NpcDiglog = 1, Exchanges = 2}
local _NPCFuncType = {
  NPC_FUNC_NONE = 0,
  NPC_FUNC_CHAT = 1,
  NPC_FUNC_BANK = 2,
  NPC_FUNC_SHOP = 3,
  NPC_FUNC_TRANSMIT = 4,
  NPC_FUNC_GUILDSTORAGE = 5,
  NPC_FUNC_OPEN_SIGN_EQUIP = 6,
  NPC_FUNC_EQUIP_INTENSIFY = 9,
  NPC_FUNC_RESET_INSTANCE = 10,
  NPC_FUNC_MAIL_NORMAL = 11,
  NPC_FUNC_MAIL_PAY = 12,
  NPC_FUNC_CLEARPOLISH = 13,
  NPC_FUNC_GUARD_START = 14,
  NPC_FUNC_EQUIP_CHECK_IN = 15,
  NPC_FUNC_PET_COMPOUND = 16,
  NPC_FUNC_GAMESEND_CHAT = 18,
  NPC_FUNC_REPEAT_QUEST = 22,
  NPC_FUNC_QUEST_CONTINUE = 27,
  NPC_FUNC_RANDOM_QUEST = 28,
  NPC_FUNC_GUILD_CREATE = 29,
  NPC_FUNC_GUILD_TERRITORY_CREATE = 30,
  NPC_FUNC_GUILD_APPLY = 31,
  NPC_FUNC_ITEM_MAKE = 32,
  NPC_FUNC_GUILD_SHOP_RESET = 33,
  NPC_FUNC_GEM_EMBED = 35,
  NPC_FUNC_GEM_MOVEOUT = 36,
  NPC_FUNC_GUILD_DONATE = 42,
  NPC_FUNC_QA = 43,
  NPC_FUNC_EQUIP_SMRITI = 44,
  NPC_FUNC_GUILD_VEHICLE_UPGRADE = 45,
  NPC_FUNC_GUILD_VEHICLE_MAKE = 46,
  NPC_FUNC_FASHION_REACTIVE = 47,
  NPC_FUNC_SPRING = 48,
  NPC_FUNC_RETURE_ITEM = 49,
  NPC_FUNC_PRAY_TEMPLE = 50,
  NPC_FUNC_BOSS = 51,
  NPC_FUNC_MATERIAL_UPGRADE = 52,
  NPC_FUNC_LEARN_ARTISAN = 53,
  NPC_FUNC_DAK = 54,
  NPC_FUNC_SCRIPT_EVENT = 55,
  NPC_FUNC_AUCTION_ITEM = 56,
  NPC_FUNC_AUCTION_MONEY = 57,
  NPC_FUNC_GUILD_UPGRADE = 58,
  NPC_FUNC_GUILD_DAILY_QUEST = 59,
  NPC_FUNC_GUILD_LEVEL = 60,
  NPC_FUNC_ENTER_FTL = 61,
  NPC_FUNC_RELATION_UP = 62,
  NPC_FUNC_CLEAR_PK = 63,
  NPC_FUNC_GOOD_CONVERT = 64,
  NPC_FUNC_GOOD_GIFT = 66,
  NPC_FUNC_SIEGE_HISTORY = 70,
  NPC_FUNC_SUMMON_EXP = 73,
  NPC_FUNC_RETRACTILE_STALL = 84,
  NPC_FUNC_NO_USE = 87,
  NPC_FUNC_ARTISAN_ERASE = 88,
  NPC_FUNC_MY_SELL_SHOP = 89,
  NPC_FUNC_GOLD_SELL_SHOP = 90,
  NPC_FUNC_BUY_SELL_SHOP = 91,
  NPC_FUNC_SELL_SELL_SHOP = 92,
  NPC_FUNC_OPEN_SELL_SHOP = 93,
  NPC_FUNC_FIND_OPEN_SELL_SHOP = 94,
  NPC_FUNC_GET_SELL_SHOP_MONEY = 95,
  NPC_FUNC_ABOUT_SELL_SHOP = 96,
  NPC_FUNC_ABOUT_GOLD_SHOP = 97,
  NPC_FUNC_ABOUT_BUY_SELL_SHOP = 98,
  NPC_FUNC_GEM_BLEND = 102,
  NPC_FUNC_QUERY_CHECK_RIDE_VEHICLE = 104,
  NPC_FUNC_EQUIP_BLEND = 107,
  NPC_FUNC_GUILD_RECRUIT = 108,
  NPC_FUNC_GUILD_RECRUIT_LIST = 109,
  NPC_FUNC_GUILD_RECRUIT_RESPOND = 110,
  NPC_FUNC_GUILD_RECRUIT_REQUEST = 111,
  NPC_FUNC_BUY_SHOP = 120,
  NPC_FUNC_SELL_SHOP = 121,
  NPC_FUNC_GEM_SACRIFICE = 122,
  NPC_FUNC_CARNIVAL_TREASURE = 123,
  NPC_FUNC_QUEST_COMMIT = 124,
  NPC_FUNC_MOUNT_INTENSIFY = 133,
  NPC_FUNC_SCENARIO_SAY = 150,
  NPC_FUNC_SCENARIO_ANIM = 151,
  NPC_FUNC_LOGIC_END = 200,
  NPC_FUNC_HOMELAND = 201,
  NPC_FUNC_GAMEMALL = 202,
  NPC_FUNC_CURRENCY = 203,
  NPC_FUNC_HIGHGEMBLEND = 209,
}
local _EquipType = {
  [_NPCFuncType.NPC_FUNC_EQUIP_INTENSIFY] = "{ImAgE^UiBtn001:Image_forge}",
  [_NPCFuncType.NPC_FUNC_EQUIP_SMRITI] = "{ImAgE^UiBtn001:Image_inherit}",
  [_NPCFuncType.NPC_FUNC_EQUIP_BLEND] = "{ImAgE^UiBtn001:Image_blend}",
  [_NPCFuncType.NPC_FUNC_OPEN_SIGN_EQUIP] = "{ImAgE^UiBtn001:Image_GemSign}"
}
local _GemType = {
  [_NPCFuncType.NPC_FUNC_GEM_EMBED] = "{ImAgE^UiBtn001:Image_gemembed}",
  [_NPCFuncType.NPC_FUNC_GEM_MOVEOUT] = "{ImAgE^UiBtn001:Image_genextraction}",
  [_NPCFuncType.NPC_FUNC_GEM_SACRIFICE] = "{ImAgE^UiBtn001:Image_Gem}",
  [_NPCFuncType.NPC_FUNC_GEM_BLEND] = "{ImAgE^UiBtn001:Image_Gem}",
  [_NPCFuncType.NPC_FUNC_HIGHGEMBLEND] = "{ImAgE^UiBtn001:Image_Gem}"
}
local _CommonType = {
  [_NPCFuncType.NPC_FUNC_BANK] = "{ImAgE^UiBtn001:Image_bank}",
  [_NPCFuncType.NPC_FUNC_GUILDSTORAGE] = "{ImAgE^UiBtn001:Image_bank}",
  [_NPCFuncType.NPC_FUNC_SHOP] = "{ImAgE^UiBtn001:Image_shop}",
  [_NPCFuncType.NPC_FUNC_MAIL_NORMAL] = "{ImAgE^UiBtn001:Image_post}",
  [_NPCFuncType.NPC_FUNC_TRANSMIT] = "{ImAgE^UiBtn001:Image_transmit}"
}
function UI_Gossip_CloseBtnClick(msg)
  Gossip_ClosePanel()
  _OnGossip_HideMessageBox()
end
function Gossip_ClosePanel()
  if gUI.Gossip.Root then
    gUI.Gossip.Root:SetProperty("Visible", "false")
    gUI.Gossip.Transmis:SetProperty("Visible", "false")
    gSelectState = -1
  end
end
function UI_Gossip_ShowItemTip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local index = msg:get_wparam()
  local count = wnd:GetItemSubscript(index)
  local id = wnd:GetItemData(index)
  if id ~= 0 then
    Lua_Tip_Item(wnd:GetToolTipWnd(0), nil, nil, nil, nil, id)
    Lua_Tip_ShowEquipCompare(wnd, -1, id)
  end
end
function UI_Gossip_AcceptBtnCick(msg)
  if gCurShowTaskStatus ~= QUEST_STATUS_AVAILABLE and gCurShowTaskStatus ~= QUEST_STATUS_FAILED then
    local GoodsIndex = gUI.Gossip.ChoiceItem:GetSelectedItem()
    if gIsNeedChoiceReward and GoodsIndex == gUI_INVALIDE then
      Lua_UI_ShowMessageInfo("请选择一个物品作为奖励!")
      return
    else
      AcceptQuest(gUI.Gossip.CurNpcId, gCurShowTaskId, DIALOG_STATUS_REWARD, GoodsIndex)
    end
  else
    local GuardTime = GetGuardTime(gCurShowTaskId)
    if GuardTime > 0 then
      Lua_Chat_ShowOSD("QUEST_GUARDTIME", GuardTime)
      UI_Gossip_BackBtnClick()
      return
    end
    local questNum = GetAcceptedQuestNum()
    if questNum >= 25 then
      Lua_Chat_ShowOSD("QUEST_NUM_LIMIT_OSD")
      Lua_Chat_ShowSysLog("QUEST_NUM_LIMIT")
      return
    end
    local costMoney = GetQuestAdvCon(gCurShowTaskId)
    if costMoney and costMoney > 0 then
      Messagebox_Show("ACCEPT_QUEST_MONEY", costMoney, gUI.Gossip.CurNpcId, gCurShowTaskId)
    else
      AcceptQuest(gUI.Gossip.CurNpcId, gCurShowTaskId, DIALOG_STATUS_AVAILABLE, 0)
    end
  end
  UI_Gossip_CloseBtnClick()
end
function QB_DropReGetTask(task_id, npcId)
  ReAccectQuest(npcId, task_id)
  UI_Gossip_CloseBtnClick()
end
function UI_Gossip_BackBtnClick(msg)
  gUI.Gossip.Gossip:SetProperty("Visible", "true")
  for i, name in ipairs({
    "Back1_pic.Center_pic.Task_cnt",
    "Back1_pic.Center_pic.Back_btn",
    "Back1_pic.Center_pic.Accept_btn",
    "Back1_pic.Center_pic.TaskName_dlab"
  }) do
    gUI.Gossip.Root:GetGrandChild(name):SetProperty("Visible", "false")
  end
end
function UI_Gossip_PosBtnOnClick(msg)
  local button = WindowToButton(msg:get_window())
  local index = tonumber(button:GetCustomUserData(0))
  local price = tonumber(button:GetCustomUserData(2))
  local isCan = tonumber(button:GetCustomUserData(3))
  if isCan == 0 then
    Lua_Chat_ShowOSD("TRANSFORM_CANNOTTO")
    return
  end
  local mapName = _GossipMapName[index]
  if gTransitionFlag then
    Messagebox_Show("TRANSMISSION", gUI.Gossip.CurNpcId, index, price, mapName)
  end
end
function UI_Gossip_PosBtnToolTip(msg)
  local wnd = msg:get_window()
  local text = wnd:GetProperty("Value")
  local index = wnd:GetCustomUserData(0)
  local level = wnd:GetCustomUserData(1)
  local price = wnd:GetCustomUserData(2)
  local isCan = wnd:GetCustomUserData(3)
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  if isCan == 1 then
    Lua_Tip_Transmission(tooltip, text, level, price)
  else
    Lua_Tip_Transmission(tooltip, "当前传送点", 0, -1)
  end
end
function UI_Gossip_TreeToolTip(msg)
  if not gUI.Gossip.Bg2PosTree then
    return
  end
  local item = gUI.Gossip.Bg2PosTree:GetHoverItem()
  if not item then
    return
  end
  local userdata = item:GetUserData()
  if userdata == -1 then
    return
  end
  local tooltip = WindowToTooltip(item:GetToolTipWnd(0))
  if not tooltip then
    return
  end
  local index = item:GetCustomUserData(0)
  local level = item:GetCustomUserData(1)
  local price = item:GetCustomUserData(2)
  local isCan = item:GetCustomUserData(3)
  local text = item:GetProperty("Text")
  if isCan == 1 then
    Lua_Tip_Transmission(tooltip, text, level, price)
  else
    Lua_Tip_Transmission(tooltip, "当前传送点", 0, -1)
  end
end
function UI_Gossip_PosTransBtnOnClick(msg)
  local NPC_TYPE_TRANSMISSION = 1
  local item = gUI.Gossip.Bg2PosTree:GetSelectedItem()
  if item == nil then
    return
  end
  local index = tonumber(item:GetUserData())
  local isCanTrans = tonumber(item:GetCustomUserData(3))
  local price = tonumber(item:GetCustomUserData(2))
  if isCanTrans == 0 then
    Lua_Chat_ShowOSD("TRANSFORM_CANNOTTO")
    return
  end
  local mapName = _GossipMapName[index]
  if index == -1 then
    return
  end
  if gTransitionFlag then
    Messagebox_Show("TRANSMISSION", gUI.Gossip.CurNpcId, index, price, mapName)
  end
end
function UI_Gossip_PosTransBtnDOnClick(msg)
  local NPC_TYPE_TRANSMISSION = 1
  local item = gUI.Gossip.Bg2PosTree:GetSelectedItem()
  if item == nil then
    return
  end
  local index = tonumber(item:GetUserData())
  local isCanTrans = tonumber(item:GetCustomUserData(3))
  local price = tonumber(item:GetCustomUserData(2))
  if isCanTrans == 0 then
    Lua_Chat_ShowOSD("TRANSFORM_CANNOTTO")
    return
  end
  local mapName = _GossipMapName[index]
  if index == -1 then
    return
  end
  if gTransitionFlag then
    Messagebox_Show("TRANSMISSION", gUI.Gossip.CurNpcId, index, price, mapName)
  end
end
function UI_Gossip_TreeItemSelect(msg)
  local item = gUI.Gossip.Bg2PosTree:GetSelectedItem()
  if item == nil then
    return
  end
  local index = tonumber(item:GetUserData())
  if index == -1 then
    if CURRENTSELECTINDEX == -1 then
      return
    else
      local temp_btn1 = WindowToButton(_Gossip_MapBtn_List[CURRENTSELECTINDEX])
      temp_btn1:SetStatus("normal")
      CURRENTSELECTINDEX = index
    end
  end
  if _Gossip_MapBtn_List[index] == nil then
    return
  end
  local temp_btn = WindowToButton(_Gossip_MapBtn_List[index])
  temp_btn:SetStatus("selected")
  if CURRENTSELECTINDEX == -1 then
  elseif CURRENTSELECTINDEX == index then
    return
  else
    local temp_btn1 = WindowToButton(_Gossip_MapBtn_List[CURRENTSELECTINDEX])
    temp_btn1:SetStatus("normal")
  end
  CURRENTSELECTINDEX = index
end
function UI_Gossip_DialogBtn(msg)
  local btn = msg:get_window()
  if btn then
    local SelIndex = btn:GetCustomUserData(0)
    _OnGossip_SelectItem(gUI.Gossip.CurNpcId, SelIndex)
  end
end
function Script_Gossip_OnEvent(event)
  if event == "NPC_GOSSIP_SHOW_TASK_DETAIL" then
    _OnGossip_ShowQuestDetail(arg1, arg2, arg3, arg4)
  elseif event == "NPC_GOSSIP_UPDATE_BEGIN" then
    _OnGossip_UpdateBegin(arg1, arg2, arg3, arg4, arg5, arg6)
  elseif event == "NPC_GOSSIP_ADD_FUNCTION_ITEM" then
    _OnGossip_UpdateAddFunction(arg1, arg2, arg3, arg4)
  elseif event == "NPC_GOSSIP_ADD_TRANSITION_ITEM" then
    _OnGossip_UpdateTransition(arg1, arg2, arg3, arg4, arg5)
  elseif event == "NPC_GOSSIP_ADD_TASK_ITEM" then
    _OnGossip_UpdateAddQuest(arg1, arg2, arg3)
  elseif event == "NPC_GOSSIP_ADD_CARNIVAL_ITEM" then
    _OnGossip_UpdateAddCarnival(arg1, arg2, arg3)
  elseif event == "NPC_GOSSIP_UPDATE_END" then
    _OnGossip_UpdateEnd(arg1, arg2)
  elseif event == "NPC_GOSSIP_ADD_REWARD_ITEM" then
    _OnGossip_AddRewardItem(arg1, arg2, arg3, arg4, arg5)
  elseif event == "NPC_GOSSIP_ADD_CHOICES_ITEM" then
    _OnGossip_AddChoiceItem(arg1, arg2, arg3, arg4, arg5)
  elseif event == "NPC_GOSSIP_SELECT_ITEM" then
    _OnGossip_SelectItem(arg1, arg2)
  elseif "NPC_GOSSIP_END" == event then
    _OnGossip_End()
  elseif event == "CLOSE_NPC_RELATIVE_DIALOG" then
    _OnGossip_CloseNpcRelativeDialog()
  elseif event == "PLAYER_QUEST_REMOVED" then
    _OnGossip_QuestRemove(arg1)
  elseif event == "NPC_TRANSHOUSE_UPDATE_BEGIN" then
    _OnGossip_PostHouseBegin(arg1, arg2, arg3)
  elseif event == "NPC_TRANSHOUSE_ADD_ITEM" then
    _OnGossip_PostHouseAddItem(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
  elseif event == "NPC_TRANSHOUSE_UPDATE_END" then
    _OnGossip_PostHouseEnd()
  elseif event == "QUEST_ASK_GUARDQUEST" then
    _OnGossip_AskGuardQuest(arg1, arg2, arg3, arg4, arg5)
  elseif event == "NPC_GUILD_BUILD_UPDATE" then
    _OnGossip_UpdataGuildBuild(arg1, arg2, arg3, arg4)
  end
end
function Script_Gossip_OnLoad()
  gWndDisplayColor = 0
  gUI.Gossip = {}
  gUI.Gossip.Root = WindowSys_Instance:GetWindow("GossipDialog_pic")
  gUI.Gossip.Transmis = WindowSys_Instance:GetWindow("Transmission")
  gUI.Gossip.Bg1Center = WindowSys_Instance:GetWindow("GossipDialog_pic.Back1_pic.Center_pic")
  gUI.Gossip.Bg2PosTree = WindowToTreeCtrl(WindowSys_Instance:GetWindow("Transmission.Back2_pic.PosList_rctl"))
  gUI.Gossip.Gossip = WindowToContainer(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt"))
  gUI.Gossip.Text = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Text_dbox"))
  gUI.Gossip.Complete_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Complete_bg")
  gUI.Gossip.Complete = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Complete_dbox"))
  gUI.Gossip.Accept_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Accept_bg")
  gUI.Gossip.Accept_db = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Accept_dbox"))
  gUI.Gossip.Proceed_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Proceed_bg")
  gUI.Gossip.Proceed = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Proceed_dbox"))
  gUI.Gossip.Common_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Common_bg")
  gUI.Gossip.Common = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Common_dbox"))
  gUI.Gossip.Jewel_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Jewel_bg")
  gUI.Gossip.Jewel = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Jewel_dbox"))
  gUI.Gossip.Equip_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Equip_bg")
  gUI.Gossip.Equip = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Equip_dbox"))
  gUI.Gossip.GuildNow_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.GuildNow_bg")
  gUI.Gossip.GuildNow = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.GuildNow_dbox"))
  gUI.Gossip.GuildNext_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.GuildNext_bg")
  gUI.Gossip.GuildNext = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.GuildNext_dbox"))
  gUI.Gossip.GuildCondition_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.GuildCondition_bg")
  gUI.Gossip.GuildCondition = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.GuildCondition_dbox"))
  gUI.Gossip.Guild_bg = gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Guild_bg")
  gUI.Gossip.Guild = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.Guild_dbox"))
  gUI.Gossip.GuildLevelup = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Gossip_cnt.GuildLevelup_dbox"))
  gUI.Gossip.Title = WindowSys_Instance:GetWindow("GossipDialog_pic.Back1_pic.Title_dlab")
  gUI.Gossip.HeadPic = gUI.Gossip.Root:GetGrandChild("Back1_pic.Head_pic.HeadPic_pic")
  gUI.Gossip.Back = gUI.Gossip.Bg1Center:GetGrandChild("Back_btn")
  gUI.Gossip.QuestName = gUI.Gossip.Bg1Center:GetGrandChild("TaskName_dlab")
  gUI.Gossip.Quest = WindowToContainer(gUI.Gossip.Bg1Center:GetGrandChild("Task_cnt"))
  gUI.Gossip.QuestDesc = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Task_cnt.Describe_dbox"))
  gUI.Gossip.QuestAward_bg = gUI.Gossip.Bg1Center:GetGrandChild("Task_cnt.Award_bg")
  gUI.Gossip.QuestAward = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Task_cnt.Award_dbox"))
  gUI.Gossip.BasicItem = WindowToGoodsBox(gUI.Gossip.Bg1Center:GetGrandChild("Task_cnt.BasicItem_gbox"))
  gUI.Gossip.ChoiceItem_bg = gUI.Gossip.Bg1Center:GetGrandChild("Task_cnt.ChoiceItem_bg")
  gUI.Gossip.ChoiceItem = WindowToGoodsBox(gUI.Gossip.Bg1Center:GetGrandChild("Task_cnt.ChoiceItem_gbox"))
  gUI.Gossip.Condition_bg = gUI.Gossip.Bg1Center:GetGrandChild("Task_cnt.Condition_bg")
  gUI.Gossip.Condition = WindowToDisplayBox(gUI.Gossip.Bg1Center:GetGrandChild("Task_cnt.Condition_dbox"))
  gUI.Gossip.Accept = gUI.Gossip.Bg1Center:GetGrandChild("Accept_btn")
  for i = 0, MAXLENGTH do
    if _Gossip_MapBtn_List[i] then
      _Gossip_MapBtn_List[i]:SetVisible(false)
    end
  end
end
function _OnGossip_ShowQuestDetail(NpcId, ScenarioQuestId, QuestStatus, QuestType)
  gUI.Gossip.CurNpcId = NpcId
  gUI.Gossip.Root:SetProperty("Visible", "true")
  gUI.Gossip.Back:SetProperty("Visible", "true")
  _ShowNormalQuestDetail(ScenarioQuestId)
  gUI.Gossip.Back:SetProperty("Visible", "false")
end
function _OnGossip_GetNpcTextParam(npcText, subFuncId)
  local strRes = ""
  if subFuncId == 0 then
    local strTime, partyList = GetFirstPastData(subFuncId)
    local strContent1 = ""
    local strContent2 = ""
    for i = 0, table.maxn(partyList) do
      if partyList[i] ~= nil then
        local strCc = partyList[i].name
        strCc = strCc .. "("
        strCc = strCc .. partyList[i].level .. "级"
        if type(partyList[i].menpai) == "number" and 0 <= partyList[i].menpai and partyList[i].menpai < 5 then
          strCc = strCc .. gUI_MenPaiName[partyList[i].menpai]
        else
          strCc = strCc .. "未知"
        end
        strCc = strCc .. ") "
        if partyList[i].isleader then
          strContent1 = strContent1 .. strCc
        else
          strContent2 = strContent2 .. strCc
        end
      end
    end
    if strContent1 == "" then
      strRes = strTime .. " " .. strContent2
    else
      strRes = strTime .. " " .. strContent1 .. "带领着自己的战友 " .. strContent2
    end
    strRes = string.format(npcText, strRes)
  end
  return strRes
end
function _OnGossip_UpdateBegin(GossipText, NpcId, NpcIcon, NpcName, IsParamType, SubFuncType)
  gUI.Gossip.CurNpcId = NpcId
  gFuncItemCount = 0
  gTaskItemCount = 0
  gTransitionFlag = false
  gUI.Gossip.Root:SetProperty("Visible", "false")
  gUI.Gossip.Transmis:SetProperty("Visible", "false")
  gUI.Gossip.Gossip:SetProperty("Visible", "false")
  gUI.Gossip.Gossip:ResetScrollBar()
  gUI.Gossip.Text:ClearAllContent()
  gUI.Gossip.Complete_bg:setEnableHeightCal(false)
  gUI.Gossip.Complete:setEnableHeightCal(false)
  gUI.Gossip.Complete:ClearAllContent()
  gUI.Gossip.Accept_bg:setEnableHeightCal(false)
  gUI.Gossip.Accept_db:setEnableHeightCal(false)
  gUI.Gossip.Accept_db:ClearAllContent()
  gUI.Gossip.Proceed_bg:setEnableHeightCal(false)
  gUI.Gossip.Proceed:setEnableHeightCal(false)
  gUI.Gossip.Proceed:ClearAllContent()
  gUI.Gossip.Common_bg:setEnableHeightCal(false)
  gUI.Gossip.Common:setEnableHeightCal(false)
  gUI.Gossip.Common:ClearAllContent()
  gUI.Gossip.Jewel_bg:setEnableHeightCal(false)
  gUI.Gossip.Jewel:setEnableHeightCal(false)
  gUI.Gossip.Jewel:ClearAllContent()
  gUI.Gossip.Equip_bg:setEnableHeightCal(false)
  gUI.Gossip.Equip:setEnableHeightCal(false)
  gUI.Gossip.Equip:ClearAllContent()
  gUI.Gossip.GuildNow_bg:setEnableHeightCal(false)
  gUI.Gossip.GuildNow:setEnableHeightCal(false)
  gUI.Gossip.GuildNow:ClearAllContent()
  gUI.Gossip.GuildNext_bg:setEnableHeightCal(false)
  gUI.Gossip.GuildNext:setEnableHeightCal(false)
  gUI.Gossip.GuildNext:ClearAllContent()
  gUI.Gossip.GuildCondition_bg:setEnableHeightCal(false)
  gUI.Gossip.GuildCondition:setEnableHeightCal(false)
  gUI.Gossip.GuildCondition:ClearAllContent()
  gUI.Gossip.Guild_bg:setEnableHeightCal(false)
  gUI.Gossip.Guild:setEnableHeightCal(false)
  gUI.Gossip.Guild:ClearAllContent()
  gUI.Gossip.GuildLevelup:setEnableHeightCal(false)
  gUI.Gossip.GuildLevelup:ClearAllContent()
  if IsParamType then
    GossipText = _OnGossip_GetNpcTextParam(GossipText, SubFuncType)
  end
  gUI.Gossip.Text:InsertBack(Lua_UI_TranslateText(GossipText), gWndDisplayColor, 0, 0)
  gUI.Gossip.Title:SetProperty("Text", NpcName)
  gUI.Gossip.HeadPic:SetProperty("BackImage", NpcIcon)
end
function _OnGossip_UpdateAddQuest(QuestTitle, QuestStatus, QuestId)
  local strFunc = string.format("{NpCfUn^%s^%d^%d}", QuestTitle, gUI.Gossip.CurNpcId, gTaskItemCount + gFuncItemCount)
  if QuestStatus == QUEST_STATUS_DONE then
    gUI.Gossip.Complete_bg:setEnableHeightCal(true)
    gUI.Gossip.Complete:setEnableHeightCal(true)
    gUI.Gossip.Complete:InsertBack("{ImAgE^UiBtn001:YellowDoubt_01}" .. strFunc, gWndDisplayColor, 0, 0)
  elseif QuestStatus == QUEST_STATUS_AVAILABLE then
    gUI.Gossip.Accept_bg:setEnableHeightCal(true)
    gUI.Gossip.Accept_db:setEnableHeightCal(true)
    gUI.Gossip.Accept_db:InsertBack("{ImAgE^UiBtn001:YellowPlaint_01}" .. strFunc, gWndDisplayColor, 0, 0)
  elseif QuestStatus == QUEST_STATUS_FAILED then
    gUI.Gossip.Accept_bg:setEnableHeightCal(true)
    gUI.Gossip.Accept_db:setEnableHeightCal(true)
    gUI.Gossip.Accept_db:InsertBack("{ImAgE^UiBtn001:YellowPlaint_01}" .. "{#B^重新接取 }" .. strFunc, gWndDisplayColor, 0, 0)
  elseif QuestStatus == QUEST_STATUS_INCOMPLETE then
    gUI.Gossip.Proceed_bg:setEnableHeightCal(true)
    gUI.Gossip.Proceed:setEnableHeightCal(true)
    gUI.Gossip.Proceed:InsertBack("{ImAgE^UiBtn001:GrayDoubt_01}" .. strFunc, gWndDisplayColor, 0, 0)
  else
    gUI.Gossip.Text:InsertBack(strFunc, gWndDisplayColor, 0, 0)
  end
  gTaskItemCount = gTaskItemCount + 1
end
function _OnGossip_UpdateAddCarnival(CarnivalTitle, CarnivalStatus, CarnivalId)
  local strFunc = string.format("{NpCfUn^%s^%d^%d}", CarnivalTitle, gUI.Gossip.CurNpcId, CarnivalId + 30000)
  if CarnivalStatus == QUEST_STATUS_DONE then
    gUI.Gossip.Complete_bg:setEnableHeightCal(true)
    gUI.Gossip.Complete:setEnableHeightCal(true)
    gUI.Gossip.Complete:InsertBack("{ImAgE^UiBtn001:BuleDoubt_01}" .. strFunc, gWndDisplayColor, 0, 0)
  elseif CarnivalStatus == QUEST_STATUS_AVAILABLE then
    gUI.Gossip.Accept_bg:setEnableHeightCal(true)
    gUI.Gossip.Accept_db:setEnableHeightCal(true)
    gUI.Gossip.Accept_db:InsertBack("{ImAgE^UiBtn001:BluePlaint_01}" .. strFunc, gWndDisplayColor, 0, 0)
  elseif CarnivalStatus == QUEST_STATUS_INCOMPLETE then
  else
    gUI.Gossip.Text:InsertBack(strFunc, gWndDisplayColor, 0, 0)
  end
end
function _OnGossip_UpdateEnd(IsGuildNpc, QuestCount)
  gUI.Gossip.Root:SetProperty("Visible", "true")
  gUI.Gossip.Gossip:SetProperty("Visible", "true")
  gUI.Gossip.Gossip:SizeSelf()
  for i, name in ipairs({
    "Back1_pic.Center_pic.Task_cnt",
    "Back1_pic.Center_pic.Back_btn",
    "Back1_pic.Center_pic.Accept_btn"
  }) do
    gUI.Gossip.Root:GetGrandChild(name):SetProperty("Visible", "false")
  end
  do break end
  gUI.Gossip.QuestName:SetProperty("Text", "今日剩余可交付总数:" .. QuestCount .. "个")
  gUI.Gossip.QuestName:SetProperty("Visible", "true")
  do break end
  gUI.Gossip.QuestName:SetProperty("Text", "")
  gUI.Gossip.QuestName:SetProperty("Visible", "false")
end
function _OnGossip_UpdateAddFunction(Index, FuncId, Text, Count)
  gFuncItemCount = Index + 1
  local strFunc
  if FuncId ~= _NPCFuncType.NPC_FUNC_QUEST_CONTINUE then
    strFunc = string.format("{NpCfUn^%s^%d^%d}", Text, gUI.Gossip.CurNpcId, Index)
  end
  if FuncId == _NPCFuncType.NPC_FUNC_QUEST_CONTINUE then
    strFunc = string.format("{NpCdLg^UiBtn001:Image_npctalk^%s^%d^%d^%d^%d}", Text, gUI.Gossip.CurNpcId, Index, BtnTypeDig.NpcDiglog, Count)
    gUI.Gossip.Text:InsertBack(strFunc, gWndDisplayColor, FuncId, 0)
  elseif _EquipType[FuncId] then
    gUI.Gossip.Equip_bg:setEnableHeightCal(true)
    gUI.Gossip.Equip:setEnableHeightCal(true)
    gUI.Gossip.Equip:InsertBack(_EquipType[FuncId] .. strFunc, gWndDisplayColor, FuncId, 0)
  elseif _GemType[FuncId] then
    gUI.Gossip.Jewel_bg:setEnableHeightCal(true)
    gUI.Gossip.Jewel:setEnableHeightCal(true)
    gUI.Gossip.Jewel:InsertBack(_GemType[FuncId] .. strFunc, gWndDisplayColor, FuncId, 0)
  else
    if _CommonType[FuncId] then
      strFunc = _CommonType[FuncId] .. strFunc
    end
    gUI.Gossip.Common_bg:setEnableHeightCal(true)
    gUI.Gossip.Common:setEnableHeightCal(true)
    gUI.Gossip.Common:InsertBack(strFunc, gWndDisplayColor, FuncId, 0)
  end
  if FuncId == _NPCFuncType.NPC_FUNC_BANK then
    Lua_Bank_CloseBank()
  end
end
function _OnGossip_UpdateTransition(Text, Price, MinLevel, MaxLev, bFlag)
  gTransitionFlag = true
  local strFormat = "{NpCfUn^%s^%d^%d}"
  local strFunc = ""
  if Price > 0 then
    local strCost = Lua_UI_Money2String(Price)
    strFormat = strFormat .. " 需要" .. strCost
  end
  if bFlag == 0 then
    if MinLevel > 0 then
      if MinLevel <= gUI_MainPlayerAttr.Level then
        strFunc = string.format(strFormat .. " 推荐等级 (%d 级- %d 级 )", Text, gUI.Gossip.CurNpcId, gFuncItemCount, MinLevel, MaxLev)
      else
        strFunc = string.format(strFormat .. " {#R^推荐等级 (%d 级- %d 级 )}", Text, gUI.Gossip.CurNpcId, gFuncItemCount, MinLevel, MaxLev)
      end
    else
      strFunc = string.format(strFormat, Text, gFuncItemCount)
    end
  end
  gFuncItemCount = gFuncItemCount + 1
  if bFlag == 0 then
    gUI.Gossip.Common_bg:setEnableHeightCal(true)
    gUI.Gossip.Common:setEnableHeightCal(true)
    gUI.Gossip.Common:InsertBack(strFunc, gWndDisplayColor, 4, 0)
  end
end
function _CalcRandQuestReward(level, times)
  local wuxing = 10 * (1 + level * ((level - 1) / 10 + times * 0.1))
  local exp = 5 * wuxing
  local money = 100
  return exp, wuxing, money
end
function _DeletPathString(PathString)
  local strNPCName = string.match(PathString, "{PaTh^.-^")
  if strNPCName then
    strNPCName = string.sub(strNPCName, 7, -2)
    PathString = string.gsub(PathString, "{PaTh.-}", strNPCName, 1)
    return _DeletPathString(PathString)
  else
    return PathString
  end
end
function _DeletTransString(TransString)
  local tempStr = ""
  return string.gsub(TransString, "{QuTrAnS.-}", tempStr)
end
function _ShowNormalQuestDetail(nTaskId)
  gUI.Gossip.Gossip:SetProperty("Visible", "false")
  gUI.Gossip.Quest:ResetScrollBar()
  gUI.Gossip.Quest:SetProperty("Visible", "true")
  gUI.Gossip.BasicItem:ResetAllGoods(true)
  gUI.Gossip.BasicItem:setEnableHeightCal(false)
  gUI.Gossip.ChoiceItem_bg:setEnableHeightCal(false)
  gUI.Gossip.ChoiceItem:ResetAllGoods(true)
  gUI.Gossip.ChoiceItem:setEnableHeightCal(false)
  local strTitle, strObjective, strGossip, nQuestLevel, nStatus, strQuestDetail, bDisableAbandon, MapName, NpcName, nCurrTimes, nTotalTimes, nQuestType, bCalLimitTime, IsTracking, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, IsGuildExp, bHaveChoiceItem, nCommission, v1, v2, v3, v4, v5, v6, v7 = GetQuestInfoLua(nTaskId, gUI_QuestRewardItemSourceID.NPC_GOSSIP)
  _Gossip_CurTaskName = strTitle
  gCurShowTaskId = nTaskId
  gCurShowTaskStatus = nStatus
  if v1 then
    strObjective = string.format(strObjective, v1, v2, v3, v4, v5, v6, v7)
  end
  if strTitle then
    gUI.Gossip.QuestName:SetProperty("Text", strTitle)
    gUI.Gossip.QuestName:SetProperty("Visible", "true")
  else
    gUI.Gossip.QuestName:SetProperty("Visible", "false")
  end
  local bShowReward = true
  local GuardTime = GetGuardTime(nTaskId)
  if nStatus == QUEST_STATUS_AVAILABLE then
    if GuardTime > 0 then
      Lua_Chat_ShowOSD("QUEST_GUARDTIME", GuardTime)
      str = string.format("接受任务 &%d&", GuardTime)
      gUI.Gossip.Accept:SetProperty("Text", str)
      gUI.Gossip.Accept:SetProperty("Enable", "false")
    else
      gUI.Gossip.Accept:SetProperty("Text", "接受任务")
    end
    gUI.Gossip.Accept:SetProperty("Visible", "true")
  elseif nStatus == QUEST_STATUS_COMPLETE then
    gUI.Gossip.Accept:SetProperty("Text", "完成任务")
    gUI.Gossip.Accept:SetProperty("Visible", "true")
  else
    gUI.Gossip.Accept:SetProperty("Visible", "false")
    bShowReward = false
  end
  gUI.Gossip.QuestDesc:ClearAllContent()
  strGossip = Lua_UI_TranslateText(strGossip)
  if strGossip == nil or strGossip == "" then
    LogMsg(string.format("config gossip string is null, quest id is %d.(in function GetQuestInfoLua())", nTaskId))
  end
  gUI.Gossip.QuestDesc:InsertBack(strGossip, 0, 0, 0)
  if bShowReward then
    gUI.Gossip.QuestAward_bg:setEnableHeightCal(true)
    gUI.Gossip.QuestAward:setEnableHeightCal(true)
    local strFormatedReward = ""
    local strReward = ""
    if RewExp and RewExp > 0 then
      strReward = string.format("经验值：%d点\n", RewExp)
      strFormatedReward = strFormatedReward .. strReward
      if IsGuildExp > 0 then
        local currentExp, nextExp = GetGuildLevMulitExp()
        if currentExp > 0 then
          strReward = string.format("(当前帮会等级加成经验值: %d)\n", tonumber(RewExp * currentExp / 100))
          strFormatedReward = strFormatedReward .. strReward
        end
        if nextExp > 0 and currentExp > 0 then
          strReward = string.format("(下一级加成经验值: %d)\n", tonumber(RewExp * nextExp / 100))
          strFormatedReward = strFormatedReward .. strReward
        end
      end
    end
    if RewNimbus and RewNimbus > 0 then
      strReward = string.format("精魄值：%d点\n", RewNimbus)
      strFormatedReward = strFormatedReward .. strReward
    end
    if RewMoney and RewMoney > 0 then
      strReward = Lua_UI_Money2String(RewMoney)
      strFormatedReward = strFormatedReward .. "金  币：" .. strReward .. "\n"
    end
    if RewBindedMoney and RewBindedMoney > 0 then
      strReward = Lua_UI_Money2String(RewBindedMoney)
      strFormatedReward = strFormatedReward .. "绑  金：" .. strReward .. "\n"
    end
    if RewWrathValue and RewWrathValue ~= 0 then
      strReward = tostring(-RewWrathValue)
      strFormatedReward = strFormatedReward .. "减少天诛值：" .. strReward .. "\n"
    end
    if GuildMoney and GuildMoney > 0 then
      strReward = string.format("帮会资金：%s\n", Lua_UI_Money2String(GuildMoney * 10000))
      strFormatedReward = strFormatedReward .. strReward
    end
    if GuildActive and GuildActive > 0 then
      strReward = string.format("帮会活跃值：%d点\n", GuildActive)
      strFormatedReward = strFormatedReward .. strReward
    end
    if GuildContribution and GuildContribution > 0 then
      strReward = string.format("帮会贡献度：%d点\n", GuildContribution)
      strFormatedReward = strFormatedReward .. strReward
    end
    if GuildConstruction and GuildConstruction > 0 then
      strReward = string.format("帮会发展度：%d点\n", GuildConstruction)
      strFormatedReward = strFormatedReward .. strReward
    end
    if GuildPractice and GuildPractice > 0 then
      strReward = string.format("守护兽成长值：%d点\n", GuildPractice)
      strFormatedReward = strFormatedReward .. strReward
    end
    gUI.Gossip.QuestAward:ClearAllContent()
    gUI.Gossip.QuestAward:InsertBack(strFormatedReward, 0, 0, 0)
    if gUI.Gossip.BasicItem:IsItemHasIcon(0) then
      gUI.Gossip.BasicItem:setEnableHeightCal(true)
    end
    gIsNeedChoiceReward = bHaveChoiceItem
    if gUI.Gossip.ChoiceItem:IsItemHasIcon(0) then
      gUI.Gossip.ChoiceItem_bg:setEnableHeightCal(true)
      gUI.Gossip.ChoiceItem:setEnableHeightCal(true)
    end
  else
    gUI.Gossip.QuestAward_bg:setEnableHeightCal(false)
    gUI.Gossip.QuestAward:setEnableHeightCal(false)
  end
  gUI.Gossip.Condition:ClearAllContent()
  strObjective = Lua_UI_TranslateText(strObjective)
  strObjective = _DeletPathString(strObjective)
  strObjective = _DeletTransString(strObjective)
  gUI.Gossip.Condition:InsertBack(strObjective, 0, 0, 0)
  gUI.Gossip.Quest:SizeSelf()
  TriggerUserGuide(17, nTaskId, nStatus)
end
function _OnGossip_AddRewardItem(Index, GoodsId, Icon, GoodsCount, sourceId)
  if sourceId == gUI_QuestRewardItemSourceID.NPC_GOSSIP then
    gUI.Gossip.BasicItem:SetItemCount(Index + 1)
    gUI.Gossip.BasicItem:SetItemGoods(Index, Icon, Lua_Bag_GetQuality(-1, GoodsId))
    gUI.Gossip.BasicItem:SetItemData(Index, GoodsId)
    if GoodsCount > 1 then
      gUI.Gossip.BasicItem:SetItemSubscript(Index, tostring(GoodsCount))
    end
  end
end
function _OnGossip_AddChoiceItem(Index, GoodsId, Icon, GoodsCount, sourceId)
  if sourceId == gUI_QuestRewardItemSourceID.NPC_GOSSIP then
    gUI.Gossip.ChoiceItem:SetItemCount(Index + 1)
    gUI.Gossip.ChoiceItem:SetItemGoods(Index, Icon, Lua_Bag_GetQuality(-1, GoodsId))
    gUI.Gossip.ChoiceItem:SetItemData(Index, GoodsId)
    if GoodsCount > 1 then
      gUI.Gossip.ChoiceItem:SetItemSubscript(Index, tostring(GoodsCount))
    end
  end
end
function _OnGossip_SelectItem(npcId, nSel)
  local NPC_TYPE_FUNCTION = 0
  local NPC_TYPE_TRANSMISSION = 1
  local NPC_TYPE_RESET_INSTANCE = 2
  local NPC_TYPE_ANSWER = 3
  local NPC_TYPE_CARNIVAL = 4
  if nSel >= 20000 and nSel < 30000 then
    SelectGossipOption(npcId, nSel, NPC_TYPE_ANSWER)
  elseif nSel >= 1000 and nSel < 20000 then
    SelectGossipOption(npcId, nSel, NPC_TYPE_RESET_INSTANCE)
    UI_Gossip_CloseBtnClick()
  elseif gTransitionFlag then
    SelectGossipOption(npcId, nSel, NPC_TYPE_TRANSMISSION)
  elseif nSel < gFuncItemCount then
    SelectGossipOption(npcId, nSel, NPC_TYPE_FUNCTION)
  elseif nSel >= 30000 then
    SelectGossipOption(npcId, nSel - 30000, NPC_TYPE_CARNIVAL)
    UI_Gossip_CloseBtnClick()
  else
    local taskid, taskstatus, nDayTimes, nTotalTimes = GetTaskIDStatusByIndex(nSel - gFuncItemCount)
    if taskid == nil then
      return
    end
    if taskstatus == QUEST_STATUS_FAILED then
      Messagebox_Show("TaskReGet", taskid, npcId)
      return
    end
    gUI.Gossip.Back:SetProperty("Visible", "true")
    _ShowNormalQuestDetail(taskid)
  end
end
function _OnGossip_End()
  local wnd = WindowSys_Instance:GetWindow("GossipDialog_pic")
  local wnd2 = WindowSys_Instance:GetWindow("Transmission")
  if wnd:IsVisible() or wnd2:IsVisible() then
    UI_Gossip_CloseBtnClick()
  end
end
function _OnGossip_CloseNpcRelativeDialog()
  local wnd = WindowSys_Instance:GetWindow("Transmission")
  local wnd2 = WindowSys_Instance:GetWindow("GossipDialog_pic")
  if wnd:IsVisible() or wnd2:IsVisible() then
    UI_Gossip_CloseBtnClick()
  end
end
function _OnGossip_QuestRemove(QuestId)
  UI_Gossip_CloseBtnClick()
end
function _OnGossip_UpdataGuildBuild(level, guildId, maxLevel, bMyGuild)
  local desc = {}
  local descN = {}
  local color = 0
  if bMyGuild == false then
    gUI.Gossip.Text:InsertBack("不是我的帮会建筑", 4294903313, 0, 0)
  else
    local buildType, needGold, curGold, needValue, curValue, needBuildId, needBuildLevel
    buildType, desc[1], desc[2], needGold, curGold, needValue, curValue, needBuildId, needBuildLevel, needGrowPoint, curGrowPoint, needPopulation, curPopulation, upgradeTime, LevCondition = GetGuildUpdateInfo(level, guildId)
    if level < maxLevel then
      local buildTypeN, needGoldN, curGoldN, needValueN, curValueN, needBuildIdN, needBuildLevelN
      buildTypeN, descN[1], descN[2], needGoldN, curGoldN, needValueN, curValueN, needBuildIdN, needBuildLevelN, needGrowPointN, curGrowPointN, needPopulationN, curPopulationN, needupgradeTime = GetGuildUpdateInfo(level + 1, guildId)
    end
    gUI.Gossip.GuildCondition_bg:setEnableHeightCal(true)
    gUI.Gossip.GuildCondition:setEnableHeightCal(true)
    gUI.Gossip.GuildCondition:InsertBack(gUI_BuildDescribe[buildType], color, 0, 0)
    gUI.Gossip.GuildNow_bg:setEnableHeightCal(true)
    gUI.Gossip.GuildNow:setEnableHeightCal(true)
    if level < maxLevel then
      if desc[1] == descN[1] then
        gUI.Gossip.GuildNow:InsertBack(desc[1], color, 0, 0)
      else
        gUI.Gossip.GuildNow:InsertBack(desc[1], color, 0, 0)
        gUI.Gossip.GuildNext_bg:setEnableHeightCal(true)
        gUI.Gossip.GuildNext:setEnableHeightCal(true)
        gUI.Gossip.GuildNext:InsertBack(descN[1], color, 0, 0)
      end
    else
      gUI.Gossip.GuildNow:InsertBack(desc[1], color, 0, 0)
    end
    gUI.Gossip.Guild_bg:setEnableHeightCal(true)
    gUI.Gossip.Guild:setEnableHeightCal(true)
    gUI.Gossip.Guild:InsertBack("当前建筑等级：" .. tostring(level), color, 0, 0)
    if maxLevel <= level then
      gUI.Gossip.Guild:InsertBack("已到最大等级", 4294903313, 0, 0)
      return
    end
    gUI.Gossip.GuildLevelup:setEnableHeightCal(true)
    local list = GetGuildBuildInfo()
    color = 0
    if needBuildId ~= -1 then
      if needBuildLevel > list[needBuildId].lv then
        color = 4294903313
      end
      gUI.Gossip.GuildLevelup:InsertBack(string.format("前置建筑：%d级 %s", needBuildLevel, gUI_BuildName[needBuildId]), color, 0, 0)
    end
    color = 0
    if needGold > 0 then
      if curGold < needGold then
        color = 4294903313
      end
      gUI.Gossip.GuildLevelup:InsertBack(string.format("帮会资金：%d金 (当前:%d金)", needGold, curGold), color, 0, 0)
    end
    if needValue > 0 then
      color = 0
      if curValue < needValue then
        color = 4294903313
      end
      gUI.Gossip.GuildLevelup:InsertBack(string.format("帮会建设值：%d点 (当前:%d点)", needValue, curValue), color, 0, 0)
    end
    if 0 < needGrowPoint then
      color = 0
      if needGrowPoint > curGrowPoint then
        color = 4294903313
      end
      gUI.Gossip.GuildLevelup:InsertBack(string.format("需求发展度：%d点 (当前:%d点)", needGrowPoint, curGrowPoint), color, 0, 0)
    end
    if 0 < needPopulation then
      color = 0
      if needPopulation > curPopulation then
        color = 4294903313
      end
      gUI.Gossip.GuildLevelup:InsertBack(string.format("需求人口：%d人 (当前:%d人)", needPopulation, curPopulation), color, 0, 0)
    end
    if 0 < needupgradeTime then
      color = 0
      local timeStr = Lua_UI_DetailTime(needupgradeTime)
      gUI.Gossip.GuildLevelup:InsertBack(string.format("所需升级时间：%s", timeStr), color, 0, 0)
    end
    if list[buildType].BuildTime ~= "" then
      gUI.Gossip.GuildLevelup:InsertBack(string.format("升级剩余时间：%s", list[buildType].BuildTime), color, 0, 0)
    end
    if LevCondition[0] ~= nil then
      for i, item in pairs(LevCondition) do
        local lv = item.Lv
        if lv ~= -1 then
          color = 0
          if not item.Can then
            color = 4294903313
          end
          gUI.Gossip.GuildLevelup:InsertBack(string.format("%d级成员人数：%d人 (当前:%d人)", lv, item.Count, item.Current), color, 0, 0)
        end
      end
    end
  end
end
function _OnGossip_PostHouseBegin(NpcId, NpcIcon, NpcName)
  gUI.Gossip.CurNpcId = NpcId
  gTransitionFlag = false
  CURRENTSELECTINDEX = -1
  gUI.Gossip.Root:SetProperty("Visible", "false")
  gUI.Gossip.Transmis:SetProperty("Visible", "false")
  gUI.Gossip.Title:SetProperty("Text", "")
  gUI.Gossip.HeadPic:SetProperty("BackImage", NpcIcon)
  gUI.Gossip.Bg2PosTree:DeleteAllItems()
  for i = 0, MAXLENGTH do
    if _Gossip_MapBtn_List[i] then
      _Gossip_MapBtn_List[i]:SetVisible(false)
    end
  end
  _Gossip_MapBtn_List = {}
end
function _OnGossip_AskGuardQuest(AskerName, QustTitle, NpcId, QustId, PlayerId)
  Messagebox_Show("ASK_GUARDQUEST", AskerName, QustTitle, NpcId, QustId, PlayerId)
end
function _OnGossip_HideMessageBox()
  Messagebox_Hide("TRANSMISSION")
end
function _OnGossip_PostHouseAddItem(Text, Level, Price, Index, MapName, MiniMapX, MiniMapY, IconName, IsCanTran)
  local _, _, _, _, level_self = GetPlaySelfProp(4)
  _GossipMapName[Index] = Text
  local parent_color = 4286248381
  local subitem_color = 4294638330
  if Level > level_self then
    subitem_color = 4294903313
  end
  gTransitionFlag = true
  if Level <= level_self then
    local parent = gUI.Gossip.Bg2PosTree:FindFirstItemWithText(MapName)
    if not parent then
      parent = gUI.Gossip.Bg2PosTree:InsertItem(MapName, nil, "kaiti_12", parent_color)
      parent:SetUserData(-1)
    end
    local subitem = gUI.Gossip.Bg2PosTree:InsertItem(Text, parent, "fzheiti_11", subitem_color)
    subitem:SetUserData(Index)
    subitem:SetCustomUserData(0, Index)
    subitem:SetCustomUserData(1, Level)
    subitem:SetCustomUserData(2, Price)
    subitem:SetCustomUserData(3, IsCanTran)
    subitem:SetProperty("Text", MapName .. "-" .. Text)
  end
  if Level > gUI_MainPlayerAttr.Level then
    return
  end
  local btn_parent = WindowSys_Instance:GetWindow("Transmission.Back2_pic.Center_pic")
  local btn_temp = WindowSys_Instance:GetWindow("Transmission.Back2_pic.Center_pic.Pos01_btn")
  local btnPic_temp = WindowToPicture(btn_temp:GetChildByName("TranBtn_pic"))
  _Gossip_MapBtn_List[Index] = WindowSys_Instance:CreateWndFromTemplate(btn_temp, btn_parent)
  _Gossip_MapBtn_List[Index]:SetLeft(p2u_x_proxy(MiniMapX))
  _Gossip_MapBtn_List[Index]:SetTop(p2u_y_proxy(MiniMapY))
  _Gossip_MapBtn_List[Index]:SetVisible(true)
  _Gossip_MapBtn_List[Index]:SetCustomUserData(0, Index)
  _Gossip_MapBtn_List[Index]:SetCustomUserData(1, Level)
  _Gossip_MapBtn_List[Index]:SetCustomUserData(2, Price)
  _Gossip_MapBtn_List[Index]:SetCustomUserData(3, IsCanTran)
  _Gossip_MapBtn_List[Index]:SetProperty("Value", MapName .. "-" .. Text)
  local wnd = WindowToPicture(_Gossip_MapBtn_List[Index]:GetGrandChild("TranBtn_pic"))
  wnd:SetVisible(true)
  wnd:SetProperty("BackImage", tostring(IconName))
  local temp_btn = WindowToButton(_Gossip_MapBtn_List[Index])
  local _, money, bmoney = GetBankAndBagMoney()
  local bool_tran = false
  if IsCanTran == 1 then
    if Level > gUI_MainPlayerAttr.Level then
      temp_btn:SetProperty("NormalImage", tostring("UiIamge004:Gray_n"))
      temp_btn:SetProperty("HoverImage", tostring("UiIamge004:Gray_n"))
      temp_btn:SetProperty("PushImage", tostring("UiIamge004:Gray_n"))
      temp_btn:SetProperty("DisableImage", tostring("UiIamge004:Gray_n"))
      temp_btn:SetProperty("SelectedImage", tostring("UiIamge004:Gray_h"))
      bool_tran = true
    elseif Level <= gUI_MainPlayerAttr.Level and Price <= money + bmoney then
      temp_btn:SetProperty("NormalImage", tostring("UiIamge004:Green_p"))
      temp_btn:SetProperty("HoverImage", tostring("UiIamge004:Green_n"))
      temp_btn:SetProperty("PushImage", tostring("UiIamge004:Green_h"))
      temp_btn:SetProperty("DisableImage", tostring("UiIamge004:Gray_h"))
      temp_btn:SetProperty("SelectedImage", tostring("UiIamge004:Green_h"))
    elseif Price > money + bmoney then
      temp_btn:SetProperty("NormalImage", tostring("UiIamge004:Gray_n"))
      temp_btn:SetProperty("HoverImage", tostring("UiIamge004:Gray_n"))
      temp_btn:SetProperty("PushImage", tostring("UiIamge004:Gray_h"))
      temp_btn:SetProperty("DisableImage", tostring("UiIamge004:Gray_n"))
      temp_btn:SetProperty("SelectedImage", tostring("UiIamge004:Gray_h"))
    end
  else
    temp_btn:SetProperty("NormalImage", tostring("UiIamge004:Blue_n"))
    temp_btn:SetProperty("HoverImage", tostring("UiIamge004:Blue_n"))
    temp_btn:SetProperty("PushImage", tostring("UiIamge004:Blue_h"))
    temp_btn:SetProperty("DisableImage", tostring("UiIamge004:Gray_n"))
    temp_btn:SetProperty("SelectedImage", tostring("UiIamge004:Blue_h"))
  end
  if bool_tran then
    _Gossip_MapBtn_List[Index]:SetVisible(false)
  else
    _Gossip_MapBtn_List[Index]:SetVisible(true)
  end
end
function _OnGossip_PostHouseEnd()
  local needShow = true
  if IsAcceptQuestArrived ~= nil and AccpetQuestCount ~= nil then
    needShow = not IsAcceptQuestArrived or AccpetQuestCount > 0
  end
  gUI.Gossip.Root:SetProperty("Visible", "false")
  gUI.Gossip.BgMain = WindowSys_Instance:GetWindow("Transmission")
  gUI.Gossip.BgMain:SetProperty("Visible", "true")
  gUI.Gossip.Transmis:SetProperty("Visible", "true")
end

