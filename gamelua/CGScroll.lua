if gUI and not gUI.CGScroll then
  gUI.CGScroll = {}
end
local _Scroll_RewardList = {
  [0] = "物品",
  "经验",
  "精魄"
}
function _OnScroll_UpdateMoney(arg1, arg2)
  if not arg1 then
    _, arg1, _ = GetBankAndBagMoney()
  end
  gUI.CGScroll.Coin:SetProperty("Text", Lua_UI_Money2String(arg1))
  arg2 = arg2 or -1
  if arg2 == -1 then
    return
  elseif arg2 > 0 then
    WorldStage:playSoundByID(1)
  elseif arg2 < 0 then
    WorldStage:playSoundByID(2)
  else
    return
  end
end
function _OnScroll_UpdateMoneyBind(arg1, arg2)
  if not arg1 then
    _, _, arg1 = GetBankAndBagMoney()
  end
  gUI.CGScroll.Boundcoin:SetProperty("Text", Lua_UI_Money2String(arg1))
  arg2 = arg2 or -1
  if arg2 == -1 then
    return
  elseif arg2 > 0 then
    WorldStage:playSoundByID(1)
  elseif arg2 < 0 then
    WorldStage:playSoundByID(2)
  else
    return
  end
end
function _Scroll_Show()
  Lua_Bag_ShowUI(true)
  _OnScroll_UpdateMoney()
  _OnScroll_UpdateMoneyBind()
  Lua_Scroll_DelItem()
  gUI.CGScroll.ScrollLog:ClearAllContent()
  gUI.CGScroll.Spend:SetProperty("Text", Lua_UI_Money2String(0))
  local daily = GetInentifyDaily()
  if daily then
    gUI.CGScroll.Daily:SetProperty("Text", tostring(daily))
  else
    gUI.CGScroll.Daily:SetProperty("Text", tostring(0))
  end
  gUI.CGScroll.Root:SetProperty("Visible", "true")
end
function _OnScroll_Show()
  if gUI.CGScroll.Root:IsVisible() then
    UI_Scroll_Close()
  else
    _Scroll_Show()
  end
end
function _OnScroll_AddItem(bagType, slot, icon, itemType)
  local isStoreMap, cost = GetItemIsStoremap(1, slot)
  if not isStoreMap then
    Lua_Chat_ShowOSD("CARNIVAL_NOT_ITEM")
    return
  end
  local _, icon, quality, _, _, _, _, _, _, _, _, itemCount = GetItemInfoBySlot(bagType, slot, 0)
  if not icon then
    Lua_Scroll_DelItem()
    return
  end
  gUI.CGScroll.Scrollgb:SetItemGoods(0, icon, quality)
  gUI.CGScroll.Scrollgb:SetItemData(0, slot)
  if itemCount > 1 then
    gUI.CGScroll.Scrollgb:SetItemSubscript(0, tostring(itemCount))
  end
  gUI.CGScroll.Spend:SetProperty("Text", Lua_UI_Money2String(cost))
end
function _OnScroll_GetReward(typeReward, count, quality, name)
  local strReward = ""
  if typeReward > 3 then
    return
  end
  if typeReward == 0 then
    strReward = string.format("获得物品{#C^%s^【%s】*%d}", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], name, count)
  else
    strReward = string.format("获得%s%d点", _Scroll_RewardList[typeReward], count)
  end
  gUI.CGScroll.ScrollLog:InsertBack(strReward, 0, 0, 0)
  local daily = GetInentifyDaily()
  if daily then
    gUI.CGScroll.Daily:SetProperty("Text", tostring(daily))
  else
    gUI.CGScroll.Daily:SetProperty("Text", tostring(0))
  end
end
function UI_Scroll_Close(msg)
  CancleStoremapItem()
  Lua_Scroll_DelItem()
  gUI.CGScroll.ScrollLog:ClearAllContent()
  gUI.CGScroll.Root:SetProperty("Visible", "false")
end
function UI_Scroll_RClickItem(msg)
  CancleStoremapItem()
  Lua_Scroll_DelItem()
end
function UI_Scroll_ItemDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local itemId = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    itemId = Lua_Bag_GetRealSlot(from_win, itemId)
    local bagType = gUI_GOODSBOX_BAG[from_type]
    SetStoremapItem(itemId, 1, bagType)
  else
    Lua_Chat_ShowOSD("CARNIVAL_NOT_ITEM")
    return
  end
end
function UI_Scroll_DoStoreMap(msg)
  ExchangeStoremap()
end
function Lua_Scroll_DropItem(from_win, slot)
  slot = Lua_Bag_GetRealSlot(from_win, slot)
  SetStoremapItem(slot, 1, 1)
end
function Lua_Scroll_DelItem()
  gUI.CGScroll.Scrollgb:ClearGoodsItem(0)
  gUI.CGScroll.Spend:SetProperty("Text", Lua_UI_Money2String(0))
end
function Script_Scroll_OnEvent(event)
  if event == "CARNIVAL_SHOW_STOREMAP_INENTIFY" then
    _OnScroll_Show()
  elseif event == "PLAYER_MONEY_CHANGED" then
    _OnScroll_UpdateMoney(arg1, arg2)
  elseif event == "PLAYER_BIND_MONEY_CHANGED" then
    _OnScroll_UpdateMoneyBind(arg1, arg2)
  elseif event == "CARNIVAL_ADD_STOREMAP_INENTIFY" then
    _OnScroll_AddItem(arg1, arg2, arg3, arg4)
  elseif event == "CARNIVAL_DEL_STOREMAP_INENTIFY" then
    Lua_Scroll_DelItem()
  elseif event == "CARNIVAL_STOREMAP_GETREWARD" then
    _OnScroll_GetReward(arg1, arg2, arg3, arg4)
  end
end
function Script_Scroll_OnLoad(event)
  gUI.CGScroll.Root = WindowSys_Instance:GetWindow("CGScroll")
  gUI.CGScroll.Boundcoin = WindowToLabel(WindowSys_Instance:GetWindow("CGScroll.Scroll_bg.Scroll_bg.Boundcoin_bg.Boundcoin_dlab"))
  gUI.CGScroll.Coin = WindowToLabel(WindowSys_Instance:GetWindow("CGScroll.Scroll_bg.Scroll_bg.Coin_bg.Coin_dlab"))
  gUI.CGScroll.Spend = WindowToLabel(WindowSys_Instance:GetWindow("CGScroll.Scroll_bg.Scroll_bg.Spend_dlab"))
  gUI.CGScroll.Scrollgb = WindowToGoodsBox(WindowSys_Instance:GetWindow("CGScroll.Scroll_bg.Scroll_bg.Scroll_gbox"))
  gUI.CGScroll.ScrollLog = WindowToDisplayBox(WindowSys_Instance:GetWindow("CGScroll.Scroll_bg.Scroll_bg.Scroll_bg.Scroll_dbox"))
  gUI.CGScroll.Daily = WindowToLabel(WindowSys_Instance:GetWindow("CGScroll.Scroll_bg.Scroll_bg.Remain_dlab"))
  Lua_Scroll_DelItem()
  gUI.CGScroll.ScrollLog:ClearAllContent()
  gUI.CGScroll.Root:SetProperty("Visible", "false")
end
ll.ScrollLog:ClearAllContent()
  gUI.CGScroll.Root:SetProperty("Visible", "false")
end
