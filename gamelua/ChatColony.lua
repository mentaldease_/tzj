if gUI and not gUI.ChatC then
  gUI.ChatC = {}
end
local _Chatc_MaxGroupNum = 20
function Lua_ChatC_ShowMotionWnd(faceBtn, editBox)
  local faceWnd = WindowSys_Instance:GetWindow("ChatFace")
  if faceWnd:IsVisible() then
    faceWnd:SetProperty("Visible", "false")
  else
    faceWnd:SetProperty("Visible", "true")
    local tablectrl = WindowSys_Instance:GetWindow("ChatFace.CommonFace_tctl")
    local tablectrl = WindowToTableCtrl(tablectrl)
    local selected = tablectrl:GetSelectItem()
    _CFace_ShowPage(selected)
    WindowSys_Instance:SetKeyboardCaptureWindow(editBox)
    _CFace_SetCurChatEditBox(editBox)
    local wnd = faceBtn
    local rect = wnd:GetWndRect()
    faceWnd:SetLeft(rect:get_left() + rect:get_width())
    faceWnd:SetBottom(rect:get_top() + rect:get_height())
  end
end
function Lua_ChatC_InitDisplay()
  gUI.ChatC.DisplayBox:ClearAllContent()
  gUI.ChatC.EditBox:SetProperty("Text", "")
  gUI.ChatC.DisplayBox:MoveToEnd()
  gUI.ChatC.DisplayBox:UpdateScrollBarConfig()
  gUI.ChatC.EditBox:SetProperty("ReadOnly", "false")
end
function _ChatC_ShowMemberNumberInAddUI()
  local count = 0
  for i = 0, table.maxn(gUI.ChatC.AddNameList) do
    if gUI.ChatC.AddNameList[i].state == true then
      count = count + 1
    end
  end
  local total = count + gUI.ChatC.ExistedMemCount
  gUI.ChatC.NameListLab:SetProperty("Text", string.format("添加成员(%d/%d)", total, _Chatc_MaxGroupNum))
end
function _ChatC_ShowText(isTo, context, tarName)
  local tData, tTime = GetDateAndTime()
  local colorTo = 4281597951
  local colorFrom = 4294967040
  local colorText = 0
  gUI.ChatC.DisplayBox:InsertBack(" ", colorText, 0, 0)
  if isTo then
    local nameStr = gUI.ChatC.PlayerName .. " " .. tData .. " " .. tTime
    gUI.ChatC.DisplayBox:InsertBack(nameStr, colorTo, 0, 0)
    gUI.ChatC.DisplayBox:InsertBack(context, colorText, 0, 0)
  else
    local nameStr = tarName .. " " .. tData .. " " .. tTime
    gUI.ChatC.DisplayBox:InsertBack(nameStr, colorFrom, 0, 0)
    gUI.ChatC.DisplayBox:InsertBack(context, colorText, 0, 0)
  end
  if not gUI.ChatC.root:IsVisible() then
    Lua_ChatC_BlinkChatCBtn(true)
  end
end
function _ChatC_SendMsg()
  local context = gUI.ChatC.EditBox:GetProperty("Text")
  if context ~= "" then
    SendChatMessage(g_CHAT_TYPE.GROUP, context, "")
    gUI.ChatC.EditBox:SetProperty("Text", "")
  end
end
function _ChatC_FillTList()
  gUI.ChatC.AddNameListL:RemoveAllItems()
  if gUI.ChatC.AddNameList == nil then
    return
  end
  for i = 0, table.maxn(gUI.ChatC.AddNameList) do
    gUI.ChatC.AddNameListL:InsertString(gUI.ChatC.AddNameList[i].name, -1)
  end
end
function _ChatC_GetIndexByName(name)
  for i = 0, table.maxn(gUI.ChatC.AddNameList) do
    if gUI.ChatC.AddNameList[i].name == name then
      return i
    end
  end
  return nil
end
function _ChatC_ShowGroupMemInList()
  local list = GetGroupMember()
  local myName = GetMyPlayerStaticInfo()
  local count = 0
  local item = gUI.ChatC.AddNameListR:InsertString(myName, -1)
  item:SetColor(4289309097)
  count = count + 1
  if list[0] == nil then
    gUI.ChatC.ExistedMemCount = count
    return
  else
    for i = 0, table.maxn(list) do
      if list[i].name ~= myName then
        local item = gUI.ChatC.AddNameListR:InsertString(list[i].name, -1)
        item:SetColor(4289309097)
        count = count + 1
      end
    end
  end
  gUI.ChatC.ExistedMemCount = count
end
function _ChatC_UpdataState()
  gUI.ChatC.AddNameListR:RemoveAllItems()
  _ChatC_ShowGroupMemInList()
  for i = 0, table.maxn(gUI.ChatC.AddNameList) do
    local item = gUI.ChatC.AddNameListL:GetItem(i)
    if gUI.ChatC.AddNameList[i].state == true then
      item:SetColor(4289309097)
      gUI.ChatC.AddNameListR:InsertString(gUI.ChatC.AddNameList[i].name, -1)
    else
      item:SetColor(4294967295)
    end
    if gUI.ChatC.AddNameList[i].disable == true then
      item:SetColor(4289309097)
    end
  end
end
function Lua_ChatC_BlinkChatCBtn(flag)
  gUI.ChatC.ChatCBtn:SetProperty("Visible", tostring(flag))
  gUI.ChatC.ChatCBg:SetProperty("Visible", tostring(flag))
end
function Lua_ChatC_CloseMainChatWnd()
  gUI.ChatC.root:SetProperty("Visible", "false")
  Lua_Fri_SetFoucsToWorld()
end
function Lua_ChatC_ShowNameListI()
  local list = GetFriendName()
  if list[0] == nil then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_COLONY_NO_FRIEND)
    return
  end
  gUI.ChatC.AddNameList = {}
  for i = 0, table.maxn(list) do
    gUI.ChatC.AddNameList[i] = {}
    gUI.ChatC.AddNameList[i].name = list[i].name
    gUI.ChatC.AddNameList[i].guid = list[i].GUID
    gUI.ChatC.AddNameList[i].state = false
    if list[i].GroupId ~= -1 then
      gUI.ChatC.AddNameList[i].name = list[i].name .. "(已有群)"
      gUI.ChatC.AddNameList[i].disable = true
    elseif list[i].SceneID == -1 then
      gUI.ChatC.AddNameList[i].name = list[i].name .. "(不在线)"
      gUI.ChatC.AddNameList[i].disable = true
    else
      gUI.ChatC.AddNameList[i].disable = false
    end
  end
  gUI.ChatC.AddNameListL:RemoveAllItems()
  gUI.ChatC.AddNameListR:RemoveAllItems()
  _ChatC_FillTList()
  _ChatC_ShowGroupMemInList()
  _ChatC_ShowMemberNumberInAddUI()
  _ChatC_UpdataState()
  gUI.ChatC.AddMenberI:SetProperty("Visible", "true")
end
function Lua_ChatC_ShowChatC()
  gUI.ChatC.MemberList:RemoveAllItems()
  gUI.ChatC.root:SetProperty("Visible", "true")
  local hasGroup, name = GetGroupInfo()
  if hasGroup == false then
    return
  end
  UpdataGChatList()
  gUI.ChatC.GroupNameSlab:SetProperty("Text", name)
  Lua_ChatC_BlinkChatCBtn(false)
end
function UI_ChatC_EditBoxEnter()
  _ChatC_SendMsg()
end
function UI_ChatC_ShowDetial(msg)
  local item = gUI.ChatC.MemberList:GetHoverItem()
  if item == nil then
    return
  end
  Lua_Tip_GroupChatMember(item:GetColumnText(0), msg:get_fwparam(), msg:get_flparam())
end
function UI_ChatC_Close()
  Lua_ChatC_InitDisplay()
  Lua_ChatC_CloseMainChatWnd()
  gUI.ChatC.ChatCBtn:SetProperty("Visible", "true")
  Lua_CFace_CloseFaceWnd()
end
function UI_ChatC_Minize()
  Lua_ChatC_CloseMainChatWnd()
  gUI.ChatC.ChatCBtn:SetProperty("Visible", "true")
  gUI.ChatC.ChatCBtn:GetParent():SetProperty("Blink", "false")
  gUI.ChatC.ChatCBg:SetProperty("Visible", "false")
  Lua_CFace_CloseFaceWnd()
end
function UI_ChatC_FaceBtn(msg)
  Lua_ChatC_ShowMotionWnd(msg:get_window(), gUI.ChatC.EditBox)
end
function UI_ChatC_AddMember()
  Lua_ChatC_ShowNameListI()
end
function UI_ChatC_ExitGroup()
  Messagebox_Show("GROUP_CHAT_QUIT")
end
function UI_ChatC_ExitAddMember()
  gUI.ChatC.AddMenberI:SetProperty("Visible", "false")
end
function UI_ChatC_ChatCBtnClick()
  Lua_ChatC_ShowChatC()
end
function UI_ChatC_CancelAddMember()
  gUI.ChatC.AddMenberI:SetProperty("Visible", "false")
end
function UI_ChatC_ComfirmAddMember()
  if gUI.ChatC.AddNameList == nil then
    return
  end
  local count = 0
  for i = 0, table.maxn(gUI.ChatC.AddNameList) do
    if gUI.ChatC.AddNameList[i].state == true then
      count = count + 1
    end
  end
  total = count + gUI.ChatC.ExistedMemCount
  if total > _Chatc_MaxGroupNum then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_COLONY_MEM_LIMIT)
    return
  end
  for i = 0, table.maxn(gUI.ChatC.AddNameList) do
    if gUI.ChatC.AddNameList[i].state == true then
      AddApplyMember(gUI.ChatC.AddNameList[i].guid)
    end
  end
  CreateChatGroup(gUI.Fri.CreateTempGroupName)
  gUI.ChatC.AddMenberI:SetProperty("Visible", "false")
end
function UI_ChatC_AddRightArrowBtn()
  local selIndex = gUI.ChatC.AddNameListL:GetFirstSelected()
  if selIndex == -1 then
    return
  end
  if gUI.ChatC.AddNameList[selIndex].state == true then
    return
  end
  if gUI.ChatC.AddNameList[selIndex].disable == true then
    Lua_Chat_ShowOSD("FRIEND_ADD_GROUP_HAS_GROUP")
    return
  end
  gUI.ChatC.AddNameList[selIndex].state = true
  _ChatC_UpdataState()
  _ChatC_ShowMemberNumberInAddUI()
end
function UI_ChatC_AddLeftArrowBtn()
  local selIndex = gUI.ChatC.AddNameListR:GetFirstSelected()
  if selIndex == -1 or selIndex < gUI.ChatC.ExistedMemCount then
    return
  end
  local selName = gUI.ChatC.AddNameListR:GetItemText(selIndex, 0)
  local index = _ChatC_GetIndexByName(selName)
  gUI.ChatC.AddNameList[index].state = false
  _ChatC_UpdataState()
  _ChatC_ShowMemberNumberInAddUI()
end
function UI_ChatC_MClick(msg)
  local item = msg:get_window()
  local index = msg:get_wparam()
  item = WindowToListBox(item)
  if item then
    local guid = item:GetItemData64(index)
    local tmpitem = item:GetItem(index)
    if tmpitem then
      local strName = tmpitem:GetColumnText(0)
      Lua_MenuShow("ON_SPEAKER_GROUP", strName, guid)
    end
  end
end
function _OnChatC_GetMsg(typeId, contend, name)
  if typeId == g_CHAT_TYPE.GROUP then
    if name == gUI.ChatC.PlayerName then
      _ChatC_ShowText(true, contend, name)
    else
      _ChatC_ShowText(false, contend, name)
    end
  end
end
function _OnChatC_AddMember(name, isOnline, guid)
  if gUI.ChatC.root:IsVisible() then
    local item = gUI.ChatC.MemberList:InsertString(name, -1)
    item:SetUserData64(guid)
    if isOnline == 0 then
      item:SetColor(4289309097)
    else
      item:SetColor(4294967295)
    end
  end
end
function _OnChatC_DelAllMember()
  if gUI.ChatC.root:IsVisible() then
    gUI.ChatC.MemberList:RemoveAllItems()
  end
end
function _OnChatC_GroupDismiss()
  UI_ChatC_Close()
  Lua_ChatC_BlinkChatCBtn(false)
  Lua_Fri_UpdataChatGroup()
end
function _OnChatC_GroupCreate()
  UI_ChatC_Close()
  Lua_Fri_UpdataChatGroup()
  Lua_ChatC_BlinkChatCBtn(true)
end
function _OnChatC_UpdataCount(count)
  gUI.ChatC.MemberCountLab:SetProperty("Text", string.format("参与人(%d/%d)", count, _Chatc_MaxGroupNum))
end
function Script_ChatC_OnEvent(event)
  if "UI_R_CHAT_MSG" == event then
    _OnChatC_GetMsg(arg1, arg2, arg3)
  elseif "CHATC_ADD_MEMBER" == event then
    _OnChatC_AddMember(arg1, arg2, arg3)
  elseif "CHATC_DEL_ALL_MEMBER" == event then
    _OnChatC_DelAllMember()
  elseif "CHATC_GROUP_DISMISS" == event then
    _OnChatC_GroupDismiss()
  elseif "CHATC_GROUP_CREATE" == event then
    _OnChatC_GroupCreate()
  elseif "CHATC_UPDATA_COUNT" == event then
    _OnChatC_UpdataCount(arg1)
  end
end
function Script_ChatC_OnLoad()
  gUI.ChatC.DisplayBox = WindowToDisplayBox(WindowSys_Instance:GetWindow("ChatColony.Pic1_bg.Picture1_bg.ChatInfo_dbox"))
  gUI.ChatC.AddMenberI = WindowSys_Instance:GetWindow("ChatAddMember")
  gUI.ChatC.AddNameListL = WindowToListBox(WindowSys_Instance:GetWindow("ChatAddMember.Pic1_bg.FriendList_lbox"))
  gUI.ChatC.AddNameListR = WindowToListBox(WindowSys_Instance:GetWindow("ChatAddMember.Pic2_bg.FriendList_lbox"))
  gUI.ChatC.root = WindowSys_Instance:GetWindow("ChatColony")
  gUI.ChatC.root:SetProperty("Visible", "false")
  gUI.ChatC.ChatCBtn = WindowToButton(WindowSys_Instance:GetWindow("ActionBar_frm.ChatC_slab.ChatC_btn"))
  gUI.ChatC.ChatCBg = WindowToPicture(WindowSys_Instance:GetWindow("ActionBar_frm.ChatC_slab.ChatC_btn.ChatCEff_bg"))
  gUI.ChatC.GroupNameSlab = WindowSys_Instance:GetWindow("ChatColony.ColonyName_dlab")
  gUI.ChatC.EditBox = WindowToEditBox(WindowSys_Instance:GetWindow("ChatColony.Pic1_bg.Picture2_bg.EditBox_ebox"))
  gUI.ChatC.MemberList = WindowToListBox(WindowSys_Instance:GetWindow("ChatColony.Pic2_bg.ListBox_lbox"))
  gUI.ChatC.PlayerName = GetMyPlayerStaticInfo()
  gUI.ChatC.NameListLab = WindowSys_Instance:GetWindow("ChatAddMember.Pic2_bg.TitleTip_slab")
  gUI.ChatC.MemberCountLab = WindowSys_Instance:GetWindow("ChatColony.Pic2_bg.MemberCount_dlab")
  gUI.ChatC.ExistedMemCount = 0
  gUI.ChatC.AddNameList = {}
  gUI.ChatC.ShowUIAfterCreate = false
  Lua_ChatC_BlinkChatCBtn(false)
  Lua_ChatC_InitDisplay()
end

