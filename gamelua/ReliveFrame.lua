local _Relive_Des = {
  [0] = "原地复活",
  [1] = "返回复活点",
  [2] = "返回复活点",
  [3] = "技能复活",
  [4] = "返回复活点",
  [5] = "城战复活",
  [6] = "副本复活",
  [7] = "",
  [8] = "出生点复活",
  [9] = "复活点复活",
  [10] = "返回复活点",
  [11] = "",
  [12] = "",
  [13] = "",
  [14] = ""
}
local _Relive_ButtonStr = {
  [1] = "",
  [2] = "",
  [3] = ""
}
local _Relive_Item = false
local _Relive_Carnival_Xiu_Map = 108
local _Relive_Str = "你已死亡，请选择复活方式！"
local _Relive_Str_NewPlayer = "25级之前，死亡不会有任何损失，你可以选择新手复活或者回城复活！"
function _OnRelive_Show(time, btncount, v1, v2, v3, bShow, mapId)
  if mapId == _Relive_Carnival_Xiu_Map then
    UI_CarnivalUI_Relive_Show(time, v1, v2)
    return
  end
  _Relive_Item = bShow
  local values = {
    [1] = v1,
    [2] = v2,
    [3] = v3
  }
  local index = 1
  Lua_Bank_CloseBank()
  Lua_Exchange_CloseApplyWnd()
  local ReliveFrame = WindowSys_Instance:GetWindow("ReliveFrame")
  local ReliveFrameTitle = ReliveFrame:GetChildByName("Label1")
  ReliveFrameTitle:SetProperty("Text", _Relive_Str)
  local btn = ReliveFrame:GetChildByName("btn1")
  if btncount ~= 1 then
    btn:SetProperty("Visible", "true")
    btn:SetProperty("Enable", "false")
    btn:SetCustomUserData(0, values[index])
    local _, _, _, _, level = GetPlaySelfProp(4)
    local str = ""
    if values[index] == 0 and level < gUI_PK_Level then
      str = "新手复活"
      ReliveFrameTitle:SetProperty("Text", _Relive_Str_NewPlayer)
    else
      str = _Relive_Des[values[index]]
    end
    btn:SetProperty("Text", str .. "&3&")
    _Relive_ButtonStr[1] = str
    index = index + 1
  else
    btn:SetProperty("Visible", "false")
  end
  btn = ReliveFrame:GetChildByName("btn2")
  if btncount ~= 2 then
    btn:SetProperty("Visible", "true")
    btn:SetProperty("Enable", "false")
    btn:SetCustomUserData(0, values[index])
    btn:SetProperty("Text", _Relive_Des[values[index]] .. "&3&")
    _Relive_ButtonStr[2] = _Relive_Des[values[index]]
    index = index + 1
  else
    btn:SetProperty("Visible", "false")
  end
  btn = ReliveFrame:GetChildByName("btn3")
  if btncount ~= 1 then
    btn:SetProperty("Visible", "true")
    btn:SetProperty("Enable", "false")
    btn:SetCustomUserData(0, values[index])
    btn:SetProperty("Text", _Relive_Des[values[index]] .. "&3&")
    _Relive_ButtonStr[3] = _Relive_Des[values[index]]
  else
    btn:SetProperty("Visible", "false")
  end
  if time > 0 then
    local wnd = WindowSys_Instance:GetWindow("ReliveFrame.Label2")
    wnd:SetProperty("Text", "&" .. time .. "&")
  end
  local str = UI_SYS_MSG.COMBAT_ON_DEAD
  Lua_Chat_AddFightLog(str)
  ReliveFrame:SetProperty("Visible", "true")
  _Relive_CloseOther()
end
function _Relive_CloseOther()
  Lua_Email_Close()
  UI_Stall_Close()
end
function _OnRelive_Hide()
  _Relive_Item = false
  local ReliveFrame = WindowSys_Instance:GetWindow("ReliveFrame")
  local wnd = WindowSys_Instance:GetWindow("ReliveFrame.Label2")
  wnd:SetProperty("Text", "")
  local str = UI_FIGHT_MSG.COMBAT_ON_RELIVE
  Lua_Chat_AddFightLog(str)
  ReliveFrame:SetProperty("Visible", "false")
end
function Lua_Relive_SetKillerName(killername)
  local wnd = WindowSys_Instance:GetWindow("ReliveFrame.Label1")
  local tex = "你被" .. tostring(killername) .. "击倒"
  wnd:SetProperty("Text", tex .. "，请选择复活方式")
  Lua_Chat_AddSysLog(tex)
end
function UI_Relive_timeout(msg)
  TimeOutRevive()
  Lua_MB_CloseBox("CONFIRM_OTHER_REVIVE")
  Lua_MB_CloseBox("RELIVE_RETURN_ITEM")
end
function UI_Relive_btnClick(msg)
  local btn = msg:get_window()
  local revive_type = btn:GetCustomUserData(0)
  btn:SetProperty("Enable", "false")
  if revive_type == 0 then
    Revive(0)
    btn:SetProperty("Text", _Relive_ButtonStr[1] .. "&1&")
  elseif revive_type == 1 or revive_type == 2 then
    if _Relive_Item then
      Messagebox_Show("RELIVE_RETURN_ITEM")
    else
      Revive(2)
    end
    btn:SetProperty("Text", _Relive_ButtonStr[3] .. "&1&")
  elseif revive_type == 5 then
    Revive(revive_type)
    btn:SetProperty("Text", _Relive_ButtonStr[3] .. "&1&")
  else
    Revive(revive_type)
  end
end
function UI_Relive_skillbtnClick(msg)
  WorldStage:otherRevive(true)
end
function Script_Relive_OnLoad()
end
function Script_Relive_OnShow()
end
function Script_Relive_OnEvent(event)
  if event == "PLAYER_DEAD" then
    _OnRelive_Show(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "PLAYER_RELIVED" then
    _OnRelive_Hide()
  elseif event == "PLAYER_BECOME_GHOST" then
    local ReliveFrame = WindowSys_Instance:GetWindow("ReliveFrame")
    if ReliveFrame:IsVisible() then
      ReliveFrame:SetProperty("Visible", "false")
    end
  end
end
