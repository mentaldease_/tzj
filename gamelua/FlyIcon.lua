if gUI and not gUI.FlyIc then
  gUI.FlyIc = {}
end
FlyIconProcessCDTime = 0.1
FlyIconNodeList = {
  {
    t = 0,
    posx = -92,
    posy = -90,
    size = 32,
    alpha = 1
  },
  {
    t = 0.1,
    posx = -75,
    posy = -107,
    size = 40,
    alpha = 1
  },
  {
    t = 0.2,
    posx = -53,
    posy = -117,
    size = 48,
    alpha = 1
  },
  {
    t = 0.3,
    posx = -31,
    posy = -107,
    size = 40,
    alpha = 1
  },
  {
    t = 0.4,
    posx = -19,
    posy = -90,
    size = 36,
    alpha = 1
  },
  {
    t = 0.5,
    posx = -9,
    posy = -66,
    size = 32,
    alpha = 1
  },
  {
    t = 0.6,
    posx = 0,
    posy = -42,
    size = 28,
    alpha = 1
  },
  {
    t = 0.7,
    posx = 6,
    posy = -21,
    size = 26,
    alpha = 0.8
  },
  {
    t = 0.75,
    posx = 15,
    posy = 15,
    size = 24,
    alpha = 0.5
  },
  {
    t = 0.8,
    posx = nil,
    posy = nil
  }
}
function Lua_FlyIc_Init()
  gUI.FlyIc.LoadingList = {}
  gUI.FlyIc.FlyList = {}
  if gUI.FlyIc.IconList == nil then
    gUI.FlyIc.IconList = {}
    for i = 1, 20 do
      local FlyIconTemp = WindowSys_Instance:GetWindow("ActionBar_frm.FlyIconTemp_bg")
      local FlyIconTempRoot = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg")
      gUI.FlyIc.IconList[i] = WindowSys_Instance:CreateWndFromTemplate(FlyIconTemp, FlyIconTempRoot)
      gUI.FlyIc.IconList[i]:SetVisible(false)
    end
  else
    for i = 1, 20 do
      gUI.FlyIc.IconList[i]:SetVisible(false)
    end
  end
  local bagIconWnd = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Bag_btn")
  local top = bagIconWnd:GetProperty("Top")
  local left = bagIconWnd:GetProperty("Left")
  gUI.FlyIc.BagIconPosy = tonumber(string.sub(top, 2, #top)) * bagIconWnd:GetRenderHeight()
  gUI.FlyIc.BagIconPosx = tonumber(string.sub(left, 2, #left)) * bagIconWnd:GetRenderWidth()
  gUI.FlyIc.LastProcessTime = 0
end
function _FlyIc_GetFreeIconSlot()
  for i = 1, 20 do
    if not gUI.FlyIc.IconList[i]:IsVisible() then
      return i
    end
  end
  return nil
end
function Lua_FlyIc_AddIcon(iconName)
  table.insert(gUI.FlyIc.LoadingList, 1, iconName)
end
function Lua_FlyIc_ProcessLoadingList()
  local curTime = TimerSys_Instance:GetCurrTime()
  if curTime - gUI.FlyIc.LastProcessTime > FlyIconProcessCDTime and gUI.FlyIc.LoadingList[1] ~= nil then
    Lua_FlyIc_StartFly(table.remove(gUI.FlyIc.LoadingList))
    gUI.FlyIc.LastProcessTime = curTime
    local bagIconWnd = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Bag_btn")
    bagIconWnd:SetProperty("NormalImage", "UiBtn012:Bag_GetItem3")
  end
end
function Lua_FlyIc_StartFly(iconName)
  local curTime = TimerSys_Instance:GetCurrTime()
  local flyicon = {}
  flyicon.initTime = curTime
  flyicon.icon = gUI.FlyIc.IconList[_FlyIc_GetFreeIconSlot()]
  flyicon.icon:SetProperty("BackImage", iconName)
  table.insert(gUI.FlyIc.FlyList, flyicon)
end
function Lua_FlyIc_UpdataIconList()
  local i = table.maxn(gUI.FlyIc.FlyList)
  while i > 0 do
    local item = gUI.FlyIc.FlyList[i]
    if item == {} then
      table.remove(gUI.FlyIc.FlyList, i)
      return
    end
    local x, y, size, alpha = _FlyIc_GetInterValue(item.initTime)
    if x ~= nil then
      _FlyIc_DrawIcon(x, y, size, alpha, item.icon)
    else
      item.icon:SetVisible(false)
      table.remove(gUI.FlyIc.FlyList, i)
      if table.maxn(gUI.FlyIc.FlyList) == 0 then
        local bagIconWnd = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Bag_btn")
        bagIconWnd:SetProperty("NormalImage", "UiBtn012:Bag_n")
      end
    end
    i = i - 1
  end
end
function _FlyIc_DrawIcon(x, y, size, alpha, icon)
  icon:SetProperty("Top", "p" .. tostring(gUI.FlyIc.BagIconPosy + y - size / 2))
  icon:SetProperty("Left", "p" .. tostring(gUI.FlyIc.BagIconPosx + x - size / 2))
  icon:SetProperty("Width", "p" .. tostring(size))
  icon:SetProperty("Height", "p" .. tostring(size))
  icon:SetProperty("Alpha", tostring(alpha))
  icon:SetVisible(true)
end
function _FlyIc_GetInterValue(initTime)
  local curTime = TimerSys_Instance:GetCurrTime()
  local localTime = curTime - initTime
  local nodeId = _FlyIc_GetNode(localTime)
  if nodeId == nil then
    return nil
  end
  local startTime = FlyIconNodeList[nodeId].t
  local endTime = FlyIconNodeList[nodeId + 1].t
  local startX = FlyIconNodeList[nodeId].posx
  local endX = FlyIconNodeList[nodeId + 1].posx
  local startY = FlyIconNodeList[nodeId].posy
  local endY = FlyIconNodeList[nodeId + 1].posy
  local startSize = FlyIconNodeList[nodeId].size
  local endSize = FlyIconNodeList[nodeId + 1].size
  local startAphla = FlyIconNodeList[nodeId].alpha
  local endAphla = FlyIconNodeList[nodeId + 1].alpha
  local x = _FlyIc_InterpoVal(startX, endX, startTime, endTime, localTime)
  local y = _FlyIc_InterpoVal(startY, endY, startTime, endTime, localTime)
  local size = _FlyIc_InterpoVal(startSize, endSize, startTime, endTime, localTime)
  local alpha = _FlyIc_InterpoVal(startAphla, endAphla, startTime, endTime, localTime)
  return x, y, size, alpha
end
function _FlyIc_GetNode(localTime)
  for i, node in pairs(FlyIconNodeList) do
    if localTime < node.t then
      if FlyIconNodeList[i].posx == nil then
        return nil
      else
        return i - 1
      end
    end
  end
  return nil
end
function _FlyIc_InterpoVal(beginVal, endVal, beginMark, endMark, flag)
  local p = (flag - beginMark) / (endMark - beginMark)
  local val = beginVal * (1 - p) + endVal * p
  return val
end
