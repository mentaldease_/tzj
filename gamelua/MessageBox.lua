MsgBoxReplaceType = {
  REPLACE,
  NOT_REPLACE,
  SAME_TYPE_HIDE
}
MessageboxTypes = {
  has_button1 = true,
  button1_text = "确定",
  has_button2 = false,
  button2_text = "取消",
  has_button3 = false,
  button3_text = "默认",
  text = "",
  title = "",
  modal_state = false,
  EditLen = 20
}
function MessageboxTypes:__index(key)
  return rawget(MessageboxTypes, key)
end
function MessageboxTypes:Create(...)
  return self
end
function MessageboxTypes:OnAccept()
end
function MessageboxTypes:OnCancel()
end
function MessageboxTypes:OnTimeOut()
end
function MessageboxTypes:Layout()
end
MessageboxTypes.DEFAULT = {
  title = "提示",
  text = "提示",
  has_button1 = true,
  has_button2 = false,
  Create = function(self, content)
    self.text = content
    return self
  end
}
MessageboxTypes.REMACC = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  text1 = "勾选后会自动保存您的帐号",
  text2 = "如果你是网吧用户,不推荐使用",
  text3 = "是否继续?",
  Create = function(self)
    return self
  end,
  OnAccept = function(self)
    local wnd = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg.Account_bg.RemAcc_cbtn")
    local cbtn = WindowToCheckButton(wnd)
    cbtn:SetChecked(true)
  end
}
MessageboxTypes.EDITPASSWORD = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  text1 = "输入大区激活密码",
  HasEditBox = true,
  EditLen = 20,
  Create = function(self)
    return self
  end,
  OnAccept = function(self)
    local pass = WindowToEditBox(self.binded_widget:GetGrandChild("passwords_bg.Edit_ebox"))
    local password = pass:GetProperty("Text")
    Lua_LoginGrp_SendActivePasswordMsgToServer(password)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.EDITPASSWORD1 = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  text1 = "请您运行绿岸手机令牌软件，将显示在",
  text2 = "屏幕上的8位动态密码填入下框中：",
  HasEditBox = true,
  EditLen = 8,
  Create = function(self, isShow)
    if isShow == 1 then
      self.text2 = "屏幕上的8位动态密码填入下框中:(密码错误){#R^&120&}"
    elseif isShow == 0 then
      self.text2 = "屏幕上的8位动态密码填入下框中:{#R^&120&}"
    end
    return self
  end,
  OnAccept = function(self)
    local pass = WindowToEditBox(self.binded_widget:GetGrandChild("passwords_bg.Edit_ebox"))
    local password = pass:GetProperty("Text")
    SendMegToLogin(password)
  end,
  OnCancel = function(self)
  end,
  OnTimeOut = function(self)
    Messagebox_Show("DEFAULT", "动态密码输入超时，请重新登录")
    Disconnect()
  end
}
MessageboxTypes.EXCHANGEPASSWORDS = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  text1 = "输入兑换道具密码",
  HasEditBox = true,
  EditLen = 20,
  Create = function(self)
    return self
  end,
  OnAccept = function(self)
    local pass = WindowToEditBox(self.binded_widget:GetGrandChild("passwords_bg.Edit_ebox"))
    local password = pass:GetProperty("Text")
    _WorldGroup_PasswordExchange(password)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CHANGENAME = {
  title = "",
  has_button1 = true,
  has_button2 = false,
  HasEditBox = true,
  EditLen = 12,
  Create = function(self, arg1, arg2)
    if arg1 == 0 then
      self.text1 = "输入新角色名"
    elseif arg1 == 1 then
      self.text1 = "重新输入新角色名,[角色名输入有误]"
    end
    self.guid = arg2
    return self
  end,
  OnAccept = function(self)
    local ed = WindowToEditBox(self.binded_widget:GetGrandChild("passwords_bg.Edit_ebox"))
    local text = ed:GetProperty("Text")
    SendNewNameToServer(text, self.guid)
    OnCancel()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CHANGENAMENEW = {
  title = "",
  has_button1 = true,
  has_button2 = false,
  HasEditBox = true,
  EditLen = 12,
  Create = function(self, arg1, arg2)
    if arg1 == 0 then
      self.text1 = "输入新角色名"
    elseif arg1 == 1 then
      self.text1 = "重新输入新角色名,[角色名输入有误]"
    end
    self.guid = arg2
    return self
  end,
  OnAccept = function(self)
    local ed = WindowToEditBox(self.binded_widget:GetGrandChild("passwords_bg.Edit_ebox"))
    local text = ed:GetProperty("Text")
    SendNewNameToServer(text, self.guid)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.GIVE_UP_TASK_OR_NOT = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, id)
    self.task_id = id
    self.strTitle = GetQuestInfoLua(self.task_id, gUI_QuestRewardItemSourceID.INVALID)
    self.text1 = string.format("确定要放弃任务[%s]吗？", self.strTitle)
    return self
  end,
  OnAccept = function(self)
    AbandonQuest(self.task_id)
  end
}
MessageboxTypes.WILL_NOT_LOGIN = {
  title = "",
  text1 = "是否确定退出游戏？",
  text2 = "{#R^&10&}后退出",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  OnAccept = function(self)
    Exit()
  end,
  OnCancel = function(self)
    CancelExit()
  end,
  OnTimeOut = function(self)
    Exit()
  end
}
MessageboxTypes.WILL_ENTER_BATTLE = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, name, Id)
    self.text1 = string.format("是否确认进入%s", name)
    self.text2 = "{#R^&60&}退出"
    self.BattleMap = Id
    return self
  end,
  OnAccept = function(self)
    BattleAcceptRequset(self.BattleMap)
  end,
  OnCancel = function(self)
    BattleRefuseRequset(self.BattleMap)
  end,
  OnTimeOut = function(self)
    BattleRefuseRequset(self.BattleMap)
  end
}
MessageboxTypes.TRADA_REQUEST_CLEAR_PASS = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, name)
    self.text1 = string.format("%s向你申请交易，", name)
    self.text2 = "是否需要打开二级密码？"
    return self
  end,
  OnAccept = function(self)
    _OnSafeSetup_PopNumericPad(1)
  end
}
MessageboxTypes.UNSIGN_WARNING = {
  title = "",
  text = "你有装备或神玉在解除签名倒计时中,\n若该操作不是你自己进行的,\n则你的帐号可能存在安全隐患.",
  has_button1 = true,
  has_button2 = false
}
MessageboxTypes.CONNECT_SERVER_FAIL = {
  title = "",
  has_button1 = false,
  has_button2 = false,
  modal_state = true,
  disableESC = true,
  Create = function(self)
    self.text1 = "与服务器的连接中断"
    self.text2 = "{#R^还有&5&自动返回登入界面}"
    return self
  end,
  OnAccept = function(self)
    BackToLogin()
  end,
  OnTimeOut = function(self)
    BackToLogin()
  end
}
MessageboxTypes.CONNECT_SERVER_FAIL2 = {
  title = "",
  text1 = "连接服务器失败",
  has_button1 = true,
  has_button2 = false,
  modal_state = true,
  OnAccept = function(self)
    BackToLogin()
  end
}
MessageboxTypes.EXIT_APP = {
  title = "",
  text1 = "正在退出游戏 {#R^&10&}",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  OnAccept = function(self)
    Exit()
  end,
  OnCancel = function(self)
    CancelExit()
  end,
  OnTimeOut = function(self)
    Exit()
  end
}
MessageboxTypes.ENTERRETURN_APP = {
  title = "",
  text1 = "正在返回选角 {#R^&10&}",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  OnAccept = function(self)
    BackToCharList()
  end,
  OnCancel = function(self)
  end,
  OnTimeOut = function(self)
    BackToCharList()
  end
}
MessageboxTypes.ENTERLOGIN_APP = {
  title = "",
  text1 = "正在返回选服 {#R^&10&}",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  OnAccept = function(self)
    BackToLogin(1)
  end,
  OnCancel = function(self)
  end,
  OnTimeOut = function(self)
    BackToLogin(1)
  end
}
MessageboxTypes.NOTIFY_MSG = {
  title = "",
  text1 = "",
  has_button1 = true,
  modal_state = true
}
MessageboxTypes.NOTIFY_MSG_RELOGIN = {
  title = "",
  text1 = "",
  has_button1 = true,
  modal_state = true,
  OnAccept = function(self)
    if gUI_CurrentStage ~= 1 then
      BackToLogin()
    end
  end,
  OnCancel = function(self)
    if gUI_CurrentStage ~= 1 then
      BackToLogin()
    end
  end
}
MessageboxTypes.CONFIRM_QUIT_TEAM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self)
    self.text1 = string.format("你是否确定离开队伍")
    return self
  end,
  OnAccept = function(self)
    QuitTeam()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_KICK_TEAM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, name)
    self.name = name
    self.text1 = string.format("你是否确定把{#G^%s}移出队伍", name)
    return self
  end,
  OnAccept = function(self)
    KickTeamMember(self.name)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_OTHER_REVIVE = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, name)
    self.text1 = string.format("【%s】准备复活你", name)
    self.text2 = string.format("是否确认？")
    return self
  end,
  OnAccept = function(self)
    AcceptReviveByOther(true)
  end,
  OnCancel = function(self)
    AcceptReviveByOther(false)
  end
}
MessageboxTypes.NPC_ITEM_EXCHANGE = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, npcFunc, param, name)
    self.text1 = string.format("是否确认兑换【%s】", name)
    self.npcfunc = npcFunc
    self.param = param
    return self
  end,
  OnAccept = function(self)
    ItemExchange(self.npcfunc, self.param)
  end
}
MessageboxTypes.NPC_ITEM_DESCOMPSE = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, name, quilty)
    self.text1 = string.format("内有贵重物品是否确认分解")
    self.npcfunc = npcFunc
    self.param = param
    return self
  end,
  OnAccept = function(self)
    DisassComfim()
  end
}
MessageboxTypes.RELIVE_RETURN_ITEM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self)
    self.text1 = string.format("")
    self.text2 = string.format("是否使用涅槃丹免除此次死亡惩罚？")
    return self
  end,
  OnAccept = function(self)
    Revive(1)
  end,
  OnCancel = function(self)
    Revive(2)
  end
}
MessageboxTypes.CONFIRM_SWAP = {
  title = "",
  text = "",
  has_button1 = true,
  button1_text = "接受",
  has_button2 = true,
  button2_text = "拒绝",
  replace_type = "NOT_REPLACE",
  Create = function(self, name)
    self.text1 = string.format("【%s】想要和你交易", name)
    self.text2 = "是否确认?"
    self.text3 = "倒计时: {#R^&15&}"
    gUI.Exchange.ApplyWndState = true
    return self
  end,
  OnAccept = function(self)
    SwapAcceptTradeRequest()
    gUI.Exchange.ApplyWndState = false
  end,
  OnCancel = function(self)
    SwapRefuseTradeRequest()
    gUI.Exchange.ApplyWndState = false
  end,
  OnTimeOut = function(self)
    SwapRefuseTradeRequest()
    gUI.Exchange.ApplyWndState = false
  end
}
MessageboxTypes.LOCK_ITEM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, name, bag, slot, lock)
    self.bag = bag
    self.slot = slot
    self.lock = lock
    if lock then
      self.text = string.format("你确定要锁定物品[%s]?\n锁定物品无法使用或交易,\n解锁需要48小时后才能生效!", name)
    else
      self.text = string.format("你确定要解锁物品[%s]?\n解锁需要48小时后才能生效!", name)
    end
    return self
  end,
  OnAccept = function(self)
    LockItem(self.bag, self.slot, self.lock)
  end
}
MessageboxTypes.DESTORY_ITEM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, name, bag, slot, num, color)
    self.bag = bag
    self.slot = slot
    self.num = num
    self.text1 = string.format("确定要丢弃{#C^%s^【%s】}*%d吗？", gUI_GOODS_QUALITY_COLOR_PREFIX[color], name, num)
    return self
  end,
  OnAccept = function(self)
    DropItem(self.bag, self.slot)
  end
}
MessageboxTypes.TaskReGet = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, QuestId, npcId)
    self.QuestId = QuestId
    self.npcId = npcId
    self.text1 = string.format("是否重新开始当前已失败的任务？")
    return self
  end,
  OnAccept = function(self)
    QB_DropReGetTask(self.QuestId, self.npcId)
  end
}
MessageboxTypes.BUYITEM_CONFIRM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, typeIndex, ItemIndex, num)
    self.typeIndex = typeIndex
    self.ItemIndex = ItemIndex
    self.num = num
    self.text1 = "您的绑金不足，扣除绑金的剩余部分将由金币支付"
    return self
  end,
  OnAccept = function(self)
    BuyItem(self.typeIndex, self.ItemIndex, self.num)
  end
}
MessageboxTypes.BUYBACKUNBMITEM_CONFIRM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, typeIndex, ItemIndex, num, money, name, quality)
    self.typeIndex = typeIndex
    self.ItemIndex = ItemIndex
    self.num = num
    self.text1 = string.format("回购{#C^%s^%s} 需要扣除%s 金币", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "[" .. name .. "]", Lua_UI_Money2String(money))
    self.text2 = "是否确认回购?"
    return self
  end,
  OnAccept = function(self)
    BuyItem(self.typeIndex, self.ItemIndex, self.num)
  end
}
MessageboxTypes.FRIENDS_DEL_RELATION = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, relation_type, guid, name)
    self.relation_type = relation_type
    self.guid = guid
    self.text1 = string.format("确定要和【%s】", name)
    self.text2 = string.format("解除好友关系吗？")
    self.text3 = string.format("解除后友好度将清零")
    return self
  end,
  OnAccept = function(self)
    DeleteRelation(self.relation_type, self.guid)
  end
}
MessageboxTypes.ENEMY_DEL_RELATION = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, relation_type, guid, name)
    self.relation_type = relation_type
    self.guid = guid
    self.text1 = string.format("是否确定需要删除仇人")
    return self
  end,
  OnAccept = function(self)
    DeleteRelation(self.relation_type, self.guid)
  end
}
MessageboxTypes.FRIENDS_ADD_RELATION = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, guid, name, level)
    self.guid = guid
    self.text1 = string.format(string.format("【%s(%d级)】", name, level))
    self.text2 = string.format(string.format("请求加你为好友"))
    self.text3 = string.format(string.format("是否确认？"))
    return self
  end,
  OnAccept = function(self)
    AcceptOrRefuseApply(true, self.guid)
  end,
  OnCancel = function(self)
    AcceptOrRefuseApply(false, self.guid)
  end
}
MessageboxTypes.ASK_ADD_FRIEND_FLOWER = {
  title = "",
  text = "",
  button1_text = "加为好友",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guid, name, level)
    self.guid = guid
    self.name = name
    self.text1 = string.format(string.format("【%s(%d级)】将鲜花赠送你,你可以找他聊聊哦", name, level))
    return self
  end,
  OnAccept = function(self)
    AddRelation(self.guid)
  end
}
MessageboxTypes.COMMUNITY_ASK_ADD_FRIEND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guid, name, level)
    self.guid = guid
    self.name = name
    self.text1 = string.format(string.format("是否向【%s(%d级)】发出好友请求?", name, level))
    return self
  end,
  OnAccept = function(self)
    ApplyRelationByName(self.name)
  end
}
MessageboxTypes.STALL_ESTABLISH = {
  title = "摆摊",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, stall_tax, transaction_tax, stall_type)
    if stall_type == 1 then
      self.text1 = "确定开始摆摊?"
      self.text2 = string.format("(当前交易税为%d%%)", transaction_tax)
    elseif stall_type == 2 then
      self.text1 = string.format("离线摆摊免收摊位费,交易税为%d%%开始摆摊后，确定要摆摊么？注意:标注有元宝字样的物品只能在襄阳广场元宝摆摊出售", transaction_tax)
    end
    self.stall_type = stall_type
    return self
  end,
  OnAccept = function(self)
    EstablishStall()
  end,
  OnCancel = function(self)
    CancleStall()
  end
}
MessageboxTypes.STALL_BUY_ITEM = {
  title = "购买确认框",
  text = "",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, strText, buyItemIndex, buyItemNum)
    self.text = strText
    self.index = buyItemIndex
    self.num = buyItemNum
    return self
  end,
  OnAccept = function(self)
    BuyStallItem(self.index, self.num)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.STALL_BIG_ONEH = {
  title = "购买确认框",
  text = "购买将花费超过100金",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, strText, buyItemIndex, buyItemNum)
    self.index = buyItemIndex
    self.num = buyItemNum
    return self
  end,
  OnAccept = function(self)
    UI_MessageConfimBiger()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.STALL_VALUABLE_ITEM = {
  title = "摆摊",
  text = "此物为贵重物品，是否真的要上架",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, bag, index)
    self.bag = bag
    self.index = index
    return self
  end,
  OnAccept = function(self)
    Stall_AddItem(self.bag, self.index)
  end
}
MessageboxTypes.TRADE_CONFIRM = {
  title = "交易确认",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, sendType, price, num, isChangeToAuction)
    self.sendType = sendType
    self.price = price
    self.num = num
    self.isChangeToAuction = isChangeToAuction
    local strUnitMoney = Lua_UI_Money2String(price)
    local strmoney = Lua_UI_Money2String(price * num)
    if sendType == 0 then
      self.text1 = string.format("您确定以单价%s购买%d福利点吗?", strUnitMoney, num * gUI_TRADE_DEFAULT_NUMTIME) .. string.format("需支付总金额%s", strmoney)
    elseif sendType == 1 then
      self.text1 = string.format("您确定以单价%s出售%d钻石吗?", strUnitMoney, num * gUI_TRADE_DEFAULT_NUMTIME) .. string.format("可获得总金额%s", strmoney)
    end
    return self
  end,
  OnAccept = function(self)
    TradeAuction(self.sendType, self.price, self.num, self.isChangeToAuction)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.TRADE_FEEDBACK = {
  title = "交易反馈",
  has_button1 = true,
  button1_text = "确定",
  modal_state = true,
  Create = function(self, sendType, price, remNum, tradeNum)
    self.sendType = sendType
    self.price = price
    self.num = remNum
    local strmoney = Lua_UI_Money2String(price)
    if sendType == 0 then
      self.text1 = string.format("您以单价%s成功出售了%d钻石,", strmoney, tradeNum * gUI_TRADE_DEFAULT_NUMTIME) .. string.format("还剩%d钻石未出售", remNum * gUI_TRADE_DEFAULT_NUMTIME)
    elseif sendType == 1 then
      self.text1 = string.format("您以单价%s成功购买了%d钻石,", strmoney, tradeNum * gUI_TRADE_DEFAULT_NUMTIME) .. string.format("还剩%d钻石未购买", remNum * gUI_TRADE_DEFAULT_NUMTIME)
    end
    return self
  end,
  OnAccept = function(self)
  end,
  OnCenter = function(self)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.TRADE_AUCTION_CONFIRM = {
  title = "挂单确认",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, sendType, price, num)
    self.sendType = sendType
    self.price = price
    self.num = num
    local strmoney = Lua_UI_Money2String(price)
    if sendType == 0 then
      self.text1 = string.format("您确定以单价%s挂单出售%d钻石吗?", strmoney, num * gUI_TRADE_DEFAULT_NUMTIME)
    elseif sendType == 1 then
      self.text1 = string.format("您确定以单价%s挂单购买%d钻石吗?", strmoney, num * gUI_TRADE_DEFAULT_NUMTIME)
    end
    return self
  end,
  OnAccept = function(self)
    AddAuction(self.sendType, self.price, self.num)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.TRADE_AUCTION_QUASH = {
  title = "撤消挂单",
  text1 = "您确定撤消当前挂单吗?",
  text2 = "(退还的钻石将直接返还至帐号上)",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, index)
    self.Index = index
    return self
  end,
  OnAccept = function(self)
    RecallMyAuction(self.Index)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.RMBSHOP_GIFT_CONFIRM = {
  title = "赠送",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, SelType, itemID64, itemID, BuyNum, recvName, itemName)
    self.SelType = SelType
    self.itemID64 = itemID64
    self.itemID = itemID
    self.BuyNum = BuyNum
    self.recvName = recvName
    self.text1 = string.format("是否确认要将[%s]赠送给[%s]?", itemName, recvName)
    return self
  end,
  OnAccept = function(self)
    GiftPageItem(self.SelType, self.itemID64, self.itemID, self.BuyNum, self.recvName)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CIMFIYEMBUYNEW = {
  title = "赠送",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, SelType, itemID64, itemID, BuyNum, itemName)
    self.SelType = SelType
    self.itemID64 = itemID64
    self.itemID = itemID
    self.BuyNum = BuyNum
    self.text1 = string.format("是否确认要购买[%s]", itemName)
    return self
  end,
  OnAccept = function(self)
    BuyPageItem(self.SelType, self.itemID64, self.itemID, self.BuyNum)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CIMFIMBUY = {
  title = "购买",
  text = "",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, count, typename, SumPrice, Type)
    self.count = count
    self.typename = typename
    self.SumPrice = SumPrice
    self.Type = Type
    self.text1 = string.format("是否花费%d【%s】,  购买%d种商品", SumPrice, typename, count)
    return self
  end,
  OnAccept = function(self)
    SetRMBItemChoose(self.Type, 0, 10, 0)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.DEL_MAIL = {
  title = "删除邮件",
  text = "此邮件含有附件，删除后无法再收取附件，确定删除邮件？",
  has_button1 = true,
  has_button2 = true,
  OnAccept = function(self)
    Lua_Email_DelMail()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.INVITE_MULTIMOUNT = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, name, objId)
    self.text1 = string.format("%s邀请你一起骑乘", name)
    self.text2 = "是否接受邀请?"
    self.text3 = "倒计时: {#R^&30&}"
    self.objId = objId
    return self
  end,
  OnAccept = function(self)
    ConfirmMountInvite(true, self.objId)
  end,
  OnCancel = function(self)
    ConfirmMountInvite(false, self.objId)
  end,
  OnTimeOut = function(self)
    ConfirmMountInvite(false, self.objId)
  end
}
MessageboxTypes.CONFIRM_USE_BIND = {
  title = "",
  text1 = "装备后会与您绑定",
  text2 = "是否确认装备？",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, bag, slot)
    self.bag = bag
    self.slot = slot
    self.Guid = GetItemGUIDByPos(bag, slot)
    return self
  end,
  OnAccept = function(self)
    local bag, slot = GetItemPosByGuid(self.Guid)
    if bag == gUI_GOODSBOX_BAG.backpack0 then
      UseItem(bag, slot)
    end
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_USE_BIND1 = {
  title = "",
  text1 = "使用后会与您绑定",
  text2 = "是否确认使用？",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, bag, slot, tobag, toslot)
    self.bag = bag
    self.slot = slot
    self.tobag = tobag
    self.toslot = toslot
    self.Guid = GetItemGUIDByPos(bag, slot)
    return self
  end,
  OnAccept = function(self)
    local bag, slot = GetItemPosByGuid(self.Guid)
    if bag == gUI_GOODSBOX_BAG.backpack0 then
      MoveItem(bag, slot, self.tobag, self.toslot)
    end
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_BIND_AUTH = {
  title = "",
  text1 = "使用绑定鉴定符会导致装备变为绑定状态",
  text2 = "是否确认鉴定？",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    return self
  end,
  OnAccept = function(self)
    DoAuthAction()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_BIND_INEFF = {
  title = "",
  text1 = "附魔后灵符不能取出",
  text2 = "是否确认附魔？",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    return self
  end,
  OnAccept = function(self)
    DoIneffctive()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_BINDITEM_INEFF = {
  title = "",
  text1 = "附魔过程有绑定道具参与，装备会被绑定",
  text2 = "是否确认附魔？",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    return self
  end,
  OnAccept = function(self)
    DoIneffctiveAction()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_JEWELBLEND = {
  title = "",
  text1 = "有绑定道具参与，合出来的神玉是绑定的",
  text2 = "是否确认",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, maxNumber, ID)
    self.maxNumber = maxNumber
    self.ID = ID
    return self
  end,
  OnAccept = function(self)
    Manufacture_StartMake(self.ID, tonumber(self.maxNumber))
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.GEM_INEFF_CHOOSE = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, Type, operateType, Guid)
    self.text1 = gUI_GEM_INEFF_DES[Type]
    self.operateType = operateType
    self.Guid = Guid
    return self
  end,
  OnAccept = function(self)
    local bagMain, SlotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
    SendGemAttrType(GEMATTR_USEORG)
    if self.operateType == OPRATER_TYPE_FORGEM.CLOSEPANEL then
      GemIneff_OnClosePanel()
      GemIneff_CancelOperate()
    elseif self.operateType == OPRATER_TYPE_FORGEM.PLACENEWITEM then
      local bag, slot = GetItemPosByGuid(self.Guid)
      GemIneff_AddNewItemOperate(bag, slot, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
    elseif self.operateType == OPRATER_TYPE_FORGEM.CANELITEM then
      GemIneff_CancelOperate()
    elseif self.operateType == OPRATER_TYPE_FORGEM.PLACENEWSUBITEM then
      local bag, slot = GetItemPosByGuid(self.Guid)
      GemIneff_AddNewItemOperate(bagMain, SlotMain, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
      GemIneff_AddNewItemOperate(bag, slot, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
    end
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_REPLACE_INEFF = {
  title = "",
  text1 = "装备已有灵符，",
  text2 = "是否替换",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    return self
  end,
  OnAccept = function(self)
    DoIneffctive()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_BIND_GEM_EXTRACT = {
  title = "",
  text1 = "是否确定摘除神玉?",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, flag)
    self.flag = flag
    return self
  end,
  OnAccept = function(self)
    SplitGem(self.flag)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_BIND_INHERIT = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self)
    self.text1 = "传承将把副装备的传承状态转移给主装备，是否继续?"
    return self
  end,
  OnAccept = function(self)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_BIND_GEM_UPDATA = {
  title = "",
  text1 = "使用绑定的神玉会使主神玉自动绑定，是否继续使用?",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    return self
  end,
  OnAccept = function(self)
    DoUpradeItem()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_USE_HORSE = {
  title = "",
  text = "坐骑使用后将放入坐骑背包并且被绑定，坐骑背包中的坐骑可右键使用，是否确定放入?",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, bag, slot)
    self.bag = bag
    self.slot = slot
    return self
  end,
  OnAccept = function(self)
    UseItem(self.bag, self.slot)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_CREATE_GUILD = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, mode)
    self.mode = mode
    self.text1 = "是否确定正式成立帮会？"
    self.text2 = ""
    self.text3 = string.format("需{#G^100}{ImAgE^UiIamge004:Image_gold}和{#G^【建帮令】}")
    return self
  end,
  OnAccept = function(self)
    CreateGuildOrganization(self.mode)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CREATE_GUILD = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, createType, name, board)
    self.createType = createType
    if createType then
      self.text1 = string.format("您确认使用一个{#G^【建帮令】}创建帮会?")
    else
      self.text1 = string.format("您确认使用" .. Lua_UI_Money2String(1000000) .. "创建帮会?")
    end
    self.name = name
    self.board = board
    return self
  end,
  OnAccept = function(self)
    if self.createType then
      DirectCreateGuild(self.createType, self.name, self.board)
    else
      local _, money, bindMoney = GetBankAndBagMoney()
      if money + bindMoney < 1000000 then
        Lua_Chat_ShowOSD("GUILD_CRATE_GOLD")
      elseif bindMoney < 1000000 then
        Messagebox_Show("CREATE_GUILD_BUYITEM_CONFIRM", self.createType, self.name, self.board)
      else
        DirectCreateGuild(self.createType, self.name, self.board)
      end
    end
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CREATE_GUILD_BUYITEM_CONFIRM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, createType, name, board)
    self.createType = createType
    self.name = name
    self.board = board
    self.text1 = "您的绑金不足，扣除绑金的剩余部分将由金币支付"
    return self
  end,
  OnAccept = function(self)
    DirectCreateGuild(self.createType, self.name, self.board)
  end
}
MessageboxTypes.CONFIRM_INHERIT = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, color1, equipName1, color2, equipName2)
    self.text1 = string.format("是否确定将{#C^%s^%s}的锻造等级传承至{#C^%s^%s}上？", gUI_GOODS_QUALITY_COLOR_PREFIX[color1], equipName1, gUI_GOODS_QUALITY_COLOR_PREFIX[color2], equipName2)
    return self
  end,
  OnAccept = function(self)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_INHERIT_RARYITY = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self)
    self.text1 = "原始装备是贵重物品"
    self.text2 = "是否确认融魂？"
    return self
  end,
  OnAccept = function(self)
    DoInherit()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.CONFIRM_USE_ITEM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, bag, slot, str)
    self.text1 = str
    self.bag = bag
    self.slot = slot
    self.Guid = GetItemGUIDByPos(bag, slot)
    return self
  end,
  OnAccept = function(self)
    local bag, slot = GetItemPosByGuid(self.Guid)
    if bag == gUI_GOODSBOX_BAG.backpack0 then
      UseItem(bag, slot)
    end
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.TEAM_ASSIGN_LOOT = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, box_id, item_id, target_guid, target_name, item_name)
    self.text1 = string.format("您是否确定将【%s】", item_name)
    self.text2 = string.format("分配给【%s】", target_name)
    self.box_id = box_id
    self.item_id = item_id
    self.target_guid = target_guid
    return self
  end,
  OnAccept = function(self)
    Lua_Pick_ThrowItemAssignLootResult(self.box_id, self.item_id, self.target_guid)
  end
}
MessageboxTypes.INSTANCE_TEAM_RECIEVE_WAIT = {
  title = "已找到匹配队伍",
  has_button1 = true,
  has_button2 = true,
  mapID = -1,
  mapType = -1,
  mapVaule = -1,
  Create = function(self, mapID, mapType, mapVaule)
    self.text = "是否开始队伍竞技副本"
    self.mapID = mapID
    self.mapType = mapType
    self.mapVaule = mapVaule
    return self
  end,
  OnAccept = function(self)
    ReturnMulitTeamInvite(self.mapID, self.mapType, self.mapVaule, true)
  end,
  OnCancel = function(self)
    ReturnMulitTeamInvite(self.mapID, self.mapType, self.mapVaule, false)
  end
}
MessageboxTypes.SEND_MAIL = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, strPlayerName, strItemName, strMoney)
    self.text1 = string.format("您确认将邮件%s %s", strItemName, strMoney)
    self.text2 = ""
    self.text3 = string.format("邮寄给{#G^[%s]}吗？", strPlayerName)
    return self
  end,
  OnAccept = function(self)
    Lua_EmailWrite_ConfirmSend()
  end
}
MessageboxTypes.DELETE_MAIL = {
  title = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, IsHasBox)
    if IsHasBox then
      self.text1 = "有未查看的邮件或未提取的附件"
      self.text2 = "是否确定需要删除？"
    else
      self.text1 = ""
      self.text2 = "是否确定需要删除？"
    end
    return self
  end,
  OnAccept = function(self)
    Lua_Email_DelMail()
  end
}
MessageboxTypes.AUCTION_OTHER_SHOP_BUY_ITEM = {
  title = "寄售购买",
  has_button1 = true,
  has_button2 = true,
  shopIndex = -1,
  slot = -1,
  Create = function(self, shopIndex, slot)
    self.text = "确定购买?"
    self.shopIndex = shopIndex
    self.slot = slot
    return self
  end,
  OnAccept = function(self)
    BuyOtherAuctionShopGoods(self.shopIndex, self.slot)
  end
}
MessageboxTypes.SHOP_RARYITY_GOODS_TO_SELL = {
  title = "NPC商店",
  text1 = "",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, slot, bagPos, itemName, quality, itemCount)
    if itemCount > 1 then
      self.text1 = string.format("确定要出售{#C^%s^【%s】*%d}吗?", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], itemName, itemCount)
    else
      self.text1 = string.format("确定要出售{#C^%s^【%s】}吗?", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], itemName)
    end
    self.Slot = slot
    self.BagPos = bagPos
    return self
  end,
  OnAccept = function(self)
    SellItem(self.Slot, self.BagPos)
  end
}
MessageboxTypes.BLEND_ASK_EQUIP = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, slot, Guid)
    self.text1 = "放置装备是已装备"
    self.text2 = "是否确认放置?"
    self.Slot = slot
    self.Guid = Guid
    return self
  end,
  OnAccept = function(self)
    Blend_AddEquipToSub(self.Slot, self.Guid)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.FORGE_ASK_CONFIRM_BIND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "使用绑定材料参与锻造将使"
    self.text2 = "装备绑定,是否确定进行锻造？"
    return self
  end,
  OnAccept = function(self)
    Lua_Forge_AskMoneyBind()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.GEMSENBLEND_ASK_CONFIRM_BIND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "使用绑定材料参与将使"
    self.text2 = "神玉绑定,是否确定进行合成？"
    return self
  end,
  OnAccept = function(self)
    GemSenItem_SubHasIneff()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.GEMSENBLEND_ASK_CONFIRM_SUBINEFF = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "你的副神玉有注灵属性"
    self.text2 = "是否确定进行合成？"
    return self
  end,
  OnAccept = function(self)
    DoUpradeItem(gUI.GemSeniorBlend.BindFirst:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.GEMINEFF_ASK_CONFIRM_BIND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "使用绑定材料参与将使"
    self.text2 = "神玉绑定,是否确定进行注灵？"
    return self
  end,
  OnAccept = function(self)
    GemIneff_AskGemIneff()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.GEMINEFF_ASK_SUBLIST = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "你的副神玉有注灵属性"
    self.text2 = "是否确定进行注灵？"
    return self
  end,
  OnAccept = function(self)
    GemBlendDoAction(false)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.FORGE_ASK_CONFIRM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, rate)
    self.text1 = "锻造成功率只有" .. rate .. "%"
    self.text2 = "是否确认继续?"
    return self
  end,
  OnAccept = function(self)
    Lua_Forge_AskBind()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.FORGE_ASK_CONFIRMMATMONEY = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "主装备已经绑定,您选择了使用"
    self.text2 = "非绑定材料或者金币,"
    self.text3 = "是否确认继续?"
    return self
  end,
  OnAccept = function(self)
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.FORGE_ASK_CONFIRMMATMONEY2 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "主装备已经绑定,您的绑定材料不足"
    self.text2 = "不足部分用非绑代替,是否确认继续?"
    return self
  end,
  OnAccept = function(self)
    Lua_Forge_ComfimMoney()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.FORGE_ASK_CONFIRMMATMONEY3 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "主装备已经绑定,您的绑金不足"
    self.text2 = "不足部分用金币代替,是否确认继续?"
    return self
  end,
  OnAccept = function(self)
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.FORGE_ASK_CONFIRMMATMONEY4 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "你使用了绑定的道具，主装备将"
    self.text2 = "绑定，你选择使用金币锻造，是否确认继续？"
    return self
  end,
  OnAccept = function(self)
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.FORGE_ASK_CONFIRMMATMONEY5 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "主装备即将绑定,您的绑金不足"
    self.text2 = "不足部分用金币代替,是否确认继续?"
    return self
  end,
  OnAccept = function(self)
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.FORGE_ASK_CONFIRMMATMONEY6 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "使用绑金进行操作将使装备绑定，"
    self.text2 = "是否确认继续?"
    return self
  end,
  OnAccept = function(self)
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.MOUNTFORGE_ASK_CONFIRM_BIND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "使用绑定材料进行操作将使坐骑"
    self.text2 = "绑定，是否确定进行进化？"
    return self
  end,
  OnAccept = function(self)
    DoMountForge(gUI.MountForge.MoneyFrist:IsChecked(), gUI.MountForge.MaterialFrist:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.MOUNTFORGE_ASK_CONFIRM_MONEY = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "使用绑金进行操作将使坐骑绑定，"
    self.text2 = "是否确定进行进化？"
    return self
  end,
  OnAccept = function(self)
    DoMountForge(gUI.MountForge.MoneyFrist:IsChecked(), gUI.MountForge.MaterialFrist:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.MOUNTFORGE_ASK_CONFIRM_MONEY1 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "坐骑即将或者已经绑定，你确定使用金币"
    self.text2 = "参与操作，进化成功后,坐骑将与你绑定"
    self.text3 = "是否确定进行进化？"
    return self
  end,
  OnAccept = function(self)
    DoMountForge(gUI.MountForge.MoneyFrist:IsChecked(), gUI.MountForge.MaterialFrist:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.MOUNTFORGE_ASK_CONFIRM_MONEY2 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "坐骑即将或者已经绑定，你的绑金不足，"
    self.text2 = "不足部分将用金币代替，进化成功后,"
    self.text3 = "坐骑将与你绑定，是否确定进行进化？"
    return self
  end,
  OnAccept = function(self)
    DoMountForge(gUI.MountForge.MoneyFrist:IsChecked(), gUI.MountForge.MaterialFrist:IsChecked())
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.MOUNTFORGE_ASK_CONFIRM_MATER = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "使用绑定材料进行操作将使坐骑"
    self.text2 = "是否确定进行进化？"
    return self
  end,
  OnAccept = function(self)
    Lua_MountForge_Money1()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.MOUNTFORGE_ASK_CONFIRM_MATER1 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "坐骑即将或者已经绑定，你确定使用"
    self.text2 = "非绑道具，是否确定进行进化？"
    return self
  end,
  OnAccept = function(self)
    Lua_MountForge_Money1()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.MOUNTFORGE_ASK_CONFIRM_MATER2 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "坐骑即将或者已经绑定，你的绑定道具"
    self.text2 = "不足，不足部分将用非绑代替，"
    self.text3 = "是否确定进行进化？"
    return self
  end,
  OnAccept = function(self)
    Lua_MountForge_Money1()
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.GEM_ASK_CONFIRM_BIND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, num)
    self.text1 = "镶嵌过程中有绑金或绑定道具参与，镶嵌成功后将使你的装备变为绑定状态，是否继续？"
    self.pos = num
    return self
  end,
  OnAccept = function(self)
    DoGemBeset(self.num)
  end,
  OnCancel = function(self)
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_BIND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "由于副装备是绑定的,"
    self.text2 = "本次操作将会使主装备绑定，是否继续？"
    return self
  end,
  OnAccept = function(self)
    Lua_Blend_ComfirmBindMoneyAndMater()
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_BINDMATER1 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "绑定道具不足,不足部分将用非绑代替,"
    self.text2 = "将会使主装备绑定，是否继续？"
    return self
  end,
  OnAccept = function(self)
    Lua_Blend_ComfirmBindMoney()
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_BINDMATER2 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "使用绑定材料进行操作将使主装备绑定,"
    self.text2 = "是否确定继续？"
    return self
  end,
  OnAccept = function(self)
    Lua_Blend_ComfirmBindMoney()
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_MONEY1 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "主装备即将或者已经绑定，如果选中优先消耗金币，"
    self.text2 = "会扣除金币，是否确定继续？"
    return self
  end,
  OnAccept = function(self)
    BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_MONEY2 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "主装备即将或者已经绑定，你的绑金不足"
    self.text2 = "不足部分会扣除金币，是否确定继续？"
    return self
  end,
  OnAccept = function(self)
    BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_MONEY3 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "使用绑金进行操作将使主装备绑定"
    self.text2 = "是否确定继续？"
    return self
  end,
  OnAccept = function(self)
    BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_MONEYANDMATER = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "主装备即将或者已经绑定，您选择了优先"
    self.text2 = "消耗金币或者选择了优先非绑材料,"
    self.text3 = "是否继续？"
    return self
  end,
  OnAccept = function(self)
    BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_MONEYANDMATER1 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "主装备即将或者已经绑定，绑定材料不足"
    self.text2 = "不足部分会用非绑代替,是否继续？"
    return self
  end,
  OnAccept = function(self)
    Lua_Blend_ComfirmBindMater3()
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_MONEYANDMATER2 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "主装备即将或者已经绑定，绑金不足"
    self.text2 = "不足部分会用金币代替,是否继续？"
    return self
  end,
  OnAccept = function(self)
    BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_RARE = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "副装备是贵重物品"
    self.text2 = "是否确认融魂？"
    return self
  end,
  OnAccept = function(self)
    Lua_Blend_ComfirmForge()
  end
}
MessageboxTypes.INHERIT_ASK_CONFIRM_BIND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "传承后会使主装备和副装备绑定，副装备的历史最高锻造等级和当前锻造等级归0,是否确认传承？"
    return self
  end,
  OnAccept = function(self)
    DoInherit()
  end
}
MessageboxTypes.INHERIT_ASK_CONFIRM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "传承后会使主装备和副装备绑定，副装备的历史最高锻造等级和当前锻造等级归0,是否确认传承？"
    return self
  end,
  OnAccept = function(self)
    DoInherit()
  end
}
MessageboxTypes.INHERIT_ASK_CONFIRM_BINDINFO1 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "传承后主装备和副装备转为绑定状态，"
    self.text2 = "副装备的历史最高锻造等级和当前"
    self.text3 = "锻造等级归0，是否确认传承？"
    return self
  end,
  OnAccept = function(self)
    DoInherit()
  end
}
MessageboxTypes.INHERIT_ASK_CONFIRM_BINDINFO2 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "传承后副装备转为绑定状态,"
    self.text2 = "副装备的历史最高锻造等级和当前"
    self.text3 = "锻造等级归0,是否确认传承？"
    return self
  end,
  OnAccept = function(self)
    DoInherit()
  end
}
MessageboxTypes.INHERIT_ASK_CONFIRM_BINDINFO3 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "传承后主装备转为绑定状态，"
    self.text2 = "副装备的历史最高锻造等级和当前"
    self.text3 = "锻造等级归0，是否确认传承？"
    return self
  end,
  OnAccept = function(self)
    DoInherit()
  end
}
MessageboxTypes.INHERIT_ASK_CONFIRM_BINDINFO4 = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "传承后副装备的历史最高锻造等级和当前"
    self.text2 = "锻造等级归0，是否确认传承？"
    return self
  end,
  OnAccept = function(self)
    DoInherit()
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_GEM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "副装备上已镶嵌神玉"
    self.text2 = "是否确认融魂？"
    return self
  end,
  OnAccept = function(self)
    Lua_Blend_ComfirmBind()
  end
}
MessageboxTypes.BLEND_ASK_CONFIRM_FORGE = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "副装备已锻造过(如果副装备历史最高锻造"
    self.text2 = "等级大于主装备建议使用传承功能)"
    self.text3 = "是否确认融魂？"
    return self
  end,
  OnAccept = function(self)
    Lua_Blend_ComfirmGem()
  end
}
MessageboxTypes.GUILD_RECRUIT_INVITATION = {
  title = "帮会招募邀请",
  text = "",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, pop_name, guid, gid, guild_name, time)
    self.text1 = string.format("【%s】邀请你加入帮会【%s】", pop_name, guild_name)
    self.text3 = string.format("是否同意加入？")
    self.invitation_guid = guid
    self.invitation_gid = gid
    self.invitation_time = time
    return self
  end,
  OnAccept = function(self)
    ReplyRecruitment(self.invitation_gid, self.invitation_guid, true)
  end,
  OnCancel = function(self)
    ReplyRecruitment(self.invitation_gid, self.invitation_guid, false)
  end,
  OnTimeOut = function(self)
    ReplyRecruitment(self.invitation_gid, self.invitation_guid, false)
  end
}
MessageboxTypes.GUILDLEADER_ABDICATE_TO_SOMEONE = {
  title = "帮主变更",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guid, name)
    self.text1 = string.format("你确认要将帮主转让给【%s】吗？", name)
    self.candicate_guid = guid
    return self
  end,
  OnAccept = function(self)
    ModifyGuildMemberPosition(self.candicate_guid, gUI_GUILD_POSITION.leader - 1, -1)
  end
}
MessageboxTypes.GUILD_DISMISS = {
  title = "解散帮会",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guild_name, delay_time, iscancel)
    if iscancel then
      self.text1 = string.format("你确定要取消解散帮会【%s】？", guild_name)
    else
      self.text1 = string.format("你确定要解散帮会【%s】？", guild_name)
    end
    self.iscancel = iscancel
    return self
  end,
  OnAccept = function(self)
    DismissGuildOrganization(self.iscancel)
  end
}
MessageboxTypes.GUILD_KICK = {
  title = "开除帮会成员",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, member_guid, member_name)
    self.text1 = string.format("您确定要开除【%s】吗？", member_name)
    self.member_guid = member_guid
    return self
  end,
  OnAccept = function(self)
    KickGuildMember(self.member_guid)
  end
}
MessageboxTypes.GUILD_QUIT = {
  title = "退出帮会",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guild_name)
    self.text1 = string.format("是否确定要离开帮会【%s】？", guild_name)
    return self
  end,
  OnAccept = function(self)
    QuitGuildOrganization(false)
  end
}
MessageboxTypes.GUILD_QUIT_LEADER = {
  title = "退出帮会",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guild_name)
    self.text1 = string.format("是否确定要离开帮会【%s】？", guild_name)
    self.text2 = string.format("离开后帮会将解散")
    return self
  end,
  OnAccept = function(self)
    DismissGuildOrganization(false)
    UI_GuildM_CloseMain()
  end
}
MessageboxTypes.GUILD_ALLICANCE_INVITE = {
  title = "帮会联盟邀请",
  guildid = -1,
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guild_name, guild_id)
    guildid = guild_id
    self.text1 = string.format("【%s】帮会邀请你的帮会加入他们的联盟", guild_name)
    self.text2 = ""
    self.text3 = string.format("是否同意对方的邀请？")
    return self
  end,
  OnAccept = function(self)
    RequestGuildAllianceInvite(guildid, true)
  end,
  OnCancel = function(self)
    RequestGuildAllianceInvite(guildid, false)
  end
}
MessageboxTypes.GUILD_ALLICANCE_QUIT = {
  title = "退出帮会联盟",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = string.format("是否确认退出帮会联盟？")
    return self
  end,
  OnAccept = function(self)
    GuildAllianceQuit()
  end
}
MessageboxTypes.GUILD_ALLICANCE_REMOVE = {
  title = "删除联盟成员",
  member_gid = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, memberName, memberGID)
    self.text1 = string.format("是否确定要将【%s】帮会踢出联盟？", memberName, memberGID)
    member_gid = memberGID
    return self
  end,
  OnAccept = function(self)
    RemoveGuildAlliance(member_gid)
  end
}
MessageboxTypes.GUILD_HOSTILE_REMOVE = {
  title = "删除敌对帮会",
  member_gid = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, memberName, memberGID)
    self.text1 = string.format("是否确定删除敌对帮会【%s】？", memberName, memberGID)
    member_gid = memberGID
    return self
  end,
  OnAccept = function(self)
    RemoveGuildHostile(member_gid)
  end
}
MessageboxTypes.GUILD_ALLICANCE_CREATE = {
  title = "创建帮会联盟",
  npc_id = -1,
  has_button1 = true,
  has_button2 = true,
  Create = function(self, npcID)
    npc_id = npcID
    self.text1 = string.format("是否确认创建帮会联盟？")
    self.text2 = string.format("创建联盟需要扣除100金币")
    return self
  end,
  OnAccept = function(self)
    CreateGuildAlliance(npc_id)
  end
}
MessageboxTypes.GUILD_USE_INVITE_ITEM = {
  title = "",
  text1 = "目标退出帮会不足24小时",
  text2 = "是否使用帮会招贤令？",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guid)
    self.guid = guid
    return self
  end,
  OnAccept = function(self)
    UseInviteItemPass(self.guid)
  end,
  OnCancel = function(self)
    Lua_Chat_ShowOSD("GUILD_INVITE_ITEM_CANCEAL")
  end
}
MessageboxTypes.GUILD_SHOW_SHOPRESET = {
  title = "重置帮会商店数量",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, id)
    self.text1 = "是否确定重置帮会商店?\n使用后所有的道具数量都将重置"
    self.id = id
    return self
  end,
  OnAccept = function(self)
    GuildShopReset(self.id)
  end
}
MessageboxTypes.GUILD_DECLARE_WAR = {
  title = "发起宣战",
  guild_Name = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guildName)
    self.text1 = string.format("是否确认要对[%s]发起宣战？", guildName)
    guild_Name = guildName
    return self
  end,
  OnAccept = function(self)
    DeclareGuildWar(guild_Name)
  end
}
MessageboxTypes.GUILD_SIEGE_APPLY_ATTACK = {
  title = "进攻领地",
  city_id = -1,
  has_button1 = true,
  has_button2 = true,
  Create = function(self, cityName, cityId)
    self.text1 = string.format("确认要报名进攻[%s]吗？", cityName)
    self.text2 = "需要消耗10金绑金"
    city_id = cityId
    return self
  end,
  OnAccept = function(self)
    ApplySiege(city_id, false)
  end
}
MessageboxTypes.GUILD_SIEGE_CANCEL_ATTACK = {
  title = "取消进攻领地",
  city_id = -1,
  has_button1 = true,
  has_button2 = true,
  Create = function(self, cityName, cityId)
    self.text1 = string.format("确认要取消进攻[%s]吗？", cityName)
    self.text2 = "(取消报名后10分钟后才能"
    self.text3 = "再次报名攻城或守城)"
    city_id = cityId
    return self
  end,
  OnAccept = function(self)
    CancelApplySiege(city_id, false)
  end
}
MessageboxTypes.GUILD_SIEGE_APPLY_DEFENCE = {
  title = "防守领地",
  city_id = -1,
  has_button1 = true,
  has_button2 = true,
  Create = function(self, cityName, cityId)
    self.text1 = string.format("确认要报名防守[%s]吗？", cityName)
    city_id = cityId
    return self
  end,
  OnAccept = function(self)
    ApplySiege(city_id, true)
  end
}
MessageboxTypes.GUILD_SIEGE_CANCEL_DEFENCE = {
  title = "取消防守领地",
  city_id = -1,
  has_button1 = true,
  has_button2 = true,
  Create = function(self, cityName, cityId)
    self.text1 = string.format("确认要取消防守[%s]吗？", cityName)
    self.text2 = "(取消报名后10分钟后才能"
    self.text3 = "再次报名攻城或守城)"
    city_id = cityId
    return self
  end,
  OnAccept = function(self)
    CancelApplySiege(city_id, true)
  end
}
MessageboxTypes.GUILD_SIEGE_DELSHARE = {
  title = "删除分享帮会",
  city_id = -1,
  has_button1 = true,
  has_button2 = true,
  Create = function(self, guildid, slot)
    self.text1 = "确认要删除分享吗？"
    self.guildid = guildid
    self.slot = slot
    return self
  end,
  OnAccept = function(self)
    DelSiegeShareGuild(self.guildid)
    RequestSiegeCityInfo(self.slot)
  end
}
MessageboxTypes.GROUP_CHAT_QUIT = {
  title = "群聊",
  text1 = "是否确定要退出群聊",
  has_button1 = true,
  has_button2 = true,
  OnAccept = function(self)
    Lua_ChatC_InitDisplay()
    Lua_ChatC_CloseMainChatWnd()
    Lua_ChatC_BlinkChatCBtn(false)
    ExitChatGroup()
  end
}
MessageboxTypes.CARNIVAL_BOX = {
  title = "领取宝箱",
  text = "领取宝箱",
  button1_text = "领取",
  button2_text = "取消 ",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, Item, FormulaID, BoxId)
    self.text = string.format("是否确定需要兑换[{#G^%s}]", Item)
    self.formulaID = FormulaID
    self.BoxId = BoxId
    return self
  end,
  OnAccept = function(self)
    ExchangeRecommend(self.formulaID, self.BoxId)
  end
}
MessageboxTypes.ASK_GUARDQUEST = {
  title = "护送任务",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  disableESC = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, AskerName, QustTitle, NpcId, QustId, PlayerId)
    self.text1 = string.format("{#G^%s}邀请您一起进行护送任务{#G^%s}，是否接受？", AskerName, QustTitle)
    self.text2 = ""
    self.text3 = string.format("倒计时:{#R^&10&}")
    self.NpcId = NpcId
    self.QustId = QustId
    self.PlayerId = PlayerId
    return self
  end,
  OnAccept = function(self)
    AskShareGuardQuest(true, self.NpcId, self.QustId, self.PlayerId)
  end,
  OnCancel = function(self)
    AskShareGuardQuest(false, self.NpcId, self.QustId, self.PlayerId)
  end,
  OnTimeOut = function(self)
    AskShareGuardQuest(false, self.NpcId, self.QustId, self.PlayerId)
  end
}
MessageboxTypes.ASK_SHAREQUEST = {
  title = "共享任务",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  disableESC = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, strAskName, strQuestTitle, nPlayerID, nQuestID)
    self.text1 = string.format("是否接受%s共享的任务%s？", strAskName, strQuestTitle)
    self.nQuestID = nQuestID
    self.nPlayerID = nPlayerID
    return self
  end,
  OnAccept = function(self)
    AcceptShareQuest(self.nQuestID)
    ShareQuestResult(true, self.nQuestID, self.nPlayerID)
  end,
  OnCancel = function(self)
    ShareQuestResult(false, self.nQuestID, self.nPlayerID)
  end
}
MessageboxTypes.REMOVE_PET = {
  title = "",
  text1 = "是否确定将宠物放生,放生后不可找回?",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, index, name, level)
    self.text1 = "是否确定将宠物 {#G^" .. name .. " Lv." .. level .. "} 放生?"
    self.text2 = "放生后将不可找回!"
    self.index = index
    return self
  end,
  OnAccept = function(self)
    PetFree(self.index)
  end
}
MessageboxTypes.REPLACE_PET_SKILL = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, oldSkillName, newSkillName, bag, from_off, newSlot)
    self.text1 = "已学习过相同类型技能"
    self.text2 = "是否将{#G^【" .. oldSkillName .. "】}替换为{#G^【" .. newSkillName .. "】}"
    self.bag = bag
    self.from_off = from_off
    self.newSlot = newSlot
    return self
  end,
  OnAccept = function(self)
    UseItemPetSkill(self.bag, self.from_off, self.newSlot)
  end
}
MessageboxTypes.REPLACE_PET_FEED = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, playerName)
    self.text1 = "【" .. playerName .. "】喂养了你的宠物"
    return self
  end
}
MessageboxTypes.CANCEL_PUBLISH = {
  title = "",
  text = "真的要取消委托任务么?",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, id)
    self.id = id
    return self
  end,
  OnAccept = function(self)
    QuoteQuestCommission(self.id, 3)
  end
}
MessageboxTypes.SUMMON_REQUEST = {
  title = "召唤请求",
  text = "",
  button1_text = "同意",
  button2_text = "拒绝",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, SummonPlace, nChair, SummonName, srcGuid, sceneKey, posX, posZ, dir)
    if nChair ~= 0 then
      self.text1 = string.format("%s%s正在召唤您，是否接受本次召唤？", gUI_GUILD_POSITION_NAME[nChair], SummonName)
      self.text2 = ""
      self.text3 = "{#R^&30&}后自动拒绝"
    else
      self.text1 = string.format("%s正在召唤您，是否接受本次召唤？", SummonName)
      self.text2 = ""
      self.text3 = "{#R^&30&}后自动拒绝"
    end
    self.srcGuid = srcGuid
    self.sceneKey = sceneKey
    self.posX = posX
    self.posZ = posZ
    self.dir = dir
    return self
  end,
  OnAccept = function(self)
    RetSummonPlayer(self.srcGuid, true, self.sceneKey, self.posX, self.posZ, self.dir)
  end,
  OnCancel = function(self)
    RetSummonPlayer(self.srcGuid, false, self.sceneKey, self.posX, self.posZ, self.dir)
  end,
  OnTimeOut = function(self)
    RetSummonPlayer(self.srcGuid, false, self.sceneKey, self.posX, self.posZ, self.dir)
  end
}
MessageboxTypes.SUMMON_REQUEST_SCENE = {
  title = "召唤请求",
  text = "召唤请求",
  button1_text = "立即传送",
  button2_text = "拒绝传送",
  has_button1 = true,
  has_button2 = true,
  replace_type = "SAME_TYPE_HIDE",
  Create = function(self, nMapId, posX, posZ, nTime)
    self.text1 = "{#R^&" .. nTime .. "&}后进行传送"
    self.nMapId = nMapId
    self.posX = posX
    self.posZ = posZ
    self.nTime = nTime
    return self
  end,
  OnAccept = function(self)
    RetSceneTranse(true, self.nMapId, self.posX, self.posZ)
  end,
  OnCancel = function(self)
    RetSceneTranse(false, self.nMapId, self.posX, self.posZ)
  end,
  OnTimeOut = function(self)
    RetSceneTranse(true, self.nMapId, self.posX, self.posZ)
  end
}
MessageboxTypes.DUEL_REQUEST = {
  title = "",
  text = "",
  button1_text = "同意",
  button2_text = "拒绝",
  has_button1 = true,
  has_button2 = true,
  replace_type = "NOT_REPLACE",
  Create = function(self, playerName)
    if playerName then
      self.text1 = string.format("【%s】邀请你与他切磋", playerName)
      self.text2 = "是否接受邀请?"
      self.text3 = "倒计时: {#R^&10&}"
    end
    return self
  end,
  OnAccept = function(self)
    WorldStage:AcceptDuel()
  end,
  OnCancel = function(self)
    WorldStage:CancelDuel()
  end,
  OnTimeOut = function(self)
    WorldStage:CancelDuel()
  end
}
MessageboxTypes.INSTANCE_KICK = {
  title = "",
  text = "",
  button1_text = "退出副本",
  has_button1 = true,
  has_button2 = false,
  Create = function(self, timeSec)
    self.text1 = "你已经离开队伍"
    self.text2 = "将在{#R^&" .. tostring(timeSec) .. "&}后传出副本"
    return self
  end,
  OnAccept = function(self)
    InstanceExit()
  end
}
MessageboxTypes.USE_PET_EGG = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, name, bag, slot)
    self.text1 = "是否确定孵化{#G^【" .. name .. "】}"
    self.bag = bag
    self.slot = slot
    self.Guid = GetItemGUIDByPos(bag, slot)
    return self
  end,
  OnAccept = function(self)
    local bag, slot = GetItemPosByGuid(self.Guid)
    if bag == gUI_GOODSBOX_BAG.backpack0 then
      UseItem(bag, slot)
    end
  end
}
MessageboxTypes.USE_PET_ITEM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, name, bag, slot)
    self.text1 = "使用后无法将宠物取出"
    self.text2 = "是否使用{#G^【" .. name .. "】}"
    self.bag = bag
    self.slot = slot
    self.Guid = GetItemGUIDByPos(bag, slot)
    return self
  end,
  OnAccept = function(self)
    local bag, slot = GetItemPosByGuid(self.Guid)
    if bag == gUI_GOODSBOX_BAG.backpack0 then
      UseItem(bag, slot)
    end
  end
}
MessageboxTypes.USE_PET_STUDY_SKILL = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, skillName, bag, slot, newSlot)
    self.text1 = "是否确定让您的仙灵学习{#G^【" .. skillName .. "】}?"
    self.bag = bag
    self.slot = slot
    self.newSlot = newSlot
    return self
  end,
  OnAccept = function(self)
    UseItemPetSkill(self.bag, self.slot, self.newSlot)
  end
}
MessageboxTypes.USE_MOUNT = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, bag, slot, name)
    self.text1 = "是否确认使用{#G^【" .. name .. "】}？"
    self.text2 = "使用后坐骑将会与你绑定"
    self.bag = bag
    self.slot = slot
    self.Guid = GetItemGUIDByPos(bag, slot)
    return self
  end,
  OnAccept = function(self)
    local bag, slot = GetItemPosByGuid(self.Guid)
    if bag == gUI_GOODSBOX_BAG.backpack0 then
      UseItem(bag, slot)
    end
  end
}
MessageboxTypes.USE_DANTIANITEM = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, bag, slot)
    self.text1 = "使用后聚灵次数已经超出上限"
    self.text2 = "是否确认使用?"
    self.bag = bag
    self.slot = slot
    self.Guid = GetItemGUIDByPos(bag, slot)
    return self
  end,
  OnAccept = function(self)
    local bag, slot = GetItemPosByGuid(self.Guid)
    if bag == gUI_GOODSBOX_BAG.backpack0 then
      UseItem(bag, slot)
    end
  end
}
MessageboxTypes.TRANSMISSION = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, npcid, index, price, mapName)
    self.text1 = "确认要花费" .. Lua_UI_Money2String(price) .. "传送到" .. mapName .. "吗?"
    self.npcid = npcid
    self.index = index
    return self
  end,
  OnAccept = function(self)
    SelectGossipOption(self.npcid, self.index, 1)
  end
}
MessageboxTypes.TRANSMISSION_QUEST = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, questID, mapName, posx, posy)
    self.text1 = "确认要传送到{#Y^" .. mapName .. " " .. posx .. "," .. posy .. "} 位置?"
    self.questID = questID
    return self
  end,
  OnAccept = function(self)
    AcceptQuestTrans(self.questID)
  end
}
MessageboxTypes.PETBLEND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, npcid, index, price, mapName)
    self.text1 = "合成后副宠将消失，是否进行合成?"
    return self
  end,
  OnAccept = function(self)
    Lua_Pet_BlendOk()
  end
}
MessageboxTypes.COMFIRM_RUNE = {
  title = "",
  text = "是否确定镶嵌符文?",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, page, runeId)
    self.page = page
    self.runeId = runeId
    return self
  end,
  OnAccept = function(self)
    InsertRune(self.page, self.runeId)
  end
}
MessageboxTypes.REMOVE_RUNE = {
  title = "",
  text = "是否确定删除符文?",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, page, index)
    self.page = page
    self.index = index
    return self
  end,
  OnAccept = function(self)
    RemoveRune(self.page, self.index)
  end
}
MessageboxTypes.ACTIVE_RUNE = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, runeId, needMoney)
    self.runeId = runeId
    if needMoney > 0 then
      self.text1 = "是否消耗" .. Lua_UI_Money2String(needMoney) .. "激活符文?"
    else
      self.text1 = "是否确定激活符文?"
    end
    return self
  end,
  OnAccept = function(self)
    ActiveRune(self.runeId)
  end
}
MessageboxTypes.ACTIVE_CASH = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text1 = "需要扣除1绑金，是否确认开始净灵"
    return self
  end,
  OnAccept = function(self)
    ActiveCashCow()
  end
}
MessageboxTypes.CARNIVAL_EXIT = {
  button1_text = "确定",
  button2_text = "取消 ",
  has_button1 = true,
  has_button2 = true,
  Create = function(self)
    self.text = "是否确定退出副本"
    return self
  end,
  OnAccept = function(self)
    ChooseFloor(0)
    gUI.CarnivalUI.Reward:SetVisible(false)
  end
}
MessageboxTypes.KUOCHONG_BAG = {
  button1_text = "确定",
  button2_text = "取消",
  has_button1 = true,
  has_button2 = true,
  item_id = -1,
  is_bank = false,
  Create = function(self, itemID, isBank)
    self.text = "是否确定扩充背包?"
    self.item_id = itemID
    self.is_bank = isBank
    return self
  end,
  OnAccept = function(self)
    local bag, slot = GetItemInBagIndex(self.item_id)
    if bag == gUI_GOODSBOX_BAG.backpack0 and slot ~= -1 then
      if not self.is_bank then
        UseItem(bag, slot)
      else
        UseItemToBankBag(bag, slot)
      end
    end
  end
}
MessageboxTypes.USE_FAERIE_BLEND = {
  title = "",
  text = "",
  has_button1 = true,
  has_button2 = true,
  Create = function(self, itemName, itemQuality, itemGuid, Desc, nMax, nBaseValue)
    self.Guid = itemGuid
    self.text1 = string.format("当前%s为%d, 使用{#C^%s^【%s】}后变为%d, 是否确定使用？", Desc, nBaseValue, gUI_GOODS_QUALITY_COLOR_PREFIX[itemQuality], itemName, nMax)
    return self
  end,
  OnAccept = function(self)
    local bag, slot = GetItemPosByGuid(self.Guid)
    if bag == gUI_GOODSBOX_BAG.backpack0 then
      UseItem(bag, slot)
    end
  end
}
MessageboxTypes.ACCEPT_QUEST_MONEY = {
  title = "接取任务",
  has_button1 = true,
  has_button2 = true,
  modal_state = true,
  Create = function(self, costMoney, NpcId, QustId)
    self.text1 = string.format("接取任务需要扣除%s，是否接取？", Lua_UI_Money2String(costMoney))
    self.NpcId = NpcId
    self.QustId = QustId
    return self
  end,
  OnAccept = function(self)
    AcceptQuest(self.NpcId, self.QustId, DIALOG_STATUS_AVAILABLE, 0)
  end
}
for k, t in pairs(MessageboxTypes) do
  if type(t) == "table" then
    setmetatable(t, MessageboxTypes)
    t.__index = t
    t.binded_widget = nil
  end
end
local MessageBoxManager = {
  widgets_state = {},
  widgets = {},
  messages = {}
}
function Messagebox_EnableMsgTypeOkBtn(msg_box_type, bEnable)
  for i, idle in pairs(MessageBoxManager.widgets_state) do
    if MessageBoxManager.messages[i].typename == msg_box_type then
      MessageBoxManager.widgets[i]:GetChildByName("Confirm"):SetEnable(bEnable)
    end
  end
end
function MessageBoxManager:HasReplaceMsg()
  for i, idle in pairs(self.widgets_state) do
    if not idle and not self.messages[i].replace_type and i < 5 then
      return i
    end
  end
  return -1
end
function MessageBoxManager:HasSameTypeHideMsg(msg_box_type)
  for i, idle in pairs(self.widgets_state) do
    if not idle and self.messages[i].typename == msg_box_type and i < 5 then
      return self.widgets[i]
    end
  end
  return nil
end
function MessageBoxManager:HasNotReplaceMsg()
  for i, idle in pairs(self.widgets_state) do
    if not idle and self.messages[i].replace_type == "NOT_REPLACE" and i >= 5 then
      return i
    end
  end
  return -1
end
function MessageBoxManager:MsgBoxCanUse(index)
  for i, idle in pairs(self.widgets_state) do
    if i >= 5 and self.parentWidgetIndex[i] == index and self.widgets[i]:IsVisible() then
      return false
    end
  end
  return true
end
function MessageBoxManager:Create(msg_box_type, ...)
  local msgbox = MessageboxTypes[msg_box_type]:Create(...)
  msgbox.typename = msg_box_type
  local stage = gUI_CurrentStage
  local widget_template = WindowSys_Instance:GetWindow("Messagebox")
  if stage == 1 or stage == 2 then
    widget_template:SetCustomUserData(0, 1)
    self.messages[1] = msgbox
    local new_widget = WindowSys_Instance:CreateWndFromTemplate(widget_template, WindowSys_Instance:GetRootWindow())
    msgbox.binded_widget = new_widget
    return msgbox
  else
    if msgbox.replace_type == nil and MessageBoxManager:HasReplaceMsg() ~= -1 then
      while MessageBoxManager:HasReplaceMsg() ~= -1 do
        local _widget = self.widgets[MessageBoxManager:HasReplaceMsg()]
        if _widget:IsVisible() then
          MessageBoxManager:HandelMessage("CANCEL", _widget:GetCustomUserData(0))
        else
          msgbox.binded_widget = self.widgets[MessageBoxManager:HasReplaceMsg()]
          self.messages[MessageBoxManager:HasReplaceMsg()] = msgbox
          return msgbox
        end
      end
    end
    if msgbox.replace_type == "SAME_TYPE_HIDE" and MessageBoxManager:HasSameTypeHideMsg(msg_box_type) then
      for i, idle in pairs(self.widgets_state) do
        if idle and i >= 5 then
          self.widgets_state[i] = false
          self.messages[i] = msgbox
          msgbox.binded_widget = self.widgets[i]
          self.widgets[i]:moveToBack()
          if msgbox.binded_widget then
            WindowSys_Instance:RemoveUpdate(msgbox.binded_widget)
            return msgbox
          end
        end
      end
      local max = table.getn(self.widgets) + 1000
      local new_widget = WindowSys_Instance:CreateWndFromTemplate(MessageBoxManager:HasSameTypeHideMsg(msg_box_type), WindowSys_Instance:GetRootWindow())
      new_widget:SetCustomUserData(0, max)
      self.widgets[max] = new_widget
      self.widgets_state[max] = false
      self.messages[max] = msgbox
      if not self.parentWidgetIndex then
        self.parentWidgetIndex = {}
      end
      self.parentWidgetIndex[max] = MessageBoxManager:HasSameTypeHideMsg(msg_box_type):GetCustomUserData(0)
      msgbox.binded_widget = new_widget
      self.widgets[max]:moveToBack()
      return msgbox
    end
    for i, idle in pairs(self.widgets_state) do
      if idle and MessageBoxManager:MsgBoxCanUse(i) then
        self.widgets_state[i] = false
        self.messages[i] = msgbox
        msgbox.binded_widget = self.widgets[i]
        if msgbox.binded_widget then
          msgbox.binded_widget:SetLeft(widget_template:GetLeft())
          msgbox.binded_widget:SetTop(widget_template:GetTop() + widget_template:GetHeight() * (i - 1))
          WindowSys_Instance:RemoveUpdate(msgbox.binded_widget)
          return msgbox
        end
      end
    end
    local max = table.getn(self.widgets) + 1
    local new_widget = WindowSys_Instance:CreateWndFromTemplate(widget_template, WindowSys_Instance:GetRootWindow())
    new_widget:SetCustomUserData(0, max)
    new_widget:SetLeft(widget_template:GetLeft())
    new_widget:SetTop(widget_template:GetTop() + widget_template:GetHeight() * (max - 1))
    self.widgets[max] = new_widget
    self.widgets_state[max] = false
    self.messages[max] = msgbox
    msgbox.binded_widget = new_widget
    return msgbox
  end
end
function Lua_MB_CloseBox(type)
  MessageBoxManager:Hide(type)
end
function MessageBoxManager:Hide(msg_box_type)
  local msg_box = MessageboxTypes[msg_box_type]
  local widget = msg_box.binded_widget
  if widget then
    local index = widget:GetCustomUserData(0)
    self.widgets_state[index] = true
    widget:SetVisible(false)
    msg_box.binded_widget = nil
  end
end
function MessageBoxManager:Clear()
  self.widgets = {}
  self.widgets_state = {}
  self.messages = {}
end
function MessageBoxManager:HandelMessage(accept, index)
  local msg_box = self.messages[index]
  if accept == "ACCEPT" then
    msg_box:OnAccept()
  elseif accept == "CANCEL" then
    msg_box:OnCancel()
  end
  self.widgets_state[index] = true
  if MessageBoxManager:HasNotReplaceMsg() ~= -1 then
    self.widgets[MessageBoxManager:HasNotReplaceMsg()]:SetLeft(msg_box.binded_widget:GetLeft())
    self.widgets[MessageBoxManager:HasNotReplaceMsg()]:SetTop(msg_box.binded_widget:GetTop())
  end
  if index >= 1000 then
    self.widgets_state[index] = false
  end
  msg_box.binded_widget = nil
end
function MessageBoxManager:HandelCenter(index)
  local msg_box = self.messages[index]
  msg_box:OnCenter()
  if gUI_CurrentStage ~= 1 and gUI_CurrentStage ~= 2 then
    self.widgets[index]:GetChildByName("Text"):SetProperty("Text", "")
    self.widgets[index]:GetChildByName("Title"):SetProperty("Text", "")
    self.widgets[index]:GetChildByName("desc1_dlab"):SetProperty("Text", "")
    self.widgets[index]:GetChildByName("desc2_dlab"):SetProperty("Text", "")
    self.widgets[index]:GetChildByName("desc3_dlab"):SetProperty("Text", "")
  end
  self.widgets_state[index] = true
  if MessageBoxManager:HasNotReplaceMsg() ~= -1 then
    self.widgets[MessageBoxManager:HasNotReplaceMsg()]:SetLeft(msg_box.binded_widget:GetLeft())
    self.widgets[MessageBoxManager:HasNotReplaceMsg()]:SetTop(msg_box.binded_widget:GetTop())
  end
  if index >= 1000 then
    self.widgets_state[index] = false
  end
  msg_box.binded_widget = nil
end
function MessageBoxManager:HandelTimeOut(index)
  MessageBoxManager:HandelMessage("CANCEL", index)
  local msg_box = self.messages[index]
  msg_box:OnTimeOut()
  msg_box.binded_widget = nil
  self.widgets_state[index] = true
  if gUI_CurrentStage ~= 1 and gUI_CurrentStage ~= 2 then
    self.widgets[index]:GetChildByName("Text"):SetProperty("Text", "")
    self.widgets[index]:GetChildByName("Title"):SetProperty("Text", "")
    self.widgets[index]:GetChildByName("desc1_dlab"):SetProperty("Text", "")
    self.widgets[index]:GetChildByName("desc2_dlab"):SetProperty("Text", "")
    self.widgets[index]:GetChildByName("desc3_dlab"):SetProperty("Text", "")
  end
  if MessageBoxManager:HasNotReplaceMsg() ~= -1 then
    self.widgets[MessageBoxManager:HasNotReplaceMsg()]:SetLeft(msg_box.binded_widget:GetLeft())
    self.widgets[MessageBoxManager:HasNotReplaceMsg()]:SetTop(msg_box.binded_widget:GetTop())
  end
  if index >= 1000 then
    self.widgets_state[index] = false
  end
end
function OnAcceptClicked(msg)
  local wnd = msg:get_window():GetParent()
  DTrace("---" .. tostring(wnd))
  wnd:SetVisible(false)
  MessageBoxManager:HandelMessage("ACCEPT", wnd:GetCustomUserData(0))
end
function OnCancelClicked(msg)
  local wnd = msg:get_window():GetParent()
  wnd:SetVisible(false)
  MessageBoxManager:HandelMessage("CANCEL", wnd:GetCustomUserData(0))
end
function OnCenterClicked(msg)
  local wnd = msg:get_window():GetParent()
  wnd:SetVisible(false)
  MessageBoxManager:HandelCenter(wnd:GetCustomUserData(0))
end
function MessageBox_OnTimerStop(msg)
  local wnd = msg:get_window():GetParent()
  if wnd:IsVisible() then
    wnd:SetVisible(false)
    MessageBoxManager:HandelTimeOut(wnd:GetCustomUserData(0))
  end
end
function Messagebox_Show(msg_box_type, ...)
  local msg_box = MessageBoxManager:Create(msg_box_type, ...)
  local root = msg_box.binded_widget
  root:GetChildByName("desc1_dlab"):SetProperty("Text", "")
  root:GetChildByName("desc2_dlab"):SetProperty("Text", "")
  root:GetChildByName("desc3_dlab"):SetProperty("Text", "")
  local show_btn1 = msg_box.has_button1
  local show_btn2 = msg_box.has_button2
  local show_btn3 = msg_box.has_button3
  root:GetChildByName("Text"):SetProperty("Text", msg_box.text)
  root:GetChildByName("Title"):SetProperty("Text", msg_box.title)
  if msg_box.text1 then
    root:GetChildByName("desc1_dlab"):SetProperty("Text", msg_box.text1)
  end
  if msg_box.text2 then
    root:GetChildByName("desc2_dlab"):SetProperty("Text", msg_box.text2)
  end
  if msg_box.text3 then
    root:GetChildByName("desc3_dlab"):SetProperty("Text", msg_box.text3)
  end
  local btnOK = root:GetChildByName("Confirm")
  local btnCancel = root:GetChildByName("Cancel")
  local btnCenter = root:GetChildByName("Center_btn")
  btnOK:SetEnable(true)
  btnCancel:SetEnable(true)
  btnCenter:SetEnable(true)
  local EditBoxPassword = root:GetGrandChild("passwords_bg")
  if msg_box.HasEditBox then
    EditBoxPassword:SetVisible(true)
    EditBoxPassword:GetChildByName("Edit_ebox"):SetProperty("MaxLength", tostring(msg_box.EditLen))
  else
    EditBoxPassword:SetVisible(false)
  end
  if show_btn1 and show_btn2 and show_btn3 then
    btnOK:SetProperty("HorzLayout", "default")
    btnCancel:SetProperty("HorzLayout", "default")
    btnCenter:SetProperty("HorzLayout", "default")
    btnOK:SetVisible(true)
    btnCancel:SetVisible(true)
    btnCenter:SetVisible(true)
    btnOK:SetProperty("Text", msg_box.button1_text)
    btnCancel:SetProperty("Text", msg_box.button2_text)
    btnCenter:SetProperty("Text", msg_box.button3_text)
  elseif show_btn1 and show_btn2 then
    btnOK:SetProperty("HorzLayout", "default")
    btnCancel:SetProperty("HorzLayout", "default")
    btnCenter:SetProperty("HorzLayout", "default")
    local width = root:GetWidth() - btnOK:GetWidth() - btnCancel:GetWidth()
    width = width / 3
    btnOK:SetProperty("Left", tostring(width))
    btnCancel:SetProperty("Left", tostring(width + width + btnOK:GetWidth()))
    btnOK:SetVisible(true)
    btnCancel:SetVisible(true)
    btnCenter:SetVisible(false)
    btnOK:SetProperty("Text", msg_box.button1_text)
    btnCancel:SetProperty("Text", msg_box.button2_text)
  elseif show_btn1 then
    btnOK:SetProperty("HorzLayout", "center")
    btnCancel:SetVisible(false)
    btnOK:SetVisible(true)
    btnCenter:SetVisible(false)
    btnOK:SetProperty("Text", msg_box.button1_text)
  elseif show_btn2 then
    btnCancel:SetProperty("HorzLayout", "center")
    btnOK:SetVisible(false)
    btnCancel:SetVisible(true)
    btnCenter:SetVisible(false)
    btnCancel:SetProperty("Text", msg_box.button2_text)
  else
    btnOK:SetVisible(false)
    btnCancel:SetVisible(false)
    btnCenter:SetVisible(false)
  end
  root:SetProperty("ModalState", tostring(msg_box.modal_state))
  root:SetVisible(true)
  WorldStage:playSoundByID(27)
  return msg_box
end
function Messagebox_OnLoad()
end
function MessageBoxManager:_OnMsgBox_Show(show)
  if not show then
    for i, wnd in pairs(self.widgets) do
      if not self.messages[i].disableESC then
        if wnd:IsVisible() then
          self:HandelMessage("CANCEL", i)
        end
        wnd:SetVisible(false)
      end
    end
  end
end
function _OnMsgBox_ConnectServerFail(bLost)
  if gUI.Safety and gUI.Safety.Root and gUI.Safety.Root:IsVisible() then
    Lua_Safety_Close()
  end
  if gUI_CurrentStage == 3 then
    if bLost then
      Messagebox_Show("CONNECT_SERVER_FAIL")
    else
    end
  else
    if bLost then
      Messagebox_Show("CONNECT_SERVER_FAIL2")
    else
    end
    if bLost and Lua_CharList_AccountGrp_Succ then
      Lua_CharList_AccountGrp_Succ()
    end
  end
end
function Messagebox_OnEvent(event)
  if event == "MESSAGEBOX_SHOW_NOTIFY_MSG" then
    local msgType
    if arg3 and string.len(arg3) > 0 then
      msgType = arg3
    else
      msgType = "NOTIFY_MSG"
    end
    MessageboxTypes[msgType].has_button1 = arg2
    MessageboxTypes[msgType].text = arg1
    MessageboxTypes[msgType].text1 = ""
    MessageboxTypes[msgType].text2 = ""
    MessageboxTypes[msgType].text3 = ""
    Messagebox_Show(msgType, arg1)
  elseif event == "CONFIRM_OTHER_REVIVE" then
    Messagebox_Show("CONFIRM_OTHER_REVIVE", arg1)
  elseif event == "NPC_ITEM_EXCHANGE" then
    Messagebox_Show("NPC_ITEM_EXCHANGE", arg1, arg2, arg3)
  elseif event == "MESSAGEBOX_SHOW" then
    MessageBoxManager:_OnMsgBox_Show(arg1)
  elseif event == "CONNECT_SERVER_FAIL" then
    _OnMsgBox_ConnectServerFail(arg1)
  elseif event == "EXIT_APP" and not gUI.Safety.Root:IsVisible() then
    SystemOption_Show(true)
  end
end
function Messagebox_OnHide()
end
function Messagebox_Hide(msg_box_type)
  MessageBoxManager:Hide(msg_box_type)
end
