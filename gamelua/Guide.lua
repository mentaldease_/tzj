if gUI and not gUI.Guide then
  gUI.Guide = {}
end
_Guide_CurrTime = 100
_Guide_CurrTime2 = 0
_Guide_CurrTime3 = 0
_Guide_IntervaTime = 60
_Guide_Cnt = 0
_Guide_bSkillDelay = false
_Guide_SkillWndBox = nil
_Guide_SkillParam1 = 0
_Guide_SkillParam2 = 0
_Guide_SkillPaoPaoDir = 0
_Guide_bSkillDelayNew = false
_Guide_SkillWndBoxNew = nil
_Guide_SkillParamNew1 = 0
_Guide_SkillParamNew2 = 0
_Guide_SkillPaoPaoDirNew = 0
local USER_GUIDE_TRADE = 86
local USER_GUIDE_GUIDE = 45
local USER_GUIDE_FUBEN = 55
local USER_GUIDE_FORGE = 57
local USER_GUIDE_BLEND = 58
local USER_GUIDE_CX = 59
local USER_GUIDE_SIGN = 67
local USER_GUIDE_AMASS = 192
local USER_GUIDE_GEM = 193
local USER_GUIDE_FRIEND = 194
local USER_GUIDE_BLENDSJ = 195
local USER_GUIDE_BLENDQS = 196
local USER_GUIDE_BLENDLX = 197
local USER_GUIDE_BLENDCX = 198
local USER_GUIDE_BLENDFL = 199
local USER_GUIDE_RUNESJ = 200
local USER_GUIDE_RUNEQS = 201
local USER_GUIDE_RUNELX = 202
local USER_GUIDE_RUNECX = 203
local USER_GUIDE_RUNEFL = 204
local USER_GUIDE_GRZY = 206
local USER_GUIDE_ZZY = 207
local USER_GUIDE_ldlh = 208
local USER_GUIDE_qqzc = 209
local QUEST_STATUS_AVAILABLE = 4
function _Guide_TimeEvent(_timer)
  if _Guide_CurrTime > _Guide_IntervaTime then
    local acceptedQusetNum = GetAcceptedQuestNum()
    local _, _, _, _, level_self = GetPlaySelfProp(4)
    if acceptedQusetNum == 0 and level_self < 30 and not gUI.Gossip.Root:IsVisible() then
      _Guide_CurrTime = 0
    end
  end
  if _Guide_CurrTime > _Guide_IntervaTime then
    return
  end
  _Guide_CurrTime = _Guide_CurrTime + _timer:GetRealInterval()
  if _Guide_CurrTime >= _Guide_IntervaTime then
    acceptedQusetNum = GetAcceptedQuestNum()
    _, _, _, _, level_self = GetPlaySelfProp(4)
    if acceptedQusetNum == 0 and 30 > level_self and not gUI.Gossip.Root:IsVisible() and _Guide_Cnt == 0 then
      TriggerUserGuide(18, 0, 0)
      _Guide_Cnt = 1
    end
  end
end
function _Guide_TimeEvent2(_timer)
  _Guide_TrackNewSkill(_Guide_SkillWndBox, _Guide_SkillParam1, _Guide_SkillParam2, _Guide_SkillPaoPaoDir)
end
function _Guide_TimeEvent3(_timer)
  _Guide_TrackRunePos(_Guide_SkillWndBoxNew, _Guide_SkillParamNew1, _Guide_SkillParamNew2, _Guide_SkillPaoPaoDirNew)
end
function _Guide_TrackSkill(wndbox, index, param2, dir)
  local wnd
  wnd = WindowToContainer(WindowSys_Instance:GetWindow("Skill.Skill_bg.Center_bg.Skill_cnt"))
  wnd:SizeSelf()
  for i = 0, index - 1 do
    local item = wnd:GetChild(i)
    if item then
      local btn = item:GetChildByName("Course_btn")
      local rect = btn:GetWndRect()
      if btn:IsEnable() then
        local RectContain = wnd:GetWndRect()
        local skillId = item:GetCustomUserData(0)
        local NewItem = wnd:GetChildByData0(skillId)
        local goodsBoxNew = WindowToGoodsBox(NewItem:GetChildByName("Skill_gbox"))
        local RectPoint = goodsBoxNew:GetItemRect(0)
        wndbox:setTrackWindow(btn)
        wndbox:setGuideClickRegion(rect)
        _Guide_TrackRect(wndbox, rect, dir)
        return
      end
    end
  end
end
function _IsPointIn(x, y, left, right, top, bottom)
  if left <= x and x < right and top <= y and y < bottom then
    return true
  else
    return false
  end
end
function _Guide_TrackRunePos(wndbox, index, param2, dir)
  local wnd
  wnd = WindowToContainer(WindowSys_Instance:GetWindow("Skill.Rune_bg.Right_bg.RuneList_cnt"))
  local item = wnd:GetChildByData0(index)
  if item then
    local btn = item:GetChildByName("Mosaic_btn")
    local rect = btn:GetWndRect()
    wndbox:setTrackWindow(btn)
    wndbox:setGuideClickRegion(rect)
    _Guide_TrackRect(wndbox, rect, dir)
    gUI.Guide.HornTimer2:Stop()
  else
    _Guide_bSkillDelayNew = true
    _Guide_SkillWndBoxNew = wndbox
    _Guide_SkillParamNew1 = index
    _Guide_SkillParamNew2 = param2
    _Guide_SkillPaoPaoDirNew = dir
    gUI.Guide.HornTimer2:Restart()
    return
  end
end
function _Guide_TrackBagItem(wndbox, bag, slot, dir)
  local wnd
  if bag == 1 then
    wnd = gUI.Bag.GoodsBoxBag
  elseif bag == 2 then
    wnd = gUI.Bag.GoodsBoxTask
  else
    return
  end
  wnd = WindowToGoodsBox(wnd)
  local rect = wnd:GetItemRect(slot)
  wndbox:setTrackWindow(wnd)
  wndbox:setGuideClickRegion(rect)
  _Guide_TrackRect(wndbox, rect, dir)
end
function _Guide_TrackEquipItem(wndbox, bag, slot, dir)
  local wnd
  wnd = gUI.Blend.MyEquipBox
  wnd = WindowToGoodsBox(wnd)
  slot = _CHARA_EQUIPPOINT_CANTAINBLEND[slot][1]
  local rect = wnd:GetItemRect(slot)
  wndbox:setTrackWindow(wnd)
  wndbox:setGuideClickRegion(rect)
  _Guide_TrackRect(wndbox, rect, dir)
end
function _Guide_TrackNpcShop(wndbox, index, slot, dir)
  local wnd = Lua_Shop_GetItemWindow(index)
  if not wnd then
    return
  end
  local rect = wnd:GetWndRect()
  wndbox:setTrackWindow(wnd)
  wndbox:setGuideClickRegion(rect)
  _Guide_TrackRect(wndbox, rect, dir)
end
function _Guide_TrackMounts(wndbox, index, nParam2, dir)
  local wnd = gUI.Mounts.gboxSkill1
  if not wnd then
    return
  end
  wnd = WindowToGoodsBox(wnd)
  local rect = wnd:GetItemRect(index)
  wndbox:setTrackWindow(wnd)
  wndbox:setGuideClickRegion(rect)
  _Guide_TrackRect(wndbox, rect, dir)
end
function _Guide_TrackNewSkill(wndbox, param1, param2, dir)
  for i = 1, 10 do
    local gb = WindowToGoodsBox(WindowSys_Instance:GetWindow("SkillGift.SkillContai_bg.Skillpart" .. tostring(i) .. "_bg.SkillIcon_gbox"))
    if gb:GetItemData(0) == param1 then
      local rect = gb:GetItemRect(0)
      wndbox:setTrackWindow(gb)
      wndbox:setGuideClickRegion(rect)
      _Guide_TrackRect(wndbox, rect, dir)
      gUI.Guide.HornTimer1:Stop()
      return
    end
  end
  _Guide_bSkillDelay = true
  _Guide_SkillWndBox = wndbox
  _Guide_SkillParam1 = param1
  _Guide_SkillParam2 = param2
  _Guide_SkillPaoPaoDir = dir
  gUI.Guide.HornTimer1:Restart()
end
function _Guide_TrackShortCutItem(wndbox, strCtrlPath, param1, param2, dir)
  local wnd, rect
  if param1 == 0 then
    wnd = WindowSys_Instance:GetWindow(strCtrlPath)
    wnd = WindowToGoodsBox(wnd)
    local index = 0
    if wnd == nil then
      return
    end
    for i = 0, wnd:GetItemCount() - 1 do
      if not wnd:IsItemHasIcon(i) then
        break
      end
      index = index + 1
    end
    rect = wnd:GetItemRect(index)
  elseif param1 == 1 then
    wnd = WindowSys_Instance:GetWindow(strCtrlPath)
    wnd = WindowToGoodsBox(wnd)
    local index = 0
    if wnd == nil then
      return
    end
    for i = 0, wnd:GetItemCount() - 1 do
      if wnd:IsItemHasIcon(i) then
        index = index + 1
        if index == param2 then
          break
        end
      end
    end
    if index > 0 then
      index = index - 1
    end
    rect = wnd:GetItemRect(index)
  end
  wndbox:setTrackWindow(wnd)
  wndbox:setGuideClickRegion(rect)
  _Guide_TrackRect(wndbox, rect, dir)
end
function _Guide_TrackControl(wndbox, strCtrlPath, nParam1, nParam2, dir)
  local wnd = WindowSys_Instance:GetWindow(strCtrlPath)
  if wnd == nil then
    return
  end
  if dir ~= 0 then
    wndbox:setTrackWindow(wnd)
  end
  local rect = wnd:GetWndRect()
  if nParam1 == 1 then
    local tableCtrl = WindowToTableCtrl(wnd)
    rect = tableCtrl:GetItemRect(nParam2)
    wndbox:setGuideClickRegion(rect)
  end
  _Guide_TrackRect(wndbox, rect, dir)
end
function _Guide_TrackSpecialText(wndbox, strCtrlPath, strText, dir)
  local wnd = WindowSys_Instance:GetWindow(strCtrlPath)
  if wnd == nil then
    return
  end
  wndbox:setTrackWindow(wnd)
  local rect = wnd:findRegionByText(strText)
  if rect == nil then
    return
  end
  wndbox:setGuideClickRegion(rect)
  _Guide_TrackRect(wndbox, rect, dir)
end
function _Guide_TrackQuestItem(wndbox, strCtrlPath, param1, param2, dir)
  local rect
  local wnd = WindowSys_Instance:GetWindow(strCtrlPath)
  if wnd == nil then
    return
  end
  LogMsg("UserGuide_TrackQuestItem 1")
  if param1 + param2 > 0 then
    LogMsg("UserGuide_TrackQuestItem 2")
    wnd = WindowToDisplayBox(wnd)
    LogMsg("UserGuide_TrackQuestItem 3")
    rect = wnd:GetHyperLinkRect(param1, param2)
    wndbox:setTrackWindow(wnd)
    wndbox:setGuideClickRegion(rect)
  else
    wndbox:setTrackWindow(wnd)
    rect = wnd:GetWndRect()
  end
  _Guide_TrackRect(wndbox, rect)
end
function _Guide_TrackRect(wndbox, rect, dir)
  if wndbox == nil then
    return
  end
  local arrow = wndbox:GetChildByName("Arrow1")
  arrow:SetProperty("Visible", "false")
  arrow = wndbox:GetChildByName("Arrow2")
  arrow:SetProperty("Visible", "false")
  arrow = wndbox:GetChildByName("Arrow3")
  arrow:SetProperty("Visible", "false")
  arrow = wndbox:GetChildByName("Arrow4")
  arrow:SetProperty("Visible", "false")
  if dir ~= 0 then
    wndbox:GetChildByName("Arrow" .. dir):SetProperty("Visible", "true")
  end
  local posxMiddle = rect:get_left() + rect:get_width() * 0.5
  local posxLeft = rect:get_left() - p2u_x_proxy(30)
  local posxRight = rect:get_right() + p2u_x_proxy(55)
  local posyMiddle = rect:get_top() + rect:get_height() * 0.5
  local posyTop = rect:get_top() - p2u_y_proxy(50)
  local posyBottom = rect:get_bottom() + p2u_y_proxy(50)
  if dir == 1 then
    wndbox:SetRight(posxLeft)
    wndbox:SetTop(posyMiddle - wndbox:GetWndRect():get_height() * 0.5)
  elseif dir == 2 or dir == 0 then
    wndbox:SetLeft(posxMiddle - wndbox:GetWndRect():get_width() * 0.5)
    wndbox:SetBottom(posyTop)
  elseif dir == 3 then
    wndbox:SetLeft(posxRight)
    wndbox:SetTop(posyMiddle - wndbox:GetWndRect():get_height() * 0.5)
  elseif dir == 4 then
    wndbox:SetLeft(posxMiddle - wndbox:GetWndRect():get_width() * 0.5)
    wndbox:SetTop(posyBottom)
  end
end
function _Guide_ShowHintMission(questId)
  gUI.Gossip.CurNpcId = -1
  gUI.Gossip.Root:SetProperty("Visible", "true")
  gUI.Gossip.Transmis:SetProperty("Visible", "false")
  gUI.Gossip.Back:SetProperty("Visible", "false")
  _ShowNormalQuestDetail(questId)
  gUI.Gossip.Title:SetProperty("Text", "引导")
  gUI.Gossip.HeadPic:SetProperty("BackImage", "tg_NPChead04:head_10")
end
function Lua_Guide_InitState()
  _Guide_CurrTime = 100
  _Guide_IntervaTime = 60
  _Guide_Cnt = 0
  AddTimerEvent(2, "GuideTimer", _Guide_TimeEvent)
  gUI.Guide.HornTimer1 = TimerSys_Instance:CreateTimerObject("GuideTimer2", 1, -1, "_Guide_TimeEvent2", false, false)
  gUI.Guide.HornTimer2 = TimerSys_Instance:CreateTimerObject("GuideTimer3", 1, -1, "_Guide_TimeEvent3", false, false)
  ClearHint()
end
local TRACK_CLOSE = 0
local TRACK_COMMON_CTRL = 1
local TRACK_COMMON_SPECIAL_TEXT = 2
local TRACK_BAG_ITEM = 3
local TRACK_SHORT_CUT = 4
local TRACK_QUEST_ITEM = 5
local TRACK_NEW_SKILL = 6
local TRACK_SKILL_DLG = 7
local TRACK_NPC_SHOP = 8
local TRACK_MOUNTS = 9
local TRACK_EQUIPED = 10
local TRACK_RUNEPOS = 11
local TRACK_CALLSCRIPT = 12
function Lua_ShowUserGuide(event, wnd, nParam1, nParam2, dir, strControl, strText)
  if not wnd then
    return
  end
  wnd = WindowGuideWindow(wnd)
  if event == TRACK_COMMON_CTRL then
    _Guide_TrackControl(wnd, strControl, nParam1, nParam2, dir)
  elseif event == TRACK_COMMON_SPECIAL_TEXT then
    _Guide_TrackSpecialText(wnd, strControl, strText, dir)
  elseif event == TRACK_BAG_ITEM then
    _Guide_TrackBagItem(wnd, nParam1, nParam2, dir)
  elseif event == TRACK_SHORT_CUT then
    _Guide_TrackShortCutItem(wnd, strControl, nParam1, nParam2, dir)
  elseif event == TRACK_QUEST_ITEM then
    _Guide_TrackQuestItem(wnd, strControl, nParam1, nParam2, dir)
  elseif event == TRACK_NEW_SKILL then
    _Guide_TrackNewSkill(wnd, nParam1, nParam2, dir)
  elseif event == TRACK_SKILL_DLG then
    _Guide_TrackSkill(wnd, nParam1, nParam2, dir)
  elseif event == TRACK_NPC_SHOP then
    _Guide_TrackNpcShop(wnd, nParam1, nParam2, dir)
  elseif event == TRACK_MOUNTS then
    _Guide_TrackMounts(wnd, nParam1, nParam2, dir)
  elseif event == TRACK_EQUIPED then
    _Guide_TrackEquipItem(wnd, nParam1, nParam2, dir)
  elseif event == TRACK_RUNEPOS then
    _Guide_TrackRunePos(wnd, nParam1, nParam2, dir)
  elseif event == TRACK_CALLSCRIPT then
    _Guide_TrackScript(wnd, nParam1)
  end
end
function _Guide_TrackScript(wnd, param1)
  local wnd
  if param1 == 1 then
    wnd = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Stall_btn.StallEff_bg")
    local wndNew = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Stall_btn")
    wnd:SetVisible(true)
    wndNew:SetVisible(true)
  elseif param1 == 2 then
    wnd = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Signature_btn.SignEff_bg")
    wnd:SetVisible(true)
  end
end
function Lua_Guide_HintOpen(msg)
  local GoodsBox = WindowToGoodsBox(msg:get_window())
  local index = GoodsBox:GetItemData(0)
  local strText, strBtnText, luaEvent, guideWindowName = GetUserGuideInfo(index)
  local displayBox = WindowToDisplayBox(WindowSys_Instance:GetWindow(guideWindowName):GetChildByName("DisplayBox_dbox"))
  if displayBox then
    displayBox:ClearAllContent()
    displayBox:InsertBack(strText, 4294967295, 0, 0)
  end
  local Labell = WindowSys_Instance:GetWindow(guideWindowName):GetChildByName("Label1_dlab")
  if Labell then
    local title = "操作小提示"
    if GUIDE_TITLE_DES[index] ~= nil then
      title = GUIDE_TITLE_DES[index]
    end
    Labell:SetProperty("Text", title)
  end
  local btn = WindowSys_Instance:GetWindow(guideWindowName):GetChildByName("Button3_btn")
  if btn then
    btn:SetProperty("Text", strBtnText)
    btn:AddScriptEvent("wm_mouseclick", luaEvent)
    btn:SetCustomUserData(0, index)
  end
  WindowSys_Instance:GetWindow(guideWindowName):SetVisible(true)
end
function OnGuide1Close(msg)
  local parent = msg:get_window():GetParent()
  parent:CloseGuide()
end
function OnGuidelCloseBtnClick(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
end
function Guide_OnBtnDefault1(msg)
  local parent = msg:get_window():GetParent()
  parent:SetProperty("Visible", "false")
  UserGuideUpdateCustomData(1)
  TriggerUserGuide(10, 2, 0)
end
function Guide_OnBtnDefault2(msg)
  local parent = msg:get_window():GetParent()
  parent:CloseGuide()
end
function Guide_OnTimeOut(msg)
  local parent = msg:get_window():GetParent()
  parent:CloseGuide()
end
function Guide_OnSkillDlg(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  DeleteHint(41)
  TriggerUserGuide(10, 41, 0)
end
function Guide_OnAcceptMission(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6001)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6001)
  else
    DeleteHint(USER_GUIDE_GUIDE)
  end
end
function Guide_AddFriend(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  _Guide_ShowHintMission(6002)
end
function Guide_OnAcceptMission1(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  _Guide_ShowHintMission(6003)
end
function Guide_OnAcceptMission2(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6005)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6005)
  else
    DeleteHint(USER_GUIDE_FUBEN)
  end
end
function Guide_OnAcceptMission3(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6007)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6007)
  else
    DeleteHint(USER_GUIDE_FORGE)
  end
end
function Guide_OnAcceptMission4(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6007)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6007)
  else
    DeleteHint(USER_GUIDE_FORGE)
  end
end
function Guide_OnAcceptMission5(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6010)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6010)
  else
    DeleteHint(USER_GUIDE_BLEND)
  end
end
function Guide_OnAcceptMission6(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6011)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6011)
  else
    DeleteHint(USER_GUIDE_CX)
  end
end
function Guide_OnAcceptMission7(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6004)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6004)
  end
end
function Guide_OnAcceptMission8(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6009)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6009)
  else
    DeleteHint(USER_GUIDE_SIGN)
  end
end
function Guide_OnAcceptMission9(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6014)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6014)
  else
    DeleteHint(USER_GUIDE_AMASS)
  end
end
function Guide_OnAcceptMission10(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6018)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6018)
  else
    DeleteHint(USER_GUIDE_GEM)
  end
end
function Guide_OnAcceptMission11(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6024)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6024)
  else
    DeleteHint(USER_GUIDE_FRIEND)
  end
end
function Guide_OnAcceptMission12(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6019)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6019)
  else
    DeleteHint(USER_GUIDE_BLENDSJ)
  end
end
function Guide_OnAcceptMission13(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6020)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6020)
  else
    DeleteHint(USER_GUIDE_BLENDQS)
  end
end
function Guide_OnAcceptMission14(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6021)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6021)
  else
    DeleteHint(USER_GUIDE_BLENDLX)
  end
end
function Guide_OnAcceptMission15(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6022)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6022)
  else
    DeleteHint(USER_GUIDE_BLENDCX)
  end
end
function Guide_OnAcceptMission16(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6023)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6023)
  else
    DeleteHint(USER_GUIDE_BLENDFL)
  end
end
function Guide_OnAcceptMission17(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6027)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6027)
  else
    DeleteHint(USER_GUIDE_RUNESJ)
  end
end
function Guide_OnAcceptMission18(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6028)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6028)
  else
    DeleteHint(USER_GUIDE_RUNEQS)
  end
end
function Guide_OnAcceptMission19(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6029)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6029)
  else
    DeleteHint(USER_GUIDE_RUNELX)
  end
end
function Guide_OnAcceptMission20(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6030)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6030)
  else
    DeleteHint(USER_GUIDE_RUNECX)
  end
end
function Guide_OnAcceptMission21(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6031)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6031)
  else
    DeleteHint(USER_GUIDE_RUNEFL)
  end
end
function Guide_OnAcceptMission22(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6032)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6032)
  else
    DeleteHint(USER_GUIDE_GRZY)
  end
end
function Guide_OnAcceptMission23(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6012)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6012)
  else
    DeleteHint(USER_GUIDE_ZZY)
  end
end
function Guide_OnAcceptMission24(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(4459)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(4459)
  else
    DeleteHint(USER_GUIDE_ldlh)
  end
end
function Guide_OnAcceptMission25(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(6036)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(6036)
  else
    DeleteHint(USER_GUIDE_qqzc)
  end
end
function Guide_OnTradeInviteShow(msg)
  local traderName = GetTradeRequestName()
  if traderName ~= nil then
    Messagebox_Show("CONFIRM_SWAP", traderName)
  end
  DeleteHint(USER_GUIDE_TRADE)
end
function Guide_Tip_TradeInviteShowText(msg)
  local traderName = GetTradeRequestName()
  if type(traderName) ~= "string" then
    traderName = "未知目标"
  end
  local tooltip = WindowToTooltip(msg:get_window():GetToolTipWnd(0))
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(traderName .. "申请与你交易", colorWhite, "", 0, 0)
  Lua_Tip_End(tooltip)
end

