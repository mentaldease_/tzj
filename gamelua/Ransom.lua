if gUI and not gUI.Ransom then
  gUI.Ransom = {}
end
local ITEM_BUYBACK_COST = 300074
local _SG_BarPos = 0
function _OnShow_Ransom_Interface()
  if not gUI.Ransom.Root:IsVisible() then
    gUI.Ransom.Root:SetVisible(true)
    gUI.Ransom.Timer:Restart()
  else
  end
end
function Ransom_Begin_AddItem(IsRember)
  local scrollbar = WindowSys_Instance:GetWindow("Ransom.Ransom_bg.Image_bg.Ransom_cnt.vertSB_")
  if IsRember ~= 1 and scrollbar then
    scrollbar = WindowToScrollBar(scrollbar)
    _SG_BarPos = scrollbar:GetCurrentPos()
  end
  gUI.Ransom.Contain:DeleteAll()
  if not gUI.Ransom.Root:IsVisible() then
    return
  end
end
function Ransom_Update_AddItem(index, IconEquip, Score, Time, IconItem, Name, quilty, quilty2)
  if not gUI.Ransom.Root:IsVisible() then
    return
  end
  local NewItem = gUI.Ransom.Contain:Insert(-1, gUI.Ransom.Template)
  NewItem:SetProperty("WindowName", "RansomData" .. tostring(index))
  NewItem:SetCustomUserData(0, index)
  NewItem:SetProperty("Visible", "true")
  local wndEq = NewItem:GetChildByName("Equip_gbox")
  wndEq = WindowToGoodsBox(wndEq)
  wndEq:SetItemGoods(0, IconEquip, quilty)
  local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.DropItem, index)
  if 0 < ForgeLevel_To_Stars[intenLev] then
    wndEq:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
  else
    wndEq:SetItemStarState(0, 0)
  end
  local wndNe = NewItem:GetChildByName("Need_gbox")
  wndNe = WindowToGoodsBox(wndNe)
  wndNe:SetItemGoods(0, IconItem, quilty2)
  local wndScroe = NewItem:GetChildByName("EquipValue_dlab")
  wndScroe = WindowToLabel(wndScroe)
  wndScroe:SetProperty("Text", tostring(Score))
  local wndTime = NewItem:GetChildByName("Time_dlab")
  wndTime = WindowToLabel(wndTime)
  local timestr = Lua_Tip_ConvertTime(Time * 1000, true)
  wndTime:SetProperty("Text", tostring(timestr))
  local wndPName = NewItem:GetChildByName("Punish_dlab")
  wndPName = WindowToLabel(wndPName)
  if Name == "" then
    Name = "无"
  end
  wndPName:SetProperty("Text", tostring(Name))
end
function Ransom_End_AddItem(IsRember)
  if not gUI.Ransom.Root:IsVisible() then
    return
  end
  if IsRember ~= 1 then
    local scrollbar = WindowSys_Instance:GetWindow("Ransom.Ransom_bg.Image_bg.Ransom_cnt.vertSB_")
    if scrollbar and scrollbar:IsVisible() and _SG_BarPos then
      scrollbar = WindowToScrollBar(scrollbar)
      scrollbar:SetCurrentPos(_SG_BarPos)
    end
  end
end
function UI_Ransom_CloseClick(msg)
  if gUI.Ransom.Root:IsVisible() then
    gUI.Ransom.Root:SetVisible(false)
    gUI.Ransom.Timer:Stop()
  else
  end
end
function UI_Ransom_HelpClick(msg)
end
function UI_Ransom_ItemTipShow(msg)
  local win = msg:get_window()
  if not gUI.Ransom.Contain:IsMouseIn() then
    return
  end
  local tooltip = win:GetToolTipWnd(0)
  Lua_Tip_Item(tooltip, nil, nil, nil, nil, ITEM_BUYBACK_COST)
end
function UI_Ransom_EquipTipShow(msg)
  local win = msg:get_window()
  if not gUI.Ransom.Contain:IsMouseIn() then
    return
  end
  local tooltip = win:GetToolTipWnd(0)
  local WinParent = win:GetParent()
  if WindowToGoodsBox(win):IsItemHasIcon(0) then
    Lua_Tip_Item(tooltip, WindowToGoodsBox(WinParent):GetCustomUserData(0), gUI_GOODSBOX_BAG.DropItem, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(WindowToGoodsBox(win), gUI_GOODSBOX_BAG.DropItem, WindowToGoodsBox(WinParent):GetCustomUserData(0))
  end
end
function UI_Ransom_ConfimClick(msg)
  local win = msg:get_window()
  if not gUI.Ransom.Contain:IsMouseIn() then
    return
  end
  local WinParent = win:GetParent()
  local index = WindowToGoodsBox(WinParent):GetCustomUserData(0)
  DoRansomClick(index)
end
function _Ranson_RansomTimer(timer)
  local itemCount = GetRansomCounts()
  if gUI.Ransom.Root:IsVisible() and itemCount > 0 then
    Ranson_UpdateList()
  end
end
function Script_Ransom_OnEvent(event)
  if "SHOW_RANSOM_INTERFACE" == event then
    _OnShow_Ransom_Interface()
  elseif "RANSOM_BEGIN_ADDITEM" == event then
    Ransom_Begin_AddItem(arg1)
  elseif "RANSOM_UPDATE_ADDITEM" == event then
    Ransom_Update_AddItem(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif "RANSOM_END_ADDITEM" == event then
    Ransom_End_AddItem(arg1)
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    UI_Ransom_CloseClick()
  end
end
function UI_Ransom_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Ransom.Ransom_bg.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function Script_Ransom_OnLoad()
  gUI.Ransom.Root = WindowSys_Instance:GetWindow("Ransom")
  gUI.Ransom.Contain = WindowToContainer(WindowSys_Instance:GetWindow("Ransom.Ransom_bg.Image_bg.Ransom_cnt"))
  gUI.Ransom.Template = WindowSys_Instance:GetWindow("Ransom.Ransom_bg.Image_bg.Template_bg")
  gUI.Ransom.Timer = TimerSys_Instance:CreateTimerObject("RansomTimer", 60, -1, "_Ranson_RansomTimer", false, false)
end
function Script_Ransom_OnHide()
end

end
