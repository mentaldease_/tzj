if gUI and not gUI.skillgift then
  gUI.skillgift = {}
end
local _SkillGift_Rune_Size = 5
local _SkillGift_Rune_Index = -1
local _SkillGift_Rune_SelectPage = 0
local _SkillGift_Rune_MaxLv = 5
local _SkillGift_Rune_Count = 0
local _SkillGift_Rune_SelectedRune = -1
local _SkillGift_Rune_CurrentPage = 0
local _SG_BarPos, _SG_RuneBarPos
local UG_SKILL_PLACERUNE = 13
local _SkillGift_Rune_LockImg = {
  [1] = "",
  [2] = "",
  [3] = "",
  [4] = "",
  [5] = ""
}
local _SkillGift_Rune_NormImg = {
  [1] = "UiIamge021:Line_01",
  [2] = "UiIamge021:Line_02",
  [3] = "UiIamge021:Line_03",
  [4] = "UiIamge021:Line_04",
  [5] = "UiIamge021:Line_05"
}
local _SkillGift_Rune_SelImg = {
  [1] = "UiIamge012:Mosaic_Use",
  [2] = "UiIamge012:Mosaic_Use",
  [3] = "UiIamge012:Mosaic_Use",
  [4] = "UiIamge012:Mosaic_Use",
  [5] = "UiIamge012:Mosaic_Use"
}
local _SkillGift_NimbusName = {
  [0] = "赤炎魔熊罴",
  [1] = "九幽紫电鹫",
  [2] = "碧海麒麟兽",
  [3] = "万花灵雀皇",
  [4] = "毒玉巡风虎"
}
local _SkillGift_NimbusLvUp = {
  [0] = {
    0,
    1,
    2,
    3,
    6,
    10
  },
  [1] = {
    0,
    1,
    2,
    3,
    6,
    10
  },
  [2] = {
    0,
    1,
    2,
    3,
    6,
    10
  },
  [3] = {
    0,
    1,
    2,
    3,
    6,
    10
  },
  [4] = {
    0,
    1,
    2,
    3,
    6,
    10
  }
}
local _SkillGift_Rune_LvImage = {
  [1] = "UiIamge013:Image_LackStar",
  [2] = "UiIamge013:Image_Star"
}
local _SkillGift_NimbusShowBoxName = "NimbusAvatar"
local _SkillGift_OpenLv = 30
function _Gift_UpdateSkillpart(index, icon, skill_id, bStudy, lv, maxlv, skill_name, skillType)
  local tmpl = WindowSys_Instance:GetWindow("Skill.SkillTemp_bg")
  local newitem = gUI.skillgift.skillContainer:Insert(-1, tmpl)
  newitem:SetProperty("WindowName", "SkillData" .. tostring(index))
  newitem:SetCustomUserData(0, skill_id)
  newitem:SetProperty("Visible", "true")
  local wnd = newitem:GetChildByName("Skill_gbox")
  wnd = WindowToGoodsBox(wnd)
  wnd:ResetAllGoods(true)
  wnd:SetItemData(0, skill_id)
  if lv == maxlv then
    wnd:SetItemGoods(0, icon, 4)
  else
    wnd:SetItemGoods(0, icon, -1)
  end
  if lv > 0 then
    wnd:SetItemEnable(0, true)
    wnd:SetProperty("ItemCanDrag", "true")
  else
    wnd:SetItemEnable(0, false)
    wnd:SetProperty("ItemCanDrag", "false")
  end
  if lv > 0 then
    newitem:GetChildByName("Level_dlab"):SetProperty("FontColor", "0xFFFFFFFF")
    newitem:GetChildByName("Level_dlab"):SetProperty("Text", tostring(lv) .. "级")
  else
    newitem:GetChildByName("Level_dlab"):SetProperty("Text", "")
  end
  wnd = newitem:GetChildByName("Course_btn")
  if skillType == 0 then
    wnd:SetVisible(false)
  elseif lv <= 0 and bStudy then
    wnd:SetCustomUserData(0, skill_id)
    wnd:SetEnable(true)
  elseif lv > 0 and bStudy then
    wnd:SetCustomUserData(0, skill_id)
    wnd:SetEnable(true)
  else
    wnd:SetCustomUserData(0, skill_id)
    wnd:SetEnable(false)
  end
  if skill_name then
    if lv > 0 then
      newitem:GetChildByName("Name_dlab"):SetProperty("FontColor", "0xFFFFFFFF")
    else
      newitem:GetChildByName("Name_dlab"):SetProperty("FontColor", "0xFF808080")
    end
    newitem:GetChildByName("Name_dlab"):SetProperty("Text", tostring(skill_name))
  end
end
function _Gift_UpdateRunepart(index, icon, runeId, bOpen)
  local partname = "Symbol" .. index .. "_gbox"
  local root = gUI.skillgift.runeStoneRoot
  local removeName = "Symbol" .. index .. "_btn"
  local wndRemove = root:GetChildByName(removeName)
  local wndparent = root:GetChildByName(partname)
  local wnd = WindowToGoodsBox(wndparent)
  wnd:ResetAllGoods(true)
  if icon ~= "" then
    wnd:SetItemGoods(0, icon, -1)
    wndRemove:SetVisible(true)
  end
  if bOpen then
    if runeId > 0 then
      wnd:SetCustomUserData(1, runeId)
      wndRemove:SetCustomUserData(0, runeId)
      wndRemove:SetVisible(true)
    else
      wnd:SetCustomUserData(1, 0)
      wndRemove:SetCustomUserData(0, 0)
      wndRemove:SetVisible(false)
    end
  else
    wnd:SetItemGoods(0, "UiIamge013:Image_BagFrameLock", -1)
    wnd:SetCustomUserData(1, -1)
    wndRemove:SetVisible(false)
    wndRemove:SetCustomUserData(0, -1)
  end
end
function _Gift_UpdateNimbusInfo(index, name, value)
  local partname = "Attribute" .. index .. "_dlab"
  local root = gUI.skillgift.SPinfo
  local wndparent = root:GetChildByName(partname)
  if value > 0 and name ~= "" then
    local str = string.format("%s +%d", name, value)
    wndparent:SetProperty("Text", str)
  else
    wndparent:SetProperty("Text", "")
  end
end
function _Gift_UpdateRuneState(index)
  local partname = "Symbol" .. index .. "_gbox"
  local pic = "Symbol" .. index .. "_pic"
  local root = gUI.skillgift.runeStoneRoot
  local wndparent = root:GetChildByName(partname)
  local wnd = WindowToGoodsBox(wndparent)
  local wndPic = root:GetChildByName(pic)
  if index ~= _SkillGift_Rune_Index + 1 then
    wnd:ClearSelectedItem()
    if wnd:GetCustomUserData(1) >= 0 then
      wndPic:SetProperty("BackImage", _SkillGift_Rune_NormImg[index])
    else
      wndPic:SetProperty("BackImage", _SkillGift_Rune_LockImg[index])
    end
  end
end
function _Gift_Show()
  local root = gUI.skillgift.root
  if root:IsVisible() then
    _SkillGift_Rune_Index = -1
    root:SetProperty("Visible", "false")
    gUI.skillgift.runeRoot:SetProperty("Visible", "false")
    _SkillGift_Rune_SelectPage = _SkillGift_Rune_CurrentPage
  else
    root:SetProperty("Visible", "true")
    local win = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Skill_btn.Skill_Eff")
    win:SetVisible(false)
    local _, _, _, _, lv = GetPlaySelfProp(4)
    if lv < _SkillGift_OpenLv then
      gUI.skillgift.ExpandBtn:SetProperty("Visible", "false")
    else
      gUI.skillgift.ExpandBtn:SetProperty("Visible", "true")
      _Gift_ShowRune()
    end
    Lua_Gift_UpdateAll()
  end
end
function _Gift_ShowRune()
  gUI.skillgift.runeRoot:SetProperty("Visible", "true")
  local currentPage, maxPage = GetRunePageInfo()
  _SkillGift_Rune_SelectPage = currentPage
  _SkillGift_Rune_CurrentPage = currentPage
  if _SkillGift_Rune_SelectPage == 0 then
    gUI.skillgift.runeOnePage:SetStatus("selected")
    if maxPage >= 1 then
      gUI.skillgift.runeTwoPage:SetStatus("normal")
    end
  else
    if maxPage >= 1 then
      gUI.skillgift.runeTwoPage:SetStatus("selected")
    end
    gUI.skillgift.runeOnePage:SetStatus("normal")
  end
  if maxPage < 1 then
    gUI.skillgift.runeTwoPage:SetEnable(false)
  else
    gUI.skillgift.runeTwoPage:SetEnable(true)
  end
  gUI.skillgift.runeChangePage:SetVisible(false)
  local pageName = string.format("第%d页符文(当前使用)", _SkillGift_Rune_CurrentPage + 1)
  gUI.skillgift.runePageName:SetProperty("Text", pageName)
  local bState = gUI.skillgift.runeCheckOpenBtn:IsChecked()
  _Gift_UpdateRune(bState)
  _Gift_UpdateRuneStone()
end
function _Gift_SPShow()
  local root = gUI.skillgift.SProot
  if root:IsVisible() then
    gUI.skillgift.showbox:hide()
    root:SetProperty("Visible", "false")
  else
    gUI.skillgift.showbox:show()
    root:SetProperty("Visible", "true")
    Lua_Gift_UpdateSPUI()
  end
end
function _Gift_CanActiveRuneLevel(runeID)
  local bActive = true
  local name, desc, lv, needNimbus, needMoney, needlv = GetRuneTipInfo(runeID)
  if needlv > 0 then
    local _, _, _, _, lv = GetPlaySelfProp(4)
    if needlv > lv then
      bActive = false
    end
  end
  return bActive, needlv
end
function _Gift_UpdateCommonSkill(skillType)
  local skillCount = GetSkillCountByType(skillType)
  local icon, skill_id, bStudy, lv
  for i = 0, skillCount do
    icon, skill_id, bStudy, lv, maxlv, skill_name = GetCharSpellInfo(skillType, i)
    if icon then
      _Gift_UpdateSkillpart(i, icon, skill_id, bStudy, lv, maxlv, skill_name, skillType)
    end
    if skillCount == i and skillType == 1 then
      TriggerUserGuide(48, skillCount, -1)
    end
  end
  local scrollbar = WindowSys_Instance:GetWindow("Skill.Skill_bg.Center_bg.Skill_cnt.vertSB_")
  if scrollbar and scrollbar:IsVisible() and _SG_BarPos then
    scrollbar = WindowToScrollBar(scrollbar)
    scrollbar:SetCurrentPos(_SG_BarPos)
  end
end
function _Gift_UpdateRune(bShow)
  gUI.skillgift.runeContainer:DeleteAll()
  _SkillGift_Rune_Count = 0
  _SkillGift_Rune_SelectedRune = -1
  local runeList = GetCharRuneInfo(_SkillGift_Rune_SelectPage, bShow)
  if runeList[0] == nil then
    return
  end
  for i = 0, table.maxn(runeList) do
    local value = runeList[i]
    local tmpl = WindowSys_Instance:GetWindow("Skill.RuneTemp_btn")
    local newitem = gUI.skillgift.runeContainer:Insert(-1, tmpl)
    newitem:SetProperty("WindowName", "SkillRune" .. tostring(i))
    newitem:SetCustomUserData(0, value.Id)
    newitem:SetProperty("Visible", "true")
    local wnd = newitem:GetChildByName("Symbol_gbox")
    local runeLv = value.Lv
    wnd = WindowToGoodsBox(wnd)
    wnd:ResetAllGoods(true)
    wnd:SetItemData(0, value.Id)
    wnd:SetItemGoods(0, value.Icon, -1)
    newitem:GetChildByName("Mosaic_btn"):SetCustomUserData(0, value.Id)
    newitem:GetChildByName("Mosaic_btn"):SetCustomUserData(1, runeLv)
    newitem:GetChildByName("Name_slab"):SetProperty("Text", tostring(value.Name))
    if runeLv > 0 then
      wnd:SetItemEnable(0, true)
      newitem:GetChildByName("Name_slab"):SetProperty("FontColor", "0xFFFFFFFF")
      if not value.Insert then
        newitem:GetChildByName("Mosaic_btn"):SetProperty("Text", "{#C^0xFF00FFFF^镶嵌}")
        newitem:GetChildByName("Mosaic_btn"):AddScriptEvent("wm_mouseclick", "UI_Gift_RuneSettingByRuneId")
      end
      newitem:GetChildByName("NeedLev_slab"):SetProperty("Visible", "false")
    else
      wnd:SetItemEnable(0, false)
      newitem:GetChildByName("Name_slab"):SetProperty("FontColor", "0xFF808080")
      newitem:GetChildByName("Mosaic_btn"):AddScriptEvent("wm_mouseclick", "UI_Gift_RuneActiveByRuneId")
      local canActive, needLev = _Gift_CanActiveRuneLevel(value.Id)
      local color = "0xFF808080"
      if canActive then
        newitem:GetChildByName("Mosaic_btn"):SetEnable(true)
        newitem:GetChildByName("Mosaic_btn"):SetProperty("Text", "{#C^0xFFFFFFFF^激活}")
      else
        color = "0xFFFF0611"
        newitem:GetChildByName("Mosaic_btn"):SetEnable(false)
        newitem:GetChildByName("Mosaic_btn"):SetProperty("Text", "{#C^0xFFC0C0C0^激活}")
      end
      local needLevStr = string.format("{#C^%s^激活等级:%d级}", color, needLev)
      if needLev > 0 then
        newitem:GetChildByName("NeedLev_slab"):SetProperty("Visible", "true")
        newitem:GetChildByName("NeedLev_slab"):SetProperty("Text", needLevStr)
      else
        newitem:GetChildByName("NeedLev_slab"):SetProperty("Visible", "false")
      end
    end
    newitem:SetCustomUserData(3, runeLv)
    if value.Insert then
      newitem:GetChildByName("Insert_slab"):SetProperty("Visible", "true")
      newitem:GetChildByName("Mosaic_btn"):SetProperty("Text", "{#C^0xFFFFD414^摘除}")
      newitem:GetChildByName("Mosaic_btn"):AddScriptEvent("wm_mouseclick", "UI_Gift_RuneRemoveByRuneId")
    else
      newitem:GetChildByName("Insert_slab"):SetProperty("Visible", "false")
    end
    _SkillGift_Rune_Count = _SkillGift_Rune_Count + 1
    newitem:SetCustomUserData(1, _SkillGift_Rune_Count)
  end
  local scrollbar = WindowSys_Instance:GetWindow("Skill.Rune_bg.Right_bg.RuneList_cnt.vertSB_")
  if scrollbar and scrollbar:IsVisible() and _SG_RuneBarPos then
    scrollbar = WindowToScrollBar(scrollbar)
    scrollbar:SetCurrentPos(_SG_RuneBarPos)
  end
end
function _Gift_UpdateRuneLine()
  local root = gUI.skillgift.runeStoneRoot
  for i = 1, _SkillGift_Rune_Size do
    local partname = "Symbol" .. i .. "_gbox"
    local nextIndex = i + 1
    if nextIndex > _SkillGift_Rune_Size then
      nextIndex = 1
    end
    local partname2 = "Symbol" .. nextIndex .. "_gbox"
    local wndparent = root:GetChildByName(partname)
    local wnd = WindowToGoodsBox(wndparent)
    local wndparent2 = root:GetChildByName(partname2)
    local wnd2 = WindowToGoodsBox(wndparent2)
    local runeId = wnd:GetCustomUserData(1)
    local runeId2 = wnd2:GetCustomUserData(1)
    if tonumber(runeId) > 0 and tonumber(runeId2) > 0 then
      root:GetChildByName("Line" .. i .. "_bg"):SetVisible(true)
    else
      root:GetChildByName("Line" .. i .. "_bg"):SetVisible(false)
    end
  end
end
function _Gift_UpdateRuneStone()
  local runeId, icon, bOpen
  for i = 1, _SkillGift_Rune_Size do
    runeId, icon, bOpen = GetCharRuneStoneInfo(_SkillGift_Rune_SelectPage, i - 1)
    _Gift_UpdateRunepart(i, icon, runeId, bOpen)
  end
  _Gift_UpdateRuneLine()
end
function _Gift_IsNewSkill(skillId)
end
function _Gift_GetNewSkillCnt()
end
function _Gift_UpdateRuneAll()
  if _SkillGift_Rune_SelectPage == 0 then
    gUI.skillgift.runeOnePage:SetStatus("selected")
    if gUI.skillgift.runeTwoPage:IsEnable() then
      gUI.skillgift.runeTwoPage:SetStatus("normal")
    end
  else
    if gUI.skillgift.runeTwoPage:IsEnable() then
      gUI.skillgift.runeTwoPage:SetStatus("selected")
    end
    gUI.skillgift.runeOnePage:SetStatus("normal")
  end
  if gUI.skillgift.runeRoot:IsVisible() then
    local bState = gUI.skillgift.runeCheckOpenBtn:IsChecked()
    _Gift_UpdateRune(bState)
    _Gift_UpdateRuneStone()
    gUI.skillgift.extractBtn:SetEnable(false)
  end
end
function _Gift_RemoveRuneByRuneId(runeID)
  local root = gUI.skillgift.runeStoneRoot
  for i = 1, _SkillGift_Rune_Size do
    local partname = "Symbol" .. i .. "_gbox"
    local wndparent = root:GetChildByName(partname)
    local wnd = WindowToGoodsBox(wndparent)
    if wnd:GetCustomUserData(1) == runeID then
      RemoveRune(_SkillGift_Rune_SelectPage, wnd:GetCustomUserData(0) + 1)
    end
  end
end
function _Gift_UpdateRuneOnly()
  _SG_RuneBarPos = nil
  Lua_Gift_UpdateSkill()
  local bState = gUI.skillgift.runeCheckOpenBtn:IsChecked()
  _Gift_UpdateRune(bState)
  _Gift_UpdateRuneStone()
  gUI.skillgift.extractBtn:SetEnable(false)
end
function _OnGift_UpdateMoney(arg1, arg2)
  if not arg1 then
    _, arg1, _ = GetBankAndBagMoney()
  end
  gUI.skillgift.coin:SetProperty("Text", Lua_UI_Money2String(arg1))
  arg2 = arg2 or -1
  if arg2 == -1 then
    return
  elseif arg2 > 0 then
    WorldStage:playSoundByID(1)
  elseif arg2 < 0 then
    WorldStage:playSoundByID(2)
  else
    return
  end
end
function _OnGift_UpdateMoneyBind(arg1, arg2)
  if not arg1 then
    _, _, arg1 = GetBankAndBagMoney()
  end
  gUI.skillgift.coinBind:SetProperty("Text", Lua_UI_Money2String(arg1))
  arg2 = arg2 or -1
  if arg2 == -1 then
    return
  elseif arg2 > 0 then
    WorldStage:playSoundByID(1)
  elseif arg2 < 0 then
    WorldStage:playSoundByID(2)
  else
    return
  end
end
function _OnGift_ConfirmUseRune(page, runeId)
  Messagebox_Show("COMFIRM_RUNE", page, runeId)
end
function _OnGift_RemoveRune()
  _Gift_UpdateRuneOnly()
end
function _OnGift_InsertRune()
  _Gift_UpdateRuneOnly()
end
function _OnGift_SpellEnhanceResult(skillId, lv)
  if gUI.skillgift.root:IsVisible() then
    Lua_Gift_UpdateSkill()
    if lv == 1 then
      local wnd = WindowToContainer(WindowSys_Instance:GetWindow("Skill.Skill_bg.Center_bg.Skill_cnt"))
      wnd:SizeSelf()
      local item = wnd:GetChildByData0(skillId)
      if item then
        local goodsBoxNew = WindowToGoodsBox(item:GetChildByName("Skill_gbox"))
        local speeds = 0.01
        local pic = goodsBoxNew:GetItemIcon(0)
        local effect = "efxc_ui_hint02"
        local sourceX = goodsBoxNew:GetItemRect(0):get_left()
        local sourceY = goodsBoxNew:GetItemRect(0):get_top()
        ActBar_SetSourceData(sourceX, sourceY, speeds, pic, effect, skillId)
      end
    end
  end
end
function Lua_Gift_UpdateSkillPage(bRemember)
  if bRemember then
    local scrollbar = WindowSys_Instance:GetWindow("Skill.Skill_bg.Center_bg.Skill_cnt.vertSB_")
    if scrollbar then
      scrollbar = WindowToScrollBar(scrollbar)
      _SG_BarPos = scrollbar:GetCurrentPos()
    end
  end
  gUI.skillgift.skillContainer:DeleteAll()
  local tablectrl = WindowToTableCtrl(gUI.skillgift.skillTabctrl)
  if tablectrl:GetSelectItem() == 0 then
    _Gift_UpdateCommonSkill(1)
  elseif tablectrl:GetSelectItem() == 1 then
    _Gift_UpdateCommonSkill(0)
  elseif tablectrl:GetSelectItem() == 2 then
  elseif tablectrl:GetSelectItem() == 3 then
  end
end
function Lua_Gift_UpdateSkill()
  local Nimbus, MaxNimbus, SkillPoint = GetPlaySelfProp(9)
  gUI.skillgift.nimbus:SetProperty("Text", tostring(Nimbus))
  if SkillPoint > 0 then
    gUI.skillgift.spSlab:SetProperty("Visible", "true")
    gUI.skillgift.sp:SetProperty("Visible", "true")
    gUI.skillgift.sp:SetProperty("Text", tostring(SkillPoint))
  else
    gUI.skillgift.spSlab:SetProperty("Visible", "false")
    gUI.skillgift.sp:SetProperty("Visible", "false")
  end
  Lua_Gift_UpdateSkillPage(true)
end
function Lua_Gift_UpdateAll()
  if gUI.skillgift.root:IsVisible() then
    local currentPage, maxPage = GetRunePageInfo()
    _SkillGift_Rune_CurrentPage = currentPage
    _SkillGift_Rune_SelectPage = currentPage
    local pageName = string.format("第%d页符文", currentPage + 1)
    if _SkillGift_Rune_SelectPage ~= currentPage then
      gUI.skillgift.runeChangePage:SetVisible(true)
    else
      gUI.skillgift.runeChangePage:SetVisible(false)
      pageName = pageName .. "(当前使用)"
    end
    if maxPage < 1 then
      gUI.skillgift.runeTwoPage:SetEnable(false)
    else
      gUI.skillgift.runeTwoPage:SetEnable(true)
    end
    gUI.skillgift.runePageName:SetProperty("Text", pageName)
    Lua_Gift_UpdateSkill()
    _Gift_UpdateRuneAll()
  else
    local skillCount = GetSkillCountByType(1)
    local icon, skill_id, bStudy, lv
    for i = 0, skillCount do
      icon, skill_id, bStudy, lv, maxlv, skill_name = GetCharSpellInfo(1, i)
      if bStudy and icon then
        local win = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Skill_btn.Skill_Eff")
        win:SetVisible(true)
        return
      end
    end
  end
end
function Lua_Rune_Active()
  gUI.skillgift.root:SetProperty("Visible", "true")
  gUI.skillgift.runeRoot:SetProperty("Visible", "true")
  Lua_Gift_UpdateAll()
end
function Lua_Gift_UpdateSPUI()
  local root = gUI.skillgift.SProot
  if root:IsVisible() then
    local _, menpai = GetMyPlayerStaticInfo()
    local lv, yuanqi, maxyuanqi, spModle, spCombat, infoList = GetNimbusInfo(_SkillGift_NimbusLvUp[menpai][1], _SkillGift_NimbusLvUp[menpai][2], _SkillGift_NimbusLvUp[menpai][3], _SkillGift_NimbusLvUp[menpai][4], _SkillGift_NimbusLvUp[menpai][5], _SkillGift_NimbusLvUp[menpai][6])
    local strname = string.format("%d级  %s", lv, _SkillGift_NimbusName[menpai])
    gUI.skillgift.SPname:SetProperty("Text", strname)
    gUI.skillgift.SPpbar:SetProperty("Progress", tostring(yuanqi / maxyuanqi))
    gUI.skillgift.SPdlab:SetProperty("Text", tostring(yuanqi) .. "/" .. tostring(maxyuanqi))
    gUI.skillgift.SPinfo = WindowSys_Instance:GetWindow("Basicmodel.Basicmodel_bg.Center_bg")
    if not gUI.skillgift.showbox:isAvatarExist("Nimbus" .. tostring(spModle)) then
      gUI.skillgift.showbox:onCharacterReset()
      gUI.skillgift.showbox:revertCamera()
      gUI.skillgift.showbox:createMountAvatar("Nimbus" .. tostring(spModle))
    end
    gUI.skillgift.showbox:setCurrentAvatar("Nimbus" .. tostring(spModle))
    gUI.skillgift.SPskillBox:ResetAllGoods(true)
    for index = 0, 5 do
      local skill_id, skill_icon = GetTemporarySkillInfo(spCombat, index)
      if skill_id then
        gUI.skillgift.SPskillBox:SetItemGoods(index, skill_icon, -1)
        gUI.skillgift.SPskillBox:SetItemData(index, skill_id)
      end
    end
    for index, item in pairs(infoList) do
      _Gift_UpdateNimbusInfo(index + 1, item.name, item.value)
    end
  end
end
function UI_Gift_Show(msg)
  _Gift_Show()
  WorldStage:playSoundByID(36)
end
function UI_Skill_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Skill.Skill_bg.Up_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Gift_ShowSP(msg)
  _Gift_SPShow()
end
function UI_Gift_ShowSkillToolTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  if not gUI.skillgift.skillContainer:IsMouseIn() then
    return
  end
  local skill_id = wnd:GetItemData(0)
  if not skill_id then
    return
  end
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local currentlv, bStudy, maxlv = GetPlayerSkillLv(skill_id)
  if currentlv <= 0 and bStudy or currentlv <= 0 and maxlv ~= -1 then
    Lua_Tip_Skill(tooltip, skill_id, currentlv, true, true)
  elseif currentlv > 0 and currentlv < maxlv then
    Lua_Tip_Skill(tooltip, skill_id, currentlv, true, false)
    Lua_Tip_ShowSkilCompare(wnd, skill_id, currentlv + 1)
  elseif currentlv == maxlv then
    Lua_Tip_Skill(tooltip, skill_id, currentlv, true, false)
  else
    Lua_Tip_Skill(tooltip, skill_id, currentlv, false, false)
  end
end
function UI_Gift_ShowRuneToolTip(msg)
  local wnd = msg:get_window()
  local runeid = wnd:GetCustomUserData(1)
  local index = wnd:GetCustomUserData(0)
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local name = ""
  Lua_Tip_Begin(tooltip)
  if runeid == 0 then
    name = "已开启"
    tooltip:InsertLeftText(name, "FFFAFAFA", "", 0, 0)
  elseif runeid == -1 then
    name = string.format("%d级时开启", index * 5 + 30)
    tooltip:InsertLeftText(name, "FFFAFAFA", "", 0, 0)
  else
    local name, desc, lv = GetRuneTipInfo(runeid)
    local itemid = tooltip:InsertLeftText(tostring(name), "FFFAFAFA", "kaiti_13", 0, 0)
    tooltip:InsertLeftText(desc, "FFFAFAFA", "", 0, 0)
  end
  Lua_Tip_End(tooltip)
end
function UI_Gift_ShowOtherRuneToolTip(msg)
  local wnd = msg:get_window()
  local runeid = wnd:GetCustomUserData(1)
  local index = wnd:GetCustomUserData(0)
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local name = ""
  Lua_Tip_Begin(tooltip)
  if runeid == 0 then
    name = "已开启"
    tooltip:InsertLeftText(name, "FFFAFAFA", "", 0, 0)
  elseif runeid == -1 then
    name = string.format("%d级时开启", index * 5 + 30)
    tooltip:InsertLeftText(name, "FFFAFAFA", "", 0, 0)
  else
    local name, desc = GetOtherRuneTipInfo(runeid)
    local itemid = tooltip:InsertLeftText(tostring(name), "FFFAFAFA", "kaiti_13", 0, 0)
    tooltip:InsertLeftText(desc, "FFFAFAFA", "", 0, 0)
  end
  Lua_Tip_End(tooltip)
end
function UI_Gift_ShowLearnSkillToolTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  if not gUI.skillgift.skillContainer:IsMouseIn() then
    return
  end
  local skill_id = wnd:GetCustomUserData(0)
  if not skill_id then
    return
  end
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local currentlv, bStudy, maxlv = GetPlayerSkillLv(skill_id)
  if currentlv < maxlv then
    Lua_Tip_Begin(tooltip)
    local needlv, blv, needSkill, needSkillLv, bSkill, needSkillP, bSkillP, needNimbus, bNimbus, needMoney, bMoney, StudyType, ItemName, nItemCount = GetSkillLVCondition(skill_id)
    if needlv > 0 then
      if blv then
        tooltip:InsertLeftText("需要等级:" .. tostring(needlv) .. "级", colorWhite, "songti_9", 0, 2)
      else
        tooltip:InsertLeftText("需要等级:" .. tostring(needlv) .. "级", colorRed, "songti_9", 0, 2)
      end
    end
    if needSkillP > 0 then
      if bSkillP then
        tooltip:InsertLeftText("需要前置技能点:" .. tostring(needSkillP) .. "点", colorWhite, "songti_9", 0, 2)
      else
        tooltip:InsertLeftText("需要前置技能点:" .. tostring(needSkillP) .. "点", colorRed, "songti_9", 0, 2)
      end
    end
    if needSkill ~= "" and needSkillLv > 0 then
      if bSkill then
        tooltip:InsertLeftText("需要前置技能:" .. tostring(needSkill) .. tostring(needSkillLv) .. "级", colorWhite, "songti_9", 0, 2)
      else
        tooltip:InsertLeftText("需要前置技能:" .. tostring(needSkill) .. tostring(needSkillLv) .. "级", colorRed, "songti_9", 0, 2)
      end
    end
    if needNimbus > 0 then
      if bNimbus then
        tooltip:InsertLeftText("需消耗精魄值:" .. tostring(needNimbus) .. "点", colorWhite, "songti_9", 0, 2)
      else
        tooltip:InsertLeftText("需消耗精魄值:" .. tostring(needNimbus) .. "点", colorRed, "songti_9", 0, 2)
      end
    end
    if needMoney > 0 then
      if bMoney then
        itemid = tooltip:InsertLeftText("需消耗金钱:", colorWhite, "songti_9", 0, 2)
      else
        itemid = tooltip:InsertLeftText("需消耗金钱:", colorRed, "songti_9", 0, 2)
      end
      _Tip_ShowMoney(tooltip, needMoney, itemid)
    end
    if StudyType and ItemName then
      if nItemCount > 0 then
        tooltip:InsertLeftText("需消耗技能书:" .. ItemName, colorWhite, "songti_9", 0, 2)
      else
        tooltip:InsertLeftText("需消耗技能书:" .. ItemName, colorRed, "songti_9", 0, 2)
      end
    end
    Lua_Tip_End(tooltip)
  elseif currentlv == maxlv then
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText("已经达到最大等级", "FFFAFAFA", "", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function UI_Gift_ShowActiveRuneToolTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  if not gUI.skillgift.runeContainer:IsMouseIn() then
    return
  end
  local runeLv = wnd:GetCustomUserData(1)
  if runeLv == 0 then
    local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
    local runeId = wnd:GetCustomUserData(0)
    local name, desc, lv, needNimbus, needMoney, needlv = GetRuneTipInfo(runeId)
    if needNimbus <= 0 and needMoney <= 0 and needlv <= 0 then
      return
    end
    Lua_Tip_Begin(tooltip)
    if needNimbus > 0 then
      local Nimbus = GetPlaySelfProp(9)
      if needNimbus <= Nimbus then
        tooltip:InsertLeftText("需消耗精魄值:" .. tostring(needNimbus) .. "点", colorWhite, "songti_9", 0, 2)
      else
        tooltip:InsertLeftText("需消耗精魄值:" .. tostring(needNimbus) .. "点", colorRed, "songti_9", 0, 2)
      end
    end
    if needMoney > 0 then
      local _, money, bmoney = GetBankAndBagMoney()
      local color1 = colorWhite
      if needMoney > money + bmoney then
        color1 = colorRed
      end
      itemid = tooltip:InsertLeftText("需消耗金钱:", tostring(color1), "songti_9", 0, 2)
      _Tip_ShowMoney(tooltip, needMoney, itemid, color1 == colorRed)
    end
    if needlv > 0 then
      local _, _, _, _, lv = GetPlaySelfProp(4)
      if needlv <= lv then
        tooltip:InsertLeftText("需要等级:" .. tostring(needlv) .. "级", colorWhite, "songti_9", 0, 2)
      else
        tooltip:InsertLeftText("需要等级:" .. tostring(needlv) .. "级", colorRed, "songti_9", 0, 2)
      end
    end
    Lua_Tip_End(tooltip)
  end
end
function UI_Gift_ShowRuneListToolTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local rune_id = wnd:GetItemData(0)
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local name, desc, lv = GetRuneTipInfo(rune_id)
  Lua_Tip_Begin(tooltip)
  local itemid = tooltip:InsertLeftText(tostring(name), "FFFAFAFA", "kaiti_13", 0, 0)
  tooltip:InsertLeftText(desc, "FFFAFAFA", "", 0, 0)
  Lua_Tip_End(tooltip)
end
function UI_Gift_LearnSkill(msg)
  if not gUI.skillgift.skillContainer:IsMouseIn() then
    return
  end
  local wnd = msg:get_window()
  local skill_id = wnd:GetCustomUserData(0)
  LearnSkill(skill_id)
end
function UI_Gift_RuneDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_win = to_win:GetDragItemParent()
  local runeId = from_win:GetItemData(0)
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "RuneList" then
    return
  else
    Lua_Chat_ShowOSD("SPELL_NOT_ITEM")
    return
  end
end
function UI_Gift_RuneLClick(msg)
  local wnd = msg:get_window()
  gUI.skillgift.extractBtn:SetEnable(true)
  _SkillGift_Rune_Index = wnd:GetCustomUserData(0)
  local pic = "Symbol" .. tostring(_SkillGift_Rune_Index + 1) .. "_pic"
  local root = gUI.skillgift.runeStoneRoot
  local wndPic = root:GetChildByName(pic)
  wndPic:SetProperty("BackImage", _SkillGift_Rune_SelImg[_SkillGift_Rune_Index + 1])
  for i = 1, _SkillGift_Rune_Size do
    _Gift_UpdateRuneState(i)
  end
end
function UI_Gift_RemoveRune(msg)
  if _SkillGift_Rune_Index ~= -1 then
    RemoveRune(_SkillGift_Rune_SelectPage, _SkillGift_Rune_Index + 1)
  end
end
function UI_Gift_TabCtrlClick(msg)
  _SG_BarPos = nil
  Lua_Gift_UpdateSkillPage(false)
end
function UI_Gift_ShowRune(msg)
  local bVisible = gUI.skillgift.runeRoot:IsVisible()
  if bVisible then
    gUI.skillgift.runeRoot:SetProperty("Visible", "false")
    _SkillGift_Rune_SelectPage = _SkillGift_Rune_CurrentPage
  else
    _Gift_ShowRune()
  end
end
function UI_Gift_CloseRune(msg)
  gUI.skillgift.runeRoot:SetProperty("Visible", "false")
  _SkillGift_Rune_SelectPage = _SkillGift_Rune_CurrentPage
end
function UI_Gift_CheckRuneOpen(msg)
  _SG_RuneBarPos = nil
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  _Gift_UpdateRune(bState)
end
function UI_Gift_RuneSelected(msg)
  local item = msg:get_window()
  if item:GetProperty("ClassName") == "GoodsBox" then
    item = item:GetParent()
  end
  if not item then
    return
  end
  local index = item:GetCustomUserData(1)
  local itemTmp = gUI.skillgift.runeContainer:GetChildByData1(index)
  if itemTmp:GetCustomUserData(3) > 0 then
    for i = 1, _SkillGift_Rune_Count do
      local itemTmp = gUI.skillgift.runeContainer:GetChildByData1(i)
      if itemTmp then
        local wnd = WindowToButton(itemTmp)
        if itemTmp:GetCustomUserData(1) == index then
          wnd:SetStatus("selected")
          gUI.skillgift.runeMosaicBtn:SetEnable(true)
          _SkillGift_Rune_SelectedRune = i
          local GoodsId = itemTmp:GetCustomUserData(0)
          UserGuideCompleteEvent(UG_SKILL_PLACERUNE, tonumber(GoodsId))
        else
          wnd:SetStatus("normal")
        end
      end
    end
  end
end
function UI_Gift_RuneSetting(msg)
  local itemTmp = gUI.skillgift.runeContainer:GetChildByData1(_SkillGift_Rune_SelectedRune)
  if itemTmp then
    local runeId = itemTmp:GetCustomUserData(0)
    InsertRune(_SkillGift_Rune_SelectPage, runeId)
  else
    ShowErrorMessage("需要先选择一个符文")
    return
  end
end
function UI_Gift_RunePageChange(msg)
  _SG_RuneBarPos = nil
  local wnd = msg:get_window()
  local selectPage = wnd:GetCustomUserData(0)
  _SkillGift_Rune_SelectPage = selectPage
  local pageName = string.format("第%d页符文", selectPage + 1)
  if selectPage ~= _SkillGift_Rune_CurrentPage then
    gUI.skillgift.runeChangePage:SetVisible(true)
  else
    gUI.skillgift.runeChangePage:SetVisible(false)
    pageName = pageName .. "(当前使用)"
  end
  gUI.skillgift.runePageName:SetProperty("Text", pageName)
  _Gift_UpdateRuneAll()
end
function UI_Gift_UseingRunePage(msg)
  ChangeRunePage(_SkillGift_Rune_SelectPage)
end
function UI_Gift_RuneRemoveByRuneId(msg)
  local wnd = msg:get_window()
  local runeId = wnd:GetCustomUserData(0)
  _Gift_RemoveRuneByRuneId(runeId)
end
function UI_Gift_RuneActiveByRuneId(msg)
  local wnd = msg:get_window()
  local runeId = wnd:GetCustomUserData(0)
  local scrollbar = WindowSys_Instance:GetWindow("Skill.Rune_bg.Right_bg.RuneList_cnt.vertSB_")
  if scrollbar then
    scrollbar = WindowToScrollBar(scrollbar)
    _SG_RuneBarPos = scrollbar:GetCurrentPos()
  end
  local name, desc, lv, needNimbus, needMoney, needlv = GetRuneTipInfo(runeId)
  Messagebox_Show("ACTIVE_RUNE", runeId, needMoney)
end
function UI_Gift_RuneSettingByRuneId(msg)
  local wnd = msg:get_window()
  local runeId = wnd:GetCustomUserData(0)
  InsertRune(_SkillGift_Rune_SelectPage, runeId)
end
function Script_Gift_OnEvent(event)
  if event == "SKILLGIFT_INIT_LIST" then
    Lua_Mounts_UpdateGift()
  elseif event == "PLAYER_INIT_SPELL_LIST" then
    Lua_Gift_UpdateAll()
  elseif event == "PLAYER_RUNE_ACTIVE" then
    Lua_Rune_Active()
  elseif event == "SPELL_ENHANCE_RESULT" then
    _OnGift_SpellEnhanceResult(arg1, arg2)
  elseif event == "SKILLGIFT_CONFIRM_RUNE" then
    _OnGift_ConfirmUseRune(arg1, arg2)
  elseif event == "SKILLGIFT_RUNE_REMOVE" then
    _OnGift_RemoveRune()
  elseif event == "SKILLGIFT_NIMBUS_LEVEL_UP" then
    Lua_Gift_UpdateSPUI()
  elseif event == "PLAYER_MONEY_CHANGED" then
    _OnGift_UpdateMoney(arg1, arg2)
  elseif event == "PLAYER_BIND_MONEY_CHANGED" then
    _OnGift_UpdateMoneyBind(arg1, arg2)
  elseif event == "SKILLGIFT_RUNE_INSERT" then
    _OnGift_InsertRune()
  end
end
function Script_Gift_OnLoad()
  gUI.skillgift.root = WindowSys_Instance:GetWindow("Skill")
  gUI.skillgift.skillRoot = WindowSys_Instance:GetWindow("Skill.Skill_bg")
  gUI.skillgift.skillContainer = WindowToContainer(WindowSys_Instance:GetWindow("Skill.Skill_bg.Center_bg.Skill_cnt"))
  gUI.skillgift.skillTabctrl = WindowSys_Instance:GetWindow("Skill.Skill_bg.Up_bg.Table_tctl")
  gUI.skillgift.nimbus = WindowSys_Instance:GetWindow("Skill.Skill_bg.Down_bg.Nimbus_dlab")
  gUI.skillgift.sp = WindowSys_Instance:GetWindow("Skill.Skill_bg.Down_bg.Skillpoint_dlab")
  gUI.skillgift.spSlab = WindowSys_Instance:GetWindow("Skill.Skill_bg.Down_bg.Skillpoint_slab")
  gUI.skillgift.coin = WindowSys_Instance:GetWindow("Skill.Skill_bg.Down_bg.Money_dlab")
  gUI.skillgift.coinBind = WindowSys_Instance:GetWindow("Skill.Skill_bg.Down_bg.Bound_dlab")
  gUI.skillgift.ExpandBtn = WindowToButton(WindowSys_Instance:GetWindow("Skill.Skill_bg.Up_bg.Expand_btn"))
  gUI.skillgift.runeRoot = WindowSys_Instance:GetWindow("Skill.Rune_bg")
  gUI.skillgift.runeStoneRoot = WindowSys_Instance:GetWindow("Skill.Rune_bg.Left_bg")
  gUI.skillgift.extractBtn = WindowSys_Instance:GetWindow("Skill.Rune_bg.Left_bg.Extract_btn")
  gUI.skillgift.runeContainer = WindowToContainer(WindowSys_Instance:GetWindow("Skill.Rune_bg.Right_bg.RuneList_cnt"))
  gUI.skillgift.runeCheckOpenBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Skill.Rune_bg.Right_bg.Display_cbtn"))
  gUI.skillgift.runeMosaicBtn = WindowSys_Instance:GetWindow("Skill.Rune_bg.Right_bg.Mosaic_btn")
  gUI.skillgift.runeOnePage = WindowToButton(WindowSys_Instance:GetWindow("Skill.Rune_bg.Left_bg.Last_btn"))
  gUI.skillgift.runeTwoPage = WindowToButton(WindowSys_Instance:GetWindow("Skill.Rune_bg.Left_bg.Next_btn"))
  gUI.skillgift.runeChangePage = WindowSys_Instance:GetWindow("Skill.Rune_bg.Left_bg.Open_btn")
  gUI.skillgift.runePageName = WindowSys_Instance:GetWindow("Skill.Rune_bg.Left_bg.Page_dlab")
  gUI.skillgift.runeRoot:SetProperty("Visible", "false")
  gUI.skillgift.runeCheckOpenBtn:SetChecked(false)
  _SkillGift_Rune_SelectPage = 0
  gUI.skillgift.runeOnePage:SetStatus("normal")
  gUI.skillgift.runeTwoPage:SetStatus("normal")
  gUI.skillgift.SProot = WindowSys_Instance:GetWindow("Basicmodel")
  gUI.skillgift.SPpicAvatar = WindowToPicture(WindowSys_Instance:GetWindow("Basicmodel.Basicmodel_bg.Up_bg.Model_bg.Model_pic"))
  local showbox = UIInterface:getShowbox(_SkillGift_NimbusShowBoxName)
  showbox = showbox or UIInterface:createShowbox(_SkillGift_NimbusShowBoxName, gUI.skillgift.SPpicAvatar)
  gUI.skillgift.showbox = showbox
  gUI.skillgift.SPname = WindowSys_Instance:GetWindow("Basicmodel.Basicmodel_bg.Up_bg.Basicmodel_dlab")
  gUI.skillgift.SPpbar = WindowToProgressBar(WindowSys_Instance:GetWindow("Basicmodel.Basicmodel_bg.Up_bg.Breath_pbar"))
  gUI.skillgift.SPdlab = WindowToLabel(WindowSys_Instance:GetWindow("Basicmodel.Basicmodel_bg.Up_bg.Breath_pbar.Breath_dlab"))
  gUI.skillgift.SPinfo = WindowSys_Instance:GetWindow("Basicmodel.Basicmodel_bg.Center_bg")
  gUI.skillgift.SPskillBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Basicmodel.Basicmodel_bg.Down_bg.Skill1_gbox"))
  local win = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Skill_btn.Skill_Eff")
  win:SetVisible(false)
  _SG_BarPos = nil
  _SG_RuneBarPos = nil
end
function Script_Gift_OnHide()
  gUI.skillgift.runeRoot:SetProperty("Visible", "false")
end

