if not gUI then
  gUI = {}
end
if not gUI.PKItem then
  gUI.PKItem = {}
end
local PK_ITEM_COST = 20000
gUI.PKItem = {
  root = nil,
  playername = nil,
  showyourname = nil,
  Glod = nil,
  Silver = nil,
  Copper = nil,
  Money = nil,
  Enemy = nil,
  Enemy_list = nil
}
function gUI.PKItem:Init()
  self.root = WindowSys_Instance:GetWindow("PKItem")
  local root = self.root
  local main = root:GetChildByName("Common_bg")
  self.playername = main:GetChildByName("Inputbox_ebox")
  self.playername = WindowToEditBox(self.playername)
  self.playername:SetProperty("Text", "")
  self.showyourname = main:GetChildByName("ShowName_cbtn")
  self.showyourname = WindowToCheckButton(self.showyourname)
  local money = main:GetChildByName("SpentMoney_bg")
  self.Money = money
  self.Glod = money:GetChildByName("Gold")
  self.Glod = WindowToEditBox(self.Glod)
  self.Glod:SetProperty("Text", "1")
  self.Silver = money:GetChildByName("Silver")
  self.Silver = WindowToEditBox(self.Silver)
  self.Silver:SetProperty("Text", "0")
  self.Copper = money:GetChildByName("Copper")
  self.Copper = WindowToEditBox(self.Copper)
  self.Copper:SetProperty("Text", "0")
  self.Enemy = main:GetChildByName("NameList_bg")
  self.Enemy_list = WindowToListBox(WindowSys_Instance:GetWindow("PKItem.Common_bg.NameList_bg.Namedi_bg.Name_lbox"))
end
function _PKItem_Close()
  gUI.PKItem.root:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(nil)
  gUI.PKItem.Enemy:SetProperty("Visible", "false")
  gUI.PKItem.playername:SetProperty("Text", "")
  gUI.PKItem.root:SetUserData64(-1)
end
function UI_PKItem_EnemyListOpen(msg)
  local enemy = gUI.PKItem.Enemy
  local enemy_list = gUI.PKItem.Enemy_list
  enemy_list:RemoveAllItems()
  local list = GetEnemyNameList()
  for i = 0, table.maxn(list) do
    local name = list[i]
    if name then
      local item = enemy_list:InsertString(name, -1)
      item:SetUserData(i)
    end
  end
  enemy_list:SetSelectItem(0)
  enemy:SetProperty("Visible", "true")
end
function UI_PKItem_GetEnemyName(msg)
  local enemy_list = gUI.PKItem.Enemy_list
  local index = enemy_list:GetSelectedItemIndex()
  if index >= 0 then
    local name = enemy_list:GetItemText(index, 0)
    gUI.PKItem.playername:SetProperty("Text", name)
  end
end
function UI_PKItem_EnemyListClose(msg)
  gUI.PKItem.Enemy:SetProperty("Visible", "false")
end
function UI_PKItem_Confim(msg)
  local wnd = msg:get_window()
  local name = gUI.PKItem.playername:GetProperty("Text")
  if name == "" then
    Lua_Chat_ShowOSD("TIANZHU_TAR_NAME_EMPTY")
    return
  end
  local checked = gUI.PKItem.showyourname:IsChecked()
  local glodtext = gUI.PKItem.Glod:GetProperty("Text")
  local glod = tonumber(glodtext)
  glod = glod * 10000
  local silver = tonumber(gUI.PKItem.Silver:GetProperty("Text"))
  silver = silver * 100
  local copper = tonumber(gUI.PKItem.Copper:GetProperty("Text"))
  local temp = glod + silver + copper
  local guid = gUI.PKItem.root:GetUserData64()
  local myName = GetPlaySelfProp(6)
  if myName == name then
    Lua_Chat_ShowOSD("TIANZHU_TAR_SELF")
    return
  end
  SendWrathRequest(checked, name, PK_ITEM_COST, guid)
end
function UI_PKItem_Cancel(msg)
  _PKItem_Close()
end
function UI_PlayName_Change(msg)
  gUI.PKItem.root:SetUserData64(-1)
end
function UI_GoldNum_Change(msg)
  local wnd = msg:get_window()
  local text = wnd:GetProperty("Text")
  if text == "" then
    wnd:SetProperty("Text", "0")
    return
  end
  _, curMoney = GetBankAndBagMoney()
  local glod = tonumber(text)
  glod = glod * 10000
  if glod > curMoney then
    local gold, silver, copper = Lua_UI_FormatMoneyByEdit(curMoney)
    gUI.PKItem.Money:GetChildByName("Gold"):SetProperty("Text", tostring(gold))
    gUI.PKItem.Money:GetChildByName("Silver"):SetProperty("Text", tostring(silver))
    gUI.PKItem.Money:GetChildByName("Copper"):SetProperty("Text", tostring(copper))
  end
end
function UI_SilverNum_Change(msg)
  local wnd = msg:get_window()
  local text = wnd:GetProperty("Text")
  if text == "" then
    wnd:SetProperty("Text", "0")
  end
end
function UI_CopperNum_Change(msg)
  local wnd = msg:get_window()
  local text = wnd:GetProperty("Text")
  if text == "" then
    wnd:SetProperty("Text", "0")
  end
end
function _OnPKItem_DefenceMurder(playername, state)
  if state == 0 then
    Lua_Chat_AddSysLog("你对[" .. playername .. "]处于正当防卫状态")
  end
  if state == 1 then
    Lua_Chat_AddSysLog("你对[" .. playername .. "]的正当防卫结束")
  end
  if state == 2 then
    Lua_Chat_AddSysLog("[" .. playername .. "]对你处于正当防卫状态")
  end
  if state == 3 then
    Lua_Chat_AddSysLog("[" .. playername .. "]对你的正当防卫结束")
  end
end
function _OnPKItem_Show(guid, name)
  gUI.PKItem.root:SetProperty("Visible", "true")
  WindowSys_Instance:SetKeyboardCaptureWindow(gUI.PKItem.playername)
  local editbox = WindowSys_Instance:GetWindow("PKItem.Common_bg.Inputbox_ebox")
  local pkName = name
  if pkName == "" then
    local char_type, _, _, targetName = GetTargetBaseInfo()
    if char_type and targetName and char_type <= gUI_CharType.CHAR_PLAYER then
      pkName = targetName
    end
  end
  editbox:SetProperty("Text", pkName)
  gUI.PKItem.Money:GetChildByName("Gold"):SetProperty("Text", "1")
  gUI.PKItem.Money:GetChildByName("Silver"):SetProperty("Text", "0")
  gUI.PKItem.Money:GetChildByName("Copper"):SetProperty("Text", "0")
  gUI.PKItem.root:SetUserData64(guid)
end
function Script_PKItem_OnEvent(event)
  if event == "USE_WRATH_ITEM" then
    _OnPKItem_Show(arg1, arg2)
  elseif event == "CLOSE_WRATH_ITEM" then
    _PKItem_Close()
  end
end
function Script_PKItem_OnLoad()
  gUI.PKItem:Init()
end
function Script_PKItem_OnHide()
  LockItemClose(2)
end
