if gUI and not gUI.BattleApply then
  gUI.BattleApply = {}
end
local SELID = 0
UIBATTLEAPPLYIMAGE = {
  [10] = "{ImAgE^UiIamge018:Image_BattleCount10}",
  [15] = "{ImAgE^UiIamge018:Image_BattleCount15}"
}
function _BattleApply_OnPanelOpen()
  if gUI.BattleApply.Root:IsVisible() then
    gUI.BattleApply.Root:SetVisible(false)
  else
    gUI.BattleApply.Root:SetVisible(true)
    GetBattleGroupInfos()
    SELID = 0
  end
end
function _BattleApply_beginUpdateInfo()
  gUI.BattleApply.ChooseList:RemoveAllItems()
end
function _BattleApply_endUpdateInfo()
  gUI.BattleApply.ChooseList:SetSelectItem(SELID)
  BattleApply_UpdateInfoBySelectId()
end
function _BattleApply_UpdateListInfo(showText, showId, IsInBattle)
  local m_ShowText = showText
  if IsInBattle == 1 then
    m_ShowText = showText .. "|{#G^ (已报名)}"
  end
  local item = gUI.BattleApply.ChooseList:InsertString(tostring(m_ShowText), -1)
  item:SetUserData(showId)
end
function BattleApply_UpdateInfoBySelectId()
  local selectId = gUI.BattleApply.ChooseList:GetSelectedItemIndex()
  local itemId = gUI.BattleApply.ChooseList:GetItemData(selectId)
  local MinLev, EquipMark, BattleType, RewardType1, RewardType2, RewardNum, IsInBattle = GetBattleMatchingInfo(itemId)
  if MinLev ~= nil then
    local strLev = ""
    if MinLev <= gUI_MainPlayerAttr.Level then
      strLev = string.format("{#G^%d}", MinLev)
    else
      strLev = string.format("{#R^%d}", MinLev)
    end
    gUI.BattleApply.JoinLev:SetProperty("Text", tostring(strLev))
    local equipScore = 0
    for i = 0, gUI_HUMAN_EQUIP.HEQUIP_NUMBER - 1 do
      local equipScoreItem = GetEquipScore(gUI_GOODSBOX_BAG.myequipbox, i)
      if equipScoreItem then
        equipScore = equipScore + equipScoreItem
      end
    end
    local strEquipMark = ""
    if EquipMark <= equipScore then
      strEquipMark = string.format("{#G^%d}", EquipMark)
    else
      strEquipMark = string.format("{#R^%d}", EquipMark)
    end
    gUI.BattleApply.EquipMark:SetProperty("Text", tostring(strEquipMark))
    gUI.BattleApply.BattleCount:SetProperty("Text", tostring(UIBATTLEAPPLYIMAGE[BattleType]))
    if RewardType1 == 6 then
      gUI.BattleApply.RewGoodsBox:SetVisible(true)
      gUI.BattleApply.RewGoodsBox:SetCustomUserData(0, RewardType2)
      gUI.BattleApply.RewGoodsBox:SetItemSubscript(0, tostring(RewardNum))
      local _, icon, quality = GetItemInfoBySlot(-1, RewardType2, -1)
      gUI.BattleApply.Exp:SetVisible(false)
      if not icon then
        return
      end
      gUI.BattleApply.RewGoodsBox:SetItemGoods(0, icon, quality)
    else
      gUI.BattleApply.RewGoodsBox:SetVisible(false)
      if RewardNum > 0 then
        gUI.BattleApply.Exp:SetVisible(true)
        gUI.BattleApply.Exp:SetProperty("Text", tostring(TokenTypeName[RewardType1].TokenName) .. "   " .. tostring(RewardNum))
      else
        gUI.BattleApply.Exp:SetVisible(false)
      end
    end
    if RewardNum > 0 then
      gUI.BattleApply.RewDes:SetVisible(true)
    else
      gUI.BattleApply.RewDes:SetVisible(false)
    end
    if IsInBattle == 1 then
      gUI.BattleApply.SignBtn:SetProperty("Text", "取消报名")
      gUI.BattleApply.GroupBtn:SetProperty("Text", "组队离开")
      gUI.BattleApply.GroupBtn:SetVisible(false)
    else
      gUI.BattleApply.SignBtn:SetProperty("Text", "个人报名")
      gUI.BattleApply.GroupBtn:SetProperty("Text", "组队报名")
      gUI.BattleApply.GroupBtn:SetVisible(true)
    end
  end
end
function _BattleApply_UpdateCurrent()
  GetBattleGroupInfos()
  BattleApply_UpdateInfoBySelectId()
end
function UI_BattleApply_JoinSign(msg)
  local selectId = gUI.BattleApply.ChooseList:GetSelectedItemIndex()
  local itemId = gUI.BattleApply.ChooseList:GetItemData(selectId)
  local wnd = msg:get_window()
  if wnd:GetProperty("Text") == "个人报名" then
    BattleMatchingJoinSign(itemId)
  else
    BattleMatchingLevelSign(itemId)
  end
end
function UI_BattleApply_JoinGroup(msg)
  local selectId = gUI.BattleApply.ChooseList:GetSelectedItemIndex()
  local itemId = gUI.BattleApply.ChooseList:GetItemData(selectId)
  local wnd = msg:get_window()
  if wnd:GetProperty("Text") == "组队报名" then
    BattleMatchingJoinGroup(itemId)
  else
    BattleMatchingLevelGroup(itemId)
  end
end
function UI_BattleApply_ClosePanel(msg)
  if gUI.BattleApply.Root:IsVisible() then
    gUI.BattleApply.Root:SetVisible(false)
  end
end
function UI_BattleApply_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Title_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_BattleApply_OpenDesInterface(msg)
  if gUI.BattleApply.RuleRoot:IsVisible() then
    gUI.BattleApply.RuleRoot:SetVisible(false)
  else
    gUI.BattleApply.RuleRoot:SetVisible(true)
  end
end
function UI_BattleApply_LListClick(msg)
  SELID = gUI.BattleApply.ChooseList:GetSelectedItemIndex()
  BattleApply_UpdateInfoBySelectId()
end
function _BattleApply_ShowWaitBtn(IsShow)
  gUI.BattleApply.WaitBtn:SetVisible(IsShow == 1)
end
function UI_Reward_Tip(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  local itemId = WindowToGoodsBox(wnd):GetCustomUserData(0)
  Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemId)
end
function UI_BattleWait_Tip(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  Lua_Tip_Begin(tooltip)
  local MapId, MapName = GetBattleApplyInfos(0)
  if MapId ~= nil and MapId > 0 then
    tooltip:InsertLeftText(MapName, "1E90FF", "kaiti_12", 0, 0)
    local remaintime = string.format("排队时间：$CT(133,0)$")
    tooltip:InsertLeftText(remaintime, "1E90FF", "kaiti_12", 0, 0)
  end
  local MapId1, MapName1 = GetBattleApplyInfos(1)
  if MapId1 ~= nil and MapId1 > 0 then
    tooltip:InsertLeftText(MapName1, "1E90FF", "kaiti_12", 0, 0)
    local remaintime1 = string.format("排队时间：$CT(133,1)$")
    tooltip:InsertLeftText(remaintime1, "1E90FF", "kaiti_12", 0, 0)
  end
  Lua_Tip_End(tooltip)
end
function UI_BattleWait_Click(msg)
  _BattleApply_OnPanelOpen()
end
function UI_BattleApply_CloseClick(msg)
  UI_BattleApply_OpenDesInterface(msg)
end
function Script_BattleApply_OnEvent(event)
  if "BATTLEAPPLY_BEGIN_UPDATE" == event then
    _BattleApply_beginUpdateInfo()
  elseif "BATTLEAPPLY_END_UPDATE" == event then
    _BattleApply_endUpdateInfo()
  elseif "BATTLEAPPLY_CUR_UPDATE" == event then
    _BattleApply_UpdateListInfo(arg1, arg2, arg3)
  elseif "BATTLEAPPLY_OPENPANEL" == event then
    _BattleApply_OnPanelOpen()
  elseif "BATTLEAPPLY_UPDATECURRENT" == event then
    _BattleApply_UpdateCurrent()
  elseif "BATTLEAPPLY_SHOWWAITBTN" == event then
    _BattleApply_ShowWaitBtn(arg1)
  end
end
function Script_BattleApply_OnHide()
end
function Script_BattleApply_OnLoad()
  gUI.BattleApply.Root = WindowSys_Instance:GetWindow("BattleApply")
  gUI.BattleApply.ChooseList = WindowToListBox(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Top_bg.Choose_lbox"))
  gUI.BattleApply.JoinLev = WindowToLabel(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Down_bg.Explain_cnt.Lev_bg.Lev_dlab"))
  gUI.BattleApply.EquipMark = WindowToLabel(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Down_bg.Explain_cnt.Equip_bg.Equip_dlab"))
  gUI.BattleApply.BattleCount = WindowToLabel(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Down_bg.Explain_cnt.BattleCount_bg.BattleCount_dlab"))
  gUI.BattleApply.Exp = WindowToLabel(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Down_bg.Explain_cnt.Reward_bg.RewardCount_dlab"))
  gUI.BattleApply.RewGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Down_bg.Explain_cnt.Reward_bg.Reward_gbox"))
  gUI.BattleApply.RewDes = WindowToLabel(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Down_bg.Explain_cnt.Reward_bg.Reward_slab"))
  gUI.BattleApply.SignBtn = WindowToButton(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Down_bg.Chara_btn"))
  gUI.BattleApply.GroupBtn = WindowToButton(WindowSys_Instance:GetWindow("BattleApply.BattleApply_bg.Down_bg.Team_btn"))
  gUI.BattleApply.WaitBtn = WindowSys_Instance:GetWindow("BattleWait")
  gUI.BattleApply.RuleRoot = WindowSys_Instance:GetWindow("BattleRule")
end
Sys_Instance:GetWindow("BattleApply.BattleApply_bg.Down_bg.Team_btn"))
  gUI.BattleApply.WaitBtn = WindowSys_Instance:GetWindow("BattleWait")
  gUI.BattleApply.RuleRoot = WindowSys_Instance:GetWindow("BattleRule")
end
