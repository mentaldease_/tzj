if gUI and not gUI.Authenticate then
  gUI.Authenticate = {}
end
AUTHENTICATESLOTCOMP = {EQUIPSLOT = 0, MATERIALSLOT = 1}
function Authenticate_GetQuality(param)
  if param <= AUTHMAXNORMAL and param > AUTHMINNORMAL then
    return "普通"
  elseif param <= AUTHMAXBATTER and param > AUTHMINBATTER then
    return "良好"
  elseif param <= AUTHMAXEXCELLENCE and param > AUTHMINEXCELLENCE then
    return "优秀"
  elseif param <= AUTHMAXPERF and param > AUTHMINPERF then
    return "完美"
  elseif param > AUTHMINTOP then
    return "极品"
  elseif param == 0 then
    return "未鉴定"
  else
    return ""
  end
end
function Authenticate_GetTopQulity(param)
  if param >= gUI_Quilty_Param.EQUALITY_ORANGE then
    return "极品"
  elseif param >= gUI_Quilty_Param.EQUALITY_YELLOW then
    return "极品"
  elseif param >= gUI_Quilty_Param.EQUALITY_PURPLE then
    return "极品"
  elseif param >= gUI_Quilty_Param.EQUALITY_BLUE then
    return "完美"
  elseif param >= gUI_Quilty_Param.EQUALITY_GREEN then
    return "优秀"
  elseif param >= gUI_Quilty_Param.EQUALITY_NORMAL then
    return "良好"
  end
end
function UI_Authenticate_OnPanelOpen(msg)
  if gUI.Authenticate.Root:IsVisible() then
    UI_Authenticate_ClosePanel()
  else
    UI_Authenticate_OpenPanel()
  end
end
function UI_Authenticate_ClosePanel(msg)
  gUI.Authenticate.Root:SetVisible(false)
end
function UI_Authenticate_OpenPanel()
  gUI.Authenticate.Root:SetVisible(true)
end
function UI_Authenticate_OnDraged(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local boxName = to_win:GetProperty("WindowName")
  local from_TypeIndex = from_win:GetProperty("GoodBoxIndex")
  if from_TypeIndex == "GB_ItemMainBag" then
    from_slot = Lua_Bag_GetRealSlot(from_win, from_slot)
    if boxName == "Main_gbox" then
      Authenticate_SendAddActionToServer(gUI_GOODSBOX_BAG.backpack0, from_slot)
    elseif boxName == "Equip_gbox" then
      Authenticate_SendAddMaterToServer(gUI_GOODSBOX_BAG.backpack0, from_slot)
    end
  end
end
function UI_Authenticate_OnRClick(msg)
  local winBox = WindowToGoodsBox(msg:get_window())
  local boxName = winBox:GetProperty("WindowName")
  if boxName == "Main_gbox" then
    SetAuthenticateEquip(0, OperateMode.Cancel)
  elseif boxName == "Equip_gbox" then
    SetAuthenticateEquip(0, OperateMode.Remove, AUTHENTICATESLOTCOMP.MATERIALSLOT)
  end
end
function UI_Authenticate_DoAction(msg)
  if not gUI.Authenticate.MainEquipGBox:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("AUTHENT_ENTER_EQUIPFIRST")
    return
  end
  if not gUI.Authenticate.MainEquipGBox:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("AUTHENT_ENTER_MATERFIRST")
    return
  end
  local bag, slot = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.EQUIPSLOT)
  local name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney, Itemid, m_TopQuilty, ItemId2
  if bag then
    _, name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney, Itemid, m_TopQuilty, ItemId2 = GetAuthenticateAttr(bag, slot)
  end
  local bagsub, slotsub = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.MATERIALSLOT)
  local bindInfo = GetItemBindInfos(bag, slot, 0)
  if bagsub and bag then
    local MaterName, MaterIconName, _, _, _, _, _, _, _, _, _, MaterItemNum, _, _, _, _, _, _, _, _, _, _, _, _, _, _, ItemIds = GetItemInfoBySlot(bagsub, slotsub, 0)
    local bindInfo = GetItemBindInfos(bag, slot, 0)
    if ItemIds == ItemId2 and not bindInfo then
      Messagebox_Show("CONFIRM_BIND_AUTH")
      return
    end
  end
  DoAuthAction()
end
function Authenticate_UpdateItemNums(ItemId)
end
function Authenticate_DelItems(NpcSlot)
  if NpcSlot == AUTHENTICATESLOTCOMP.EQUIPSLOT then
    gUI.Authenticate.MainEquipGBox:ClearGoodsItem(0)
    gUI.Authenticate.FeeLab:SetProperty("Text", "")
    gUI.Authenticate.QulityLab:SetProperty("Text", "")
    gUI.Authenticate.QulDesLab:SetProperty("Text", "")
    gUI.Authenticate.SubMaterGBox:ClearGoodsItem(0)
    gUI.Authenticate.ABtn:SetEnable(true)
  elseif NpcSlot == AUTHENTICATESLOTCOMP.MATERIALSLOT then
    gUI.Authenticate.SubMaterGBox:ClearGoodsItem(0)
  end
end
function Authenticate_UpdateItems(srcBag, srcSlot, NpcSlot)
  if not gUI.Authenticate.Root:IsVisible() then
    return
  end
  local name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney, Itemid, m_TopQuilty, ItemId2
  if NpcSlot == AUTHENTICATESLOTCOMP.EQUIPSLOT then
    _, name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney, Itemid, m_TopQuilty, ItemId2 = GetAuthenticateAttr(srcBag, srcSlot)
    if name == nil then
      return
    end
    if reidentValue == 0 then
    else
      gUI.Authenticate.SubMaterGBox:ClearGoodsItem(0)
    end
    gUI.Authenticate.MainEquipGBox:SetItemGoods(0, icon, Lua_Bag_GetQuality(gUI_GOODSBOX_BAG.backpack0, srcSlot), quality)
    local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.backpack0, srcSlot)
    if 0 < ForgeLevel_To_Stars[intenLev] then
      gUI.Authenticate.MainEquipGBox:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
    else
      gUI.Authenticate.MainEquipGBox:SetItemStarState(0, 0)
    end
    Authenticate_Update_baseAttr(reidentValue, bagNeedMoney, quality)
  elseif NpcSlot == AUTHENTICATESLOTCOMP.MATERIALSLOT then
    local bag, slot = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.EQUIPSLOT)
    if bag then
      _, name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney, Itemid, m_TopQuilty, ItemId2 = GetAuthenticateAttr(bag, slot)
      MaterName, MaterIconName, _, _, _, _, _, _, _, _, _, MaterItemNum, _, _, _, _, _, _, _, _, _, _, _, _, _, _, ItemIds = GetItemInfoBySlot(srcBag, srcSlot, 0)
      local qulity = Authenticate_GetQuality(reidentValue)
      local qulityByQ = Authenticate_GetTopQulity(quality)
      if qulity == qulityByQ then
        SetAuthenticateEquip(0, OperateMode.Remove, AUTHENTICATESLOTCOMP.MATERIALSLOT)
        return
      end
      if Itemid == ItemIds or ItemId2 == ItemIds then
        gUI.Authenticate.SubMaterGBox:SetItemGoods(0, MaterIconName, Lua_Bag_GetQuality(srcBag, srcSlot))
        gUI.Authenticate.SubMaterGBox:SetItemSubscript(0, tostring(MaterItemNum) .. "/" .. tostring(BagNumNeed))
      end
    end
  end
end
function Authenticate_Update_baseAttr(reidentValue, bagNeedMoney, Equilty)
  local qulity = Authenticate_GetQuality(reidentValue)
  local qulityByQ = Authenticate_GetTopQulity(Equilty)
  if qulity == "未鉴定" then
    qulity = tostring(qulity) .. "(第一次不需要消耗鉴定符)"
  end
  gUI.Authenticate.QulityLab:SetProperty("Text", tostring(qulity))
  gUI.Authenticate.QulDesLab:SetProperty("Text", tostring(qulityByQ))
  if qulity == qulityByQ then
    gUI.Authenticate.ABtn:SetEnable(false)
  else
    gUI.Authenticate.ABtn:SetEnable(true)
  end
  gUI.Authenticate.FeeLab:SetProperty("Text", Lua_UI_Money2String(bagNeedMoney))
end
function Authenticate_SendAddActionToServer(srcBag, srcSlot)
  if NPC_IsEquip(srcSlot) == false then
    Lua_Chat_ShowOSD("AUTHENT_DOACT_ONLYEQUIP")
    return false
  end
  local AuthAttr = GetAuthenticateAttr(srcBag, srcSlot)
  if AuthAttr ~= -1 then
    SetAuthenticateEquip(0, OperateMode.Cancel)
    SetAuthenticateEquip(srcSlot, OperateMode.Add, AUTHENTICATESLOTCOMP.EQUIPSLOT)
  else
    Lua_Chat_ShowOSD("AUTHENT_CANNOT_DOACTION")
    return false
  end
end
function Authenticate_SendAddMaterToServer(srcBag, srcSlot)
  if not gUI.Authenticate.MainEquipGBox:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("AUTHENT_ENTER_EQUIPFIRST")
  else
    local bag, slot = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.EQUIPSLOT)
    if bag then
      _, name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney, Itemid, m_TopQuilty, ItemId2 = GetAuthenticateAttr(bag, slot)
      MaterName, MaterIconName, _, _, _, _, _, _, _, _, _, MaterItemNum, _, _, _, _, _, _, _, _, _, _, _, _, _, _, ItemIds = GetItemInfoBySlot(srcBag, srcSlot, 0)
      if reidentValue == 0 then
        Lua_Chat_ShowOSD("AUTHENT_MATER_NO")
        return
      end
      local qulityd = Authenticate_GetQuality(reidentValue)
      local qulityByQ = Authenticate_GetTopQulity(quality)
      if qulityd == qulityByQ then
        Lua_Chat_ShowOSD("AUTHENT_MATER_TOP")
        return
      end
      if Itemid == ItemIds or ItemId2 == ItemIds then
        SetAuthenticateEquip(srcSlot, OperateMode.Add, AUTHENTICATESLOTCOMP.MATERIALSLOT)
      else
        Lua_Chat_ShowOSD("AUTHENT_MATER_ERR")
      end
    end
  end
end
function Authenticate_Sue()
  Lua_Chat_ShowOSD("AUTHENT_ENTER_RES")
  local bag, slot = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.EQUIPSLOT)
  local bagsub, slotsub = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.MATERIALSLOT)
  SetAuthenticateEquip(0, OperateMode.Cancel)
  if bag then
    Authenticate_SendAddActionToServer(bag, slot)
  end
  if bagsub then
    Authenticate_SendAddMaterToServer(bagsub, slotsub)
  end
end
function Authenticate_Update()
  if not gUI.Authenticate.Root:IsVisible() or not gUI.Authenticate.MainEquipGBox:IsItemHasIcon(0) or not gUI.Authenticate.SubMaterGBox:IsItemHasIcon(0) then
    return
  end
  local bag, slot = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.EQUIPSLOT)
  local _, name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney, Itemid, m_TopQuilty = GetAuthenticateAttr(bag, slot)
  if name == nil then
    return
  end
  local bagsub, slotsub = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.MATERIALSLOT)
  if bagsub then
    MaterName, MaterIconName, _, _, _, _, _, _, _, _, _, MaterItemNum, _, _, _, _, _, _, _, _, _, _, _, _, _, _, ItemIds = GetItemInfoBySlot(bagsub, slotsub, 0)
    gUI.Authenticate.SubMaterGBox:SetItemSubscript(0, tostring(MaterItemNum) .. "/" .. tostring(BagNumNeed))
  else
    gUI.Authenticate.SubMaterGBox:ClearGoodsItem(0)
  end
end
function UI_EquipsShowItem_Tips(msg)
  local win = msg:get_window()
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  local bag, slot = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.EQUIPSLOT)
  if not gUI.Authenticate.Root:IsVisible() or not gUI.Authenticate.MainEquipGBox:IsItemHasIcon(0) then
    return
  elseif bag then
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(win, bag, slot)
  end
end
function UI_AuthMaterial_ShowTips(msg)
  local win = msg:get_window()
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  if not gUI.Authenticate.Root:IsVisible() or not gUI.Authenticate.SubMaterGBox:IsItemHasIcon(0) then
    return
  else
    local bag, slot = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.EQUIPSLOT)
    local _, name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney, Itemid, m_TopQuilty = GetAuthenticateAttr(bag, slot)
    local bagsub, slotsub = GetAuthCurrentItem(AUTHENTICATESLOTCOMP.MATERIALSLOT)
    if bagsub then
      Lua_Tip_Item(tooltip, slotsub, bagsub, nil, nil, nil)
    end
  end
end
function Script_Authenticate_OnLoad()
  gUI.Authenticate.Root = WindowSys_Instance:GetWindow("Authenticate")
  gUI.Authenticate.ABtn = WindowToButton(WindowSys_Instance:GetWindow("Authenticate.Authenticate_bg.Image_bg.Spend_bg.Authenticate_btn"))
  gUI.Authenticate.MainEquipGBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Authenticate.Authenticate_bg.Image_bg.Need_bg.Main_bg.Main_gbox"))
  gUI.Authenticate.SubMaterGBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Authenticate.Authenticate_bg.Image_bg.Need_bg.Equip_bg.Equip_gbox"))
  gUI.Authenticate.FeeLab = WindowToLabel(WindowSys_Instance:GetWindow("Authenticate.Authenticate_bg.Image_bg.Spend_bg.Spend_dlab"))
  gUI.Authenticate.NeedLab = WindowToLabel(WindowSys_Instance:GetWindow("Authenticate.Authenticate_bg.Image_bg.Need_bg.Need_slab"))
  gUI.Authenticate.SubMaterRoot = WindowSys_Instance:GetWindow("Authenticate.Authenticate_bg.Image_bg.Need_bg.Equip_bg")
  gUI.Authenticate.QulityLab = WindowToLabel(WindowSys_Instance:GetWindow("Authenticate.Authenticate_bg.Image_bg.Need_bg.Main_bg.Label2_dlab"))
  gUI.Authenticate.QulDesLab = WindowToLabel(WindowSys_Instance:GetWindow("Authenticate.Authenticate_bg.Image_bg.Need_bg.Main_bg.Label4_dlab"))
  gUI.Authenticate.MainEquipGBox:ClearGoodsItem(0)
  gUI.Authenticate.SubMaterGBox:ClearGoodsItem(0)
  gUI.Authenticate.FeeLab:SetProperty("Text", "")
  gUI.Authenticate.QulityLab:SetProperty("Text", "")
  gUI.Authenticate.QulDesLab:SetProperty("Text", "")
end
function Script_Authenticate_OnHide()
  SetAuthenticateEquip(0, OperateMode.Cancel)
end
function Script_Authenticate_OnEvent(event)
  if "AUTH_ADD_ITEM" == event then
    Authenticate_UpdateItems(arg1, arg2, arg3)
  elseif "ITEM_ADD_TO_BAG" == event then
    Authenticate_Update()
  elseif "ITEM_DEL_FROM_BAG" == event then
    Authenticate_Update()
  elseif "AUTH_DEL_ITEM" == event then
    Authenticate_DelItems(arg1)
  elseif "AUTH_RES_SUESS" == event then
    Authenticate_Sue()
  end
end

