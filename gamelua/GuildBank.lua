if gUI and not gUI.GBank then
  gUI.GBank = {}
end
local _GBank_Base_Size = 20
local _GBank_Lev_Num = 20
local _GBank_Max_Lev = 5
local _GBank_Max_BagSize = _GBank_Base_Size + _GBank_Lev_Num * _GBank_Max_Lev
gGBank_BankSlotMap = {
  [0] = 0,
  [1] = 36,
  [2] = 72,
  [3] = 108,
  [4] = 144,
  [5] = 180
}
function _GBank_ClearGoodbox()
  gUI.GBank.GoodBox:ResetAllGoods(false)
end
function Lua_GBank_ConvertLocalSlotToRealSlot(localSlot)
  return localSlot
end
function Lua_GBank_ConvertRealSlotToLocalSlot(RealSlot)
  return RealSlot
end
function _GBank_SetItem(icon, count, index)
  rIndex = Lua_GBank_ConvertLocalSlotToRealSlot(index)
  gUI.GBank.GoodBox:SetItemGoods(index, icon, Lua_Bag_GetQuality(gUI_GOODSBOX_BAG.Gbankbag, rIndex))
  local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.Gbankbag, rIndex)
  if ForgeLevel_To_Stars[intenLev] > 0 then
    gUI.GBank.GoodBox:SetItemStarState(index, ForgeLevel_To_Stars[intenLev])
  else
    gUI.InheritNew.SubEquipSlot:SetItemStarState(index, 0)
  end
  gUI.GBank.GoodBox:SetItemBindState(index, Lua_Bag_GetBindInfo(gUI_GOODSBOX_BAG.Gbankbag, rIndex))
  if count > 1 then
    gUI.GBank.GoodBox:SetItemSubscript(index, tostring(count))
  end
end
function _GBank_GetCurGoodboxIndex()
  local index = gUI.GBank.TableCtrl:GetSelectItem()
  return index
end
function _GBank_UpdateTableControl()
  local count = GetAviableGBankSlotCount()
  for i = 0, math.floor(count / 36) - 1 do
    gUI.GBank.TableCtrl:SetItemEnable(i, true)
  end
  for i = math.floor(count / 36), 4 do
    gUI.GBank.TableCtrl:SetItemEnable(i, false)
  end
end
function _GBank_CloseUI()
  if gUI.GBank.Root:IsVisible() then
    CloseGBank()
    gUI.GBank.Root:SetProperty("Visible", "false")
  end
end
function _GBank_AdjustWndSize()
  local widthWnd = RenderSys_Instance:GetPixelWidth()
  local heightWnd = RenderSys_Instance:GetPixelHeight()
  local itemBoxHeight = gUI.GBank.GoodBox:GetHeight()
  local picBagBox = WindowSys_Instance:GetWindow("GuildBank.Pic1_bg")
  local picBack = WindowSys_Instance:GetWindow("GuildBank")
  if itemBoxHeight * heightWnd < 145 then
    itemBoxHeight = 145 / heightWnd
  end
  local picBagBoxHeight = itemBoxHeight + 12 / heightWnd
  picBagBox:SetHeight(picBagBoxHeight + 4 / heightWnd)
  picBack:SetHeight(picBagBox:GetHeight() + picBagBox:GetTop() + 20 / heightWnd)
end
function _GBank_SetBagSize()
  local guildLev = GetGuildProsperity()
  local beginPos = 0
  local endPos = _GBank_Max_BagSize
  for i = beginPos, endPos - 1 do
    gUI.GBank.GoodBox:SetItemVisible(i, true)
  end
  beginPos = _GBank_Base_Size + guildLev * _GBank_Lev_Num
  for i = beginPos, endPos - 1 do
    gUI.GBank.GoodBox:SetItemVisible(i, false)
  end
  gUI.GBank.GoodBox:ResetItemRect()
  _GBank_AdjustWndSize()
end
function UI_GBank_SelectChannel()
  _OnGBank_Update()
end
function UI_GBank_DonateBtnClick()
  Lua_GuildM_ShowDonateUI()
end
function UI_GBank_CancelBtnClick()
  _GBank_CloseUI()
end
function UI_GBank_LClickGoodbox()
  gUI.GBank.FromBankId = _GBank_GetCurGoodboxIndex()
end
function UI_GBank_Drop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  local from_off = msg:get_wparam()
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(from_win, from_off)
    local tooffset = msg:get_lparam()
    MoveItem(gUI_GOODSBOX_BAG[from_type], from_off, gUI_GOODSBOX_BAG[to_type], Lua_GBank_ConvertLocalSlotToRealSlot(tooffset))
  elseif from_type == "Gbankbag" then
    local tooffset = msg:get_lparam()
    MoveItem(gUI_GOODSBOX_BAG[from_type], gGBank_BankSlotMap[_GBank_GetCurGoodboxIndex()] + from_off, gUI_GOODSBOX_BAG[to_type], Lua_GBank_ConvertLocalSlotToRealSlot(tooffset))
  end
end
function UI_GBank_ShowTooltip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local slot = msg:get_wparam()
  slot = Lua_GBank_ConvertLocalSlotToRealSlot(slot)
  Lua_Tip_Item(wnd:GetToolTipWnd(0), slot, gUI_GOODSBOX_BAG.Gbankbag, nil, nil, nil)
  Lua_Tip_ShowEquipCompare(wnd, gUI_GOODSBOX_BAG.Gbankbag, slot)
end
function UI_GBank_RClick(msg)
  local from_type = msg:get_window():GetProperty("BoxType")
  local from_off = msg:get_lparam()
  local bag = gUI_GOODSBOX_BAG[from_type]
  from_off = Lua_GBank_ConvertLocalSlotToRealSlot(from_off)
  MoveItem(bag, from_off, gUI_GOODSBOX_BAG.backpack0, -1)
end
function Lua_GBank_StoreItemByRClick(from_off, from_type)
  MoveItem(gUI_GOODSBOX_BAG[from_type], from_off, gUI_GOODSBOX_BAG.Gbankbag, -1)
end
function _OnGBank_Show()
  _GBank_SetBagSize()
  gUI.GBank.Root:SetProperty("Visible", "true")
end
function _OnGBank_Update()
  _GBank_ClearGoodbox()
  local guildLev = GetGuildProsperity()
  local openCount = GetAviableGBankSlotCount()
  local beginPos = 0
  local endPos = openCount
  for i = beginPos, endPos - 1 do
    local icon, count = GetGBankItemInfoBySlot(i)
    if icon then
      _GBank_SetItem(icon, count, i)
    end
  end
  local curGold, maxGold, curDaliyGold, maxDaliyGold = GetGuildContributeInfo()
  gUI.GBank.DonateLab:SetProperty("Text", Lua_UI_Money2String(curGold * 10000))
end
function Script_GBank_OnLoad()
  gUI.GBank.Root = WindowSys_Instance:GetWindow("GuildBank")
  gUI.GBank.GoodBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("GuildBank.Pic1_bg.GoodsBox1_gbox"))
  gUI.GBank.DonateLab = WindowSys_Instance:GetWindow("GuildBank.Label2_dlab")
  gUI.GBank.TableCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("GuildBank.TableCtrl1_tctl"))
  gUI.GBank.FromBankId = 0
  gUI.GBank.GoodBox:SetItemColumn(10)
  gUI.GBank.GoodBox:SetItemCount(_GBank_Max_BagSize)
end
function Script_GBank_OnHide()
  Clear_GuildOpenFlag()
end
function Script_GBank_OnEvent(event)
  if event == "GUILD_BANK_SHOW" then
    _OnGBank_Show()
  elseif event == "GUILD_BANK_UPDATE" then
    _OnGBank_Update()
  elseif event == "CLOSE_NPC_RELATIVE_DIALOG" then
    _GBank_CloseUI()
  end
end
function Script_GBank_OnEscape()
  CloseGBank()
end
