if gUI and not gUI.Refine then
  gUI.Refine = {}
end
gUI_Refine_Table = {
  [-1] = 0,
  [0] = 10,
  [1] = 60,
  [2] = 120,
  [3] = 480
}
UNKOWNEDNUMBER = -1
CURRENT_SELECTED_INDEX = UNKOWNEDNUMBER + 1
REFINE_CHECKBUTTONS_BASEURL = "Refine.Refine_bg.Image_bg.Center_bg."
function Lua_Refine_UpdateCheckBtn(curName)
  for i = 0, 3 do
    local wndName = gUI.Refine.RefCheckBoxs[i]:GetProperty("WindowName")
    if wndName ~= curName then
      gUI.Refine.RefCheckBoxs[i]:SetCheckedState(false)
    end
  end
end
function Lua_Refine_Close_Open_Panel()
  Lua_Refine_INIT_Data()
  local b_flagOpening = "-1"
  if not gUI.Refine.Root:IsVisible() then
    b_flagOpening = "1"
    Lua_Refine_UpdateCheckBtn("")
    gUI.Refine.RefCheckBoxs[0]:SetCheckedState(true)
    Lua_Refine_SetData()
  else
    b_flagOpening = "0"
  end
  gUI.Refine.Root:SetVisible("1" == b_flagOpening)
  return "1" == b_flagOpening
end
function Lua_Refine_INIT_Data()
  CURRENT_SELECTED_INDEX = UNKOWNEDNUMBER + 1
end
function Lua_RefineSet_ShowData(actionPoint, spritePoint, expEvery, expTotal, soulEvery, soulTotal)
  gUI.Refine.ActionPoint:SetProperty("Text", tostring(actionPoint))
  gUI.Refine.SpritePoint:SetProperty("Text", tostring(spritePoint))
  gUI.Refine.ExpPointEvery:SetProperty("Text", tostring(expEvery))
  gUI.Refine.ExpPointTotal:SetProperty("Text", tostring(expTotal))
  gUI.Refine.RefSoulPointEvery:SetProperty("Text", tostring(soulEvery))
  gUI.Refine.RefSoulPointTotal:SetProperty("Text", tostring(soulTotal))
end
function Lua_Refine_Init()
  Lua_RefineSet_ShowData(0, 0, 0, 0, 0, 0)
end
function Lua_Refine_SetData()
  local actionPoint, spritePoint, expEvery, soulEvery = RefineInfosData()
  local expTotal = tonumber(gUI_Refine_Table[tonumber(CURRENT_SELECTED_INDEX)]) * tonumber(expEvery)
  local soulTotal = tonumber(gUI_Refine_Table[tonumber(CURRENT_SELECTED_INDEX)]) * tonumber(soulEvery)
  Lua_RefineSet_ShowData(actionPoint, spritePoint, expEvery, expTotal, soulEvery, soulTotal)
  local strIconName, ItemCount = RefineGetItemInfo()
  gUI.Refine.RefineItem:SetItemGoods(0, strIconName, -1)
  gUI.Refine.RefineItemText:SetProperty("Text", tostring(ItemCount) .. "/" .. tostring(gUI_Refine_Table[tonumber(CURRENT_SELECTED_INDEX)]))
  if ItemCount >= tonumber(gUI_Refine_Table[tonumber(CURRENT_SELECTED_INDEX)]) then
    gUI.Refine.RefineItemText:SetProperty("FontColor", "FF00FF00")
  else
    gUI.Refine.RefineItemText:SetProperty("FontColor", "FFFF0611")
  end
end
function OnRefine_UpdateDatas_Info()
  Lua_Refine_SetData()
end
function OnRefine_ClosePanelBySucc()
  gUI.Refine.Root:SetVisible(false)
end
function UI_Refine_CheckBtnClick(msg)
  if msg == nil then
    return
  end
  local CheckBtn = msg:get_window()
  CheckBtn = WindowToCheckButton(CheckBtn)
  local curName = CheckBtn:GetProperty("WindowName")
  if CheckBtn:IsChecked() then
    CURRENT_SELECTED_INDEX = CheckBtn:GetProperty("CustomUserData")
    Lua_Refine_UpdateCheckBtn(curName)
    Lua_Refine_SetData()
  else
    CheckBtn:SetChecked(true)
  end
end
function UI_RefineSure_Click(msg)
  if UNKOWNEDNUMBER == CURRENT_SELECTED_INDEX then
    Lua_Chat_ShowOSD("REFINE_MUSTBE_SELECT")
  else
    RefineReturnDTPracticeAura(gUI_Refine_Table[tonumber(CURRENT_SELECTED_INDEX)])
  end
end
function UI_Refine_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Refine.Refine_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_RefineExit_Click(msg)
  UI_RefineCancel_Click(msg)
end
function UI_RefineCancel_Click(msg)
  Lua_Refine_Close_Open_Panel()
end
function On_Refine_Open()
  Lua_Refine_Close_Open_Panel()
end
function Script_Refine_OnEvent(event)
  if event == "REFINE_UPDATE_INFO" or event == "PLAYER_INFO_CHANGED" then
    OnRefine_UpdateDatas_Info()
  elseif event == "REFINE_SUCC_GETINFO" then
    OnRefine_ClosePanelBySucc()
  elseif event == "REFINE_SHOW_PANEL" then
    On_Refine_Open()
  end
end
function Script_Refine_OnLoad()
  gUI.Refine.Root = WindowSys_Instance:GetWindow("Refine")
  gUI.Refine.ActionPoint = WindowToLabel(WindowSys_Instance:GetWindow("Refine.Refine_bg.Image_bg.Up_bg.Image_bg.Xlian_dlab"))
  gUI.Refine.SpritePoint = WindowToLabel(WindowSys_Instance:GetWindow("Refine.Refine_bg.Image_bg.Up_bg.Image_bg.LLi_dlab"))
  gUI.Refine.ExpPointEvery = WindowToLabel(WindowSys_Instance:GetWindow("Refine.Refine_bg.Image_bg.Down_bg.Exp_dlab"))
  gUI.Refine.ExpPointTotal = WindowToLabel(WindowSys_Instance:GetWindow("Refine.Refine_bg.Image_bg.Down_bg.ExpTotal_dlab"))
  gUI.Refine.RefSoulPointEvery = WindowToLabel(WindowSys_Instance:GetWindow("Refine.Refine_bg.Image_bg.Down_bg.Nimbus_dlab"))
  gUI.Refine.RefSoulPointTotal = WindowToLabel(WindowSys_Instance:GetWindow("Refine.Refine_bg.Image_bg.Down_bg.NimbusTotal_dlab"))
  gUI.Refine.RefCheckBoxs = {}
  gUI.Refine.RefCheckBoxs[0] = WindowToCheckButton(WindowSys_Instance:GetWindow(REFINE_CHECKBUTTONS_BASEURL .. "Cuitivate" .. tostring(gUI_Refine_Table[0]) .. "_cbtn"))
  gUI.Refine.RefCheckBoxs[1] = WindowToCheckButton(WindowSys_Instance:GetWindow(REFINE_CHECKBUTTONS_BASEURL .. "Cuitivate" .. tostring(gUI_Refine_Table[1]) .. "_cbtn"))
  gUI.Refine.RefCheckBoxs[2] = WindowToCheckButton(WindowSys_Instance:GetWindow(REFINE_CHECKBUTTONS_BASEURL .. "Cuitivate" .. tostring(gUI_Refine_Table[2]) .. "_cbtn"))
  gUI.Refine.RefCheckBoxs[3] = WindowToCheckButton(WindowSys_Instance:GetWindow(REFINE_CHECKBUTTONS_BASEURL .. "Cuitivate" .. tostring(gUI_Refine_Table[3]) .. "_cbtn"))
  gUI.Refine.RefineItem = WindowToGoodsBox(WindowSys_Instance:GetWindow("Refine.Refine_bg.Image_bg.Up_bg.Condition_gbox"))
  gUI.Refine.RefineItemText = WindowSys_Instance:GetWindow("Refine.Refine_bg.Image_bg.Up_bg.Count_slab")
end
function Script_Refine_OnHide()
  CURRENT_SELECTED_INDEX = UNKOWNEDNUMBER + 1
end
