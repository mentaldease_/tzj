if gUI and not gUI.GemIneff then
  gUI.GemIneff = {}
end
local GEMINEFF_PANELOPEN = 50
GEMATTR_USEORG = 2
GEMATTR_USENOW = 1
local MinGemIneffGemLev = 3
OPRATER_TYPE_FORGEM = {
  CLOSEPANEL = 0,
  PLACENEWITEM = 1,
  CANELITEM = 2,
  PLACENEWSUBITEM = 3
}
GEMINEFF_SLOT_COLLATE = {
  MAINGEMSLOT = 0,
  SUBGEMSLOT = 1,
  MATERSLOT1 = 2,
  MATERSLOT2 = 3
}
GEM_ATTRORGLEFT = 1
GEM_ATTRNEWRIGHT = 2
function GemIneff_OnPanelOpen()
  if not gUI.GemIneff.Root:IsVisible() then
    local _, _, _, _, lev = GetPlaySelfProp(4)
    if lev < GEMINEFF_PANELOPEN then
      Lua_Chat_ShowOSD("GEMINEFF_CANNOTOPEN_LEV")
      return
    end
    gUI.GemIneff.Root:SetVisible(true)
    gUI.GemIneff.DesPic:SetVisible(false)
    Lua_Bag_ShowUI(true)
    GemIneff_UpdateByMainChange()
  else
  end
end
function GemIneff_OnClosePanel()
  if gUI.GemIneff.Root:IsVisible() then
    gUI.GemIneff.Root:SetVisible(false)
  end
end
function UI_GemIneff_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Close_GemIneff(msg)
  local listAttr = GetItemGemAttr(0, 0, 0, 1)
  if listAttr ~= nil then
    Messagebox_Show("GEM_INEFF_CHOOSE", 1, OPRATER_TYPE_FORGEM.CLOSEPANEL, 0)
  else
    GemIneff_OnClosePanel()
  end
end
function UI_GemIneff_Esc(msg)
  if IsKeyPressed(1) then
    local listAttr = GetItemGemAttr(0, 0, 0, 1)
    if listAttr ~= nil then
      Messagebox_Show("GEM_INEFF_CHOOSE", 1, OPRATER_TYPE_FORGEM.CLOSEPANEL, 0)
    else
      GemIneff_OnClosePanel()
    end
  end
end
function GemIneff_UpdateByMainChange()
  local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  if not slotMain then
    gUI.GemIneff.MainSlot:ClearGoodsItem(0)
    gUI.GemIneff.MainSlot:SetItemBindState(0, 0)
    gUI.GemIneff.MoneyLab:SetProperty("Text", Lua_UI_Money2String(0))
  else
    local _, icon, quality = GetItemInfoBySlot(bagMain, slotMain, 0)
    if not icon then
      return
    end
    gUI.GemIneff.MainSlot:SetItemGoods(0, icon, quality)
    local bindinfo = Lua_Bag_GetBindInfo(bagMain, slotMain)
    gUI.GemIneff.MainSlot:SetItemBindState(0, bindinfo)
  end
  GemIneff_UpdateAtt(GEM_ATTRORGLEFT)
  GemIneff_UpdateAtt(GEM_ATTRNEWRIGHT)
  GemIneff_UpdateButton()
end
function GemIneff_AddNewItemOperate(bagType, Slot, CollateType)
  SetNpcFunctionEquip(bagType, Slot, OperateMode.Add, CollateType, NpcFunction.NPC_FUNC_GEM_BLEND)
end
function Gem_Ineff_RemoveItemOperate(bagType, Slot, CollateType)
  SetNpcFunctionEquip(bagType, Slot, OperateMode.Remove, CollateType, NpcFunction.NPC_FUNC_GEM_BLEND)
end
function GemIneff_CancelOperate()
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, 0, OperateMode.Cancel, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT, NpcFunction.NPC_FUNC_GEM_BLEND)
end
function GemIneff_RemItem(types, bag, slot)
  if not gUI.GemIneff.Root:IsVisible() then
    return
  end
  if types ~= 0 then
    GemIneff_AddNewItemOperate(bag, slot, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  end
end
function GemIneff_UpdateAtt(Atype)
  local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  if slotMain then
    local MainAttrDis, SubAttrDis, listAttr
    if Atype == GEM_ATTRORGLEFT then
      MainAttrDis = gUI.GemIneff.MainAttDisBefore
      SubAttrDis = gUI.GemIneff.SubAttDisBefore
      listAttr = GetItemGemAttr(bagMain, slotMain, 0)
    elseif Atype == GEM_ATTRNEWRIGHT then
      MainAttrDis = gUI.GemIneff.MainAttDisAfter
      SubAttrDis = gUI.GemIneff.SubAttDisAfter
      listAttr = GetItemGemAttr(bagMain, slotMain, 0, 1)
    end
    if listAttr ~= nil then
      MainAttrDis:ClearAllContent()
      if listAttr[1].MainAttrValue ~= 0 and listAttr[1].MainAttrDes ~= "" then
        local strMain = ""
        strMain = string.format("%s : %d%%", listAttr[1].MainAttrDes, listAttr[1].MainAttrValue)
        MainAttrDis:InsertBack(strMain, 4278255360, 0, 0)
      end
      SubAttrDis:ClearAllContent()
      for i = 0, listAttr[1].AttrCount - 1 do
        local strAttr = ""
        strAttr = string.format("%s : %d%%", listAttr[1][i].AttrDes, listAttr[1][i].AttrValue)
        SubAttrDis:InsertBack(strAttr, 4278255360, 0, 0)
      end
    else
      MainAttrDis:ClearAllContent()
      SubAttrDis:ClearAllContent()
    end
  else
    gUI.GemIneff.MainAttDisBefore:ClearAllContent()
    gUI.GemIneff.SubAttDisBefore:ClearAllContent()
    gUI.GemIneff.MainAttDisAfter:ClearAllContent()
    gUI.GemIneff.SubAttDisAfter:ClearAllContent()
  end
end
function GemIneff_UpdateBySubChange()
  local bagSub, slotSub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
  if not slotSub then
    gUI.GemIneff.SubSlot:ClearGoodsItem(0)
    gUI.GemIneff.MaterialSlotMain:ClearGoodsItem(0)
    gUI.GemIneff.MaterialSlotSub:ClearGoodsItem(0)
    gUI.GemIneff.SubSlot:SetItemBindState(0, 0)
    gUI.GemIneff.MaterialSlotMain:SetItemBindState(0, 0)
    gUI.GemIneff.MaterialSlotSub:SetItemBindState(0, 0)
    gUI.GemIneff.MoneyLab:SetProperty("Text", Lua_UI_Money2String(0))
  else
    local _, icon, quality = GetItemInfoBySlot(bagSub, slotSub, 0)
    if not icon then
      return
    end
    gUI.GemIneff.SubSlot:SetItemGoods(0, icon, quality)
    local bindinfo = Lua_Bag_GetBindInfo(bagSub, slotSub)
    gUI.GemIneff.SubSlot:SetItemBindState(0, bindinfo)
    GemIneff_UpdateMaterials()
  end
end
function GemIneff_UpdateMaterials()
  if not gUI.GemIneff.Root:IsVisible() then
    return
  end
  local bagsub, slotsub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
  if not slotsub then
    gUI.GemIneff.SubSlot:ClearGoodsItem(0)
    gUI.GemIneff.MaterialSlotMain:ClearGoodsItem(0)
    gUI.GemIneff.MaterialSlotSub:ClearGoodsItem(0)
    gUI.GemIneff.SubSlot:SetItemBindState(0, 0)
    gUI.GemIneff.MaterialSlotMain:SetItemBindState(0, 0)
    gUI.GemIneff.MaterialSlotSub:SetItemBindState(0, 0)
    gUI.GemIneff.MoneyLab:SetProperty("Text", Lua_UI_Money2String(0))
    return
  end
  local nGemLev, nBlendCtrl, nMoney, nMaterial1, nMaterialBind1, nCostNum1, nMaterial2, nMaterialBind2, nCostNum2 = GetGemBlendBaseInfo(bagsub, slotsub)
  GemIneff_UpdateMoney()
  if nMaterial1 == nil then
    return
  end
  local _, icon1, quality1 = GetItemInfoBySlot(-1, nMaterial1, 0)
  if icon1 then
    gUI.GemIneff.MaterialSlotMain:SetItemGoods(0, icon1, quality1)
  end
  local _, icon2, quality2 = GetItemInfoBySlot(-1, nMaterial2, 0)
  if icon2 then
    gUI.GemIneff.MaterialSlotSub:SetItemGoods(0, icon2, quality2)
  end
  if nMaterial1 < 1 then
    gUI.GemIneff.MaterialSlotMain:ClearGoodsItem(0)
    gUI.GemIneff.MaterialSlotMain:SetItemBindState(0, 0)
  else
    local count1 = GetItemCountInBag(nMaterial1)
    if nCostNum1 > count1 then
      gUI.GemIneff.MaterialSlotMain:SetIconState(0, 7, true)
    else
      gUI.GemIneff.MaterialSlotMain:SetIconState(0, 7, false)
    end
    gUI.GemIneff.MaterialSlotMain:SetItemSubscript(0, tostring(count1) .. "/" .. tostring(nCostNum1))
    gUI.GemIneff.MaterialSlotMain:SetCustomUserData(0, nMaterial1)
  end
  if nMaterial2 < 1 then
    gUI.GemIneff.MaterialSlotSub:ClearGoodsItem(0)
    gUI.GemIneff.MaterialSlotSub:SetItemBindState(0, 0)
  else
    local count2 = GetItemCountInBag(nMaterial2)
    local count3 = GetItemCountInBag(nMaterialBind2)
    if not gUI.GemIneff.BindFrist:IsChecked() then
      gUI.GemIneff.MaterialSlotSub:SetItemSubscript(0, tostring(count3 + count2) .. "/" .. tostring(nCostNum2))
      if nCostNum2 > count3 + count2 then
        UI.GemIneff.MaterialSlotSub:SetIconState(0, 7, true)
      else
        UI.GemIneff.MaterialSlotSub:SetIconState(0, 7, false)
      end
    else
      gUI.GemIneff.MaterialSlotSub:SetItemSubscript(0, tostring(count2) .. "/" .. tostring(nCostNum2))
      if nCostNum2 > count2 then
        UI.GemIneff.MaterialSlotSub:SetIconState(0, 7, true)
      else
        UI.GemIneff.MaterialSlotSub:SetIconState(0, 7, false)
      end
    end
    gUI.GemIneff.MaterialSlotSub:SetCustomUserData(0, nMaterial2)
  end
end
function GemIneff_UpdateMoney(newMoney, changeMoney, event)
  if not gUI.GemIneff.Root:IsVisible() then
    return
  end
  local bagsub, slotsub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
  if slotsub then
    local nGemLev, nBlendCtrl, nMoney, nMaterial1, nMaterialBind1, nCostNum1, nMaterial2, nMaterialBind2, nCostNum2 = GetGemBlendBaseInfo(bagsub, slotsub)
    if nMoney > 0 then
      gUI.GemIneff.MoneyLab:SetProperty("Text", Lua_UI_Money2String(nMoney, false, false, false, true, newMoney, event))
    else
      gUI.GemIneff.MoneyLab:SetProperty("Text", Lua_UI_Money2String(0))
    end
  end
end
function GemIneff_SetGemToNpcSer(bagType, from_off)
  if bagType == gUI_GOODSBOX_BAG.backpack0 then
    local currPlace = GemIneff_GetCurrSlot()
    if currPlace == GEMINEFF_SLOT_COLLATE.MAINGEMSLOT then
      GemIneff_PlaceMainSlot(bagType, from_off)
    elseif currPlace == GEMINEFF_SLOT_COLLATE.SUBGEMSLOT then
      GemIneff_PlaceSubSlot(bagType, from_off)
    end
  else
  end
end
function GemIneff_GetCurrSlot()
  local bagMain, SlotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  if not SlotMain then
    return GEMINEFF_SLOT_COLLATE.MAINGEMSLOT
  else
    return GEMINEFF_SLOT_COLLATE.SUBGEMSLOT
  end
end
function GemIneff_PlaceMainSlot(bagType, slot)
  local _, _, _, _, _, _, _, _, _, _, _, _, _, itemType = GetItemInfoBySlot(bagType, slot, 0)
  if itemType ~= 8 then
    Lua_Chat_ShowOSD("GEMINEFF_ONLYGEM")
    return
  else
    local gemLev, nBlend, _, _, _, _, _, _, _ = GetGemBlendBaseInfo(bagType, slot)
    if gemLev < MinGemIneffGemLev then
      Lua_Chat_ShowOSD("GEMINEFF_GEM_LEVNOTEN")
      return
    end
    local listAttr = GetItemGemAttr(0, 0, 0, 1)
    local guid = GetItemGUIDByPos(bagType, slot, 0)
    if listAttr ~= nil then
      Lua_Chat_ShowOSD("GEMINEFF_NO_CHOOSE")
    else
      GemIneff_AddNewItemOperate(bagType, slot, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
    end
  end
end
function GemIneff_PlaceSubSlot(bagType, slot)
  local _, _, _, _, _, _, _, _, _, _, _, _, _, itemType = GetItemInfoBySlot(bagType, slot, 0)
  if itemType ~= 8 then
    Lua_Chat_ShowOSD("GEMINEFF_ONLYGEM")
    return
  else
    local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
    if not bagMain then
      Lua_Chat_ShowOSD("GEMINEFF_GEM_PLACEMAIN")
      return
    end
    local gemLev, nBlend, _, _, _, _, _, _, _ = GetGemBlendBaseInfo(bagType, slot)
    if gemLev < MinGemIneffGemLev then
      Lua_Chat_ShowOSD("GEMINEFF_GEM_LEVNOTEN1")
      return
    end
    local gemLevMain = GetGemBlendBaseInfo(bagMain, slotMain)
    if gemLev > gemLevMain then
      Lua_Chat_ShowOSD("GEMINEFF_GEM_LEVCANMORE")
      return
    end
    local listAttr = GetItemGemAttr(0, 0, 0, 1)
    local guid = GetItemGUIDByPos(bagType, slot, 0)
    if listAttr ~= nil then
      Lua_Chat_ShowOSD("GEMINEFF_NO_CHOOSE")
    else
      GemIneff_AddNewItemOperate(bagType, slot, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
    end
  end
end
function GemIneff_UpdateButton()
  local listAttr = GetItemGemAttr(0, 0, 0, 1)
  if listAttr ~= nil then
    gUI.GemIneff.KeepOrgBtn:SetEnable(true)
    gUI.GemIneff.ResetNewBtn:SetEnable(true)
  else
    gUI.GemIneff.KeepOrgBtn:SetEnable(false)
    gUI.GemIneff.ResetNewBtn:SetEnable(false)
  end
end
function UI_GemIneff_DargMainSlot(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    GemIneff_PlaceMainSlot(gUI_GOODSBOX_BAG.backpack0, slot)
  end
end
function UI_GemIneff_DargSubSlot(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    GemIneff_PlaceSubSlot(gUI_GOODSBOX_BAG.backpack0, slot)
  end
end
function UI_GemIneff_MainRClick(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  local guid = GetItemGUIDByPos(bag, slot, 0)
  if not slot then
    return
  end
  local listAttr = GetItemGemAttr(0, 0, 0, 1)
  if listAttr ~= nil then
    Lua_Chat_ShowOSD("GEMINEFF_NO_CHOOSE1")
  else
    GemIneff_CancelOperate()
  end
end
function UI_GemIneff_SubRClick(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  local guid = GetItemGUIDByPos(bag, slot, 0)
  local bagsub, slotsub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
  if not slotsub then
    return
  end
  Gem_Ineff_RemoveItemOperate(bagsub, slotsub, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
end
function UI_GemIneff_DoAction(msg)
  local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  if not slotMain then
    Lua_Chat_ShowOSD("GEMINEFF_GEM_NOTHASITEM")
    return
  end
  local bagsub, slotsub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
  if not slotsub then
    Lua_Chat_ShowOSD("GEMINEFF_GEM_NOTHASITEMEX")
    return
  end
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  local nGemLev, nBlendCtrl, nMoney, nMaterial1, nMaterialBind1, nCostNum1, nMaterial2, nMaterialBind2, nCostNum2 = GetGemBlendBaseInfo(bagsub, slotsub)
  if playerNonBindGold < nMoney then
    Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
    return
  end
  local count1 = GetItemCountInBag(nMaterial1)
  local count2 = GetItemCountInBag(nMaterial2)
  local count3 = GetItemCountInBag(nMaterialBind2)
  if nCostNum1 > count1 then
    Lua_Chat_ShowOSD("NOT_ENOUGH_GEMMATERIAL1")
    return
  end
  if not gUI.GemIneff.BindFrist:IsChecked() then
    if nCostNum2 > count2 + count3 then
      Lua_Chat_ShowOSD("NOT_ENOUGH_GEMMATERIAL2")
      return
    end
  elseif nCostNum2 > count2 then
    Lua_Chat_ShowOSD("NOT_ENOUGH_GEMMATERIAL2")
    return
  end
  if GetBindInfo(slotMain, bagMain) == false then
    if GetBindInfo(slotsub, bagsub) == true or not gUI.GemIneff.BindFrist:IsChecked() and count3 > 0 then
      Messagebox_Show("GEMINEFF_ASK_CONFIRM_BIND")
    else
      GemIneff_AskGemIneff()
    end
  else
    GemIneff_AskGemIneff()
  end
end
function GemIneff_AskGemIneff()
  local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  local bagsub, slotsub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
  local listAttrSub = GetItemGemAttr(bagsub, slotsub, 0)
  if listAttrSub and listAttrSub[1].AttrCount == 0 then
    GemBlendDoAction(false)
  else
    Messagebox_Show("GEMINEFF_ASK_SUBLIST")
  end
end
function UI_GemIneff_MainTip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  if slotMain == nil then
    return
  end
  Lua_Tip_Item(tooltip, slotMain, bagMain, nil, nil, nil)
end
function UI_GemIneff_SubTip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  local bagSub, slotSub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.SUBGEMSLOT)
  if slotSub == nil then
    return
  end
  Lua_Tip_Item(tooltip, slotSub, bagSub, nil, nil, nil)
end
function UI_GemIneff_MaterialTip1(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  if win:IsItemHasIcon(0) then
    local itemId = win:GetCustomUserData(0)
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemId)
  end
end
function UI_GemIneff_MaterialTip2(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  if win:IsItemHasIcon(0) then
    local itemId = win:GetCustomUserData(0)
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemId)
  end
end
function UI_GemCostBindFirst(msg)
  GemIneff_UpdateMaterials()
end
function UI_UseOrgClick(msg)
  SendGemAttrType(GEMATTR_USEORG)
end
function UI_UseNewClick(msg)
  SendGemAttrType(GEMATTR_USENOW)
end
function UI_GemIneffOpenDes_Click(msg)
  if gUI.GemIneff.DesPic:IsVisible() then
    gUI.GemIneff.DesPic:SetVisible(false)
  else
    gUI.GemIneff.DesPic:SetVisible(true)
  end
end
function _GemIneff_DelMainGem(bagSlot)
  GemIneff_UpdateByMainChange()
  GemIneff_UpdateBySubChange()
end
function _GemIneff_DelSubGem(bagSlot)
  GemIneff_UpdateBySubChange()
end
function _GemIneff_AddMainGem(bagSlot)
  GemIneff_UpdateByMainChange()
end
function _GemIneff_AddSubGem(bagSlot)
  GemIneff_UpdateBySubChange()
end
function _OnGemIneff_AddItem(bagSlot, SlotType)
  if SlotType == GEMINEFF_SLOT_COLLATE.MAINGEMSLOT then
    _GemIneff_AddMainGem(bagSlot)
  elseif SlotType == GEMINEFF_SLOT_COLLATE.SUBGEMSLOT then
    _GemIneff_AddSubGem(bagSlot)
  end
end
function _OnGemIneff_DelItem(bagSlot, SlotType)
  if SlotType == GEMINEFF_SLOT_COLLATE.MAINGEMSLOT then
    _GemIneff_DelMainGem(bagSlot)
  elseif SlotType == GEMINEFF_SLOT_COLLATE.SUBGEMSLOT then
    _GemIneff_DelSubGem(bagSlot)
  end
end
function _OnGemIneff_CancelItem()
  GemIneff_UpdateByMainChange()
  GemIneff_UpdateBySubChange()
end
function _OnGemIneff_ShowSubAttr(isFlag)
  if not gUI.GemIneff.Root:IsVisible() then
    return
  end
  GemIneff_UpdateBySubChange()
  GemIneff_UpdateAtt(GEM_ATTRNEWRIGHT)
  GemIneff_UpdateButton()
  local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_BLEND, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT)
  local listAttrOld
  if slotMain then
    listAttrOld = GetItemGemAttr(bagMain, slotMain, 0)
  end
  if listAttrOld and listAttrOld[1].AttrCount == 0 and isFlag then
    SendGemAttrType(GEMATTR_USENOW)
    return
  end
end
function Script_GemIneff_OnLoad()
  gUI.GemIneff.Root = WindowSys_Instance:GetWindow("GemIneff")
  gUI.GemIneff.MainSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Left_bg.Top_bg.Main_gbox"))
  gUI.GemIneff.SubSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Left_bg.Top_bg.Attach_gbox"))
  gUI.GemIneff.MaterialSlotMain = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Left_bg.Down_bg.Gem_gbox"))
  gUI.GemIneff.MaterialSlotSub = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Left_bg.Down_bg.Ineff_gbox"))
  gUI.GemIneff.MoneyLab = WindowToLabel(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Left_bg.Down_bg.Money_dlab"))
  gUI.GemIneff.MainAttDisBefore = WindowToDisplayBox(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Center_bg.Up_bg.Main_dbox"))
  gUI.GemIneff.SubAttDisBefore = WindowToDisplayBox(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Center_bg.Down_bg.Ineff_dbox"))
  gUI.GemIneff.MainAttDisAfter = WindowToDisplayBox(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Right_bg.Up_bg.Main_dbox"))
  gUI.GemIneff.SubAttDisAfter = WindowToDisplayBox(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Right_bg.Down_bg.Ineff_dbox"))
  gUI.GemIneff.KeepOrgBtn = WindowToButton(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Center_bg.Self_btn"))
  gUI.GemIneff.ResetNewBtn = WindowToButton(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Right_bg.Ineff_btn"))
  gUI.GemIneff.BindFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Image_bg.Left_bg.Down_bg.Mould_cbtn"))
  gUI.GemIneff.DesPic = WindowSys_Instance:GetWindow("GemIneff.GemIneff_bg.Explain_bg")
end
function Script_GemIneff_OnEvent(event)
  if event == "GEM_INEFF_ADD_ITEM" then
    _OnGemIneff_AddItem(arg1, arg2)
  elseif event == "GEM_INEFF_DEL_ITEM" then
    _OnGemIneff_DelItem(arg1, arg2)
  elseif event == "GEM_INEFF_CANCEL" then
    _OnGemIneff_CancelItem()
  elseif event == "GEM_INEFF_SUBATTR" then
    _OnGemIneff_ShowSubAttr(arg1)
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    GemIneff_OnClosePanel()
  elseif "ITEM_ADD_TO_BAG" == event then
    GemIneff_UpdateMaterials()
  elseif "ITEM_DEL_FROM_BAG" == event then
    GemIneff_UpdateMaterials()
  elseif "GEM_INEFF_SHOW" == event then
    GemIneff_OnPanelOpen()
  elseif event == "PLAYER_MONEY_CHANGED" then
    GemIneff_UpdateMoney(arg1, arg2, event)
  elseif event == "GEM_INEFF_REM" then
    GemIneff_RemItem(arg1, arg2, arg3)
  end
end
function Script_GemIneff_OnHide()
  GemIneff_CancelOperate()
end
