if gUI and not gUI.GemSeniorBlend then
  gUI.GemSeniorBlend = {}
end
GEM_SENIORBLEND = {
  MAINSLOT = 0,
  SUBSLOT1 = 1,
  SUBSLOT2 = 2,
  SUBSLOT3 = 3,
  SUBSLOT4 = 4
}
local MinGemSenLev = 3
function GetCurrGenSeniorBlendIndex()
  for i = GEM_SENIORBLEND.MAINSLOT, GEM_SENIORBLEND.SUBSLOT4 do
    local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, i)
    if not slot then
      return i
    end
  end
  return -1
end
function GemSeniorBlend_OpenPanel()
  if gUI.GemSeniorBlend.Root:IsVisible() then
    gUI.GemSeniorBlend.Root:SetVisible(false)
  else
    gUI.GemSeniorBlend.Root:SetVisible(true)
    gUI.GemSeniorBlend.Des:SetVisible(false)
    Lua_Bag_ShowUI(true)
    _GemSenInitData()
  end
end
function GemSeniorBlend_ClosePanel()
  if gUI.GemSeniorBlend.Root:IsVisible() then
    gUI.GemSeniorBlend.Root:SetVisible(false)
  end
end
function GemSeniorBlend_UpdateGb(pos)
  if pos < GEM_SENIORBLEND.MAINSLOT or pos > GEM_SENIORBLEND.SUBSLOT4 then
    return
  end
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, pos)
  if not slot then
    gUI.GemSeniorBlend.Slots[pos]:ClearGoodsItem(0)
    gUI.GemSeniorBlend.Slots[pos]:SetItemBindState(0, 0)
  else
    local _, icon, quality = GetItemInfoBySlot(bag, slot, 0)
    if icon then
      gUI.GemSeniorBlend.Slots[pos]:SetItemGoods(0, icon, quality)
      local bindinfo = Lua_Bag_GetBindInfo(bag, slot)
      gUI.GemSeniorBlend.Slots[pos]:SetItemBindState(0, bindinfo)
    end
  end
  if pos == GEM_SENIORBLEND.MAINSLOT then
    _GemSenUpdateMaterialAndPerSlot()
  end
end
function GemSeniorBlend_AddNewItemOperate(bag, slot, colltype)
  SetNpcFunctionEquip(bag, slot, OperateMode.Add, colltype, NpcFunction.NPC_FUNC_GEM_SACRIFICE)
end
function GemSeniorBlend_RemoveItemOperate(colltype)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, colltype)
  SetNpcFunctionEquip(bag, slot, OperateMode.Remove, colltype, NpcFunction.NPC_FUNC_GEM_SACRIFICE)
end
function GemSeniorBlend_CancelItemOperate()
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, 0, OperateMode.Cancel, GEM_SENIORBLEND.MAINSLOT, NpcFunction.NPC_FUNC_GEM_SACRIFICE)
end
function _GemSenUpdateMaterialAndPerSlot()
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, GEM_SENIORBLEND.MAINSLOT)
  if not slot then
    gUI.GemSeniorBlend.Material:ClearGoodsItem(0)
    gUI.GemSeniorBlend.PreSlot:ClearGoodsItem(0)
    gUI.GemSeniorBlend.CostLab:SetProperty("Text", Lua_UI_Money2String(0))
  else
    local targetId, cost, ubindId1, bindId1, matCost1, ubindId2, bindId2, matCost2 = GetGemCamInfo(bag, slot, 0)
    if targetId then
      local _, iconTar, qualityTar = GetItemInfoBySlot(-1, targetId, targetId)
      gUI.GemSeniorBlend.PreSlot:SetItemGoods(0, iconTar, qualityTar)
      local _, iconMaterial, qualityMaterial = GetItemInfoBySlot(-1, ubindId1, ubindId1)
      gUI.GemSeniorBlend.Material:SetItemGoods(0, iconMaterial, qualityMaterial)
      GemSen_UpdateMoney()
      local count1 = GetItemCountInBag(ubindId1)
      local count2 = GetItemCountInBag(bindId1)
      local count = count1 + count2
      if gUI.GemSeniorBlend.BindFirst:IsChecked() then
        count = count1
      end
      if matCost1 > count then
        gUI.GemSeniorBlend.Material:SetIconState(0, 7, true)
      else
        gUI.GemSeniorBlend.Material:SetIconState(0, 7, false)
      end
      gUI.GemSeniorBlend.Material:SetItemSubscript(0, tostring(count) .. "/" .. tostring(matCost1))
    end
  end
end
function GemSen_UpdateMoney(newMoney, changeMoney, event)
  local bagsub, slotsub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, GEM_SENIORBLEND.MAINSLOT)
  if slotsub then
    local targetId, cost, ubindId1, bindId1, matCost1, ubindId2, bindId2, matCost2 = GetGemCamInfo(bagsub, slotsub, 0)
    if cost > 0 then
      gUI.GemSeniorBlend.CostLab:SetProperty("Text", Lua_UI_Money2String(cost, true, false, false, true, newMoney, event))
    else
      gUI.GemSeniorBlend.CostLab:SetProperty("Text", Lua_UI_Money2String(0))
    end
  end
end
function _OnUpdateGemSenMaterialInfo()
  _GemSenUpdateMaterialAndPerSlot()
end
function _GemSenInitData()
  for i = GEM_SENIORBLEND.MAINSLOT, GEM_SENIORBLEND.SUBSLOT4 do
    gUI.GemSeniorBlend.Slots[i]:ClearGoodsItem(0)
    gUI.GemSeniorBlend.Slots[i]:SetItemBindState(0, 0)
  end
  gUI.GemSeniorBlend.Material:ClearGoodsItem(0)
  gUI.GemSeniorBlend.PreSlot:ClearGoodsItem(0)
  gUI.GemSeniorBlend.CostLab:SetProperty("Text", Lua_UI_Money2String(0))
end
function _OnCancel_GemSen()
  _GemSenInitData()
end
function _OnAdd_GemSenBlend(pos)
  GemSeniorBlend_UpdateGb(pos)
end
function _OnDel_GemSenBlend(pos)
  GemSeniorBlend_UpdateGb(pos)
end
function UI_GemSeniorBlend_DoAction(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, GEM_SENIORBLEND.MAINSLOT)
  if not slot then
    Lua_Chat_ShowOSD("GEMSENIORBLEND_MAINGEM")
    return
  end
  local BINDFLAG = 1
  for i = GEM_SENIORBLEND.MAINSLOT + 1, GEM_SENIORBLEND.SUBSLOT4 do
    local bagSub, slotSub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, i)
    if not slotSub then
      Lua_Chat_ShowOSD("GEMSENIORBLEND_SUBGEM")
      return
    end
    if GetBindInfo(slotSub, bagSub) == true then
      BINDFLAG = 0
    end
  end
  local targetId, cost, ubindId1, bindId1, matCost1, ubindId2, bindId2, matCost2 = GetGemCamInfo(bag, slot, 0)
  local count2 = 0
  if targetId then
    local count1 = GetItemCountInBag(ubindId1)
    count2 = GetItemCountInBag(bindId1)
    local count = count1 + count2
    if gUI.GemSeniorBlend.BindFirst:IsChecked() then
      count = count1
    end
    if matCost1 > count then
      Lua_Chat_ShowOSD("GEMSENIORBLEND_MATERENOU")
      return
    end
  end
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  if cost > playerNonBindGold then
    Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
    return
  end
  if GetBindInfo(slot, bag) == false then
    if BINDFLAG == 0 or not gUI.GemSeniorBlend.BindFirst:IsChecked() and count2 > 0 then
      Messagebox_Show("GEMSENBLEND_ASK_CONFIRM_BIND")
    else
      GemSenItem_SubHasIneff()
    end
  else
    GemSenItem_SubHasIneff()
  end
end
function GemSenItem_SubHasIneff()
  for i = GEM_SENIORBLEND.MAINSLOT + 1, GEM_SENIORBLEND.SUBSLOT4 do
    local bagSub, slotSub = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, i)
    if not slotSub then
      Lua_Chat_ShowOSD("GEMSENIORBLEND_SUBGEM")
      return
    end
    local listAttrSub = GetItemGemAttr(bagSub, slotSub, 0)
    if listAttrSub and listAttrSub[1].AttrCount == 0 then
    else
      Messagebox_Show("GEMSENBLEND_ASK_CONFIRM_SUBINEFF")
      return
    end
  end
  DoUpradeItem(gUI.GemSeniorBlend.BindFirst:IsChecked())
end
function UI_GemSenItem_Tip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local index = tonumber(wnd:GetProperty("CustomUserData"))
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, index)
  local tooltip = wnd:GetToolTipWnd(0)
  if not slot then
    return
  end
  Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
end
function UI_GemSenPreAndMater_Tip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = wnd:GetToolTipWnd(0)
  local index = tonumber(wnd:GetProperty("CustomUserData"))
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, GEM_SENIORBLEND.MAINSLOT)
  local targetId, cost, ubindId1, bindId1, matCost1, ubindId2, bindId2, matCost2 = GetGemCamInfo(bag, slot, 0)
  if targetId then
    if index == 1 then
      Lua_Tip_Item(tooltip, nil, nil, nil, nil, targetId, nil, nil, true)
    elseif index == 2 then
      Lua_Tip_Item(tooltip, nil, nil, nil, nil, ubindId1)
    end
  end
end
function UI_GemSenItem_RClick(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local index = tonumber(wnd:GetProperty("CustomUserData"))
  if index == GEM_SENIORBLEND.MAINSLOT then
    GemSeniorBlend_CancelItemOperate()
  else
    GemSeniorBlend_RemoveItemOperate(index)
  end
end
function UI_GemSenItem_Drop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    local index = tonumber(to_win:GetProperty("CustomUserData"))
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    GemSen_PlaceSlot(slot, index)
  end
end
function UI_GemSeniorBlend_Close(msg)
  gUI.GemSeniorBlend.Root:SetVisible(false)
end
function UI_GemSeniorBlend_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Title_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_GemSeniorBlend_DesClick(msg)
  if gUI.GemSeniorBlend.Des:IsVisible() then
    gUI.GemSeniorBlend.Des:SetVisible(false)
  else
    gUI.GemSeniorBlend.Des:SetVisible(true)
  end
end
function UI_GemSenCostBindFirst(msg)
  _OnUpdateGemSenMaterialInfo()
end
function GemSen_PlaceSlot(slot, pos)
  if pos < GEM_SENIORBLEND.MAINSLOT or pos > GEM_SENIORBLEND.SUBSLOT4 then
    return
  end
  local _, _, _, _, _, _, _, _, _, _, _, _, _, itemType = GetItemInfoBySlot(gUI_GOODSBOX_BAG.backpack0, slot, 0)
  if itemType ~= 8 then
    Lua_Chat_ShowOSD("GEMINEFF_ONLYGEM")
    return
  end
  local gemLev, nBlend = GetGemBlendBaseInfo(gUI_GOODSBOX_BAG.backpack0, slot)
  if gemLev < MinGemSenLev then
    Lua_Chat_ShowOSD("GEMSENIORBLEND_LEVNOT")
    return
  end
  if pos == GEM_SENIORBLEND.MAINSLOT then
    GemSeniorBlend_AddNewItemOperate(gUI_GOODSBOX_BAG.backpack0, slot, GEM_SENIORBLEND.MAINSLOT)
  else
    local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, GEM_SENIORBLEND.MAINSLOT)
    if not slotMain then
      Lua_Chat_ShowOSD("GEMSENIORBLEND_MAINGEM")
      return
    end
    GemSeniorBlend_AddNewItemOperate(gUI_GOODSBOX_BAG.backpack0, slot, pos)
  end
end
function Script_GemSeniorBlend_OnEvent(event)
  if event == "GEM_UPGRADE_SHOW" then
    GemSeniorBlend_OpenPanel()
  elseif event == "CLOSE_NPC_RELATIVE_DIALOG" then
    GemSeniorBlend_ClosePanel()
  elseif event == "ITEM_ADD_TO_BAG" then
    _OnUpdateGemSenMaterialInfo()
  elseif event == "ITEM_DEL_FROM_BAG" then
    _OnUpdateGemSenMaterialInfo()
  elseif event == "GEM_UPGRADE_ADD_ITEM" then
    _OnAdd_GemSenBlend(arg1)
  elseif event == "GEM_UPGRADE_REMOVE_ITEM" then
    _OnDel_GemSenBlend(arg1)
  elseif event == "GEM_UPGRADE_CANCEL_ITEM" then
    _OnCancel_GemSen()
  elseif event == "GEM_UPGRADE_RESULT" then
    _OnCancel_GemSen()
  elseif event == "PLAYER_MONEY_CHANGED" then
    GemSen_UpdateMoney(arg1, arg2, event)
  end
end
function Script_GemSeniorBlend_OnHide()
  GemSeniorBlend_CancelItemOperate()
end
function Script_GemSeniorBlend_OnLoad()
  gUI.GemSeniorBlend.Root = WindowSys_Instance:GetWindow("GemSeniorBlend")
  gUI.GemSeniorBlend.Slots = {}
  gUI.GemSeniorBlend.Slots[0] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Top_bg.Main_gbox"))
  gUI.GemSeniorBlend.Slots[1] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Center_bg.MaterialMain1_bg.Append1_gbox"))
  gUI.GemSeniorBlend.Slots[2] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Center_bg.MaterialMain1_bg.Append2_gbox"))
  gUI.GemSeniorBlend.Slots[3] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Center_bg.MaterialMain1_bg.Append3_gbox"))
  gUI.GemSeniorBlend.Slots[4] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Center_bg.MaterialMain1_bg.Append4_gbox"))
  gUI.GemSeniorBlend.Material = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Down_bg.Compose_gbox"))
  gUI.GemSeniorBlend.PreSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Top_bg.Preview_gbox"))
  gUI.GemSeniorBlend.CostLab = WindowToLabel(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Down_bg.Money_bg.Money_dlab"))
  gUI.GemSeniorBlend.BindFirst = WindowToCheckButton(WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Down_bg.Mould_cbtn"))
  gUI.GemSeniorBlend.BindFirst:SetChecked(true)
  gUI.GemSeniorBlend.Des = WindowSys_Instance:GetWindow("GemSeniorBlend.GemSeniorBlend_bg.Explain_bg")
end
