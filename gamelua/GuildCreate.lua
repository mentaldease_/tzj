if gUI and not gUI.GuildC then
  gUI.GuildC = {}
end
local _GuildC_MaxGuildsPerPage = 100
local _GuildC_OrganizationItem = 306000
local _GuildC_RequestedImage = "{ImAgE^UiBtn001:Image_apply}"
local _GuildC_RespondedImage = "{ImAgE^UiBtn001:Image_respond}"
function _GuildC_SetPageEntry(page, max)
  local wnd = WindowSys_Instance:GetWindow("GuildList.Page")
  local str = string.format("%d/%d", page, max)
  wnd:SetProperty("Text", str)
end
function _GuildC_CloseRecruitCreate()
  gUI.GuildC.RecruitCreate:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function _GuildC_CloseRecruitMember()
  gUI.GuildC.RecruitMember:SetProperty("Visible", "false")
end
function _GuildC_CloseRecruitList()
  gUI.GuildC.RecruitList:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function _GuildC_CloseRequestJoin()
  gUI.GuildC.RequestJoin:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function _GuildC_RefreshRespondBtn()
  local list = WindowToListBox(gUI.GuildC.RecruitList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local index = list:GetFirstSelected()
  local isRespond = 0
  if index >= 0 then
    isRespond = list:GetItemData(index)
  end
  local btn1 = WindowSys_Instance:GetWindow("GuildRespond.TryRespond_btn")
  local btn2 = WindowSys_Instance:GetWindow("GuildRespond.CancelRespond_btn")
  if isRespond == 0 then
    btn1:SetVisible(true)
    btn2:SetVisible(false)
  else
    btn1:SetVisible(false)
    btn2:SetVisible(true)
  end
end
function _GuildC_RefreshRequestBtn()
  local list = WindowToListBox(gUI.GuildC.RequestJoin:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local index = list:GetFirstSelected()
  local isRequest = 0
  if index >= 0 then
    isRequest = list:GetItemData(index)
  end
  local btn1 = WindowSys_Instance:GetWindow("GuildRequest.TryFor_btn")
  local btn2 = WindowSys_Instance:GetWindow("GuildRequest.CancelRequest_btn")
  if isRequest == 0 then
    btn1:SetVisible(true)
    btn2:SetVisible(false)
  else
    btn1:SetVisible(false)
    btn2:SetVisible(true)
  end
end
function Lua_GuildC_RecruitMemberShow(isshow)
  local guild_id, _, _, guild_level, _, count_now, _, count_limit = GetGuildBaseInfo()
  if gUI_INVALIDE == guild_id then
    Lua_UI_ShowMessageInfo("你没有申请招募帮会")
    return
  end
  if guild_level ~= 0 then
    Lua_UI_ShowMessageInfo("你已加入帮会")
    return
  end
  local _, _, boolLeader, _ = GetGuildAuthority()
  local label = WindowToListBox(gUI.GuildC.RecruitMember:GetGrandChild("Count_dlab"))
  label:SetProperty("Text", tostring(count_now) .. "/" .. count_limit)
  local population = GetGuildPopulation() - 1
  local list = WindowToListBox(gUI.GuildC.RecruitMember:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  list:RemoveAllItems()
  for i = 0, population do
    local guid, name, _, _, _, _, _, level, menpai = GetGuildMemberInfo(nil, i)
    local str = string.format("%s|%s|%d", name, gUI_MenPaiName[menpai], level)
    local item = list:InsertString(str, -1)
    item:SetUserData(guid)
    list:SetProperty("TextHorzAlign", "Left")
    list:SetProperty("TextHorzAlign", "HCenter")
  end
  gUI.GuildC.RecruitMember:SetProperty("Visible", tostring(isshow))
  local btn = gUI.GuildC.RecruitMember:GetGrandChild("Confirm_btn")
  btn:SetVisible(true)
  btn:SetEnable(true)
  if boolLeader then
    btn:SetProperty("Text", "修改宣言")
    btn:AddScriptEvent("wm_mouseclick", "UI_GuildM_ModifyAnounce")
  else
    btn:SetProperty("Text", "取消响应")
    btn:AddScriptEvent("wm_mouseclick", "UI_OnGuildC_QuitGuild")
  end
end
function _OnGuildC_CloseNpcGUI()
  _GuildC_CloseRecruitCreate()
  _GuildC_CloseRecruitMember()
  _GuildC_CloseRecruitList()
  _GuildC_CloseRequestJoin()
end
function _OnGuildC_CreateRecruitShow(isshow)
  if isshow then
    local edit = gUI.GuildC.RecruitCreate:GetGrandChild("Image_bg.GuildName_ebox")
    edit:SetProperty("Text", "")
    local edit1 = gUI.GuildC.RecruitCreate:GetGrandChild("Image2_bg.Proclaim_ebox")
    edit1:SetProperty("Text", "欢迎加入！")
    WindowSys_Instance:SetKeyboardCaptureWindow(edit)
    gUI.GuildC._GuildC_NameEdit = edit
    gUI.GuildC._GuildC_BulletinEdit = edit1
  else
    Messagebox_Show("DEFAULT", UI_SYS_MSG.GUILD_DISMISS_WARNING)
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
  end
  gUI.GuildC.RecruitCreate:SetProperty("Visible", tostring(isshow))
end
function _OnGuildC_RecruitListShow(isshow)
  local list = WindowToListBox(gUI.GuildC.RecruitList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  list:RemoveAllItems()
  RequestRecruitList(1)
  gUI.GuildC.RespondPage:SetProperty("Text", "")
  gUI.GuildC.RespondCurPage = -1
  gUI.GuildC.RecruitList:SetProperty("Visible", tostring(isshow))
  gUI.GuildC.RespondInfo:SetProperty("Visible", "false")
end
function _OnGuildC_RecruitListPage(pageNum, memNum, guildID, guildName, leaderName, founTime, population, isResponded)
  local list = WindowToListBox(gUI.GuildC.RecruitList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  if pageNum == -1 then
    list:RemoveAllItems()
    return
  end
  local requestImage = ""
  if isResponded then
    requestImage = _GuildC_RespondedImage
  end
  local str = string.format("%s|%s|%s|%d|%s", guildName, leaderName, founTime, population, requestImage)
  local item = list:InsertString(str, -1)
  list:SetProperty("TextHorzAlign", "Left")
  list:SetProperty("TextHorzAlign", "HCenter")
  item:SetUserData64(guildID)
  if isResponded then
    item:SetUserData(1)
  else
    item:SetUserData(0)
  end
  if gUI.GuildC.RespondCurPage == -1 then
    gUI.GuildC.RespondCurPage = 1
    local _, maxPage = GetMaxGuildPage()
    gUI.GuildC.RespondPage:SetProperty("Text", "1/" .. tostring(maxPage))
  end
  _GuildC_RefreshRespondBtn()
end
function _OnGuildC_GuildListShow(isshow)
  local list = WindowToListBox(gUI.GuildC.RequestJoin:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  list:RemoveAllItems()
  RequestGuildList(1)
  gUI.GuildC.RequestCurPage = -1
  gUI.GuildC.RequestPage:SetProperty("Text", "")
  gUI.GuildC.RequestJoin:SetProperty("Visible", tostring(isshow))
  gUI.GuildC.RequestInfo:SetProperty("Visible", "false")
end
function _OnGuildC_GuildListPage(pageNum, memNum, guildID, guildName, leaderName, level, population, isRequested, maxPop)
  local list = WindowToListBox(gUI.GuildC.RequestJoin:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  if pageNum == -1 then
    list:RemoveAllItems()
    return
  end
  local bShow = gUI.GuildC.FullCBtn:IsChecked()
  if not bShow and population == maxPop then
    return
  end
  local requestImage = ""
  local index = -1
  if isRequested then
    requestImage = _GuildC_RequestedImage
    index = 0
  end
  local str = string.format("%s|%s|%d级|%d/%d|%s", guildName, leaderName, level, population, maxPop, requestImage)
  local item = list:InsertString(str, index)
  list:SetProperty("TextHorzAlign", "Left")
  list:SetProperty("TextHorzAlign", "HCenter")
  item:SetUserData64(guildID)
  if isRequested then
    item:SetUserData(1)
  else
    item:SetUserData(0)
  end
  if gUI.GuildC.RequestCurPage == -1 then
    gUI.GuildC.RequestCurPage = 1
    local maxPage = GetMaxGuildPage()
    gUI.GuildC.RequestPage:SetProperty("Text", "1/" .. tostring(maxPage))
  end
  _GuildC_RefreshRequestBtn()
end
function _OnGuildC_JoinSucc(guid, guildname, level)
  if level <= 0 then
    local list = WindowToListBox(gUI.GuildC.RecruitList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
    local count = list:GetItemCount()
    for i = 0, count - 1 do
      local item = list:GetItem(i)
      if item ~= nil and guid == item:GetUserData64() then
        item:SetColumnText(_GuildC_RespondedImage, 4)
        item:SetUserData(1)
      end
    end
  end
  _GuildC_RefreshRespondBtn()
end
function _OnGuildC_QuitSucc(guildname, level)
  if level <= 0 then
    local list = WindowToListBox(gUI.GuildC.RecruitList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
    local count = list:GetItemCount()
    for i = 0, count - 1 do
      local item = list:GetItem(i)
      item:SetColumnText("", 4)
      item:SetUserData(0)
    end
  else
    Lua_GuildM_ReadyBaseInfo(false)
  end
  _GuildC_RefreshRespondBtn()
end
function _OnGuildC_RequestJoinUpdate()
  local list = WindowToListBox(gUI.GuildC.RequestJoin:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local count = list:GetItemCount()
  for i = 0, count - 1 do
    local item = list:GetItem(i)
    if item ~= nil and CheckGuildApply(item:GetUserData64()) then
      item:SetColumnText(_GuildC_RequestedImage, 4)
      item:SetUserData(1)
    else
      item:SetColumnText("", 4)
      item:SetUserData(0)
    end
  end
  _GuildC_RefreshRequestBtn()
end
function _OnGuildC_OrganizationShow(isshow, mode)
  if isshow then
    Messagebox_Show("CONFIRM_CREATE_GUILD", mode)
  else
    Lua_GuildM_ReadyBaseInfo(true)
    WorldStage:playSoundByID(12)
  end
end
function UI_GuildC_LeftResqustBtn()
  local maxPage = GetMaxGuildPage()
  if gUI.GuildC.RequestCurPage > 1 then
    gUI.GuildC.RequestCurPage = gUI.GuildC.RequestCurPage - 1
    gUI.GuildC.RequestPage:SetProperty("Text", tostring(gUI.GuildC.RequestCurPage) .. "/" .. tostring(maxPage))
    RequestGuildList(gUI.GuildC.RequestCurPage)
  end
end
function UI_GuildC_RightResqustBtn()
  local maxPage = GetMaxGuildPage()
  if maxPage > gUI.GuildC.RequestCurPage then
    gUI.GuildC.RequestCurPage = gUI.GuildC.RequestCurPage + 1
    gUI.GuildC.RequestPage:SetProperty("Text", tostring(gUI.GuildC.RequestCurPage) .. "/" .. tostring(maxPage))
    RequestGuildList(gUI.GuildC.RequestCurPage)
  end
end
function UI_GuildC_LeftRespondBtn()
  local _, maxPage = GetMaxGuildPage()
  if gUI.GuildC.RespondCurPage > 1 then
    gUI.GuildC.RespondCurPage = gUI.GuildC.RespondCurPage - 1
    gUI.GuildC.RespondPage:SetProperty("Text", tostring(gUI.GuildC.RespondCurPage) .. "/" .. tostring(maxPage))
    RequestRecruitList(gUI.GuildC.RespondCurPage)
  end
end
function UI_GuildC_RightRespondBtn()
  local _, maxPage = GetMaxGuildPage()
  if maxPage > gUI.GuildC.RespondCurPage then
    gUI.GuildC.RespondCurPage = gUI.GuildC.RespondCurPage + 1
    gUI.GuildC.RespondPage:SetProperty("Text", tostring(gUI.GuildC.RespondCurPage) .. "/" .. tostring(maxPage))
    RequestRecruitList(gUI.GuildC.RespondCurPage)
  end
end
function UI_GuildC_Create(msg)
  local wnd = msg:get_window()
  local name = gUI.GuildC._GuildC_NameEdit:GetProperty("Text")
  local board = gUI.GuildC._GuildC_BulletinEdit:GetProperty("Text")
  local createType = wnd:GetCustomUserData(0)
  if string.len(name) == 0 then
    ShowErrorMessage("帮会名称不能为空")
  elseif string.find(name, " ") then
    ShowErrorMessage("帮会名称不能包含空格")
  elseif WorldStage:isValidString(name) == 0 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_INVALID)
  elseif createType == 0 then
    Messagebox_Show("CREATE_GUILD", false, name, board)
  elseif createType == 1 then
    Messagebox_Show("CREATE_GUILD", true, name, board)
  end
end
function UI_GuildC_CloseRecruitCreate(msg)
  _GuildC_CloseRecruitCreate()
end
function UI_GuildCreate_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GuildCreate.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_GuildC_CloseRecruitMembet(msg)
  _GuildC_CloseRecruitMember()
end
function UI_GuildC_CloseRecruitList(msg)
  _GuildC_CloseRecruitList()
end
function UI_GuildRespond_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GuildRespond.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_GuildC_CloseRequestJoin(msg)
  _GuildC_CloseRequestJoin()
end
function UI_GuildRequset_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GuildRequest.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_GuildC_JoinOtherTerr(msg)
  local list = WindowToListBox(gUI.GuildC.RequestJoin:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    return
  end
  local guid = item:GetUserData64()
  EnterOtherGuildTerr(guid)
end
function UI_GuildC_ApplyRespondRecruit(msg)
  local list = WindowToListBox(gUI.GuildC.RecruitList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    return
  end
  local guid = item:GetUserData64()
  if gUI_INVALIDE == GetGuildBaseInfo() then
    ApplyJoinGuild(guid)
  else
    Lua_Chat_ShowOSD("GUILD_APPLY_OTHER")
  end
end
function UI_GuildC_CancelRespondRecruit(msg)
  local list = WindowToListBox(gUI.GuildC.RecruitList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    return
  end
  local guid = item:GetUserData64()
  if guid == GetGuildBaseInfo() then
    QuitGuildOrganization(true)
  else
    Lua_UI_ShowMessageInfo("此帮会未响应！")
  end
end
function UI_GuildC_SearchRespondRecruit(msg)
  local ebox = WindowToEditBox(gUI.GuildC.RecruitList:GetGrandChild("Image_bg.SearchGuild_ebox"))
  local text = ebox:GetProperty("Text")
  GuildSearchRespondRecruit(text)
end
function UI_GuildC_ApplyRequestJoin(msg)
  local list = WindowToListBox(gUI.GuildC.RequestJoin:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    return
  end
  local guid = item:GetUserData64()
  if false == CheckGuildApply(guid) then
    ApplyJoinGuild(guid)
  end
end
function UI_GuildC_RequestInfoShow(msg)
  local wnd = WindowToListBox(msg:get_window())
  local item = wnd:GetSelectedItem()
  if not item then
    return
  end
  local guid = item:GetUserData64()
  local ownerName, guildName, announce = GetRecruitmentInfo(gUI.GuildC.RequestCurPage, guid, 2)
  if ownerName ~= nil then
    local guildNameLab = WindowSys_Instance:GetWindow("GuildRequest.GuildInfo_bg.Picture1_bg.Picture1_bg.Label2_dlab")
    local ownerNameLab = WindowSys_Instance:GetWindow("GuildRequest.GuildInfo_bg.Picture1_bg.Picture1_bg.Label4_dlab")
    local announceLab = WindowSys_Instance:GetWindow("GuildRequest.GuildInfo_bg.Picture1_bg.Picture1_bg.Picture1_bg.Label1_dlab")
    guildNameLab:SetProperty("Text", tostring(guildName))
    ownerNameLab:SetProperty("Text", tostring(ownerName))
    announceLab:SetProperty("Text", tostring(announce))
    _GuildC_RefreshRequestBtn()
    gUI.GuildC.RequestInfo:SetProperty("Visible", "true")
  end
end
function UI_GuildC_RespondInfoShow(msg)
  local wnd = WindowToListBox(msg:get_window())
  local item = wnd:GetSelectedItem()
  if not item then
    return
  end
  local guid = item:GetUserData64()
  local ownerName, guildName, announce = GetRecruitmentInfo(gUI.GuildC.RespondCurPage, guid, 1)
  if ownerName ~= nil then
    local guildNameLab = WindowSys_Instance:GetWindow("GuildRespond.GuildInfo_bg.Picture1_bg.Picture1_bg.Label2_dlab")
    local ownerNameLab = WindowSys_Instance:GetWindow("GuildRespond.GuildInfo_bg.Picture1_bg.Picture1_bg.Label4_dlab")
    local announceLab = WindowSys_Instance:GetWindow("GuildRespond.GuildInfo_bg.Picture1_bg.Picture1_bg.Picture1_bg.Label1_dlab")
    guildNameLab:SetProperty("Text", tostring(guildName))
    ownerNameLab:SetProperty("Text", tostring(ownerName))
    announceLab:SetProperty("Text", tostring(announce))
    _GuildC_RefreshRespondBtn()
    gUI.GuildC.RespondInfo:SetProperty("Visible", "true")
  end
end
function UI_GuildC_CancelRequestJoin(msg)
  local list = WindowToListBox(gUI.GuildC.RequestJoin:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    return
  end
  local guid = item:GetUserData64()
  if CheckGuildApply(guid) then
    CancelApplyGuild(guid)
  else
    Lua_UI_ShowMessageInfo("此帮会未申请！")
  end
end
function UI_GuildC_SearchRequestJoin(msg)
  local ebox = WindowToEditBox(gUI.GuildC.RequestJoin:GetGrandChild("Image_bg.SearchGuild_ebox"))
  local text = ebox:GetProperty("Text")
  GuildSearchRequestGuild(text)
end
function UI_GuildC_RequestInfoClose()
  gUI.GuildC.RequestInfo:SetProperty("Visible", "false")
end
function UI_GuildC_RespondInfoClose()
  gUI.GuildC.RespondInfo:SetProperty("Visible", "false")
end
function UI_GuildC_ShowRecruit()
  if gUI.GuildC.RecruitList:IsVisible() then
    gUI.GuildC.RecruitList:SetVisible(false)
  else
    _OnGuildC_RecruitListShow(true)
  end
end
function UI_OnGuildC_QuitGuild()
  local guild_id, _, _, guild_level, _, count_now, _, count_limit = GetGuildBaseInfo()
  if gUI_INVALIDE == guild_id then
    Lua_UI_ShowMessageInfo("此帮会未响应！")
  end
  QuitGuildOrganization(true)
  gUI.GuildC.RecruitMember:SetVisible(false)
  local wnd = WindowSys_Instance:GetWindow("GuildMain")
  if wnd:IsVisible() then
    local wnd = WindowSys_Instance:GetWindow("GuildMain.GuildInfo03_bg.Create_bg.Refer_bg.hint_pg")
    local wnd2 = WindowSys_Instance:GetWindow("GuildMain.GuildInfo03_bg.Create_bg.Finish_bg.hint_bg")
    local referbtn = WindowSys_Instance:GetWindow("GuildMain.GuildInfo03_bg.Create_bg.Refer_bg.Refer_btn")
    wnd:SetVisible(false)
    wnd2:SetVisible(false)
    referbtn:SetEnable(false)
  end
end
function UI_GuildC_ShowFullCheck()
  local list = WindowToListBox(gUI.GuildC.RequestJoin:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  list:RemoveAllItems()
  RequestGuildList(gUI.GuildC.RequestCurPage)
end
function Script_GuildC_OnEvent(event)
  if event == "CLOSE_NPC_RELATIVE_DIALOG" then
    _OnGuildC_CloseNpcGUI()
  elseif event == "GUILD_CREATE_RECRUIT_SHOW" then
    _OnGuildC_CreateRecruitShow(arg1)
  elseif event == "GUILD_CREATE_RECRUITMEMBER_SHOW" then
    Lua_GuildC_RecruitMemberShow(arg1)
  elseif event == "GUILD_CREATE_RECRUITLIST_SHOW" then
    _OnGuildC_RecruitListShow(arg1)
  elseif event == "GUILD_CREATE_RECRUITLIST_UPDATE" then
    _OnGuildC_RecruitListPage(arg1 - 1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif event == "GUILD_CREATE_GUILDLIST_SHOW" then
    _OnGuildC_GuildListShow(arg1)
  elseif event == "GUILD_CREATE_GUILDLIST_UPDATE" then
    _OnGuildC_GuildListPage(arg1 - 1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
  elseif event == "GUILD_JOIN_SUCC" then
    _OnGuildC_JoinSucc(arg1, arg2, arg3)
  elseif event == "GUILD_QUIT_SUCC" then
    _OnGuildC_QuitSucc(arg1, arg2)
  elseif event == "GUILD_APPLY_UPDATE" then
    _OnGuildC_RequestJoinUpdate()
  elseif event == "GUILD_CREATE_ORGANIZATION_SHOW" then
    _OnGuildC_OrganizationShow(arg1, arg2)
  end
end
function Script_GuildC_OnLoad()
  gUI.GuildC.RecruitCreate = WindowSys_Instance:GetWindow("GuildCreate")
  gUI.GuildC.RecruitCreateMoney = WindowSys_Instance:GetWindow("GuildCreate.Picture2.Label5_slab")
  gUI.GuildC.RecruitCreateMoney:SetProperty("Text", Lua_UI_Money2String(2000))
  gUI.GuildC.RecruitMember = WindowSys_Instance:GetWindow("GuildRecruitMember")
  gUI.GuildC.RecruitList = WindowSys_Instance:GetWindow("GuildRespond")
  gUI.GuildC.RequestJoin = WindowSys_Instance:GetWindow("GuildRequest")
  gUI.GuildC.RespondInfo = WindowSys_Instance:GetWindow("GuildRespond.GuildInfo_bg")
  gUI.GuildC.RequestInfo = WindowSys_Instance:GetWindow("GuildRequest.GuildInfo_bg")
  gUI.GuildC.RequestPage = WindowSys_Instance:GetWindow("GuildRequest.GuildImage_bg.Label1_dlab")
  gUI.GuildC.RespondPage = WindowSys_Instance:GetWindow("GuildRespond.GuildImage_bg.Label1_dlab")
  gUI.GuildC.RespondCurPage = -1
  gUI.GuildC.RequestCurPage = -1
  gUI.GuildC.FullCBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("GuildRequest.Full_cbtn"))
end
function Script_GuildC_OnHide()
  _OnGuildC_CloseNpcGUI()
end
ndowSys_Instance:GetWindow("GuildRequest.GuildImage_bg.Label1_dlab")
  gUI.GuildC.RespondPage = WindowSys_Instance:GetWindow("GuildRespond.GuildImage_bg.Label1_dlab")
  gUI.GuildC.RespondCurPage = -1
  gUI.GuildC.RequestCurPage = -1
  gUI.GuildC.FullCBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("GuildRequest.Full_cbtn"))
end
function Script_GuildC_OnHide()
  _OnGuildC_CloseNpcGUI()
end
