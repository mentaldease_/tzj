if gUI and not gUI.CombatStat then
  gUI.CombatStat = {}
end
local numOfStat = 0
local title = "姓名"
local Column = "70"
local CurrentSelect = 0
function UI_CombatStat_Close_Click(msg)
  SetToolTestState(false, 0)
  gUI.CombatStat.Root:SetProperty("Visible", "false")
end
function UI_CombatStat_Begin_Click(msg)
  SetToolTestState(true, 0)
end
function UI_CombatStat_End_Click(msg)
  SetToolTestState(false, 0)
end
function UI_CombatStat_Click1(msg)
  if gUI.CombatStat.Statisticbtn:GetCustomUserData(0) == 0 then
    SetToolTestState(true, 1)
    gUI.CombatStat.Statisticbtn:SetCustomUserData(0, 1)
    gUI.CombatStat.Statisticbtn:SetProperty("Text", "结束统计")
    local tData, tTime, tWeek = GetTimeString()
    if #gUI.CombatStat.StatList >= 10 then
      table.remove(gUI.CombatStat.StatList, 1)
    end
    table.insert(gUI.CombatStat.StatList, tTime)
    _CombatStat_FreshHistory()
    gUI.CombatStat.HistoryList:SetSelectItem(#gUI.CombatStat.StatList - 1)
  else
    SetToolTestState(false, 1)
    gUI.CombatStat.Statisticbtn:SetCustomUserData(0, 0)
    gUI.CombatStat.Statisticbtn:SetProperty("Text", "开始统计")
  end
end
function UI_CombatStat_ShowDefine()
  if gUI.CombatStat.Define:IsVisible() then
    gUI.CombatStat.Define:SetProperty("Visible", "flase")
  else
    gUI.CombatStat.Define:SetProperty("Visible", "true")
  end
end
function UI_CombatStat_ShowDetail()
  if gUI.CombatStat.Root:IsVisible() then
    gUI.CombatStat.Root:SetProperty("Visible", "flase")
  else
    gUI.CombatStat.Root:SetProperty("Visible", "true")
  end
end
function UI_CombatStat_ChangeStat()
  _CombatStat_ChangeStat()
end
function UI_CombatStat_ListSelect(msg)
  local index = gUI.CombatStat.HistoryList:GetSelectedItemIndex()
  CurrentSelect = index
  _CombatStat_FreshCombatStat(CurrentSelect)
end
function UI_CombatStat_ShowMsg()
  if gUI.CombatStat.Msg:IsVisible() then
    gUI.CombatStat.Msg:SetProperty("Visible", "flase")
  else
    gUI.CombatStat.Msg:SetProperty("Visible", "true")
  end
end
function UI_CombatStat_SendStatMsg(msg)
  local wnd = msg:get_window()
  local nIndex = wnd:GetCustomUserData(0)
  local combatStat = GetCombatStat(CurrentSelect)
  if combatStat[0] == nil then
    return
  end
  for i = 0, table.maxn(combatStat) do
    local str = combatStat[i].Name .. " 总伤害:" .. tostring(combatStat[i].AllAttack) .. " 总承受伤害:" .. tostring(combatStat[i].BeAttack) .. " 总治疗:" .. tostring(combatStat[i].AllHeal) .. " 总接收治疗:" .. tostring(combatStat[i].AllBeHeal)
    SendChatMessage(gUI_SENDMSG_CHANNEL_MAP[nIndex], str, "")
  end
end
function _CombatStat_Open()
  gUI.CombatStat.Root1:SetProperty("Visible", "true")
  gUI.CombatStat.CombatList:RemoveAllItems()
  _CombatStat_ChangeStat()
  _CombatStat_FreshHistory()
  gUI.CombatStat.HistoryList:SetSelectItem(0)
end
function _CombatStat_Updata(arg1)
  local displayBox = gUI.CombatStat.Root:GetChildByName("Data")
  displayBox = WindowToDisplayBox(displayBox)
  displayBox:ClearAllContent()
  displayBox:InsertBack(arg1, 0, 0, 0)
end
function _CombatStat_ChangeStat()
  gUI.CombatStat.CombatList:RemoveAllItems()
  title = "角色名"
  numOfStat = 0
  Column = "70"
  if gUI.CombatStat.CheckButton1_cbtn:IsChecked() then
    title = title .. "|总伤害"
    numOfStat = numOfStat + 1
    Column = Column .. "|90"
  end
  if gUI.CombatStat.CheckButton2_cbtn:IsChecked() then
    title = title .. "|总治疗"
    numOfStat = numOfStat + 1
    Column = Column .. "|90"
  end
  if gUI.CombatStat.CheckButton3_cbtn:IsChecked() then
    title = title .. "|受到治疗"
    numOfStat = numOfStat + 1
    Column = Column .. "|90"
  end
  if gUI.CombatStat.CheckButton4_cbtn:IsChecked() then
    title = title .. "|承受伤害"
    numOfStat = numOfStat + 1
    Column = Column .. "|90"
  end
  if numOfStat > 1 then
    gUI.CombatStat.Root1Bg:SetProperty("Left", "p" .. tostring((numOfStat - 1) * -85))
    gUI.CombatStat.Root1Bg:SetProperty("Width", "p" .. tostring(250 + (numOfStat - 1) * 85))
    gUI.CombatStat.Present:SetProperty("Width", "p" .. tostring(230 + (numOfStat - 1) * 80))
    gUI.CombatStat.CombatList:SetProperty("Width", "p" .. tostring(220 + (numOfStat - 1) * 80))
  else
    gUI.CombatStat.Root1Bg:SetProperty("Left", "p0")
    gUI.CombatStat.Root1Bg:SetProperty("Width", "p250")
    gUI.CombatStat.Present:SetProperty("Width", "p230")
    gUI.CombatStat.CombatList:SetProperty("Width", "p220")
  end
  gUI.CombatStat.CombatList:SetProperty("ColumnWidth", Column)
  gUI.CombatStat.CombatList:InsertString(title, -1)
end
function _CombatStat_Updata1()
  if CurrentSelect == gUI.CombatStat.HistoryList:GetSelectedItemIndex() then
    _CombatStat_FreshCombatStat(CurrentSelect)
  end
end
function _CombatStat_TargetChange(obj_id)
  if gUI.CombatStat.Root1:IsVisible() then
    local char_type, _, bHideName, name = GetTargetBaseInfo(obj_id)
    if char_type then
      gUI.CombatStat.TargetName:SetProperty("Text", name)
      gUI.CombatStat.TargetName:SetProperty("Visible", tostring(not bHideName))
    end
  end
end
function _CombatStat_FreshCombatStat(index)
  gUI.CombatStat.CombatList:RemoveAllItems()
  gUI.CombatStat.CombatList:InsertString(title, -1)
  local combatStat = GetCombatStat(index)
  if combatStat[0] == nil then
    return
  end
  for i = 0, table.maxn(combatStat) do
    local str = combatStat[i].Name
    if gUI.CombatStat.CheckButton1_cbtn:IsChecked() then
      str = str .. "|" .. tostring(combatStat[i].AllAttack)
    end
    if gUI.CombatStat.CheckButton2_cbtn:IsChecked() then
      str = str .. "|" .. tostring(combatStat[i].AllHeal)
    end
    if gUI.CombatStat.CheckButton3_cbtn:IsChecked() then
      str = str .. "|" .. tostring(combatStat[i].AllBeHeal)
    end
    if gUI.CombatStat.CheckButton4_cbtn:IsChecked() then
      str = str .. "|" .. tostring(combatStat[i].BeAttack)
    end
    local item = gUI.CombatStat.CombatList:InsertString(str, -1)
    gUI.CombatStat.CombatList:SetProperty("TextHorzAlign", "Left")
    gUI.CombatStat.CombatList:SetProperty("TextHorzAlign", "HCenter")
  end
end
function _CombatStat_FreshHistory()
  gUI.CombatStat.HistoryList:RemoveAllItems()
  if gUI.CombatStat.StatList[1] == nil then
    return
  end
  for i = 1, table.maxn(gUI.CombatStat.StatList) do
    local str = gUI.CombatStat.StatList[i]
    local item = gUI.CombatStat.HistoryList:InsertString(tostring(str), -1)
    gUI.CombatStat.HistoryList:SetProperty("TextHorzAlign", "Left")
    gUI.CombatStat.HistoryList:SetProperty("TextHorzAlign", "HCenter")
  end
end
function Script_CombatStat_OnEvent(event)
  if event == "UPDATA_COMBATSTAT_INFO" then
    _CombatStat_Updata(arg1)
  elseif event == "UPDATA_COMBATSTAT_INFO1" then
    _CombatStat_Updata1()
  elseif event == "OPEN_COMBATSTAT" then
    _CombatStat_Open()
  elseif event == "PLAYER_TARGET_CHANGED" then
    _CombatStat_TargetChange(arg1)
  end
end
function Script_CombatStat_OnLoad(event)
  gUI.CombatStat.Root = WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.CombatStat")
  gUI.CombatStat.Root1 = WindowSys_Instance:GetWindow("CombatStatMain")
  gUI.CombatStat.Root1Bg = WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg")
  gUI.CombatStat.Present = WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Present_bg")
  gUI.CombatStat.TargetName = WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Present_bg.Target_dlab")
  gUI.CombatStat.Define = WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Define_bg")
  gUI.CombatStat.Msg = WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Msg_bg")
  gUI.CombatStat.TargetName:SetProperty("Text", "")
  gUI.CombatStat.Statisticbtn = WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Present_bg.Statistic_btn")
  gUI.CombatStat.CombatList = WindowToListBox(WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Present_bg.Object_lbox"))
  gUI.CombatStat.CheckButton1_cbtn = WindowToCheckButton(WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Define_bg.CheckButton1_cbtn"))
  gUI.CombatStat.CheckButton2_cbtn = WindowToCheckButton(WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Define_bg.CheckButton2_cbtn"))
  gUI.CombatStat.CheckButton3_cbtn = WindowToCheckButton(WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Define_bg.CheckButton3_cbtn"))
  gUI.CombatStat.CheckButton4_cbtn = WindowToCheckButton(WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Define_bg.CheckButton4_cbtn"))
  gUI.CombatStat.HistoryList = WindowToListBox(WindowSys_Instance:GetWindow("CombatStatMain.Combat_bg.Memory_bg.Container1.Object_lbox"))
  gUI.CombatStat.CombatList:RemoveAllItems()
  gUI.CombatStat.StatList = {}
  _CombatStat_ChangeStat()
end
function Script_CombatStat_OnHide()
  SetToolTestState(false, 1)
  gUI.CombatStat.Root1:SetProperty("Visible", "false")
  gUI.CombatStat.CombatList:RemoveAllItems()
  gUI.CombatStat.Statisticbtn:SetCustomUserData(0, 0)
  gUI.CombatStat.Statisticbtn:SetProperty("Text", "开始统计")
end

