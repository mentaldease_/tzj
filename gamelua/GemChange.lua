if gUI and not gUI.GemChange then
  gUI.GemChange = {}
end
GEMCHANGESLOT = {MAINGEM = 0}
GEMCHANGEERRORCODE = {NOTGEM = -2, NOTTARGET = -1}
local LastItemGuid = 0
function _GemChange_ShowInterface()
  LastItemGuid = 0
  local _, _, _, _, level = GetPlaySelfProp(4)
  if level < 50 then
    Lua_Chat_ShowOSD("GEM_CHANGE_NEEDLEVEL")
    return
  end
  if gUI.GemChange.Root:IsVisible() then
    gUI.GemChange.Root:SetVisible(false)
  else
    gUI.GemChange.Root:SetVisible(true)
    Lua_Bag_ShowUI(true)
  end
end
function UI_GemChange_MainTip(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EXCHANGE, GEMCHANGESLOT.MAINGEM)
  if slot == nil then
    return
  end
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = wnd:GetToolTipWnd(0)
  Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
end
function UI_GemChange_PreviewTip(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EXCHANGE, GEMCHANGESLOT.MAINGEM)
  if slot == nil then
    return
  end
  local itemId = GetGemExchangeInfo(bag, slot)
  if itemId < 1 then
    return
  end
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = wnd:GetToolTipWnd(0)
  Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemId)
end
function UI_GemChange_MainDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    Lua_GemChange_PlaceMainSlot(gUI_GOODSBOX_BAG.backpack0, slot)
  end
end
function UI_GemChange_MainRClick(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EXCHANGE, GEMCHANGESLOT.MAINGEM)
  SetNpcFunctionEquip(bag, slot, OperateMode.Remove, GEMCHANGESLOT.MAINGEM, NpcFunction.NPC_FUNC_GEM_EXCHANGE)
end
function UI_GemChange_DoAction(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EXCHANGE, GEMCHANGESLOT.MAINGEM)
  if slot then
    DoGemChange()
    LastItemGuid = GetItemGUIDByPos(bag, slot, 0)
  end
end
function Lua_GemChange_PlaceMainSlot(bag, slot)
  local itemId = GetGemExchangeInfo(bag, slot)
  if GEMCHANGEERRORCODE.NOTGEM == itemId then
    Lua_Chat_ShowOSD("GEM_CHANGE_NOTGEM")
  elseif GEMCHANGEERRORCODE.NOTTARGET == itemId then
    Lua_Chat_ShowOSD("GEM_CHANGE_NOTCHANGE")
  end
  if itemId > 1 then
    SetNpcFunctionEquip(bag, slot, OperateMode.Add, GEMCHANGESLOT.MAINGEM, NpcFunction.NPC_FUNC_GEM_EXCHANGE)
  end
end
function _OnGemChange_CloseWindow()
  if gUI.GemChange.Root:IsVisible() then
    gUI.GemChange.Root:SetVisible(false)
  end
end
function UI_GemChange_ClosePanel(msg)
  _OnGemChange_CloseWindow()
end
function _OnGemChange_ShowWindow()
  _GemChange_ShowInterface()
end
function _OnGemChange_AddItem(slot, itemType)
  if itemType == GEMCHANGESLOT.MAINGEM then
    _GemChange_AddMainGem()
  end
end
function _OnGemChange_DelItem(slot, itemType)
  if itemType == GEMCHANGESLOT.MAINGEM then
    _GemChange_AddMainGem()
  end
end
function _OnGemChange_Reslut()
  _GemChange_AddMainGem()
  local bag, slot = GetItemPosByGuid(LastItemGuid)
  if slot then
    Lua_GemChange_PlaceMainSlot(bag, slot)
  end
end
function _GemChange_AddMainGem()
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EXCHANGE, GEMCHANGESLOT.MAINGEM)
  if bag and slot then
    local _, icon, quality, _, _, _, _, _, _, _, bindinfo, count = GetItemInfoBySlot(bag, slot, 0)
    if not icon then
      return
    end
    gUI.GemChange.MainSlot:SetItemGoods(0, icon, quality)
    gUI.GemChange.MainSlot:SetItemBindState(0, Lua_Bag_GetBindInfo(bag, slot))
    if count > 1 then
      gUI.GemChange.MainSlot:SetItemSubscript(0, tostring(count) .. "/1")
    end
    local itemId = GetGemExchangeInfo(bag, slot)
    local name1, icon1, quality1 = GetItemInfoBySlot(-1, itemId, 0)
    gUI.GemChange.PreviewSlot:SetItemGoods(0, icon1, quality1)
    gUI.GemChange.PreviewSlot:SetItemBindState(0, Lua_Bag_GetBindInfo(bag, slot))
    gUI.GemChange.PreviewSlot:SetItemSubscript(0, tostring(1))
  else
    gUI.GemChange.MainSlot:ClearGoodsItem(0)
    gUI.GemChange.PreviewSlot:ClearGoodsItem(0)
  end
end
function Script_GemChange_OnEvent(event)
  if "CLOSE_NPC_RELATIVE_DIALOG" == event then
    _OnGemChange_CloseWindow()
  elseif "GEMEXCHANGE_SHOW" == event then
    _OnGemChange_ShowWindow()
  elseif "GEMEXCHANGE_ADD_ITEM" == event then
    _OnGemChange_AddItem(arg1, arg2)
  elseif "GEMEXCHANGE_DEL_ITEM" == event then
    _OnGemChange_DelItem(arg1, arg2)
  elseif "GEMEXCHANGE_RESLUT" == event then
    _OnGemChange_Reslut()
  end
end
function Script_GemChange_OnHide()
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EXCHANGE, GEMCHANGESLOT.MAINGEM)
  SetNpcFunctionEquip(bag, slot, OperateMode.Remove, GEMCHANGESLOT.MAINGEM, NpcFunction.NPC_FUNC_GEM_EXCHANGE)
  LastItemGuid = 0
end
function Script_GemChange_OnLoad()
  gUI.GemChange.Root = WindowSys_Instance:GetWindow("GemChange")
  gUI.GemChange.MainSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemChange.GemChange_bg.Image_bg.MainGem_gbox"))
  gUI.GemChange.PreviewSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemChange.GemChange_bg.Image_bg.PreviewGem_gbox"))
end
