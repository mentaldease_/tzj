local TYPELINKPET = 1
local TYPELINKITEM = 0
function _OnLinkE_ChatItemClickNew(itemSerialKey, guid, tableId, quality, LogId)
  if IsKeyPressed(29) and itemSerialKey ~= -1 then
    GetItemObjByTalkSerialId(itemSerialKey, 0)
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
    WorldGroup_SetInputCapture()
    local str = "{ItEm^" .. tostring(itemSerialKey) .. "^" .. guid .. "^" .. tostring(tableId) .. "^" .. tostring(quality) .. "}"
    _Chat_AddTextToInputBox(str)
    return
  end
  if itemSerialKey == 0 then
    _OnLinkE_ChatShowToolTip(0, tableId)
  elseif itemSerialKey == -1 then
    _OnLinkE_ChatShowToolTip(-1, -1, LogId)
  else
    GetItemObjByTalkSerialId(itemSerialKey, 1)
  end
end
function _OnLinkE_ChatShowToolTip(itemSerialKey, tableId, LogId)
  local wnd = WindowSys_Instance:GetWindow("TooltipWindows.Tooltip1")
  if itemSerialKey == 0 and tableId ~= nil then
    Lua_Tip_Item(wnd, nil, -1, nil, 0, tableId)
  elseif itemSerialKey == -1 then
    Lua_Tip_Item(wnd, LogId, gUI_GOODSBOX_BAG.GbankbagLog, nil, 0, nil)
  else
    Lua_Tip_Item(wnd, itemSerialKey, gUI_GOODSBOX_BAG.chattempitem, nil, 0, nil)
  end
  _OnLinkE_ChatShowTipPos(wnd)
end
function _OnLinkE_ChatShowTipPos(wnd)
  local wndWidth = wnd:GetWidth()
  local wndHeight = wnd:GetHeight()
  local wndClose = WindowSys_Instance:GetWindow("TooltipWindows.Tooltip1.close")
  wndClose:SetTop(0)
  wndClose:SetLeft(wndWidth - wnd:GetWidth())
  local wndAct = WindowSys_Instance:GetWindow("PlayerStats")
  local wndActTop = wndAct:GetTop()
  local wndChat = WindowSys_Instance:GetWindow("ChatDialog")
  local wndChatRight = wndChat:GetRight()
  WindowSys_Instance:GetWindow("TooltipWindows"):SetProperty("Visible", "true")
  wnd:SetLeft(wndChatRight + 0.02)
  local NewTop = wndActTop + 0.75 - wndHeight - 0.02
  if NewTop < 0.05 then
    NewTop = 0.05
  end
  wnd:SetTop(NewTop)
  wnd:SetWidth(wndWidth)
  wnd:SetHeight(wndHeight)
  wnd:SetProperty("Visible", "true")
end
function OnLinkEChatPetInfosShow(name)
  UI_Pet_OpenOther(name)
end
function _OnLinkE_ChatPlayerClick(eventType, name, nType, guid)
  local myName = GetPlaySelfProp(6)
  if eventType == 1 then
    if myName == name then
      WorldGroup_ShowNewStr(UI_SYS_MSG.CAN_NOT_TALK_WITH_SELF)
      return
    end
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, name, guid)
  elseif eventType == 2 then
    if myName == name then
      return
    end
    if nType == 1 then
      Lua_MenuShow("ON_SPEAKER", name, guid)
    elseif nType == 2 then
      Lua_MenuShow("ON_SPEAKER_STALL", name, guid)
    elseif nType == 3 then
      Lua_MenuShow("ON_SPEAKER_1V1", name, guid)
    elseif nType == 4 then
      Lua_MenuShow("ON_SPEAKER_INFOS", name, guid)
    end
  end
end
function _OnLinkE_ChatChanClick(eventType, channId)
  if eventType == 1 then
  end
end
function _OnLinkE_ChatQuestClick(eventType, QuestId)
  if eventType == 1 or eventType == 2 then
    local containerWnd = WindowToContainer(WindowSys_Instance:GetWindow("TooltipQuest.Common_bg.Task_cnt"))
    local awardWnd = WindowSys_Instance:GetWindow("TooltipQuest.Common_bg.Task_cnt.Award_dbox")
    awardWnd = WindowToDisplayBox(awardWnd)
    local describeWnd = WindowSys_Instance:GetWindow("TooltipQuest.Common_bg.Task_cnt.Describe_dbox")
    describeWnd = WindowToDisplayBox(describeWnd)
    local title = WindowSys_Instance:GetWindow("TooltipQuest.Common_bg.Name_slab")
    local root = WindowSys_Instance:GetWindow("TooltipQuest")
    containerWnd:ResetScrollBar()
    gUI.Chat.QuestGoodsBox:ResetAllGoods(true)
    awardWnd:ClearAllContent()
    describeWnd:ClearAllContent()
    local strTitle, strObjective, strIncomplete, nQuestLevel, nStatus, strQuestDetail, bDisableAbandon, MapName, NpcName, nCurrTimes, nTotalTimes, nQuestType, bCalLimitTime, IsTracking, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, IsGuildExp, bHaveChoiceItem, nCommission, v1, v2, v3, v4, v5, v6, v7 = GetQuestInfoLua(QuestId, gUI_QuestRewardItemSourceID.CHAT_QUEST_LINK)
    if IsKeyPressed(29) then
      WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
      WorldGroup_SetInputCapture()
      local str = "{QuEsT^" .. strTitle .. "^" .. QuestId .. "}"
      _Chat_AddTextToInputBox(str)
      return
    end
    if v1 then
      strObjective = string.format(strObjective, v1, v2, v3, v4, v5, v6, v7)
    end
    if strTitle then
      title:SetProperty("Text", strTitle)
      title:SetProperty("Visible", "true")
    else
      title:SetProperty("Visible", "false")
    end
    MapName = GetParsedStr(MapName)
    NpcName = GetParsedStr(NpcName)
    describeWnd:InsertBack("\n任务回复：\n  " .. MapName .. " -> " .. NpcName, 0, 0, 0)
    if strObjective then
      strObjective = Lua_UI_TranslateText(strObjective)
      strObjective = GetParsedStr(strObjective)
      describeWnd:InsertBack("\n任务目标：\n  " .. strObjective .. "\n", 0, 0, 0)
    end
    if nQuestLevel then
      strObjective = Lua_UI_TranslateText(strObjective)
      describeWnd:InsertBack("\n任务等级： " .. tostring(nQuestLevel) .. "\n", 0, 0, 0)
    end
    local strReward = ""
    if RewExp and RewExp > 0 then
      strReward = strReward .. string.format("%d点经验值\n", RewExp)
      if IsGuildExp > 0 then
        local currentExp, nextExp = GetGuildLevMulitExp()
        if currentExp > 0 then
          strReward = strReward .. string.format("(当前帮会等级加成经验值: %d)\n", tonumber(RewExp * currentExp / 100))
        end
        if nextExp > 0 and currentExp > 0 then
          strReward = strReward .. string.format("(下一级加成经验值: %d)\n", tonumber(RewExp * nextExp / 100))
        end
      end
    end
    if RewNimbus and RewNimbus > 0 then
      strReward = strReward .. string.format("%d点精魄值\n", RewNimbus)
    end
    if RewMoney and RewMoney > 0 then
      strReward = strReward .. Lua_UI_Money2String(RewMoney) .. "\n"
    end
    if RewBindedMoney and RewBindedMoney > 0 then
      strReward = strReward .. Lua_UI_Money2String(RewBindedMoney) .. "(绑  金)\n"
    end
    if RewWrathValue and RewWrathValue ~= 0 then
      strReward = strReward .. RewWrathValue .. "(减少天诛值)\n"
    end
    if GuildMoney and GuildMoney > 0 then
      strReward = strReward .. string.format("帮会资金：%s\n", Lua_UI_Money2String(GuildMoney * 10000))
    end
    if GuildActive and GuildActive > 0 then
      strReward = strReward .. string.format("帮会活跃值：%d点\n", GuildActive)
    end
    if GuildContribution and GuildContribution > 0 then
      strReward = strReward .. string.format("帮会贡献度：%d点\n", GuildContribution)
    end
    if GuildConstruction and GuildConstruction > 0 then
      strReward = strReward .. string.format("帮会发展度：%d点\n", GuildConstruction)
    end
    if GuildPractice and GuildPractice > 0 then
      strReward = strReward .. string.format("守护兽成长值：%d点\n", GuildPractice)
    end
    if strReward ~= "" then
      awardWnd:InsertBack("\n任务奖励：\n" .. strReward, 0, 0, 0)
      if bHaveChoiceItem and gUI.Chat.QuestGoodsBox:IsItemHasIcon(0) then
        awardWnd:InsertBack("\n可选奖励：", 0, 0, 0)
      end
      if gUI.Chat.QuestGoodsBox:IsItemHasIcon(0) then
        gUI.Chat.QuestGoodsBox:setEnableHeightCal(true)
      else
        gUI.Chat.QuestGoodsBox:setEnableHeightCal(false)
      end
    end
    containerWnd:SizeSelf()
    root:SetProperty("Visible", "true")
  end
end
gUIOpenTypeFunction = {
  [1] = function()
    if not gUI.Quest.Board:IsVisible() then
      UI_QB_Show()
    end
  end,
  [2] = function()
    if not gUI.Amass.Root:IsVisible() then
      UI_Amass_OpenClick()
    end
  end,
  [3] = function()
    Lua_Carnival_ShowByTab(0)
  end,
  [4] = function()
    Lua_Carnival_ShowByTab(1)
  end
}
function _OnLinkE_OpenUIClick(type)
  gUIOpenTypeFunction[type]()
end

