local _Conf_Init = false
local _Conf_CurrSelectedBtn = -1
local _Conf_OffsetImg1 = -1
local _Conf_OffsetImg2 = -1
local _Conf_OffsetImg3 = -1
local _Conf_OffsetImg4 = -1
local Conf = {
  profession = 0,
  gender = 0,
  head = 1,
  face_model = 1,
  hair_model = 1,
  hair_color = 1
}
function Conf:Init()
  self.root = WindowSys_Instance:GetWindow("CharConfigure")
  local c = self.root:GetChildByName("Configure_bg")
  self.name_edit = self.root:GetGrandChild("Direction_bg.Picture1_bg.NameEdit_ebox")
  self.icon_pic = c:GetChildByName("Picture6_bg")
end
function Conf:Reset(gender)
  self.gender = gender
  self.head = 0
  self.face_model = 0
  self.hair_model = 0
  self.hair_color = 0
  self.show_box = UIInterface:getShowbox("selectShowbox")
  local pro = self.profession
  local guid = pro * 10 + gender + 5
  local name = "CreateCharModel" .. guid
  local show_box = self.show_box
  local create = gUI_PROFESSION_INTRODUCE[pro].create
  if not show_box:isAvatarExist(name) then
    show_box:createPreviewAvatar(guid, pro, gender, name, true)
  end
  show_box:setCurrentAvatar(name)
  show_box:setAvatarPosition(create.rx, create.ry, create.rz)
  show_box:setAvatarOrientation(create.rotation)
  show_box:setAvatarScale(create.scale)
  show_box:setCameraPos(create.cx, create.cy, create.cz)
  show_box:setCameraDir(create.dx, create.dy, create.dz)
  self:UpdateHeadIcon(0)
  self:UpdateFaceModel(0)
  self:UpdateHairModel(0)
  self:UpdateHairColor(0)
end
function Conf:UpdateHeadIcon(delta)
  self.head = _CharConfigure_GetIndex(delta, 0, gUI_PROFESSION_INTRODUCE[self.profession][self.gender].icon.maxnum - 1)
end
function Conf:UpdateFaceModel(delta)
  local show_box = self.show_box
  show_box:changeFaceModel(self.profession, self.gender, self.face_model, false)
  self.face_model = _CharConfigure_GetIndex(delta, 0, GetAppearanceCount(0, self.profession, self.gender) - 1)
  show_box:changeFaceModel(self.profession, self.gender, self.face_model, true)
end
function Conf:UpdateHairModel(delta)
  local show_box = self.show_box
  show_box:changeHairModel(self.profession, self.gender, self.hair_model, false)
  self.hair_model = _CharConfigure_GetIndex(delta, 0, GetAppearanceCount(1, self.profession, self.gender) - 1)
  show_box:changeHairModel(self.profession, self.gender, self.hair_model, true)
end
function Conf:UpdateHairColor(delta)
  local show_box = self.show_box
  self.hair_color = _CharConfigure_GetIndex(delta, 0, GetAppearanceCount(2, self.profession, self.gender) - 1)
  show_box:changeHairColor(self.profession, self.gender, self.hair_color)
end
function Conf:_Random(max)
  local r = Randomize()
  return math.fmod(r, max) + 1
end
function Conf:GetRandomAppearance()
  local r = self:_Random(GetAppearanceCount(0, self.profession, self.gender))
  Conf:UpdateFaceModel(r)
  r = self:_Random(GetAppearanceCount(1, self.profession, self.gender))
  Conf:UpdateHairModel(r)
  r = self:_Random(GetAppearanceCount(2, self.profession, self.gender))
  Conf:UpdateHairColor(r)
end
function Conf:GetRandomName()
  local last_name = self.last_name
  local first_name = self.gender == 0 and self.female_first_name or self.male_first_name
  local r1 = self:_Random(table.maxn(last_name))
  local r2 = self:_Random(table.maxn(first_name))
  self.name_edit:SetProperty("Text", last_name[r1] .. first_name[r2])
end
function _CharConfigure_GetIndex(index, min, max)
  if max < index then
    index = max
  end
  if min > index then
    index = min
  end
  return index
end
function _CharConfigure_SetImg(pro, strType, indexId, offset, gender)
  for i = 0, 2 do
    local wndName = string.format("Picture%d_pic", i)
    local img = Conf.icon_pic:GetChildByName(wndName)
    local strImg = gUI_PROFESSION_INTRODUCE[pro][gender][strType][offset + i]
    local btn = WindowToButton(img:GetChildByName("Button1_btn"))
    if not strImg then
      strImg = ""
      btn:SetProperty("Enable", "false")
    else
      btn:SetProperty("Enable", "true")
    end
    img:SetProperty("BackImage", strImg)
    if indexId == i + offset then
      btn:SetStatus("selected")
    else
      btn:SetStatus("normal")
    end
  end
end
function _CharConfigure_InitSetting()
  local wnd = WindowSys_Instance:GetWindow("CharConfigure")
  wnd:SetProperty("Visible", "true")
  Conf.icon_pic:SetVisible(false)
  Conf.profession = Lua_ProPreview_GetSelectedProfession()
  Conf.name_edit:SetProperty("Text", "")
  WindowSys_Instance:SetKeyboardCaptureWindow(Conf.name_edit)
  local bio = gUI_PROFESSION_INTRODUCE[Conf.profession].biography
  local Introduce = wnd:GetGrandChild("Picture1_bg.Label1_dlab")
  Introduce:SetProperty("Text", bio.proviewText)
  local menpaiPic = wnd:GetGrandChild("Picture1_bg.Picture1_pic")
  menpaiPic:SetProperty("BackImage", bio.menpaiPic)
  local MeiPaiTrait = wnd:GetGrandChild("Picture4_bg.Picture2_bg")
  local MainAttribute = MeiPaiTrait:GetChildByName("Label1_dlab")
  MainAttribute:SetProperty("Text", bio.mainAttribute)
  local FightTendency = MeiPaiTrait:GetChildByName("Label2_dlab")
  FightTendency:SetProperty("Text", bio.fightTendency)
  local TeamTendency = MeiPaiTrait:GetChildByName("Label3_dlab")
  TeamTendency:SetProperty("Text", bio.teamTendency)
  local OperDiff = WindowToProgressBar(wnd:GetGrandChild("Picture4_bg.Dif_bg.Dif_Par"))
  OperDiff:SetProperty("Progress", tostring(bio.operdiff * 2 / 10))
  local FightPic = wnd:GetGrandChild("Picture3_bg.Picture2_pic")
  FightPic:SetProperty("BackImage", bio.fightPic)
  _Conf_OffsetImg1 = 0
  _Conf_OffsetImg2 = 0
  _Conf_OffsetImg3 = 0
  _Conf_OffsetImg4 = 0
  _Conf_CurrSelectedBtn = -1
  _CharConfigure_SetBtnEnable()
end
function _CharConfigure_ConfigImg()
  if _Conf_CurrSelectedBtn == 1 then
    _CharConfigure_SetImg(Conf.profession, "faceModel", Conf.face_model, _Conf_OffsetImg1, Conf.gender)
  elseif _Conf_CurrSelectedBtn == 2 then
    _CharConfigure_SetImg(Conf.profession, "hairModel", Conf.hair_model, _Conf_OffsetImg2, Conf.gender)
  elseif _Conf_CurrSelectedBtn == 3 then
    _CharConfigure_SetImg(Conf.profession, "hairColor", Conf.hair_color, _Conf_OffsetImg3, Conf.gender)
  elseif _Conf_CurrSelectedBtn == 4 then
    _CharConfigure_SetImg(Conf.profession, "icon", Conf.head, _Conf_OffsetImg4, Conf.gender)
  end
end
function _CharConfigure_SetBtnEnable()
  for i = 1, 4 do
    local btn = Conf.root:GetGrandChild(string.format("Configure_bg.Button%d_btn", i))
    btn:SetProperty("Enable", "true")
  end
  if _Conf_CurrSelectedBtn ~= -1 then
    local btn = Conf.root:GetGrandChild(string.format("Configure_bg.Button%d_btn", _Conf_CurrSelectedBtn))
    btn:SetProperty("Enable", "false")
  end
end
function Lua_CharConfigure_Show(gender)
  _CharConfigure_InitSetting()
  Conf:Reset(gender)
end
function _OnCharConfigure_CreateResult(result)
  if result == 0 then
    Lua_CharList_HaveCreatedNewChar(true)
    Conf.show_box:setCurrentAvatar("")
    Conf.root:SetVisible(false)
  else
    Lua_CharList_HaveCreatedNewChar(false)
  end
end
function UI_CharConfigure_ChangeBtnClick(msg)
  _Conf_CurrSelectedBtn = string.match(msg:get_window():GetProperty("WindowName"), "%d")
  _Conf_CurrSelectedBtn = tonumber(_Conf_CurrSelectedBtn)
  Conf.icon_pic:SetVisible(true)
  _CharConfigure_ConfigImg()
  _CharConfigure_SetBtnEnable()
end
function UI_CharConfigure_DetailModify(msg)
  local btn = msg:get_window()
  local offset = 0
  local delta = 0
  if string.find(btn:GetProperty("WindowName"), "Left") then
    offset = -1
  elseif string.find(btn:GetProperty("WindowName"), "Right") then
    offset = 1
  end
  if offset == 0 then
    local parent = btn:GetParent()
    delta = string.match(parent:GetProperty("WindowName"), "%d")
    delta = tonumber(delta)
    if _Conf_CurrSelectedBtn == 1 then
      Conf:UpdateFaceModel(delta + _Conf_OffsetImg1)
    elseif _Conf_CurrSelectedBtn == 2 then
      Conf:UpdateHairModel(delta + _Conf_OffsetImg2)
    elseif _Conf_CurrSelectedBtn == 3 then
      Conf:UpdateHairColor(delta + _Conf_OffsetImg3)
    elseif _Conf_CurrSelectedBtn == 4 then
      Conf:UpdateHeadIcon(delta + _Conf_OffsetImg4)
    end
  elseif _Conf_CurrSelectedBtn == 1 then
    _Conf_OffsetImg1 = _CharConfigure_GetIndex(_Conf_OffsetImg1 + offset, 0, GetAppearanceCount(0, Conf.profession, Conf.gender) - 3)
  elseif _Conf_CurrSelectedBtn == 2 then
    _Conf_OffsetImg2 = _CharConfigure_GetIndex(_Conf_OffsetImg2 + offset, 0, GetAppearanceCount(1, Conf.profession, Conf.gender) - 3)
  elseif _Conf_CurrSelectedBtn == 3 then
    _Conf_OffsetImg3 = _CharConfigure_GetIndex(_Conf_OffsetImg3 + offset, 0, GetAppearanceCount(2, Conf.profession, Conf.gender) - 3)
  elseif _Conf_CurrSelectedBtn == 4 then
    _Conf_OffsetImg4 = _CharConfigure_GetIndex(_Conf_OffsetImg4 + offset, 0, gUI_PROFESSION_INTRODUCE[Conf.profession][Conf.gender].icon.maxnum - 3)
  end
  _CharConfigure_ConfigImg()
end
function UI_CharConfigure_ChangeGender(msg)
  local wnd = msg:get_window()
  local gender = wnd:GetProperty("WindowName") == "Male" and 1 or 0
  Conf:Reset(gender)
end
function UI_CharConfigure_ChangeFaceModel(msg)
  local wnd = msg:get_window()
  local delta = wnd:GetProperty("WindowName") == "Left2" and -1 or 1
  Conf:UpdateFaceModel(delta)
end
function UI_CharConfigure_ChangeHairModel(msg)
  local wnd = msg:get_window()
  local delta = wnd:GetProperty("WindowName") == "Left3" and -1 or 1
  Conf:UpdateHairModel(delta)
end
function UI_CharConfigure_ChangeHairColor(msg)
  local wnd = msg:get_window()
  local delta = wnd:GetProperty("WindowName") == "Left4" and -1 or 1
  Conf:UpdateHairColor(delta)
end
function UI_CharConfigure_Back()
  Conf.show_box:setCurrentAvatar("")
  Conf.root:SetProperty("Visible", "false")
  Lua_ProPreview_AllShow()
end
function UI_CharConfigure_Create()
  local name = Conf.name_edit:GetProperty("Text")
  if string.len(name) < 4 or string.len(name) > 12 then
    MessageboxTypes.NOTIFY_MSG.has_button1 = true
    MessageboxTypes.NOTIFY_MSG.text = "角色名长度必须为4~12个字符"
    MessageboxTypes.NOTIFY_MSG.text1 = ""
    MessageboxTypes.NOTIFY_MSG.text2 = ""
    MessageboxTypes.NOTIFY_MSG.text3 = ""
    Messagebox_Show("NOTIFY_MSG")
    return
  end
  CreateNewChar(name, Conf.profession, Conf.gender, Conf.head, Conf.face_model, Conf.hair_model, Conf.hair_color)
end
function UI_CharConfigure_RandomAll(msg)
  Conf:GetRandomAppearance()
end
function UI_CharConfigure_RandomName(msg)
  Conf:GetRandomName()
end
function Script_CharConfigure_OnLoad()
  local conf = Conf
  conf:Init()
  if _Conf_Init then
    return
  end
  local n = require("Names")
  conf.last_name = n.LAST_NAME
  conf.female_first_name = n.FEMALE_FIRST_NAME
  conf.male_first_name = n.MALE_FIRST_NAME
  _Conf_Init = true
end
function Script_CharConfigure_OnEvent(event)
  if event == "CREATE_CHAR_RESULT" then
    _OnCharConfigure_CreateResult(arg1)
  elseif event == "CHAR_CAMERA_TEST" then
    local wnd = WindowSys_Instance:GetWindow("CharConfigure.test.campos")
    wnd:SetProperty("Text", arg1)
    wnd = WindowSys_Instance:GetWindow("CharConfigure.test.targetpos")
    wnd:SetProperty("Text", arg2)
    wnd = WindowSys_Instance:GetWindow("CharConfigure.test.Label1")
    wnd:SetProperty("Text", tostring(arg3))
  end
end
function Test_ScaleChanged(msg)
  local str = msg:get_window():GetProperty("Text")
  local n = tonumber(str)
  if n < 0.5 then
    n = 0.5
  end
  Conf.show_box:setAvatarScale(n)
end
function Test_ShowRotation(msg)
  local r = Conf.show_box:getRotaion()
  msg:get_window():SetProperty("Text", "点我看旋转:" .. tostring(r))
end
function CharConfigure_EnableCameraEdit(msg)
  Conf.show_box:enableCameraEdit(msg:get_wparam() > 0)
end
function CharConfigure_TargetPosChanged(msg)
  local str = msg:get_window():GetProperty("Text")
  Conf.show_box:setCameraTargetPos(str)
end
function CharConfigure_CameraPosChanged(msg)
  local str = msg:get_window():GetProperty("Text")
  Conf.show_box:setCameraPosition(str)
end

