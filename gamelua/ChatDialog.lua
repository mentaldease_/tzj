if gUI and not gUI.Chat then
  gUI.Chat = {}
end
local _Chat_Speakername = ""
local _Chat_Golble_Index = 0
local _Chat_Golble_Index_Laba = 0
local _Chat_OSDTime = 2
local _Chat_Laba_Time1 = 0
local _Chat_Laba_Time2 = 0
local _CHAT_CURRENT_SELECT = -1
local _CHAT_HAS_CHANGE = false
local _Chat_ItemLinkType = {
  [0] = {
    typeName = "achieveItem",
    minVaule = 0,
    maxVaule = 1024
  },
  [1] = {
    typeName = "titleItem",
    minVaule = 1025,
    maxVaule = 2048
  },
  [2] = {
    typeName = "goodsItem",
    minVaule = 100000,
    maxVaule = 999999
  }
}
local _Chat_Horn = {
  chatType = -1,
  bigHornItemID = 360045,
  smallHornItemID = 360044,
  ColorChoseRadio = {
    Color1 = true,
    Color2 = false,
    Color3 = false
  },
  RadioColor = {
    Color1 = "#G",
    Color2 = "#Y",
    Color3 = "#B"
  }
}
function _Chat_Horn:InitShow(chatType)
  local wnd = WindowSys_Instance:GetWindow("Detail.Horn.Main")
  local title = WindowSys_Instance:GetWindow("Detail.Horn.Title_bg")
  local label3 = WindowSys_Instance:GetWindow("Detail.Horn.Label3")
  local editboxSmal = WindowSys_Instance:GetWindow("Detail.Horn.EditSmalHorn")
  local editboxBig = WindowSys_Instance:GetWindow("Detail.Horn.EditBigHorn")
  local editbox
  _Chat_Horn.chatType = g_CHAT_TYPE.ROAR
  title:SetProperty("Text", "狮吼符发言")
  label3:SetProperty("Text", "每次发言将消耗一个大喇叭")
  editbox = editboxBig
  editboxSmal:SetProperty("Visible", "false")
  editboxBig:SetProperty("Visible", "true")
  editbox:SetProperty("Text", "")
  WindowSys_Instance:SetKeyboardCaptureWindow(editbox)
end
function _Chat_Horn:SendResult(result)
  CancelHorn()
  Lua_Detail_ShowChildWnd("Horn", false)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
  Lua_CFace_CloseFaceWnd()
end
function _Chat_Horn:Show(chatType)
  _Chat_Horn:InitShow(chatType)
  Lua_Detail_ShowChildWnd("Horn", true)
end
function _Chat_ShowItemLink(itemid, isGUID)
end
function _Chat_GetChannelWnd(index)
  return gUI.Chat.Channel[index]
end
function _Chat_InitScrollBarRange(page)
  local chatbox = _Chat_GetChannelWnd(page)
  chatbox:MoveToEnd()
end
function _Chat_GetItemLinkTypeName(itemData)
  for index, tabVaule in pairs(_Chat_ItemLinkType) do
    if itemData >= tabVaule.minVaule and itemData <= tabVaule.maxVaule then
      return tabVaule.typeName, index
    end
  end
  return ""
end
function _Chat_AddTextToInputBox(str)
  local box = WindowSys_Instance:GetKeyboardCaptureWindow()
  local str1 = box:GetProperty("Text")
  local count = select(2, string.gsub(str1, "%{##%^%d%d%}", "{##%^%d%d%}"))
  if count > 6 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_FACE_LIMITED)
    return
  end
  if Lua_Chat_CountItem(str1) >= 3 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_ITEM_LIMITED)
    return
  end
  box = WindowToEditBox(box)
  box:InsertText(str)
end
function _Chat_BroadcastToAllPage(str, color)
  for i = gUI_CHANNEL_STR.Integration, gUI_CHANNEL_STR.BattleGrounp - 1 do
    _Chat_DoAddText(i, color, str)
  end
end

function _Chat_AddChatText(chat_type, strMsg, name, exInfo, title, guid, menpaiId, GMLev, errorColor)
  local str = ""
  local current_channel = _Chat_GetChatChannel()
  local index = gUI_SENDMSG_CHANNEL_MAP2[chat_type]
  local indexNew = gUI_SENDMSG_CHANNEL_MAP3[chat_type]
  if index == nil then
    return
  end
  local msg_color = String2Color(gUI_CHAT_CHANNEL_SETTINGINFO[index].COLOR)
  if errorColor ~= nil then
    msg_color = String2Color(errorColor)
  end
  local GmStr = ""
  if GMLev and GMLev > 0 and GMLev ~= 9 then
    GmStr = "{ImAgE^UiBtn012:GM_n}"
    msg_color = String2Color("0xFFFF0611")
  end
  if chat_type == g_CHAT_TYPE.TELLSELF then
    str = "你悄悄的对" .. GmStr .. "{PlAyEr^" .. name .. "^" .. guid .. "^" .. gUI_MenPaiColor[menpaiId] .. "^1}说:" .. strMsg
    Lua_Chat_SetLastWishperName(name)
  elseif chat_type == g_CHAT_TYPE.SELF then
    str = strMsg
  elseif chat_type >= g_CHAT_TYPE.BROADCASTA and chat_type <= g_CHAT_TYPE.BROADCASTC then
    str = strMsg
    _Chat_BroadcastToAllPage(str, msg_color)
    gUI.Chat.BulletinTimer:Stop()
    if errorColor == nil then
      _Chat_DoAddText(index, msg_color, str)
    end
  elseif chat_type == g_CHAT_TYPE.RUMOR then
    local _beg, _end = string.find(strMsg, "%[$[%d]+$%]")
    if _beg then
      local money = string.sub(strMsg, _beg + 2, _end - 2)
      money = tonumber(money)
      local moneyStr = Lua_UI_Money2String(money)
      strMsg = string.gsub(strMsg, "%[$[%d]+$%]", moneyStr)
    end
    str = "{ChAn^" .. tostring(index) .. "^" .. gUI_CHAR_IMG[index] .. "}" .. strMsg
    for i = 0, 7 do
      local flag = GetShowChannelConfig(i, 7)
      if flag == 1 or flag == 2 then
        _Chat_DoAddText(i, msg_color, str)
      end
    end
  elseif chat_type == g_CHAT_TYPE.TELL then
    if not guid then
      str = "[" .. name .. "]对你说:" .. strMsg
      for i = 0, 7 do
        local flag = GetShowChannelConfig(i, 8)
        if flag == 1 or flag == 2 then
          _Chat_DoAddText(i, String2Color(gUI_CHAT_CHANNEL_SETTINGINFO[100].COLOR), str)
        end
      end
      return
    else
      str = GmStr .. "{PlAyEr^" .. name .. "^" .. guid .. "^" .. gUI_MenPaiColor[menpaiId] .. "^1}悄悄的对你说:" .. strMsg
      Lua_Chat_SetLastWishperName(name)
      if AmassGetCurrentAmassState() and UIConfig:getAmassRevert() then
        SendChatMessage(g_CHAT_TYPE.TELL, "正处于聚灵中..", name)
      end
    end
  elseif chat_type == g_CHAT_TYPE.ROAR then
    str = "[喇叭]" .. GmStr .. "{PlAyEr^" .. name .. "^" .. guid .. "^" .. gUI_MenPaiColor[menpaiId] .. "^1}:" .. strMsg
    _Chat_DoAddText(index, msg_color, str)
  elseif index == gUI_CHANNEL_STR.System then
    str = strMsg
  elseif exInfo and tostring(exInfo) == "npcTalk" then
    str = "[" .. name .. "]:" .. strMsg
  else
    local guildInfo = ""
    if chat_type == g_CHAT_TYPE.GUILD and exInfo ~= 0 then
      guildInfo = "[帮主]"
    end
    if name ~= "" and name ~= nil then
      if not guid then
        str = "{ChAn^" .. tostring(index) .. "^" .. gUI_CHAR_IMG[index] .. "}" .. "[" .. name .. "]:" .. strMsg
      elseif chat_type ~= g_CHAT_TYPE.PROP then
        str = "{ChAn^" .. tostring(index) .. "^" .. gUI_CHAR_IMG[index] .. "}" .. GmStr .. guildInfo .. "{PlAyEr^" .. name .. "^" .. guid .. "^" .. gUI_MenPaiColor[menpaiId] .. "^1}:" .. strMsg
      else
        str = "{ChAn^" .. tostring(index) .. "^" .. gUI_CHAR_IMG[index] .. "}" .. GmStr .. guildInfo .. "{PlAyEr^" .. name .. "^" .. guid .. "^" .. gUI_MenPaiColor[menpaiId] .. "^1} {ImAgE^UiIamge020:Image_Spread}:" .. strMsg
      end
    elseif chat_type == g_CHAT_TYPE.PROP then
      str = "{ChAn^" .. tostring(index) .. "^" .. gUI_CHAR_IMG[index] .. "} {ImAgE^UiIamge020:Image_Spread} :" .. strMsg
    else
      str = "{ChAn^" .. tostring(index) .. "^" .. gUI_CHAR_IMG[index] .. "}:" .. strMsg
    end
  end
  if index <= gUI_CHANNEL_STR.BattleGrounp then
    for i = 0, 7 do
      local flag = GetShowChannelConfig(i, indexNew)
      if flag == 1 or flag == 2 then
        _Chat_DoAddText(i, msg_color, str)
      end
    end
  end
end
function _Chat_DoAddText(index, color, str_text)
  if type(str_text) ~= "number" then
    str_text = tostring(str_text)
  end
  local chatshowbox = _Chat_GetChannelWnd(index)
  if index < gUI_CHANNEL_STR.Friend then
    chatshowbox = _Chat_GetChannelWnd(index + 1)
  elseif index == 7 then
    chatshowbox = _Chat_GetChannelWnd(index + 2)
  end
  if index == gUI_CHANNEL_STR.Broadcasta then
    if chatshowbox and chatshowbox:GetProperty("WindowName") == "Post1_dlab" then
      chatshowbox:SetProperty("Visible", "true")
      local wnd = WindowToPicture(WindowSys_Instance:GetWindow("Post.Picture1_bg"))
      wnd:SetProperty("Visible", "true")
      chatshowbox:SetProperty("Text", str_text)
    end
    return
  end
  if index == gUI_CHANNEL_STR.Roar then
    local hornBack = WindowToPicture(WindowSys_Instance:GetWindow("ChatDialog.HornBox_bg"))
    local hornBack1 = WindowToPicture(WindowSys_Instance:GetWindow("ChatDialog.HornBox1_bg"))
    local times = GetCurrSysTime()
    if not hornBack:IsVisible() then
      hornBack:SetProperty("Visible", "true")
      gUI.Chat.Channel[10]:InsertBack(str_text, color, 0, 0)
      gUI.Chat.HornTimer:Restart()
      _Chat_Laba_Time1 = times
      _Chat_Golble_Index_Laba = 1
    elseif not hornBack1:IsVisible() then
      hornBack1:SetProperty("Visible", "true")
      gUI.Chat.Channel[13]:InsertBack(str_text, color, 0, 0)
      gUI.Chat.HornTimer1:Restart()
      _Chat_Laba_Time2 = times
      _Chat_Golble_Index_Laba = 2
    else
      local item = {
        strMsg = str_text,
        color = color,
        times = times,
        index = index
      }
      table.insert(gUI.Chat.LabaMark, item)
      gUI.Chat.HornTimer2:Restart()
    end
    return
  end
  if type(color) ~= "number" then
    color = String2Color(color)
  end
  local old_channel = _Chat_GetChannelWnd(_Chat_GetChatChannel())
  local pageWindow = gUI.Chat.Chat_InfoWindow.pageWindow
  local indexSel = WindowToTableCtrl(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg.Info_tctl")):GetSelectItem()
  if indexSel == 1 then
  else
    if indexSel == 0 and old_channel == chatshowbox then
      local ScrollBar = WindowToScrollBar(chatshowbox:GetChildByName("vertSB_"))
      if ScrollBar:IsVisible() then
        local CheckBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg.Lock_cbtn"))
        if CheckBtn:IsChecked() then
          _Chat_SetCurrSay_Show(str_text, color)
        else
          _Chat_SetCurrSay_Show("", 0)
        end
      end
    else
    end
  end
  if chatshowbox ~= nil then
    chatshowbox:InsertBack(str_text, color, 0, 0)
  end
end
function _Chat_SetCurrSay_Show(str, color)
  gUI.Chat.CurrSayBtn:ClearAllContent()
  local wndChat = WindowSys_Instance:GetWindow("ChatDialog")
  local y = wndChat:GetRenderHeight()
  if str == "" then
    gUI.Chat.CurrSayBtn1:SetProperty("Visible", "false")
    gUI.Chat.CurrSayBtn2:SetProperty("Visible", "false")
    gUI.Chat.CurrSayBtn:SetProperty("Visible", "false")
    if _CHAT_HAS_CHANGE == true then
      gUI.Chat.CtrlsBg:SetTop(gUI.Chat.CtrlsBg:GetTop() - 19 / y)
      gUI.Chat.InputsBg:SetTop(gUI.Chat.InputsBg:GetTop() - 19 / y)
      gUI.Chat.ShowBtn:SetTop(gUI.Chat.ShowBtn:GetTop() - 19 / y)
      gUI.Chat.HideBtn:SetTop(gUI.Chat.HideBtn:GetTop() - 19 / y)
      _CHAT_HAS_CHANGE = false
    end
  else
    local btn = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.Show_btn")
    if not btn:IsVisible() then
      gUI.Chat.CurrSayBtn1:SetProperty("Visible", "true")
      gUI.Chat.CurrSayBtn2:SetProperty("Visible", "true")
      gUI.Chat.CurrSayBtn:SetProperty("Visible", "true")
      str = Lua_UI_TranslateTextNew(str)
      gUI.Chat.CurrSayBtn:InsertBack(str, color, 0, 0)
      if _CHAT_HAS_CHANGE == false then
        gUI.Chat.CtrlsBg:SetTop(gUI.Chat.CtrlsBg:GetTop() + 19 / y)
        gUI.Chat.InputsBg:SetTop(gUI.Chat.InputsBg:GetTop() + 19 / y)
        gUI.Chat.ShowBtn:SetTop(gUI.Chat.ShowBtn:GetTop() + 19 / y)
        gUI.Chat.HideBtn:SetTop(gUI.Chat.HideBtn:GetTop() + 19 / y)
        _CHAT_HAS_CHANGE = true
      end
    end
  end
end
function Lua_UI_TranslateTextNew(Text)
  return string.gsub(Text, "%{##%^%d%d%d%}", "%[图片]")
end
function _Chat_HornTimer(timer)
  local hornBack = WindowToPicture(WindowSys_Instance:GetWindow("ChatDialog.HornBox_bg"))
  hornBack:SetProperty("Visible", "false")
  _Chat_Laba_Time1 = 0
end
function _Chat_HornTimer1(timer)
  local hornBack = WindowToPicture(WindowSys_Instance:GetWindow("ChatDialog.HornBox1_bg"))
  hornBack:SetProperty("Visible", "false")
  _Chat_Laba_Time2 = 0
end
function _Chat_ShowQuestInfo(QuestId)
end
function _Chat_FormatChat(str)
  local name = ""
  local msg = ""
  local i, j
  i, j, name, msg = string.find(str, "对([^%s]+)说：(.*)")
  if i == 1 and name and name ~= "" then
    if msg and msg ~= "" then
      return name, msg
    else
      return name, ""
    end
  end
  i, j, name, msg = string.find(str, "/([^%s]+)%s+(.*)")
  if i == 1 and name and name ~= "" then
    if msg and msg ~= "" then
      return name, msg
    else
      return name, ""
    end
  end
  return "", str
end
function _Chat_ShowNameEbox(name, visible)
  gUI.Chat.NameEBox:SetVisible(visible)
  gUI.Chat.Namebg:SetVisible(visible)
  if visible then
    if name ~= nil and name ~= "" then
      gUI.Chat.NameEBox:SetProperty("Text", name)
    else
      gUI.Chat.NameEBox:SetProperty("Text", "")
    end
    local enterBtnLeft = gUI.Chat.EnterBtn:GetLeft()
    local labRight = gUI.Chat.Namebg:GetRight()
    gUI.Chat.MainInputBox:SetLeft(labRight + 0.001)
    gUI.Chat.MainInputBox:SetWidth(enterBtnLeft - labRight)
  else
    local labLeft = gUI.Chat.NameEBox:GetLeft()
    local enterBtnLeft = gUI.Chat.EnterBtn:GetLeft()
    gUI.Chat.MainInputBox:SetLeft(labLeft)
    gUI.Chat.MainInputBox:SetWidth(enterBtnLeft - labLeft)
  end
end
function _Chat_LoopBulletinShow()
  AddABulletin()
end
function _Chat_GetTalkName()
  if gUI.Chat.NameEBox:IsVisible() then
    return gUI.Chat.NameEBox:GetProperty("Text")
  end
  return ""
end
function _Chat_SetChatChannel(index)
  gUI.Chat.nCurrentChannel = index
end
function _Chat_GetChatChannel()
  return gUI.Chat.nCurrentChannel
end
function _Chat_SetChatType(index)
  gUI.Chat.nCurrentChatType = index
end
function _Chat_GetChatType()
  return gUI.Chat.nCurrentChatType
end
function _Chat_ShowPaopao(tarId, contStr, chatInfo)
  ShowPaopao(tarId, contStr)
  if chatInfo == nil then
    return
  end
  local chatType = chatInfo.chatType
  local tarName = chatInfo.tarName
  if chatType == 0 then
    _Chat_AddChatText(g_CHAT_TYPE.NORMAL, contStr, tarName, "npcTalk", nil, nil, nil, nil)
  elseif chatType == 1 then
    _Chat_AddChatText(g_CHAT_TYPE.TELL, contStr, tarName, "npcTalk", nil, nil, nil, nil)
  end
end
function Lua_Chat_SelectChatChannel(index, chatName)
  local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
  pic = WindowToPicture(pic)
  pic:OnFadeIn()
  if index >= gUI_CHANNEL_STR.Integration and index <= gUI_CHANNEL_STR.BattleGrounp then
    local RealINDEX = gUI_CHANNEL_INDEX_RELATION[index]
    _Chat_SetChatType(RealINDEX)
    local realIndex = _Chat_GetChatType()
    gUI.Chat.ChannelBtn:SetProperty("Text", gUI_CHAT_CHANNEL_SETTINGINFO[realIndex].CHANNEL_NAME)
    gUI.Chat.MainInputBox:SetFontColor(String2Color(gUI_CHAT_CHANNEL_SETTINGINFO[realIndex].COLOR))
    gUI.Chat.MainInputBox:SetProperty("DefultColor", gUI_CHAT_CHANNEL_SETTINGINFO[realIndex].COLOR)
    gUI.Chat.MainInputBox:SetProperty("MaxLength", tostring(gUI_CHAT_CHANNEL_SETTINGINFO[realIndex].MAXWORDLIMIT))
    if realIndex == 3 then
      if chatName ~= nil or chatName ~= "" then
        _Chat_ShowNameEbox(chatName, true)
      else
        _Chat_ShowNameEbox(Lua_Chat_GetLastWishperName(0), true)
      end
    else
      _Chat_ShowNameEbox("", false)
    end
  end
end
function Lua_Chat_ShowSysLog(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  _OnChat_AddSysLog(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
end
function Lua_Chat_ShowOSD(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  local str = UI_SYS_MSG[arg1]
  if str == nil then
    ShowErrorMessage(tostring(arg1) .. "--有参数错误!")
    return
  end
  if arg1 == "GET_MONEY" or arg1 == "GET_BIND_MONEY" or arg1 == "USE_MONEY" or arg1 == "USE_BIND_MONEY" or "GUILD_MONEYUP" == arg1 then
    arg2 = Lua_UI_Money2String(tonumber(arg2))
  end
  count = select(2, string.gsub(str, "%%", "%%"))
  local paraList = {
    arg2,
    arg3,
    arg4,
    arg5,
    arg6,
    arg7
  }
  if count > 0 then
    for i = 1, count do
      if paraList[i] == nil then
        ShowErrorMessage(str .. "--有参数错误!")
        return
      end
    end
  end
  str = string.format(str, arg2, arg3, arg4, arg5, arg6, arg7)
  ShowErrorMessage(str)
end
function Lua_Chat_ShowNotity(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  local str = UI_SYS_MSG[arg1]
  if str == nil then
    ShowErrorMessage(tostring(arg1) .. "--有参数错误!")
    return
  end
  if arg1 == "GET_MONEY" or arg1 == "GET_BIND_MONEY" or arg1 == "USE_MONEY" or arg1 == "USE_BIND_MONEY" or "GUILD_MONEYUP" == arg1 then
    arg2 = Lua_UI_Money2String(tonumber(arg2))
  end
  count = select(2, string.gsub(str, "%%", "%%"))
  local paraList = {
    arg2,
    arg3,
    arg4,
    arg5,
    arg6,
    arg7
  }
  if count > 0 then
    for i = 1, count do
      if paraList[i] == nil then
        ShowErrorMessage(str .. "--有参数错误!")
        return
      end
    end
  end
  str = string.format(str, arg2, arg3, arg4, arg5, arg6, arg7)
  local color = "0xFFFFB400"
  if arg0 == 0 then
    color = "0xFFFFB400"
  elseif arg0 == 1 then
    color = "0xFFFF0611"
  elseif arg0 == 2 then
    color = "0xFF48D1FF"
  end
  _Chat_BroadcastToAllPage(str, color)
end
function Lua_Chat_CHAT(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  local str = UI_OSD[arg1]
  if str == nil then
    ShowErrorMessage(tostring(arg1) .. "--有参数错误!")
    return
  end
  count = select(2, string.gsub(str, "%%", "%%"))
  local paraList = {
    arg2,
    arg3,
    arg4,
    arg5,
    arg6,
    arg7
  }
  if count > 0 then
    for i = 1, count do
      if paraList[i] == nil then
        ShowErrorMessage(str .. "--有参数错误!")
        return
      end
    end
  end
  str = string.format(str, arg2, arg3, arg4, arg5, arg6, arg7)
  _Chat_BroadcastToAllPage(str, "0xFFFFB400")
end
function Lua_Chat_ShowInChatZone(StrOperate, str, nType)
  if nType < gUI_CHANNEL_STR.Integration or nType > gUI_CHANNEL_STR.BattleGrounp then
    ShowErrorMessage(tostring(str) .. "--有参数错误!")
    return
  end
  _Chat_DoAddText(nType, String2Color(gUI_CHAT_CHANNEL_SETTINGINFO[nType].COLOR), str)
end
function Lua_Chat_SetLastWishperName(name)
  for i = 0, 4 do
    if gUI.Chat.WhisperHis[i] == name then
      for j = i, 3 do
        if j <= 3 then
          gUI.Chat.WhisperHis[j] = gUI.Chat.WhisperHis[j + 1]
        end
      end
      break
    end
  end
  for i = 0, 3 do
    gUI.Chat.WhisperHis[3 - i + 1] = gUI.Chat.WhisperHis[3 - i]
  end
  gUI.Chat.WhisperHis[0] = name
end
function Lua_Chat_GetLastWishperName(index)
  return gUI.Chat.WhisperHis[index]
end
function Lua_Chat_GetTabFouse(index)
  name = Lua_Chat_GetLastWishperName(0)
  if name == "" then
    return
  end
  if _Chat_Golble_Index > 4 then
    _Chat_Golble_Index = 0
  end
  local wnd = WindowSys_Instance:GetKeyboardCaptureWindow()
  if wnd == nil then
    return
  end
  local wndType = wnd:GetProperty("WindowName")
  if wndType == "Input_ebox" or wndType == "name_ebox" then
    name = Lua_Chat_GetLastWishperName(_Chat_Golble_Index)
    if name ~= "" then
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, name)
      _Chat_Golble_Index = _Chat_Golble_Index + 1
    else
      _Chat_Golble_Index = 0
      Lua_Chat_GetTabFouse(0)
    end
  end
end
function Lua_Chat_ReplyByR()
  local wnd = WindowSys_Instance:GetKeyboardCaptureWindow()
  if wnd == nil then
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
    wnd = WindowSys_Instance:GetKeyboardCaptureWindow()
  end
  local wndType = wnd:GetProperty("ClassName")
  if wndType == "EditBox" then
    return
  end
  name = Lua_Chat_GetLastWishperName(0)
  if name ~= "" then
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, name)
  end
end
function Lua_Chat_AddFightLog(str)
  gUI.Chat.Chat_InfoWindow:InsertMsg(2, str)
end
function Lua_Chat_AddSysLog(str)
  gUI.Chat.Chat_InfoWindow:InsertMsg(1, str)
end
function Lua_Chat_CountItem(str)
  local itemCount = select(2, string.gsub(str, "ItEm^", "ItEm^"))
  local questCount = select(2, string.gsub(str, "QuEsT^", "QuEsT^"))
  return itemCount + questCount
end
function Lua_Chat_SelectDisplayChannel(index, name)
  local tc = WindowToTableCtrl(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Channels_tctl"))
  local old_channel = _Chat_GetChannelWnd(_Chat_GetChatChannel())
  old_channel:SetProperty("Visible", "false")
  local RealIndex = gUI_CHANNEL_INDEX_RELATION[index]
  local new_channel = _Chat_GetChannelWnd(RealIndex)
  new_channel:SetProperty("Visible", "true")
  _Chat_SetChatChannel(RealIndex)
  Lua_Chat_SelectChatChannel(index)
  _Chat_InitScrollBarRange(RealIndex)
  new_channel:UpdateScrollBarConfig()
  WindowToTableCtrl(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg.Info_tctl")):SelectItem(0)
end
function Lua_Chat_SelectDisplayChannelNew(index)
  local old_channel = _Chat_GetChannelWnd(_Chat_GetChatChannel())
  local pageWindow = gUI.Chat.Chat_InfoWindow.pageWindow
  if index == 1 then
    old_channel:SetProperty("Visible", "false")
    pageWindow[2]:SetProperty("Visible", "true")
  elseif index == 0 then
    old_channel:SetProperty("Visible", "true")
    pageWindow[2]:SetProperty("Visible", "false")
  end
end
function UI_ClearCurrContent_Click(msg)
  local index = WindowToTableCtrl(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg.Info_tctl")):GetSelectItem()
  local pageWindow = gUI.Chat.Chat_InfoWindow.pageWindow
  if index == 1 then
    WindowToDisplayBox(pageWindow[2]):ClearAllContent()
    return
  end
  local curr_channel = _Chat_GetChannelWnd(_Chat_GetChatChannel())
  curr_channel:ClearAllContent()
  local pic2 = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.HintEffect_bg")
  local pic3 = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.CurrentSay_btn")
  local pic4 = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.CurrentSay_dbox")
  if pic2:IsVisible() then
    pic2:SetProperty("Visible", "false")
  end
  if pic3:IsVisible() then
    pic3:SetProperty("Visible", "false")
    pic4:SetProperty("Visible", "false")
    if _CHAT_HAS_CHANGE == true then
      local wndChat = WindowSys_Instance:GetWindow("ChatDialog")
      local y = wndChat:GetRenderHeight()
      gUI.Chat.CtrlsBg:SetTop(gUI.Chat.CtrlsBg:GetTop() - 19 / y)
      gUI.Chat.InputsBg:SetTop(gUI.Chat.InputsBg:GetTop() - 19 / y)
      gUI.Chat.ShowBtn:SetTop(gUI.Chat.ShowBtn:GetTop() - 19 / y)
      gUI.Chat.HideBtn:SetTop(gUI.Chat.HideBtn:GetTop() - 19 / y)
      _CHAT_HAS_CHANGE = false
    end
  end
end
function Lua_Chat_AddLinkItem(bag, slot)
  local box = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.Input_ebox")
  local edit_box = WindowToEditBox(box)
  if not edit_box:IsKeyboardCaptureWindow() then
    local curCaptureWnd = WindowSys_Instance:GetKeyboardCaptureWindow()
    local wndType = curCaptureWnd:GetProperty("ClassName")
    if wndType == "EditBox" then
      box = curCaptureWnd
    else
      WindowSys_Instance:SetKeyboardCaptureWindow(box)
      local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
      pic = WindowToPicture(pic)
      pic:OnFadeIn()
    end
  end
  local num, id, quality, name, IdTable, _, _, _, _, _, _, strGuid = GetItemBaseInfoBySlot(bag, slot)
  if strGuid then
    local str = "{ItEm^" .. "0" .. "^" .. strGuid .. "^" .. tostring(IdTable) .. "^" .. tostring(quality) .. "}"
    _Chat_AddTextToInputBox(str)
  end
end
function UI_Chat_ItemClick(msg)
  local box = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.Input_ebox")
  local edit_box = WindowToEditBox(box)
  if not edit_box:IsKeyboardCaptureWindow() then
    local curCaptureWnd = WindowSys_Instance:GetKeyboardCaptureWindow()
    local wndType = curCaptureWnd:GetProperty("ClassName")
    if wndType == "EditBox" then
      box = curCaptureWnd
    else
      WindowSys_Instance:SetKeyboardCaptureWindow(box)
      local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
      pic = WindowToPicture(pic)
      pic:OnFadeIn()
    end
  end
  local to_win = WindowToEditBox(msg:get_window())
  local itemId = msg:get_wparam()
  local form_win = to_win:GetDragItemParent()
  if form_win then
    local form_type = form_win:GetProperty("BoxType")
    if form_type == "backpack0" then
      local num, id, quality, name, IdTable, _, _, _, _, _, _, strGuid = GetItemBaseInfoBySlot(1, itemId)
      if strGuid then
        local str = "{ItEm^" .. "0^" .. strGuid .. "^" .. tostring(IdTable) .. "^" .. tostring(quality) .. "}"
        _Chat_AddTextToInputBox(str)
      end
    end
  end
end
function Lua_Chat_ChangeChannelByPageBtn(strDir)
  local curIndex = _Chat_GetChatType()
  if strDir == "forward" and curIndex + 1 < gUI_CHANNEL_STR.BattleGrounp then
    local newIndex = curIndex + 1
    if newIndex == gUI_CHANNEL_STR.Map then
      newIndex = gUI_CHANNEL_STR.BattleGrounp
    end
    local RealIndex = gUI_CHANNEL_INDEX_RELATION1[newIndex]
    Lua_Chat_SelectChatChannel(RealIndex)
  elseif strDir == "backward" and curIndex - 1 > gUI_CHANNEL_STR.Integration then
    local newIndex = curIndex - 1
    if newIndex == gUI_CHANNEL_STR.Map then
      newIndex = gUI_CHANNEL_STR.Friend
    end
    local RealIndex = gUI_CHANNEL_INDEX_RELATION1[newIndex]
    Lua_Chat_SelectChatChannel(RealIndex)
  end
end
function Lua_Chat_ApplyPaoPao(paoPaoId)
  local paoItemT = Chat_PaoPao_Content[paoPaoId]
  if paoItemT == nil then
    Lua_Chat_ShowLuaErr("paopao配置错误，没有id为" .. paoPaoId .. "的泡泡")
    return
  end
  if paoItemT.mapId == nil or paoItemT.npcId == nil or paoItemT.contStr == nil then
    Lua_Chat_ShowLuaErr("paopao配置错误，id为" .. paoPaoId .. "的泡泡")
    return
  end
  local mapId = GetMPlayerMapId()
  if mapId ~= paoItemT.mapId then
    return
  end
  local targetId = -1
  if paoItemT.npcId == -1 then
  else
    targetId = GetNPCObjId(paoItemT.npcId)
    if targetId == -1 then
      return
    end
  end
  local paoItem = {}
  paoItem.id = paoPaoId
  paoItem.tarId = targetId
  local curTime = TimerSys_Instance:GetCurrTime()
  paoItem.startTime = curTime
  paoItem.phase = 0
  table.insert(gUI.Chat.PaoList, paoItem)
  local item = Chat_PaoPao_Content[paoItem.id]
  if item.contStr[0] ~= "" then
    _Chat_ShowPaopao(paoItem.tarId, item.contStr[0], paoItemT.chat)
  end
end
function Lua_Chat_ApplyAuraPaoPao(AuraID, StackNum)
  if StackNum > 0 then
    local paoStackId = Aura_PaoPao_Content[AuraID]
    if paoStackId == nil then
      return
    end
    local paoId = paoStackId[StackNum]
    if paoId == nil then
      return
    end
    Lua_Chat_ApplyPaoPao(paoId)
  end
end
function Lua_Chat_UpdataPaopaoList()
  local curTime = TimerSys_Instance:GetCurrTime()
  for i, n in pairs(gUI.Chat.PaoList) do
    local item = Chat_PaoPao_Content[n.id]
    if n.startTime == -1 then
      n.startTime = curTime
      n.phase = 0
      if item.contStr[0] ~= "" then
        _Chat_ShowPaopao(n.tarId, item.contStr[0], item.chat)
      end
    else
      local elapseTime = curTime - n.startTime
      if elapseTime > item.contTime[n.phase] then
        if item.contTime[n.phase + 1] ~= nil then
          n.startTime = curTime
          n.phase = n.phase + 1
          if item.contStr[n.phase] ~= "" then
            _Chat_ShowPaopao(n.tarId, item.contStr[n.phase], item.chat)
          end
        else
          table.remove(gUI.Chat.PaoList, i)
        end
      end
    end
  end
end
function Lua_Chat_OSDBottom(id)
  ShowErrorMessageBottom(UI_OSD_BOTTOM[id])
end
function Lua_Chat_OSD(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  local str = UI_OSD[arg1]
  if str == nil then
    ShowErrorMessage(tostring(arg1) .. "--有参数错误!")
    return
  end
  count = select(2, string.gsub(str, "%%", "%%"))
  local paraList = {
    arg2,
    arg3,
    arg4,
    arg5,
    arg6,
    arg7
  }
  if count > 0 then
    for i = 1, count do
      if paraList[i] == nil then
        ShowErrorMessage(str .. "--有参数错误!")
        return
      end
    end
  end
  str = string.format(str, arg2, arg3, arg4, arg5, arg6, arg7)
  ShowErrorMessage(str)
end
function Lua_Chat_ShowLuaErr(str)
  _Chat_AddChatText(g_CHAT_TYPE.BROADCASTA, str, nil, "", nil, nil, nil, nil, "0xFFFFB400")
end
function Lua_Chat_ShowEngineErr(str)
  _Chat_AddChatText(g_CHAT_TYPE.BROADCASTA, str, nil, "", nil, nil, nil, nil, "0xFFFFFF00")
end
function Lua_Chat_MoveBtnClick(hori, ver)
  windowMovedList = {
    [0] = "Input_bg",
    [1] = "ChatBox_bg",
    [2] = "HornBox_bg",
    [3] = "HornBox1_bg"
  }
  local wnd
  for i, name in pairs(windowMovedList) do
    wnd = WindowSys_Instance:GetWindow("ChatDialog." .. name)
    wnd:SetLeft(wnd:GetLeft() + hori)
    wnd:SetTop(wnd:GetTop() + ver)
  end
end
function Lua_Chat_MoveBtnClick_Sys(hori, ver)
  windowMovedList = {
    [0] = "FightBox_bg",
    [1] = "FightBox_dbox",
    [2] = "Common_dbox",
    [3] = "System_dbox",
    [4] = "Page_tctl",
    [5] = "ResetSize_btn",
    [6] = "lock_btn"
  }
  local wnd
  for i, name in pairs(windowMovedList) do
    wnd = WindowSys_Instance:GetWindow("FightMessage." .. name)
    wnd:SetLeft(wnd:GetLeft() + hori)
    wnd:SetTop(wnd:GetTop() + ver)
  end
end
function _OnChat_EnterCombat(arg1, arg2)
  local wnd = WindowSys_Instance:GetWindow("PlayerStats.TopSelf_bg.Stats_bg.Combat_bg")
  if arg1 then
    if not arg2 then
      Lua_Chat_AddFightLog(UI_FIGHT_MSG.INTO_COMBAT_MODE)
      Messagebox_EnableMsgTypeOkBtn("SUMMON_REQUEST", false)
    end
    wnd:SetVisible(true)
  else
    if not arg2 then
      Lua_Chat_AddFightLog(UI_FIGHT_MSG.EXIT_COMBAT_MODE)
      Messagebox_EnableMsgTypeOkBtn("SUMMON_REQUEST", true)
    end
    wnd:SetVisible(false)
  end
end
function _OnChat_HornShow(arg1)
  _Chat_Horn:Show(arg1)
end
function _OnChat_HornClose(arg1)
  _Chat_Horn:SendResult(arg1)
end
function _OnChat_OSDShow(timer)
  _Chat_OSDTime = _Chat_OSDTime - timer:GetRealInterval()
  if _Chat_OSDTime < 0 then
    local wnd = WindowSys_Instance:GetWindow("EfxNoLoop_pic.Duel_pic")
    wnd:SetVisible(false)
    DelTimerEvent(1, "ShowDuelOSD")
  end
end
function _OnChat_AddFightLog(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  local str = UI_FIGHT_MSG[arg1]
  if arg2 and str then
    if arg1 == "COMBAT_BE_KILL" then
      Lua_Relive_SetKillerName(arg2)
      str = string.format(str, arg2, arg3, arg4, arg5, arg6, arg7)
    elseif arg1 == "GET_MONEY" or arg1 == "GET_BIND_MONEY" or arg1 == "USE_MONEY" or arg1 == "USE_BIND_MONEY" or "GUILD_MONEYUP" == arg1 then
      str = string.format(str, Lua_UI_Money2String(arg2))
    else
      str = string.format(str, arg2, arg3, arg4, arg5, arg6, arg7)
    end
  end
  if str then
    Lua_Chat_AddFightLog(str)
  end
end
function _OnChat_RecvChat(arg1, arg2, arg3, arg4, arg5, arg6, menpaiId, GMLev)
  _Chat_AddChatText(arg1, arg2, arg3, arg4, arg5, arg6, menpaiId, GMLev)
  if arg1 == g_CHAT_TYPE.TELL then
    WorldStage:playSoundByID(17)
  end
end
function _OnChat_UpdateRewardItem(Index, GoodsId, Icon, GoodsCount, sourceId)
  if sourceId == gUI_QuestRewardItemSourceID.CHAT_QUEST_LINK then
    gUI.Chat.QuestGoodsBox:SetItemGoods(Index, Icon, Lua_Bag_GetQuality(-1, GoodsId))
    gUI.Chat.QuestGoodsBox:SetItemData(Index, GoodsId)
    if GoodsCount > 1 then
      gUI.Chat.QuestGoodsBox:SetItemSubscript(Index, tostring(GoodsCount))
    end
    if GoodsCount > 5 then
      gUI.Chat.QuestGoodsBox:SetProperty("GoodsRow", "2")
    end
  end
end
function _OnChat_AddSysLog(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  local str = UI_SYS_MSG[arg1]
  if str == nil then
    Lua_Chat_AddSysLog(tostring(arg1) .. "--有参数错误!")
    return
  end
  if arg1 == "GET_MONEY" or arg1 == "GET_BIND_MONEY" or arg1 == "USE_MONEY" or arg1 == "USE_BIND_MONEY" or "GUILD_MONEYUP" == arg1 then
    arg2 = Lua_UI_Money2String(tonumber(arg2))
  end
  count = select(2, string.gsub(str, "%%", "%%"))
  local paraList = {
    arg2,
    arg3,
    arg4,
    arg5,
    arg6,
    arg7
  }
  if count > 0 then
    for i = 1, count do
      if paraList[i] == nil then
        Lua_Chat_AddSysLog(str .. "--有参数错误!")
        return
      end
    end
  end
  str = string.format(str, arg2, arg3, arg4, arg5, arg6, arg7)
  Lua_Chat_AddSysLog(str)
  Lua_Chat_DealSpeacil(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
end
function Lua_Chat_DealSpeacil(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  if arg1 == "DUEL_SUCCESS" then
    local wnd = WindowSys_Instance:GetWindow("EfxNoLoop_pic.Duel_pic")
    wnd:setEffectFile("efxc_ui_xianshi_shengli")
    wnd:SetVisible(true)
    _Chat_OSDTime = 2
    AddTimerEvent(1, "ShowDuelOSD", _OnChat_OSDShow)
    WorldStage:playSoundByID(19)
  elseif arg1 == "DUEL_FAILED" then
    local wnd = WindowSys_Instance:GetWindow("EfxNoLoop_pic.Duel_pic")
    wnd:setEffectFile("efxc_ui_xianshi_shibai")
    wnd:SetVisible(true)
    _Chat_OSDTime = 2
    AddTimerEvent(1, "ShowDuelOSD", _OnChat_OSDShow)
    WorldStage:playSoundByID(20)
  end
end
function _OnChat_DuelInvitationReceive(arg1)
  if arg1 then
    WorldStage:playSoundByID(18)
  end
end
function _OnChat_GuildWarRe(selfGuild, killCount, enemyGuild, killCountE, selfVIPName, meCount, enemyVIPName, enemyCount)
  local str = string.format("[%s]当前击倒数为%d人", selfGuild, killCount)
  if meCount and meCount > 0 then
    str = str .. string.format("，MVP为[%s]击倒%d人", selfVIPName, meCount)
  end
  _Chat_AddChatText(g_CHAT_TYPE.BROADCASTA, str, nil, "", nil, nil, nil, nil)
  str = string.format("[%s]当前击倒数为%d人", enemyGuild, killCountE)
  if enemyCount and enemyCount > 0 then
    str = str .. string.format("，MVP为[%s]击倒%d人", enemyVIPName, enemyCount)
  end
  _Chat_AddChatText(g_CHAT_TYPE.BROADCASTA, str, nil, "", nil, nil, nil, nil)
end
function UI_Chat_NameKeyDown(msg)
end
function UI_Chat_KeyDown(msg)
  if msg:get_wparam() == 200 and IsKeyPressed(29) then
    Lua_Chat_ChangeChannelByPageBtn("forward")
  end
  if msg:get_wparam() == 208 and IsKeyPressed(29) then
    Lua_Chat_ChangeChannelByPageBtn("backward")
  end
end
function UI_Chat_SelectPage(msg)
end
function UI_Chat_MouseEnter(msg)
  local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
  pic = WindowToPicture(pic)
  pic:SetProperty("ShowTime", "500")
  pic:OnFadeIn()
end
function UI_Chat_MouseLeave(msg)
  local inputbox = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.Input_ebox")
  local namebox = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.name_ebox")
  if inputbox:IsKeyboardCaptureWindow() or namebox:IsKeyboardCaptureWindow() then
    return
  end
  local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
  pic = WindowToPicture(pic)
  pic:SetProperty("HideTime", "5000")
  pic:OnFadeOut()
  Lua_CFace_CloseFaceWnd()
end
function UI_Chat_FilterShow3(msg)
  Lua_Chat_OnOpenSetUserDefine()
end
function UI_Chat_ScrollEnd()
  gUI.Chat.Channel[11]:SetProperty("Visible", "false")
  local wnd = WindowToPicture(WindowSys_Instance:GetWindow("Post.Picture1_bg"))
  wnd:SetProperty("Visible", "false")
  gUI.Chat.BulletinTimer:Restart()
end
function UI_Chat_ChangeChannelBySpaceBtn(msg)
  str = gUI.Chat.MainInputBox:GetProperty("Text")
  local name = ""
  local msg = ""
  if IsDebugCmd(str) == false then
    i, j, name, msg = string.find(str, "/([^%s]+)%s")
    if i == 1 and name and name ~= "" then
      local index = gUI_CHATCHANNEL_QUICK_NAME[name]
      local realIndex = gUI_CHANNEL_INDEX_RELATION1[index]
      if realIndex then
        Lua_Chat_SelectChatChannel(realIndex)
        gUI.Chat.MainInputBox:SetProperty("Text", "")
      else
        local len = string.len(name)
        if len <= 12 then
          Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell)
          _Chat_ShowNameEbox(name, true)
          gUI.Chat.MainInputBox:SetProperty("Text", "")
        end
      end
    end
  end
end
function UI_Chat_QuestInfoClose(msg)
  local root = WindowSys_Instance:GetWindow("TooltipQuest")
  root:SetProperty("Visible", "false")
end
function UI_Chat_SelectChannel(msg)
  local index = msg:get_wparam()
  Lua_Chat_SelectDisplayChannel(index)
  _Chat_SetCurrSay_Show("")
end
function UI_Chat_SelectChannelNew(msg)
  local index = msg:get_wparam()
  Lua_Chat_SelectDisplayChannelNew(index)
end
function UI_Channels_HoldOn(msg)
  local index = msg:get_wparam()
  local wnd = msg:get_window()
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  if index <= 0 or index > 8 then
    return
  end
  Lua_Tip_Begin(tooltip)
  index = index - 1
  local RealIndex = gUI_CHANNEL_INDEX_RELATION[index]
  local desc = gUI_CHAT_CHANNEL_SETTINGINFO[RealIndex].TIPDES
  tooltip:InsertLeftPraseText(tostring(desc), colorUserDefine, "", 0, 0)
  Lua_Tip_End(tooltip)
  local posx, posy = GetMousePos()
  tooltip:SetLeft(posx + 0.01)
  tooltip:SetTop(posy - 0.1)
end
function UI_ShowChatDialog_Tip(msg)
  local wnd = msg:get_window()
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local desc = wnd:GetProperty("TooltipText")
  local desc1 = wnd:GetProperty("TooltipTextDes")
  if desc == "" then
    return
  end
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftPraseText(tostring(desc), colorUserDefine, "", 0, 0)
  if desc1 ~= "" then
    tooltip:InsertLeftPraseText(tostring(desc1), colorUserDefine, "", 0, 0)
  end
  Lua_Tip_End(tooltip)
end
function UI_ChatButton_Tip(msg)
  local wnd = msg:get_window()
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftPraseText(tostring("点击直接显示页尾信息"), colorUserDefine, "", 0, 0)
  Lua_Tip_End(tooltip)
end
function UI_Chat_ColorCheckHandler(msg)
  Lua_UI_DealRadioButton(msg, _Chat_Horn.ColorChoseRadio)
end
function UI_Chat_HornSend(msg)
  local editboxBig = WindowSys_Instance:GetWindow("Detail.Horn.EditBigHorn")
  local context = editboxBig:GetProperty("Text")
  local count = select(2, string.gsub(context, "%{##%^%d%d%}", "{##%^%d%d%}"))
  if count > 6 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_FACE_LIMITED)
    return
  end
  if Lua_Chat_CountItem(context) > 3 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_ITEM_LIMITED)
    return
  end
  if string.len(context) <= 0 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_CONTEXT_IS_EMPTY)
    return
  end
  WorldGroup_ShowNewStr(UI_SYS_MSG.BIG_HORN_SEND_SUCCESS)
  SendChatMessage(g_CHAT_TYPE.ROAR, context, "")
  editboxBig = WindowToEditBox(editboxBig)
  if editboxBig then
    editboxBig:SaveHistoryText()
  end
end
function UI_Chat_SysMouseEnter()
  local pic = WindowSys_Instance:GetWindow("FightMessage")
  pic = WindowToPicture(pic)
  pic:OnFadeIn()
end
function UI_Chat_SysMouseLeave()
  local pic = WindowSys_Instance:GetWindow("FightMessage")
  pic = WindowToPicture(pic)
  pic:OnFadeOut()
end
function UI_Chat_CloseBtnClick(msg)
  WindowSys_Instance:GetWindow("TooltipWindows.Tooltip1"):SetProperty("Visible", "false")
end
function UI_Chat_facebtnclick(msg)
  local faceWnd = WindowSys_Instance:GetWindow("ChatFace")
  if faceWnd:IsVisible() then
    faceWnd:SetProperty("Visible", "false")
  else
    faceWnd:SetProperty("Visible", "true")
    local tablectrl = WindowSys_Instance:GetWindow("ChatFace.CommonFace_tctl")
    local tablectrl = WindowToTableCtrl(tablectrl)
    local selected = tablectrl:GetSelectItem()
    _CFace_ShowPage(selected)
    local editChatBox = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.Input_ebox")
    WindowSys_Instance:SetKeyboardCaptureWindow(editChatBox)
    _CFace_SetCurChatEditBox(editChatBox)
    local chatdialog = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Channels_tctl")
    local rect = chatdialog:GetWndRect()
    faceWnd:SetLeft(rect:get_left() + rect:get_width())
    faceWnd:SetBottom(rect:get_top() + rect:get_height())
  end
end
function UI_Chat_HornFaceShow(msg)
  local faceWnd = WindowSys_Instance:GetWindow("ChatFace")
  if faceWnd:IsVisible() then
    faceWnd:SetProperty("Visible", "false")
  else
    faceWnd:SetProperty("Visible", "true")
    local tablectrl = WindowSys_Instance:GetWindow("ChatFace.CommonFace_tctl")
    local tablectrl = WindowToTableCtrl(tablectrl)
    local selected = tablectrl:GetSelectItem()
    _CFace_ShowPage(selected)
    local editboxBig = WindowSys_Instance:GetWindow("Detail.Horn.EditBigHorn")
    local editChatBox
    editChatBox = editboxBig
    WindowSys_Instance:SetKeyboardCaptureWindow(editChatBox)
    _CFace_SetCurChatEditBox(editChatBox)
    local chatdialog = WindowSys_Instance:GetWindow("Detail.Horn.Face")
    local rect = chatdialog:GetWndRect()
    faceWnd:SetLeft(rect:get_left() + rect:get_width())
    faceWnd:SetBottom(rect:get_top() + rect:get_height())
  end
end
function UI_Chat_CloseClick(msg)
  Lua_UI_Close(msg)
  CancelHorn()
  Lua_CFace_CloseFaceWnd()
end
function UI_Chat_MoveBtnClick(msg)
  fChangedHori = msg:get_fwparam()
  fChangedVer = msg:get_flparam()
  local wnd = WindowSys_Instance:GetWindow("ChatDialog")
  wnd:SetLeft(wnd:GetLeft() + fChangedHori)
  wnd:SetTop(wnd:GetTop() + fChangedVer)
  local btn = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg.Move_btn")
  btn:SetLeft(btn:GetLeft() - fChangedHori)
  btn:SetTop(btn:GetTop() - fChangedVer)
end
function UI_Chat_MoveBtnClick_Sys(msg)
  fChangedHori = msg:get_fwparam()
  fChangedVer = msg:get_flparam()
  local wnd = WindowSys_Instance:GetWindow("FightMessage")
  wnd:SetLeft(wnd:GetLeft() + fChangedHori)
  wnd:SetTop(wnd:GetTop() + fChangedVer)
  local btn = WindowSys_Instance:GetWindow("FightMessage.move_btn")
  btn:SetLeft(btn:GetLeft() - fChangedHori)
  btn:SetTop(btn:GetTop() - fChangedVer)
end
function UI_Chat_LockBtnClick(msg)
  local btn = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg.Move_btn")
  local bVal = btn:IsEnable()
  btn:SetEnable(not bVal)
end
function UI_Chat_LockBtnClick_Sys(msg)
  local btn = WindowSys_Instance:GetWindow("FightMessage.move_btn")
  local bVal = btn:IsEnable()
  btn:SetEnable(not bVal)
end
function UI_Lock_CheckBtnClick(msg)
  local CheckBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg.Lock_cbtn"))
  if CheckBtn:IsChecked() then
    for i = 0, 9 do
      local channel = gUI.Chat.Channel[i]
      channel:SetProperty("AutoRunBar", "false")
    end
  else
    for i = 0, 9 do
      local channel = gUI.Chat.Channel[i]
      channel:SetProperty("AutoRunBar", "true")
      _Chat_SetCurrSay_Show("")
      local old_channel = _Chat_GetChannelWnd(_Chat_GetChatChannel())
      if channel == old_channel then
        channel:MoveToEnd()
      end
    end
  end
end
function UI_Chat_ChangeSize(msg)
  local fChangedHeight = msg:get_flparam()
  local fChangedWidht = msg:get_fwparam()
  local wndChat = WindowSys_Instance:GetWindow("ChatDialog")
  local y = wndChat:GetRenderHeight()
  local x = wndChat:GetRenderWidth()
  if wndChat:GetHeight() - fChangedHeight > 180 / y and wndChat:GetHeight() - fChangedHeight < 380 / y then
    wndChat:SetHeight(wndChat:GetHeight() - fChangedHeight)
    local wnd = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
    wnd:SetTop(wnd:GetTop() - fChangedHeight)
    local wndNew = WindowSys_Instance:GetWindow("ChatDialog.Other_bg")
    wndNew:SetTop(wndNew:GetTop() - fChangedHeight)
    for i = 0, 9 do
      local channel = gUI.Chat.Channel[i]
      channel:SetHeight(channel:GetHeight() - fChangedHeight)
      channel:UpdateScrollBarConfig()
    end
    local pageWindow = WindowToDisplayBox(WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg.FightBox_dbox"))
    pageWindow:SetHeight(pageWindow:GetHeight() - fChangedHeight)
    pageWindow:UpdateScrollBarConfig()
    wnd = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
    wnd:SetHeight(wnd:GetHeight() - fChangedHeight)
    wnd = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg")
    wnd:SetTop(wnd:GetTop() + fChangedHeight)
  end
  local wndChat1 = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
  if wndChat1:GetWidth() + fChangedWidht > 330 / x and wndChat1:GetWidth() + fChangedWidht < 500 / x then
    for i = 0, 9 do
      local channel = gUI.Chat.Channel[i]
      channel:SetWidth(channel:GetWidth() + fChangedWidht)
      channel:OnRelayout()
    end
    local pageWindow = WindowToDisplayBox(WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg.FightBox_dbox"))
    pageWindow:SetWidth(pageWindow:GetWidth() + fChangedWidht)
    pageWindow:OnRelayout()
    wndChat1:SetWidth(wndChat1:GetWidth() + fChangedWidht)
  end
  local btn = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg.ChangeSize_btn")
  local wnd = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
  btn:SetTop(0)
  btn:SetRight(wnd:GetWidth())
end
function UI_Chat_UpSize(msg)
  local wndChat = WindowSys_Instance:GetWindow("ChatDialog")
  local y = wndChat:GetRenderHeight()
  local changeSize = 0
  local Top = 0
  if wndChat:GetHeight() <= 278 / y then
    changeSize = 280 / y - wndChat:GetHeight()
  elseif wndChat:GetHeight() <= 358 / y then
    changeSize = 360 / y - wndChat:GetHeight()
  elseif wndChat:GetHeight() <= 438 / y then
    changeSize = 440 / y - wndChat:GetHeight()
  else
    return
  end
  wndChat:SetHeight(wndChat:GetHeight() + changeSize)
  local wnd = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
  wnd:SetTop(wnd:GetTop() + changeSize)
  local wndNew = WindowSys_Instance:GetWindow("ChatDialog.Other_bg")
  wndNew:SetTop(wndNew:GetTop() + changeSize)
  for i = 0, 9 do
    local channel = gUI.Chat.Channel[i]
    channel:SetHeight(channel:GetHeight() + changeSize)
    channel:UpdateScrollBarConfig()
  end
  local pageWindow = WindowToDisplayBox(WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg.FightBox_dbox"))
  pageWindow:SetHeight(pageWindow:GetHeight() + changeSize)
  pageWindow:UpdateScrollBarConfig()
  wnd = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
  wnd:SetHeight(wnd:GetHeight() + changeSize)
  wnd = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg")
  wnd:SetTop(wnd:GetTop() - changeSize)
end
function UI_Chat_DownSize(msg)
  local wndChat = WindowSys_Instance:GetWindow("ChatDialog")
  local y = wndChat:GetRenderHeight()
  local changeSize = 0
  local Top = 0
  if wndChat:GetHeight() >= 362 / y then
    changeSize = 360 / y - wndChat:GetHeight()
  elseif wndChat:GetHeight() >= 282 / y then
    changeSize = 280 / y - wndChat:GetHeight()
  elseif wndChat:GetHeight() >= 198 / y then
    changeSize = 200 / y - wndChat:GetHeight()
  else
    return
  end
  wndChat:SetHeight(wndChat:GetHeight() + changeSize)
  local wnd = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
  wnd:SetTop(wnd:GetTop() + changeSize)
  local wndNew = WindowSys_Instance:GetWindow("ChatDialog.Other_bg")
  wndNew:SetTop(wndNew:GetTop() + changeSize)
  for i = 0, 9 do
    local channel = gUI.Chat.Channel[i]
    channel:SetHeight(channel:GetHeight() + changeSize)
    channel:UpdateScrollBarConfig()
  end
  local pageWindow = WindowToDisplayBox(WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg.FightBox_dbox"))
  pageWindow:SetHeight(pageWindow:GetHeight() + changeSize)
  pageWindow:UpdateScrollBarConfig()
  wnd = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
  wnd:SetHeight(wnd:GetHeight() + changeSize)
  wnd = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg")
  wnd:SetTop(wnd:GetTop() - changeSize)
end
function UI_Chat_ChangeSize_Sys(msg)
  local fReszieHeight = msg:get_flparam()
  local wnd_bg = WindowSys_Instance:GetWindow("FightMessage.FightBox_bg")
  local wnd_tctl = WindowSys_Instance:GetWindow("FightMessage.Page_tctl")
  local moveBtn = WindowSys_Instance:GetWindow("FightMessage.move_btn")
  local LockBtn = WindowSys_Instance:GetWindow("FightMessage.lock_btn")
  local BaseWndHegiht = wnd_bg:GetHeight() + wnd_tctl:GetHeight()
  local RenderWndHeight = wnd_bg:GetRenderHeight()
  local DefaultMinHeight = 118 / RenderWndHeight
  local DefaultMaxHeight = 200 / RenderWndHeight
  if DefaultMinHeight < BaseWndHegiht - fReszieHeight and DefaultMaxHeight > BaseWndHegiht - fReszieHeight then
    wnd_bg:SetTop(wnd_bg:GetTop() + fReszieHeight)
    wnd_bg:SetHeight(wnd_bg:GetHeight() - fReszieHeight)
    wnd_tctl:SetTop(wnd_tctl:GetTop() + fReszieHeight)
    wnd_tctl:SetHeight(wnd_tctl:GetHeight() - fReszieHeight)
    moveBtn:SetTop(moveBtn:GetTop() + fReszieHeight)
    LockBtn:SetTop(LockBtn:GetTop() + fReszieHeight)
    local wnd = WindowSys_Instance:GetWindow("FightMessage.FightBox_dbox")
    wnd:SetTop(wnd:GetTop() + fReszieHeight)
    wnd:SetHeight(wnd:GetHeight() - fReszieHeight)
    wnd = WindowToDisplayBox(wnd)
    wnd:UpdateScrollBarConfig()
    wnd = WindowSys_Instance:GetWindow("FightMessage.Common_dbox")
    wnd:SetTop(wnd:GetTop() + fReszieHeight)
    wnd:SetHeight(wnd:GetHeight() - fReszieHeight)
    wnd = WindowToDisplayBox(wnd)
    wnd:UpdateScrollBarConfig()
    wnd = WindowSys_Instance:GetWindow("FightMessage.System_dbox")
    wnd:SetTop(wnd:GetTop() + fReszieHeight)
    wnd:SetHeight(wnd:GetHeight() - fReszieHeight)
    wnd = WindowToDisplayBox(wnd)
    wnd:UpdateScrollBarConfig()
  end
  local btn = WindowSys_Instance:GetWindow("FightMessage.ResetSize_btn")
  local fightWnd = WindowSys_Instance:GetWindow("FightMessage.FightBox_bg")
  btn:SetTop(fightWnd:GetTop())
  btn:SetLeft(fightWnd:GetLeft())
end
function UI_Chat_NameEditboxEnter(msg)
end
function UI_ChatHide_Click(msg)
  local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
  local pic1 = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
  local pic2 = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.HintEffect_bg")
  local pic3 = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.CurrentSay_btn")
  local pic4 = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.CurrentSay_dbox")
  pic:SetProperty("Visible", "false")
  pic1:SetProperty("Visible", "false")
  if pic2:IsVisible() then
    pic2:SetProperty("Visible", "false")
  end
  if pic4:IsVisible() then
    pic3:SetProperty("Visible", "false")
    pic4:SetProperty("Visible", "false")
    if _CHAT_HAS_CHANGE == true then
      local wndChat = WindowSys_Instance:GetWindow("ChatDialog")
      local y = wndChat:GetRenderHeight()
      gUI.Chat.CtrlsBg:SetTop(gUI.Chat.CtrlsBg:GetTop() - 19 / y)
      gUI.Chat.InputsBg:SetTop(gUI.Chat.InputsBg:GetTop() - 19 / y)
      gUI.Chat.ShowBtn:SetTop(gUI.Chat.ShowBtn:GetTop() - 19 / y)
      gUI.Chat.HideBtn:SetTop(gUI.Chat.HideBtn:GetTop() - 19 / y)
      _CHAT_HAS_CHANGE = false
    end
  end
  local btn = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.Show_btn")
  btn:SetProperty("Visible", "true")
end
function UI_ChatShow_Click(msg)
  local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
  local pic1 = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
  pic:SetProperty("Visible", "true")
  pic1:SetProperty("Visible", "true")
  local btn = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.Show_btn")
  btn:SetProperty("Visible", "false")
end
function DelS(s)
  return s:match("^%s+(.-)%s+$")
end
function UI_Chat_EditboxEnterNew(msg)
  Chat_Send_Msg(1)
end
function UI_Chat_EditboxEnter(msg)
  Chat_Send_Msg()
end
function Chat_Send_Msg(isCapture)
  local str = gUI.Chat.MainInputBox:GetProperty("Text")
  Lua_CFace_CloseFaceWnd()
  local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
  pic = WindowToPicture(pic)
  pic:SetProperty("HideTime", "10000")
  local strNew = DelS(str)
  if strNew == "" then
    Lua_Chat_ShowNotity(0, "CHAT_CANNOT_KONGGE")
  end
  if str ~= "" then
    if IsDebugCmd(str) == false then
      local count = select(2, string.gsub(str, "%{##%^%d%d%}", "{##%^%d%d%}"))
      if count > 6 then
        Lua_Chat_ShowNotity(0, "CHAT_FACE_LIMITED")
        return
      end
      if Lua_Chat_CountItem(str) > 3 then
        Lua_Chat_ShowNotity(0, "CHAT_ITEM_LIMITED")
        return
      end
      if _Chat_GetChatType() == 3 then
        name = _Chat_GetTalkName()
        if name == "" then
          Lua_Chat_ShowNotity(0, "CHAT_INPUT_NAME")
          return
        end
        local myName = GetPlaySelfProp(6)
        if myName == name then
          Lua_Chat_ShowNotity(0, "CAN_NOT_TALK_WITH_SELF")
          return
        end
        local tempStr = GetParsedStr(str)
        local textlen = string.len(tempStr)
        if textlen > gUI_CHAT_CHANNEL_SETTINGINFO[_Chat_GetChatType()].MAXWORDLIMIT then
          Lua_Chat_ShowNotity(0, "CHAT_TOO_MANY_WORD", gUI_CHAT_CHANNEL_SETTINGINFO[_Chat_GetChatType()].CHANNEL_NAME, tostring(gUI_CHAT_CHANNEL_SETTINGINFO[_Chat_GetChatType()].MAXWORDLIMIT / 2))
        else
          SendChatMessage(gUI_SENDMSG_CHANNEL_MAP[_Chat_GetChatType()], str, name)
          if isCapture == 1 then
            WindowSys_Instance:SetKeyboardCaptureWindow(nil)
          end
        end
        gUI.Chat.MainInputBox:SaveHistoryText()
        gUI.Chat.MainInputBox:SetProperty("Text", "")
        gUI.Chat.MainInputBox:ResetCaretPos()
      else
        local tempStr = GetParsedStr(str)
        local textlen = string.len(tempStr)
        if textlen > gUI_CHAT_CHANNEL_SETTINGINFO[_Chat_GetChatType()].MAXWORDLIMIT then
          Lua_Chat_ShowNotity(0, "CHAT_TOO_MANY_WORD", gUI_CHAT_CHANNEL_SETTINGINFO[_Chat_GetChatType()].CHANNEL_NAME, tostring(gUI_CHAT_CHANNEL_SETTINGINFO[_Chat_GetChatType()].MAXWORDLIMIT / 2))
        else
          if _Chat_GetChatType() == gUI_CHANNEL_STR.Guild then
            local _, _, _, guild_level = GetGuildBaseInfo()
            if guild_level <= 0 then
              Lua_Chat_ShowNotity(0, "CHAT_NO_GUILD")
              return
            end
          end
          if _Chat_GetChatType() == gUI_CHANNEL_STR.BattleGrounp and IsInCombatGroup() == 0 then
            Lua_Chat_ShowNotity(0, "CHAT_NOT_INCAMBAT")
            return
          end
          SendChatMessage(gUI_SENDMSG_CHANNEL_MAP[_Chat_GetChatType()], str, "")
          if isCapture == 1 then
            WindowSys_Instance:SetKeyboardCaptureWindow(nil)
          end
        end
        gUI.Chat.MainInputBox:SaveHistoryText()
        gUI.Chat.MainInputBox:SetProperty("Text", "")
        gUI.Chat.MainInputBox:ResetCaretPos()
      end
    end
  else
    Lua_Chat_ShowNotity(0, "CHAT_CANNOT_KONGGE")
  end
end
function ChatDialog_GMCommand(GMType, GMPar1, GMPar2)
  local str = ""
  if GMType == 106 then
    str = "-killnpc" .. " " .. "1"
  elseif GMType == 107 then
    str = "-Seenpc"
  elseif GMType == 108 then
    str = "-Distance"
  elseif GMType == 109 and GMPar1 ~= 0 then
    str = "-Showlottery " .. tostring(GMPar1)
  elseif GMType == 110 then
    str = "-see " .. tostring(GMPar1)
  elseif GMType == 111 then
    str = "-kickoff " .. tostring(GMPar1)
  end
  SendChatMessage(gUI_SENDMSG_CHANNEL_MAP[2], str, "")
end
function UI_Chat_ChannelClick(msg)
  local wnd = msg:get_window()
  local channId = wnd:GetCustomUserData(0)
  if channId <= gUI_CHANNEL_STR.BattleGrounp then
    local RealIndex = gUI_CHANNEL_INDEX_RELATION1[channId]
    Lua_Chat_SelectChatChannel(RealIndex)
  end
end
function _Chat_HornTimer2(timer)
  local times = GetCurrSysTime()
  if #gUI.Chat.LabaMark > 0 then
    local deTime1 = times - _Chat_Laba_Time1
    local deTime2 = times - _Chat_Laba_Time2
    local isType = 3
    if deTime1 > 2 and deTime2 > 2 then
      if deTime1 >= deTime2 then
        isType = 1
      else
        isType = 2
      end
    end
    local hornBack = WindowToPicture(WindowSys_Instance:GetWindow("ChatDialog.HornBox_bg"))
    local hornBack1 = WindowToPicture(WindowSys_Instance:GetWindow("ChatDialog.HornBox1_bg"))
    local item = gUI.Chat.LabaMark[1]
    if isType == 1 then
      hornBack:SetProperty("Visible", "true")
      gUI.Chat.Channel[10]:InsertBack(item.strMsg, item.color, 0, 0)
      gUI.Chat.HornTimer:Restart()
      _Chat_Laba_Time1 = times
      table.remove(gUI.Chat.LabaMark, 1)
    else
      if isType == 2 then
        hornBack1:SetProperty("Visible", "true")
        gUI.Chat.Channel[13]:InsertBack(item.strMsg, item.color, 0, 0)
        gUI.Chat.HornTimer1:Restart()
        _Chat_Laba_Time2 = times
        table.remove(gUI.Chat.LabaMark, 1)
      else
      end
    end
  else
    gUI.Chat.HornTimer2:Stop()
  end
end
function _Chat_Clear_Laba()
  _Chat_Laba_Time1 = 0
  _Chat_Laba_Time2 = 0
end
function Chat_Unlock_CBtn(bLock)
  local lockBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg.Lock_cbtn"))
  lockBtn:SetChecked(bLock)
  UI_Lock_CheckBtnClick()
end
function UI_CurrentSay_Click(msg)
  gUI.Chat.CurrSayBtn1:SetProperty("Visible", "false")
  gUI.Chat.CurrSayBtn:SetProperty("Visible", "false")
  gUI.Chat.CurrSayBtn2:SetProperty("Visible", "false")
  Chat_Unlock_CBtn(false)
end
function UI_SbnPositionClick(msg)
  local wnd = msg:get_window()
  wnd = WindowToDisplayBox(wnd)
  local ScrollBar = WindowToScrollBar(wnd:GetChildByName("vertSB_"))
  if ScrollBar and ScrollBar:IsVisible() then
    local ScrollBarPos = ScrollBar:GetCurrentPos()
    local ScrollBarMaxPos = ScrollBar:GetMaxPos()
    if ScrollBarPos >= ScrollBarMaxPos then
      Chat_Unlock_CBtn(false)
    else
      Chat_Unlock_CBtn(true)
    end
  end
end
function UI_Chat_ResetConfig(msg)
  for i = 0, 11 do
    local flag = GetShowDefalutConfig(_CHAT_CURRENT_SELECT - 1, i)
    if flag == 1 then
      gUI.Chat.CheckBtns[i]:SetChecked(true)
      gUI.Chat.CheckBtns[i]:SetProperty("Enable", "false")
    elseif flag == 2 then
      gUI.Chat.CheckBtns[i]:SetChecked(true)
      gUI.Chat.CheckBtns[i]:SetProperty("Enable", "true")
    elseif flag == 3 then
      gUI.Chat.CheckBtns[i]:SetChecked(false)
      gUI.Chat.CheckBtns[i]:SetProperty("Enable", "true")
    elseif flag == 4 then
      gUI.Chat.CheckBtns[i]:SetChecked(false)
      gUI.Chat.CheckBtns[i]:SetProperty("Enable", "false")
    else
      gUI.Chat.CheckBtns[i]:SetChecked(true)
      gUI.Chat.CheckBtns[i]:SetProperty("Enable", "false")
    end
  end
end
function UI_Chat_FilterClose(msg)
  SaveToConfigs()
  gUI.Chat.Filter:SetProperty("Visible", "false")
  _CHAT_CURRENT_SELECT = -1
end
function UI_Cancel_Click(msg)
  gUI.Chat.Filter:SetProperty("Visible", "false")
  _CHAT_CURRENT_SELECT = -1
end
function UI_Channel_rClick(msg)
  local index = msg:get_wparam()
  if index >= 0 and index < 8 then
    local realIndex = gUI_CHANNEL_INDEX_RELATION[index]
    _CHAT_CURRENT_SELECT = realIndex
    UI_OpenFilter_interface()
  end
end
function UI_OpenFilter_interface()
  gUI.Chat.Filter:SetProperty("Visible", "true")
  gUI.Chat.SettingLab:SetProperty("Text", gUI_CHAT_CHANNEL_SETTINGINFO[_CHAT_CURRENT_SELECT].DESCRIPTION)
  for i = 0, 11 do
    if _CHAT_CURRENT_SELECT < gUI_CHANNEL_STR.Map then
      local flag = GetShowChannelConfig(_CHAT_CURRENT_SELECT - 1, i)
      UI_SetFilter_State(flag, gUI.Chat.CheckBtns[i])
    else
      local flag = GetShowChannelConfig(_CHAT_CURRENT_SELECT - 2, i)
      UI_SetFilter_State(flag, gUI.Chat.CheckBtns[i])
    end
  end
end
function UI_SetFilter_State(flag, cbtn, isVisble)
  if flag == 1 then
    cbtn:SetChecked(true)
    cbtn:SetProperty("Enable", "false")
  elseif flag == 2 then
    cbtn:SetChecked(true)
    cbtn:SetProperty("Enable", "true")
  elseif flag == 3 then
    cbtn:SetChecked(false)
    cbtn:SetProperty("Enable", "true")
  elseif flag == 4 then
    cbtn:SetChecked(false)
    cbtn:SetProperty("Enable", "false")
  else
    cbtn:SetChecked(true)
    cbtn:SetProperty("Enable", "false")
  end
end
function SaveToConfigs()
  for i = 0, 11 do
    UI_SaveConfigs(gUI.Chat.CheckBtns[i], i, _CHAT_CURRENT_SELECT - 1)
  end
  SaveToConfig()
end
function UI_SaveConfigs(cbtn, index, currSel)
  if cbtn:GetProperty("Enable") == "false" and cbtn:IsChecked() then
    SetShowChannelConfig(currSel, index, 1)
  elseif cbtn:GetProperty("Enable") == "true" and cbtn:IsChecked() then
    SetShowChannelConfig(currSel, index, 2)
  elseif cbtn:GetProperty("Enable") == "true" and not cbtn:IsChecked() then
    SetShowChannelConfig(currSel, index, 3)
  elseif cbtn:GetProperty("Enable") == "false" and not cbtn:IsChecked() then
    SetShowChannelConfig(currSel, index, 4)
  end
end
function Script_Chat_OnLoad()
  _Chat_Golble_Index_Laba = 0
  _CHAT_CURRENT_SELECT = -1
  gUI.Chat.LabaMark = {}
  _Chat_Clear_Laba()
  gUI.Chat.Channel = {
    [0] = nil,
    [1] = nil,
    [2] = nil,
    [3] = nil,
    [4] = nil,
    [5] = nil,
    [6] = nil,
    [7] = nil,
    [8] = nil,
    [9] = nil
  }
  gUI.Chat.Conf1 = {
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1
  }
  gUI.Chat.EnterBtn = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.Enter_btn")
  gUI.Chat.ChannelBtn = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.Channel_btn")
  gUI.Chat.QuestGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("TooltipQuest.Common_bg.Task_cnt.Describe_gbox"))
  local root = WindowSys_Instance:GetWindow("ChatDialog")
  local tc = WindowToTableCtrl(root:GetGrandChild("Input_bg.Chat_bg.Channels_tctl"))
  gUI.Chat.CtrlsBg = WindowToTableCtrl(root:GetGrandChild("Input_bg.Chat_bg.Channels_tctl"))
  gUI.Chat.InputsBg = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg")
  gUI.Chat.ShowBtn = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.Show_btn")
  gUI.Chat.HideBtn = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Hide_btn")
  gUI.Chat.NameEBox = WindowToEditBox(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.name_ebox"))
  gUI.Chat.Namebg = WindowToEditBox(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.name_bg"))
  gUI.Chat.MainInputBox = WindowToEditBox(root:GetGrandChild("Input_bg.Inputborder_bg.Input_ebox"))
  gUI.Chat.MainInputBox:SetProperty("Text", "")
  gUI.Chat.MainInputBox:ClearHistoryText()
  gUI.Chat.WhisperHis = {
    [0] = "",
    [1] = "",
    [2] = "",
    [3] = "",
    [4] = ""
  }
  local parent = root:GetChildByName("ChatBox_bg")
  for i, name in pairs({
    [0] = "Integration_dbox",
    [1] = "Common_dbox",
    [2] = "Group_dbox",
    [3] = "Guild_dbox",
    [4] = "World_dbox",
    [5] = "Whisper_dbox",
    [6] = "Alliance_dbox",
    [7] = "System_dbox",
    [8] = "StepServer_dbox",
    [9] = "Userdefine_dbox"
  }) do
    gUI.Chat.Channel[i] = WindowToDisplayBox(parent:GetChildByName(name))
    gUI.Chat.Channel[i]:SetVisible(false)
    gUI.Chat.Channel[i]:ClearAllContent()
  end
  gUI.Chat.Channel[10] = WindowToDisplayBox(WindowSys_Instance:GetWindow("ChatDialog.HornBox_bg.HornBox_dbox"))
  gUI.Chat.Channel[10]:ClearAllContent()
  gUI.Chat.Channel[13] = WindowToDisplayBox(WindowSys_Instance:GetWindow("ChatDialog.HornBox1_bg.HornBox1_dbox"))
  gUI.Chat.Channel[13]:ClearAllContent()
  local hornBack = WindowToPicture(WindowSys_Instance:GetWindow("ChatDialog.HornBox_bg"))
  hornBack:SetProperty("Visible", "false")
  local hornBack1 = WindowToPicture(WindowSys_Instance:GetWindow("ChatDialog.HornBox1_bg"))
  hornBack1:SetProperty("Visible", "false")
  local confUserDefine = gUI.Chat.Conf1
  gUI.Chat.SelChoose = {
    [0] = nil,
    [1] = nil,
    [2] = nil,
    [3] = nil,
    [4] = nil,
    [5] = nil,
    [6] = nil
  }
  local chatDef = WindowSys_Instance:GetWindow("ChannelDefine.Channel_bg.Up_bg")
  for i, name in pairs({
    [0] = "CheckButton1",
    [1] = "CheckButton2",
    [2] = "CheckButton3",
    [3] = "CheckButton4",
    [4] = "CheckButton5",
    [5] = "CheckButton6",
    [6] = "CheckButton7"
  }) do
    gUI.Chat.SelChoose[i] = WindowToCheckButton(chatDef:GetChildByName(name))
    if confUserDefine[i] == 0 then
      gUI.Chat.SelChoose[i]:SetCheckedState(false)
    else
      gUI.Chat.SelChoose[i]:SetCheckedState(true)
    end
  end
  _Chat_SetChatChannel(1)
  gUI.Chat.ChannelBtn:SetProperty("Text", gUI_CHAT_CHANNEL_SETTINGINFO[1].CHANNEL_NAME)
  _Chat_SetChatType(gUI_CHANNEL_STR.Normal)
  gUI.Chat.MainInputBox:SetProperty("DefultColor", gUI_CHAT_CHANNEL_SETTINGINFO[1].COLOR)
  gUI.Chat.Channel[1]:SetProperty("Visible", "true")
  _Chat_InitScrollBarRange(1)
  gUI.Chat.Channel[1]:UpdateScrollBarConfig()
  gUI.Chat.MainInputBox:SetProperty("MaxLength", tostring(gUI_SENDMSG_CHANNEL_MAX_WORD[1]))
  tc:SetSelectedState(0)
  gUI.Chat.Channel[11] = WindowSys_Instance:GetWindow("Post.Post1_dlab")
  gUI.Chat.Channel[11]:SetVisible(false)
  local btn = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg.ChangeSize_btn")
  local wnd = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
  btn:SetTop(0)
  btn:SetRight(wnd:GetWidth())
  gUI.Chat.Chat_InfoWindow = {
    curPageIndex = nil,
    pageWindow = {
      [0] = nil,
      [1] = nil,
      [2] = nil
    }
  }
  function gUI.Chat.Chat_InfoWindow:Init()
    local pageWindow = self.pageWindow
    local root = WindowSys_Instance:GetWindow("FightMessage")
    local wndsName = {
      [0] = "Common_dbox",
      [1] = "System_dbox",
      [2] = "FightBox_dbox"
    }
    for i, name in pairs(wndsName) do
      pageWindow[i] = root:GetChildByName(name)
      pageWindow[i] = WindowToDisplayBox(pageWindow[i])
      pageWindow[i]:ClearAllContent()
    end
    local root = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
    pageWindow[2] = root:GetChildByName("FightBox_dbox")
    pageWindow[2] = WindowToDisplayBox(pageWindow[2])
    pageWindow[2]:ClearAllContent()
    WindowToTableCtrl(WindowSys_Instance:GetWindow("FightMessage.Page_tctl")):SelectItem(0)
    gUI.Chat.Chat_InfoWindow:ChosePage(1)
  end
  function gUI.Chat.Chat_InfoWindow:ChosePage(pageIndex)
    local pageWindow = self.pageWindow
    for i = 0, 2 do
      pageWindow[i]:SetProperty("Visible", "false")
    end
    self.curPageIndex = pageIndex
    pageWindow[self.curPageIndex]:SetProperty("Visible", "true")
    local root = WindowSys_Instance:GetWindow("FightMessage")
    pageWindow[self.curPageIndex]:UpdateScrollBarConfig()
    pageWindow[self.curPageIndex]:MoveToEnd()
  end
  function gUI.Chat.Chat_InfoWindow:InsertMsg(pageIndex, msg)
    local pageWindow = self.pageWindow
    if pageIndex ~= 0 then
    end
    pageWindow[pageIndex]:InsertBack(msg, 4294966016, 0, 0)
  end
  function gUI.Chat.Chat_InfoWindow:Clear()
    for i, showbox in pairs(self.pageWindow) do
      if showbox then
        showbox:ClearAllContent()
      end
    end
  end
  gUI.Chat.Chat_InfoWindow:Init()
  _Chat_ShowNameEbox("", false)
  gUI.Chat.HornTimer = TimerSys_Instance:CreateTimerObject("HornTimer", 15, 1, "_Chat_HornTimer", false, false)
  gUI.Chat.HornTimer1 = TimerSys_Instance:CreateTimerObject("HornTimer1", 15, 1, "_Chat_HornTimer1", false, false)
  gUI.Chat.HornTimer2 = TimerSys_Instance:CreateTimerObject("HornTimer2", 1, -1, "_Chat_HornTimer2", false, false)
  gUI.Chat.BulletinTimer = TimerSys_Instance:CreateTimerObject("BulletinTimer", 600, 1, "_Chat_LoopBulletinShow", true, false)
  root = WindowToPicture(root:GetChildByName("Input_bg"))
  root:SetProperty("Alpha", "0.00001")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
  CloseIme()
  gUI.Chat.PaoList = {}
  UI_Chat_ScrollEnd()
  local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
  local pic1 = WindowSys_Instance:GetWindow("ChatDialog.ChatBox_bg")
  pic:SetProperty("Visible", "true")
  pic1:SetProperty("Visible", "true")
  local btn = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.Show_btn")
  btn:SetProperty("Visible", "false")
  gUI.Chat.CurrSayBtn = WindowToDisplayBox(WindowSys_Instance:GetWindow("ChatDialog.Other_bg.CurrentSay_dbox"))
  gUI.Chat.CurrSayBtn1 = WindowToButton(WindowSys_Instance:GetWindow("ChatDialog.Other_bg.CurrentSay_btn"))
  gUI.Chat.CurrSayBtn2 = WindowSys_Instance:GetWindow("ChatDialog.Other_bg.HintEffect_bg")
  Chat_Unlock_CBtn(false)
  gUI.Chat.Filter = WindowSys_Instance:GetWindow("ChannelFilter")
  gUI.Chat.CheckBtns = {}
  for i = 0, 11 do
    gUI.Chat.CheckBtns[i] = WindowToCheckButton(WindowSys_Instance:GetWindow("ChannelFilter.Channel" .. tostring(i + 1) .. "_btn"))
  end
  gUI.Chat.SettingLab = WindowToLabel(WindowSys_Instance:GetWindow("ChannelFilter.Setting_dlab"))
  gUI.Chat.ChanInterface = WindowSys_Instance:GetWindow("ChanFiltem.ChanFiltem_bg")
  gUI.Chat.ChanContainer = WindowToContainer(WindowSys_Instance:GetWindow("ChanFiltem.ChanFiltem_bg.ChanTemp_cnt"))
  gUI.Chat.ChanListTemp = WindowSys_Instance:GetWindow("ChanFiltem.ChanFiltem_bg.ChanTemp_bg")
  gUI.Chat.InterfaceTem = WindowSys_Instance:GetWindow("ChanFiltem")
  gUI.Chat.InterfaceTem:SetProperty("Visible", "false")
end
function Script_Chat_OnEvent(event)
  if event == "UI_R_CHAT_MSG" then
    _OnChat_RecvChat(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif event == "UI_FIGHT_LOG" then
    _OnChat_AddFightLog(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "UI_SYS_LOG" then
    _OnChat_AddSysLog(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "ENTER_COMBAT_MODE" then
    _OnChat_EnterCombat(arg1, arg2)
  elseif event == "UI_SHOW_HORN" then
    _OnChat_HornShow(arg1)
  elseif event == "UI_HORN_CLOSE" then
    _OnChat_HornClose(arg1)
  elseif event == "NPC_GOSSIP_ADD_REWARD_ITEM" then
    _OnChat_UpdateRewardItem(arg1, arg2, arg3, arg4, arg5)
  elseif event == "DUEL_INVITATION_RECEIVE" then
    _OnChat_DuelInvitationReceive(arg1)
  elseif event == "UI_CHAT_GUILD_WAR_RE" then
    _OnChat_GuildWarRe(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif event == "UI_CHAT_INFO" then
    Lua_Chat_ShowInChatZone(arg1, arg2, arg3)
  elseif event == "UI_LUA_ERROR" then
    Lua_Chat_ShowLuaErr(arg1)
  elseif event == "UI_ENGINE_ERROR" then
    Lua_Chat_ShowEngineErr(arg1)
  elseif event == "SHOW_OSD_BOTTOM" then
    Lua_Chat_OSDBottom(arg1)
  elseif event == "SHOW_OSD" then
    Lua_Chat_OSD(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "SHOW_CHAT" then
    Lua_Chat_CHAT(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "SHOW_PAOPAO" then
    Lua_Chat_ApplyPaoPao(arg1)
  elseif event == "SHOW_AURA_PAOPAO" then
    Lua_Chat_ApplyAuraPaoPao(arg1, arg2)
  elseif event == "UI_CHAT_GETFOUSE" then
    Lua_Chat_GetTabFouse(arg1)
  end
end

