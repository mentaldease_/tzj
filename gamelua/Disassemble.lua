if gUI and not gUI.Disassemble then
  gUI.Disassemble = {}
end
ERRORRESLUT = {
  NOTITEM = 99,
  NOTEQUIP = 100,
  CANNOTDIS = 101,
  INFISMAX = 102,
  HASGEM = 103,
  HASSIGN = 104,
  CANDIS = 105
}
function Disassemble_OpenPanel()
  if gUI.Disassemble.Root:IsVisible() then
    gUI.Disassemble.Root:SetVisible(false)
  else
    gUI.Disassemble.Root:SetVisible(true)
    Disassemble_InitItems()
    SetDisassBagInfos()
  end
end
function Disassemble_InitItems()
  gUI.Disassemble.CostLabel:SetProperty("Text", "0")
  gUI.Disassemble.ItemSelectGB:ClearGoodsItem(0)
  gUI.Disassemble.ItemSelectLB:SetProperty("Text", "")
  gUI.Disassemble.RecordDisB:ClearAllContent()
end
function _OnDisassemble_Reslut(content)
  gUI.Disassemble.RecordDisB:InsertBack(content, 4294967295, 0, 0)
end
function Lua_SendToDisassembleMainSlot(bagType, bagSlot)
  if bagType ~= gUI_GOODSBOX_BAG.backpack0 then
    return
  end
  local itemCanDis = GetDisassReslut(bagType, bagSlot)
  if itemCanDis == ERRORRESLUT.NOTITEM then
    return
  elseif itemCanDis == ERRORRESLUT.NOTEQUIP then
    Lua_Chat_ShowOSD("DISASSEMBLE_NOEQUIP")
    return
  elseif itemCanDis == ERRORRESLUT.CANNOTDIS then
    Lua_Chat_ShowOSD("DISASSEMBLE_CANNOTDIS")
    return
  elseif itemCanDis == ERRORRESLUT.INFISMAX then
    Lua_Chat_ShowOSD("DISASSEMBLE_INFISMAX")
    return
  elseif itemCanDis == ERRORRESLUT.HASGEM then
    Lua_Chat_ShowOSD("DISASSENBLE_HASGEM")
    return
  elseif itemCanDis == ERRORRESLUT.HASSIGN then
    Lua_Chat_ShowOSD("DISASSENBLE_HASSIGN")
    return
  elseif itemCanDis == ERRORRESLUT.CANDIS then
    SendDisassEquip(bagSlot, OperateMode.Add)
    return
  end
end
function UI_Disassemble_Close(msg)
  gUI.Disassemble.Root:SetVisible(false)
  SendDisassEquip(0, OperateMode.Cancel)
end
function UI_Disassemble_DoAction(msg)
  local bag, slot = GetDisassSlotInfo()
  if not bag then
    Lua_Chat_ShowOSD("DISASSENBLE_NOITEM")
  else
    local money, icon, quilty = GetDisassembleInfo(bag, slot)
    local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
    if money > nonBindemoney then
      Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
      return
    end
    if quilty >= 1 then
      Messagebox_Show("NPC_ITEM_DESCOMPSE")
    else
      DisassComfim()
    end
  end
end
function UI_Disass_OnDraged(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    Lua_SendToDisassembleMainSlot(gUI_GOODSBOX_BAG.backpack0, slot)
  end
end
function UI_Disass_EquipOnRClick(msg)
  SendDisassEquip(0, OperateMode.Cancel)
end
function Script_Disassemble_OnHide()
  SendDisassEquip(0, OperateMode.Cancel)
end
function _OnDisassemble_Additem(bagType, bagSlot)
  if bagType ~= gUI_GOODSBOX_BAG.backpack0 then
    return
  end
  OnDisassemble_UpdateMoneyAndIcon(bagType, bagSlot)
end
function OnDisassemble_UpdateMoneyAndIcon(bagType, bagSlot)
  local money, icon, quilty, name = GetDisassembleInfo(bagType, bagSlot)
  gUI.Disassemble.CostLabel:SetProperty("Text", Lua_UI_Money2String(money, true))
  gUI.Disassemble.ItemSelectGB:SetItemGoods(0, icon, quilty)
  gUI.Disassemble.ItemSelectLB:SetProperty("Text", "{#C^" .. tostring(gUI_GOODS_QUALITY_COLOR_PREFIX[quilty]) .. "^" .. name .. "}")
end
function _OnDisassemble_DelItem()
  gUI.Disassemble.CostLabel:SetProperty("Text", "0")
  gUI.Disassemble.ItemSelectGB:ClearGoodsItem(0)
  gUI.Disassemble.ItemSelectLB:SetProperty("Text", "")
end
function Disassemble_UpdateInfoEquip()
end
function UI_ItemDesComposeClick(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local Guid = wnd:GetUserData64()
  local bagType1, itemSlot1 = GetItemPosByGuid(Guid)
  if bagType1 == gUI_GOODSBOX_BAG.backpack0 then
    SendDisassEquip(itemSlot1, OperateMode.Add)
  end
end
function UI_ItemDesCompose_Tip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  if win:IsItemHasIcon(0) then
    local Guid = win:GetUserData64()
    local bag, slot = GetItemPosByGuid(Guid)
    if slot ~= -1 then
      Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
      Lua_Tip_ShowEquipCompare(win, bag, slot)
    end
  end
end
function UI_ItemDes_Tip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  local bag, slot = GetDisassSlotInfo()
  if bag then
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(win, bag, slot)
  end
end
function _OnDisassemble_Suess()
  _OnDisassemble_DelItem()
end
function _OnDisassemble_BeginUpdate()
  gUI.Disassemble.ItemContain:DeleteAll()
end
function _OnDisassemble_Update(icon, name, quilty, guid, index)
  if icon then
    local Items = gUI.Disassemble.ItemContain:Insert(-1, gUI.Disassemble.ItemTemplate)
    Items:SetProperty("WindowName", "Pro_gbox_index" .. tostring(index))
    Items:SetProperty("Visible", "true")
    local Gb = WindowToGoodsBox(Items:GetGrandChild("Item_gbox"))
    local Lb = WindowToLabel(Items:GetGrandChild("Name_dlab"))
    Lb:SetProperty("Text", "{#C^" .. tostring(gUI_GOODS_QUALITY_COLOR_PREFIX[quilty]) .. "^" .. tostring(name) .. "}")
    Gb:SetItemGoods(0, icon, quilty)
    Gb:SetUserData64(guid)
  end
end
function _OnDisassemble_EndUpdate()
end
function _OnDisassemble_UpdateList()
  if gUI.Disassemble.Root:IsVisible() then
    SetDisassBagInfos()
  end
end
function Script_Disassemble_OnEvent(event)
  if event == "DISASSEMBLE_ADD_ITEM" then
    _OnDisassemble_Additem(arg1, arg2)
  elseif event == "DISASSEMBLE_DEL_ITEM" then
    _OnDisassemble_DelItem()
  elseif event == "DISASSEMBLE_UPDATEINFOBEGIN" then
    _OnDisassemble_BeginUpdate()
  elseif event == "DISASSEMBLE_UPDATEINFO" then
    _OnDisassemble_Update(arg1, arg2, arg3, arg4, arg5)
  elseif event == "DISASSEMBLE_UPDATEINFOEND" then
    _OnDisassemble_EndUpdate()
  elseif event == "ITEM_ADD_TO_BAG" then
    _OnDisassemble_UpdateList()
  elseif event == "ITEM_DEL_FROM_BAG" then
    _OnDisassemble_UpdateList()
  end
end
function Script_Disassemble_OnLoad()
  gUI.Disassemble.Root = WindowSys_Instance:GetWindow("Disassemble")
  gUI.Disassemble.ItemTemplate = WindowSys_Instance:GetWindow("Disassemble.Disassemble_bg.Left_bg.Item_bg")
  gUI.Disassemble.ItemContain = WindowToContainer(WindowSys_Instance:GetWindow("Disassemble.Disassemble_bg.Left_bg.Equip_cnt"))
  gUI.Disassemble.ItemSelectGB = WindowToGoodsBox(WindowSys_Instance:GetWindow("Disassemble.Disassemble_bg.Right_bg.Up_bg.Equip_bg.Equip_gbox"))
  gUI.Disassemble.ItemSelectLB = WindowToLabel(WindowSys_Instance:GetWindow("Disassemble.Disassemble_bg.Right_bg.Up_bg.Equip_bg.Equip_dlab"))
  gUI.Disassemble.CostLabel = WindowToLabel(WindowSys_Instance:GetWindow("Disassemble.Disassemble_bg.Right_bg.Up_bg.Money_bg.Money_dlab"))
  gUI.Disassemble.RecordDisB = WindowToDisplayBox(WindowSys_Instance:GetWindow("Disassemble.Disassemble_bg.Right_bg.Down_bg.List_bg.DisplayBox1"))
end
