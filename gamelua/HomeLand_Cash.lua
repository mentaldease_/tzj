if gUI and not gUI.HomeLandCash then
  gUI.HomeLandCash = {}
end
local _HomeLandCash = false
local _HomeLandCash_ShowBoxName = "HomeLandCash"
local _HomeLandCash_ShowModelId = {
  [1] = 561,
  [2] = 562,
  [3] = 563
}
local _HomeLandCash_ShowName = ""
local _HomeLandCash_MaxRain = 5
local _HomeLandCash_MaxHard = 1
local _HomeLandCash_CurrentLog = 0
local _HomeLandCash_Tip = "每天可以给好友浇水5次，自己种植的花朵被浇水5次后，自己的花朵再被浇水1次，则增加1次给好友的花朵浇水的机会，最多可以增加至10次。"
local _HomeLandCash_WaterList = {}
local _HomeLandCash_FriendList = {}
local _HomeLandCash_Fri_Count = 1
local _HomeLandCash_Select_Fri = -1
local _HL_BarPos
function _HomeLandCash_Update()
  if gUI.HomeLandCash.Root:IsVisible() then
    _HomeLandCash_UpdateCash()
  end
end
function _HomeLandCash_AddFri(relation_type)
  if relation_type == gUI_RELATION_TYPE.friend then
    _HomeLandCash_UpdateFri()
  end
end
function _HomeLandCash_InsterFri(insertList)
  local list = insertList
  for i = 1, table.maxn(list) do
    local _item = list[i]
    local newItem = gUI.HomeLandCash.NameContainer:Insert(-1, gUI.HomeLandCash.FriTemp)
    local str = string.format("%s的花园", _item.Name)
    if _item.BeWater then
      str = string.format("{#B^%s}", str)
    end
    if _item.Guid == _HomeLandCash_Select_Fri then
      local wnd = WindowToButton(newItem)
      wnd:SetStatus("selected")
    end
    newItem:SetProperty("Text", str)
    newItem:SetProperty("Visible", "true")
    newItem:SetUserData64(_item.Guid)
    _HomeLandCash_Fri_Count = _HomeLandCash_Fri_Count + 1
    newItem:SetCustomUserData(1, _HomeLandCash_Fri_Count)
  end
  local scrollbar = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Right_bg.Target_cnt.vertSB_")
  if scrollbar and scrollbar:IsVisible() and _HL_BarPos then
    scrollbar = WindowToScrollBar(scrollbar)
    scrollbar:SetCurrentPos(_HL_BarPos)
  end
end
function _HomeLandCash_UpdateFri()
  gUI.HomeLandCash.NameContainer:DeleteAll()
  _HomeLandCash_Fri_Count = 1
  local newItem = gUI.HomeLandCash.NameContainer:Insert(-1, gUI.HomeLandCash.FriTemp)
  newItem:SetProperty("Text", "我的花园")
  newItem:SetProperty("Visible", "true")
  if -1 == _HomeLandCash_Select_Fri then
    local wnd = WindowToButton(newItem)
    wnd:SetStatus("selected")
  end
  newItem:SetUserData64(-1)
  newItem:SetCustomUserData(1, _HomeLandCash_Fri_Count)
  local list = UpdateHomeFriendRainList()
  if list == nil then
    return
  end
  _HomeLandCash_WaterList = {}
  _HomeLandCash_FriendList = {}
  for i = 1, table.maxn(list) do
    local _item = list[i]
    if _item.Water then
      table.insert(_HomeLandCash_WaterList, _item)
    else
      table.insert(_HomeLandCash_FriendList, _item)
    end
  end
  local nWaterList = table.maxn(_HomeLandCash_WaterList)
  if nWaterList > 0 then
    local newItem = gUI.HomeLandCash.NameContainer:Insert(-1, gUI.HomeLandCash.WaterBtn)
    newItem:SetProperty("Visible", "true")
    newItem:SetCustomUserData(1, -1)
    _HomeLandCash_InsterFri(_HomeLandCash_WaterList)
  end
  local nFriList = table.maxn(_HomeLandCash_FriendList)
  if nFriList > 0 then
    local newItem = gUI.HomeLandCash.NameContainer:Insert(-1, gUI.HomeLandCash.FriBtn)
    newItem:SetProperty("Visible", "true")
    newItem:SetCustomUserData(1, -1)
    _HomeLandCash_InsterFri(_HomeLandCash_FriendList)
  end
end
function _HomeLandCash_UpdateCash()
  gUI.HomeLandCash.NoGarden:SetVisible(false)
  local bSelf, state, maxCount, curCount, Ramain, cashModle = GetGardenInfo()
  if state == nil then
    gUI.HomeLandCash.Cashshowbox:hide()
    return
  end
  if bSelf then
    _HomeLandCash_ShowName = "我的花园"
    gUI.HomeLandCash.Garden_btn:SetProperty("Text", "给自己浇水")
    gUI.HomeLandCash.RamainDesc:SetProperty("TooltipTextDes", "")
  else
    gUI.HomeLandCash.Garden_btn:SetProperty("Text", "给好友浇水")
    gUI.HomeLandCash.RamainDesc:SetProperty("TooltipTextDes", _HomeLandCash_Tip)
  end
  if state == 4 then
    gUI.HomeLandCash.Garden_btn:SetProperty("Enable", "false")
    gUI.HomeLandCash.NoGarden:SetVisible(true)
  elseif Ramain > 0 then
    gUI.HomeLandCash.Garden_btn:SetProperty("Enable", "true")
  else
    gUI.HomeLandCash.Garden_btn:SetProperty("Enable", "false")
  end
  gUI.HomeLandCash.NameTitle:SetProperty("Text", _HomeLandCash_ShowName)
  gUI.HomeLandCash.RainCount:SetProperty("Text", tostring(curCount) .. "/" .. tostring(maxCount))
  gUI.HomeLandCash.Ramain:SetProperty("Text", tostring(Ramain))
  if cashModle ~= nil then
    if cashModle > 0 then
      gUI.HomeLandCash.showbox:SetVisible(true)
      gUI.HomeLandCash.Cashshowbox:show()
      if not gUI.HomeLandCash.Cashshowbox:isAvatarExist("HomeLand" .. tostring(cashModle)) then
        gUI.HomeLandCash.Cashshowbox:createMountAvatar(cashModle, "HomeLand")
      end
      gUI.HomeLandCash.Cashshowbox:setCurrentAvatar("HomeLand" .. tostring(cashModle))
    else
      gUI.HomeLandCash.showbox:SetVisible(false)
      gUI.HomeLandCash.Cashshowbox:hide()
    end
  end
end
function _HomeLandCash_UpdateLog()
  gUI.HomeLandCash.LogdBox:ClearAllContent()
  gUI.HomeLandCash.LogListBox:RemoveAllItems()
  if gUI.HomeLandCash.Info:IsVisible() then
    local list = HomeGetLog()
    if list[0] ~= nil then
      for i = table.maxn(list), 0, -1 do
        local str = list[i].LogInfo
        gUI.HomeLandCash.LogdBox:InsertBack(str, 4294967295, 0, 0)
      end
    end
    gUI.HomeLandCash.LogdBox:MoveToTop()
    local listRank = HomeGetRainRank()
    if listRank[0] ~= nil then
      for i = 0, table.maxn(listRank) do
        local name = listRank[i].Name
        local rainMe = listRank[i].Rain
        local beRain = listRank[i].BeRain
        if rainMe > 0 or beRain > 0 then
          local str = string.format("%s|%d|%d", name, rainMe, beRain)
          local item = gUI.HomeLandCash.LogListBox:InsertString(str, -1)
          gUI.HomeLandCash.LogListBox:SetProperty("TextHorzAlign", "Left")
          gUI.HomeLandCash.LogListBox:SetProperty("TextHorzAlign", "HCenter")
        end
      end
    end
  end
  MarkFarmLogRead()
end
function _HomeLandCash_UpdateMain()
  AskHomeInfo(0, _HomeLandCash_Select_Fri)
end
function _HomeLandCash_UpdateWater()
  if gUI.HomeLandCash.Garden:IsVisible() then
    _HomeLandCash_UpdateMain()
    _HomeLandCash_UpdateFri()
  end
end
function _HomeLandCash_MainShow()
  gUI.HomeLandCash.Root:SetVisible(true)
  _HL_BarPos = nil
  _HomeLandCash_Select_Fri = -1
  gUI.HomeLandCash.TableCtrl:SetSelectedState(0)
  _HomeLandCash_UpdateMain()
  gUI.HomeLandCash.Garden:SetVisible(true)
  gUI.HomeLandCash.Info:SetVisible(false)
  _HomeLandCash_UpdateFri()
end
function UI_HomeLandCash_ShowHomeLandCash()
  if gUI.HomeLandCash.Root:IsVisible() then
    gUI.HomeLandCash.Root:SetVisible(false)
    gUI.HomeLandCash.Cashshowbox:hide()
  else
    _HomeLandCash_MainShow()
  end
end
function UI_HomeLandCash_ShowMain()
  if not gUI.HomeLandCash.Root:IsVisible() then
    _HomeLandCash_MainShow()
  end
end
function UI_HomeLandCash_CloseHomeLandCash()
  gUI.HomeLandCash.Root:SetVisible(false)
  gUI.HomeLandCash.Cashshowbox:hide()
end
function UI_HomeLandCash_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("HomeLand.Image_bg.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_HomeLandCash_ActiveCash()
  Messagebox_Show("ACTIVE_CASH")
end
function UI_HomeLandCash_FarmAddRain()
  FarmAddRain()
end
function UI_HomeLandCash_FarmAddHarden()
  FarmAddHarden()
end
function UI_HomeLandCash_FarmGainCash()
  FarmGainCash()
  gUI.HomeLandCash.Harden1:SetVisible(false)
  gUI.HomeLandCash.Harden2:SetVisible(false)
  gUI.HomeLandCash.Harden3:SetVisible(false)
end
function UI_HomeLandCash_SelectedFri(msg)
  local item = msg:get_window()
  if not item then
    return
  end
  local index = item:GetCustomUserData(1)
  if index >= 0 then
    local itemTmp = gUI.HomeLandCash.NameContainer:GetChildByData1(index)
    for i = 1, _HomeLandCash_Fri_Count do
      local itemTmp = gUI.HomeLandCash.NameContainer:GetChildByData1(i)
      if itemTmp then
        local wnd = WindowToButton(itemTmp)
        if itemTmp:GetCustomUserData(1) == index then
          wnd:SetStatus("selected")
          local target_guid = itemTmp:GetUserData64()
          _HomeLandCash_ShowName = itemTmp:GetProperty("Text")
          _HomeLandCash_Select_Fri = target_guid
          local scrollbar = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Right_bg.Target_cnt.vertSB_")
          if scrollbar then
            scrollbar = WindowToScrollBar(scrollbar)
            _HL_BarPos = scrollbar:GetCurrentPos()
          end
          AskHomeInfo(0, target_guid)
        else
          wnd:SetStatus("normal")
        end
      end
    end
  end
end
function UI_HomeLandCash_ReturnHome()
  _HomeLandCash_Select_Fri = -1
  _HomeLandCash_UpdateMain()
end
function UI_HomeLandCash_ShowInfo()
end
function UI_HomeLandCash_PageChanged()
  local selected = gUI.HomeLandCash.TableCtrl:GetSelectItem()
  if selected == 0 then
    _HomeLandCash_UpdateMain()
    gUI.HomeLandCash.Garden:SetVisible(true)
    gUI.HomeLandCash.Info:SetVisible(false)
  else
    FarmLogRefresh()
    gUI.HomeLandCash.LogdBox:ClearAllContent()
    gUI.HomeLandCash.LogListBox:RemoveAllItems()
    gUI.HomeLandCash.Garden:SetVisible(false)
    gUI.HomeLandCash.Info:SetVisible(true)
  end
end
function Script_HomeLandCash_OnEvent(event)
  if event == "HOME_UPDATE_FRI_LAND" then
    _HomeLandCash_Update()
  elseif event == "HOME_UPDATE_CASH" then
    _HomeLandCash_Update()
  elseif event == "HOME_LOG_UPDATE" then
    _HomeLandCash_UpdateLog()
  elseif event == "COMMUNITY_UPDATE_RELATION" then
    _HomeLandCash_AddFri(arg1)
  elseif event == "HOME_UPDATE_WATER" then
    _HomeLandCash_UpdateWater()
  end
end
function Script_HomeLandCash_OnLoad(event)
  gUI.HomeLandCash.Root = WindowSys_Instance:GetWindow("HomeLand")
  gUI.HomeLandCash.Garden = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg")
  gUI.HomeLandCash.Info = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Info_bg")
  gUI.HomeLandCash.RainCount = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Left_bg.Down_bg.Present_bg.Present_dlab")
  gUI.HomeLandCash.NameTitle = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Left_bg.Top_bg.Title_dlab")
  gUI.HomeLandCash.NameContainer = WindowToContainer(WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Right_bg.Target_cnt"))
  gUI.HomeLandCash.WaterBtn = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Right_bg.CheckButton1")
  gUI.HomeLandCash.FriBtn = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Right_bg.CheckButton2")
  gUI.HomeLandCash.FriTemp = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Right_bg.Tem_btn")
  gUI.HomeLandCash.Ramain = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Left_bg.Down_bg.Remain_bg.Remain_dlab")
  gUI.HomeLandCash.Garden_btn = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Left_bg.Down_bg.Garden_btn")
  gUI.HomeLandCash.TableCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("HomeLand.Image_bg.Top_bg.TableCtrl1_tctl"))
  gUI.HomeLandCash.LogdBox = WindowToDisplayBox(WindowSys_Instance:GetWindow("HomeLand.Image_bg.Info_bg.Up_bg.Info_dbox"))
  gUI.HomeLandCash.LogListBox = WindowToListBox(WindowSys_Instance:GetWindow("HomeLand.Image_bg.Info_bg.Down_bg.Info_lbox"))
  gUI.HomeLandCash.Cashshowbox = WindowToPicture(WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Left_bg.Top_bg.Model_pic"))
  local showbox = UIInterface:getShowbox(_HomeLandCash_ShowBoxName)
  showbox = showbox or UIInterface:createShowbox(_HomeLandCash_ShowBoxName, gUI.HomeLandCash.Cashshowbox)
  gUI.HomeLandCash.Cashshowbox = showbox
  gUI.HomeLandCash.showbox = WindowToPicture(WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Left_bg.Top_bg.Model_pic"))
  gUI.HomeLandCash.NoGarden = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Left_bg.Top_bg.NoGarden_lab")
  gUI.HomeLandCash.RamainDesc = WindowSys_Instance:GetWindow("HomeLand.Image_bg.Garden_bg.Left_bg.Down_bg.Remain_bg.Remain_slab")
  _HL_BarPos = nil
  _HomeLandCash_Select_Fri = -1
end
