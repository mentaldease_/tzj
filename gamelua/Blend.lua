if gUI and not gUI.Blend then
  gUI.Blend = {}
end
BLEND_SLOT_COLLATE = {
  MAINEQUIP = 0,
  SUBEQUIP = 1,
  MATERIAL = 2
}
local ItemGUID = 0
local ItemPos = -1
local ItemBagType = -1
local BagSlotOffset = -1
local BagSlotOffsetNew = -1
local ItemSubGuid = -1
local ItemSubSlot = -1
local ItemSubType = -1
local CanFlag = false
local ItemGUIDLast = 0
local ItemPosLast = -1
local MATERIALSOLTRECORD = -1
local BLENDSUBEQUIP = -1
_CHARA_EQUIPPOINT_CANTAINBLEND = {
  [0] = {0, "武器"},
  [9] = {1, "头盔"},
  [1] = {2, "衣服"},
  [4] = {3, "肩饰"},
  [2] = {4, "手套"},
  [5] = {5, "鞋子"},
  [8] = {6, "耳环"},
  [6] = {7, "项链"},
  [3] = {8, "手镯"},
  [7] = {9, "戒指"},
  [10] = {10, "时装"}
}
_CHARA_EQUIPPOINT_CANTAINBLEND1 = {
  [0] = {0, "武器"},
  [1] = {9, "头盔"},
  [2] = {1, "衣服"},
  [3] = {4, "肩饰"},
  [4] = {2, "手套"},
  [5] = {5, "鞋子"},
  [6] = {8, "耳环"},
  [7] = {6, "项链"},
  [8] = {3, "手镯"},
  [9] = {7, "戒指"},
  [10] = {10, "时装"}
}
function UI_Blend_OpenPanel(msg)
  _OnBlend_ShowWindow()
end
function _OnBlend_ShowWindow()
  local wnd = WindowSys_Instance:GetWindow("Blend")
  if wnd:IsVisible() then
    wnd:SetProperty("Visible", "false")
    return
  end
  wnd:SetProperty("Visible", "true")
  if gUI.Blend.QuiltyInterface:IsVisible() then
    gUI.Blend.FunctionBg:SetProperty("Left", "p315")
  else
    gUI.Blend.FunctionBg:SetProperty("Left", "p110")
  end
  Lua_Blend_Init()
  Lua_Bag_ShowUI(true)
  gUI.Blend.FunctionBg:SetVisible(false)
end
function _OnBlend_ChangeEquip()
  local wnd = WindowSys_Instance:GetWindow("Blend")
  if wnd:IsVisible() then
    Lua_Blend_ShowItemCantainEquip()
  end
end
function _OnBlend_OnPanelOpen()
  _OnBlend_ShowWindow()
end
function Lua_Blend_Init()
  gUI.Blend.MainEquipBox:ClearGoodsItem(0)
  gUI.Blend.SideEquipBox:ClearGoodsItem(0)
  gUI.Blend.MaterialBox:ClearGoodsItem(0)
  gUI.Blend.MainEquipBox:SetItemBindState(0, 0)
  gUI.Blend.SideEquipBox:SetItemBindState(0, 0)
  gUI.Blend.MaterialBox:SetItemBindState(0, 0)
  gUI.Blend.BlendEquipInfo:RemoveAllItems()
  gUI.Blend.SuccLab:SetProperty("Text", "0")
  gUI.Blend.CostLab:SetProperty("Text", "0")
  gUI.Blend.ItemCantainer:DeleteAll()
  for i = 0, 2 do
    gUI.Blend.QulityCheckBtn[i]:SetProperty("Checked", "true")
  end
  Lua_Blend_ShowItemCantainEquip()
end
function Lua_Blend_ShowItemCantainEquip()
  local EquipContainInfos = GetEquipContain()
  if EquipContainInfos[0] ~= nil then
    for n = 0, table.maxn(EquipContainInfos) do
      if EquipContainInfos[n].Icon ~= "" then
        gUI.Blend.MyEquipBox:SetItemGoods(_CHARA_EQUIPPOINT_CANTAINBLEND[n][1], EquipContainInfos[n].Icon, EquipContainInfos[n].quality)
        local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.myequipbox, n)
        if 0 < ForgeLevel_To_Stars[intenLev] then
          gUI.Blend.MyEquipBox:SetItemStarState(_CHARA_EQUIPPOINT_CANTAINBLEND[n][1], ForgeLevel_To_Stars[intenLev])
        else
          gUI.Blend.MyEquipBox:SetItemStarState(_CHARA_EQUIPPOINT_CANTAINBLEND[n][1], 0)
        end
        gUI.Blend.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINBLEND[n][1]]:SetProperty("Enable", "true")
        gUI.Blend.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINBLEND[n][1]]:SetProperty("Text", "放入")
      else
        gUI.Blend.MyEquipBox:ClearGoodsItem(_CHARA_EQUIPPOINT_CANTAINBLEND[n][1])
        if n == ItemPos then
          gUI.Blend.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINBLEND[n][1]]:SetProperty("Enable", "true")
          gUI.Blend.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINBLEND[n][1]]:SetProperty("Text", "{#R^收回}")
        else
          gUI.Blend.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINBLEND[n][1]]:SetProperty("Enable", "false")
          gUI.Blend.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINBLEND[n][1]]:SetProperty("Text", "放入")
        end
      end
    end
  end
end
function UI_Blend_SideEquipDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local itemId = msg:get_wparam()
  local form_win = to_win:GetDragItemParent()
  local form_type = form_win:GetProperty("BoxType")
  if form_type == "backpack0" then
    itemId = Lua_Bag_GetRealSlot(form_type, itemId)
    Lua_Blend_CheckSideEquip(itemId)
  end
end
function UI_Blend_OkBtnClick(msg)
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  local sbag, sslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.SUBEQUIP)
  if not sslot or not mslot then
    Lua_Chat_ShowOSD("BLEND_NO_EQUIPMENT")
    return
  end
  local mabag, maslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MATERIAL)
  BLENDSUBEQUIP = sslot
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  local _, coefMain, kindMain, _, qualityMain = GetBlendEquipInfo(mslot)
  local _, coefSide, kindSide, _, qualitySide = GetBlendEquipInfo(sslot)
  if kindMain ~= kindSide then
    Lua_Chat_ShowOSD("BLEND_PART_DIFF")
    return false
  end
  if coefMain ~= coefSide then
    Lua_Chat_ShowOSD("BLEND_ATTR_DIFF")
    return false
  end
  if CanFlag == false then
    Lua_Chat_ShowOSD("BLEND_ATTR_NOADD")
    return
  end
  local _, _, level, _, _ = GetForgeEquipInfo(sslot, sbag)
  if level >= 7 then
    Lua_Chat_ShowOSD("BLEND_SUB_CANNOTOVER7")
    return false
  end
  local needMoney, needBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, name, per, unCounts, bindCounts = GetBlendConsume(mslot)
  local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
  if gUI.Blend.MoneyFrist:IsChecked() then
    if tonumber(nonBindemoney) < tonumber(needMoney) then
      Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
      return false
    end
  elseif tonumber(nonBindemoney) + tonumber(bindMoney) < tonumber(needMoney) then
    Lua_Chat_ShowOSD("NOT_ENOUGH_MONEY")
    return false
  end
  if gUI.Blend.MaterialFrist:IsChecked() then
    if unCounts < needStoneCount then
      Lua_Chat_ShowOSD("BLEND_MAT_NOENOUGHUNBINDMATER")
      return false
    end
  elseif totalPropInBag < needStoneCount then
    Lua_Chat_ShowOSD("BLEND_MAT_NOENOUGHBINDMATER")
    return false
  end
  if qualitySide >= 3 then
    Messagebox_Show("BLEND_ASK_CONFIRM_RARE")
  else
    Lua_Blend_ComfirmForge()
  end
end
function Lua_Blend_ComfirmForge()
  local sbag, sslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.SUBEQUIP)
  if not sbag then
    return
  end
  local _, _, level = GetForgeEquipInfo(sslot, gUI_GOODSBOX_BAG.backpack0, 0)
  if level and level > 0 then
    Messagebox_Show("BLEND_ASK_CONFIRM_FORGE")
  else
    Lua_Blend_ComfirmGem()
  end
end
function Lua_Blend_ComfirmGem()
  local sbag, sslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.SUBEQUIP)
  if not sbag then
    return
  end
  local isEmbbed = GetEquipGemInfo(gUI_GOODSBOX_BAG.backpack0, sslot, 0)
  if isEmbbed == true then
    Messagebox_Show("BLEND_ASK_CONFIRM_GEM")
  else
    Lua_Blend_ComfirmBind()
  end
end
function Lua_Blend_ComfirmBind()
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  local sbag, sslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.SUBEQUIP)
  if GetBindInfo(sslot, sbag) == true and GetBindInfo(mslot, mbag) == false then
    Messagebox_Show("BLEND_ASK_CONFIRM_BIND")
  elseif GetBindInfo(mslot, mbag) == true then
    BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
  else
    Lua_Blend_ComfirmBindMater()
  end
end
function Lua_Blend_ComfirmBindMoneyAndMater()
  if gUI.Blend.MaterialFrist:IsChecked() or gUI.Blend.MoneyFrist:IsChecked() then
    Messagebox_Show("BLEND_ASK_CONFIRM_MONEYANDMATER")
  else
    local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
    local needMoney, needBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, name, per, unCounts, bindCounts = GetBlendConsume(mslot)
    if bindCounts < needStoneCount then
      Messagebox_Show("BLEND_ASK_CONFIRM_MONEYANDMATER1")
    else
      Lua_Blend_ComfirmBindMater3()
    end
  end
end
function Lua_Blend_ComfirmBindMater3()
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
  local needMoney, needBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, name, per, unCounts, bindCounts = GetBlendConsume(mslot)
  if tonumber(bindMoney) < tonumber(needMoney) then
    Messagebox_Show("BLEND_ASK_CONFIRM_MONEYANDMATER2")
  else
    BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
  end
end
function Lua_Blend_ComfirmBindMater()
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  local needMoney, needBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, name, per, unCounts, bindCounts = GetBlendConsume(mslot)
  if gUI.Blend.MaterialFrist:IsChecked() then
    if needStoneCount <= unCounts then
      Lua_Blend_ComfirmBindMoney1()
    end
  elseif totalPropInBag < needStoneCount then
  elseif bindCounts < needStoneCount and bindCounts > 0 then
    Messagebox_Show("BLEND_ASK_CONFIRM_BINDMATER1")
  elseif bindCounts == 0 then
    Lua_Blend_ComfirmBindMoney1()
  else
    Messagebox_Show("BLEND_ASK_CONFIRM_BINDMATER2")
  end
end
function Lua_Blend_ComfirmBindMoney1()
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
  local needMoney, needBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, name, per, unCounts, bindCounts = GetBlendConsume(mslot)
  if gUI.Blend.MoneyFrist:IsChecked() then
    if tonumber(nonBindemoney) >= tonumber(needMoney) then
      BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
    else
    end
  elseif bindMoney > 0 then
    Messagebox_Show("BLEND_ASK_CONFIRM_MONEY3")
  else
    BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
  end
end
function Lua_Blend_ComfirmBindMoney()
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  local needMoney, needBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, name, per, unCounts, bindCounts = GetBlendConsume(mslot)
  local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
  if gUI.Blend.MoneyFrist:IsChecked() then
    Messagebox_Show("BLEND_ASK_CONFIRM_MONEY1")
  elseif tonumber(bindMoney) < tonumber(needMoney) then
    Messagebox_Show("BLEND_ASK_CONFIRM_MONEY2")
  else
    BeginBlend(gUI.Blend.MoneyFrist:IsChecked(), gUI.Blend.MaterialFrist:IsChecked())
  end
end
function UI_MaterialChecked(msg)
  Blend_UpdateStones()
end
function UI_MoneyChecked(msg)
  Blend_UpdateSuccAndMoney()
end
function UI_ShowNewItems(msg)
  local sbag, sslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if not sbag then
    return
  end
  Blend_UpdateShowCommonItems()
end
function UI_Blend_QuickBuy(msg)
  local btn = msg:get_window()
  local parent = btn:GetParent()
  local goodsBox = WindowToGoodsBox(parent:GetChildByName("ForgeStone_gbox"))
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if mslot then
    local bindMoney, nonBindMoney, stoneSlot, bindStone, nonBindStone, needPropCount, totalPropInBag, icon = GetBlendConsume(mslot)
    local itemID = bindStone
    Lua_RMBShop_QuickBuy(itemID)
  end
end
function UI_Blend_MaterialPlace(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local itemId = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local form_type = from_win:GetProperty("BoxType")
  local bagType = gUI_GOODSBOX_BAG[form_type]
  local boxName = to_win:GetProperty("WindowName")
  if bagType ~= gUI_GOODSBOX_BAG.backpack0 then
    return
  end
  local materialbind, materialunbind, name = Lua_BlendStoneId()
  local itemNewId = GetTableIdBySlot(gUI_GOODSBOX_BAG.backpack0, itemId)
  if materialbind == itemNewId or materialunbind == itemNewId then
    SetBlendEquip(itemId, OperateMode.Add, BLEND_SLOT_COLLATE.MATERIAL)
    return
  else
    local strTips = "你放置材料错误，请放入" .. tostring(name)
    ShowErrorMessage(strTips)
  end
end
function Lua_Blend_GetRBtnTargetSlot()
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if not mslot then
    return "AddMainEquip"
  else
    return "AddSideEquip"
  end
end
function Lua_BlendStoneId()
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if not mslot then
    return -1, -1
  end
  local bindMoney, nonBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, name = GetBlendConsume(mslot)
  return bindStone, nonBindStone, name
end
function Lua_Blend_CheckSideEquip(slot)
  if not slot or slot == -1 then
    return false
  end
  if NPC_IsEquip(slot) == false then
    Lua_Chat_ShowOSD("BLEND_NOT_RIGHT_EQUIPED")
    return false
  end
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if not mslot then
    Lua_Chat_ShowOSD("BLEND_NO_MAIN_EUIQPMENT")
    return false
  end
  local _, coefMain, kindMain, _, _ = GetBlendEquipInfo(mslot)
  local _, coefSide, kindSide, _, _ = GetBlendEquipInfo(slot)
  if kindMain ~= kindSide then
    Lua_Chat_ShowOSD("BLEND_PART_DIFF")
    return false
  end
  if coefMain ~= coefSide then
    Lua_Chat_ShowOSD("BLEND_ATTR_DIFF")
    return false
  end
  local _, _, level, _, _ = GetForgeEquipInfo(slot, mbag)
  if level >= 7 then
    Lua_Chat_ShowOSD("BLEND_SUB_CANNOTOVER7")
    return false
  end
  SetBlendEquip(slot, OperateMode.Add, BLEND_SLOT_COLLATE.SUBEQUIP)
end
function UI_Blend_MainEquipDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local itemId = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    itemId = Lua_Bag_GetRealSlot(from_win, itemId)
    Lua_Blend_CheckMainEquip(itemId)
  else
    return
  end
end
function Lua_Blend_CheckMainEquip(slot)
  if NPC_IsEquip(slot) == false then
    Lua_Chat_ShowOSD("BLEND_NOT_RIGHT_EQUIPED")
    return false
  end
  local mainAttr = {}
  mainAttr = GetBlendAttr(slot, nil, nil)
  if mainAttr[0] == nil then
    Lua_Chat_ShowOSD("BLEND_NO_ATTR")
    return false
  end
  Lua_Blend_ResetItemByGuid()
  SetBlendEquip(slot, OperateMode.Add, BLEND_SLOT_COLLATE.MAINEQUIP)
  return true
end
function UI_Equip_Click(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local offset = msg:get_lparam()
  UI_Place_MainEquip(offset)
end
function UI_Place_EquipToBlend(msg)
  local wnd = msg:get_window()
  local text = wnd:GetProperty("Text")
  if text == "{#R^收回}" then
    Lua_Blend_ResetItemByGuid()
    SetBlendEquip(0, OperateMode.Cancel, BLEND_SLOT_COLLATE.MAINEQUIP)
  else
    local index = wnd:GetProperty("CustomUserData")
    UI_Place_MainEquip(tonumber(index))
  end
end
function UI_Place_MainEquip(offset)
  local wnd = WindowToGoodsBox(WindowSys_Instance:GetWindow("Blend.Arrows_bg.Equip_bg.Equip_gbox"))
  if wnd:IsItemHasIcon(offset) then
    BagSlotOffset = GetItemBlankSlot(1)
    if BagSlotOffset == -1 then
      Lua_Chat_ShowOSD("FORGE_BAGNOSET")
      return
    end
    local mainAttr = {}
    mainAttr = GetBlendAttr(_CHARA_EQUIPPOINT_CANTAINBLEND1[offset][1], gUI_GOODSBOX_BAG.myequipbox, nil)
    if mainAttr[0] == nil then
      Lua_Chat_ShowOSD("BLEND_NO_ATTR")
      return false
    end
    Lua_Blend_ResetItemByGuid()
    ItemGUID = GetItemGUIDByPos(gUI_GOODSBOX_BAG.myequipbox, _CHARA_EQUIPPOINT_CANTAINBLEND1[offset][1], 0)
    ItemPos = _CHARA_EQUIPPOINT_CANTAINBLEND1[offset][1]
    ItemBagType = gUI_GOODSBOX_BAG.myequipbox
    MoveItem(gUI_GOODSBOX_BAG.myequipbox, ItemPos, gUI_GOODSBOX_BAG.backpack0, BagSlotOffset)
    SetBlendEquip(BagSlotOffset, OperateMode.Add, BLEND_SLOT_COLLATE.MAINEQUIP)
  end
end
function UI_SetSubEquip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local Guid = wnd:GetUserData64()
  UI_SetSubEquipByGuid(Guid)
end
function UI_SetSubEquipByGuid(Guid)
  local bagType1, itemSlot1 = GetItemPosByGuid(Guid)
  if bagType1 == gUI_GOODSBOX_BAG.backpack0 then
    SetBlendEquip(itemSlot1, OperateMode.Add, BLEND_SLOT_COLLATE.SUBEQUIP)
  elseif bagType1 == gUI_GOODSBOX_BAG.myequipbox then
    Messagebox_Show("BLEND_ASK_EQUIP", itemSlot1, Guid)
  end
end
function UI_SetSubEquipByButton(msg)
  local wnd = WindowToButton(msg:get_window())
  local wnd_parent = WindowToGoodsBox(wnd:GetParent())
  if wnd:GetProperty("Text") == "放入" then
    local Guid = wnd_parent:GetUserData64()
    UI_SetSubEquipByGuid(Guid)
  else
    local sbag, sslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.SUBEQUIP)
    if not sslot then
      return
    end
    SetBlendEquip(0, OperateMode.Remove, BLEND_SLOT_COLLATE.SUBEQUIP)
  end
end
function Blend_AddEquipToSub(slot, Guid)
  BagSlotOffsetNew = GetItemBlankSlot(1)
  if BagSlotOffsetNew == -1 then
    Lua_Chat_ShowOSD("FORGE_BAGNOSET")
    return
  end
  MoveItem(gUI_GOODSBOX_BAG.myequipbox, slot, gUI_GOODSBOX_BAG.backpack0, BagSlotOffsetNew)
  SetBlendEquip(BagSlotOffsetNew, OperateMode.Add, BLEND_SLOT_COLLATE.SUBEQUIP)
  ItemSubGuid = Guid
  ItemSubSlot = slot
  ItemSubType = gUI_GOODSBOX_BAG.myequipbox
end
function Blend_Reset_Sub(Guid)
  if Guid ~= ItemSubGuid then
    if ItemSubType == gUI_GOODSBOX_BAG.myequipbox then
      local isExist = GetMainEquipIsExist(ItemSubSlot)
      if isExist == 0 then
        local bagType, bagSlot = GetItemPosByGuid(ItemSubGuid)
        if bagType == gUI_GOODSBOX_BAG.backpack0 then
          local bagType1, bagSlot1 = GetBlendSlotInfo(BLEND_SLOT_COLLATE.SUBEQUIP)
          local ItemGuidNew = GetItemGUIDByPos(bagType1, bagSlot1)
          if ItemSubGuid == ItemGuidNew then
            SetBlendEquip(0, OperateMode.Remove, BLEND_SLOT_COLLATE.SUBEQUIP)
          end
          MoveItem(gUI_GOODSBOX_BAG.backpack0, bagSlot, gUI_GOODSBOX_BAG.myequipbox, ItemSubSlot)
        end
      end
    end
    ItemSubGuid = 0
    ItemSubSlot = -1
    ItemSubType = -1
  end
end
function Lua_Blend_ResetItemByGuid()
  if ItemBagType == gUI_GOODSBOX_BAG.myequipbox then
    local isExist = GetMainEquipIsExist(ItemPos)
    if isExist == 0 then
      local bagType, bagSlot = GetItemPosByGuid(ItemGUID)
      if bagType == gUI_GOODSBOX_BAG.backpack0 then
        SetBlendEquip(0, OperateMode.Cancel, BLEND_SLOT_COLLATE.MAINEQUIP)
        MoveItem(gUI_GOODSBOX_BAG.backpack0, bagSlot, gUI_GOODSBOX_BAG.myequipbox, ItemPos)
      end
    end
  end
  ItemGUID = 0
  ItemPos = -1
  ItemBagType = -1
  Blend_Reset_Sub()
end
function UI_Blend_CloseBtnClick(msg)
  _Blend_CloseWindow()
end
function _Blend_CloseWindow()
  local wnd = WindowSys_Instance:GetWindow("Blend")
  if not wnd:IsVisible() then
    return
  end
  wnd:SetProperty("Visible", "false")
  Messagebox_Hide("BLEND_ASK_CONFIRM_FORGE")
  Messagebox_Hide("BLEND_ASK_CONFIRM_GEM")
  Messagebox_Hide("BLEND_ASK_CONFIRM_BIND")
  Messagebox_Hide("BLEND_ASK_CONFIRM_RARE")
  Messagebox_Hide("BLEND_ASK_CONFIRM_MONEYANDMATER2")
  Messagebox_Hide("BLEND_ASK_CONFIRM_MONEYANDMATER1")
  Messagebox_Hide("BLEND_ASK_CONFIRM_MONEYANDMATER")
  Messagebox_Hide("BLEND_ASK_CONFIRM_MONEY3")
  Messagebox_Hide("BLEND_ASK_CONFIRM_MONEY2")
  Messagebox_Hide("BLEND_ASK_CONFIRM_MONEY1")
  Messagebox_Hide("BLEND_ASK_CONFIRM_BINDMATER2")
  Messagebox_Hide("BLEND_ASK_CONFIRM_BINDMATER1")
end
function _OnBlend_CloseWindw()
  _Blend_CloseWindow()
end
function UI_Blend_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Blend.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function Blend_CANCEL()
  SetBlendEquip(0, OperateMode.Cancel, BLEND_SLOT_COLLATE.MAINEQUIP)
end
function Blend_UpdateSuccAndMoney(newMoney, changeMoney, event)
  local wnd = WindowSys_Instance:GetWindow("Blend")
  if not wnd:IsVisible() then
    return
  end
  local bagType, itemSlot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if bagType == gUI_GOODSBOX_BAG.backpack0 then
    local bindMoney, nonBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, names, per = GetBlendConsume(itemSlot)
    if gUI.Blend.MoneyFrist:IsChecked() then
      gUI.Blend.CostLab:SetProperty("Text", Lua_UI_Money2String(bindMoney, true, false, false, true, newMoney, event))
    else
      gUI.Blend.CostLab:SetProperty("Text", Lua_UI_Money2String(bindMoney, false, false, false, true, newMoney, event))
    end
  end
end
function Blend_UpdateSucc()
  local wnd = WindowSys_Instance:GetWindow("Blend")
  if not wnd:IsVisible() then
    return
  end
  local bagType, itemSlot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if bagType == 1 then
    local bindMoney, nonBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, names, per = GetBlendConsume(itemSlot)
    gUI.Blend.SuccLab:SetProperty("Text", tostring(per) .. "%")
  end
end
function Blend_UpdateShowCommonItems()
  local bagType, itemSlot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if not itemSlot then
    return
  end
  local rateList = GetBlendAttr(itemSlot, bagType, 0)
  local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffectTime, sellPrice, _, equipKind, offlineTime, isTimeLimit, EquipPoint = GetItemInfoBySlot(bagType, itemSlot, 0)
  local _, coefMain, kindMain, _, _ = GetBlendEquipInfo(itemSlot)
  gUI.Blend.ItemCantainer:DeleteAll()
  local index = 0
  for i = 0, table.maxn(rateList) do
    if rateList[0] == nil then
      return
    end
    local GuidList = GetBlendCommonInfo(rateList[i].typeId, rateList[i].value, EquipPoint, minLevel, coefMain)
    local newItems = WindowSys_Instance:GetWindow("Blend.Arrow_bg.Property_bg.Item_bg")
    newItems:SetVisible(false)
    local count = 0
    local BlendName, newObj, Gb1, Gb2, Gb3
    if GuidList ~= nil then
      for j = 0, table.maxn(GuidList) do
        local CREATEFLAG = false
        local itemGuid = GuidList[j].ItemGuid
        local bagType1, itemSlot1 = GetItemPosByGuid(itemGuid)
        if itemSlot1 ~= -1 then
          local _, iconNew, qualityNew = GetItemInfoBySlot(bagType1, itemSlot1, 0)
          if count >= 3 then
            break
          end
          if gUI.Blend.QulityCheckBtn[0]:IsChecked() and qualityNew == 3 then
            count = count + 1
            CREATEFLAG = true
          elseif gUI.Blend.QulityCheckBtn[1]:IsChecked() and qualityNew == 2 then
            count = count + 1
            CREATEFLAG = true
          elseif gUI.Blend.QulityCheckBtn[2]:IsChecked() and qualityNew == 1 then
            count = count + 1
            CREATEFLAG = true
          elseif qualityNew == 0 or qualityNew == 4 then
            count = count + 1
            CREATEFLAG = true
          end
          if count == 1 and CREATEFLAG then
            newObj = gUI.Blend.ItemCantainer:Insert(-1, newItems)
            newObj:SetProperty("WindowName", "Property1_gbox_index" .. tostring(index))
            BlendName = WindowToLabel(newObj:GetGrandChild("Property1_dlab"))
            BlendName:SetProperty("Text", rateList[i].name)
            index = index + 1
            Gb1 = WindowToGoodsBox(newObj:GetGrandChild("Property1_gbox"))
            Gb1:SetProperty("Visible", "false")
            Gb2 = WindowToGoodsBox(newObj:GetGrandChild("Property2_gbox"))
            Gb2:SetProperty("Visible", "false")
            Gb3 = WindowToGoodsBox(newObj:GetGrandChild("Property3_gbox"))
            Gb3:SetProperty("Visible", "false")
            newObj:SetVisible(true)
          end
          if count == 1 and CREATEFLAG then
            Gb1:SetItemGoods(0, iconNew, qualityNew)
            local intenLev = Lua_Bag_GetStarInfo(bagType1, itemSlot1)
            if 0 < ForgeLevel_To_Stars[intenLev] then
              Gb1:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
            else
              Gb1:SetItemStarState(0, 0)
            end
            local bindinfo = Lua_Bag_GetBindInfo(bagType1, itemSlot1)
            Gb1:SetItemBindState(0, bindinfo)
            Gb1:SetUserData64(itemGuid)
            local gbText = WindowToLabel(Gb1:GetGrandChild("Label2_dlab"))
            local Eq_bg = Gb1:GetGrandChild("Eq_bg")
            local diffdes = GuidList[j].Diff
            if Lua_Tip_NeedShowRate(rateList[i].typeId) then
              diffdes = string.format("%.2f", Lua_Tip_PostProcessVal(rateList[i].typeId, GuidList[j].Diff)) .. "%"
            end
            gbText:SetProperty("Text", "+" .. diffdes)
            Gb1:SetProperty("Visible", "true")
            local btn1 = Gb1:GetGrandChild("Add_btn")
            local guid = GetGuidByNpcSer(NpcFunction.NPC_FUNC_EQUIP_BLEND, BLEND_SLOT_COLLATE.SUBEQUIP)
            if guid == itemGuid then
              btn1:SetProperty("Text", "{#R^收回}")
            else
              btn1:SetProperty("Text", "放入")
            end
            if bagType1 == gUI_GOODSBOX_BAG.myequipbox then
              Eq_bg:SetProperty("Visible", "true")
            else
              Eq_bg:SetProperty("Visible", "false")
            end
          elseif count == 2 and CREATEFLAG then
            Gb2:SetItemGoods(0, iconNew, qualityNew)
            local intenLev = Lua_Bag_GetStarInfo(bagType1, itemSlot1)
            if 0 < ForgeLevel_To_Stars[intenLev] then
              Gb2:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
            else
              Gb2:SetItemStarState(0, 0)
            end
            local bindinfo = Lua_Bag_GetBindInfo(bagType1, itemSlot1)
            Gb2:SetItemBindState(0, bindinfo)
            Gb2:SetUserData64(itemGuid)
            local gbText = WindowToLabel(Gb2:GetGrandChild("Label1_dlab"))
            local Eq_bg = Gb2:GetGrandChild("Eq_bg")
            local diffdes = GuidList[j].Diff
            if Lua_Tip_NeedShowRate(rateList[i].typeId) then
              diffdes = string.format("%.2f", Lua_Tip_PostProcessVal(rateList[i].typeId, GuidList[j].Diff)) .. "%"
            end
            gbText:SetProperty("Text", "+" .. diffdes)
            Gb2:SetProperty("Visible", "true")
            local btn2 = Gb2:GetGrandChild("Add_btn")
            local guid = GetGuidByNpcSer(NpcFunction.NPC_FUNC_EQUIP_BLEND, BLEND_SLOT_COLLATE.SUBEQUIP)
            if guid == itemGuid then
              btn2:SetProperty("Text", "{#R^收回}")
            else
              btn2:SetProperty("Text", "放入")
            end
            if bagType1 == gUI_GOODSBOX_BAG.myequipbox then
              Eq_bg:SetProperty("Visible", "true")
            else
              Eq_bg:SetProperty("Visible", "false")
            end
          elseif count == 3 and CREATEFLAG then
            Gb3:SetItemGoods(0, iconNew, qualityNew)
            local intenLev = Lua_Bag_GetStarInfo(bagType1, itemSlot1)
            if 0 < ForgeLevel_To_Stars[intenLev] then
              Gb3:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
            else
              Gb3:SetItemStarState(0, 0)
            end
            local bindinfo = Lua_Bag_GetBindInfo(bagType1, itemSlot1)
            Gb3:SetItemBindState(0, bindinfo)
            Gb3:SetUserData64(itemGuid)
            local gbText = WindowToLabel(Gb3:GetGrandChild("Label3_dlab"))
            local Eq_bg = Gb3:GetGrandChild("Eq_bg")
            local diffdes = GuidList[j].Diff
            if Lua_Tip_NeedShowRate(rateList[i].typeId) then
              diffdes = string.format("%.2f", Lua_Tip_PostProcessVal(rateList[i].typeId, GuidList[j].Diff)) .. "%"
            end
            gbText:SetProperty("Text", "+" .. diffdes)
            Gb3:SetProperty("Visible", "true")
            local btn3 = Gb3:GetGrandChild("Add_btn")
            local guid = GetGuidByNpcSer(NpcFunction.NPC_FUNC_EQUIP_BLEND, BLEND_SLOT_COLLATE.SUBEQUIP)
            if guid == itemGuid then
              btn3:SetProperty("Text", "{#R^收回}")
            else
              btn3:SetProperty("Text", "放入")
            end
            if bagType1 == gUI_GOODSBOX_BAG.myequipbox then
              Eq_bg:SetProperty("Visible", "true")
            else
              Eq_bg:SetProperty("Visible", "false")
            end
          end
        end
      end
    end
  end
end
function Blend_UpdateIcon()
  local bagType, itemSlot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  local _, icon, quality = GetItemInfoBySlot(bagType, itemSlot, 0)
  if not icon then
    return
  end
  gUI.Blend.MainEquipBox:SetItemGoods(0, icon, quality)
  local intenLev = Lua_Bag_GetStarInfo(bagType, itemSlot)
  if 0 < ForgeLevel_To_Stars[intenLev] then
    gUI.Blend.MainEquipBox:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
  else
    gUI.Blend.MainEquipBox:SetItemStarState(0, 0)
  end
  local bindinfo = Lua_Bag_GetBindInfo(bagType, itemSlot)
  gUI.Blend.MainEquipBox:SetItemBindState(0, bindinfo)
end
function _Blend_AddMainEquip(slot)
  Blend_UpdateSuccAndMoney()
  Blend_UpdateSucc()
  Blend_UpdateIcon()
  Blend_UpdateShowCommonItems()
  local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.backpack0, slot, 0)
  if gUI.Blend.QuiltyInterface:IsVisible() then
    gUI.Blend.FunctionBg:SetProperty("Left", "p315")
  else
    gUI.Blend.FunctionBg:SetProperty("Left", "p110")
  end
  Blend_UpdateStones()
  Blend_UpdateReslut(slot)
end
function _Blend_AddSubEquip(slot)
  local name, coef, kind, icon, quality = GetBlendEquipInfo(slot)
  if name == nil then
    return
  end
  Blend_UpdateShowCommonItems()
  gUI.Blend.SideEquipBox:SetItemGoods(0, icon, quality)
  local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.backpack0, slot)
  if 0 < ForgeLevel_To_Stars[intenLev] then
    gUI.Blend.SideEquipBox:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
  else
    gUI.Blend.SideEquipBox:SetItemStarState(0, 0)
  end
  local bindinfo = Lua_Bag_GetBindInfo(gUI_GOODSBOX_BAG.backpack0, slot)
  gUI.Blend.SideEquipBox:SetItemBindState(0, bindinfo)
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if not mbag then
    _Blend_DelMainEquip(-1)
    return
  end
  _Blend_ShowResult(mslot, slot)
  local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.backpack0, slot, 0)
  Blend_Reset_Sub(guid)
end
function Blend_UpdateReslut(slot)
  if slot == -1 then
    return
  end
  local mainAttr = {}
  mainAttr = GetBlendAttr(slot, nil, nil)
  local listBox = gUI.Blend.BlendEquipInfo
  listBox:RemoveAllItems()
  if mainAttr[0] == nil then
    return
  end
  for i = 0, table.maxn(mainAttr) do
    local itemId = GetTableIdBySlot(gUI_GOODSBOX_BAG.backpack0, slot)
    local colorMain = Lua_ToolTip_GetBlendColor(itemId, mainAttr[i].typeId, mainAttr[i].value)
    if Lua_Tip_NeedShowRate(mainAttr[i].typeId) then
      local strBlend = tostring(mainAttr[i].name) .. "|{#C^" .. colorMain .. "^+" .. string.format("%.2f", Lua_Tip_PostProcessVal(mainAttr[i].typeId, mainAttr[i].value)) .. "%}"
      listBox:InsertString(strBlend, -1)
    else
      local strBlend = tostring(mainAttr[i].name) .. "|{#C^" .. colorMain .. "^+" .. tostring(mainAttr[i].value) .. "}"
      listBox:InsertString(strBlend, -1)
    end
  end
end
function _Blend_ShowResult(mainEquipSlot, sideEquipSlot)
  if mainEquipSlot == -1 and sideEquipSlot == -1 then
    return
  end
  local mainAttr = {}
  local sideAttr = {}
  mainAttr = GetBlendAttr(mainEquipSlot, nil, nil)
  sideAttr = GetBlendAttr(sideEquipSlot, nil, nil)
  if not mainAttr then
    _Blend_DelMainEquip(-1)
    return
  end
  CanFlag = false
  if mainAttr[0] ~= nil then
    local blendAttr = {}
    local listBox = gUI.Blend.BlendEquipInfo
    listBox:RemoveAllItems()
    local colorList = {}
    for i = 0, table.maxn(mainAttr) do
      blendAttr[i] = {}
      blendAttr[i].name = mainAttr[i].name
      blendAttr[i].value = mainAttr[i].value
      blendAttr[i].typeId = mainAttr[i].typeId
      colorList[i] = "FAFAFA"
      if sideAttr[0] ~= nil then
        for j = 0, table.maxn(sideAttr) do
          if mainAttr[i].typeId == sideAttr[j].typeId and mainAttr[i].value < sideAttr[j].value then
            blendAttr[i].value = sideAttr[j].value
            blendAttr[i].Old = mainAttr[i].value
            CanFlag = true
            colorList[i] = "FF00FF00"
          end
        end
      end
    end
    if blendAttr[0] == nil then
      _Blend_DelMainEquip(-1)
      return
    end
    for i = 0, table.maxn(blendAttr) do
      local itemId = GetTableIdBySlot(gUI_GOODSBOX_BAG.backpack0, mainEquipSlot)
      local colorMain
      if blendAttr[i].Old ~= nil then
        colorMain = Lua_ToolTip_GetBlendColor(itemId, blendAttr[i].typeId, blendAttr[i].Old)
      else
        colorMain = Lua_ToolTip_GetBlendColor(itemId, blendAttr[i].typeId, blendAttr[i].value)
      end
      local colorSub = Lua_ToolTip_GetBlendColor(itemId, blendAttr[i].typeId, blendAttr[i].value)
      if Lua_Tip_NeedShowRate(blendAttr[i].typeId) then
        if colorList[i] == "FF00FF00" then
          local strBlend = tostring(blendAttr[i].name) .. "|{#C^" .. colorMain .. "^+" .. string.format("%.2f", Lua_Tip_PostProcessVal(blendAttr[i].typeId, blendAttr[i].Old)) .. "%}|{#C^" .. colorSub .. "^+" .. string.format("%.2f", Lua_Tip_PostProcessVal(blendAttr[i].typeId, blendAttr[i].value)) .. "%" .. "}|{ImAgE^UiIamge019:Image_EquipArrows}"
          listBox:InsertString(strBlend, -1)
        else
          local strBlend = tostring(blendAttr[i].name) .. "|{#C^" .. colorMain .. "^+" .. string.format("%.2f", Lua_Tip_PostProcessVal(blendAttr[i].typeId, blendAttr[i].value)) .. "%}|{#C^" .. colorSub .. "^+" .. string.format("%.2f", Lua_Tip_PostProcessVal(blendAttr[i].typeId, blendAttr[i].value)) .. "%" .. "}"
          listBox:InsertString(strBlend, -1)
        end
      elseif colorList[i] == "FF00FF00" then
        local diff = blendAttr[i].value - blendAttr[i].Old
        local strBlend = tostring(blendAttr[i].name) .. "|{#C^" .. colorMain .. "^+" .. tostring(blendAttr[i].Old) .. "}|{#C^" .. colorSub .. "^+" .. tostring(blendAttr[i].value) .. "}|{ImAgE^UiIamge019:Image_EquipArrows}"
        listBox:InsertString(strBlend, -1)
      else
        local strBlend = tostring(blendAttr[i].name) .. "|{#C^" .. colorMain .. "^+" .. tostring(blendAttr[i].value) .. "}|{#C^" .. colorSub .. "^+" .. tostring(blendAttr[i].value) .. "}"
        listBox:InsertString(strBlend, -1)
      end
    end
  end
  local name, coef, kind, icon, quality = GetBlendEquipInfo(mainEquipSlot)
  if name == nil then
    return
  end
  gUI.Blend.MainEquipBox:SetItemGoods(0, icon, quality)
  local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.backpack0, mainEquipSlot)
  if 0 < ForgeLevel_To_Stars[intenLev] then
    gUI.Blend.MainEquipBox:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
  else
    gUI.Blend.MainEquipBox:SetItemStarState(0, 0)
  end
  local bindinfo = Lua_Bag_GetBindInfo(gUI_GOODSBOX_BAG.backpack0, mainEquipSlot)
  gUI.Blend.MainEquipBox:SetItemBindState(0, bindinfo)
end
function _Blend_DelMainEquip(slot)
  if slot ~= nil and slot ~= -1 then
    local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.backpack0, slot, 0)
  end
  gUI.Blend.MainEquipBox:ClearGoodsItem(0)
  gUI.Blend.SideEquipBox:ClearGoodsItem(0)
  gUI.Blend.MainEquipBox:SetItemBindState(0, 0)
  gUI.Blend.SideEquipBox:SetItemBindState(0, 0)
  Blend_UpdateStones()
  gUI.Blend.BlendEquipInfo:RemoveAllItems()
  gUI.Blend.SuccLab:SetProperty("Text", "0")
  gUI.Blend.CostLab:SetProperty("Text", "0")
  gUI.Blend.ItemCantainer:DeleteAll()
end
function Blend_UpdateStones()
  local bagType, bagSlot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if bagSlot then
    local bindMoney, nonBindMoney, stoneSlot, bindStone, nonBindStone, needStoneCount, totalPropInBag, icon, name, _, nonBindCount, BindCount = GetBlendConsume(bagSlot)
    local _, _, quality = GetItemInfoBySlot(-1, bindStone, nil)
    gUI.Blend.MaterialBox:SetItemGoods(0, icon, quality)
    local strItemCount = ""
    local HasCount = 0
    local ItemId = 0
    if gUI.Blend.MaterialFrist:IsChecked() then
      gUI.Blend.MaterialBox:SetItemBindState(0, 0)
      strItemCount = tostring(nonBindCount) .. "/" .. tostring(needStoneCount)
      HasCount = nonBindCount
      ItemId = nonBindStone
    else
      strItemCount = tostring(totalPropInBag) .. "/" .. tostring(needStoneCount)
      HasCount = totalPropInBag
      if BindCount > 0 then
        gUI.Blend.MaterialBox:SetItemBindState(0, 1)
        ItemId = bindStone
      else
        gUI.Blend.MaterialBox:SetItemBindState(0, 0)
        ItemId = nonBindStone
      end
    end
    if needStoneCount <= HasCount then
      gUI.Blend.MaterialBox:SetIconState(0, 7, false)
    else
      gUI.Blend.MaterialBox:SetIconState(0, 7, true)
    end
    gUI.Blend.MaterialBox:SetItemSubscript(0, tostring(strItemCount))
    gUI.Blend.MaterialBox:SetCustomUserData(0, ItemId)
  else
    gUI.Blend.MaterialBox:ClearGoodsItem(0)
    gUI.Blend.MaterialBox:SetItemBindState(0, 0)
  end
end
function UI_Blend_ShowItemTooltips(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  if win:IsItemHasIcon(0) then
    local itemId = win:GetCustomUserData(0)
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemId)
  end
end
function _Blend_DelSubEquip()
  gUI.Blend.SideEquipBox:ClearGoodsItem(0)
  gUI.Blend.SideEquipBox:SetItemBindState(0, 0)
  Blend_Reset_Sub()
  local bagType, bagSlot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if bagSlot then
    Blend_UpdateReslut(bagSlot)
  end
  Blend_UpdateShowCommonItems()
end
function _Blend_DelMaterial()
  Blend_UpdateStones()
end
function _OnBlend_AddItem(slot, itemType)
  if itemType == BLEND_SLOT_COLLATE.MAINEQUIP then
    _Blend_AddMainEquip(slot)
  elseif itemType == BLEND_SLOT_COLLATE.SUBEQUIP then
    _Blend_AddSubEquip(slot)
  elseif itemType == BLEND_SLOT_COLLATE.MATERIAL then
  end
end
function _OnBlend_DelItem(slot, itemType)
  if itemType == BLEND_SLOT_COLLATE.MAINEQUIP then
    _Blend_DelMainEquip(slot)
  elseif itemType == BLEND_SLOT_COLLATE.SUBEQUIP then
    _Blend_DelSubEquip()
  elseif itemType == BLEND_SLOT_COLLATE.MATERIAL then
    _Blend_DelMaterial()
  end
end
function UI_ShowCommonItem_Tip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  if win:IsItemHasIcon(0) then
    local Guid = win:GetUserData64()
    local bag, slot = GetItemPosByGuid(Guid)
    if slot ~= -1 then
      if bag ~= gUI_GOODSBOX_BAG.myequipbox then
        Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
        Lua_Tip_ShowEquipCompare(win, bag, slot)
      else
        Lua_Tip_Item(tooltip, slot, bag, true, nil, nil)
      end
    end
  end
end
function UI_Blend_ShowEquipTooltips(msg)
  local win = msg:get_window()
  local boxName = win:GetProperty("WindowName")
  local boxParName = win:GetParent():GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  if WindowToGoodsBox(win):IsItemHasIcon(0) then
    if boxParName == "P1_pg" then
      local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
      Lua_Tip_Item(tooltip, mslot, gUI_GOODSBOX_BAG.backpack0, nil, 0, nil)
      Lua_Tip_ShowEquipCompare(win, gUI_GOODSBOX_BAG.backpack0, mslot)
    elseif boxParName == "P2_pg" then
      local sbag, sslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.SUBEQUIP)
      Lua_Tip_Item(tooltip, sslot, gUI_GOODSBOX_BAG.backpack0, nil, 0, nil)
      Lua_Tip_ShowEquipCompare(win, gUI_GOODSBOX_BAG.backpack0, sslot)
    end
  end
end
function UI_ContainShowTipBlend(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local offset = msg:get_wparam()
  local tooltip = wnd:GetToolTipWnd(0)
  Lua_Tip_Item(tooltip, _CHARA_EQUIPPOINT_CANTAINBLEND1[offset][1], gUI_GOODSBOX_BAG.myequipbox, true, nil, nil)
end
function UI_Blend_MainEquipRClick(msg)
  local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
  if not mslot then
    return
  end
  Lua_Blend_ResetItemByGuid()
  SetBlendEquip(0, OperateMode.Cancel, BLEND_SLOT_COLLATE.MAINEQUIP)
end
function UI_Blend_SideEquipRClick(msg)
  local sbag, sslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.SUBEQUIP)
  if not sslot then
    return
  end
  SetBlendEquip(0, OperateMode.Remove, BLEND_SLOT_COLLATE.SUBEQUIP)
end
function UI_Blend_MaterialRClick(msg)
  SetBlendEquip(0, OperateMode.Remove, BLEND_SLOT_COLLATE.MATERIAL)
end
function _OnBlend_BagItemChange()
  local wnd = WindowSys_Instance:GetWindow("Blend")
  if not wnd:IsVisible() then
    return
  end
  Blend_UpdateShowCommonItems()
  Blend_UpdateStones()
end
function _OnBlend_GetResult(arg1)
  Blend_UpdateShowCommonItems()
  if arg1 == 1 then
    Lua_Chat_ShowSysLog("BLEND_SUCC")
    Lua_Chat_ShowOSD("BLEND_SUCC")
    local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
    if not mbag then
      return
    end
    local mainEquipSlot = mslot
    local sbag, sslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.SUBEQUIP)
    if not sslot then
      _Blend_DelSubEquip()
    end
    local bagType, itemSlot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
    local bindinfo = Lua_Bag_GetBindInfo(bagType, itemSlot)
    gUI.Blend.MainEquipBox:SetItemBindState(0, bindinfo)
  else
    if BLENDSUBEQUIP ~= -1 then
      gUI.Blend.SideEquipBox:ClearGoodsItem(0)
      gUI.Blend.SideEquipBox:SetItemBindState(0, 0)
      SetBlendEquip(BLENDSUBEQUIP, OperateMode.Add, BLEND_SLOT_COLLATE.SUBEQUIP)
    end
    Lua_Chat_ShowSysLog("BLEND_FAILED")
  end
end
function UI_Blend_Show_FunctionBg(msg)
  if gUI.Blend.FunctionBg:IsVisible() then
    gUI.Blend.FunctionBg:SetVisible(false)
  else
    if gUI.Blend.QuiltyInterface:IsVisible() then
      gUI.Blend.FunctionBg:SetProperty("Left", "p315")
    else
      gUI.Blend.FunctionBg:SetProperty("Left", "p110")
    end
    gUI.Blend.FunctionBg:SetVisible(true)
  end
end
function Script_Blend_OnEvent(event)
  if "BLEND_SHOW_WINDOW" == event then
    _OnBlend_OnPanelOpen()
  elseif "BLEND_ADD_ITEM" == event then
    _OnBlend_AddItem(arg1, arg2)
  elseif "BLEND_DEL_ITEM" == event then
    _OnBlend_DelItem(arg1, arg2)
  elseif "BLEND_SHOW_RESULT" == event then
    _OnBlend_GetResult(arg1)
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    _OnBlend_CloseWindw()
  elseif "ITEM_ADD_TO_BAG" == event then
    _OnBlend_BagItemChange()
  elseif "ITEM_DEL_FROM_BAG" == event then
    _OnBlend_BagItemChange()
  elseif event == "ITEM_EQUIP_UPDATE" then
    _OnBlend_ChangeEquip()
  elseif event == "PLAYER_MONEY_CHANGED" then
    Blend_UpdateSuccAndMoney(arg1, arg2, event)
  elseif event == "PLAYER_BIND_MONEY_CHANGED" then
    Blend_UpdateSuccAndMoney(arg1, arg2, event)
  end
end
function Script_Blend_OnLoad()
  gUI.Blend.MainEquipBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Blend.P1_pg.Arm_gbox"))
  gUI.Blend.SideEquipBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Blend.P2_pg.Arm_gbox"))
  gUI.Blend.MaterialBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Blend.P4_pg.ForgeStone_gbox"))
  gUI.Blend.BlendEquipInfo = WindowToListBox(WindowSys_Instance:GetWindow("Blend.P3_pg.Info_lbox"))
  gUI.Blend.BuyBtn = WindowToButton(WindowSys_Instance:GetWindow("Blend.P4_pg.BuyButton_btn"))
  gUI.Blend.SuccLab = WindowToLabel(WindowSys_Instance:GetWindow("Blend.ExpendMoney_slab.Arm_dlab"))
  gUI.Blend.MyEquipBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Blend.Arrows_bg.Equip_bg.Equip_gbox"))
  gUI.Blend.CostLab = WindowToLabel(WindowSys_Instance:GetWindow("Blend.ExpendMoney_slab.MoneyAuBind_dlab"))
  gUI.Blend.QulityCheckBtn = {}
  gUI.Blend.QulityCheckBtn[0] = WindowToCheckButton(WindowSys_Instance:GetWindow("Blend.Arrow_bg.Property_bg.CheckButton1_cbtn"))
  gUI.Blend.QulityCheckBtn[1] = WindowToCheckButton(WindowSys_Instance:GetWindow("Blend.Arrow_bg.Property_bg.CheckButton2_cbtn"))
  gUI.Blend.QulityCheckBtn[2] = WindowToCheckButton(WindowSys_Instance:GetWindow("Blend.Arrow_bg.Property_bg.CheckButton3_cbtn"))
  gUI.Blend.ItemCantainer = WindowToContainer(WindowSys_Instance:GetWindow("Blend.Arrow_bg.Property_bg.Container1_cnt"))
  gUI.Blend.QuiltyInterface = WindowSys_Instance:GetWindow("Blend.Arrow_bg")
  gUI.Blend.SetBtnEquip = {}
  for i = 1, 10 do
    gUI.Blend.SetBtnEquip[i - 1] = WindowToButton(WindowSys_Instance:GetWindow("Blend.Arrows_bg.Equip_bg.But" .. tostring(i) .. "_btn"))
  end
  gUI.Blend.FunctionBg = WindowSys_Instance:GetWindow("Blend.Function_bg.Function_bg")
  gUI.Blend.MoneyFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("Blend.ExpendMoney_slab.IsBind_cbtn"))
  gUI.Blend.MoneyFrist:SetChecked(false)
  gUI.Blend.MaterialFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("Blend.ExpendMoney_slab.Mould_cbtn"))
  gUI.Blend.MaterialFrist:SetChecked(false)
end
function Script_Blend_OnHide()
  SetBlendEquip(0, OperateMode.Cancel, BLEND_SLOT_COLLATE.MAINEQUIP)
  Lua_Blend_ResetItemByGuid()
end

  gUI.Blend.MoneyFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("Blend.ExpendMoney_slab.IsBind_cbtn"))
  gUI.Blend.MoneyFrist:SetChecked(false)
  gUI.Blend.MaterialFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("Blend.ExpendMoney_slab.Mould_cbtn"))
  gUI.Blend.MaterialFrist:SetChecked(false)
end
function Script_Blend_OnHide()
  SetBlendEquip(0, OperateMode.Cancel, BLEND_SLOT_COLLATE.MAINEQUIP)
  Lua_Blend_ResetItemByGuid()
end
