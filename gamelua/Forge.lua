if gUI and not gUI.Forge then
  gUI.Forge = {}
end
FORGE_SLOT_COLLATE = {
  EQUIPSLOT = 0,
  LUCKYSLOT1 = 1,
  LUCKYSLOT2 = 2,
  LUCKYSLOT3 = 3,
  LUCKYSLOT4 = 4
}
FORGE_SLOT_TYPE = {
  FORGE_UNKOWNTYPE = 1,
  FORGE_ISLUCKYSTONE = 2,
  FORGE_SAFEYSTONE = 3,
  FORGE_ERRORLUCKY = 4,
  FORGE_ERRORSAFEY = 5
}
local _CHARA_EQUIPPOINT_CANTAINFORGE = {
  [0] = {0, "武器"},
  [9] = {1, "头盔"},
  [1] = {2, "衣服"},
  [4] = {3, "肩饰"},
  [2] = {4, "手套"},
  [5] = {5, "鞋子"},
  [8] = {6, "耳环"},
  [6] = {7, "项链"},
  [3] = {8, "手镯"},
  [7] = {9, "戒指"},
  [10] = {10, "时装"}
}
local _CHARA_EQUIPPOINT_CANTAINFORGE1 = {
  [0] = {0, "武器"},
  [1] = {9, "头盔"},
  [2] = {1, "衣服"},
  [3] = {4, "肩饰"},
  [4] = {2, "手套"},
  [5] = {5, "鞋子"},
  [6] = {8, "耳环"},
  [7] = {6, "项链"},
  [8] = {3, "手镯"},
  [9] = {7, "戒指"},
  [10] = {10, "时装"}
}
local ItemGUID = 0
local ItemGUIDLAST = 0
local ContainPos = -1
local BagSlotOffset = -1
local MATERIALSLOTRECORD = -1
local MATERIALBAGRECORD = -1
GUIDER_FORGE_ITEMID = 300072
GUIDER_FORGE_ITEMIDUNBIND = 300073
function UI_Forge_OpenPanel(msg)
  local wnd = WindowSys_Instance:GetWindow("Forge")
  if wnd:IsVisible() then
    _OnForge_CloseWindw()
  else
    _OnForge_ShowRootWin()
  end
end
function Lua_Forge_IsEquip(equipSlot, bagType)
  local _, _, _, _, _, _, _, _, _, _, _, _, _, itemType = GetItemInfoBySlot(bagType, equipSlot, 0)
  if itemType == 2 then
    return true
  end
  return false
end
function Lua_Forge_CheckMount(equipSlot, bagType)
  local canForge, isBelowMax = GetIsMountCanIntensify(equipSlot, bagType)
  if not canForge then
    Lua_Chat_ShowOSD("FORGE_MOUNT_DISABLE")
    return false
  elseif not isBelowMax then
    Lua_Chat_ShowOSD("FROGE_MAX_LEVEL")
    return false
  end
  return true
end
function Forge_UpdateEquipInfos()
  local EquipContainInfos = GetEquipContain()
  if EquipContainInfos[0] ~= nil then
    for n = 0, table.maxn(EquipContainInfos) do
      if EquipContainInfos[n].Icon ~= "" then
        gUI.Forge.EContainGBox:SetItemGoods(_CHARA_EQUIPPOINT_CANTAINFORGE[n][1], EquipContainInfos[n].Icon, EquipContainInfos[n].quality)
        local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.myequipbox, n)
        if 0 < ForgeLevel_To_Stars[intenLev] then
          gUI.Forge.EContainGBox:SetItemStarState(_CHARA_EQUIPPOINT_CANTAINFORGE[n][1], ForgeLevel_To_Stars[intenLev])
        else
          gUI.Forge.EContainGBox:SetItemStarState(_CHARA_EQUIPPOINT_CANTAINFORGE[n][1], 0)
        end
        gUI.Forge.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINFORGE[n][1]]:SetProperty("Enable", "true")
        gUI.Forge.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINFORGE[n][1]]:SetProperty("Text", "放入")
      else
        gUI.Forge.EContainGBox:ClearGoodsItem(_CHARA_EQUIPPOINT_CANTAINFORGE[n][1])
        gUI.Forge.EContainGBox:SetItemBindState(_CHARA_EQUIPPOINT_CANTAINFORGE[n][1], 0)
        if n == ContainPos then
          gUI.Forge.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINFORGE[n][1]]:SetProperty("Enable", "true")
          gUI.Forge.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINFORGE[n][1]]:SetProperty("Text", "{#R^收回}")
        else
          gUI.Forge.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINFORGE[n][1]]:SetProperty("Enable", "false")
          gUI.Forge.SetBtnEquip[_CHARA_EQUIPPOINT_CANTAINFORGE[n][1]]:SetProperty("Text", "放入")
        end
      end
    end
  end
end
function Lua_Forge_CheckEquip(equipSlot, bagType)
  local canForge, name, level, maxXiouweiLevel, maxLevel = GetForgeEquipInfo(equipSlot, bagType)
  if canForge == nil then
    Lua_Chat_ShowOSD("FORGE_EQUIPMENT_ERONG")
    return true
  else
    if maxLevel == 0 then
      Lua_Chat_ShowOSD("FROGE_CANNOT")
      return false
    end
    if canForge == false then
      Lua_Chat_ShowOSD("FROGE_MAX_LEVEL")
      return false
    end
  end
  return true
end
function _Forge_AddEquip(bagType, bagSlot)
  Forge_ResetEquip(bagType, bagSlot)
  _Forge_UpdateEquipInfo()
  _Forge_UpdateBounce()
  _Forge_UpdateSuccessRate()
  _Forge_UpdateIcon(bagSlot, bagType)
  _Forge_UpdateStones()
end
function _Forge_UpdateStones()
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  if bagSlot then
    local needPropCount, totalPropInBag, propBagslot, propName, propIcon, bindStoneId, nonBindStoneId, propDec, quality, bindCount, nonBindCount = GetForgePropStone()
    local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev, nextlevel, nextclrpoint = GetForgeEquipInfo(bagSlot)
    local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, _, equipKind, offlineTime, isTimeLimit, EquipPoint, SkillId, RuneLv, _, nKind, MaxInBag = GetItemInfoBySlot(bagType, bagSlot, -1)
    local icon, itemCount1 = GetBaseInfoByItemId(GUIDER_FORGE_ITEMID)
    local icon1, itemCount2 = GetBaseInfoByItemId(GUIDER_FORGE_ITEMIDUNBIND)
    if totalPropInBag < 0 then
      totalPropInBag = 0
    end
    local _, _, quality = GetItemInfoBySlot(-1, bindStoneId, nil)
    if curClrLev < 3 and minLevel < 31 and equipKind == 1 then
      if gUI.Forge.MaterialFrist:IsChecked() then
        gUI.Forge.MaterGBox:SetItemGoods(0, propIcon, quality)
        if nonBindCount < needPropCount then
          gUI.Forge.MaterGBox:SetIconState(0, 7, true)
          gUI.Forge.MaterLab:SetProperty("FontColor", colorRed)
        else
          gUI.Forge.MaterGBox:SetIconState(0, 7, false)
          gUI.Forge.MaterLab:SetProperty("FontColor", colorWhite)
        end
        gUI.Forge.MaterGBox:SetCustomUserData(0, nonBindStoneId)
        gUI.Forge.MaterGBox:SetItemBindState(0, 0)
        local itemCount = tostring(nonBindCount) .. "/" .. tostring(needPropCount)
        gUI.Forge.MaterLab:SetProperty("Text", tostring(itemCount))
      else
        local itemCount = ""
        if itemCount1 > 0 then
          itemCount = tostring(itemCount1) .. "/" .. tostring(needPropCount)
          gUI.Forge.MaterGBox:SetItemGoods(0, icon1, quality)
          gUI.Forge.MaterGBox:SetIconState(0, 7, false)
          gUI.Forge.MaterLab:SetProperty("FontColor", colorWhite)
          gUI.Forge.MaterGBox:SetCustomUserData(0, GUIDER_FORGE_ITEMID)
          gUI.Forge.MaterGBox:SetItemBindState(0, 1)
        elseif needPropCount <= totalPropInBag then
          itemCount = tostring(totalPropInBag) .. "/" .. tostring(needPropCount)
          gUI.Forge.MaterGBox:SetItemGoods(0, propIcon, quality)
          gUI.Forge.MaterGBox:SetIconState(0, 7, false)
          gUI.Forge.MaterLab:SetProperty("FontColor", colorWhite)
          if bindCount > 0 then
            gUI.Forge.MaterGBox:SetItemBindState(0, 1)
            gUI.Forge.MaterGBox:SetCustomUserData(0, bindStoneId)
          else
            gUI.Forge.MaterGBox:SetItemBindState(0, 0)
            gUI.Forge.MaterGBox:SetCustomUserData(0, nonBindStoneId)
          end
        elseif needPropCount > totalPropInBag then
          itemCount = tostring(totalPropInBag) .. "/" .. tostring(needPropCount)
          gUI.Forge.MaterGBox:SetItemGoods(0, propIcon, quality)
          gUI.Forge.MaterGBox:SetIconState(0, 7, true)
          gUI.Forge.MaterLab:SetProperty("FontColor", colorRed)
          if bindCount > 0 then
            gUI.Forge.MaterGBox:SetItemBindState(0, 1)
            gUI.Forge.MaterGBox:SetCustomUserData(0, bindStoneId)
          else
            gUI.Forge.MaterGBox:SetItemBindState(0, 0)
            gUI.Forge.MaterGBox:SetCustomUserData(0, nonBindStoneId)
          end
        end
        gUI.Forge.MaterLab:SetProperty("Text", tostring(itemCount))
      end
    else
      gUI.Forge.MaterGBox:SetItemGoods(0, propIcon, quality)
      if gUI.Forge.MaterialFrist:IsChecked() then
        if nonBindCount < needPropCount then
          gUI.Forge.MaterGBox:SetIconState(0, 7, true)
          gUI.Forge.MaterLab:SetProperty("FontColor", colorRed)
        else
          gUI.Forge.MaterGBox:SetIconState(0, 7, false)
          gUI.Forge.MaterLab:SetProperty("FontColor", colorWhite)
        end
        gUI.Forge.MaterGBox:SetCustomUserData(0, nonBindStoneId)
        gUI.Forge.MaterGBox:SetItemBindState(0, 0)
        local itemCount = tostring(nonBindCount) .. "/" .. tostring(needPropCount)
        gUI.Forge.MaterLab:SetProperty("Text", tostring(itemCount))
      else
        if needPropCount <= totalPropInBag then
          gUI.Forge.MaterLab:SetProperty("FontColor", colorWhite)
          gUI.Forge.MaterGBox:SetIconState(0, 7, false)
        else
          gUI.Forge.MaterLab:SetProperty("FontColor", colorRed)
          gUI.Forge.MaterGBox:SetIconState(0, 7, true)
        end
        if bindCount > 0 then
          gUI.Forge.MaterGBox:SetCustomUserData(0, bindStoneId)
          gUI.Forge.MaterGBox:SetItemBindState(0, 1)
        else
          gUI.Forge.MaterGBox:SetCustomUserData(0, nonBindStoneId)
          gUI.Forge.MaterGBox:SetItemBindState(0, 0)
        end
        local itemCount = tostring(totalPropInBag) .. "/" .. tostring(needPropCount)
        gUI.Forge.MaterLab:SetProperty("Text", tostring(itemCount))
      end
    end
  else
    gUI.Forge.MaterGBox:ClearGoodsItem(0)
    gUI.Forge.MaterGBox:SetItemBindState(0, 0)
  end
end
function _Forge_UpdateIcon(bagSlot, bagType)
  _Forge_ShowIconBySlot(bagSlot, bagType, gUI.Forge.EquipGBox)
end
function Forge_ResetEquip(bagType, bagSlot)
  if bagType == -1 then
    local bagOld, slotOld = GetItemPosByGuid(ItemGUID)
    if bagOld == 1 and slotOld ~= -1 then
      MoveItem(gUI_GOODSBOX_BAG.backpack0, slotOld, gUI_GOODSBOX_BAG.myequipbox, ContainPos)
      ItemGUID = 0
      ContainPos = -1
      return
    end
  end
  local NowGuid = GetItemGUIDByPos(bagType, bagSlot)
  if NowGuid == ItemGUID then
    return
  end
  local bagOld, slotOld = GetItemPosByGuid(ItemGUID)
  if bagOld == 1 and slotOld ~= -1 then
    MoveItem(gUI_GOODSBOX_BAG.backpack0, slotOld, gUI_GOODSBOX_BAG.myequipbox, ContainPos)
    ItemGUID = 0
    ContainPos = -1
  end
end
function _Forge_UpdateEquipInfo()
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  if bagType then
    local _, icon, quality, _, _, _, _, _, _, _, _, itemCount = GetItemInfoBySlot(bagType, bagSlot, 0)
    if not Lua_Forge_IsEquip(bagSlot, bagType) then
      gUI.Forge.LevDes:SetProperty("Text", "最大锻造等级:")
      gUI.Forge.MaxLab:SetProperty("Text", "10")
      local _, _, _, name = GetItemBaseInfoBySlot(bagType, bagSlot)
      gUI.Forge.EquipNameLab:SetProperty("Text", tostring(name))
      gUI.Forge.EquipNameLab:SetProperty("FontColor", gUI_GOODS_QUALITY_COLOR_PREFIX[quality])
    else
      local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev = GetForgeEquipInfo(bagSlot)
      gUI.Forge.LevDes:SetProperty("Text", "最大锻造等级:")
      gUI.Forge.MaxLab:SetProperty("Text", tostring(maxLevel))
      if level > 0 then
        name = name .. "+" .. tostring(level)
      end
      gUI.Forge.EquipNameLab:SetProperty("Text", tostring(name))
      gUI.Forge.EquipNameLab:SetProperty("FontColor", gUI_GOODS_QUALITY_COLOR_PREFIX[quality])
    end
  end
end
function _Forge_UpdateBounce()
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  if bagType then
    gUI.Forge.DesLBox:RemoveAllItems()
    if not Lua_Forge_IsEquip(bagSlot, bagType) then
    else
      local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev, nextlevel, nextclrpoint = GetForgeEquipInfo(bagSlot)
      local strForgeLev, strClrLev
      if level == maxLevel then
        strForgeLev = "历史锻造等级:|" .. tostring(level) .. "|{#G^" .. tostring(level) .. "}"
        strClrLev = "当前锻造等级:|" .. tostring(curClrLev) .. "|{#G^" .. tostring(curClrLev) .. "}"
      else
        strClrLev = "当前锻造等级:|" .. tostring(curClrLev) .. "|{#G^" .. tostring(curClrLev + 1) .. "}"
      end
      if level == curClrLev then
        strForgeLev = "历史锻造等级:|" .. tostring(level) .. "|{#G^" .. tostring(level + 1) .. "}"
      else
        strForgeLev = "历史锻造等级:|" .. tostring(level) .. "|{#G^" .. tostring(level) .. "}"
      end
      gUI.Forge.DesLBox:InsertString(strForgeLev, -1)
      gUI.Forge.DesLBox:InsertString(strClrLev, -1)
      gUI.Forge.DesLBox:InsertString(" | | ", -1)
      local _, _, level, _, _, _, clrPoint, nextlevel, nextclrpoint = GetForgeEquipInfo(bagSlot)
      local rateList = GetForgeRateBounce(bagSlot, 0)
      local rateListN = GetForgeRateBounce(bagSlot, 0)
      if level == curClrLev then
        rateListN = GetForgeRateBounce(bagSlot, 1)
      end
      local fixList = GetForgeFixBounce(bagSlot, 0)
      local fixListN = GetForgeFixBounce(bagSlot, 0)
      if level == curClrLev then
        fixListN = GetForgeFixBounce(bagSlot, 1)
      end
      local rateListFlag = {
        [0] = false,
        [1] = false,
        [2] = false,
        [3] = false,
        [4] = false,
        [5] = false,
        [6] = false,
        [7] = false
      }
      local minAtkCur = -1
      local maxAtkCur = -1
      local minAtkNext = -1
      local maxAtkNext = -1
      local currentForgeLevel = level
      local nextForgeLevel = nextlevel
      local currentClPoint = curClrLev
      local nextClPoint = nextclrpoint
      for i = 0, table.maxn(rateList) do
        if rateList[i].attrName == "攻击最小值" then
          minAtkCur = rateList[i].value
          minAtkNext = rateListN[i].value
        elseif rateList[i].attrName == "攻击最大值" then
          maxAtkCur = rateList[i].value
          maxAtkNext = rateListN[i].value
        else
          rateListFlag[i] = true
        end
      end
      if minAtkCur ~= -1 and maxAtkCur ~= 0 and maxAtkNext ~= 0 then
        local strAtr = string.format("攻击:|%d-%d |{#G^%d-%d}", minAtkCur, maxAtkCur, minAtkNext, maxAtkNext)
        gUI.Forge.DesLBox:InsertString(strAtr, -1)
      end
      for i = 0, table.maxn(rateList) do
        if rateListFlag[i] == true and rateList[i].typeId ~= -1 then
          local strAttr = ""
          if Lua_Tip_NeedShowRate(rateList[i].typeId) then
            strAttr = "%s:|+%d%% |{#G^+%d%%(%d%%%s)}"
          else
            strAttr = "%s:|%d |{#G^%d(%d%s)}"
          end
          local strJianTou = ""
          if 0 < Lua_Tip_PostProcessVal(rateList[i].typeId, rateListN[i].value) - Lua_Tip_PostProcessVal(rateList[i].typeId, rateList[i].value) then
            strJianTou = "↑"
          end
          strAttr = string.format(strAttr, rateList[i].attrName, Lua_Tip_PostProcessVal(rateList[i].typeId, rateList[i].value), Lua_Tip_PostProcessVal(rateList[i].typeId, rateListN[i].value), Lua_Tip_PostProcessVal(rateList[i].typeId, rateListN[i].value) - Lua_Tip_PostProcessVal(rateList[i].typeId, rateList[i].value), strJianTou)
          gUI.Forge.DesLBox:InsertString(strAttr, -1)
        end
      end
      gUI.Forge.DesLBox:InsertString(" | | ", -1)
      for i = 0, table.maxn(fixList) do
        if 1 > fixListN[i].value then
          break
        end
        local strAttr = ""
        if 1 > fixList[i].value then
          if Lua_Tip_NeedShowRate(fixListN[i].typeId) then
          else
          end
        else
          if Lua_Tip_NeedShowRate(fixList[i].typeId) then
            strAttr = "%s: |+%d%% |{#G^+%d%%(%d%%%s)}"
          else
            strAttr = "%s: |%d |{#G^%d(%d%s)}"
          end
          local strJianTou = ""
          if 0 < Lua_Tip_PostProcessVal(fixList[i].typeId, fixListN[i].value) - Lua_Tip_PostProcessVal(fixList[i].typeId, fixList[i].value) then
            strJianTou = "↑"
          end
          strAttr = string.format(strAttr, fixList[i].attrName, Lua_Tip_PostProcessVal(fixList[i].typeId, fixList[i].value), Lua_Tip_PostProcessVal(fixList[i].typeId, fixListN[i].value), Lua_Tip_PostProcessVal(fixList[i].typeId, fixListN[i].value) - Lua_Tip_PostProcessVal(fixList[i].typeId, fixList[i].value), strJianTou)
        end
        gUI.Forge.DesLBox:InsertString(strAttr, -1)
      end
    end
  end
end
function _Forge_GetSuccssRate()
  local baseRate, extraRate = GetForgeSuccessRate()
  if not baseRate then
    Lua_Chat_ShowLuaErr("_Forge_GetSuccssRate-baseRate为空")
    return
  end
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev = GetForgeEquipInfo(bagSlot)
  if level ~= curClrLev then
    baseRate = baseRate + 1000
  end
  return baseRate / 100, extraRate / 100
end
function _Forge_UpdateSuccessRate()
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  if bagType then
    _Forge_UpdateMoney()
    local baseRate, extraRate = _Forge_GetSuccssRate()
    if baseRate > 100 then
      baseRate = 100
    end
    if baseRate + extraRate > 100 then
      extraRate = 100 - baseRate
    end
    local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
    local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev = GetForgeEquipInfo(bagSlot)
    if level ~= curClrLev then
      gUI.Forge.RateLab:SetProperty("FontColor", colorYellow)
    else
      gUI.Forge.RateLab:SetProperty("FontColor", colorWhite)
    end
    if extraRate <= 0 then
      gUI.Forge.RateLab:SetProperty("Text", tostring(baseRate) .. "%")
      return
    end
    gUI.Forge.RateLab:SetProperty("Text", tostring(baseRate) .. "%+" .. tostring(extraRate) .. "%")
  end
end
function _Forge_UpdateMoney(newMoney, changeMoney, event)
  local wnd = WindowSys_Instance:GetWindow("Forge")
  if wnd:IsVisible() then
    local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
    if bagType then
      local type1, amount1, type2, amount2 = GetForgeGoldConsume(bagSlot, bagType, 0)
      if gUI.Forge.MoneyFrist:IsChecked() then
        gUI.Forge.CostLab:SetProperty("Text", Lua_UI_Money2String(amount1, true, false, false, true, newMoney, event))
      else
        gUI.Forge.CostLab:SetProperty("Text", Lua_UI_Money2String(amount1, false, false, false, true, newMoney, event))
      end
    end
  end
end
function _Forge_AddLuckyStone(index, itemId)
  _Forge_UpdateSuccessRate()
  _Forge_ShowIconBySlot(itemId, 1, gUI.Forge.LuckyStoneBox[index])
end
function _Forge_ShowIconBySlot(itemSlot, bagType, goodBox)
  local _, icon, quality, _, _, _, _, _, _, _, _, itemCount = GetItemInfoBySlot(bagType, itemSlot, 0)
  if not icon then
    return
  end
  goodBox:SetItemGoods(0, icon, quality)
  local intenLev = Lua_Bag_GetStarInfo(bagType, itemSlot)
  if 0 < ForgeLevel_To_Stars[intenLev] then
    goodBox:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
  else
    goodBox:SetItemStarState(0, 0)
  end
  local bindinfo = Lua_Bag_GetBindInfo(bagType, itemSlot)
  goodBox:SetItemBindState(0, bindinfo)
end
function _Forge_AddMaterial(bagType, bagSlot)
end
function _OnForge_ShowRootWin()
  local wnd = WindowSys_Instance:GetWindow("Forge")
  if wnd:IsVisible() then
    return
  end
  Lua_Bag_ShowUI(true)
  Forge_Show()
  gUI.Forge.FunctionBg:SetVisible(false)
end
function Forge_Show()
  local wnd = WindowSys_Instance:GetWindow("Forge")
  wnd:SetProperty("Visible", "true")
  Forge_InitUI()
end
function Forge_InitUI()
  Forge_ClearUI()
end
function Forge_ClearUI()
  gUI.Forge.EquipGBox:ClearGoodsItem(0)
  gUI.Forge.EquipGBox:SetItemBindState(0, 0)
  gUI.Forge.EquipNameLab:SetProperty("Text", "请放入需要锻造的装备")
  gUI.Forge.EquipNameLab:SetProperty("FontColor", "FFFF0611")
  gUI.Forge.MaxLab:SetProperty("Text", "")
  gUI.Forge.LevDes:SetProperty("Text", "")
  _Forge_UpdateStones()
  gUI.Forge.MaterLab:SetProperty("Text", "")
  gUI.Forge.LuckyStoneBox[0]:ClearGoodsItem(0)
  gUI.Forge.LuckyStoneBox[1]:ClearGoodsItem(0)
  gUI.Forge.LuckyStoneBox[2]:ClearGoodsItem(0)
  gUI.Forge.LuckyStoneBox[3]:ClearGoodsItem(0)
  gUI.Forge.LuckyStoneBox[0]:SetItemBindState(0, 0)
  gUI.Forge.LuckyStoneBox[1]:SetItemBindState(0, 0)
  gUI.Forge.LuckyStoneBox[2]:SetItemBindState(0, 0)
  gUI.Forge.LuckyStoneBox[3]:SetItemBindState(0, 0)
  gUI.Forge.DesLBox:RemoveAllItems()
  gUI.Forge.RateLab:SetProperty("Text", "0")
  gUI.Forge.CostLab:SetProperty("Text", "0")
  Forge_UpdateEquipInfos()
end
function _OnForge_ChangeEquip()
  local wnd = WindowSys_Instance:GetWindow("Forge")
  if wnd:IsVisible() then
    Forge_UpdateEquipInfos()
  end
end
function Forge_CheckEquip(itemId, bagType)
  if Lua_Forge_IsEquip(itemId, bagType) then
    if Lua_Forge_CheckEquip(itemId, bagType) then
      SetEnhanceNeedItem(itemId, OperateMode.Add, FORGE_SLOT_COLLATE.EQUIPSLOT, bagType)
    else
    end
  else
    Lua_Chat_ShowOSD("FORGE_EQUIPMENT_ERONG")
  end
end
function Forge_CheckMaterialNew(slot, PlaceSlot, bagType)
  if bagType ~= gUI_GOODSBOX_BAG.backpack0 then
    return
  end
  if Lua_Forge_CheckMaterialNew(slot) then
    SetEnhanceNeedItem(slot, OperateMode.Add, PlaceSlot, bagType)
  end
end
function Lua_Forge_CheckMaterialNew(itemSlot)
  local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  if not bag then
    Lua_Chat_ShowOSD("FORGE_NO_EQUIPMENT")
    return false
  end
  local needPropCount, totalPropInBag, propBagslot, propName, propIcon, bindStoneId, nonBindStoneId, propDec, quality, tableId = GetForgePropStone()
  local itemId = GetTableIdBySlot(gUI_GOODSBOX_BAG.backpack0, itemSlot)
  if itemId == bindStoneId or itemId == nonBindStoneId or itemId == GUIDER_FORGE_ITEMID or itemId == GUIDER_FORGE_ITEMIDUNBIND then
    return true
  end
  return false
end
function Forge_CheckMaterial(slot, PlaceSlot, bagType)
  if bagType ~= gUI_GOODSBOX_BAG.backpack0 then
    return
  end
  if Lua_Forge_CheckMaterial(slot) then
    SetEnhanceNeedItem(slot, OperateMode.Add, PlaceSlot, bagType)
  end
end
function Forge_GetCurrentSlotCanPlaced()
  local lbag1, lslot1 = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT1)
  if not lbag1 then
    return FORGE_SLOT_COLLATE.LUCKYSLOT1
  end
  local lbag2, lslot2 = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT2)
  if not lbag2 then
    return FORGE_SLOT_COLLATE.LUCKYSLOT2
  end
  local lbag3, lslot3 = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT3)
  if not lbag3 then
    return FORGE_SLOT_COLLATE.LUCKYSLOT3
  end
  local lbag4, lslot4 = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT4)
  if not lbag4 then
    return FORGE_SLOT_COLLATE.LUCKYSLOT4
  end
  return FORGE_SLOT_COLLATE.LUCKYSLOT1
end
function Forge_CheckLuckyStone(slot, PlaceSlot, bagType)
  if bagType ~= gUI_GOODSBOX_BAG.backpack0 then
    return
  end
  local ItemType = Lua_Forge_CheckLuckyOrSaftStone(slot)
  if ItemType == FORGE_SLOT_TYPE.FORGE_ISLUCKYSTONE then
    local baseRate, extraRate = _Forge_GetSuccssRate()
    if baseRate + extraRate >= 100 then
      Lua_Chat_ShowOSD("FORGE_LUCKY_NO_NEED")
    else
      SetEnhanceNeedItem(slot, OperateMode.Add, PlaceSlot, bagType)
    end
  elseif ItemType == FORGE_SLOT_TYPE.FORGE_SAFEYSTONE then
  elseif ItemType == FORGE_SLOT_TYPE.FORGE_ERRORLUCKY then
    Lua_Chat_ShowOSD("FORGE_ITEM_WRONG_LUCK")
  elseif ItemType == FORGE_SLOT_TYPE.FORGE_ERRORSAFEY then
  end
end
function Lua_Forge_CheckLuckyOrSaftStone(itemSlot)
  local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  if not bag then
    Lua_Chat_ShowOSD("FORGE_NO_EQUIPMENT")
    return FORGE_SLOT_TYPE.FORGE_UNKOWNTYPE
  end
  local tLuckyStoneBind = {}
  local tLuckyStoneNonBind = {}
  local SafeStoneBind, SafeStoneNonBind
  tLuckyStoneBind[0], tLuckyStoneNonBind[0], tLuckyStoneBind[1], tLuckyStoneNonBind[1], tLuckyStoneBind[2], tLuckyStoneNonBind[2], SafeStoneBind, SafeStoneNonBind = GetLuckyStoneAndSafeStone(slot, bag, 0)
  local itemId, isLuckOrSafeyStone = GetTableIdBySlot(gUI_GOODSBOX_BAG.backpack0, itemSlot)
  if itemId then
    for i = 0, 2 do
      if tLuckyStoneBind[i] == itemId or tLuckyStoneNonBind[i] == itemId then
        return FORGE_SLOT_TYPE.FORGE_ISLUCKYSTONE
      end
    end
    if SafeStoneBind == itemId or SafeStoneNonBind == itemId then
      return FORGE_SLOT_TYPE.FORGE_SAFEYSTONE
    end
  end
  if isLuckOrSafeyStone ~= FORGE_SLOT_TYPE.FORGE_ISLUCKYSTONE and isLuckOrSafeyStone ~= FORGE_SLOT_TYPE.FORGE_SAFEYSTONE then
    Lua_Chat_ShowOSD("FORGE_NOT_LUCKYSTONE")
  else
    return isLuckOrSafeyStone + 2
  end
  return FORGE_SLOT_TYPE.FORGE_UNKOWNTYPE
end
function Lua_Forge_UpdataPropStone()
  _Forge_UpdataPropStone()
end
function _Forge_UpdataPropStone()
end
function Lua_Forge_CheckMaterial(itemSlot)
  local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  if not bag then
    Lua_Chat_ShowOSD("FORGE_NO_EQUIPMENT")
    return false
  end
  local needPropCount, totalPropInBag, propBagslot, propName, propIcon, bindStoneId, nonBindStoneId, propDec, quality, tableId = GetForgePropStone()
  local itemId = GetTableIdBySlot(gUI_GOODSBOX_BAG.backpack0, itemSlot)
  if itemId == bindStoneId or itemId == nonBindStoneId or itemId == GUIDER_FORGE_ITEMID or itemId == GUIDER_FORGE_ITEMIDUNBIND then
    return true
  end
  local strTips = "你放置材料错误，请放入" .. tostring(propName)
  ShowErrorMessage(strTips)
  return false
end
function UI_Forge_ItemDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local itemId = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local form_type = from_win:GetProperty("BoxType")
  local bagType = gUI_GOODSBOX_BAG[form_type]
  local boxName = to_win:GetProperty("WindowName")
  if from_win:GetProperty("GoodBoxIndex") == "GB_Character" then
    Lua_Chat_ShowOSD("FROGE_ON_EQUIPED")
    return
  end
  if from_win:GetProperty("GoodBoxIndex") == "GB_ItemMainBag" or from_win:GetProperty("GoodBoxIndex") == "GB_Mount" then
    if from_win:GetProperty("GoodBoxIndex") == "GB_ItemMainBag" then
      itemId = Lua_Bag_GetRealSlot(from_win, itemId)
    end
    if from_win:GetProperty("GoodBoxIndex") == "GB_Mount" then
      itemId = Lua_Mount_GetRealSlot(itemId)
    end
    if boxName == "Arm_gbox" then
      Forge_CheckEquip(itemId, bagType)
    elseif boxName == "LuckStone1_gbox" then
      Forge_CheckLuckyStone(itemId, FORGE_SLOT_COLLATE.LUCKYSLOT1, bagType)
    elseif boxName == "LuckStone2_gbox" then
      Forge_CheckLuckyStone(itemId, FORGE_SLOT_COLLATE.LUCKYSLOT2, bagType)
    elseif boxName == "LuckStone3_gbox" then
      Forge_CheckLuckyStone(itemId, FORGE_SLOT_COLLATE.LUCKYSLOT3, bagType)
    elseif boxName == "LuckStone4_gbox" then
      Forge_CheckLuckyStone(itemId, FORGE_SLOT_COLLATE.LUCKYSLOT4, bagType)
    elseif boxName == "ForgeStone_gbox" then
      Forge_CheckMaterial(itemId, FORGE_SLOT_COLLATE.MATERIALSLOT, bagType)
    end
  end
end
function _Forge_DelLuckyStone(index)
  local lbag, lslot = GetEnhanceSlotInfo(index + 1)
  if not lbag then
    gUI.Forge.LuckyStoneBox[index]:ClearGoodsItem(0)
    gUI.Forge.LuckyStoneBox[index]:SetItemBindState(0, 0)
    return
  end
  SetEnhanceNeedItem(lslot, OperateMode.Remove, index + 1, lbag)
end
function _Forge_DelMaterial()
  local lbag, lslot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.MATERIALSLOT)
  if not lbag then
    _Forge_UpdateStones()
    return
  end
  SetEnhanceNeedItem(lslot, OperateMode.Remove, FORGE_SLOT_COLLATE.MATERIALSLOT, lbag)
end
function UI_Forge_RClickItem(msg)
  local winBox = WindowToGoodsBox(msg:get_window())
  local boxName = winBox:GetProperty("WindowName")
  if boxName == "Arm_gbox" then
    _Forge_DelEquip()
  elseif boxName == "LuckStone1_gbox" then
    _Forge_DelLuckyStone(0)
  elseif boxName == "LuckStone2_gbox" then
    _Forge_DelLuckyStone(1)
  elseif boxName == "LuckStone3_gbox" then
    _Forge_DelLuckyStone(2)
  elseif boxName == "LuckStone4_gbox" then
    _Forge_DelLuckyStone(3)
  elseif boxName == "ForgeStone_gbox" then
    _Forge_DelMaterial()
  end
end
function UI_Forge_ShowTooltips(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  if boxName == "Arm_gbox" then
    local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
    if not bag then
      return
    end
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(win, bag, slot)
  elseif boxName == "LuckStone1_gbox" then
    local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT1)
    if not bag then
      return
    end
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
  elseif boxName == "LuckStone2_gbox" then
    local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT2)
    if not bag then
      return
    end
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
  elseif boxName == "LuckStone3_gbox" then
    local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT3)
    if not bag then
      return
    end
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
  elseif boxName == "LuckStone4_gbox" then
    local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT4)
    if not bag then
      return
    end
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
  elseif boxName == "ForgeStone_gbox" then
    local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.MATERIALSLOT)
    if not bag then
      if win:IsItemHasIcon(0) then
        local itemId = gUI.Forge.MaterGBox:GetCustomUserData(0)
        Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemId)
      end
      return
    end
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
  end
end
function Lua_Forge_DragItem(winBox, BoxType)
  local boxName = winBox:GetProperty("WindowName")
  if boxName == "Arm_gbox" then
    _Forge_DelEquip()
  elseif boxName == "LuckStone1_gbox" then
    _Forge_DelLuckyStone(0)
  elseif boxName == "LuckStone2_gbox" then
    _Forge_DelLuckyStone(1)
  elseif boxName == "LuckStone3_gbox" then
    _Forge_DelLuckyStone(2)
  elseif boxName == "LuckStone4_gbox" then
    _Forge_DelLuckyStone(3)
  elseif boxName == "ForgeStone_gbox" then
    _Forge_DelMaterial()
  end
end
function UI_Forge_OkBtnClick(msg)
  local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  if not bag then
    Lua_Chat_ShowOSD("FORGE_NO_EQUIPMENT")
    return
  end
  local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev, nextlevel, nextclrpoint = GetForgeEquipInfo(slot)
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, _, equipKind, offlineTime, isTimeLimit, EquipPoint, SkillId, RuneLv, _, nKind, MaxInBag = GetItemInfoBySlot(bagType, bagSlot, -1)
  local icon, itemCount1 = GetBaseInfoByItemId(GUIDER_FORGE_ITEMID)
  local icon1, itemCount2 = GetBaseInfoByItemId(GUIDER_FORGE_ITEMIDUNBIND)
  local needPropCount, totalPropInBag, propBagslot, propName, propIcon, bindStoneId, nonBindStoneId, propDec, quality, bindCount, nonBindCount = GetForgePropStone()
  if totalPropInBag < 0 then
    totalPropInBag = 0
  end
  if curClrLev < 3 and minLevel < 31 and equipKind == 1 then
    if gUI.Forge.MaterialFrist:IsChecked() then
      if nonBindCount < needPropCount then
        Lua_Chat_ShowOSD("FORGE_NOE_MATERIAL")
        return
      end
    elseif needPropCount > itemCount1 + itemCount2 + totalPropInBag then
      Lua_Chat_ShowOSD("FORGE_NOE_MATERIAL")
      return
    end
  elseif gUI.Forge.MaterialFrist:IsChecked() then
    if nonBindCount < needPropCount then
      Lua_Chat_ShowOSD("FORGE_NOE_MATERIAL")
      return
    end
  elseif needPropCount > totalPropInBag then
    Lua_Chat_ShowOSD("FORGE_NOE_MATERIAL")
    return
  end
  local isWarming = false
  local type1, amount1, type2, amount2 = GetForgeGoldConsume(slot, bag, 0)
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  local totalMoney = tonumber(playerNonBindGold) + tonumber(playerBindGold)
  if gUI.Forge.MoneyFrist:IsChecked() then
    if tonumber(playerNonBindGold) < tonumber(amount1) then
      Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
      return false
    end
  elseif tonumber(totalMoney) < tonumber(amount1) then
    Lua_Chat_ShowOSD("NOT_ENOUGH_MONEY")
    return
  end
  Lua_Forge_AskRate()
end
function Lua_Forge_AskRate()
  local baseRate, extraRate = _Forge_GetSuccssRate()
  if not baseRate then
    Lua_Chat_ShowLuaErr("UI_Forge_OkBtnClick-baseRate为空")
    return
  end
  if baseRate + extraRate <= 50 then
    Messagebox_Show("FORGE_ASK_CONFIRM", baseRate + extraRate)
  else
    Lua_Forge_AskBind()
  end
end
function Lua_Forge_AskBind()
  local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev, nextlevel, nextclrpoint = GetForgeEquipInfo(slot)
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, _, equipKind, offlineTime, isTimeLimit, EquipPoint, SkillId, RuneLv, _, nKind, MaxInBag = GetItemInfoBySlot(bagType, bagSlot, -1)
  if not bag then
    Lua_Chat_ShowOSD("FORGE_NO_EQUIPMENT")
    return
  end
  if GetBindInfo(slot, bag) == false then
    Lua_Forge_ComfimWhenMainUnBind()
  else
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  end
end
function Lua_Forge_ComfimWhenMainBind()
  local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev, nextlevel, nextclrpoint = GetForgeEquipInfo(slot)
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, _, equipKind, offlineTime, isTimeLimit, EquipPoint, SkillId, RuneLv, _, nKind, MaxInBag = GetItemInfoBySlot(bagType, bagSlot, -1)
  local icon, itemCount1 = GetBaseInfoByItemId(GUIDER_FORGE_ITEMID)
  local icon1, itemCount2 = GetBaseInfoByItemId(GUIDER_FORGE_ITEMIDUNBIND)
  local needPropCount, totalPropInBag, propBagslot, propName, propIcon, bindStoneId, nonBindStoneId, propDec, quality, bindCount, nonBindCount = GetForgePropStone()
  if totalPropInBag < 0 then
    totalPropInBag = 0
  end
  if gUI.Forge.MoneyFrist:IsChecked() or gUI.Forge.MaterialFrist:IsChecked() then
    Messagebox_Show("FORGE_ASK_CONFIRMMATMONEY")
  elseif curClrLev < 3 and minLevel < 31 and equipKind == 1 then
    if needPropCount > itemCount1 + itemCount2 + bindCount then
      Messagebox_Show("FORGE_ASK_CONFIRMMATMONEY2")
    else
      Lua_Forge_ComfimMoney()
    end
  elseif bindCount < needPropCount then
    Messagebox_Show("FORGE_ASK_CONFIRMMATMONEY2")
  else
    Lua_Forge_ComfimMoney()
  end
end
function Lua_Forge_ComfimMoney()
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local type1, amount1, type2, amount2 = GetForgeGoldConsume(bagSlot, bagType, 0)
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  local totalMoney = tonumber(playerNonBindGold) + tonumber(playerBindGold)
  if tonumber(playerBindGold) < tonumber(amount1) then
    Messagebox_Show("FORGE_ASK_CONFIRMMATMONEY3")
  else
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  end
end
function Lua_Forge_ComfimWhenMainUnBind()
  local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev, nextlevel, nextclrpoint = GetForgeEquipInfo(slot)
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, _, equipKind, offlineTime, isTimeLimit, EquipPoint, SkillId, RuneLv, _, nKind, MaxInBag = GetItemInfoBySlot(bagType, bagSlot, -1)
  local icon, itemCount1 = GetBaseInfoByItemId(GUIDER_FORGE_ITEMID)
  local icon1, itemCount2 = GetBaseInfoByItemId(GUIDER_FORGE_ITEMIDUNBIND)
  local needPropCount, totalPropInBag, propBagslot, propName, propIcon, bindStoneId, nonBindStoneId, propDec, quality, bindCount, nonBindCount = GetForgePropStone()
  local lbag1, lslot1 = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT1)
  local lbag2, lslot2 = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT2)
  local lbag3, lslot3 = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT3)
  local lbag4, lslot4 = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.LUCKYSLOT4)
  local isWarming = false
  if lslot1 and GetBindInfo(lslot1, lbag1) == true then
    isWarming = true
  elseif lslot2 and GetBindInfo(lslot2, lbag2) == true then
    isWarming = true
  elseif lslot3 and GetBindInfo(lslot3, lbag3) == true then
    isWarming = true
  elseif lslot4 and GetBindInfo(lslot4, lbag4) == true then
    isWarming = true
  end
  if nonBindCount < needPropCount then
    isWarming = true
  end
  if not gUI.Forge.MaterialFrist:IsChecked() then
    if curClrLev < 3 and minLevel < 31 and equipKind == 1 then
      if bindCount > 0 or itemCount1 > 0 then
        isWarming = true
      end
    elseif bindCount > 0 then
      isWarming = true
    end
  end
  if isWarming == true then
    Messagebox_Show("FORGE_ASK_CONFIRM_BIND")
  else
    Lua_Forge_AskMoneyBind1()
  end
end
function Lua_Forge_AskMoneyBind()
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local type1, amount1, type2, amount2 = GetForgeGoldConsume(bagSlot, bagType, 0)
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  local totalMoney = tonumber(playerNonBindGold) + tonumber(playerBindGold)
  if gUI.Forge.MoneyFrist:IsChecked() then
    Messagebox_Show("FORGE_ASK_CONFIRMMATMONEY4")
  elseif amount1 > playerBindGold then
    Messagebox_Show("FORGE_ASK_CONFIRMMATMONEY5")
  else
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  end
end
function Lua_Forge_AskMoneyBind1()
  local bagType, bagSlot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local type1, amount1, type2, amount2 = GetForgeGoldConsume(bagSlot, bagType, 0)
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  if gUI.Forge.MoneyFrist:IsChecked() then
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  elseif playerBindGold > 0 then
    Messagebox_Show("FORGE_ASK_CONFIRMMATMONEY6")
  else
    EnhanceItem(gUI.Forge.MoneyFrist:IsChecked(), gUI.Forge.MaterialFrist:IsChecked())
  end
end
function UI_Forge_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Forge.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_ForgeBuy(msg)
  local btn = msg:get_window()
  local parent = btn:GetParent()
  local needPropCount, totalPropInBag, propBagslot, propName, propIcon, bindStoneId, nonBindStoneId, propDec, quality, tableId = GetForgePropStone()
  Lua_RMBShop_QuickBuy(nonBindStoneId)
end
function UI_First_CostUnBindMater(msg)
  _Forge_UpdateStones()
end
function UI_First_CostUnBind(msg)
  _Forge_UpdateMoney()
end
function UI_Forge_CloseRootWin(msg)
  _Forge_CloseWindow()
end
function _OnForge_AddItem(bagType, slot, icon, itemType)
  if itemType == FORGE_SLOT_COLLATE.EQUIPSLOT then
    _Forge_AddEquip(bagType, slot)
  elseif itemType == FORGE_SLOT_COLLATE.MATERIALSLOT then
  elseif itemType == FORGE_SLOT_COLLATE.LUCKYSLOT1 then
    _Forge_AddLuckyStone(0, slot)
  elseif itemType == FORGE_SLOT_COLLATE.LUCKYSLOT2 then
    _Forge_AddLuckyStone(1, slot)
  elseif itemType == FORGE_SLOT_COLLATE.LUCKYSLOT3 then
    _Forge_AddLuckyStone(2, slot)
  elseif itemType == FORGE_SLOT_COLLATE.LUCKYSLOT4 then
    _Forge_AddLuckyStone(3, slot)
  end
end
function _OnForge_DelItem(boxType)
  if boxType == FORGE_SLOT_COLLATE.EQUIPSLOT then
    Forge_ClearUI()
    if ItemGUID ~= 0 then
      local bagOld, slotOld = GetItemPosByGuid(ItemGUID)
      if bagOld == 1 and slotOld ~= -1 then
        MoveItem(gUI_GOODSBOX_BAG.backpack0, slotOld, gUI_GOODSBOX_BAG.myequipbox, ContainPos)
        ContainPos = -1
      end
    end
  elseif boxType == FORGE_SLOT_COLLATE.LUCKYSLOT1 then
    gUI.Forge.LuckyStoneBox[0]:ClearGoodsItem(0)
    gUI.Forge.LuckyStoneBox[0]:SetItemBindState(0, 0)
    _Forge_UpdateSuccessRate()
  elseif boxType == FORGE_SLOT_COLLATE.LUCKYSLOT2 then
    gUI.Forge.LuckyStoneBox[1]:ClearGoodsItem(0)
    gUI.Forge.LuckyStoneBox[1]:SetItemBindState(0, 0)
    _Forge_UpdateSuccessRate()
  elseif boxType == FORGE_SLOT_COLLATE.LUCKYSLOT3 then
    gUI.Forge.LuckyStoneBox[2]:ClearGoodsItem(0)
    gUI.Forge.LuckyStoneBox[2]:SetItemBindState(0, 0)
    _Forge_UpdateSuccessRate()
  elseif boxType == FORGE_SLOT_COLLATE.LUCKYSLOT4 then
    gUI.Forge.LuckyStoneBox[3]:ClearGoodsItem(0)
    gUI.Forge.LuckyStoneBox[3]:SetItemBindState(0, 0)
    _Forge_UpdateSuccessRate()
  end
end
function UI_QuickForge_Click(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local offset = msg:get_wparam()
  local off = msg:get_lparam()
  Forge_Quick_Palce(off)
end
function UI_Place_EquipToForge(msg)
  local wnd = msg:get_window()
  local text = wnd:GetProperty("Text")
  if text == "{#R^收回}" then
    _Forge_DelEquip()
  else
    local index = wnd:GetProperty("CustomUserData")
    Forge_Quick_Palce(tonumber(index))
  end
end
function Forge_Quick_Palce(off)
  local wnd = gUI.Forge.EContainGBox
  if Lua_Forge_IsEquip(_CHARA_EQUIPPOINT_CANTAINFORGE1[off][1], gUI_GOODSBOX_BAG.myequipbox) then
    if Lua_Forge_CheckEquip(_CHARA_EQUIPPOINT_CANTAINFORGE1[off][1], gUI_GOODSBOX_BAG.myequipbox) then
    else
      return
    end
  else
    return
  end
  if wnd:IsItemHasIcon(off) then
    BagSlotOffset = GetItemBlankSlot(1)
    if BagSlotOffset == -1 then
      Lua_Chat_ShowOSD("FORGE_BAGNOSET")
      return
    end
    _Forge_DelEquip()
    Forge_ResetEquip(-1, -1)
    ContainPos = _CHARA_EQUIPPOINT_CANTAINFORGE1[off][1]
    ItemGUID = GetItemGUIDByPos(gUI_GOODSBOX_BAG.myequipbox, _CHARA_EQUIPPOINT_CANTAINFORGE1[off][1], 0)
    if ContainPos ~= -1 then
      MoveItem(gUI_GOODSBOX_BAG.myequipbox, ContainPos, gUI_GOODSBOX_BAG.backpack0, BagSlotOffset)
    end
    SetEnhanceNeedItem(BagSlotOffset, OperateMode.Add, FORGE_SLOT_COLLATE.EQUIPSLOT, gUI_GOODSBOX_BAG.backpack0)
    if ItemGUIDLAST ~= 0 then
    end
    ItemGUIDLAST = ItemGUID
  end
end
function UI_ContainShowTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local offset = msg:get_wparam()
  local tooltip = wnd:GetToolTipWnd(0)
  Lua_Tip_Item(tooltip, _CHARA_EQUIPPOINT_CANTAINFORGE1[offset][1], gUI_GOODSBOX_BAG.myequipbox, true, nil, nil)
end
function _OnForge_GetResult(isSuccess, name, guidStr, lv)
  if isSuccess ~= 0 then
    Lua_Chat_ShowOSD("FORGR_SUCC")
    WorldStage:playSoundByID(3)
  else
    Lua_Chat_ShowOSD("FORGE_FAILED")
    WorldStage:playSoundByID(4)
  end
  local bag, slot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
  local bag1, slot1 = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.MATERIALSLOT)
  if not bag then
    return
  end
  if Lua_Forge_IsEquip(slot, bag) and Lua_Forge_CheckEquip(slot, bag) == false then
    _Forge_DelEquip()
    return
  end
  _Forge_AddEquip(bag, slot)
  _Forge_UpdateStones()
  for i = 2, 5 do
    local lbag, lslot = GetEnhanceSlotInfo(i)
    if lbag and Lua_Forge_CheckLuckyOrSaftStone(lslot) == 2 then
      _Forge_AddLuckyStone(i - 2, lslot)
    else
      _Forge_DelLuckyStone(i - 2)
    end
  end
end
function _OnForge_BagItemChange()
  local wnd = WindowSys_Instance:GetWindow("Forge")
  if wnd:IsVisible() then
    _Forge_UpdateStones()
  end
end
function Script_Forge_OnEvent(event)
  if "SHOW_ENHANCE_WINDOW" == event then
    _OnForge_ShowRootWin()
  elseif "ENHANCE_ADD_ITEM" == event then
    _OnForge_AddItem(arg1, arg2, arg3, arg4)
  elseif "SHOW_ENHANCE_RESULT" == event then
    _OnForge_GetResult(arg1, arg2, arg3, arg4)
  elseif "ENHANCE_DEL_ITEM" == event then
    _OnForge_DelItem(arg1)
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    _OnForge_CloseWindw()
  elseif event == "ITEM_EQUIP_UPDATE" then
    _OnForge_ChangeEquip()
  elseif "ITEM_ADD_TO_BAG" == event then
    _OnForge_BagItemChange()
  elseif "ITEM_DEL_FROM_BAG" == event then
    _OnForge_BagItemChange()
  elseif event == "PLAYER_MONEY_CHANGED" then
    _Forge_UpdateMoney(arg1, arg2, event)
  elseif event == "PLAYER_BIND_MONEY_CHANGED" then
    _Forge_UpdateMoney(arg1, arg2, event)
  end
end
function _OnForge_CloseWindw()
  local wnd = WindowSys_Instance:GetWindow("Forge")
  if wnd:IsVisible() then
    _Forge_CloseWindow()
  end
end
function Script_Forge_OnHide()
  _Forge_CloseWindow()
  if ItemGUID ~= 0 then
    local bagOld, slotOld = GetItemPosByGuid(ItemGUID)
    if slotOld ~= -1 then
      MoveItem(gUI_GOODSBOX_BAG.backpack0, slotOld, gUI_GOODSBOX_BAG.myequipbox, ContainPos)
    end
  end
end
function _Forge_DelAllItem()
  _Forge_DelEquip()
end
function _Forge_DelEquip()
  CancleEnhanceItem()
end
function _Forge_CloseWindow()
  local wnd = WindowSys_Instance:GetWindow("Forge")
  wnd:SetProperty("Visible", "false")
  _Forge_DelAllItem()
  Messagebox_Hide("FORGE_ASK_CONFIRM_BIND")
  Messagebox_Hide("FORGE_ASK_CONFIRM")
  Messagebox_Hide("FORGE_ASK_CONFIRMMATMONEY")
  Messagebox_Hide("FORGE_ASK_CONFIRMMATMONEY2")
  Messagebox_Hide("FORGE_ASK_CONFIRMMATMONEY3")
  Messagebox_Hide("FORGE_ASK_CONFIRMMATMONEY4")
  Messagebox_Hide("FORGE_ASK_CONFIRMMATMONEY5")
  Messagebox_Hide("FORGE_ASK_CONFIRMMATMONEY6")
end
function UI_Forge_Show_FunctionBg(msg)
  if gUI.Forge.FunctionBg:IsVisible() then
    gUI.Forge.FunctionBg:SetVisible(false)
  else
    gUI.Forge.FunctionBg:SetVisible(true)
  end
end
function Script_Forge_OnLoad()
  gUI.Forge.EquipGBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Forge.P1_bg.Arm_gbox"))
  gUI.Forge.EquipNameLab = WindowToLabel(WindowSys_Instance:GetWindow("Forge.P1_bg.Label1_slab"))
  gUI.Forge.MaterGBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Forge.P3_bg.ForgeStone_gbox"))
  gUI.Forge.MaterLab = WindowToLabel(WindowSys_Instance:GetWindow("Forge.P3_bg.Number_dlab"))
  gUI.Forge.LevDes = WindowToLabel(WindowSys_Instance:GetWindow("Forge.P1_bg.Level_slab"))
  gUI.Forge.MaxLab = WindowToLabel(WindowSys_Instance:GetWindow("Forge.P1_bg.Level_dlab"))
  gUI.Forge.DesLBox = WindowToListBox(WindowSys_Instance:GetWindow("Forge.P2_bg.Contrast_lbox"))
  gUI.Forge.LuckyStoneBox = {}
  gUI.Forge.LuckyStoneBox[0] = WindowToGoodsBox(WindowSys_Instance:GetWindow("Forge.P3_bg.LuckStone1_gbox"))
  gUI.Forge.LuckyStoneBox[1] = WindowToGoodsBox(WindowSys_Instance:GetWindow("Forge.P3_bg.LuckStone2_gbox"))
  gUI.Forge.LuckyStoneBox[2] = WindowToGoodsBox(WindowSys_Instance:GetWindow("Forge.P3_bg.LuckStone3_gbox"))
  gUI.Forge.LuckyStoneBox[3] = WindowToGoodsBox(WindowSys_Instance:GetWindow("Forge.P3_bg.LuckStone4_gbox"))
  gUI.Forge.BuyBtn = WindowToButton(WindowSys_Instance:GetWindow("Forge.P3_bg.BuyButton_btn"))
  gUI.Forge.RateLab = WindowToLabel(WindowSys_Instance:GetWindow("Forge.P3_bg.RateTitle_slab.Rate_slab"))
  gUI.Forge.CostLab = WindowToLabel(WindowSys_Instance:GetWindow("Forge.P3_bg.Lab_slab.MoneyAuBind_dlab"))
  gUI.Forge.ComBtn = WindowToButton(WindowSys_Instance:GetWindow("Forge.Confirm_btn"))
  gUI.Forge.CloBtn = WindowToButton(WindowSys_Instance:GetWindow("Forge.Close_btn"))
  gUI.Forge.HelpBtn = WindowToButton(WindowSys_Instance:GetWindow("Forge.Help_btn"))
  gUI.Forge.EContainGBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Forge.Explain_pg.Btborder_pg.Equip_gbox"))
  gUI.Forge.FunctionBg = WindowSys_Instance:GetWindow("Forge.Function_bg.Function_bg")
  gUI.Forge.SetBtnEquip = {}
  for i = 1, 10 do
    gUI.Forge.SetBtnEquip[i - 1] = WindowToButton(WindowSys_Instance:GetWindow("Forge.Explain_pg.Btborder_pg.But" .. tostring(i) .. "_btn"))
  end
  gUI.Forge.MoneyFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("Forge.P3_bg.IsBind_cbtn"))
  gUI.Forge.MoneyFrist:SetChecked(false)
  gUI.Forge.MaterialFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("Forge.P3_bg.Mould_cbtn"))
  gUI.Forge.MaterialFrist:SetChecked(false)
end
tn"))
  gUI.Forge.HelpBtn = WindowToButton(WindowSys_Instance:GetWindow("Forge.Help_btn"))
  gUI.Forge.EContainGBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Forge.Explain_pg.Btborder_pg.Equip_gbox"))
  gUI.Forge.FunctionBg = WindowSys_Instance:GetWindow("Forge.Function_bg.Function_bg")
  gUI.Forge.SetBtnEquip = {}
  for i = 1, 10 do
    gUI.Forge.SetBtnEquip[i - 1] = WindowToButton(WindowSys_Instance:GetWindow("Forge.Explain_pg.Btborder_pg.But" .. tostring(i) .. "_btn"))
  end
  gUI.Forge.MoneyFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("Forge.P3_bg.IsBind_cbtn"))
  gUI.Forge.MoneyFrist:SetChecked(false)
  gUI.Forge.MaterialFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("Forge.P3_bg.Mould_cbtn"))
  gUI.Forge.MaterialFrist:SetChecked(false)
end
