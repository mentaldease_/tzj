if gUI and not gUI.GuildS then
  gUI.GuildS = {}
end
local _GuildS_CurrSelTypeWar = 1
local _GuildS_AllianceNum = 4
local _GuildS_War_Level = 2
function _GuildS_ShowGuildRelation()
  local selected = gUI.GuildS.Relation_tctl:GetSelectItem()
  if selected == 0 then
    local alliNumber = GetGuildAllianceNumber()
    if alliNumber > 0 then
      gUI.GuildS.AlliGuildState:SetVisible(false)
      gUI.GuildS.AlliGuildMain:SetVisible(true)
      Lua_GuildS_ShowAlliInfo(true)
    else
      gUI.GuildS.AlliGuildState:SetVisible(true)
      gUI.GuildS.AlliGuildMain:SetVisible(false)
    end
    gUI.GuildS.MainAlliance:SetVisible(true)
    gUI.GuildS.MainHostile:SetVisible(false)
  else
    gUI.GuildS.MainAlliance:SetVisible(false)
    gUI.GuildS.MainHostile:SetVisible(true)
    Lua_Guilds_ShowHostileInfo()
  end
end
function _GuildS_ShowGuildWar()
  local selected = gUI.GuildS.War_ctcl:GetSelectItem()
  if selected == 0 then
    local _, _, _, level = GetGuildBaseInfo()
    gUI.GuildS.WarGuild:RemoveAllItems()
    if level >= _GuildS_War_Level then
      RequestGuildWarActiveList(1)
    end
    gUI.GuildS.WarListMain:SetVisible(true)
    gUI.GuildS.WarLogMain:SetVisible(false)
  else
    gUI.GuildS.WarListMain:SetVisible(false)
    gUI.GuildS.WarLogMain:SetVisible(true)
    Lua_GuildS_ShowWarInfo()
  end
end
function _GuildS_ShowGuildSiege()
  local selected = gUI.GuildS.SiegeTableCtrl:GetSelectItem()
  if selected == 0 then
    gUI.GuildS.SiegeMain:SetVisible(true)
    gUI.GuildS.SiegeShareBg:SetVisible(false)
    gUI.GuildS.SiegeRuleBg:SetVisible(false)
    Lua_Guilds_ShowSiegeInfo()
  elseif selected == 1 then
    gUI.GuildS.SiegeMain:SetVisible(false)
    gUI.GuildS.SiegeShareBg:SetVisible(true)
    gUI.GuildS.SiegeRuleBg:SetVisible(false)
  elseif selected == 2 then
    gUI.GuildS.SiegeMain:SetVisible(false)
    gUI.GuildS.SiegeShareBg:SetVisible(false)
    gUI.GuildS.SiegeRuleBg:SetVisible(true)
  end
end
function Lua_GuildS_ShowWarInfo()
  gUI.GuildS.WarLog:RemoveAllItems()
  local count = GetWarLogCount()
  if count > 0 then
    for i = count - 1, 0, -1 do
      local failPoint, killPoint, re, guildName, timeStr = GetWarLog(i)
      if failPoint ~= nil then
        local strLog = string.format("%s|%s|%d:%d", timeStr, guildName, killPoint, failPoint)
        gUI.GuildS.WarLog:InsertString(strLog, -1)
      end
    end
  end
  gUI.GuildS.WarRank:RemoveAllItems()
  count = GetWarRankCount()
  if count > 0 then
    for i = count - 1, 0, -1 do
      local nameRank, reRank = GetEachRankInfo(i)
      if nameRank ~= nil then
        if nameRank == "" then
          nameRank = "未知"
        end
        local insert_index = -1
        local size = gUI.GuildS.WarRank:GetItemCount()
        if size > 0 then
          for i = 0, size - 1 do
            local item1 = gUI.GuildS.WarRank:GetItem(i)
            local key = reRank
            local key_2 = item1:GetColumnText(2)
            key_2 = tonumber(key_2)
            if key > key_2 then
              insert_index = i
              break
            end
          end
        end
        local str = string.format("%d|%s|%d", 1, nameRank, reRank)
        local item = gUI.GuildS.WarRank:InsertColoredString(str, insert_index, 4289982159)
      end
    end
    for i = 0, gUI.GuildS.WarRank:GetItemCount() - 1 do
      local item1 = gUI.GuildS.WarRank:GetItem(i)
      item1:SetColumnText(tostring(i + 1), 0)
    end
  end
end
function Lua_GuildS_ShowAlliInfo(allwaysRequest)
  local visible = gUI.GuildS.MainAlliance:IsVisible()
  if visible or allwaysRequest then
    RequestGuildAllianceInfo()
  end
  local _, _, isGuildLeader, isAllianceLeader = GetGuildAuthority()
  local button1 = WindowToButton(gUI.GuildS.MainAlliance:GetGrandChild("Image_bg.Join_bg.Invite_btn"))
  local button2 = WindowToButton(gUI.GuildS.MainAlliance:GetGrandChild("Image_bg.Join_bg.Delete_btn"))
  local button3 = WindowToButton(gUI.GuildS.MainAlliance:GetGrandChild("Image_bg.Join_bg.Cancel_btn"))
  button1:SetEnable(isAllianceLeader and isGuildLeader)
  button2:SetEnable(isAllianceLeader and isGuildLeader)
  button3:SetEnable(isGuildLeader)
end
function Lua_Guilds_ShowHostileInfo()
  gUI.GuildS.HostileGuildList:RemoveAllItems()
  if gUI.GuildS.MainHostile:IsVisible() then
    RequestGuildHostileInfo()
  end
  local _, _, isGuildLeader = GetGuildAuthority()
  local button1 = WindowToButton(gUI.GuildS.MainHostile:GetGrandChild("Invite_btn"))
  button1:SetEnable(isGuildLeader)
  button1:SetProperty("Text", "添加敌对")
  button1:AddScriptEvent("wm_mouseclick", "UI_GuildS_AddHostileMember")
end
function Lua_Guilds_ShowSiegeInfo()
  local visible = gUI.GuildS.MainSiege:IsVisible()
  if visible then
    RequestGuildAllianceInfo()
    _GuildS_SetCommand()
  end
end
function Lua_GuildS_ConfirmAddMemberAlliance(guildName)
  InviteGuildAlliance(guildName)
end
function Lua_GuildS_ConfirmAddMemberHostile(guildName)
  AddGuildHostile(guildName)
end
function Lua_GuildS_ShowInit()
  _OnGuildS_ChooseCertainInfoWar()
end
function UI_GuildS_StartWar()
  local item = gUI.GuildS.WarGuild:GetSelectedItem()
  if not item then
    Lua_Chat_ShowOSD("GUILD_WAR_TARGET_NONE")
    return
  end
  local guildName = item:GetRenderText(0)
  if guildName ~= "" then
    Messagebox_Show("GUILD_DECLARE_WAR", guildName)
  else
    Lua_Chat_ShowOSD("GUILD_WAR_TARGET_NONE")
  end
end
function UI_GuildS_MainButtonAlliance()
  _GuildS_CurrSelTypeWar = 1
  _OnGuildS_ChooseCertainInfoWar()
end
function UI_GuildS_MainButtonWar()
  _GuildS_CurrSelTypeWar = 2
  _OnGuildS_ChooseCertainInfoWar()
end
function UI_GuildS_MainButtonSiege()
  _GuildS_CurrSelTypeWar = 3
  _OnGuildS_ChooseCertainInfoWar()
end
function UI_GuildS_MainButtonHostile()
  Lua_Guilds_ShowHostileInfo()
end
function UI_GuildS_AlliInviteMember()
  Lua_GuildM_ShowAddMember(2)
end
function UI_GuildS_AlliRemoveMember()
  local item = gUI.GuildS.AlliGuildList:GetSelectedItem()
  if not item then
    Lua_Chat_ShowOSD("GUILD_ALLICANCE_REMOVE_ERROR")
    return
  end
  local guildid = item:GetUserData64()
  local name = item:GetColumnText(0)
  Messagebox_Show("GUILD_ALLICANCE_REMOVE", name, guildid)
end
function UI_GuildS_AlliLeaveMember()
  Messagebox_Show("GUILD_ALLICANCE_QUIT")
end
function UI_GuildS_AddHostileMember()
  Lua_GuildM_ShowAddMember(3)
end
function UI_GuildS_RemoveHostileMember()
  local item = gUI.GuildS.HostileGuildList:GetSelectedItem()
  if not item then
    Lua_Chat_ShowOSD("GUILD_ALLICANCE_REMOVE_ERROR")
    return
  end
  local guildid = item:GetUserData64()
  local name = item:GetColumnText(0)
  Messagebox_Show("GUILD_HOSTILE_REMOVE", name, guildid)
end
function UI_GuildS_HostileSelect()
  local _, _, isGuildLeader = GetGuildAuthority()
  local button1 = WindowToButton(gUI.GuildS.MainHostile:GetGrandChild("Invite_btn"))
  button1:SetEnable(isGuildLeader)
  button1:SetProperty("Text", "删除敌对")
  button1:AddScriptEvent("wm_mouseclick", "UI_GuildS_RemoveHostileMember")
end
function UI_CityShareSetting(curWnd)
  gUI.GuildS.plCity:SetCheckedState(false)
  gUI.GuildS.bwCity:SetCheckedState(false)
  gUI.GuildS.klCity:SetCheckedState(false)
  curWnd:SetCheckedState(true)
  _GuildS_SetCommand()
end
function _GuildS_SetCommand()
  local slot = 0
  if gUI.GuildS.plCity:IsChecked() then
    slot = 0
  elseif gUI.GuildS.bwCity:IsChecked() then
    slot = 2
  elseif gUI.GuildS.klCity:IsChecked() then
    slot = 1
  end
  RequestSiegeCityInfo(slot)
end
function UI_GuildS_SiegeCtrlChange(msg)
  _GuildS_ShowGuildSiege()
end
function _GuildS_Apply_Number()
  local slot = 0
  if gUI.GuildS.plCity:IsChecked() then
    slot = 0
  elseif gUI.GuildS.bwCity:IsChecked() then
    slot = 2
  elseif gUI.GuildS.klCity:IsChecked() then
    slot = 1
  end
  return slot
end
function UI_GuildS_ApplyAttack()
  local slot = _GuildS_Apply_Number()
  Messagebox_Show("GUILD_SIEGE_APPLY_ATTACK", gUI_GUILD_SIEGE_CITY[slot + 1], slot)
end
function UI_GuildS_CancelAttack()
  local slot = _GuildS_Apply_Number()
  Messagebox_Show("GUILD_SIEGE_CANCEL_ATTACK", gUI_GUILD_SIEGE_CITY[slot + 1], slot)
end
function UI_GuildS_ApplyDefence()
  local slot = _GuildS_Apply_Number()
  Messagebox_Show("GUILD_SIEGE_APPLY_DEFENCE", gUI_GUILD_SIEGE_CITY[slot + 1], slot)
end
function UI_GuildS_CancelDefence()
  local slot = _GuildS_Apply_Number()
  Messagebox_Show("GUILD_SIEGE_CANCEL_DEFENCE", gUI_GUILD_SIEGE_CITY[slot + 1], slot)
end
function UI_GuildS_ShareSetting()
  local bVisible = gUI.GuildS.SiegeShareSetting:IsVisible()
  gUI.GuildS.SiegeShareSetting:SetVisible(not bVisible)
end
function UI_GuildS_ShareSettingClose()
  gUI.GuildS.SiegeShareSetting:SetVisible(false)
end
function UI_GuildS_DelShare()
  local item = gUI.GuildS.SiegeShareList:GetSelectedItem()
  if not item then
    ShowErrorMessage("请选择需要删除的帮会")
    return
  end
  local guildID = item:GetUserData64()
  local slot = _GuildS_Apply_Number()
  Messagebox_Show("GUILD_SIEGE_DELSHARE", guildID, slot)
end
function UI_GuildS_ShareSettingSure()
  for i = 1, _GuildS_AllianceNum do
    local ctrlName = string.format("GuildWarShare.GuildWarShare_bg.Share_bg.GuildName%d_cbtn", i)
    local checkbtn = WindowToCheckButton(WindowSys_Instance:GetWindow(ctrlName))
    if checkbtn:IsChecked() then
      local guildID = checkbtn:GetUserData64()
      SetSiegeShareGuild(guildID)
    end
  end
  _GuildS_SetCommand()
  gUI.GuildS.SiegeShareSetting:SetVisible(false)
end
function UI_GuildS_RelationCtrlChange()
  _GuildS_ShowGuildRelation()
end
function UI_GuildS_WarCtrlChange()
  _GuildS_ShowGuildWar()
end
function UI_GuildS_ShowRule(msg)
  local wnd = msg:get_window()
  if gUI.GuildS.RelationRule:IsVisible() then
    gUI.GuildS.RelationRule:SetVisible(false)
  else
    local nType = wnd:GetCustomUserData(0)
    if nType == 0 then
      gUI.GuildS.RelationRule:GetGrandChild("Image_bg.Hostile_bg"):SetVisible(false)
      gUI.GuildS.RelationRule:GetGrandChild("Image_bg.Union_bg"):SetVisible(true)
    elseif nType == 1 then
      gUI.GuildS.RelationRule:GetGrandChild("Image_bg.Hostile_bg"):SetVisible(true)
      gUI.GuildS.RelationRule:GetGrandChild("Image_bg.Union_bg"):SetVisible(false)
    end
    gUI.GuildS.RelationRule:SetVisible(true)
  end
end
function UI_CityShareClick(msg)
  local curr_cbn = WindowToCheckButton(msg:get_window())
  UI_CityShareSetting(curr_cbn)
end
function _OnGuildS_AddWarGuildList(guid, name, lv, leaderName)
  local level = GetGuildProsperity()
  local str = ""
  if level <= 1 or lv <= 1 then
    return
  end
  if 1 >= math.abs(level - lv) then
    local str = string.format("%s|%d级|%s", name, lv, leaderName)
    local item = gUI.GuildS.WarGuild:InsertString(str, -1)
    item:SetUserData64(guid)
  end
end
function _OnGuildS_ShowWarGuildList(curPage, maxPage)
  gUI.GuildS.WarGuild:RemoveAllItems()
end
function _OnGuildS_AddAlliGuildList(guildName, guildLeader, guildLevel, guildID, index)
  if guildName == nil then
    gUI.GuildS.AlliGuildList:RemoveAllItems()
  else
    local str = string.format("%s|%s|%d", guildName, guildLeader, guildLevel)
    local item = gUI.GuildS.AlliGuildList:InsertString(str, -1)
    item:SetUserData64(guildID)
  end
end
function _OnGuildS_GuildHostileUpdate(guildName, guildLeader, guildLevel, guildID, index)
  if guildName == nil then
    Lua_Guilds_ShowHostileInfo()
    local _, _, isGuildLeader = GetGuildAuthority()
    local button1 = WindowToButton(gUI.GuildS.MainHostile:GetGrandChild("Invite_btn"))
    button1:SetEnable(isGuildLeader)
    button1:SetProperty("Text", "添加敌对")
    button1:AddScriptEvent("wm_mouseclick", "UI_GuildS_AddHostileMember")
  else
    local str = string.format("%s|%s|%d", guildName, guildLeader, guildLevel)
    local item = gUI.GuildS.HostileGuildList:InsertString(str, -1)
    item:SetUserData64(guildID)
  end
end
function _OnGuildS_AlliRecieveInvite(guildID, guildName)
  Messagebox_Show("GUILD_ALLICANCE_INVITE", guildName, guildID)
end
function _OnGuildS_AlliSendInvite()
  Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_ALLICANCE_SEND)
end
function _OnGuildS_AllianceQuit(bKicked, guildName)
  if guildName == nil then
    local visible = gUI.GuildS.MainRelation:IsVisible()
    if visible then
      gUI.GuildS.MainAlliance:SetVisible(false)
      gUI.GuildS.MainWar:SetVisible(true)
    end
    if bKicked then
      Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_ALLICANCE_KICKSELF)
    else
      Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_ALLICANCE_QUITSELF)
    end
  elseif bKicked then
    local strLog = UI_SYS_MSG.GUILD_ALLICANCE_KICK
    strLog = string.format(strLog, guildName)
    Lua_Chat_AddSysLog(strLog)
  else
    local strLog = UI_SYS_MSG.GUILD_ALLICANCE_QUIT
    strLog = string.format(strLog, guildName)
    Lua_Chat_AddSysLog(strLog)
  end
end
function _OnGuildS_AllianceCreate(npcID)
  Messagebox_Show("GUILD_ALLICANCE_CREATE", npcID)
end
function _OnGuildS_AllianceCreateSuccess()
  Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_ALLICANCE_CREATE)
end
function _OnGuildS_AllianceAddSuccess(isOwner, guildName)
  if isOwner then
    local strLog = UI_SYS_MSG.GUILD_ALLICANCE_ADDSELF
    strLog = string.format(strLog, guildName)
    Lua_Chat_AddSysLog(strLog)
  else
    local strLog = UI_SYS_MSG.GUILD_ALLICANCE_ADD
    strLog = string.format(strLog, guildName)
    Lua_Chat_AddSysLog(strLog)
  end
end
function _OnGuildS_AllianceAddFailed(guildName)
  local strLog = UI_SYS_MSG.GUILD_ALLICANCE_FAILED
  strLog = string.format(strLog, guildName)
  Lua_Chat_AddSysLog(strLog)
end
function _OnGuildS_HostileAdd(guildName)
  local strLog = UI_SYS_MSG.GUILD_HOSTILE_ADD
  strLog = string.format(strLog, guildName)
  Lua_Chat_AddSysLog(strLog)
end
function _OnGuildS_BeHostileAdd(guildName)
  local strLog = UI_SYS_MSG.GUILD_BEHOSTILE_ADD
  strLog = string.format(strLog, guildName)
  Lua_Chat_AddSysLog(strLog)
end
function _OnGuildS_ChooseCertainInfoWar()
  local btn1 = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Left_bg.Relation_btn")
  local btn2 = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Left_bg.War_btn")
  local btn3 = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Left_bg.UnionWar_btn")
  btn1:SetProperty("Enable", "true")
  btn2:SetProperty("Enable", "true")
  btn3:SetProperty("Enable", "true")
  gUI.GuildS.MainWar:SetVisible(false)
  gUI.GuildS.MainRelation:SetVisible(false)
  gUI.GuildS.MainSiege:SetVisible(false)
  if tonumber(btn1:GetProperty("CustomUserData")) == _GuildS_CurrSelTypeWar then
    btn1:SetProperty("Enable", "false")
    gUI.GuildS.MainRelation:SetVisible(true)
    _GuildS_ShowGuildRelation()
  elseif tonumber(btn2:GetProperty("CustomUserData")) == _GuildS_CurrSelTypeWar then
    btn2:SetProperty("Enable", "false")
    gUI.GuildS.MainWar:SetVisible(true)
    _GuildS_ShowGuildWar()
  elseif tonumber(btn3:GetProperty("CustomUserData")) == _GuildS_CurrSelTypeWar then
    btn3:SetProperty("Enable", "false")
    gUI.GuildS.MainSiege:SetVisible(true)
    _GuildS_ShowGuildSiege()
  end
end
function _OnGuildS_SiegeCityInfo(guild_name, guild_leader)
  local label = WindowToLabel(gUI.GuildS.MainSiege:GetGrandChild("GuildName_dlab"))
  label:SetProperty("Text", guild_name)
  label = WindowToLabel(gUI.GuildS.MainSiege:GetGrandChild("LeaderName_dlab"))
  label:SetProperty("Text", guild_leader)
  gUI.GuildS.SiegeAttackList:RemoveAllItems()
  gUI.GuildS.SiegeDefenceList:RemoveAllItems()
  gUI.GuildS.SiegeShareList:RemoveAllItems()
  gUI.GuildS.SiegeAttackBtn1:SetVisible(true)
  gUI.GuildS.SiegeAttackBtn2:SetVisible(false)
  gUI.GuildS.SiegeDefenceBtn1:SetVisible(true)
  gUI.GuildS.SiegeDefenceBtn2:SetVisible(false)
end
function _OnGuildS_SiegeAttackInfo(guild_name, guild_leader)
  local str = string.format("%s|%s", guild_name, guild_leader)
  local item = gUI.GuildS.SiegeAttackList:InsertString(str, -1)
end
function _OnGuildS_SiegeDefenceInfo(guild_name, guild_leader)
  local str = string.format("%s|%s", guild_name, guild_leader)
  local item = gUI.GuildS.SiegeDefenceList:InsertString(str, -1)
end
function _OnGuildS_SiegeShareInfo(guild_name, guild_leader, guildID)
  local str = string.format("%s|%s", guild_name, guild_leader)
  local item = gUI.GuildS.SiegeShareList:InsertString(str, -1)
  item:SetUserData64(guildID)
end
function _OnGuildS_SiegeShareSettingInfo(guild_name, guild_id, index)
  if guild_name == nil then
    for i = 1, _GuildS_AllianceNum do
      local ctrlName = string.format("GuildWarShare.GuildWarShare_bg.Share_bg.GuildName%d_cbtn", i)
      local checkbtn = WindowToCheckButton(WindowSys_Instance:GetWindow(ctrlName))
      checkbtn:SetProperty("Text", "")
      checkbtn:SetProperty("Visible", "false")
      checkbtn:SetUserData64(0)
    end
  else
    local ctrlName = string.format("GuildWarShare.GuildWarShare_bg.Share_bg.GuildName%d_cbtn", index + 1)
    local checkbtn = WindowToCheckButton(WindowSys_Instance:GetWindow(ctrlName))
    local checkName = checkbtn:GetProperty("Text")
    checkbtn:SetProperty("Text", guild_name)
    checkbtn:SetProperty("Visible", "true")
    checkbtn:SetUserData64(guild_id)
  end
end
function _OnGuildS_SiegeAttackSelf()
  gUI.GuildS.SiegeAttackBtn1:SetVisible(false)
  gUI.GuildS.SiegeAttackBtn2:SetVisible(true)
end
function _OnGuildS_SiegeDefenceSelf()
  gUI.GuildS.SiegeDefenceBtn1:SetVisible(false)
  gUI.GuildS.SiegeDefenceBtn2:SetVisible(true)
end
function Script_GuildS_OnEvent(event)
  if event == "GUILD_WAR_ACTIVE_SHOW" then
    _OnGuildS_ShowWarGuildList(arg1, arg2)
  elseif event == "GUILD_WAR_ACTIVE_UPDATE" then
    _OnGuildS_AddWarGuildList(arg1, arg2, arg3, arg4)
  elseif event == "GUILD_ALLIANCE_INFO_UPDATE" then
    _OnGuildS_AddAlliGuildList(arg1, arg2, arg3, arg4, arg5)
  elseif event == "GUILD_ALLIANCE_RECIEVE" then
    _OnGuildS_AlliRecieveInvite(arg1, arg2)
  elseif event == "GUILD_ALLIANCE_SEND" then
    _OnGuildS_AlliSendInvite()
  elseif event == "GUILD_ALLIANCE_QUIT" then
    _OnGuildS_AllianceQuit(arg1, arg2)
  elseif event == "GUILD_ALLIANCE_CREATE" then
    _OnGuildS_AllianceCreate(arg1)
  elseif event == "GUILD_ALLIANCE_CREATE_SUCCESS" then
    _OnGuildS_AllianceCreateSuccess()
  elseif event == "GUILD_ALLIANCE_ADD_SUCCESS" then
    _OnGuildS_AllianceAddSuccess(arg1, arg2)
  elseif event == "GUILD_ALLIANCE_ADD_FAILED" then
    _OnGuildS_AllianceAddFailed(arg1, arg2)
  elseif event == "GUILD_SIEGE_CITYINFO" then
    _OnGuildS_SiegeCityInfo(arg1, arg2)
  elseif event == "GUILD_SIEGE_ATTACKINFO" then
    _OnGuildS_SiegeAttackInfo(arg1, arg2)
  elseif event == "GUILD_SIEGE_DEFENCEINFO" then
    _OnGuildS_SiegeDefenceInfo(arg1, arg2)
  elseif event == "GUILD_SIEGE_SHAREINFO" then
    _OnGuildS_SiegeShareInfo(arg1, arg2, arg3)
  elseif event == "GUILD_SIEGE_SHARESETTINGINFO" then
    _OnGuildS_SiegeShareSettingInfo(arg1, arg2, arg3)
  elseif event == "GUILD_SIEGE_ATTACKSELF" then
    _OnGuildS_SiegeAttackSelf()
  elseif event == "GUILD_SIEGE_DEFENCESELF" then
    _OnGuildS_SiegeDefenceSelf()
  elseif event == "GUILD_HOSTILE_INFO_UPDATE" then
    _OnGuildS_GuildHostileUpdate(arg1, arg2, arg3, arg4, arg5)
  elseif event == "GUILD_HOSTILE_ADD" then
    _OnGuildS_HostileAdd(arg1)
  elseif event == "GUILD_BEHOSTILE_ADD" then
    _OnGuildS_BeHostileAdd(arg1)
  end
end
function Script_GuildS_OnLoad()
  gUI.GuildS.MainRelation = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.Relation_bg")
  gUI.GuildS.MainAlliance = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.Relation_bg.Union_bg")
  gUI.GuildS.MainWar = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.War_bg")
  gUI.GuildS.MainSiege = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg")
  gUI.GuildS.MainHostile = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.Relation_bg.Hostile_bg")
  gUI.GuildS.SiegeShareSetting = WindowSys_Instance:GetWindow("GuildWarShare")
  gUI.GuildS.AlliGuildList = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.Relation_bg.Union_bg.Image_bg.Join_bg.UnionList_lbox"))
  gUI.GuildS.AddMember = WindowSys_Instance:GetWindow("GuildAdd")
  gUI.GuildS.AddMemberLabel = WindowSys_Instance:GetWindow("GuildAdd.UserNameTips_dlab")
  gUI.GuildS.AlliGuildState = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.Relation_bg.Union_bg.Image_bg.NotVia_bg")
  gUI.GuildS.AlliGuildMain = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.Relation_bg.Union_bg.Image_bg.Join_bg")
  gUI.GuildS.Relation_tctl = WindowToTableCtrl(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.Relation_bg.Relation_tctl"))
  gUI.GuildS.WarListMain = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.War_bg.List_bg")
  gUI.GuildS.WarLogMain = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.War_bg.Note_bg")
  gUI.GuildS.WarRank = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.War_bg.Note_bg.Down_bg.Kill_lbox"))
  gUI.GuildS.WarLog = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.War_bg.Note_bg.Up_bg.Note_lbox"))
  gUI.GuildS.WarGuild = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.War_bg.List_bg.Up_bg.List_lbox"))
  gUI.GuildS.War_ctcl = WindowToTableCtrl(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.War_bg.War_ctcl"))
  gUI.GuildS.plCity = WindowToCheckButton(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.AreaName_bg.plcity_cbtn"))
  gUI.GuildS.bwCity = WindowToCheckButton(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.AreaName_bg.bwcity_cbtn"))
  gUI.GuildS.klCity = WindowToCheckButton(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.AreaName_bg.klcity_cbtn"))
  gUI.GuildS.plCity:SetCheckedState(true)
  gUI.GuildS.bwCity:SetCheckedState(false)
  gUI.GuildS.klCity:SetCheckedState(false)
  gUI.GuildS.SiegeMain = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Info_bg")
  gUI.GuildS.SiegeAttackList = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Info_bg.Attack_bg.GuildList_lbox"))
  gUI.GuildS.SiegeAttackBtn1 = WindowToButton(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Info_bg.Attack_bg.RegistAttack1_btn"))
  gUI.GuildS.SiegeAttackBtn2 = WindowToButton(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Info_bg.Attack_bg.RegistAttack2_btn"))
  gUI.GuildS.SiegeAttackBtn1:SetVisible(true)
  gUI.GuildS.SiegeAttackBtn2:SetVisible(false)
  gUI.GuildS.SiegeDefenceList = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Info_bg.Defense_bg.GuildList_lbox"))
  gUI.GuildS.SiegeDefenceBtn1 = WindowToButton(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Info_bg.Defense_bg.RegistDefence1_btn"))
  gUI.GuildS.SiegeDefenceBtn2 = WindowToButton(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Info_bg.Defense_bg.RegistDefence2_btn"))
  gUI.GuildS.SiegeDefenceBtn1:SetVisible(true)
  gUI.GuildS.SiegeDefenceBtn2:SetVisible(false)
  gUI.GuildS.SiegeShareBg = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Share_bg")
  gUI.GuildS.SiegeShareList = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Share_bg.ShareList_lbox"))
  gUI.GuildS.SiegeShareBtn = WindowToButton(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Share_bg.RegistShare_btn"))
  gUI.GuildS.SiegeRuleBg = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.Di_bg.Rule_bg")
  gUI.GuildS.SiegeMain:SetVisible(true)
  gUI.GuildS.SiegeShareBg:SetVisible(false)
  gUI.GuildS.SiegeRuleBg:SetVisible(false)
  gUI.GuildS.SiegeTableCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.UnionWar_bg.UnionWar_tctrl"))
  gUI.GuildS.SiegeTableCtrl:SelectItem(0)
  gUI.GuildS.HostileGuildList = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg.Right_bg.Relation_bg.Hostile_bg.List_bg.HostileList_lbox"))
  gUI.GuildS.RelationRule = WindowSys_Instance:GetWindow("GuildMainRule")
  _GuildS_CurrSelTypeWar = 1
end

