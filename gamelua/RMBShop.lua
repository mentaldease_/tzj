local _RMBSHOP_TYPE = {
  Diamond = 0,
  Crystal = 1,
  Welfare = 2
}
local _RMBSHOP_TYPENAME = {
  [0] = "钻石",
  [1] = "水晶",
  [2] = "福利点"
}
local {
  [5] = "UiIamge005:Discount05_Image",
  [10] = "UiIamge005:Discount1_Image",
  [15] = "UiIamge005:Discount15_Image",
  [20] = "UiIamge005:Discount2_Image",
  [25] = "UiIamge005:Discount25_Image",
  [30] = "UiIamge005:Discount3_Image",
  [35] = "UiIamge005:Discount35_Image",
  [40] = "UiIamge005:Discount4_Image",
  [45] = "UiIamge005:Discount45_Image",
  [50] = "UiIamge005:Discount5_Image",
  [55] = "UiIamge005:Discount55_Image",
  [60] = "UiIamge005:Discount6_Image",
  [65] = "UiIamge005:Discount65_Image",
  [70] = "UiIamge005:Discount7_Image",
  [75] = "UiIamge005:Discount75_Image",
  [80] = "UiIamge005:Discount8_Image",
  [85] = "UiIamge005:Discount85_Image",
  [90] = "UiIamge005:Discount9_Image"
}[95], _RMBDISCOUNT = "UiIamge005:Discount95_Image", {
  [5] = "UiIamge005:Discount05_Image",
  [10] = "UiIamge005:Discount1_Image",
  [15] = "UiIamge005:Discount15_Image",
  [20] = "UiIamge005:Discount2_Image",
  [25] = "UiIamge005:Discount25_Image",
  [30] = "UiIamge005:Discount3_Image",
  [35] = "UiIamge005:Discount35_Image",
  [40] = "UiIamge005:Discount4_Image",
  [45] = "UiIamge005:Discount45_Image",
  [50] = "UiIamge005:Discount5_Image",
  [55] = "UiIamge005:Discount55_Image",
  [60] = "UiIamge005:Discount6_Image",
  [65] = "UiIamge005:Discount65_Image",
  [70] = "UiIamge005:Discount7_Image",
  [75] = "UiIamge005:Discount75_Image",
  [80] = "UiIamge005:Discount8_Image",
  [85] = "UiIamge005:Discount85_Image",
  [90] = "UiIamge005:Discount9_Image"
}
local _RMBSHOP_ITEMTYPE = {
  [0] = "UiIamge022:Image_News",
  [1] = "UiIamge022:Image_Hot",
  [2] = "UiIamge022:Image_LimitTime",
  [3] = "UiIamge022:Image_Special"
}
local _RMBWND_AVATARNAME = "RMBShop"
local _RMBSHOP_AVATAROTATE = "RMB_Rotate"
local _RMBShop_ItemWnd_MaxNum = 9
local _RMBShop_ItemMax = 10
local _RMBShop_ItemWnd_RowNum = 3
local _RMBSHOP_GROUPIDMIN = 0
local _RMBSHOP_GROUPIDMAX = 9
local _RMBWnd_Root, _RMBWnd_AvatarWnd, _RMBWnd_GiftAndBuyRoot, _RMBWnd_BuyWnd, _RMBWnd_FriendListRoot, _RMBWnd_RmbShoppingCartWnd, _RMBWnd_NewGiftWnd, _RMBWnd_CHARTNUM, _RMBWnd_SumItems, _RMBWnd_SumPrices, _RMBWnd_DiamondNumParent, _RMBWnd_WelfareNumParent, _RMBWnd_CrystalNumParent
local _RMBWnd_Item_List = {}
local _RMBWnd_CurrChart_List = {}
local _RMBWnd_DiamondNum, _RMBWnd_WelfareNum, _RMBWnd_CrystalNum, _RMBShop_ShowBox, _RMBShop_CurrSelType, _RMBShop_ShoppingChart_Type, _RMBShop_CurrSelGroup, _RMBShop_ShowTips
function _RMBShop_Init()
  _RMBShop_VariableInit()
  _RMBShop_FunctionInit()
end
function _RMBShop_VariableInit()
  _RMBWnd_Root = WindowSys_Instance:GetWindow("RMBShop")
  _RMBWnd_DiamondNumParent = WindowSys_Instance:GetWindow("RMBShop.Picture4_bg.Picture1_bg")
  _RMBWnd_DiamondNum = WindowSys_Instance:GetWindow("RMBShop.Picture4_bg.Picture1_bg.Currency01_dlab")
  _RMBWnd_WelfareNumParent = WindowSys_Instance:GetWindow("RMBShop.Picture4_bg.Picture2_bg")
  _RMBWnd_WelfareNum = WindowSys_Instance:GetWindow("RMBShop.Picture4_bg.Picture2_bg.Currency02_dlab")
  _RMBWnd_CrystalNumParent = WindowSys_Instance:GetWindow("RMBShop.Picture4_bg.Picture3_bg")
  _RMBWnd_CrystalNum = WindowSys_Instance:GetWindow("RMBShop.Picture4_bg.Picture3_bg.Currency03_dlab")
  _RMBWnd_AvatarWnd = WindowSys_Instance:GetWindow("RMBShop.Picture3_bg.ItemModle_pic.Yulan_bg")
  _RMBWnd_GiftAndBuyRoot = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy")
  _RMBWnd_BuyWnd = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBBuy")
  _RMBWnd_FriendListRoot = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBGift.NameList_bg")
  _RMBWnd_RmbShoppingCartWnd = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBBuy")
  _RMBWnd_NewGiftWnd = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBGift")
  _RMBWnd_CHARTNUM = WindowSys_Instance:GetWindow("RMBShop.Picture4_bg.Shopping_bg.Shopping_dlab")
  _RMBWnd_SumItems = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBBuy.Down_bg.SumCount_dlab")
  _RMBWnd_SumPrices = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBBuy.Down_bg.SumPrice_dlab")
  _RMBShop_ShowTips = WindowSys_Instance:GetWindow("RMBShop.Picture4_bg.Currency_slab")
  _RMBWnd_BuyWnd_New = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.Buy_bg")
  _RMBWnd_ScrollBar = WindowToLabel(WindowSys_Instance:GetWindow("RMBShop.Post_bg.Post_dlab"))
  _RMBWnd_ScrollBar:SetProperty("Text", tostring(""))
end
function RMBShop_SetContent()
  local content = RMBGetContent()
  if content ~= "" then
    _RMBWnd_ScrollBar:SetProperty("Text", tostring(content))
  end
end
function UI_RMBShop_ScrollEnd()
  if _RMBWnd_Root:IsVisible() then
    RMBShop_SetContent()
  end
end
function _RMBShop_FunctionInit()
  _RMBShop_ShowBox = UIInterface:getShowbox(_RMBWND_AVATARNAME)
  if not _RMBShop_ShowBox then
    _RMBShop_ShowBox = UIInterface:createShowbox(_RMBWND_AVATARNAME, _RMBWnd_AvatarWnd)
  end
  _RMBShop_CurrSelType = _RMBSHOP_TYPE.Diamond
  _RMBShop_ShoppingChart_Type = _RMBShop_CurrSelType
  _RMBShop_CurrSelGroup = _RMBSHOP_GROUPIDMIN
  _RMBShop_UpdateWndVisiable()
end
function _RMBShop_CreateTmpWnd()
  local itemWnd_Tmp = WindowSys_Instance:GetWindow("RMBShop.Template02_bg")
  local itemWnd_parent = WindowSys_Instance:GetWindow("RMBShop.Picture1_bg.Picture5")
  local itemWndTmp2 = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBBuy.Image_bg.Image_bg.Shoping_Template")
  local itemWndParent2 = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBBuy.Image_bg.Image_bg")
  local OffsetY = 19
  for i = 0, _RMBShop_ItemWnd_MaxNum - 1 do
    _RMBWnd_Item_List[i] = WindowSys_Instance:CreateWndFromTemplate(itemWnd_Tmp, itemWnd_parent)
    local rect = _RMBWnd_Item_List[i]:GetWndRect()
    _RMBWnd_Item_List[i]:SetLeft((rect:get_width() + rect:get_width() * 0.01) * math.floor(i % _RMBShop_ItemWnd_RowNum) + rect:get_width() * 0.05)
    _RMBWnd_Item_List[i]:SetTop((rect:get_height() + rect:get_width() * 0.01) * math.floor(i / _RMBShop_ItemWnd_RowNum) + rect:get_height() * 0.05)
    _RMBWnd_Item_List[i]:SetVisible(false)
  end
  for j = 0, _RMBShop_ItemMax - 1 do
    _RMBWnd_CurrChart_List[j] = WindowSys_Instance:CreateWndFromTemplate(itemWndTmp2, itemWndParent2)
    local rect = _RMBWnd_CurrChart_List[j]:GetWndRect()
    _RMBWnd_CurrChart_List[j]:SetTop(rect:get_height() * j + rect:get_height() * 0.05)
    _RMBWnd_CurrChart_List[j]:SetVisible(false)
  end
end
function _RMBShop_GetChildWndByParentWnd(Parent)
  local goodsBox = WindowToGoodsBox(Parent:GetGrandChild("GoodsBox_gbox"))
  local itemNameWnd = Parent:GetGrandChild("ItemName_slab")
  local StackNumWnd = Parent:GetGrandChild("ItemCount02_dlab")
  local PriceNumWnd = Parent:GetGrandChild("ItemPrice04_dlab")
  local IndateWnd = Parent:GetGrandChild("ItemIndate_dlab")
  local DisCWnd = Parent:GetGrandChild("Picture3_pic")
  local DisWnd2 = Parent:GetGrandChild("Discount_pic")
  local itemNameWnd1 = Parent:GetGrandChild("ItemName2_slab")
  local itemNameWnd2 = Parent:GetGrandChild("ItemName3_slab")
  local OrginPriceWnd = Parent:GetGrandChild("Price_dlab")
  local OrginLine = Parent:GetGrandChild("Price_dlab.Line_bg")
  return goodsBox, itemNameWnd, StackNumWnd, PriceNumWnd, IndateWnd, DisCWnd, DisWnd2, itemNameWnd1, itemNameWnd2, OrginPriceWnd, OrginLine
end
function _RMBShop_ClearAllPageItem()
  for i = 0, #_RMBWnd_Item_List do
    local goodsBox, itemNameWnd, StackNumWnd, PriceNumWnd, IndateWnd, DisCWnd, DisWnd2, itemNameWnd1, itemNameWnd2, OrginPriceWnd = _RMBShop_GetChildWndByParentWnd(_RMBWnd_Item_List[i])
    goodsBox:ClearGoodsItem(0)
    itemNameWnd:SetProperty("Text", "")
    itemNameWnd1:SetProperty("Text", "")
    itemNameWnd2:SetProperty("Text", "")
    StackNumWnd:SetProperty("Text", "")
    PriceNumWnd:SetProperty("Text", "")
    IndateWnd:SetProperty("Text", "")
    DisCWnd:SetVisible(false)
    DisWnd2:SetVisible(false)
    _RMBWnd_Item_List[i]:SetVisible(false)
    OrginPriceWnd:SetProperty("Text", "")
  end
end
function _RMBShop_ClearAllSelectItem()
  for i = 0, #_RMBWnd_Item_List do
    local frameWnd = _RMBWnd_Item_List[i]:GetGrandChild("Picture2_bg")
    frameWnd:SetVisible(false)
  end
  _RMBShop_ClearPreview()
end
function _RMBShop_ClearPreview()
end
function _RMBShop_SetSelectPageItem(parent)
  _RMBShop_ClearAllSelectItem()
  local frameWnd = parent:GetGrandChild("Picture2_bg")
  frameWnd:SetVisible(true)
  local goodsBox = WindowToGoodsBox(parent:GetGrandChild("GoodsBox_gbox"))
  local itemID = goodsBox:GetItemData(0)
  local bIsShowModelItem, nType, itemDesc = PreviewItem(itemID)
  if bIsShowModelItem and nType ~= -1 then
    _RMBWnd_AvatarWnd:SetVisible(true)
    _RMBShop_ShowBox:revertCamera()
    _RMBShop_ShowBox:createMallAvatar(itemID)
    if nType == 0 then
      _RMBShop_ShowBox:setAvatarScale(0.9)
    elseif nType == 1 then
      _RMBShop_ShowBox:setrevertCamera(-50)
      _RMBShop_ShowBox:setAvatarScale(0.7)
    elseif nType == 2 then
      _RMBShop_ShowBox:setAvatarScale(0.7)
    end
  end
end
function _RMBShop_CloseAllChildWnd()
  _RMBWnd_GiftAndBuyRoot:SetVisible(false)
  _RMBWnd_NewGiftWnd:SetVisible(false)
  _RMBWnd_BuyWnd:SetVisible(false)
  _RMBWnd_FriendListRoot:SetVisible(false)
  _RMBWnd_BuyWnd_New:SetVisible(false)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_RMBShop_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("RMBShop.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function _RMBShop_GetMoneyInfo()
  if _RMBShop_CurrSelType == _RMBSHOP_TYPE.Diamond then
    return tonumber(_RMBWnd_DiamondNum:GetProperty("Text"))
  elseif _RMBShop_CurrSelType == _RMBSHOP_TYPE.Welfare then
    return tonumber(_RMBWnd_WelfareNum:GetProperty("Text"))
  elseif _RMBShop_CurrSelType == _RMBSHOP_TYPE.Crystal then
    return tonumber(_RMBWnd_CrystalNum:GetProperty("Text"))
  end
end
function _RMBShop_SetEnableGiftBtn()
  local visible = true
  if _RMBShop_CurrSelType ~= _RMBSHOP_TYPE.Diamond then
    visible = false
  end
  for index, value in pairs(_RMBWnd_Item_List) do
  end
end
function _RMBShop_SetTypeBtnEnable()
  local btn1 = _RMBWnd_Root:GetGrandChild("ShopButton01_btn")
  local btn2 = _RMBWnd_Root:GetGrandChild("ShopButton02_btn")
  local btn3 = _RMBWnd_Root:GetGrandChild("ShopButton03_btn")
  btn1:SetProperty("Enable", "true")
  btn2:SetProperty("Enable", "true")
  btn3:SetProperty("Enable", "true")
  _RMBShop_UpdateWndVisiable()
  if tonumber(btn1:GetProperty("CustomUserData")) == _RMBShop_CurrSelType then
    _RMBWnd_DiamondNumParent:SetProperty("Visible", "true")
    _RMBShop_ShowTips:SetProperty("Text", "")
    btn1:SetProperty("Enable", "false")
  elseif tonumber(btn2:GetProperty("CustomUserData")) == _RMBShop_CurrSelType then
    _RMBWnd_CrystalNumParent:SetProperty("Visible", "true")
    _RMBShop_ShowTips:SetProperty("Text", "（提示：在水晶商城购买了物品后会直接与你绑定）")
    btn2:SetProperty("Enable", "false")
  elseif tonumber(btn3:GetProperty("CustomUserData")) == _RMBShop_CurrSelType then
    _RMBWnd_WelfareNumParent:SetProperty("Visible", "true")
    _RMBShop_ShowTips:SetProperty("Text", "（提示：在福利商城购买了物品后会直接与你绑定）")
    btn3:SetProperty("Enable", "false")
  end
end
function _RMBShop_InitBuyWindow(selItem, btn)
end
function _RMBShop_InitGiftWindow(selItem, btn)
  if GetRemainProtectTime() > 0 then
    Lua_Chat_ShowOSD("SAFE_TIME_LIMIT")
    return
  end
  local parentWnd
  parentWnd = _RMBWnd_NewGiftWnd
  if parentWnd == nil then
    return
  end
  local buy_goodsBox = WindowToGoodsBox(parentWnd:GetGrandChild("Image_bg.Gift_Template.GoodsBox1"))
  local buy_ItemName = parentWnd:GetGrandChild("Image_bg.Gift_Template.Name_dlab")
  local buy_Tprice = parentWnd:GetGrandChild("Image_bg.Gift_Template.SumPrice_dlab")
  local buy_Price = parentWnd:GetGrandChild("Image_bg.Gift_Template.Price_dlab")
  local buy_editBox = parentWnd:GetGrandChild("Image_bg.Gift_Template.Count_bg.Count_bg.Count_ebox")
  local goodsBox, itemNameWnd, StackNumWnd, PriceNumWnd, IndateWnd, DisCWnd, DisWnd2, itemNameWnd1, itemNameWnd2, OrginPriceWnd = _RMBShop_GetChildWndByParentWnd(selItem)
  if goodsBox:GetItemIcon(0) ~= "" then
    _RMBWnd_GiftAndBuyRoot:SetVisible(true)
    parentWnd:SetVisible(true)
    buy_goodsBox:SetItemGoods(0, goodsBox:GetItemIcon(0), goodsBox:GetItemQuality(0))
    buy_goodsBox:SetItemData(0, goodsBox:GetItemData(0))
    buy_goodsBox:SetUserData64(goodsBox:GetUserData64())
    if itemNameWnd:GetProperty("Text") == "" then
      buy_ItemName:SetProperty("Text", itemNameWnd1:GetProperty("Text"))
    else
      buy_ItemName:SetProperty("Text", itemNameWnd:GetProperty("Text"))
    end
    buy_Price:SetProperty("Text", PriceNumWnd:GetProperty("Text"))
    buy_editBox:SetProperty("Text", "1")
    buy_editBox:SetCustomUserData(0, tonumber(StackNumWnd:GetProperty("Text")))
    buy_Tprice:SetProperty("Text", buy_Price:GetProperty("Text"))
  else
    _RMBShop_CloseAllChildWnd()
  end
end
function _RMBShop_InitBuyWindowNew(selItem, btn)
  if GetRemainProtectTime() > 0 then
    Lua_Chat_ShowOSD("SAFE_TIME_LIMIT")
    return
  end
  local parentWnd
  parentWnd = _RMBWnd_BuyWnd_New
  if parentWnd == nil then
    return
  end
  local buy_goodsBox = WindowToGoodsBox(parentWnd:GetGrandChild("Buy_bg.Image_bg.Article_gbox"))
  local buy_ItemName = WindowToLabel(parentWnd:GetGrandChild("Buy_bg.Image_bg.Article_dlab"))
  local buy_editBox = WindowToEditBox(parentWnd:GetGrandChild("Buy_bg.Image_bg.Count_bg.Count_ebox"))
  local buy_Tprice = WindowToLabel(parentWnd:GetGrandChild("Buy_bg.Image_bg.Price_bg.AllPrice_dlab"))
  local buy_price = WindowToLabel(parentWnd:GetGrandChild("Buy_bg.Image_bg.Price_bg.Priec_dlab"))
  local goodsBox, itemNameWnd, StackNumWnd, PriceNumWnd, IndateWnd, DisCWnd, DisWnd2, itemNameWnd1, itemNameWnd2, OrginPriceWnd = _RMBShop_GetChildWndByParentWnd(selItem)
  if goodsBox:GetItemIcon(0) ~= "" then
    _RMBWnd_GiftAndBuyRoot:SetVisible(true)
    parentWnd:SetVisible(true)
    buy_goodsBox:SetItemGoods(0, goodsBox:GetItemIcon(0), goodsBox:GetItemQuality(0))
    buy_goodsBox:SetItemData(0, goodsBox:GetItemData(0))
    buy_goodsBox:SetUserData64(goodsBox:GetUserData64())
    if itemNameWnd:GetProperty("Text") == "" then
      buy_ItemName:SetProperty("Text", itemNameWnd1:GetProperty("Text"))
    else
      buy_ItemName:SetProperty("Text", itemNameWnd:GetProperty("Text"))
    end
    buy_price:SetProperty("Text", PriceNumWnd:GetProperty("Text"))
    buy_editBox:SetProperty("Text", "1")
    buy_editBox:SetCustomUserData(0, tonumber(StackNumWnd:GetProperty("Text")))
    buy_Tprice:SetProperty("Text", buy_price:GetProperty("Text"))
  else
    _RMBShop_CloseAllChildWnd()
  end
end
function UI_RMBShop_ItemToolTip(msg)
  local goodsbox = WindowToGoodsBox(msg:get_window())
  if goodsbox:GetItemIcon(0) == "" then
    return
  end
  local itemID = goodsbox:GetItemData(0)
  Lua_Tip_Item(goodsbox:GetToolTipWnd(0), nil, nil, nil, nil, itemID)
end
function UI_RMBShop_BuyClose()
  _RMBShop_CloseAllChildWnd()
end
function GETShoppingChart()
  _RMBShop_CloseAllChildWnd()
  _RMBWnd_GiftAndBuyRoot:SetVisible(true)
  local ItemList
  ItemList = GETOrderInfos(_RMBShop_ShoppingChart_Type)
  local TotalPrices = 0
  local TotalItems = 0
  for i = 0, _RMBShop_ItemMax - 1 do
    _RMBWnd_CurrChart_List[i]:SetVisible(false)
  end
  for i = 1, #ItemList do
    local Choose = ItemList[i].Choose
    local Icon = ItemList[i].Icon
    local Name = ItemList[i].Name
    local Price = ItemList[i].Price
    local Number = ItemList[i].Number
    local GUID = ItemList[i].GUID
    local Quality = ItemList[i].Quality
    local ItemId = ItemList[i].ItemId
    if Price == 0 then
    else
      _RMBWnd_CurrChart_List[i]:GetGrandChild("Choose_cbtn"):SetProperty("Checked", tostring(Choose))
      WindowToGoodsBox(_RMBWnd_CurrChart_List[i]:GetGrandChild("GoodsBox1")):SetItemGoods(0, Icon, Quality)
      WindowToGoodsBox(_RMBWnd_CurrChart_List[i]:GetGrandChild("GoodsBox1")):SetItemData(0, ItemId)
      WindowToGoodsBox(_RMBWnd_CurrChart_List[i]:GetGrandChild("GoodsBox1")):SetCustomUserData(1, i)
      WindowToGoodsBox(_RMBWnd_CurrChart_List[i]:GetGrandChild("GoodsBox1")):SetUserData64(GUID)
      _RMBWnd_CurrChart_List[i]:GetGrandChild("Name_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", gUI_GOODS_QUALITY_COLOR_PREFIX[Quality], tostring(Name)))
      _RMBWnd_CurrChart_List[i]:GetGrandChild("Price_dlab"):SetProperty("Text", tostring(Price))
      _RMBWnd_CurrChart_List[i]:GetGrandChild("Count_bg.Count_bg.Count_ebox"):SetProperty("Text", tostring(Number))
      local totalPrice = tonumber(Number) * tonumber(Price)
      _RMBWnd_CurrChart_List[i]:GetGrandChild("SumPrice_dlab"):SetProperty("Text", tostring(totalPrice))
      _RMBWnd_CurrChart_List[i]:SetVisible(true)
      if tostring(Choose) == "true" then
        TotalItems = TotalItems + 1
        TotalPrices = TotalPrices + totalPrice
      end
    end
  end
  local len, Total = GETMaxOrderLen()
  _RMBWnd_CHARTNUM:SetProperty("Text", tostring(Total))
  _RMBWnd_SumItems:SetProperty("Text", tostring(TotalItems))
  _RMBWnd_SumPrices:SetProperty("Text", tostring(TotalPrices))
  AutoSizeOFChart(len)
end
function UI_GetMall_OverClick(msg)
  local count = GetItemCountByMallType(_RMBShop_ShoppingChart_Type)
  if count < 1 then
    Lua_Chat_ShowOSD("RMBSHOP_NOITEM")
  else
    Messagebox_Show("CIMFIMBUY", _RMBWnd_SumItems:GetProperty("Text"), _RMBSHOP_TYPENAME[_RMBShop_ShoppingChart_Type], _RMBWnd_SumPrices:GetProperty("Text"), _RMBShop_ShoppingChart_Type)
  end
end
function AutoSizeOFChart(len)
  if len == 0 then
    len = 1
  end
  local ChartBg = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBBuy")
  local ChartDownBg = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBBuy.Down_bg")
  local ChartImageBg = WindowSys_Instance:GetWindow("RMBShopGiftAndBuy.RMBBuy.Image_bg.Image_bg")
  ChartBg:SetHeight(p2u_y_proxy(200 + len * 30))
  ChartDownBg:SetTop(p2u_y_proxy(135 + len * 30))
  ChartImageBg:SetHeight(p2u_y_proxy(95 + len * 30))
end
function _OnRMBShop_Open()
  UI_RMBShop_Show()
end
function UI_RMBShop_Show()
  local _, _, _, _, level = GetPlaySelfProp(4)
  if level < 15 then
    Lua_Chat_ShowOSD("RMBSHOP_NEEDLEVEL")
    return
  end
  _RMBWnd_Root:SetVisible(not _RMBWnd_Root:IsVisible())
  if _RMBWnd_Root:IsVisible() then
    if _RMBWnd_Item_List[0] == nil then
      _RMBShop_CreateTmpWnd()
    end
    OpenRMBShopRequest()
    RMBRequestContent()
    ChangeRMBShopType(_RMBSHOP_TYPE.Diamond)
    _RMBWnd_Root:SetVisible(true)
    for i = 0, _RMBShop_ItemWnd_MaxNum - 1 do
      _RMBWnd_Item_List[i]:SetVisible(false)
    end
    for j = 0, _RMBShop_ItemMax - 1 do
      _RMBWnd_CurrChart_List[j]:SetVisible(false)
    end
    _RMBShop_ShowBox:show()
    _RMBWnd_AvatarWnd:SetVisible(true)
    UI_GetDef_Player()
  else
    _RMBWnd_Root:SetVisible(false)
    _RMBShop_CloseAllChildWnd()
    _RMBShop_ShowBox:hide()
    _RMBWnd_AvatarWnd:SetVisible(false)
  end
end
function Lua_RMBShop_QuickBuy(itemID)
  if _RMBWnd_Item_List[0] == nil then
    _RMBShop_CreateTmpWnd()
  end
  RMBOpenByIds(itemID)
  _RMBWnd_Root:SetVisible(true)
  _RMBShop_ShowBox:show()
  _RMBWnd_AvatarWnd:SetVisible(true)
  UI_GetDef_Player()
end
function UI_RMBShop_Close()
  _RMBWnd_Root:SetVisible(false)
  _RMBShop_CloseAllChildWnd()
  _RMBShop_ShowBox:hide()
end
function UI_RMBShop_TypeChange(msg)
  local wnd = msg:get_window()
  local _type = tonumber(wnd:GetProperty("CustomUserData"))
  if _type >= _RMBSHOP_TYPE.Diamond and _type <= _RMBSHOP_TYPE.Welfare then
    ChangeRMBShopType(_type)
  end
end
function UI_RMBShop_SelChange(msg)
  local tcWnd = WindowToTableCtrl(msg:get_window())
  if tcWnd then
    local selectItemID = tcWnd:GetSelectItem()
    local groupID = tcWnd:GetItemData(selectItemID)
    if groupID >= _RMBSHOP_GROUPIDMIN and groupID <= _RMBSHOP_GROUPIDMAX then
      _RMBShop_CurrSelGroup = groupID
      ChangeRMBShopGroup(_RMBShop_CurrSelType, groupID)
    end
  end
end
function UI_Close_Chart_Click(msg)
  _RMBWnd_NewGiftWnd:SetVisible(false)
  _RMBWnd_GiftAndBuyRoot:SetVisible(false)
end
function UI_RMBShopChart_SelMallInfo(msg)
  local wnd = WindowToTableCtrl(msg:get_window())
  local selectTab = wnd:GetSelectItem()
  if selectTab == 2 then
    selectTab = 1
  elseif selectTab == 1 then
    selectTab = 2
  end
  _RMBShop_ShoppingChart_Type = selectTab
  GETShoppingChart()
end
function _OnRMBShop_UpdateContent()
  RMBShop_SetContent()
end
function _OnRMBSHOP_AddGroupInfos(groupCount, g1, g2, g3, g4, g5, g6, g7, g8, g9, g10)
  local tableCrtl = WindowToTableCtrl(WindowSys_Instance:GetWindow("RMBShop.TableCtrl_tctl"))
  if groupCount > 0 then
    tableCrtl:SetProperty("ItemCount", tostring(groupCount))
  end
  local tablestr = ""
  local groupList = {
    g1,
    g2,
    g3,
    g4,
    g5,
    g6,
    g7,
    g8,
    g9,
    g10
  }
  for i = 1, groupCount do
    tablestr = tablestr .. groupList[i]
    if i ~= groupCount then
      tablestr = tablestr .. "|"
    end
    tableCrtl:SetItemData(i - 1, i - 1)
  end
  tableCrtl:SetProperty("ItemsText", tablestr)
end
function _OnRMBSHOP_SetCurrentSelInfos(MallType, groupId, currPage, totalPage)
  local PageNumWnd = WindowSys_Instance:GetWindow("RMBShop.Picture2_bg.Page_dlab")
  if currPage and totalPage > 0 then
    local strPage = string.format("%d/%d", currPage + 1, totalPage)
    PageNumWnd:SetProperty("Text", strPage)
  else
    local strPage = string.format("%d/%d", currPage, totalPage)
    PageNumWnd:SetProperty("Text", strPage)
  end
  local tableCrtl = WindowToTableCtrl(WindowSys_Instance:GetWindow("RMBShop.TableCtrl_tctl"))
  tableCrtl:SetSelectedState(groupId)
  _RMBShop_CurrSelType = MallType
  _RMBShop_SetTypeBtnEnable()
  _RMBShop_ClearAllPageItem()
  _RMBShop_ClearAllSelectItem()
  _RMBShop_SetEnableGiftBtn()
end
function UI_RMBShop_ChangePage(msg)
  local btn = msg:get_window()
  local btnName = btn:GetProperty("WindowName")
  if string.find(btnName, "Right") then
    ChangePage(true)
  else
    ChangePage(false)
  end
end
function UI_RMBShop_PageItemClick(msg)
  local itemWnd = msg:get_window()
  _RMBShop_SetSelectPageItem(itemWnd)
end
function UI_RMBShop_GoodsBoxClick(msg)
  local goodsbox = msg:get_window()
  _RMBShop_SetSelectPageItem(goodsbox:GetParent())
end
function UI_RMBShop_BuyBtnClick(msg)
  local btn = msg:get_window()
  _RMBShop_SetSelectPageItem(btn:GetParent())
  _RMBShop_CloseAllChildWnd()
  _RMBShop_InitBuyWindowNew(btn:GetParent(), btn)
end
function UI_RMBShop_GiftBtnClick(msg)
  local btn = msg:get_window()
  _RMBShop_SetSelectPageItem(btn:GetParent())
  _RMBShop_CloseAllChildWnd()
  _RMBShop_InitGiftWindow(btn:GetParent(), btn)
end
function UI_AddShopChart(msg)
  local btn = msg:get_window()
  _RMBShop_SetSelectPageItem(btn:GetParent())
  _RMBShop_CloseAllChildWnd()
  RMB_ADDTOCHART(btn:GetParent())
end
function UI_RMBChoose_Click(msg)
  if msg == nil then
    return
  end
  local CheckBtn = msg:get_window()
  CheckBtn = WindowToCheckButton(CheckBtn)
  local index
  if CheckBtn:IsChecked() then
    index = WindowToGoodsBox(CheckBtn:GetParent():GetGrandChild("GoodsBox1")):GetCustomUserData(1)
    SetRMBItemChoose(_RMBShop_ShoppingChart_Type, index, 0, 0)
  else
    index = WindowToGoodsBox(CheckBtn:GetParent():GetGrandChild("GoodsBox1")):GetCustomUserData(1)
    SetRMBItemChoose(_RMBShop_ShoppingChart_Type, index, 1, 0)
  end
  GETShoppingChart()
end
function UIRMB_ShopDelete_Click(msg)
  if msg == nil then
    return
  end
  local Btn = msg:get_window()
  local index = WindowToGoodsBox(Btn:GetParent():GetGrandChild("GoodsBox1")):GetCustomUserData(1)
  SetRMBItemChoose(_RMBShop_ShoppingChart_Type, index, 2, 0)
  local len, Total = GETMaxOrderLen()
  _RMBWnd_CHARTNUM:SetProperty("Text", tostring(Total))
  GETShoppingChart()
end
function RMB_ADDTOCHART(SelectItem)
  local goodsBox, itemNameWnd, StackNumWnd, PriceNumWnd, IndateWnd, DisCWnd, DisWnd2 = _RMBShop_GetChildWndByParentWnd(SelectItem)
  if goodsBox:GetItemIcon(0) ~= "" then
    local itemId = goodsBox:GetItemData(0)
    local itemId64 = goodsBox:GetUserData64()
    local itemcount = tonumber(StackNumWnd:GetProperty("Text"))
    ADDOrderRequest(itemId64, itemId, itemcount, _RMBShop_CurrSelType)
    local len, Total = GETMaxOrderLen()
    _RMBWnd_CHARTNUM:SetProperty("Text", tostring(Total))
  end
end
function UI_RMBShop_BuyPageItem(msg)
  local parent = msg:get_window():GetParent():GetParent():GetParent()
  if parent == nil then
    return
  end
  local buy_editBox = parent:GetGrandChild("Image_bg.Gift_Template.Count_bg.Count_bg.Count_ebox")
  local BuyNum = buy_editBox:GetProperty("Text")
  if BuyNum == "" then
    Lua_Chat_ShowOSD("RMBSHOP_NEEDNUM")
    return
  end
  local totalPriceWnd = parent:GetGrandChild("Image_bg.Gift_Template.SumPrice_dlab")
  local totalPrice = tonumber(totalPriceWnd:GetProperty("Text"))
  local CurrMoney = _RMBShop_GetMoneyInfo()
  if totalPrice > CurrMoney then
    Lua_Chat_ShowOSD("RMBSHOP_NEEDMONEY", _RMBSHOP_TYPENAME[_RMBShop_CurrSelType])
    UI_RMBShop_BuyClose()
    return
  end
  local buy_goodsBox = WindowToGoodsBox(parent:GetGrandChild("Image_bg.Gift_Template.GoodsBox1"))
  local itemID64 = buy_goodsBox:GetUserData64()
  local itemID = buy_goodsBox:GetItemData(0)
  local recvName = parent:GetGrandChild("Image_bg.Down_bg.Edit_bg.Gift_ebox"):GetProperty("Text")
  if recvName ~= "" then
    local myName = GetPlaySelfProp(6)
    if recvName == myName then
      Lua_Chat_ShowOSD("RMBSHOP_SELFNAME")
      return
    end
    local itemName = parent:GetGrandChild("Image_bg.Gift_Template.Name_dlab"):GetProperty("Text")
    Messagebox_Show("RMBSHOP_GIFT_CONFIRM", _RMBShop_CurrSelType, itemID64, itemID, tonumber(BuyNum), recvName, itemName)
  else
    Lua_Chat_ShowOSD("RMBSHOP_NEEDNAME")
    return
  end
  UI_RMBShop_BuyClose()
end
function UI_GameMall_BuyConfim(msg)
  local parent = msg:get_window():GetParent():GetParent():GetParent()
  if parent == nil then
    return
  end
  local buy_editBox = parent:GetGrandChild("Buy_bg.Image_bg.Count_bg.Count_ebox")
  local BuyNum = buy_editBox:GetProperty("Text")
  if BuyNum == "" or tonumber(BuyNum) < 1 then
    Lua_Chat_ShowOSD("RMBSHOP_NEEDNUM")
    return
  end
  local totalPriceWnd = parent:GetGrandChild("Buy_bg.Image_bg.Price_bg.AllPrice_dlab")
  local totalPrice = tonumber(totalPriceWnd:GetProperty("Text"))
  local CurrMoney = _RMBShop_GetMoneyInfo()
  if totalPrice > CurrMoney then
    Lua_Chat_ShowOSD("RMBSHOP_NEEDMONEY", _RMBSHOP_TYPENAME[_RMBShop_CurrSelType])
    UI_RMBShop_BuyClose()
    return
  end
  local buy_goodsBox = WindowToGoodsBox(parent:GetGrandChild("Buy_bg.Image_bg.Article_gbox"))
  local itemID64 = buy_goodsBox:GetUserData64()
  local itemID = buy_goodsBox:GetItemData(0)
  local itemName = parent:GetGrandChild("Buy_bg.Image_bg.Article_dlab"):GetProperty("Text")
  Messagebox_Show("CIMFIYEMBUYNEW", _RMBShop_CurrSelType, itemID64, itemID, tonumber(BuyNum), itemName)
  UI_RMBShop_BuyClose()
end
function UI_RMBShop_BuyNumAdd(msg)
  local parent = msg:get_window():GetParent():GetParent():GetParent():GetParent()
  if parent == nil then
    return
  end
  local buy_editBox = parent:GetGrandChild("Image_bg.Gift_Template.Count_bg.Count_bg.Count_ebox")
  local BuyNum = buy_editBox:GetProperty("Text")
  local MaxNum = buy_editBox:GetCustomUserData(0)
  if BuyNum == "" then
    return
  end
  BuyNum = tonumber(BuyNum)
  local goodsBox = WindowToGoodsBox(_RMBWnd_NewGiftWnd:GetGrandChild("Image_bg.Gift_Template.GoodsBox1"))
  local _, _, _, _, _, _, _, _, _, _, _, _, maxCount = GetItemInfoBySlot(-1, goodsBox:GetItemData(0), nil)
  MaxNum = 99
  if BuyNum and BuyNum < MaxNum then
    BuyNum = BuyNum + 1
    buy_editBox:SetProperty("Text", tostring(BuyNum))
    _RMBShop_TotalPrice(BuyNum, parent)
  end
end
function UI_RMBShop_BuyNumAddNew(msg)
  local parent = msg:get_window():GetParent():GetParent():GetParent():GetParent()
  if parent == nil then
    return
  end
  local buy_editBox = parent:GetGrandChild("Buy_bg.Image_bg.Count_bg.Count_ebox")
  local BuyNum = buy_editBox:GetProperty("Text")
  local MaxNum = buy_editBox:GetCustomUserData(0)
  if BuyNum == "" then
    return
  end
  BuyNum = tonumber(BuyNum)
  local goodsBox = WindowToGoodsBox(_RMBWnd_BuyWnd_New:GetGrandChild("Buy_bg.Image_bg.Article_gbox"))
  local _, _, _, _, _, _, _, _, _, _, _, _, maxCount = GetItemInfoBySlot(-1, goodsBox:GetItemData(0), nil)
  MaxNum = 99
  if BuyNum and BuyNum < MaxNum then
    BuyNum = BuyNum + 1
    buy_editBox:SetProperty("Text", tostring(BuyNum))
    _RMBShop_TotalPriceNew(BuyNum, parent)
  end
end
function UI_RMBShop_ChangeSumPrice(msg)
  local parent = msg:get_window():GetParent():GetParent():GetParent():GetParent()
  if parent == nil then
    return
  end
  local buy_editBox = parent:GetGrandChild("Buy_bg.Image_bg.Count_bg.Count_ebox")
  local BuyNum = buy_editBox:GetProperty("Text")
  local MaxNum = buy_editBox:GetCustomUserData(0)
  if BuyNum == "" then
    return
  end
  BuyNum = tonumber(BuyNum)
  MaxNum = 99
  if BuyNum and BuyNum <= MaxNum then
    buy_editBox:SetProperty("Text", tostring(BuyNum))
    _RMBShop_TotalPriceNew(BuyNum, parent)
  end
end
function UI_RMBShop_BuyNumAdd1(msg)
  local parent = msg:get_window():GetParent():GetParent()
  if parent == nil then
    return
  end
  local buy_editBox = parent:GetGrandChild("Count_bg.Count_bg.Count_ebox")
  local BuyNum = buy_editBox:GetProperty("Text")
  local MaxNum = buy_editBox:GetCustomUserData(0)
  if BuyNum == "" then
    return
  end
  BuyNum = tonumber(BuyNum)
  local goodsBox = WindowToGoodsBox(parent:GetGrandChild("GoodsBox1"))
  local _, _, _, _, _, _, _, _, _, _, _, _, maxCount = GetItemInfoBySlot(-1, goodsBox:GetItemData(0), nil)
  local EmptyNum = GetEmptySlotNum()
  MaxNum = math.floor(EmptyNum * maxCount / MaxNum)
  if MaxNum > 99 then
    MaxNum = 99
  end
  if BuyNum and BuyNum < MaxNum then
    BuyNum = BuyNum + 1
    buy_editBox:SetProperty("Text", tostring(BuyNum))
    _RMBShop_TotalPrice1(BuyNum, parent)
    local index = WindowToGoodsBox(parent:GetGrandChild("GoodsBox1")):GetCustomUserData(1)
    SetRMBItemChoose(_RMBShop_ShoppingChart_Type, index, 3, BuyNum)
    GETShoppingChart()
  end
end
function UI_RMBShop_BuyNumDec(msg)
  local parent = msg:get_window():GetParent():GetParent():GetParent():GetParent()
  if parent == nil then
    return
  end
  local buy_editBox = parent:GetGrandChild("Image_bg.Gift_Template.Count_bg.Count_bg.Count_ebox")
  local BuyNum = buy_editBox:GetProperty("Text")
  if BuyNum == "" then
    return
  end
  BuyNum = tonumber(BuyNum)
  if BuyNum and BuyNum > 1 then
    BuyNum = BuyNum - 1
    buy_editBox:SetProperty("Text", tostring(BuyNum))
    _RMBShop_TotalPrice(BuyNum, parent)
  end
end
function UI_RMBShop_BuyNumDecNew(msg)
  local parent = msg:get_window():GetParent():GetParent():GetParent():GetParent()
  if parent == nil then
    return
  end
  local buy_editBox = parent:GetGrandChild("Buy_bg.Image_bg.Count_bg.Count_ebox")
  local BuyNum = buy_editBox:GetProperty("Text")
  if BuyNum == "" then
    return
  end
  BuyNum = tonumber(BuyNum)
  if BuyNum and BuyNum > 1 then
    BuyNum = BuyNum - 1
    buy_editBox:SetProperty("Text", tostring(BuyNum))
    _RMBShop_TotalPriceNew(BuyNum, parent)
  end
end
function UI_RMBShop_BuyNumDec1(msg)
  local parent = msg:get_window():GetParent():GetParent()
  if parent == nil then
    return
  end
  local buy_editBox = parent:GetGrandChild("Count_bg.Count_bg.Count_ebox")
  local BuyNum = buy_editBox:GetProperty("Text")
  if BuyNum == "" then
    return
  end
  BuyNum = tonumber(BuyNum)
  if BuyNum and BuyNum > 1 then
    BuyNum = BuyNum - 1
    buy_editBox:SetProperty("Text", tostring(BuyNum))
    _RMBShop_TotalPrice1(BuyNum, parent)
    local index = WindowToGoodsBox(parent:GetGrandChild("GoodsBox1")):GetCustomUserData(1)
    SetRMBItemChoose(_RMBShop_ShoppingChart_Type, index, 3, BuyNum)
    GETShoppingChart()
  end
end
function _RMBShop_TotalPrice(buyNum, parent)
  local buy_SalePrice = parent:GetGrandChild("Image_bg.Gift_Template.Price_dlab")
  local price = buy_SalePrice:GetProperty("Text")
  if price == "" then
    return
  end
  price = tonumber(price)
  local totalPrice = buyNum * price
  local totalPriceWnd = parent:GetGrandChild("Image_bg.Gift_Template.SumPrice_dlab")
  totalPriceWnd:SetProperty("Text", tostring(totalPrice))
end
function _RMBShop_TotalPriceNew(buyNum, parent)
  local buy_SalePrice = parent:GetGrandChild("Buy_bg.Image_bg.Price_bg.Priec_dlab")
  local price = buy_SalePrice:GetProperty("Text")
  if price == "" then
    return
  end
  price = tonumber(price)
  local totalPrice = buyNum * price
  local totalPriceWnd = parent:GetGrandChild("Buy_bg.Image_bg.Price_bg.AllPrice_dlab")
  totalPriceWnd:SetProperty("Text", tostring(totalPrice))
end
function _RMBShop_TotalPrice1(buyNum, parent)
  local buy_SalePrice = parent:GetGrandChild("Price_dlab")
  local price = buy_SalePrice:GetProperty("Text")
  if price == "" then
    return
  end
  price = tonumber(price)
  local totalPrice = buyNum * price
  local totalPriceWnd = parent:GetGrandChild("SumPrice_dlab")
  totalPriceWnd:SetProperty("Text", tostring(totalPrice))
end
function _OnRMBShop_UpdateMoney(DiamondNum, CrystalNum, WelfareNum)
  _RMBWnd_DiamondNum:SetProperty("Text", tostring(DiamondNum))
  _RMBWnd_WelfareNum:SetProperty("Text", tostring(WelfareNum))
  _RMBWnd_CrystalNum:SetProperty("Text", tostring(CrystalNum))
  _Shop_Update_CurrWelfareNum(WelfareNum)
end
function _OnRMBShop_AllInfosUpdate(Index, Guid, TableId, Number, salePrice, VaildTime, Discount, Falg, Limit, LimitBeginTime, LimitEndTime, iconName, ItemName, Quality, Select)
  if Index >= _RMBShop_ItemWnd_MaxNum then
    return
  end
  local times = GetServerTime()
  if iconName then
    local goodsBox, itemNameWnd, StackNumWnd, PriceNumWnd, IndateWnd, DisCWnd, DisWnd2, itemNameWnd1, itemNameWnd2, OrginPriceWnd, OrginLine = _RMBShop_GetChildWndByParentWnd(_RMBWnd_Item_List[Index])
    _RMBWnd_Item_List[Index]:SetVisible(true)
    WindowToButton(_RMBWnd_Item_List[Index]:GetGrandChild("Shop_btn")):SetVisible(false)
    WindowToButton(_RMBWnd_Item_List[Index]:GetGrandChild("Buy_btn")):SetProperty("Enable", "false")
    goodsBox:SetItemGoods(0, iconName, Quality)
    goodsBox:SetItemData(0, TableId)
    goodsBox:SetCustomUserData(1, Limit)
    goodsBox:SetCustomUserData(2, LimitBeginTime)
    goodsBox:SetCustomUserData(3, LimitEndTime)
    goodsBox:SetUserData64(Guid)
    StackNumWnd:SetProperty("Text", tostring(Number))
    PriceNumWnd:SetProperty("Text", tostring(salePrice))
    OrginPriceWnd:SetProperty("Text", tostring(Discount))
    OrginLine:SetVisible(Discount ~= salePrice)
    OrginLine:SetWidth(WindowToLabel(OrginPriceWnd):GetTextLength())
    if Select == "true" or Select == true then
      _RMBShop_ClearAllSelectItem()
      local frameWnd = _RMBWnd_Item_List[Index]:GetGrandChild("Picture2_bg")
      frameWnd:SetVisible(true)
    end
    local str_des = ""
    str_des = string.format("{#C^%s^%s}", gUI_GOODS_QUALITY_COLOR_PREFIX[Quality], ItemName)
    if VaildTime == "-1" or VaildTime == -1 then
      local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, _, equipKind, offlineTime, isTimeLimit, EquipPoint = GetItemInfoBySlot(-1, TableId, nil)
      if isTimeLimit then
        if leftEffTime > 0 then
          if offlineTime ~= "" then
            str_des = str_des .. string.format("{#R^(%s)}", "有效期至 " .. tostring(offlineTime))
          else
            local des = string.format(" $CT(128,%d,%d,%d)$", -1, TableId, 0)
            if des == "" then
            else
              str_des = str_des .. string.format("{#R^(%s)}", des)
            end
          end
        else
          str_des = str_des .. string.format("{#R^(%s)}", "已过期")
          elseif VaildTime > 0 then
            str_des = str_des .. string.format("{#R^(限时使用:%s)}", tostring(Lua_UI_DetailTime(tonumber(VaildTime) * 60 * 60)))
          end
        end
      else
      end
    itemNameWnd1:SetProperty("Text", str_des)
    if _RMBSHOP_ITEMTYPE[Falg] ~= nil then
      DisCWnd:SetVisible(true)
      DisCWnd:SetProperty("BackImage", _RMBSHOP_ITEMTYPE[Falg])
    end
    if Limit == "0" or Limit == "false" or Limit == 0 then
      WindowToButton(_RMBWnd_Item_List[Index]:GetGrandChild("Shop_btn")):SetVisible(false)
      WindowToButton(_RMBWnd_Item_List[Index]:GetGrandChild("Buy_btn")):SetProperty("Enable", "true")
      _RMBWnd_Item_List[Index]:GetGrandChild("ItemIndate_dlab"):SetVisible(false)
      _RMBWnd_Item_List[Index]:GetGrandChild("ItemIndate_slab"):SetVisible(false)
    else
      local str_des = ""
      if LimitBeginTime > times then
        local str_desNew = string.format("{#R^%s}", "距开始时间:" .. Lua_UI_DetailTime(tonumber(LimitBeginTime) - times))
        str_des = str_des .. tostring(str_desNew)
      elseif LimitEndTime < times then
        local str_desNew = string.format("{#R^%s}", "已结束")
        str_des = str_des .. tostring(str_desNew)
      elseif LimitEndTime > times and LimitBeginTime < times then
        WindowToButton(_RMBWnd_Item_List[Index]:GetGrandChild("Shop_btn")):SetVisible(false)
        WindowToButton(_RMBWnd_Item_List[Index]:GetGrandChild("Buy_btn")):SetProperty("Enable", "true")
        local str_desNew = string.format("{#R^%s}", Lua_UI_DetailTime(tonumber(LimitEndTime) - times))
        str_des = str_des .. tostring(str_desNew)
      end
      _RMBWnd_Item_List[Index]:GetGrandChild("ItemIndate_dlab"):SetVisible(true)
      _RMBWnd_Item_List[Index]:GetGrandChild("ItemIndate_slab"):SetVisible(true)
      IndateWnd:SetProperty("Text", str_des)
    end
  end
end
function _RMBShop_UpdateWndVisiable()
  _RMBWnd_DiamondNumParent:SetProperty("Visible", "false")
  _RMBWnd_WelfareNumParent:SetProperty("Visible", "false")
  _RMBWnd_CrystalNumParent:SetProperty("Visible", "false")
end
function UI_RMBShop_OpenFriendList()
  _RMBWnd_FriendListRoot:SetVisible(true)
  local friendList = WindowToListBox(_RMBWnd_FriendListRoot:GetGrandChild("Namedi_bg.Name_lbox"))
  friendList:RemoveAllItems()
  local list = GetFriendName()
  for index, value in pairs(list) do
    local name = value.name
    if name then
      local item = friendList:InsertString(name, -1)
      item:SetUserData(index)
    end
  end
  friendList:SetSelectItem(0)
end
function UI_RMBShop_GetFriendName()
  local friendList = WindowToListBox(_RMBWnd_FriendListRoot:GetGrandChild("Namedi_bg.Name_lbox"))
  local index = friendList:GetSelectedItemIndex()
  if index >= 0 then
    local name = friendList:GetItemText(index, 0)
    local FriendName_eBox = _RMBWnd_NewGiftWnd:GetGrandChild("Image_bg.Down_bg.Edit_bg.Gift_ebox")
    FriendName_eBox:SetProperty("Text", name)
    UI_RMBShop_CloseFriendList()
  end
end
function UI_RMBShop_GetFriendName1()
  local friendList = WindowToListBox(_RMBWnd_FriendListRoot:GetGrandChild("Namedi_bg.Name_lbox"))
  local index = friendList:GetSelectedItemIndex()
  if index >= 0 then
    local name = friendList:GetItemText(index, 0)
    local FriendName_eBox = _RMBWnd_NewGiftWnd:GetGrandChild("Image_bg.Down_bg.Edit_bg.Gift_ebox")
    FriendName_eBox:SetProperty("Text", name)
  end
end
function UI_RMBShop_CloseFriendList()
  _RMBWnd_FriendListRoot:SetVisible(false)
end
function UI_RMBShop_OpenWebRMB()
  local WebRecharge = WindowSys_Instance:GetWindow("WebRecharge")
  WebRecharge:SetVisible(true)
  UIInterface:CreateBrowser()
end
function UI_RMBShop_CloseWebRMB()
  local WebRecharge = WindowSys_Instance:GetWindow("WebRecharge")
  WebRecharge:SetVisible(false)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_RMBShop_ModelLeftRotate()
  AddTimerEvent(2, _RMBSHOP_AVATAROTATE, function()
    _RMBShop_ShowBox:onCharacterLeft()
  end)
end
function UI_RMBShop_ModelRightRotate()
  AddTimerEvent(2, _RMBSHOP_AVATAROTATE, function()
    _RMBShop_ShowBox:onCharacterRight()
  end)
end
function UI_RMBShop_ModelStopRotate()
  DelTimerEvent(2, _RMBSHOP_AVATAROTATE)
end
function UI_GetDef_Player(msg)
  _RMBWnd_AvatarWnd:SetVisible(true)
  _RMBShop_ShowBox:revertCamera()
  _RMBShop_ShowBox:createMallAvatar(0)
  _RMBShop_ShowBox:setAvatarScale(0.9)
end
function UI_RMBShop_Refresh(msg)
  OpenRMBShopRequest()
  RMBRequestContent()
  local btn = msg:get_window()
  btn:SetProperty("Text", "&30&")
  btn:SetProperty("Enable", "false")
end
function UI_RMBShop_TimeOver(msg)
  local btn = msg:get_window()
  btn:SetProperty("Text", "")
  btn:SetProperty("Enable", "true")
end
function UI_Square_Info(msg)
  GETShoppingChart()
end
function _OnRMBShop_UpdateTime(times)
  if _RMBWnd_Root:IsVisible() then
    for i = 0, #_RMBWnd_Item_List do
      local goodsBox, itemNameWnd, StackNumWnd, PriceNumWnd, IndateWnd, DisCWnd, DisWnd2, itemNameWnd1, itemNameWnd2, OrginPriceWnd = _RMBShop_GetChildWndByParentWnd(_RMBWnd_Item_List[i])
      if _RMBWnd_Item_List[i]:IsVisible() then
        local Limit = goodsBox:GetCustomUserData(1)
        local LimitBeginTime = goodsBox:GetCustomUserData(2)
        local LimitEndTime = goodsBox:GetCustomUserData(3)
        if Limit ~= 0 then
          if times < LimitBeginTime then
            itemNameWnd2:SetProperty("Text", "距开始时间:" .. Lua_UI_DetailTime(tonumber(LimitBeginTime) - times))
          elseif times > LimitEndTime then
            itemNameWnd2:SetProperty("Text", "已结束")
          elseif times < LimitEndTime and times > LimitBeginTime then
            itemNameWnd2:SetProperty("Text", "倒计时:" .. Lua_UI_DetailTime(tonumber(LimitEndTime) - times))
          end
        end
      end
    end
  end
end
function _OnRMBShop_UpdateShowChart(Types)
  GETShoppingChart()
end
function _OnRMBShop_CloseWin()
  _RMBWnd_Root:SetVisible(false)
end
function Script_RMBShop_OnLoad()
  _RMBShop_Init()
end
function Script_RMBShop_OnEvent(event)
  if event == "MALL_ADDGROUPINFOS" then
    _OnRMBSHOP_AddGroupInfos(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11)
  elseif event == "MALL_UPDATECURSEL" then
    _OnRMBSHOP_SetCurrentSelInfos(arg1, arg2, arg3, arg4)
  elseif event == "MALL_SETMONEY" then
    _OnRMBShop_UpdateMoney(arg1, arg2, arg3)
  elseif event == "MALL_SETITEM" then
    _OnRMBShop_AllInfosUpdate(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15)
  elseif event == "MALL_TIMEUPDATE" then
    _OnRMBShop_UpdateTime(arg1)
  elseif event == "MALL_UPDATESHOWCHART" then
    _OnRMBShop_UpdateShowChart(arg1)
  elseif event == "MALL_UPDATESHOWCONTENT" then
    _OnRMBShop_UpdateContent()
  elseif event == "MALL_OPENMALL" then
    _OnRMBShop_Open()
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    _OnRMBShop_CloseWin()
  end
end
pdate(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15)
  elseif event == "MALL_TIMEUPDATE" then
    _OnRMBShop_UpdateTime(arg1)
  elseif event == "MALL_UPDATESHOWCHART" then
    _OnRMBShop_UpdateShowChart(arg1)
  elseif event == "MALL_UPDATESHOWCONTENT" then
    _OnRMBShop_UpdateContent()
  elseif event == "MALL_OPENMALL" then
    _OnRMBShop_Open()
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    _OnRMBShop_CloseWin()
  end
end
