if gUI and not gUI.Safety then
  gUI.Safety = {}
end
function _Safety_Show()
  gUI.Safety.Root:SetProperty("Visible", "true")
end
local checkBtnCount = 0
local FirstSelect, SecondSel
function UI_Safety_TimerStop()
  if gUI.Safety.Root then
    gUI.Safety.Root:SetProperty("Visible", "false")
  end
end
function UI_Safety_OkBtnClick()
  Lua_Safety_Close()
  if gUI.Safety.ModeId == 0 then
    local checkedCount = 0
    for i = 1, 8 do
      if gUI.Safety.PatternCheckBtn[i]:IsChecked() then
        checkedCount = checkedCount + 1
      end
    end
    local low = -1
    local high = -1
    if checkedCount == 2 then
      for i = 1, 8 do
        if gUI.Safety.PatternCheckBtn[i]:IsChecked() then
          if low == -1 then
            low = i - 1
          else
            high = i - 1
          end
        end
      end
    end
    ImageCheckAnswerQuestion(0, low, high)
  elseif gUI.Safety.ModeId == 2 then
    local strVer = gUI.Safety.EditBox:GetProperty("Text")
    ImageCheckAnswerQuestion(2, strVer)
  elseif gUI.Safety.ModeId == 3 then
    for i = 1, 8 do
      if gUI.Safety.QuesCheckBtn[i]:IsChecked() then
        ImageCheckAnswerQuestion(3, i)
        return
      end
    end
  end
end
function UI_Safety_RefreshClick()
  ImageCheckRefreshQuestion()
end
function UI_Safety_PatternBtnChecked(msg)
  local wnd1 = msg:get_window()
  local SelectItemIndex = wnd1:GetCustomUserData(1)
  local curName = wnd1:GetProperty("WindowName")
  local tempSelect
  if WindowToCheckButton(gUI.Safety.PatternCheckBtn[SelectItemIndex]):IsChecked() then
    if FirstSelect == 0 then
      FirstSelect = SelectItemIndex
    elseif SecondSel == 0 then
      SecondSel = SelectItemIndex
    end
    if checkBtnCount >= 2 then
      tempSelect = FirstSelect
      FirstSelect = SecondSel
      SecondSel = SelectItemIndex
      gUI.Safety.PatternCheckBtn[tempSelect]:SetCheckedState(false)
      checkBtnCount = checkBtnCount - 1
    end
    checkBtnCount = checkBtnCount + 1
  else
    if SelectItemIndex == FirstSelect then
      if SecondSel ~= 0 then
        FirstSelect = SecondSel
      else
        FirstSelect = 0
      end
    elseif SelectItemIndex == SecondSel then
      SecondSel = 0
    end
    checkBtnCount = checkBtnCount - 1
  end
  if checkBtnCount == 2 then
    gUI.Safety.ConfirmBtn:SetProperty("Enable", "true")
  else
    gUI.Safety.ConfirmBtn:SetProperty("Enable", "false")
  end
end
function UI_Safety_BtnChecked(msg)
  if msg == nil then
    return
  end
  local wnd = msg:get_window()
  wnd = WindowToCheckButton(wnd)
  local curName = wnd:GetProperty("WindowName")
  if wnd:IsChecked() then
    for i = 1, 4 do
      local wndName = gUI.Safety.QuesCheckBtn[i]:GetProperty("WindowName")
      if wndName ~= curName then
        gUI.Safety.QuesCheckBtn[i]:SetCheckedState(false)
      end
    end
  else
    wnd:SetCheckedState(true)
  end
end
function _OnSafety_Start(mode, countTime, anws1, anws2, anws3, anws4, question)
  gUI.Safety.ModeId = mode
  if mode == 0 then
    for i = 1, 8 do
      gUI.Safety.PatternCheckBtn[i]:SetCheckedState(false)
    end
    checkBtnCount = 0
    gUI.Safety.PatternMode:SetProperty("Visible", "true")
    gUI.Safety.VeriMode:SetProperty("Visible", "false")
    gUI.Safety.QuestionMode:SetProperty("Visible", "false")
    gUI.Safety.ConfirmBtn:SetProperty("Enable", "false")
    FirstSelect = 0
    SecondSel = 0
  elseif mode == 1 then
  elseif mode == 2 then
    WindowSys_Instance:SetKeyboardCaptureWindow(gUI.Safety.EditBox)
    gUI.Safety.EditBox:SetProperty("Text", "")
    gUI.Safety.PatternMode:SetProperty("Visible", "false")
    gUI.Safety.VeriMode:SetProperty("Visible", "true")
    gUI.Safety.QuestionMode:SetProperty("Visible", "false")
    gUI.Safety.ConfirmBtn:SetProperty("Enable", "true")
  elseif mode == 3 then
    gUI.Safety.ConfirmBtn:SetProperty("Enable", "true")
    for i = 1, 4 do
      if i == 1 then
        gUI.Safety.QuesCheckBtn[i]:SetCheckedState(true)
      else
        gUI.Safety.QuesCheckBtn[i]:SetCheckedState(false)
      end
    end
    local answers = {
      [1] = anws1,
      [2] = anws2,
      [3] = anws3,
      [4] = anws4
    }
    for i = 1, 4 do
      if answers[i] and 0 < string.len(answers[i]) then
        gUI.Safety.QuesLable[i]:SetProperty("Visible", "true")
        gUI.Safety.QuesCheckBtn[i]:SetProperty("Visible", "true")
        gUI.Safety.QuesLable[i]:SetProperty("Text", answers[i])
      else
        gUI.Safety.QuesCheckBtn[i]:SetProperty("Visible", "false")
        gUI.Safety.QuesLable[i]:SetProperty("Visible", "false")
      end
    end
    gUI.Safety.QuestLable:SetProperty("Text", question)
    gUI.Safety.PatternMode:SetProperty("Visible", "false")
    gUI.Safety.VeriMode:SetProperty("Visible", "false")
    gUI.Safety.QuestionMode:SetProperty("Visible", "true")
  end
  if countTime > 0 then
    gUI.Safety.CountDownLab:SetProperty("Text", "&" .. countTime / 1000 .. "&")
  end
  _Safety_Show()
end
function Lua_Safety_Close()
  local win = WindowSys_Instance:GetRootWindow()
  WindowSys_Instance:SetFocusWindow(win)
  WindowSys_Instance:SetKeyboardCaptureWindow(win)
  gUI.Safety.Root:SetProperty("Visible", "false")
end
function Script_Safety_OnEvent(event)
  if event == "IMAGECHECK_SHOW" then
    _OnSafety_Start(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  end
end
function Script_Safety_OnLoad()
  gUI.Safety.Root = WindowSys_Instance:GetWindow("Safety")
  gUI.Safety.ModeId = -1
  gUI.Safety.PatternMode = WindowSys_Instance:GetWindow("Safety.Picture_bg.Pattern_bg")
  gUI.Safety.VeriMode = WindowSys_Instance:GetWindow("Safety.Picture_bg.Input_bg")
  gUI.Safety.QuestionMode = WindowSys_Instance:GetWindow("Safety.Picture_bg.Choose_bg")
  gUI.Safety.EditBox = WindowSys_Instance:GetWindow("Safety.Picture_bg.Input_bg.Input_bg.Input_ebox")
  gUI.Safety.ConfirmBtn = WindowSys_Instance:GetWindow("Safety.Picture_bg.Confirm_btn")
  local veriPicWnd = WindowSys_Instance:GetWindow("Safety.Picture_bg.Input_bg.Picture1_pic")
  UIInterface:bindFanGuaWindow(0, veriPicWnd)
  gUI.Safety.PatternCheckBtn = {}
  for i = 1, 8 do
    local wndName = string.format("Safety.Picture_bg.Pattern_bg.Channel%d_pic.Channel%d_cbtn", i, i)
    gUI.Safety.PatternCheckBtn[i] = WindowToCheckButton(WindowSys_Instance:GetWindow(wndName))
    wndName = string.format("Safety.Picture_bg.Pattern_bg.Channel%d_pic", i)
    local tempWnd = WindowSys_Instance:GetWindow(wndName)
    UIInterface:bindFanGuaWindow(i, tempWnd)
    gUI.Safety.PatternCheckBtn[i]:SetCustomUserData(1, i)
  end
  gUI.Safety.QuesCheckBtn = {}
  gUI.Safety.QuesLable = {}
  for i = 1, 4 do
    local wndName = string.format("Safety.Picture_bg.Choose_bg.CheckBtn0%d_cbtn", i)
    gUI.Safety.QuesCheckBtn[i] = WindowToCheckButton(WindowSys_Instance:GetWindow(wndName))
    wndName = string.format("Safety.Picture_bg.Choose_bg.CheckName%d_dlab", i)
    gUI.Safety.QuesLable[i] = WindowSys_Instance:GetWindow(wndName)
  end
  gUI.Safety.CountDownLab = WindowSys_Instance:GetWindow("Safety.Picture_bg.Time_dlab")
  gUI.Safety.QuestLable = WindowSys_Instance:GetWindow("Safety.Picture_bg.Choose_bg.TitleTip_slab")
end
