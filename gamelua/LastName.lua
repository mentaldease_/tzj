module("Names")
LAST_NAME = {
  "赵",
  "钱",
  "孙",
  "李",
  "周",
  "吴",
  "郑",
  "王",
  "冯",
  "陈",
  "楮",
  "卫",
  "蒋",
  "沈",
  "韩",
  "杨",
  "朱",
  "秦",
  "尤",
  "许",
  "何",
  "吕",
  "施",
  "张",
  "孔",
  "曹",
  "严",
  "华",
  "金",
  "魏",
  "陶",
  "姜",
  "戚",
  "谢",
  "邹",
  "喻",
  "柏",
  "水",
  "窦",
  "章",
  "云",
  "苏",
  "潘",
  "葛",
  "奚",
  "范",
  "彭",
  "郎",
  "鲁",
  "韦",
  "昌",
  "马",
  "苗",
  "凤",
  "花",
  "方",
  "俞",
  "任",
  "袁",
  "柳",
  "酆",
  "鲍",
  "史",
  "唐",
  "费",
  "廉",
  "岑",
  "薛",
  "雷",
  "贺",
  "倪",
  "汤",
  "滕",
  "殷",
  "罗",
  "毕",
  "郝",
  "邬",
  "安",
  "常",
  "乐",
  "于",
  "时",
  "傅",
  "皮",
  "卞",
  "齐",
  "康",
  "伍",
  "余",
  "元",
  "卜",
  "顾",
  "孟",
  "平",
  "黄",
  "和",
  "穆",
  "萧",
  "尹",
  "姚",
  "邵",
  "湛",
  "汪",
  "祁",
  "毛",
  "禹",
  "狄",
  "米",
  "贝",
  "明",
  "臧",
  "计",
  "伏",
  "成",
  "戴",
  "谈",
  "宋",
  "茅",
  "庞",
  "熊",
  "纪",
  "舒",
  "屈",
  "项",
  "祝",
  "董",
  "梁",
  "杜",
  "阮",
  "蓝",
  "闽",
  "席",
  "季",
  "麻",
  "强",
  "贾",
  "路",
  "娄",
  "危",
  "江",
  "童",
  "颜",
  "郭",
  "梅",
  "盛",
  "林",
  "刁",
  "锺",
  "徐",
  "丘",
  "骆",
  "高",
  "夏",
  "蔡",
  "田",
  "樊",
  "胡",
  "凌",
  "霍",
  "虞",
  "万",
  "支",
  "柯",
  "昝",
  "管",
  "卢",
  "莫",
  "经",
  "房",
  "裘",
  "缪",
  "干",
  "解",
  "应",
  "宗",
  "丁",
  "宣",
  "贲",
  "邓",
  "郁",
  "单",
  "杭",
  "洪",
  "包",
  "诸",
  "左",
  "石",
  "崔",
  "吉",
  "钮",
  "龚",
  "程",
  "嵇",
  "邢",
  "滑",
  "裴",
  "陆",
  "荣",
  "翁",
  "荀",
  "羊",
  "於",
  "惠",
  "甄",
  "麹",
  "家",
  "封",
  "芮",
  "羿",
  "储",
  "靳",
  "汲",
  "邴",
  "糜",
  "松",
  "井",
  "段",
  "富",
  "巫",
  "乌",
  "焦",
  "巴",
  "弓",
  "牧",
  "隗",
  "山",
  "谷",
  "车",
  "侯",
  "宓",
  "蓬",
  "全",
  "郗",
  "班",
  "仰",
  "秋",
  "仲",
  "伊",
  "宫",
  "宁",
  "仇",
  "栾",
  "暴",
  "甘",
  "斜",
  "厉",
  "戎",
  "祖",
  "武",
  "符",
  "刘",
  "景",
  "詹",
  "束",
  "龙",
  "叶",
  "幸",
  "司",
  "韶",
  "郜",
  "黎",
  "蓟",
  "薄",
  "印",
  "宿",
  "白",
  "怀",
  "蒲",
  "邰",
  "从",
  "鄂",
  "索",
  "咸",
  "籍",
  "赖",
  "卓",
  "蔺",
  "屠",
  "蒙",
  "池",
  "乔",
  "阴",
  "胥",
  "能",
  "苍",
  "双",
  "闻",
  "莘",
  "党",
  "翟",
  "谭",
  "贡",
  "劳",
  "逄",
  "姬",
  "申",
  "扶",
  "堵",
  "冉",
  "宰",
  "郦",
  "雍",
  "郤",
  "璩",
  "桑",
  "桂",
  "濮",
  "牛",
  "寿",
  "通",
  "边",
  "扈",
  "燕",
  "冀",
  "郏",
  "浦",
  "尚",
  "农",
  "温",
  "别",
  "庄",
  "晏",
  "柴",
  "瞿",
  "阎",
  "充",
  "慕",
  "连",
  "茹",
  "习",
  "宦",
  "艾",
  "鱼",
  "容",
  "向",
  "古",
  "易",
  "慎",
  "戈",
  "廖",
  "庾",
  "终",
  "暨",
  "居",
  "衡",
  "步",
  "都",
  "耿",
  "满",
  "弘",
  "匡",
  "国",
  "文",
  "寇",
  "广",
  "禄",
  "阙",
  "东",
  "欧",
  "殳",
  "沃",
  "利",
  "蔚",
  "越",
  "夔",
  "隆",
  "师",
  "巩",
  "厍",
  "聂",
  "晁",
  "勾",
  "敖",
  "融",
  "冷",
  "訾",
  "辛",
  "阚",
  "那",
  "简",
  "饶",
  "空",
  "曾",
  "毋",
  "沙",
  "乜",
  "养",
  "鞠",
  "须",
  "丰",
  "巢",
  "关",
  "蒯",
  "相",
  "查",
  "后",
  "荆",
  "红",
  "游",
  "竺",
  "权",
  "逑",
  "盖",
  "益",
  "桓",
  "公",
  "逮",
  "盍",
  "万俟",
  "司马",
  "上官",
  "欧阳",
  "夏侯",
  "诸葛",
  "闻人",
  "东方",
  "赫连",
  "皇甫",
  "尉迟",
  "公羊",
  "澹台",
  "公冶",
  "宗政",
  "濮阳",
  "淳于",
  "仲孙",
  "太叔",
  "申屠",
  "公孙",
  "乐正",
  "轩辕",
  "令狐",
  "钟离",
  "闾丘",
  "长孙",
  "慕容",
  "鲜于",
  "宇文",
  "司徒",
  "司空",
  "亓官",
  "司寇",
  "仉督",
  "子车",
  "颛孙",
  "端木",
  "巫马",
  "公西",
  "漆雕",
  "壤驷",
  "公良",
  "拓拔",
  "夹谷",
  "宰父",
  "谷粱",
  "晋楚",
  "闫法",
  "汝鄢",
  "涂钦",
  "段干",
  "百里",
  "东郭",
  "南门",
  "呼延",
  "妫海",
  "羊舌",
  "微生",
  "岳帅",
  "缑亢",
  "况后",
  "有琴",
  "梁丘",
  "左丘",
  "东门",
  "西门",
  "商牟",
  "佘佴",
  "伯赏",
  "南宫",
  "墨哈",
  "谯笪",
  "年爱",
  "阳佟",
  "第五",
}


FEMALE_FIRST_NAME = {
  "言福",
  "亦玉",
  "靖荷",
  "碧萱",
  "寒云",
  "向南",
  "书雁",
  "怀薇",
  "思菱",
  "忆文",
  "翠巧",
  "怀山",
  "若山",
  "向秋",
  "凡白",
  "绮烟",
  "从蕾",
  "天曼",
  "又亦",
  "依琴",
  "曼彤",
  "沛槐",
  "又槐",
  "元绿",
  "安珊",
  "夏之",
  "易槐",
  "宛亦",
  "白翠",
  "丹云",
  "问寒",
  "易文",
  "傲易",
  "青旋",
  "思真",
  "妙之",
  "半双",
  "若翠",
  "初兰",
  "怀曼",
  "惜萍",
  "初之",
  "宛丝",
  "寄南",
  "小萍",
  "幻儿",
  "千风",
  "天蓉",
  "雅青",
  "寄文",
  "代天",
  "春海",
  "惜珊",
  "向薇",
  "冬灵",
  "惜芹",
  "凌青",
  "谷芹",
  "香巧",
  "雁桃",
  "映雁",
  "书兰",
  "盼香",
  "向山",
  "寄风",
  "访烟",
  "绮晴",
  "傲柔",
  "寄容",
  "以珊",
  "紫雪",
  "芷容",
  "书琴",
  "寻桃",
  "涵阳",
  "怀寒",
  "易云",
  "采蓝",
  "代秋",
  "惜梦",
  "尔烟",
  "谷槐",
  "怀莲",
  "涵菱",
  "水蓝",
  "访冬",
  "半兰",
  "又柔",
  "冬卉",
  "安双",
  "冰岚",
  "香薇",
  "语芹",
  "静珊",
  "幻露",
  "访天",
  "静柏",
  "凌丝",
  "小翠",
  "雁卉",
  "访文",
  "凌文",
  "芷云",
  "思柔",
  "巧凡",
  "慕山",
  "依云",
  "千柳",
  "从凝",
  "安梦",
  "香旋",
  "凡巧",
  "映天",
  "安柏",
  "平萱",
  "以筠",
  "忆曼",
  "新竹",
  "绮露",
  "觅儿",
  "碧蓉",
  "白竹",
  "飞兰",
  "曼雁",
  "雁露",
  "凝冬",
  "含灵",
  "初阳",
  "海秋",
  "香天",
  "夏容",
  "傲冬",
  "谷翠",
  "冰双",
  "绿兰",
  "盼易",
  "思松",
  "梦山",
  "友灵",
  "绿竹",
  "灵安",
  "凌柏",
  "秋柔",
  "又蓝",
  "尔竹",
  "天蓝",
  "青枫",
  "问芙",
  "语海",
  "灵珊",
  "凝丹",
  "小蕾",
  "迎夏",
  "水之",
  "飞珍",
  "冰夏",
  "亦竹",
  "飞莲",
  "海白",
  "元蝶",
  "春蕾",
  "芷天",
  "怀绿",
  "尔容",
  "元芹",
  "若云",
  "寒烟",
  "听筠",
  "采梦",
  "凝莲",
  "元彤",
  "觅山",
  "痴瑶",
  "代桃",
  "冷之",
  "盼秋",
  "秋寒",
  "慕蕊",
  "巧夏",
  "海亦",
  "初晴",
  "巧蕊",
  "听安",
  "芷雪",
  "以松",
  "梦槐",
  "寒梅",
  "香岚",
  "寄柔",
  "映冬",
  "孤容",
  "晓蕾",
  "安萱",
  "听枫",
  "夜绿",
  "雪莲",
  "从丹",
  "绮琴",
  "雨文",
  "幼荷",
  "青柏",
  "痴凝",
  "初蓝",
  "忆安",
  "盼晴",
  "寻冬",
  "雪珊",
  "梦寒",
  "迎南",
  "巧香",
  "采南",
  "如彤",
  "春竹",
  "采枫",
  "若雁",
  "翠阳",
  "沛容",
  "幻翠",
  "山兰",
  "芷波",
  "雪瑶",
  "代巧",
  "寄云",
  "慕卉",
  "冷松",
  "涵梅",
  "书白",
  "乐天",
  "宛秋",
  "傲旋",
  "新之",
  "凡儿",
  "夏真",
  "静枫",
  "痴柏",
  "恨蕊",
  "乐双",
  "白玉",
  "问玉",
  "寄松",
  "丹蝶",
  "元瑶",
  "冰蝶",
  "访曼",
  "代灵",
  "芷烟",
  "白易",
  "尔阳",
  "怜烟",
  "平卉",
  "丹寒",
  "访梦",
  "绿凝",
  "冰菱",
  "语蕊",
  "痴梅",
  "思烟",
  "忆枫",
  "映菱",
  "访儿",
  "凌兰",
  "曼岚",
  "若枫",
  "傲薇",
  "凡灵",
  "乐蕊",
  "秋灵",
  "觅云",
}

MALE_FIRST_NAME = {
  "安邦",
  "安福",
  "安歌",
  "安国",
  "安和",
  "安康",
  "安澜",
  "安民",
  "安宁",
  "安平",
  "安然",
  "安顺",
  "安翔",
  "安晏",
  "安宜",
  "安怡",
  "安易",
  "安志",
  "昂然",
  "昂雄",
  "宾白",
  "宾鸿",
  "宾实",
  "彬彬",
  "彬炳",
  "彬郁",
  "斌斌",
  "斌蔚",
  "滨海",
  "波光",
  "波鸿",
  "波峻",
  "波涛",
  "博瀚",
  "博超",
  "博达",
  "博厚",
  "博简",
  "博明",
  "博容",
  "博赡",
  "博涉",
  "博实",
  "博涛",
  "博文",
  "博学",
  "博雅",
  "博延",
  "博艺",
  "博易",
  "博裕",
  "博远",
  "才捷",
  "才良",
  "才艺",
  "才英",
  "才哲",
  "才俊",
  "成和",
  "成弘",
  "成化",
  "成济",
  "成礼",
  "成龙",
  "成仁",
  "成双",
  "成天",
  "成文",
  "成业",
  "成益",
  "成荫",
  "成周",
  "承安",
  "承弼",
  "承德",
  "承恩",
  "承福",
  "承基",
  "承教",
  "承平",
  "承嗣",
  "承天",
  "承望",
  "承宣",
  "承颜",
  "承业",
  "承悦",
  "承允",
  "承运",
  "承载",
  "承泽",
  "承志",
  "德本",
  "德海",
  "德厚",
  "德华",
  "德辉",
  "德惠",
  "德容",
  "德润",
  "德寿",
  "德水",
  "德馨",
  "德曜",
  "德业",
  "德义",
  "德庸",
  "德佑",
  "德宇",
  "德元",
  "德运",
  "德泽",
  "德明",
  "飞昂",
  "飞白",
  "飞飙",
  "飞掣",
  "飞尘",
  "飞沉",
  "飞驰",
  "飞光",
  "飞翰",
  "飞航",
  "飞翮",
  "飞鸿",
  "飞虎",
  "飞捷",
  "飞龙",
  "飞鸾",
  "飞鸣",
  "飞鹏",
  "飞扬",
  "飞文",
  "飞翔",
  "飞星",
  "飞翼",
  "飞英",
  "飞宇",
  "飞羽",
  "飞雨",
  "飞语",
  "飞跃",
  "飞章",
  "飞舟",
  "风华",
  "丰茂",
  "丰羽",
  "刚豪",
  "刚洁",
  "刚捷",
  "刚毅",
  "高昂",
  "高岑",
  "高畅",
  "高超",
  "高驰",
  "高达",
  "高澹",
  "高飞",
  "高芬",
  "高峯",
  "高峰",
  "高歌",
  "高格",
  "高寒",
  "高翰",
  "高杰",
  "高洁",
  "高峻",
  "高朗",
  "高丽",
  "高邈",
  "高旻",
  "高明",
  "高爽",
  "高兴",
  "高轩",
  "高雅",
  "高扬",
  "高阳",
  "高义",
  "高谊",
  "高逸",
  "高懿",
  "高原",
  "高远",
  "高韵",
  "高卓",
  "光赫",
  "光华",
  "光辉",
  "光济",
  "光霁",
  "光亮",
  "光临",
  "光明",
  "光启",
  "光熙",
  "光耀",
  "光誉",
  "光远",
  "国安",
  "国兴",
  "国源",
  "冠宇",
  "冠玉",
  "晗昱",
  "晗日",
  "涵畅",
  "涵涤",
  "涵亮",
  "涵忍",
  "涵容",
  "涵润",
  "涵涵",
  "涵煦",
  "涵蓄",
  "涵衍",
  "涵意",
  "涵映",
  "涵育",
  "翰采",
  "翰池",
  "翰飞",
  "翰海",
  "翰翮",
  "翰林",
  "翰墨",
  "翰学",
  "翰音",
  "瀚玥",
  "翰藻",
  "瀚海",
  "瀚漠",
  "昊苍",
  "昊昊",
  "昊空",
  "昊乾",
  "昊穹",
  "昊然",
  "昊天",
  "昊焱",
  "昊英",
  "浩波",
  "浩博",
  "浩初",
  "浩大",
  "浩宕",
  "浩荡",
  "浩歌",
  "浩广",
  "浩涆",
  "浩瀚",
  "浩浩",
  "浩慨",
  "浩旷",
  "浩阔",
  "浩漫",
  "浩淼",
  "浩渺",
  "浩邈",
  "浩气",
  "浩然",
  "浩穰",
  "浩壤",
  "浩思",
  "浩言",
  "皓轩",
  "和蔼",
  "和安",
  "和璧",
  "和昶",
  "和畅",
  "和风",
  "和歌",
  "和光",
  "和平",
  "和洽",
  "和惬",
  "和顺",
  "和硕",
  "和颂",
  "和泰",
  "和悌",
  "和通",
  "和同",
  "和煦",
  "和雅",
  "和宜",
  "和怡",
  "和玉",
  "和裕",
  "和豫",
  "和悦",
  "和韵",
  "和泽",
  "和正",
  "和志",
  "鹤轩",
  "弘博",
  "弘大",
  "弘方",
  "弘光",
  "弘和",
  "弘厚",
  "弘化",
  "弘济",
  "弘阔",
  "弘亮",
  "弘量",
  "弘深",
  "弘盛",
  "弘图",
  "弘伟",
  "弘文",
  "弘新",
  "弘雅",
  "弘扬",
  "弘业",
  "弘义",
  "弘益",
  "弘毅",
  "弘懿",
  "弘致",
  "弘壮",
  "宏伯",
  "宏博",
  "宏才",
  "宏畅",
  "宏达",
  "宏大",
  "宏放",
  "宏富",
  "宏峻",
  "宏浚",
  "宏恺",
  "宏旷",
  "宏阔",
  "宏朗",
  "宏茂",
  "宏邈",
  "宏儒",
  "宏深",
  "宏胜",
  "宏盛",
  "宏爽",
  "宏硕",
  "宏伟",
  "宏扬",
  "宏义",
  "宏逸",
  "宏毅",
  "宏远",
  "宏壮",
  "鸿宝",
  "鸿波",
  "鸿博",
  "鸿才",
  "鸿彩",
  "鸿畅",
  "鸿畴",
  "鸿达",
  "鸿德",
  "鸿飞",
  "鸿风",
  "鸿福",
  "鸿光",
  "鸿晖",
  "鸿朗",
  "鸿文",
  "鸿熙",
  "鸿羲",
  "鸿禧",
  "鸿信",
  "鸿轩",
  "鸿煊",
  "鸿雪",
  "鸿羽",
  "鸿远",
  "鸿云",
  "鸿运",
  "鸿哲",
  "鸿祯",
  "鸿振",
  "鸿志",
  "鸿卓",
  "华奥",
  "华采",
  "华彩",
  "华灿",
  "华藏",
  "华池",
  "华翰",
  "华皓",
  "华晖",
  "华辉",
  "华茂",
  "华美",
  "华清",
  "华荣",
  "华容",
  "嘉赐",
  "嘉德",
  "嘉福",
  "嘉良",
  "嘉茂",
  "嘉木",
  "嘉慕",
  "嘉纳",
  "嘉年",
  "嘉平",
  "嘉庆",
  "嘉荣",
  "嘉容",
  "嘉瑞",
  "嘉胜",
  "嘉石",
  "嘉实",
  "嘉树",
  "嘉澍",
  "嘉熙",
  "嘉禧",
  "嘉祥",
  "嘉歆",
  "嘉许",
  "嘉勋",
  "嘉言",
  "嘉谊",
  "嘉懿",
  "嘉颖",
  "嘉佑",
  "嘉玉",
  "嘉誉",
  "嘉悦",
  "嘉运",
  "嘉泽",
  "嘉珍",
  "嘉祯",
  "嘉志",
  "嘉致",
  "坚白",
  "坚壁",
  "坚秉",
  "坚成",
  "坚诚",
  "建安",
  "建白",
  "建柏",
  "建本",
  "建弼",
  "建德",
  "建华",
  "建明",
  "建茗",
  "建木",
  "建树",
  "建同",
  "建修",
  "建业",
  "建义",
  "建元",
  "建章",
  "建中",
  "健柏",
  "金鑫",
  "锦程",
  "瑾瑜",
  "晋鹏",
  "经赋",
  "经亘",
  "经国",
  "经略",
  "经纶",
  "经纬",
  "经武",
  "经业",
  "经义",
  "经艺",
  "景澄",
  "景福",
  "景焕",
  "景辉",
  "景龙",
  "景明",
  "景山",
  "景胜",
  "景铄",
  "景天",
  "景同",
  "景曜",
  "靖琪",
  "君昊",
  "君浩",
  "俊艾",
  "俊拔",
  "俊弼",
  "俊才",
  "俊材",
  "俊驰",
  "俊楚",
  "俊达",
  "俊德",
  "俊发",
  "俊风",
  "俊豪",
  "俊健",
  "俊杰",
  "俊捷",
  "俊郎",
  "俊力",
  "俊良",
  "俊迈",
  "俊茂",
  "俊美",
  "俊民",
  "俊名",
  "俊明",
  "俊楠",
  "俊能",
  "俊人",
  "俊爽",
  "俊悟",
  "俊晤",
  "俊侠",
  "俊贤",
  "俊雄",
  "俊雅",
  "俊彦",
  "俊逸",
  "俊英",
  "俊友",
  "俊语",
  "俊誉",
  "俊远",
  "俊哲",
  "俊喆",
  "俊智",
  "峻熙",
  "季萌",
  "季同",
  "乐安",
  "乐邦",
  "乐成",
  "乐池",
  "乐和",
  "乐家",
  "乐康",
  "乐人",
  "乐容",
  "乐山",
  "乐生",
  "乐圣",
  "乐水",
  "乐天",
  "乐童",
  "乐贤",
  "乐心",
  "乐欣",
  "乐逸",
  "乐意",
  "乐音",
  "乐咏",
  "乐游",
  "乐语",
  "乐悦",
  "乐湛",
  "乐章",
  "乐正",
  "乐志",
  "黎昕",
  "黎明",
  "力夫",
  "力强",
  "力勤",
  "力行",
  "力学",
  "力言",
  "立诚",
  "立果",
  "立人",
  "立辉",
  "立轩",
  "立群",
  "良奥",
  "良弼",
  "良才",
  "良材",
  "良策",
  "良畴",
  "良工",
  "良翰",
  "良吉",
  "良骥",
  "良俊",
  "良骏",
  "良朋",
  "良平",
  "良哲",
  "理群",
  "理全",
}