if gUI and not gUI.MountForge then
  gUI.MountForge = {}
end
local _MountForge_ShowCurr = "MountForgeCurrent"
local _MountForge_ShowNext = "MountForgeNext"
local _MountForge_AvatarCurr = "MountForgeAvatarCurr"
local _MountForge_AvatarNext = "MountForgeAvatarNext"
MOUNTFORGE_SLOT_COLLATE = {EQUIPSLOT = 0, MATERIALSLOT = 1}
function Lua_MountForge_IsMount(equipSlot, bagType)
  local _, _, _, _, _, _, _, _, _, _, _, _, _, itemType, _, _, _, _, _, _, equipKind = GetItemInfoBySlot(bagType, equipSlot, 0)
  if itemType == 5 and equipKind == 2 then
    return true
  end
  return false
end
function UI_MountForge_CloseClick(msg)
  _OnMountForge_CloseWindw()
end
function _OnMountForge_CloseWindw()
  if gUI.MountForge.Root:IsVisible() then
    MountForge_CloseWindw()
  end
end
function MountForge_CloseWindw()
  gUI.MountForge.Root:SetProperty("Visible", "false")
  gUI.MountForge.ShowBoxCurr:hide()
  gUI.MountForge.ShowBoxNext:hide()
  gUI.MountForge.CurrAvatar:SetProperty("Visible", "false")
  gUI.MountForge.NextAvatar:SetProperty("Visible", "false")
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_MOUNT_INTENSIFY, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT)
  Messagebox_Hide("MOUNTFORGE_ASK_CONFIRM_BIND")
  Messagebox_Hide("MOUNTFORGE_ASK_CONFIRM_MONEY")
  Messagebox_Hide("MOUNTFORGE_ASK_CONFIRM_MONEY1")
  Messagebox_Hide("MOUNTFORGE_ASK_CONFIRM_MONEY2")
  Messagebox_Hide("MOUNTFORGE_ASK_CONFIRM_MATER")
  Messagebox_Hide("MOUNTFORGE_ASK_CONFIRM_MATER1")
  Messagebox_Hide("MOUNTFORGE_ASK_CONFIRM_MATER2")
  if not slot then
    return
  end
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Cancel, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT, NpcFunction.NPC_FUNC_MOUNT_INTENSIFY)
end
function UI_MountForge_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("MountForge.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_MountForge_RClick(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_MOUNT_INTENSIFY, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT)
  if not slot then
    return
  end
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Cancel, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT, NpcFunction.NPC_FUNC_MOUNT_INTENSIFY)
end
function UI_MountForge_Darg(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    MountForge_CanForge(slot)
  end
end
function MountForge_CanForge(slot)
  local mountLev = GetMountEnhanceLevel(slot, gUI_GOODSBOX_BAG.backpack0)
  local mountMaxLev = GetMountMaxLevel(slot, gUI_GOODSBOX_BAG.backpack0)
  if mountLev >= mountMaxLev then
    Lua_Chat_ShowOSD("MOUNTFORGE_MAXLEVEL")
    return
  end
  if Lua_MountForge_IsMount(slot, gUI_GOODSBOX_BAG.backpack0) then
    SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Add, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT, NpcFunction.NPC_FUNC_MOUNT_INTENSIFY)
  else
    Lua_Chat_ShowOSD("MOUNTFORGE_NOTMOUNT")
  end
end
function UI_MountForge_ShowToolTip(msg)
  local win = msg:get_window()
  local tooltip = win:GetToolTipWnd(0)
  if WindowToGoodsBox(win):IsItemHasIcon(0) then
    local sbag, sslot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_MOUNT_INTENSIFY, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT)
    Lua_Tip_Item(tooltip, sslot, gUI_GOODSBOX_BAG.backpack0, nil, 0, nil)
  end
end
function UI_MountForge_ShowSubToolTip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  if win:IsItemHasIcon(0) then
    local itemId = win:GetCustomUserData(0)
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemId)
  end
end
function UI_Confim_Click(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_MOUNT_INTENSIFY, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT)
  if not bag then
    Lua_Chat_ShowOSD("MONUTFORGE_NO_EQUIP")
    return
  end
  local type1, amount1, type2, amount2 = GetForgeGoldConsume(slot, bag, 0)
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  local totalMoney = tonumber(playerNonBindGold) + tonumber(playerBindGold)
  if gUI.MountForge.MoneyFrist:IsChecked() then
    if amount1 > playerNonBindGold then
      Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
      return
    end
  elseif tonumber(totalMoney) < tonumber(amount1) then
    Lua_Chat_ShowOSD("NOT_ENOUGH_MONEY")
    return
  end
  local needPropCount, totalPropInBag, propBagslot, propName, propIcon, bindStoneId, nonBindStoneId, propDec, quality, bindNum, unBindNum = GetMountPropStone()
  if gUI.MountForge.MaterialFrist:IsChecked() then
    if unBindNum < needPropCount then
      Lua_Chat_ShowOSD("MONUTFORGE_NO_PROP1")
      return
    end
  elseif totalPropInBag < needPropCount then
    Lua_Chat_ShowOSD("MONUTFORGE_NO_PROP")
    return
  end
  if GetBindInfo(slot, bag) == false then
    if gUI.MountForge.MaterialFrist:IsChecked() then
      if needPropCount <= unBindNum then
        Lua_MountForge_Money()
      else
      end
    elseif bindNum > 0 then
      Messagebox_Show("MOUNTFORGE_ASK_CONFIRM_MATER")
    else
      Lua_MountForge_Money()
    end
  elseif GetBindInfo(slot, bag) == true then
    DoMountForge(gUI.MountForge.MoneyFrist:IsChecked(), gUI.MountForge.MaterialFrist:IsChecked())
  end
end
function Lua_MountForge_Money()
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_MOUNT_INTENSIFY, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT)
  local type1, amount1, type2, amount2 = GetForgeGoldConsume(slot, bag, 0)
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  local totalMoney = tonumber(playerNonBindGold) + tonumber(playerBindGold)
  if gUI.MountForge.MoneyFrist:IsChecked() then
    if amount1 <= playerNonBindGold then
      DoMountForge(gUI.MountForge.MoneyFrist:IsChecked(), gUI.MountForge.MaterialFrist:IsChecked())
    else
    end
  elseif playerBindGold == 0 then
    DoMountForge(gUI.MountForge.MoneyFrist:IsChecked(), gUI.MountForge.MaterialFrist:IsChecked())
  else
    Messagebox_Show("MOUNTFORGE_ASK_CONFIRM_MONEY")
  end
end
function Lua_MountForge_Money1()
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_MOUNT_INTENSIFY, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT)
  local type1, amount1, type2, amount2 = GetForgeGoldConsume(slot, bag, 0)
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  local totalMoney = tonumber(playerNonBindGold) + tonumber(playerBindGold)
  if gUI.MountForge.MoneyFrist:IsChecked() then
    if playerNonBindGold > 0 then
      Messagebox_Show("MOUNTFORGE_ASK_CONFIRM_MONEY1")
    end
  elseif amount1 > playerBindGold then
    Messagebox_Show("MOUNTFORGE_ASK_CONFIRM_MONEY2")
  else
    DoMountForge(gUI.MountForge.MoneyFrist:IsChecked(), gUI.MountForge.MaterialFrist:IsChecked())
  end
end
function MountForge_OpenPanel()
  if gUI.MountForge.Root:IsVisible() then
  else
    gUI.MountForge.Root:SetProperty("Visible", "true")
    gUI.MountForge.ConfimBtn:SetProperty("Enable", "false")
    gUI.MountForge.CurrAvatar:SetProperty("Visible", "false")
    gUI.MountForge.NextAvatar:SetProperty("Visible", "false")
    Lua_Bag_ShowUI(true)
    MountForge_ClearUI()
  end
end
function MountForge_ClearUI()
  gUI.MountForge.MainSlot:ClearGoodsItem(0)
  gUI.MountForge.SubSlot:ClearGoodsItem(0)
  gUI.MountForge.MainSlot:SetItemBindState(0, 0)
  gUI.MountForge.SubSlot:SetItemBindState(0, 0)
  gUI.MountForge.CurrSpeed:SetProperty("Text", "0 %")
  gUI.MountForge.CurrLab:SetProperty("Text", "--")
  gUI.MountForge.MaterLab:SetProperty("Text", "--")
  gUI.MountForge.SuccLab:SetProperty("Text", "0 %")
  gUI.MountForge.CostLab:SetProperty("Text", Lua_UI_Money2String(0))
  gUI.MountForge.NextSpeed:SetProperty("Text", "0 %")
  gUI.MountForge.NextLab:SetProperty("Text", "--")
  gUI.MountForge.SubNumb:SetProperty("Text", "")
end
function _OnMountForge_GetResult(isSuccess, name, guidStr, lv)
  if isSuccess ~= 0 then
    Lua_Chat_ShowOSD("MONUTFORGR_SUCC")
    WorldStage:playSoundByID(3)
  else
    Lua_Chat_ShowOSD("MONUTFORGE_FAILED")
    WorldStage:playSoundByID(4)
  end
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_MOUNT_INTENSIFY, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT)
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Cancel, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT, NpcFunction.NPC_FUNC_MOUNT_INTENSIFY)
  if bag == gUI_GOODSBOX_BAG.backpack0 then
    local mountLev = GetMountEnhanceLevel(slot, gUI_GOODSBOX_BAG.backpack0)
    local mountMaxLev = GetMountMaxLevel(slot, gUI_GOODSBOX_BAG.backpack0)
    if mountLev >= mountMaxLev then
      gUI.MountForge.ConfimBtn:SetProperty("Enable", "false")
      Lua_Chat_ShowOSD("MOUNTFORGE_MAXLEVEL")
      return
    end
    SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Add, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT, NpcFunction.NPC_FUNC_MOUNT_INTENSIFY)
  end
end
function _OnMountForge_OpenPanel()
  MountForge_OpenPanel()
end
function _MountForge_AddEquip(bagType, slot)
  _MountForge_UpdateBounce()
  _MountForge_UpdateEquipInfo()
end
function _MountForge_UpdateBounce()
  local bagType, bagSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_MOUNT_INTENSIFY, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT)
  if bagType then
    local mountLev = GetMountEnhanceLevel(bagSlot, bagType)
    local mountMaxLev = GetMountMaxLevel(bagSlot, bagType)
    local StrMountLev, StrSpeed
    gUI.MountForge.ShowBoxCurr:show()
    gUI.MountForge.ShowBoxNext:show()
    gUI.MountForge.CurrAvatar:SetProperty("Visible", "true")
    gUI.MountForge.NextAvatar:SetProperty("Visible", "true")
    if mountLev >= mountMaxLev then
      MountForge_ClearUI()
      gUI.MountForge.ConfimBtn:SetProperty("Enable", "false")
      gUI.MountForge.ShowBoxNext:hide()
      gUI.MountForge.ShowBoxCurr:hide()
      gUI.MountForge.NextAvatar:SetProperty("Visible", "false")
      Lua_Chat_ShowOSD("MOUNTFORGE_MAXLEVEL")
    else
      gUI.MountForge.ConfimBtn:SetProperty("Enable", "true")
      local SpeedNext = GetMountSpeeds(bagSlot, bagType, 1)
      gUI.MountForge.NextSpeed:SetProperty("Text", tostring(SpeedNext) .. " %")
      gUI.MountForge.NextLab:SetProperty("Text", tostring(gUI_Mount_Rank[mountLev + 1]))
      _MountForge_Refurbish_Items()
      _MountForge_Refurbish_money()
      local modelIdNext = GetMountModle(1)
      if not gUI.MountForge.ShowBoxNext:isAvatarExist("Mount" .. modelIdNext) then
        gUI.MountForge.ShowBoxNext:onCharacterReset()
        gUI.MountForge.ShowBoxNext:revertCamera()
        gUI.MountForge.ShowBoxNext:createMountAvatar(modelIdNext, "Mount")
      end
      gUI.MountForge.ShowBoxNext:setCurrentAvatar("Mount" .. modelIdNext)
    end
    gUI.MountForge.CurrLab:SetProperty("Text", tostring(gUI_Mount_Rank[mountLev]))
    local Speed = GetMountSpeeds(bagSlot, bagType, 0)
    gUI.MountForge.CurrSpeed:SetProperty("Text", tostring(Speed) .. " %")
    local modelIdCur = GetMountModle(0)
    if not gUI.MountForge.ShowBoxCurr:isAvatarExist("Mount" .. modelIdCur) then
      gUI.MountForge.ShowBoxCurr:onCharacterReset()
      gUI.MountForge.ShowBoxCurr:revertCamera()
      gUI.MountForge.ShowBoxCurr:createMountAvatar(modelIdCur, "Mount")
    end
    gUI.MountForge.ShowBoxCurr:setCurrentAvatar("Mount" .. modelIdCur)
  end
end
function _MountForge_Refurbish_Items()
  if gUI.MountForge.Root:IsVisible() then
    local needPropCount, totalPropInBag, propBagslot, propName, propIcon, bindStoneId, nonBindStoneId, propDec, quality, bindNum, unBindNum = GetMountPropStone()
    if needPropCount then
      if gUI.MountForge.MaterialFrist:IsChecked() then
        local strNeed = tostring(unBindNum) .. "/" .. tostring(needPropCount)
        gUI.MountForge.SubNumb:SetProperty("Text", strNeed)
        if unBindNum < needPropCount then
          gUI.MountForge.SubSlot:SetIconState(0, 7, true)
        else
          gUI.MountForge.SubSlot:SetIconState(0, 7, false)
        end
        gUI.MountForge.SubSlot:SetItemBindState(0, 0)
        gUI.MountForge.SubSlot:SetCustomUserData(0, nonBindStoneId)
      else
        local strNeed = tostring(totalPropInBag) .. "/" .. tostring(needPropCount)
        gUI.MountForge.SubNumb:SetProperty("Text", strNeed)
        if totalPropInBag < needPropCount then
          gUI.MountForge.SubSlot:SetIconState(0, 7, true)
        else
          gUI.MountForge.SubSlot:SetIconState(0, 7, false)
        end
        if bindNum > 0 then
          gUI.MountForge.SubSlot:SetItemBindState(0, 1)
          gUI.MountForge.SubSlot:SetCustomUserData(0, bindStoneId)
        else
          gUI.MountForge.SubSlot:SetItemBindState(0, 0)
          gUI.MountForge.SubSlot:SetCustomUserData(0, nonBindStoneId)
        end
      end
      local _, _, quality = GetItemInfoBySlot(-1, bindStoneId, nil)
      gUI.MountForge.SubSlot:SetItemGoods(0, tostring(propIcon), quality)
      gUI.MountForge.MaterLab:SetProperty("Text", tostring(propName))
    end
  end
end
function _MountForge_Refurbish_money(newMoney, changeMoney, event)
  if gUI.MountForge.Root:IsVisible() then
    local SuccRate, mountCostMoney = GetMountCost()
    if SuccRate then
      gUI.MountForge.SuccLab:SetProperty("Text", tostring(SuccRate) .. " %")
      if gUI.MountForge.MoneyFrist:IsChecked() then
        gUI.MountForge.CostLab:SetProperty("Text", Lua_UI_Money2String(mountCostMoney, true, false, false, true, newMoney, event))
      else
        gUI.MountForge.CostLab:SetProperty("Text", Lua_UI_Money2String(mountCostMoney, false, false, false, true, newMoney, event))
      end
    end
  end
end
function _MountForge_UpdateEquipInfo()
  local bagType, bagSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_MOUNT_INTENSIFY, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT)
  if bagType then
    local _, icon, quality, _, _, _, _, _, _, _, _, itemCount = GetItemInfoBySlot(bagType, bagSlot, 0)
    local canForge, name, level, maxXiouweiLevel, maxLevel, curXiuwei, curClrLev = GetForgeEquipInfo(bagSlot)
    if not icon then
      return
    end
    gUI.MountForge.MainSlot:SetItemGoods(0, icon, quality)
    local bindinfo = Lua_Bag_GetBindInfo(bagType, bagSlot)
    gUI.MountForge.MainSlot:SetItemBindState(0, bindinfo)
  end
end
function _OnMountForge_AddItem(bagType, slot, icon, itemType)
  if itemType == MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT then
    _MountForge_AddEquip(bagType, slot)
  end
end
function _OnMountForge_DelItem(boxType)
  if boxType == MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT then
    MountForge_ClearUI()
    gUI.MountForge.ShowBoxCurr:hide()
    gUI.MountForge.ShowBoxNext:hide()
    gUI.MountForge.CurrAvatar:SetProperty("Visible", "false")
    gUI.MountForge.NextAvatar:SetProperty("Visible", "false")
  end
end
function Script_MountForge_OnEvent(event)
  if "HORSE_TRAINING_WINDOW" == event then
    _OnMountForge_OpenPanel()
  elseif "MOUNTENHANCE_ADD_ITEM" == event then
    _OnMountForge_AddItem(arg1, arg2, arg3, arg4)
  elseif "SHOW_MOUNTENHANCE_RESULT" == event then
    _OnMountForge_GetResult(arg1, arg2, arg3, arg4)
  elseif "MOUNTENHANCE_DEL_ITEM" == event then
    _OnMountForge_DelItem(arg1)
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    _OnMountForge_CloseWindw()
  elseif "ITEM_ADD_TO_BAG" == event then
    _MountForge_Refurbish_Items()
  elseif "ITEM_DEL_FROM_BAG" == event then
    _MountForge_Refurbish_Items()
  elseif event == "PLAYER_MONEY_CHANGED" then
    _MountForge_Refurbish_money(arg1, arg2, event)
  elseif event == "PLAYER_BIND_MONEY_CHANGED" then
    _MountForge_Refurbish_money(arg1, arg2, event)
  end
end
function Script_MountForge_OnHide()
  MountForge_CloseWindw()
end
function UI_MountForge_MoneyFrist(msg)
  _MountForge_Refurbish_money()
end
function UI_MountForge_MaterFrist(msg)
  _MountForge_Refurbish_Items()
end
function UI_MountForge_ModelLeftRotate()
  AddTimerEvent(2, _MountForge_AvatarCurr, function()
    gUI.MountForge.ShowBoxCurr:onCharacterLeft()
  end)
end
function UI_MountForge_ModelRightRotate()
  AddTimerEvent(2, _MountForge_AvatarCurr, function()
    gUI.MountForge.ShowBoxCurr:onCharacterRight()
  end)
end
function UI_MountForge_ModelStopRotate()
  DelTimerEvent(2, _MountForge_AvatarCurr)
end
function UI_MountForge_ModelLeftRotate1()
  AddTimerEvent(2, _MountForge_AvatarNext, function()
    gUI.MountForge.ShowBoxNext:onCharacterLeft()
  end)
end
function UI_MountForge_ModelRightRotate1()
  AddTimerEvent(2, _MountForge_AvatarNext, function()
    gUI.MountForge.ShowBoxNext:onCharacterRight()
  end)
end
function UI_MountForge_ModelStopRotate1()
  DelTimerEvent(2, _MountForge_AvatarNext)
end
function Script_MountForge_OnLoad()
  gUI.MountForge.Root = WindowSys_Instance:GetWindow("MountForge")
  gUI.MountForge.MainSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("MountForge.Image_bg.Left_bg.Mount_gbox"))
  gUI.MountForge.CurrLab = WindowToLabel(WindowSys_Instance:GetWindow("MountForge.Image_bg.Left_bg.Property_dlab"))
  gUI.MountForge.CurrSpeed = WindowToLabel(WindowSys_Instance:GetWindow("MountForge.Image_bg.Left_bg.Speed_dlab"))
  gUI.MountForge.SubSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("MountForge.Image_bg.Center_bg.Evolve_gbox"))
  gUI.MountForge.SubNumb = WindowToGoodsBox(WindowSys_Instance:GetWindow("MountForge.Image_bg.Center_bg.Evolve_dlab"))
  gUI.MountForge.MaterLab = WindowToLabel(WindowSys_Instance:GetWindow("MountForge.Image_bg.Center_bg.Evolve_slab"))
  gUI.MountForge.SuccLab = WindowToLabel(WindowSys_Instance:GetWindow("MountForge.Image_bg.Center_bg.Odds_dlab"))
  gUI.MountForge.CostLab = WindowToLabel(WindowSys_Instance:GetWindow("MountForge.Image_bg.Center_bg.Spend_dlab"))
  gUI.MountForge.NextLab = WindowToLabel(WindowSys_Instance:GetWindow("MountForge.Image_bg.Right_bg.Property_dlab"))
  gUI.MountForge.NextSpeed = WindowToLabel(WindowSys_Instance:GetWindow("MountForge.Image_bg.Right_bg.Speed_dlab"))
  gUI.MountForge.ConfimBtn = WindowToButton(WindowSys_Instance:GetWindow("MountForge.Image_bg.Center_bg.Evolve_btn"))
  gUI.MountForge.CurrAvatar = WindowToPicture(WindowSys_Instance:GetWindow("MountForge.Image_bg.Left_bg.Mounts_pic"))
  gUI.MountForge.NextAvatar = WindowToPicture(WindowSys_Instance:GetWindow("MountForge.Image_bg.Right_bg.Mounts_pic"))
  local showboxCurr = UIInterface:getShowbox(_MountForge_ShowCurr)
  local showboxNext = UIInterface:getShowbox(_MountForge_ShowNext)
  showboxCurr = showboxCurr or UIInterface:createShowbox(_MountForge_ShowCurr, gUI.MountForge.CurrAvatar)
  showboxNext = showboxNext or UIInterface:createShowbox(_MountForge_ShowNext, gUI.MountForge.NextAvatar)
  gUI.MountForge.ShowBoxCurr = showboxCurr
  gUI.MountForge.ShowBoxNext = showboxNext
  gUI.MountForge.MoneyFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("MountForge.Image_bg.Center_bg.Money_cbtn"))
  gUI.MountForge.MoneyFrist:SetChecked(false)
  gUI.MountForge.MaterialFrist = WindowToCheckButton(WindowSys_Instance:GetWindow("MountForge.Image_bg.Center_bg.Pro_cbtn"))
  gUI.MountForge.MaterialFrist:SetChecked(false)
end
