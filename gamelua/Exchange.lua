if gUI and not gUI.Exchange then
  gUI.Exchange = {}
end
function _Exchange_UpdateSelfMoney(money)
  if money < 0 then
    money = 0
  end
  local gold = math.floor(money / 10000)
  gUI.Exchange.SelfGoldLab:SetProperty("Text", tostring(gold))
  local silver = math.floor(money / 100) % 100
  gUI.Exchange.SelfSilverLab:SetProperty("Text", tostring(silver))
  local copper = money % 100
  gUI.Exchange.SelfCopperLab:SetProperty("Text", tostring(copper))
  _OnExchange_UpdateSelfColor()
end
function _Exchange_UpdateMoney(money)
  if money < 0 then
    money = 0
  end
  local gold = math.floor(money / 10000)
  gUI.Exchange.TarGoldLab:SetProperty("Text", tostring(gold))
  local silver = math.floor(money / 100) % 100
  gUI.Exchange.TarSilverLab:SetProperty("Text", tostring(silver))
  local copper = money % 100
  gUI.Exchange.TarCopperLab:SetProperty("Text", tostring(copper))
end
function _Exchange_GetMoney()
  local money = 0
  local text
  text = gUI.Exchange.SelfGoldLab:GetProperty("Text")
  if text ~= "" then
    money = tonumber(text) * 10000
  end
  text = gUI.Exchange.SelfSilverLab:GetProperty("Text")
  if text ~= "" then
    money = money + tonumber(text) * 100
  end
  text = gUI.Exchange.SelfCopperLab:GetProperty("Text")
  if text ~= "" then
    money = money + tonumber(text)
  end
  return money
end
function _Exchange_EnableMoneyInput(enable)
  local read_only = not enable
  gUI.Exchange.SelfGoldLab:SetProperty("ReadOnly", tostring(read_only))
  gUI.Exchange.SelfSilverLab:SetProperty("ReadOnly", tostring(read_only))
  gUI.Exchange.SelfCopperLab:SetProperty("ReadOnly", tostring(read_only))
  if read_only then
    local win = WindowSys_Instance:GetRootWindow()
    WindowSys_Instance:SetFocusWindow(win)
    WindowSys_Instance:SetKeyboardCaptureWindow(win)
  else
  end
end
function _Exchange_CloseTrade(IsOk)
  if gUI.Exchange.Root:IsVisible() then
    if IsOk == 0 then
      SwapCancleTrade()
    end
    gUI.Exchange.Root:SetProperty("Visible", "false")
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
  else
  end
end
function _Exchange_ClosePanel()
  _Exchange_CloseTrade(0)
end
function Lua_Exchange_ApplyAddItem(from_win, from_off)
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(from_win, from_off)
  end
  if Lua_Bag_IsBackPackbox(from_type) then
    if from_type == "backpack1" then
      Lua_Chat_ShowOSD("STALL_INVALID_ITEM")
    else
      SwapSetTradeItem(gUI_GOODSBOX_BAG[from_type], from_off)
    end
  else
  end
end
function Lua_Exchange_CloseApplyWnd()
  if gUI.Exchange.ApplyWndState == true then
    SwapRefuseTradeRequest()
    Lua_MB_CloseBox("CONFIRM_SWAP")
    gUI.Exchange.ApplyWndState = false
  end
end
function UI_Exchange_MoneyBoxRclick(msg)
  if gUI.Exchange.SelfMask:IsVisible() then
    return
  end
  gUI.Exchange.SelfGoldLab:SetProperty("Text", "")
  gUI.Exchange.SelfSilverLab:SetProperty("Text", "")
  gUI.Exchange.SelfCopperLab:SetProperty("Text", "")
end
function UI_Exchange_ItemDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_off = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  Lua_Exchange_ApplyAddItem(from_win, from_off)
end
function UI_Exchange_ItemRClick(msg)
  SwapClearTradeItem(msg:get_lparam())
end
function UI_Exchange_TarTooltip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local slot = msg:get_wparam()
  if wnd:IsItemHasIcon(slot) then
    Lua_Tip_Item(wnd:GetToolTipWnd(0), slot + 100, gUI_GOODSBOX_BAG.exchange, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(wnd, gUI_GOODSBOX_BAG.exchange, slot + 100)
  end
end
function UI_Exchange_SelfTooltip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local slot = msg:get_wparam()
  if wnd:IsItemHasIcon(slot) then
    Lua_Tip_Item(wnd:GetToolTipWnd(0), slot, gUI_GOODSBOX_BAG.exchange, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(wnd, gUI_GOODSBOX_BAG.exchange, slot)
  end
end
function UI_Exchange_MoneyChange(msg)
  local nMoney = _Exchange_GetMoney()
  local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
  if nMoney > nonBindemoney then
    _Exchange_UpdateSelfMoney(nonBindemoney)
  else
    _OnExchange_UpdateSelfColor()
  end
end
function UI_Exchange_LockBtnClick(msg)
  local nMoney = _Exchange_GetMoney()
  SwapSetTradeGold(nMoney)
  SwapLockTrade()
end
function UI_Exchange_OKClick(msg)
  local btn = msg:get_window()
  SwapCommitTrade()
  btn:SetProperty("Enable", "false")
end
function UI_Exchange_CancelBtnClick(msg)
  _Exchange_CloseTrade(0)
end
function UI_Exchange_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Exchange.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function _OnExchange_Show(who, level, menpai)
  if not gUI.Exchange.Root:IsVisible() then
    gUI.Exchange.TarLockText:SetVisible(false)
    gUI.Exchange.TarLockCheck:SetVisible(false)
    gUI.Exchange.TarLockCheck:SetChecked(false)
    gUI.Exchange.SelfLockText:SetVisible(false)
    gUI.Exchange.SelfLockCheck:SetVisible(false)
    gUI.Exchange.SelfLockCheck:SetChecked(false)
    WorldStage:CloseAllFloatWindow()
    gUI.Exchange.Root:SetProperty("Visible", "true")
    Lua_Bag_ShowUI(true)
    gUI.Exchange.LockBtn:SetProperty("Enable", "true")
    gUI.Exchange.TradeBtn:SetProperty("Enable", "false")
    gUI.Exchange.SelfName:SetProperty("Text", "我的交易栏")
    gUI.Exchange.TarName:SetProperty("Text", who .. " " .. tostring(level) .. "级 " .. gUI_MenPaiName[menpai])
    _Exchange_UpdateMoney(0)
    _Exchange_UpdateSelfMoney(0)
    gUI.Exchange.SelfMask:SetProperty("Visible", "false")
    gUI.Exchange.TarMask:SetProperty("Visible", "false")
    gUI.Exchange.TarBox:ResetAllGoods(true)
    gUI.Exchange.SelfBox:ResetAllGoods(true)
    _Exchange_EnableMoneyInput(true)
  end
end
function _OnExchange_AcceptTrade(arg1)
  Messagebox_Show("CONFIRM_SWAP", arg1)
end
function _OnExchange_AddItem(index, name, icon, num, id, quality)
  local gb
  local intenLev = 0
  if index >= gUI.Exchange.SlotCount then
    gb = gUI.Exchange.TarBox
    index = index - gUI.Exchange.SlotCount
    intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.exchange, index + 100)
  else
    gb = gUI.Exchange.SelfBox
    intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.exchange, index)
  end
  gb:SetItemGoods(index, icon, quality)
  if 0 < ForgeLevel_To_Stars[intenLev] then
    gb:SetItemStarState(index, ForgeLevel_To_Stars[intenLev])
  else
    gb:SetItemStarState(index, 0)
  end
  if num and num > 1 then
    gb:SetItemSubscript(index, tostring(num))
  end
  gb:SetItemData(index, id)
end
function _OnExchange_DelItem(index)
  local gb
  if index >= gUI.Exchange.SlotCount then
    gb = gUI.Exchange.TarBox
    index = index - gUI.Exchange.SlotCount
  else
    gb = gUI.Exchange.SelfBox
  end
  gb:ClearGoodsItem(index)
  gb:SetItemData(index, 0)
end
function _OnExchange_Lock(bMyLocked, bOtherLocked)
  gUI.Exchange.TarMask:SetProperty("Visible", tostring(bOtherLocked))
  gUI.Exchange.TarLockText:SetVisible(bOtherLocked)
  gUI.Exchange.TarLockCheck:SetVisible(bOtherLocked)
  gUI.Exchange.TarLockCheck:SetProperty("Checked", tostring(bOtherLocked))
  if bMyLocked then
    gUI.Exchange.SelfLockText:SetVisible(true)
    gUI.Exchange.SelfLockCheck:SetVisible(true)
    gUI.Exchange.SelfLockCheck:SetProperty("Checked", "true")
    gUI.Exchange.SelfMask:SetProperty("Visible", "true")
    _Exchange_EnableMoneyInput(false)
    gUI.Exchange.LockBtn:SetProperty("Enable", "false")
  else
    gUI.Exchange.SelfLockText:SetVisible(false)
    gUI.Exchange.SelfLockCheck:SetVisible(false)
    gUI.Exchange.SelfLockCheck:SetProperty("Checked", "false")
    gUI.Exchange.SelfMask:SetProperty("Visible", "false")
    gUI.Exchange.LockBtn:SetProperty("Enable", "true")
  end
  gUI.Exchange.TradeBtn:SetProperty("Enable", tostring(bMyLocked and bOtherLocked))
end
function _OnExchange_UpdateMoney(money)
  _Exchange_UpdateMoney(money)
end
function _OnExchange_UpdateSelfColor()
  local moneyReal = tonumber(gUI.Exchange.SelfGoldLab:GetProperty("Text"))
  local color = Lua_UI_GetGoldColor(moneyReal, true)
  gUI.Exchange.SelfGoldLab:SetProperty("FontColor", color)
  gUI.Exchange.SelfSilverLab:SetProperty("FontColor", color)
  gUI.Exchange.SelfCopperLab:SetProperty("FontColor", color)
end
function Script_Exchange_OnEvent(event)
  if event == "ON_TRADE_OPEN" then
    _OnExchange_Show(arg1, arg2, arg3)
  elseif event == "ON_TRADE_REQUEST" then
    _OnExchange_AcceptTrade(arg1, arg2, arg3)
  elseif event == "ON_TRADE_ADD_ITEM" then
    _OnExchange_AddItem(arg1, arg2, arg3, arg4, arg5, arg6)
  elseif event == "ON_TRADE_DEL_ITEM" then
    _OnExchange_DelItem(arg1)
  elseif event == "ON_TRADE_CLOSE" then
    _Exchange_CloseTrade(arg1)
  elseif event == "ON_TRADE_UPDATE_LOCK" then
    _OnExchange_Lock(arg1, arg2)
  elseif event == "ON_TRADE_UPDATE_MONEY" then
    _OnExchange_UpdateMoney(arg2)
  elseif event == "CLOSE_SWAP_WINDOW" then
    _Exchange_ClosePanel()
  elseif event == "STAGE_CHANGED" then
    gUI.Exchange.ApplyWndState = false
  end
end
function Script_Exchange_OnLoad()
  gUI.Exchange.Root = WindowSys_Instance:GetWindow("Exchange")
  gUI.Exchange.SelfName = WindowSys_Instance:GetWindow("Exchange.Pic2_bg.PicName_bg.UserName_dlab")
  gUI.Exchange.TarName = WindowSys_Instance:GetWindow("Exchange.Pic1_bg.PicName_bg.ElseUserName_dlab")
  gUI.Exchange.SelfBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Exchange.Pic2_bg.Picture1_bg.GoodsBox2_gbox"))
  gUI.Exchange.TarBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Exchange.Pic1_bg.Picture1_bg.GoodsBox1_gbox"))
  gUI.Exchange.LockBtn = WindowToButton(WindowSys_Instance:GetWindow("Exchange.Locked_btn"))
  gUI.Exchange.TradeBtn = WindowToButton(WindowSys_Instance:GetWindow("Exchange.Exchange_btn"))
  gUI.Exchange.SelfGoldLab = WindowSys_Instance:GetWindow("Exchange.Pic2_bg.Money_bg.MoneyAu_ebox")
  gUI.Exchange.SelfSilverLab = WindowSys_Instance:GetWindow("Exchange.Pic2_bg.Money_bg.MoneyAg_ebox")
  gUI.Exchange.SelfCopperLab = WindowSys_Instance:GetWindow("Exchange.Pic2_bg.Money_bg.MoneyCu_ebox")
  gUI.Exchange.TarGoldLab = WindowSys_Instance:GetWindow("Exchange.Pic1_bg.Money_bg.MoneyAu_ebox")
  gUI.Exchange.TarSilverLab = WindowSys_Instance:GetWindow("Exchange.Pic1_bg.Money_bg.MoneyAg_ebox")
  gUI.Exchange.TarCopperLab = WindowSys_Instance:GetWindow("Exchange.Pic1_bg.Money_bg.MoneyCu_ebox")
  gUI.Exchange.SelfLockCheck = WindowToCheckButton(WindowSys_Instance:GetWindow("Exchange.Pic2_bg.Locked_cbtn"))
  gUI.Exchange.SelfLockText = WindowSys_Instance:GetWindow("Exchange.Pic2_bg.PicLocked_bg")
  gUI.Exchange.TarLockCheck = WindowToCheckButton(WindowSys_Instance:GetWindow("Exchange.Pic1_bg.Locked_cbtn"))
  gUI.Exchange.TarLockText = WindowSys_Instance:GetWindow("Exchange.Pic1_bg.PicLocked_bg")
  gUI.Exchange.SelfMask = WindowSys_Instance:GetWindow("Exchange.Pic2_bg.LockedPic_pic")
  gUI.Exchange.TarMask = WindowSys_Instance:GetWindow("Exchange.Pic1_bg.LockedPic_pic")
  gUI.Exchange.IsTradeGold = false
  gUI.Exchange.SlotCount = 12
  if gUI.Exchange.ApplyWndState == true then
    SwapRefuseTradeRequest()
    gUI.Exchange.ApplyWndState = false
  else
    gUI.Exchange.ApplyWndState = false
  end
end
function Script_Exchange_OnHide()
end
function Script_Exchange_OnEscape()
  SwapCancleTrade()
end

