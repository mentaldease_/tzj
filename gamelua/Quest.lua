DIALOG_STATUS_NONE = 0
DIALOG_STATUS_UNAVAILABLE = 1
DIALOG_STATUS_CHAT = 2
DIALOG_STATUS_INCOMPLETE = 3
DIALOG_STATUS_AVAILABLE = 4
DIALOG_STATUS_REWARD = 5
QUEST_TYPE_NORMAL = 0
QUEST_STATUS_NONE = 0
QUEST_STATUS_COMPLETE = 1
QUEST_STATUS_UNAVAILABLE = 2
QUEST_STATUS_INCOMPLETE = 3
QUEST_STATUS_AVAILABLE = 4
QUEST_STATUS_DONE = 5
QUEST_STATUS_FAILED = 6
FIELD_TYPE_INVALID_VALUE = 0
FIELD_TYPE_CHANGE_COLOR = 1
FIELD_TYPE_ITEM_LINK = 2
FIELD_TYPE_HYPER_LINK = 3
FIELD_TYPE_ANIMATION = 4
FIELD_TYPE_NEW_LINE = 5
FIELD_TYPE_NEW_FONT = 6
FIELD_TYPE_PICTURE = 7
FIELD_TYPE_COORDINATE_LINK = 8
FIELD_TYPE_DEFAULT_COLOR = 9
FIELD_TYPE_ITEM = 10
FIELD_TYPE_SHADOW_COLOR = 11
local _QB_QuestNumText = "已接任务(%d/25)"
local _QB_CurrSelectedQuest = 0
local _QB_AskCanAcceptList = true
local _QB_TrackSelectedQuest = 0
local _QB_CurrentNpcLink = ""
local _QB_TreeItemImage = {
  [1] = "UiBtn001:Btnreduce_n",
  [2] = "UiBtn001:Btnadd_n"
}
local _QB_TrackCount = 0
local _QB_TRACKMAXNUM = 25
local _QB_LastSearchPathId = -1
local _QB_QuestStageImage = {
  [0] = "  ",
  [1] = "  ",
  [2] = "  ",
  [3] = "  "
}
local _QB_QuestKindDesc = {
  [0] = "全部任务",
  [1] = "主线",
  [2] = "支线",
  [3] = "副本",
  [4] = "职业",
  [5] = "活动",
  [6] = "引导",
  [7] = "日常",
  [8] = "跑环",
  [9] = "帮会",
  [10] = "押镖"
}
local _QB_QuestKind = {
  [0] = {
    Desc = "全部任务",
    Kind = {}
  },
  [1] = {
    Desc = "主线",
    Kind = {1}
  },
  [2] = {
    Desc = "副本",
    Kind = {3}
  },
  [3] = {
    Desc = "活动",
    Kind = {5}
  },
  [4] = {
    Desc = "日常",
    Kind = {7}
  },
  [5] = {
    Desc = "帮会",
    Kind = {9}
  },
  [6] = {
    Desc = "其他",
    Kind = {
      2,
      4,
      6,
      8,
      10
    }
  }
}
local _QB_CommissionDesc = {
  [1] = "[已委托]",
  [2] = "[委]"
}
local _QB_TrackTable = {}
local _QB_BarPos
local _QB_TrackTotalCount = 0
local _QB_MouseInCtrlList = false
function _QB_GetQuestTextColor(QuestLevel, MyLevel)
  local QuestColor
  if QuestLevel > MyLevel + 6 then
    QuestColor = 2331967488
  elseif QuestLevel >= MyLevel + 4 then
    QuestColor = 2332000256
  elseif QuestLevel >= MyLevel + 1 then
    QuestColor = 2332032768
  elseif QuestLevel >= MyLevel - 2 then
    QuestColor = 2315321088
  else
    QuestColor = 2325782688
  end
  return QuestColor
end
function _QB_UpdateAcceptedQuestList()
  local acceptedQusetNum = GetAcceptedQuestNum()
  if acceptedQusetNum == 0 then
    if gUI.Quest.Board:IsVisible() then
      Lua_UI_ShowMessageInfo("没有已接取任务！")
    end
    _QB_UpdateCanAcceptQuestList()
    return
  end
  gUI.Quest.BoardAcceptedNum:SetVisible(true)
  gUI.Quest.BoardAcceptedNum:SetProperty("Text", string.format(_QB_QuestNumText, acceptedQusetNum))
  gUI.Quest.wndCanAcceptTree:SetProperty("Visible", "false")
  gUI.Quest.wndQuestBoardTree:SetProperty("Visible", "true")
  gUI.Quest.BoardTrack:SetProperty("Visible", "true")
  gUI.Quest.wndAbandon:SetProperty("Text", "放弃任务")
  gUI.Quest.cbNearLevel:SetProperty("Visible", "false")
  gUI.Quest.wndQuestBoardTree:LockUpdate(true)
  gUI.Quest.wndQuestBoardTree:DeleteAllItems()
  Lua_QB_ClearQuestInfo(gUI.Quest.BoardTask, gUI.Quest.Board)
  local fontColor = 0
  local nTaskNum = 0
  local strMap = ""
  local count = acceptedQusetNum - 1
  for index = count, 0, -1 do
    local mapname, mapid, nQuestLevel, strTitle, IsComplete, bFailed, taskid, bTracking, bDisableAbandon, QuestType, DoneTimes, TotalTimes, QuestKind, nCommission = GetAcceptedQuestList(index)
    if mapname ~= nil then
      local parent = gUI.Quest.wndQuestBoardTree:FindFirstItemWithText(mapname)
      if parent == nil then
        parent = gUI.Quest.wndQuestBoardTree:InsertItem(mapname, nil, "kaiti_11", 4294638330)
        parent:SetUserData(-1)
      end
      strObjective = strObjective
      if v1 then
        strObjective = string.format(strObjective, v1, v2, v3, v4, v5, v6, v7)
      end
      if _QB_QuestKindDesc[QuestKind + 1] ~= nil then
        strTitle = string.format("[%s]", _QB_QuestKindDesc[QuestKind + 1]) .. strTitle
      end
      if nCommission ~= 0 then
        strTitle = strTitle .. _QB_CommissionDesc[nCommission]
      end
      if bFailed then
        strTitle = strTitle .. "(失败)"
      end
      if bTracking then
        strTitle = "(√)" .. strTitle
      end
      if IsComplete then
        strTitle = strTitle .. "(完成)"
      end
      if QuestType == QUEST_TYPE_NORMAL and TotalTimes > 0 then
        strTitle = strTitle .. string.format("(%d/%d)", DoneTimes, TotalTimes)
      end
      if string.len(strTitle) > 31 then
        local asc2Num = 0
        for i = 1, 28 do
          if string.byte(strTitle, i) < 128 then
            asc2Num = asc2Num + 1
          end
        end
        strTitle = string.sub(strTitle, 1, 28 + asc2Num % 2)
        strTitle = strTitle .. "..."
      end
      local QuestColor = _QB_GetQuestTextColor(nQuestLevel, gUI_MainPlayerAttr.Level)
      local subitem = gUI.Quest.wndQuestBoardTree:InsertItem(strTitle, parent, "fzheiti_11", QuestColor)
      subitem:SetUserData(taskid)
      nTaskNum = index + 1
    end
  end
  gUI.Quest.wndQuestBoardTree:LockUpdate(false)
  gUI.Quest.wndQuestBoardTree:SelectFirstChild(1)
  gUI.Quest.tabQuestType:SelectItem(0)
end
function _QB_UpdateAcceptedQuestSingle(questID)
  if questID > 0 and questID == _QB_CurrSelectedQuest then
    Lua_QB_ShowAcceptedQuestInfo(questID, gUI.Quest.BoardTask, gUI.Quest.Board, gUI_QuestRewardItemSourceID.QUEST_WINDOW)
  end
  UpdateTrackQuestList()
end
function _QB_UpdateCanAcceptQuestList()
  gUI.Quest.BoardAcceptedNum:SetVisible(false)
  gUI.Quest.wndQuestBoardTree:SetProperty("Visible", "false")
  gUI.Quest.wndCanAcceptTree:SetProperty("Visible", "true")
  gUI.Quest.BoardTrack:SetProperty("Visible", "false")
  gUI.Quest.wndAbandon:SetProperty("Text", "去接任务")
  gUI.Quest.cbNearLevel:SetProperty("Visible", "true")
  gUI.Quest.BoardShareBtn:SetProperty("Enable", "false")
  gUI.Quest.wndCanAcceptTree:LockUpdate(true)
  gUI.Quest.wndCanAcceptTree:DeleteAllItems()
  local NearLevel = gUI.Quest.cbNearLevel:IsChecked()
  UpdateCanAcceptTaskList(NearLevel)
  gUI.Quest.wndCanAcceptTree:LockUpdate(false)
  gUI.Quest.wndCanAcceptTree:SelectFirstChild(1)
  gUI.Quest.tabQuestType:SelectItem(1)
end
function _QB_UpdateTrackQuestList()
  if _QB_CurrSelectedQuest ~= 0 then
    local selectedItem = gUI.Quest.wndQuestBoardTree:GetSelectedItem()
    if selectedItem then
      local selText = selectedItem:GetText()
      local TrackState = gUI.Quest.BoardTrack:IsChecked()
      TrackQuest(_QB_CurrSelectedQuest, TrackState)
      if TrackState then
        selectedItem:SetText("(√)" .. selText)
      else
        local _, _, text = string.find(selText, "%(√%)(.+)")
        if text then
          selectedItem:SetText(text)
        end
      end
    end
  end
  UpdateTrackQuestList()
end
function _QB_IsHasItem(GoodsBox)
  local itemID = GoodsBox:GetItemData(0)
  local bagType, itemIndex = GetItemInBagIndex(itemID)
  if bagType and itemIndex then
    local _, _, _, _, duration, rest = GetItemExInfo(bagType, itemIndex)
    if duration ~= -1 then
      GoodsBox:SetItemCoolTime(0, duration, rest)
    else
      GoodsBox:ClearItemCoolTime(0)
    end
    GoodsBox:SetItemsLock(0, 1, false)
  else
    GoodsBox:SetItemsLock(0, 1, true)
  end
end
function _QB_DoneMsg(strTitle, QuestId)
  Lua_Chat_ShowOSD("QUEST_DONE", strTitle)
  local acceptId, compId, finiId = GetQuestPaopaoInfo(QuestId)
  if finiId and finiId ~= -1 then
    Lua_Chat_ApplyPaoPao(finiId)
  end
  local wnd = WindowSys_Instance:GetWindow("EfxNoLoop_pic.Quest_pic")
  wnd:setEffectFile("efxc_ui_xianshi_wanchengrenwu")
  WorldStage:playSoundByID(41)
end
function _OnQB_TransMessageBox(questID)
  local mapid, posx, posy = GetQuestTransInfo(questID)
  mapName = GetMapInfo(mapid)
  Messagebox_Show("TRANSMISSION_QUEST", questID, mapName, posx, posy)
end
function _OnQB_AddCanAcceptQuest(strTitle, nQuestID, mapname, mapid, nQuestLevelMin, mylevel, track, QuestKind, QuestType, CurrTimes, MaxTimes, IsShowIn, Other)
  local QuestColor = _QB_GetQuestTextColor(nQuestLevelMin, mylevel)
  local parent = gUI.Quest.wndCanAcceptTree:FindFirstItemWithText(mapname)
  if not parent then
    parent = gUI.Quest.wndCanAcceptTree:InsertItem(mapname, nil, "kaiti_11", 4294638330)
    parent:SetUserData(-1)
  end
  if _QB_QuestKindDesc[QuestKind + 1] ~= nil then
    strTitle = string.format("(%d)", nQuestLevelMin) .. string.format("[%s]", _QB_QuestKindDesc[QuestKind + 1]) .. strTitle
    if QuestType == QUEST_TYPE_NORMAL and MaxTimes > 0 then
      strTitle = strTitle .. string.format("(%d/%d)", CurrTimes, MaxTimes)
    end
  end
  if string.len(strTitle) > 31 then
    local asc2Num = 0
    for i = 1, 28 do
      if string.byte(strTitle, i) < 128 then
        asc2Num = asc2Num + 1
      end
    end
    strTitle = string.sub(strTitle, 1, 28 + asc2Num % 2)
    strTitle = strTitle .. "..."
  end
  if IsShowIn == 1 then
    local subitem = gUI.Quest.wndCanAcceptTree:InsertItem(strTitle, parent, "fzheiti_11", QuestColor)
    subitem:SetUserData(nQuestID)
  end
end
function _OnQB_AddCanAcceptQuestEnd(CanAcceptCount)
  gUI.Quest.wndCanAcceptTree:LockUpdate(false)
end
function _OnQB_PlayerAcceptNewQuest(QuestId, MapId, ChapterPictureStr, QuestTitle)
  _QB_UpdateTrackQuestList()
  _QB_UpdateCanAcceptQuestList()
  _QB_UpdateAcceptedQuestList()
  if QuestTitle then
    Lua_Chat_AddSysLog("你接取了任务[" .. QuestTitle .. "]")
    local wnd = WindowSys_Instance:GetWindow("EfxNoLoop_pic.Quest_pic")
    wnd:setEffectFile("efxc_ui_xianshi_jiequrenwu")
  end
  local acceptId, compId, finiId = GetQuestPaopaoInfo(QuestId)
  if acceptId and acceptId ~= -1 then
    Lua_Chat_ApplyPaoPao(acceptId)
  end
  WorldStage:playSoundByID(9)
end
function _OnQB_PlayerQuestComplete(QuestTitle, IsQuestDone, QuestId)
  if IsQuestDone > 0 then
    ShowErrorMessage("任务[" .. QuestTitle .. "]已完成")
    Lua_Chat_AddSysLog("完成了[" .. QuestTitle .. "]任务")
    local acceptId, compId, finiId = GetQuestPaopaoInfo(QuestId)
    if compId and compId ~= -1 then
      Lua_Chat_ApplyPaoPao(compId)
    end
  else
    ShowErrorMessage("任务[" .. QuestTitle .. "]失败,请放弃后重新接取")
    Lua_Chat_AddSysLog("[" .. QuestTitle .. "]任务失败")
    WorldStage:playSoundByID(10)
  end
end
function _OnQB_QuestShowUseItem(IsShow, ItemId, Icon, Locked)
  local wnd = WindowSys_Instance:GetWindow("UseItemBack")
  wnd:SetProperty("Visible", tostring(arg1))
  if arg1 then
    wnd = wnd:GetChildByName("GoodsBox")
    wnd = WindowToGoodsBox(wnd)
    wnd:SetItemData(0, ItemId)
    wnd:SetItemGoods(0, Icon, Lua_Bag_GetQuality(-1, GoodsId))
    wnd:SetItemsLock(0, 1, Locked)
  end
end
function _OnQB_TrackAddBegin(TrackingCount)
  if gUI.Quest.BoardTableCtrl:GetSelectItem() ~= 0 then
    return
  end
  if not gUI.Quest.wndQuestContainer:IsVisible() then
    return
  end
  local scrollbar = WindowSys_Instance:GetWindow("QuestTrackHeader.Container_cnt.vertSB_")
  if scrollbar then
    scrollbar = WindowToScrollBar(scrollbar)
    _QB_BarPos = scrollbar:GetCurrentPos()
  end
  gUI.Quest.wndQuestContainer:DeleteAll()
  local Floor = WindowSys_Instance:GetWindow("Template.Scoreboard_pic")
  local bShow = Floor:GetCustomUserData(0)
  if bShow == 1 then
    local tmp = gUI.Quest.wndQuestContainer:Insert(-1, Floor)
    tmp:SetVisible(true)
  end
  _QB_TrackTable = {}
  _QB_TrackTable.DisplayBox = {}
  _QB_TrackCount = 0
  _QB_TrackTotalCount = TrackingCount
  if TrackingCount == 0 then
    local str2 = "您目前未追踪任务{OpEnUI^(前往任务界面)^1}\n任务追踪小技巧：\n1.所有的任务在接取后都会被默认为追踪状态。\n2.更好的规划自己的任务将提高您的升级速度。"
    local text = gUI.Quest.wndQuestContainer:Insert(-1, gUI.Quest.wndQuestAccepted)
    text:SetProperty("MouseTrans", "false")
    text = WindowToDisplayBox(text)
    text:ClearAllContent()
    text:InsertBack(str2, 0, 0, 0)
  end
end
function _OnQB_TrackAddEnd(TrackingCount)
  local scrollbar = WindowSys_Instance:GetWindow("QuestTrackHeader.Container_cnt.vertSB_")
  if scrollbar and scrollbar:IsVisible() and _QB_BarPos then
    scrollbar = WindowToScrollBar(scrollbar)
    scrollbar:SetCurrentPos(_QB_BarPos)
  end
  for TrackIndex, displaybox in ipairs(_QB_TrackTable.DisplayBox) do
    displaybox:UpdateHyperLinkStatus()
  end
  gUI.Quest.wndQuestContainer:setSizeSelfMax(220)
  gUI.Quest.wndQuestContainer:SizeSelf()
  RefreshCurrentUserGuide()
end

function _OnQB_TrackAddItem(Title, IsComplete, IsFailed, Desc, ReqLevel, MyLevel, QuestId, nCommission, nQuestKind, nTimeLimitType)
  if gUI.Quest.BoardTableCtrl:GetSelectItem() ~= 0 then
    return
  end
  if not gUI.Quest.wndQuestContainer:IsVisible() then
    return
  end
  _QB_TrackCount = _QB_TrackCount + 1
  if _QB_TrackCount <= _QB_TRACKMAXNUM then
    local QuestColor = 4294967140
    local str = ""
    local index = gUI.Quest.wndQuestQuestKind:GetSelectItem()
    local kind = gUI.Quest.wndQuestQuestKind:GetItemData(index)
    if _QB_QuestKindDesc[nQuestKind + 1] ~= nil and kind ~= 0 then
      local bFind = false
      for x = 0, #_QB_QuestKind[kind].Kind do
        if _QB_QuestKind[kind].Kind[x] == nQuestKind + 1 then
          bFind = true
        end
      end
      if not bFind then
        return
      end
    end
    if gUI.Quest.wndQuestCompleted:IsChecked() and (not IsComplete or IsComplete == 0) then
      return
    end
    if gUI.Quest.wndQuestInCompleted:IsChecked() and IsComplete and IsComplete ~= 0 then
      return
    end
    if _QB_QuestKindDesc[nQuestKind + 1] ~= nil then
      str = string.format("[%s]", _QB_QuestKindDesc[nQuestKind + 1])
    end
    if IsComplete and IsComplete ~= 0 then
      str = str .. "(完成)"
      QuestColor = 4278255615
    elseif IsFailed and IsFailed ~= 0 then
      str = str .. "(失败)"
      QuestColor = 4294901760
    end
    str = str .. Title
    str = str .. string.format(" {QuClOsE^%d}", QuestId)
    local showTime = false
    if nTimeLimitType > 0 then
      showTime = not IsComplete or IsComplete == 0
    else
      showTime = true
    end
    if showTime then
      if IsFailed and IsFailed == 0 then
        local time = CT(130, QuestId)
        if time then
          str = str .. "\n{#G^剩余时间：|" .. time .. "|}      "
        end
      elseif nCommission ~= 0 then
        local time = CT(131, QuestId)
        str = str .. _QB_CommissionDesc[nCommission]
        if time then
          str = str .. "\n{#G^剩余时间：|" .. time .. "|}      "
        end
      end
    end
    local text = gUI.Quest.wndQuestContainer:Insert(-1, gUI.Quest.wndQuestAccepted)
    text:SetProperty("MouseTrans", "false")
    text = WindowToDisplayBox(text)
    text:ClearAllContent()
    text:InsertBack(str, QuestColor, QuestId, 0)
    text:InsertBack(Desc, 4294638330, QuestId, 0)
    _QB_TrackTable.DisplayBox[_QB_TrackCount] = text
    _QB_TrackTable[_QB_TrackCount] = {}
    local itemIDList = GetGoodsBoxItemIDFromDisplayBoxName(text:GetAbsWindowName())
    if itemIDList then
      for index, itemID in pairs(itemIDList) do
        local GoodsBox = text:GetGoodsBox(itemID)
        if GoodsBox then
          _QB_IsHasItem(GoodsBox)
          if IsComplete and IsComplete ~= 0 then
            GoodsBox:SetVisible(false)
          end
        end
        _QB_TrackTable[_QB_TrackCount][index] = GoodsBox
      end
    end
  end
end
function _OnQB_UpdateRewardItem(Index, GoodsId, Icon, GoodsCount, sourceId)
  if sourceId == gUI_QuestRewardItemSourceID.QUEST_WINDOW then
    local BoardFixGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("QuestBoard.Task_cnt.fix_gbox"))
    BoardFixGoodsBox:SetItemGoods(Index, Icon, Lua_Bag_GetQuality(-1, GoodsId))
    BoardFixGoodsBox:SetItemData(Index, GoodsId)
    if GoodsCount > 1 then
      BoardFixGoodsBox:SetItemSubscript(Index, tostring(GoodsCount))
    end
  elseif sourceId == gUI_QuestRewardItemSourceID.GUILD_QUEST_WINDOW then
    local BoardFixGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg.Image_bg.Right_bg.Quest_cnt.Reward_gbox"))
    BoardFixGoodsBox:SetItemGoods(Index, Icon, Lua_Bag_GetQuality(-1, GoodsId))
    BoardFixGoodsBox:SetItemData(Index, GoodsId)
    if GoodsCount > 1 then
      BoardFixGoodsBox:SetItemSubscript(Index, tostring(GoodsCount))
    end
  elseif sourceId == gUI_QuestRewardItemSourceID.MAP_TOOLTIP then
    gUI.Quest.QuestRewardItemList = {
      [Index] = GoodsId
    }
  end
end
function _OnQB_UpdateRewardChoicesItem(Index, GoodsId, Icon, GoodsCount, sourceId)
  if sourceId == gUI_QuestRewardItemSourceID.QUEST_WINDOW then
    local BoardChoiceGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("QuestBoard.Task_cnt.choice_gbox"))
    BoardChoiceGoodsBox:SetItemGoods(Index, Icon, Lua_Bag_GetQuality(-1, GoodsId))
    BoardChoiceGoodsBox:SetItemData(Index, GoodsId)
    if GoodsCount > 1 then
      BoardChoiceGoodsBox:SetItemSubscript(Index, tostring(GoodsCount))
    end
  elseif sourceId == gUI_QuestRewardItemSourceID.GUILD_QUEST_WINDOW then
    local BoardChoiceGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("GuildMain.GuildInfo04_bg.Right_bg.Task_cnt.choice_gbox"))
    BoardChoiceGoodsBox:SetItemGoods(Index, Icon, Lua_Bag_GetQuality(-1, GoodsId))
    BoardChoiceGoodsBox:SetItemData(Index, GoodsId)
    if GoodsCount > 1 then
      BoardChoiceGoodsBox:SetItemSubscript(Index, tostring(GoodsCount))
    end
  elseif sourceId == gUI_QuestRewardItemSourceID.MAP_TOOLTIP then
    gUI.Quest.QuestRewardItemList = {
      [#gUI.Quest.QuestRewardItemList + 1] = GoodsId
    }
  end
end
function _OnQB_UpdateTrackItemState(bagType, bagIndex)
  if bagType ~= gUI_GOODSBOX_BAG.backpack1 then
    return
  end
  for TrackIndex, value in ipairs(_QB_TrackTable) do
    for GoodsBoxIdx, GoodsBox in ipairs(value) do
      if GoodsBox then
        _QB_IsHasItem(GoodsBox)
      end
    end
  end
end
function _OnQB_AskShareQuest(strPlayerName, strQuestTitle, nPlayerID, nQuestID)
  Messagebox_Show("ASK_SHAREQUEST", strPlayerName, strQuestTitle, nPlayerID, nQuestID)
end
function _OnQB_SetItemCoolDown(slot, duration, rest, isTask, tableID)
  if isTask and tableID ~= 0 then
    for TrackIndex, value in ipairs(_QB_TrackTable) do
      for GoodsBoxIdx, GoodsBox in ipairs(value) do
        if GoodsBox and GoodsBox:GetItemData(0) == tableID then
          if rest == 0 then
            GoodsBox:ClearItemCoolTime(0)
          else
            GoodsBox:SetItemCoolTime(0, duration, rest)
          end
        end
      end
    end
  end
end
function _OnQB_YabiaoMessageBox(questID, cmd, questComment)
  if not gUI.Quest.YaBiao:IsVisible() then
    gUI.Quest.YaBiao:SetVisible(true)
  end
  local _, _, _, _, level_self = GetPlaySelfProp(4)
  gUI.Quest.YaBiao:SetCustomUserData(0, questID)
  local text1 = string.format("本次即将押送%s, 是否接受", questComment)
  if cmd == 0 then
    local money = tonumber(level_self) * 4000 - 60000
    text1 = text1 .. "\n(本次重置将花费" .. Lua_UI_Money2String(money) .. ")"
  else
    text1 = text1 .. "\n(本次重置将花费{#G^5}钻石)"
  end
  gUI.Quest.YaBiaoLavel:SetProperty("Text", text1)
  gUI.Quest.YaBiaoConfirm:SetCustomUserData(0, questID)
  gUI.Quest.YaBiaoReset:SetCustomUserData(0, cmd)
end
function UI_QB_NearLevelClick(msg)
  _QB_UpdateCanAcceptQuestList()
end
function UI_QB_TrackQuestChecked(msg)
  _QB_UpdateTrackQuestList()
end
function UI_QB_CloseBtnClick(msg)
  local QuestBoard = WindowSys_Instance:GetWindow("QuestBoard")
  QuestBoard:SetProperty("Visible", "false")
end
function UI_QuestBoard_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("QuestBoard.Help")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function Lua_QB_ShowAcceptedQuestInfo(QuestId, BoardTask, BoardParent, SourceId)
  local BoradAccept = BoardParent:GetGrandChild("acceptable")
  local BoardTrack = WindowToCheckButton(BoardParent:GetGrandChild("TrackQuest_cbtn"))
  local BoardTime = BoardParent:GetGrandChild("time")
  local BoardWndAbandon = BoardParent:GetGrandChild("GrveUp")
  local BoardShareBtn = BoardParent:GetGrandChild("Share_btn")
  local BoardCommitBtn = BoardParent:GetGrandChild("Commit_btn")
  local BoardTitle = BoardParent:GetGrandChild("MissName_dlab")
  local BoardCompleteFlag = BoardParent:GetGrandChild("CompleteFlag_pic")
  local BoardFixGoodsBox = WindowToGoodsBox(BoardTask:GetGrandChild("fix_gbox"))
  local BoardChoiceGoodsBox = WindowToGoodsBox(BoardTask:GetGrandChild("choice_gbox"))
  local BoardAward = WindowToDisplayBox(BoardTask:GetGrandChild("award_dbox"))
  local BoardRevertTitle = BoardTask:GetGrandChild("revert_bg")
  local BoardRevert = WindowToDisplayBox(BoardTask:GetGrandChild("revert_dbox"))
  local BoardTerm = WindowToDisplayBox(BoardTask:GetGrandChild("term_dbox"))
  local BoardInfo = WindowToDisplayBox(BoardTask:GetGrandChild("info_dbox"))
  local BoardChoiceTitle = BoardTask:GetGrandChild("choice_bg")
  local BoardTermTitle = BoardTask:GetGrandChild("term_bg.term_bg")
  if BoradAccept ~= nil then
    BoradAccept:SetProperty("Visible", "false")
  end
  BoardTask:SetProperty("Visible", "true")
  BoardTask:ResetScrollBar()
  BoardFixGoodsBox:ResetAllGoods(true)
  BoardChoiceGoodsBox:ResetAllGoods(true)
  BoardAward:ClearAllContent()
  BoardRevert:ClearAllContent()
  BoardTerm:ClearAllContent()
  BoardInfo:ClearAllContent()
  local strTitle, strObjective, strIncomplete, nQuestLevel, nStatus, strQuestDetail, bDisableAbandon, MapName, NpcName, nCurrTimes, nTotalTimes, nQuestType, bCalLimitTime, IsTracking, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, IsGuildExp, bHaveChoiceItem, nCommission, v1, v2, v3, v4, v5, v6, v7 = GetQuestInfoLua(QuestId, SourceId)
  if v1 then
    strObjective = string.format(strObjective, v1, v2, v3, v4, v5, v6, v7)
  end
  if BoardTime ~= nil then
    if bCalLimitTime then
      local str = string.format("剩余时间: $CT(125,%d)$", QuestId)
      BoardTime:SetProperty("Text", str)
      BoardTime:SetProperty("Visible", "true")
      local tr = CT(130, QuestId)
    else
      BoardTime:SetProperty("Visible", "false")
    end
  end
  if strTitle then
    BoardTitle:SetProperty("Text", strTitle)
    BoardTitle:SetProperty("Visible", "true")
  else
    BoardTitle:SetProperty("Visible", "false")
  end
  local strReward = ""
  if RewExp and RewExp > 0 then
    strReward = strReward .. string.format("经验值：%d点\n", RewExp)
    if IsGuildExp > 0 then
      local currentExp, nextExp = GetGuildLevMulitExp()
      if currentExp > 0 then
        strReward = strReward .. string.format("(当前帮会等级加成经验值: %d)\n", tonumber(RewExp * currentExp / 100))
      end
      if nextExp > 0 and currentExp > 0 then
        strReward = strReward .. string.format("(下一级加成经验值: %d)\n", tonumber(RewExp * nextExp / 100))
      end
    end
  end
  if RewNimbus and RewNimbus > 0 then
    strReward = strReward .. string.format("精魄值：%d点\n", RewNimbus)
  end
  if RewMoney and RewMoney > 0 then
    strReward = strReward .. "金  币：" .. Lua_UI_Money2String(RewMoney) .. "\n"
  end
  if RewBindedMoney and RewBindedMoney > 0 then
    strReward = strReward .. "绑  金：" .. Lua_UI_Money2String(RewBindedMoney) .. "\n"
  end
  if RewWrathValue and RewWrathValue ~= 0 then
    strReward = strReward .. "减少天诛值：" .. tostring(-RewWrathValue) .. "\n"
  end
  if GuildMoney and GuildMoney > 0 then
    strReward = strReward .. string.format("帮会资金：%s\n", Lua_UI_Money2String(GuildMoney * 10000))
  end
  if GuildActive and GuildActive > 0 then
    strReward = strReward .. string.format("帮会活跃值：%d点\n", GuildActive)
  end
  if GuildContribution and GuildContribution > 0 then
    strReward = strReward .. string.format("帮会贡献度：%d点\n", GuildContribution)
  end
  if GuildConstruction and GuildConstruction > 0 then
    strReward = strReward .. string.format("帮会发展度：%d点\n", GuildConstruction)
  end
  if GuildPractice and GuildPractice > 0 then
    strReward = strReward .. string.format("守护兽成长值：%d点\n", GuildPractice)
  end
  if strReward ~= "" then
    BoardAward:setEnableHeightCal(true)
    BoardAward:InsertBack(strReward, 0, 0, 0)
  else
    BoardAward:setEnableHeightCal(false)
  end
  if BoardFixGoodsBox:IsItemHasIcon(0) then
    BoardFixGoodsBox:setEnableHeightCal(true)
  else
    BoardFixGoodsBox:setEnableHeightCal(false)
  end
  if bHaveChoiceItem and BoardChoiceGoodsBox:IsItemHasIcon(0) then
    BoardChoiceGoodsBox:setEnableHeightCal(true)
    BoardChoiceTitle:setEnableHeightCal(true)
  else
    BoardChoiceGoodsBox:setEnableHeightCal(false)
    BoardChoiceTitle:setEnableHeightCal(false)
  end
  if NpcName == "" or NpcName == "npc" then
    BoardRevertTitle:setEnableHeightCal(false)
    BoardRevert:setEnableHeightCal(false)
  else
    BoardRevertTitle:setEnableHeightCal(true)
    BoardRevert:setEnableHeightCal(true)
    BoardRevert:InsertBack(MapName .. " -> " .. NpcName, 0, 0, 0)
  end
  BoardTermTitle:SetProperty("BackImage", "UiIamge013:Quest_Condition")
  if strObjective then
    strObjective = Lua_UI_TranslateText(strObjective)
    BoardTerm:InsertBack(strObjective, 0, 0, 0)
  end
  if strQuestDetail then
    strQuestDetail = Lua_UI_TranslateText(strQuestDetail)
    BoardInfo:InsertBack(strQuestDetail, 0, 0, 0)
  end
  BoardTask:SizeSelf()
  if nStatus == QUEST_STATUS_DONE or nStatus == QUEST_STATUS_COMPLETE then
    BoardCompleteFlag:SetProperty("Visible", "true")
  else
    BoardCompleteFlag:SetProperty("Visible", "false")
  end
  if BoardWndAbandon ~= nil then
    if bDisableAbandon then
      BoardWndAbandon:SetProperty("Enable", "false")
    elseif nCommission == 1 then
      BoardWndAbandon:SetProperty("Enable", "false")
    else
      BoardWndAbandon:SetProperty("Enable", "true")
    end
  end
  _QB_CurrSelectedQuest = 0
  if BoardTrack ~= nil then
    BoardTrack:SetProperty("Visible", "true")
    BoardTrack:SetChecked(IsTracking)
  end
  _QB_CurrSelectedQuest = QuestId
  if BoardShareBtn ~= nil then
    local bVal = IsQuestCanShareByQuestID(QuestId)
    if bVal then
      BoardShareBtn:SetProperty("Enable", "true")
    else
      BoardShareBtn:SetProperty("Enable", "false")
    end
  end
  if BoardCommitBtn ~= nil then
    if nStatus == QUEST_STATUS_COMPLETE and NpcName == "" then
      BoardCommitBtn:SetVisible(true)
      BoardCommitBtn:SetCustomUserData(0, QuestId)
    else
      BoardCommitBtn:SetVisible(false)
    end
  end
end
function Lua_QB_ShowCanAcceptQuestInfo(QuestId, BoardTask, BoardParent)
  local BoradAccept = BoardParent:GetGrandChild("acceptable")
  local BoardTime = BoardParent:GetGrandChild("time")
  local BoardWndAbandon = BoardParent:GetGrandChild("GrveUp")
  local BoardTitle = BoardParent:GetGrandChild("MissName_dlab")
  local BoardCompleteFlag = BoardParent:GetGrandChild("CompleteFlag_pic")
  local BoardFixGoodsBox = WindowToGoodsBox(BoardTask:GetGrandChild("fix_gbox"))
  local BoardChoiceGoodsBox = WindowToGoodsBox(BoardTask:GetGrandChild("choice_gbox"))
  local BoardAward = WindowToDisplayBox(BoardTask:GetGrandChild("award_dbox"))
  local BoardRevertTitle = BoardTask:GetGrandChild("revert_bg")
  local BoardRevert = WindowToDisplayBox(BoardTask:GetGrandChild("revert_dbox"))
  local BoardTerm = WindowToDisplayBox(BoardTask:GetGrandChild("term_dbox"))
  local BoardInfo = WindowToDisplayBox(BoardTask:GetGrandChild("info_dbox"))
  local BoardChoiceTitle = BoardTask:GetGrandChild("choice_bg")
  local BoardTermTitle = BoardTask:GetGrandChild("term_bg.term_bg")
  if BoradAccept ~= nil then
    BoradAccept:SetProperty("Visible", "false")
  end
  BoardTask:SetProperty("Visible", "true")
  BoardTask:ResetScrollBar()
  BoardFixGoodsBox:ResetAllGoods(true)
  BoardChoiceGoodsBox:ResetAllGoods(true)
  BoardAward:ClearAllContent()
  BoardRevert:ClearAllContent()
  BoardTerm:ClearAllContent()
  BoardInfo:ClearAllContent()
  local strTitle, strQuestDetail
  local mapname = ""
  local npcname = ""
  local strTitle, mapname, npcname, strQuestDetail, strObjective, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, bHaveChoiceItem, IsGuildExp = GetCanAcceptQuestInfo(QuestId)
  _QB_CurrentNpcLink = npcname
  if BoardTime ~= nil then
    BoardTime:SetProperty("Visible", "false")
  end
  if strTitle then
    BoardTitle:SetProperty("Text", strTitle)
    BoardTitle:SetProperty("Visible", "true")
  else
    BoardTitle:SetProperty("Visible", "false")
  end
  local strReward = ""
  if RewExp and RewExp > 0 then
    strReward = strReward .. string.format("经验值：%d点\n", RewExp)
    if IsGuildExp > 0 then
      local currentExp, nextExp = GetGuildLevMulitExp()
      if currentExp > 0 then
        strReward = strReward .. string.format("(当前帮会等级加成经验值: %d)\n", tonumber(RewExp * currentExp / 100))
      end
      if nextExp > 0 and currentExp > 0 then
        strReward = strReward .. string.format("(下一级加成经验值: %d)\n", tonumber(RewExp * nextExp / 100))
      end
    end
  end
  if RewNimbus and RewNimbus > 0 then
    strReward = strReward .. string.format("精魄值：%d点\n", RewNimbus)
  end
  if RewMoney and RewMoney > 0 then
    strReward = strReward .. "金  币：" .. Lua_UI_Money2String(RewMoney) .. "\n"
  end
  if RewBindedMoney and RewBindedMoney > 0 then
    strReward = strReward .. "绑  金：" .. Lua_UI_Money2String(RewBindedMoney) .. "\n"
  end
  if RewWrathValue and RewWrathValue ~= 0 then
    strReward = strReward .. "减少天诛值：" .. tostring(-RewWrathValue) .. "\n"
  end
  if GuildMoney and GuildMoney > 0 then
    strReward = strReward .. string.format("帮会资金：%s\n", Lua_UI_Money2String(GuildMoney * 10000))
  end
  if GuildActive and GuildActive > 0 then
    strReward = strReward .. string.format("帮会活跃值：%d点\n", GuildActive)
  end
  if GuildContribution and GuildContribution > 0 then
    strReward = strReward .. string.format("帮会贡献度：%d点\n", GuildContribution)
  end
  if GuildConstruction and GuildConstruction > 0 then
    strReward = strReward .. string.format("帮会发展度：%d点\n", GuildConstruction)
  end
  if GuildPractice and GuildPractice > 0 then
    strReward = strReward .. string.format("守护兽成长值：%d点\n", GuildPractice)
  end
  if strReward ~= "" then
    BoardAward:setEnableHeightCal(true)
    BoardAward:InsertBack(strReward, 0, 0, 0)
  else
    BoardAward:setEnableHeightCal(false)
  end
  if BoardFixGoodsBox:IsItemHasIcon(0) then
    BoardFixGoodsBox:setEnableHeightCal(true)
  else
    BoardFixGoodsBox:setEnableHeightCal(false)
  end
  if bHaveChoiceItem and BoardChoiceGoodsBox:IsItemHasIcon(0) then
    BoardChoiceGoodsBox:setEnableHeightCal(true)
    BoardChoiceTitle:setEnableHeightCal(true)
  else
    BoardChoiceGoodsBox:setEnableHeightCal(false)
    BoardChoiceTitle:setEnableHeightCal(false)
  end
  BoardRevertTitle:setEnableHeightCal(false)
  BoardRevert:setEnableHeightCal(false)
  BoardTermTitle:SetProperty("BackImage", "UiIamge013:Quest_QuestAccept")
  BoardTerm:InsertBack(mapname .. " -> " .. npcname, 0, 0, 0)
  if strQuestDetail then
    strQuestDetail = Lua_UI_TranslateText(strQuestDetail)
    BoardInfo:InsertBack(strQuestDetail, 0, 0, 0)
  end
  BoardCompleteFlag:SetProperty("Visible", "false")
  if BoardWndAbandon ~= nil then
    BoardWndAbandon:SetProperty("Enable", "true")
  end
  BoardTask:SizeSelf()
end
function Lua_QB_ClearQuestInfo(BoardTask, BoardParent)
  local BoradAccept = BoardParent:GetGrandChild("acceptable")
  local BoardWndAbandon = BoardParent:GetGrandChild("GrveUp")
  local BoardCommitBtn = BoardParent:GetGrandChild("Commit_btn")
  local BoardTitle = BoardParent:GetGrandChild("MissName_dlab")
  local BoardCompleteFlag = BoardParent:GetGrandChild("CompleteFlag_pic")
  local BoardFixGoodsBox = WindowToGoodsBox(BoardTask:GetGrandChild("fix_gbox"))
  local BoardChoiceGoodsBox = WindowToGoodsBox(BoardTask:GetGrandChild("choice_gbox"))
  local BoardAward = WindowToDisplayBox(BoardTask:GetGrandChild("award_dbox"))
  local BoardRevertTitle = BoardTask:GetGrandChild("revert_bg")
  local BoardRevert = WindowToDisplayBox(BoardTask:GetGrandChild("revert_dbox"))
  local BoardTerm = WindowToDisplayBox(BoardTask:GetGrandChild("term_dbox"))
  local BoardInfo = WindowToDisplayBox(BoardTask:GetGrandChild("info_dbox"))
  BoardAward:ClearAllContent()
  BoardRevert:ClearAllContent()
  BoardTerm:ClearAllContent()
  BoardInfo:ClearAllContent()
  BoardFixGoodsBox:ResetAllGoods(true)
  BoardChoiceGoodsBox:ResetAllGoods(true)
  BoardCompleteFlag:SetProperty("Visible", "false")
  BoardTitle:SetProperty("Visible", "false")
  BoardTask:SetProperty("Visible", "false")
  if BoardWndAbandon ~= nil then
    BoardWndAbandon:SetProperty("Enable", "false")
  end
  if BoardCommitBtn ~= nil then
    BoardCommitBtn:SetVisible(false)
  end
  if BoradAccept ~= nil then
    _QB_CurrSelectedQuest = 0
  end
end
function UI_QB_GiveupBtnClick(msg)
  local select = gUI.Quest.tabQuestType:GetSelectItem()
  if select == 0 then
    Messagebox_Show("GIVE_UP_TASK_OR_NOT", _QB_CurrSelectedQuest)
  else
    local x, y, mapid = string.match(_QB_CurrentNpcLink, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)")
    if tonumber(mapid) == -1 then
      TalkToNpc(tonumber(x), tonumber(y))
    else
      MoveToTargetLocation(tonumber(x), tonumber(y), tonumber(mapid))
    end
  end
end
function UI_QB_Show(msg)
  local BoardTitle = WindowSys_Instance:GetWindow("QuestBoard.MissName_dlab")
  if not gUI.Quest.Board:IsVisible() then
    gUI.Quest.Board:SetProperty("Visible", "true")
    BoardTitle:SetProperty("Visible", "false")
    _QB_UpdateAcceptedQuestList()
  else
    gUI.Quest.Board:SetProperty("Visible", "false")
  end
  WorldStage:playSoundByID(35)
end
function UI_QB_AcceptedTreeItemChanged(msg)
  local QuestId = msg:get_lparam()
  if IsKeyPressed(29) and _QB_MouseInCtrlList and gUI.Quest.Board:IsVisible() and QuestId and QuestId >= 0 then
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
    WorldGroup_SetInputCapture()
    local strTitle = GetQuestInfoLua(QuestId, gUI_QuestRewardItemSourceID.INVALID)
    local str = "{QuEsT^" .. strTitle .. "^" .. QuestId .. "}"
    _Chat_AddTextToInputBox(str)
  end
  if QuestId >= 0 then
    Lua_QB_ShowAcceptedQuestInfo(QuestId, gUI.Quest.BoardTask, gUI.Quest.Board, gUI_QuestRewardItemSourceID.QUEST_WINDOW)
    if IsKeyPressed(42) or IsKeyPressed(54) then
    end
  else
    Lua_QB_ClearQuestInfo(gUI.Quest.BoardTask, gUI.Quest.Board)
  end
end
function UI_QB_CanAcceptTreeItemChanged(msg)
  local questID = msg:get_lparam()
  if IsKeyPressed(29) and _QB_MouseInCtrlList and gUI.Quest.Board:IsVisible() and questID and questID >= 0 then
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
    WorldGroup_SetInputCapture()
    local strTitle = GetQuestInfoLua(questID, gUI_QuestRewardItemSourceID.INVALID)
    local str = "{QuEsT^" .. strTitle .. "^" .. questID .. "}"
    _Chat_AddTextToInputBox(str)
  end
  if questID >= 0 then
    _QB_CurrSelectedQuest = questID
    Lua_QB_ShowCanAcceptQuestInfo(questID, gUI.Quest.BoardTask, gUI.Quest.Board)
  else
    Lua_QB_ClearQuestInfo(gUI.Quest.BoardTask, gUI.Quest.Board)
  end
end
function UI_QT_CloseBtnClick(msg)
  if gUI.Quest.wndQuestContainer:IsVisible() then
    gUI.Quest.wndQuestContainer:SetProperty("Visible", "false")
  else
    gUI.Quest.wndQuestContainer:SetProperty("Visible", "true")
    UpdateTrackQuestList()
  end
end
function UI_QB_PagingChanged(msg)
  local wnd = WindowToTableCtrl(msg:get_window())
  local selected = wnd:GetSelectItem()
  Lua_QB_ClearQuestInfo(gUI.Quest.BoardTask, gUI.Quest.Board)
  if selected == 0 then
    _QB_UpdateAcceptedQuestList()
  else
    _QB_UpdateCanAcceptQuestList()
  end
end
function UI_QB_ShowItemTip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local index = msg:get_wparam()
  local count = wnd:GetItemSubscript(index)
  local id = wnd:GetItemData(index)
  if id ~= 0 then
    Lua_Tip_Item(wnd:GetToolTipWnd(0), nil, nil, nil, nil, id)
    Lua_Tip_ShowEquipCompare(wnd, -1, id)
  end
end
function UI_QB_UseItem(msg)
  local goodBox = msg:get_window()
  if goodBox then
    goodBox = WindowToGoodsBox(goodBox)
    local itemID = goodBox:GetItemData(0)
    local bagType, itemIndex = GetItemInBagIndex(itemID)
    if bagType and itemIndex then
      UseItem(bagType, itemIndex)
    else
      WorldGroup_ShowNewStr(UI_SYS_MSG.BAG_NOT_FOUND_ITEM)
    end
  end
end
function UI_QB_ItemToolTip(msg)
  local goodBox = msg:get_window()
  if goodBox then
    goodBox = WindowToGoodsBox(goodBox)
    local itemID = goodBox:GetItemData(0)
    if itemID > 0 then
      local bagType, itemIndex = GetItemInBagIndex(itemID)
      if bagType then
        Lua_Tip_Item(goodBox:GetToolTipWnd(0), itemIndex, bagType, nil, nil, nil)
      else
        Lua_Tip_Item(goodBox:GetToolTipWnd(0), nil, nil, nil, nil, itemID)
      end
    end
  end
end
function UI_QB_CloseButtonClick(msg)
  local btn = msg:get_window()
  if btn then
    _QB_TrackSelectedQuest = btn:GetCustomUserData(0)
    gUI.Quest.AskWnd:SetVisible(true)
    local textWnd = gUI.Quest.AskWnd:GetChildByName("Text_slab")
    local strTitle, _, _, _, _, _, CanAbandon, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, nCommission = GetQuestInfoLua(_QB_TrackSelectedQuest, gUI_QuestRewardItemSourceID.INVALID)
    textWnd:SetProperty("Text", string.format("你可以选择隐藏追踪或放弃任务【%s】\n取消请点击右上角关闭按钮。", strTitle))
    local GiveUp_btn = gUI.Quest.AskWnd:GetChildByName("GiveUp_btn")
    if CanAbandon then
      GiveUp_btn:SetEnable(false)
    elseif nCommission == 1 then
      GiveUp_btn:SetEnable(false)
    else
      GiveUp_btn:SetEnable(true)
    end
  end
end
function UI_QB_CloseAskWnd()
  gUI.Quest.AskWnd:SetVisible(false)
end
function UI_QB_DeletQuest()
  Messagebox_Show("GIVE_UP_TASK_OR_NOT", _QB_TrackSelectedQuest)
  _QB_TrackSelectedQuest = 0
  gUI.Quest.AskWnd:SetVisible(false)
end
function UI_QB_AbandonTrack()
  TrackQuest(_QB_TrackSelectedQuest, false)
  UpdateTrackQuestList()
  _QB_TrackSelectedQuest = 0
  gUI.Quest.AskWnd:SetVisible(false)
  if gUI.Quest.Board:IsVisible() and gUI.Quest.tabQuestType:GetSelectItem() == 0 then
    _QB_UpdateAcceptedQuestList()
  end
end
function UI_QB_CommitBtnClick(msg)
  local wnd = msg:get_window()
  local nQuestID = wnd:GetCustomUserData(0)
  AcceptQuest(-1, nQuestID, DIALOG_STATUS_REWARD, 0)
end
function UI_ShareQuest(msg)
  ShareQuest(_QB_CurrSelectedQuest)
end
function UI_QB_TrackChanged(msg)
  local wnd = WindowToTableCtrl(msg:get_window())
  local selected = wnd:GetSelectItem()
  if selected == 1 then
    gUI.Quest.wndQuestContainer:DeleteAll()
  elseif selected == 0 then
    UpdateTrackQuestList()
  end
end
function UI_QB_TrackCanAcceptShow()
  gUI.Quest.Board:SetVisible(true)
  _QB_UpdateCanAcceptQuestList()
end
function UI_QB_TrackKindChanged(msg)
  UpdateTrackQuestList()
end
function UI_QB_TrackIsCompleteChanged(msg)
  local wnd = msg:get_window()
  local select = wnd:GetCustomUserData(0)
  if select == 0 then
    if gUI.Quest.wndQuestInCompleted:IsChecked() then
      gUI.Quest.wndQuestCompleted:SetChecked(false)
    end
  elseif gUI.Quest.wndQuestCompleted:IsChecked() then
    gUI.Quest.wndQuestInCompleted:SetChecked(false)
  end
  UpdateTrackQuestList()
end
function UI_QB_MouseIn()
  _QB_MouseInCtrlList = true
end
function UI_QB_MouseOut()
  _QB_MouseInCtrlList = false
end
function UI_QuestYaBiao_Confirm(msg)
  local wnd = msg:get_window()
  local quest = wnd:GetCustomUserData(0)
  AcceptYabiaoQuest(quest)
  gUI.Quest.YaBiao:SetVisible(false)
end
function UI_QuestYaBiao_Reset(msg)
  local wnd = msg:get_window()
  local cmd = wnd:GetCustomUserData(0)
  wnd:SetProperty("Text", "重置&2&")
  wnd:SetEnable(false)
  ResetYabiaoQuest(cmd)
end
function Script_QB_OnLoad()
  gUI.Quest = {}
  gUI.Quest.wndCanAcceptTree = WindowToTreeCtrl(WindowSys_Instance:GetWindow("QuestBoard.list.TreeList1"))
  gUI.Quest.wndQuestBoardTree = WindowToTreeCtrl(WindowSys_Instance:GetWindow("QuestBoard.list.TreeList"))
  gUI.Quest.wndAbandon = WindowSys_Instance:GetWindow("QuestBoard.GrveUp")
  gUI.Quest.cbNearLevel = WindowToCheckButton(WindowSys_Instance:GetWindow("QuestBoard.NearLevel_cbtn"))
  gUI.Quest.tabQuestType = WindowToTableCtrl(WindowSys_Instance:GetWindow("QuestBoard.Picture1.Paging"))
  gUI.Quest.Board = WindowSys_Instance:GetWindow("QuestBoard")
  gUI.Quest.BoardAcceptedNum = WindowSys_Instance:GetWindow("QuestBoard.QuestNumber")
  gUI.Quest.BoardTask = WindowToContainer(WindowSys_Instance:GetWindow("QuestBoard.Task_cnt"))
  gUI.Quest.BoardTableCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("QuestTrackHeader.backpic.Change_tctl"))
  gUI.Quest.BoardShareBtn = WindowSys_Instance:GetWindow("QuestBoard.Share_btn")
  gUI.Quest.AskWnd = WindowSys_Instance:GetWindow("Detail.QuestGiveUp")
  gUI.Quest.QuestRewardItemList = ""
  gUI.Quest.BoardTrack = WindowToCheckButton(WindowSys_Instance:GetWindow("QuestBoard.TrackQuest_cbtn"))
  gUI.Quest.wndQuestContainer = WindowToContainer(WindowSys_Instance:GetWindow("QuestTrackHeader.Container_cnt"))
  gUI.Quest.wndQuestAccepted = WindowToDisplayBox(WindowSys_Instance:GetWindow("QuestTrackHeader.accepted_dbox"))
  gUI.Quest.wndQuestCompleted = WindowToCheckButton(WindowSys_Instance:GetWindow("QuestTrackHeader.Quest_bg.Finish_cbtn"))
  gUI.Quest.wndQuestInCompleted = WindowToCheckButton(WindowSys_Instance:GetWindow("QuestTrackHeader.Quest_bg.NotFinis_cbtn"))
  gUI.Quest.wndQuestQuestKind = WindowToComboBox(WindowSys_Instance:GetWindow("QuestTrackHeader.Quest_bg.Quset_cbox"))
  gUI.Quest.wndQuestCompleted:SetChecked(false)
  gUI.Quest.wndQuestInCompleted:SetChecked(false)
  gUI.Quest.wndQuestQuestKind:RemoveAllItems()
  for i = 0, #_QB_QuestKind do
    local item = gUI.Quest.wndQuestQuestKind:InsertString(_QB_QuestKind[i].Desc, i)
    item:SetUserData(i)
  end
  gUI.Quest.wndQuestQuestKind:SetSelectItem(0)
  _QB_LastSearchPathId = -1
  _QB_AskCanAcceptList = true
  gUI.Quest.cbNearLevel:SetProperty("Visible", "false")
  _QB_MouseInCtrlList = false
  gUI.Quest.YaBiao = WindowSys_Instance:GetWindow("YaBiao")
  gUI.Quest.YaBiaoLavel = WindowSys_Instance:GetWindow("YaBiao.YaBiao_bg.Label1")
  gUI.Quest.YaBiaoConfirm = WindowSys_Instance:GetWindow("YaBiao.YaBiao_bg.Confirm")
  gUI.Quest.YaBiaoReset = WindowSys_Instance:GetWindow("YaBiao.YaBiao_bg.Cancel")
end
function Script_QB_OnEvent(event)
  if event == "PLAYER_QUEST_UPDATE" then
    _QB_UpdateTrackQuestList()
    _QB_UpdateAcceptedQuestList()
  elseif event == "PLAYER_QUEST_UPDATESINGLE" then
    _QB_UpdateAcceptedQuestSingle(arg1)
  elseif event == "PLAYER_ACCEPT_NEW_QUEST" then
    _OnQB_PlayerAcceptNewQuest(arg1, arg2, arg3, arg4)
  elseif event == "PLAYER_QUEST_REMOVED" then
    _QB_UpdateTrackQuestList()
    _QB_UpdateCanAcceptQuestList()
    _QB_UpdateAcceptedQuestList()
  elseif event == "PLAYER_QUEST_COMPLETE" then
    _OnQB_PlayerQuestComplete(arg1, arg2, arg3)
  elseif event == "QUEST_ADD_CAN_ACCEPT" then
    _OnQB_AddCanAcceptQuest(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13)
  elseif event == "QUEST_CAN_ACCEPT_END" then
    _OnQB_AddCanAcceptQuestEnd(arg1)
  elseif event == "QUEST_SHOW_USE_ITEM" then
    _OnQB_QuestShowUseItem(arg1, arg2, arg3, arg4)
  elseif event == "QUEST_TRACK_BEGIN" then
    _OnQB_TrackAddBegin(arg1)
  elseif event == "QUEST_TRACK_END" then
    _OnQB_TrackAddEnd(arg1)
  elseif event == "QUEST_ADD_TRACK_ITEM" then
    _OnQB_TrackAddItem(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10)
  elseif event == "NPC_GOSSIP_ADD_REWARD_ITEM" then
    _OnQB_UpdateRewardItem(arg1, arg2, arg3, arg4, arg5)
  elseif event == "NPC_GOSSIP_ADD_CHOICES_ITEM" then
    _OnQB_UpdateRewardChoicesItem(arg1, arg2, arg3, arg4, arg5)
  elseif event == "ITEM_ADD_TO_BAG" or event == "ITEM_DEL_FROM_BAG" then
    _OnQB_UpdateTrackItemState(arg1, arg2)
  elseif event == "QUEST_ASK_SHARE" then
    _OnQB_AskShareQuest(arg1, arg2, arg3, arg4)
  elseif event == "BAG_ITEM_COOLDOWN" then
    _OnQB_SetItemCoolDown(arg1, arg2, arg3, arg4, arg5)
  elseif event == "QUEST_DONE" then
    _QB_DoneMsg(arg1, arg2)
  elseif event == "QUEST_TRANS_MSGBOX" then
    _OnQB_TransMessageBox(arg1)
  elseif event == "QUEST_YABIAO_RES" then
    _OnQB_YabiaoMessageBox(arg1, arg2, arg3)
  end
end
function Script_QuestYaBiao_OnEscape()
  local questid = gUI.Quest.YaBiao:GetCustomUserData(0)
  AcceptYabiaoQuest(questid)
end
