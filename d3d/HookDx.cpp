// HookDx.cpp : 定义 DLL 应用程序的入口点。
//

#include "stdafx.h"
#include <d3d9.h>
#include <D3dx9core.h>
#include "../detour/detours.h"
#include <strsafe.h>
#include <fstream>
#include <iostream>

#ifdef _MANAGED
#pragma managed(push, off)
#endif

void OutLog( char *pStr )
{  
	using namespace std;
	//判断日志文件是否存在
	bool bFileExist = false;
	FILE *fp = NULL;
	fp = fopen( "C:\\log.txt", "r" );
	if( fp != NULL )
	{
		bFileExist = true;
		fclose(fp);
		fp = NULL;
	}

	//char lpPath[100] = {0};
	//GetCurrentDirectory( 100, lpPath );
	//MessageBox( NULL, lpPath, NULL ,MB_OK );

	//文件存在，写日志
	//if( bFileExist )
	//{
	//	ofstream out("C:\\log.txt", ios::app );
	//	if( out != nullptr )
	//	{
	//		out<<pStr<<endl;
	//	}
	//	out.close();
	//}
}

void SetByte5Jmp(DWORD Oldadr,DWORD mubiao,BYTE* TheOld5Byte)
{
	DWORD oldpro;
	VirtualProtect((LPVOID)Oldadr,5,PAGE_EXECUTE_READWRITE,&oldpro);
	memcpy(TheOld5Byte,(LPVOID)Oldadr,5);
	*(BYTE*)Oldadr=0xe9;
	*(DWORD*)((BYTE*)Oldadr+1)=(DWORD)mubiao-(DWORD)Oldadr-5;

}

void ReSetByte5(DWORD mubiao,BYTE* TheOld5Byte)
{
	DWORD oldpro;
	VirtualProtect((LPVOID)mubiao,5,PAGE_EXECUTE_READWRITE,&oldpro);
	memcpy((LPVOID)mubiao,(LPVOID)TheOld5Byte,5);


}

IDirect3D9 * _stdcall hookedDirect3DCreate9(UINT SDKVersion); 


typedef IDirect3D9 *( _stdcall* _OldDirect3DCreate9)(UINT SDKVersion); 
typedef HRESULT (WINAPI* _OLD_D3DXCreateTextureFromFileA)(LPDIRECT3DDEVICE9 pDevice, LPCSTR pSrcFile, LPDIRECT3DTEXTURE9* ppTexture);
typedef HRESULT (WINAPI* _OLD_D3DXCreateTextureFromFileW)( LPDIRECT3DDEVICE9 pDevice, LPCWSTR pSrcFile, LPDIRECT3DTEXTURE9* ppTexture);
 typedef HRESULT( WINAPI* _OLD_D3DXCreateTextureFromFileExA)(
							 LPDIRECT3DDEVICE9         pDevice,
							 LPCSTR                    pSrcFile,
							 UINT                      Width,
							 UINT                      Height,
							 UINT                      MipLevels,
							 DWORD                     Usage,
							 D3DFORMAT                 Format,
							 D3DPOOL                   Pool,
							 DWORD                     Filter,
							 DWORD                     MipFilter,
							 D3DCOLOR                  ColorKey,
							 D3DXIMAGE_INFO*           pSrcInfo,
							 PALETTEENTRY*             pPalette,
							 LPDIRECT3DTEXTURE9*       ppTexture);

 _OLD_D3DXCreateTextureFromFileExA  OLD_D3DXCreateTextureFromFileExA;
_OldDirect3DCreate9 OldDirect3DCreate9;
 _OLD_D3DXCreateTextureFromFileA OLD_D3DXCreateTextureFromFileA;
 _OLD_D3DXCreateTextureFromFileW  OLD_D3DXCreateTextureFromFileW;


 HRESULT  WINAPI  NEW_D3DXCreateTextureFromFileExA (
	 LPDIRECT3DDEVICE9         pDevice,
	 LPCSTR                    pSrcFile,
	 UINT                      Width,
	 UINT                      Height,
	 UINT                      MipLevels,
	 DWORD                     Usage,
	 D3DFORMAT                 Format,
	 D3DPOOL                   Pool,
	 DWORD                     Filter,
	 DWORD                     MipFilter,
	 D3DCOLOR                  ColorKey,
	 D3DXIMAGE_INFO*           pSrcInfo,
	 PALETTEENTRY*             pPalette,
	 LPDIRECT3DTEXTURE9*       ppTexture)
 {

MessageBoxA(0,pSrcFile,pSrcFile,0);
return 0;

 };



HRESULT WINAPI _NEW_D3DXCreateTextureFromFileA(LPDIRECT3DDEVICE9 pDevice, LPCSTR pSrcFile, LPDIRECT3DTEXTURE9* ppTexture)
{
	 
MessageBoxA(0,pSrcFile,pSrcFile,0);
return 0;
};


HRESULT WINAPI _NEW_D3DXCreateTextureFromFileW(LPDIRECT3DDEVICE9 pDevice, LPCWSTR pSrcFile, LPDIRECT3DTEXTURE9* ppTexture)
{
	 
		MessageBoxW(0,pSrcFile,pSrcFile,0);
	return 0;
};


BOOL _stdcall DrawMyText(LPDIRECT3DDEVICE9 pDxdevice,char* strText ,int nbuf);
HRESULT _stdcall hookedCreateDevice(
								  LPDIRECT3D9 pDx9,
								  UINT Adapter,
								  D3DDEVTYPE DeviceType,
								  HWND hFocusWindow,
								  DWORD BehaviorFlags,
								  D3DPRESENT_PARAMETERS * pPresentationParameters,
								  IDirect3DDevice9 ** ppReturnedDeviceInterface

								  );

HRESULT _stdcall hookedPresent(
							 LPDIRECT3DDEVICE9 pDxdevice,
							 CONST RECT * pSourceRect,
							 CONST RECT * pDestRect,
							 HWND hDestWindowOverride,
							 CONST RGNDATA * pDirtyRegion
							 );


LPDIRECT3D9EX *m_pD3DEx=NULL; //Direct3D对象的接口指针

void * pCEx=NULL;//Direct3DCreate9函数地址指针
void * pCdevEx=NULL;//IDirect3D9::CreateDevice函数地址指针
void * pPreEx=NULL;//IDirect3DDevice9::Present函数地址指针
void * pPIDEx=NULL;//IDirect3DDevice9::DrawIndexedPrimitive函数地址指针

BYTE d3dcen5bytesEx[5];//用于保存Direct3DCreate9入口的5字节
BYTE devcen5bytesEx[5];//用于保存IDirect3D9::CreateDevice入口的字节
BYTE pren5bytesEx[5];//用于保存IDirect3DDevice9::Present入口的5字节
BYTE PID5BYTEEx[5];//用于保存IDirect3DDevice9::DrawIndexedPrimitive入口的5字节

LPDIRECT3D9 m_pD3D=NULL; //Direct3D对象的接口指针

void * pC=NULL;//Direct3DCreate9函数地址指针
void * pCdev=NULL;//IDirect3D9::CreateDevice函数地址指针
void * pPre=NULL;//IDirect3DDevice9::Present函数地址指针
void * pPID=NULL;//IDirect3DDevice9::DrawIndexedPrimitive函数地址指针

BYTE d3dcen5bytes[5];//用于保存Direct3DCreate9入口的5字节
BYTE devcen5bytes[5];//用于保存IDirect3D9::CreateDevice入口的字节
BYTE pren5bytes[5];//用于保存IDirect3DDevice9::Present入口的5字节
BYTE PID5BYTE[5];//用于保存IDirect3DDevice9::DrawIndexedPrimitive入口的5字节

char buf1[1024];
HRESULT _stdcall MYDrawIndexedPrimitive( LPDIRECT3DDEVICE9 pDxdevice,
										D3DPRIMITIVETYPE TYpe,
										INT BaseVertexIndex,
										UINT MinVertexIndex,
										UINT NumVertices,
										UINT startIndex,
										UINT primCount)
{
	__asm pushad
	
	//过滤掉0 28 8 42 4	 ,0 8	20  12  10,		0 0	8  0  4,
	
	//sprintf(buf1," type=%d BaseVertexIndex=%d   MinVertexIndex=%d  NumVertices=%d  startIndex=%d  primCount=%d  \n",TYpe,BaseVertexIndex,MinVertexIndex,NumVertices,startIndex,primCount);
				// DrawMyText(pDxdevice,buf1,sizeof buf1);
				OutLog(buf1);
ReSetByte5((DWORD)pPID,PID5BYTE);
			if (primCount<100)
			{HRESULT RE=pDxdevice->DrawIndexedPrimitive(TYpe,BaseVertexIndex,MinVertexIndex,NumVertices,startIndex,primCount);
			SetByte5Jmp((DWORD)pPID,(DWORD)MYDrawIndexedPrimitive,PID5BYTE);
			return RE;

			}
				
	

	

 
SetByte5Jmp((DWORD)pPID,(DWORD)MYDrawIndexedPrimitive,PID5BYTE);
__asm popad
	return 0;
}
//当程序运行到IDirect3DDevice9::Present入口处将跳转到这里
HRESULT _stdcall hookedPresent(
							 LPDIRECT3DDEVICE9 pDxdevice,//类的this指针
							 CONST RECT * pSourceRect,//此参数请参考dx sdk
							 CONST RECT * pDestRect,//同上
							 HWND hDestWindowOverride,//同上
							 CONST RGNDATA * pDirtyRegion//同上

							 )
{
	__asm pushad
	printDebugMessage("hookedPresent 已调用");
		Sleep(100);

		if(pCdev && pC && pPre){
			char strdraw[]="沫、D HOOK测试";
			DrawMyText(pDxdevice,strdraw,sizeof strdraw-1);//绘制文本
			LPDIRECT3DTEXTURE9 LP1;
			
			//D3DXCreateTextureFromFileExA(pDxdevice,"C:\\TEST.BMP", D3DX_DEFAULT,  D3DX_DEFAULT,  D3DX_DEFAULT, 0,D3DFMT_A1R5G5B5, D3DPOOL_MANAGED, D3DX_FILTER_TRIANGLE,  D3DX_FILTER_TRIANGLE,D3DCOLOR_RGBA(0,0,0,255), NULL,NULL,NULL);
		
			//在这里写入您的其它绘图代码
		}

		if(pC && pCdev && pPre)
			memcpy(pPre,pren5bytes,5);//先还原IDirect3DDevice9::Present入口的5字节

		HRESULT retdata= pDxdevice->Present(pSourceRect,pDestRect,hDestWindowOverride,pDirtyRegion);

		if(pC && pCdev && pPre){
			//DWORD oldpro=0;
			//VirtualProtect(pPre,5,PAGE_EXECUTE_READWRITE,&oldpro);
			//调用完IDirect3DDevice9::Present后再hook一次
			*(BYTE*)pPre=0xe9;
			*(DWORD*)((BYTE*)pPre+1)=(DWORD)hookedPresent-(DWORD)pPre-5;
		}

		__asm popad
			return retdata;
}

//我自己的绘制文本的过程
BOOL _stdcall DrawMyText(LPDIRECT3DDEVICE9 pDxdevice,char* strText ,int nbuf)
{

	if(m_pD3D && pDxdevice){

		RECT myrect;
		myrect.top=150;  //文本块的y坐标
		myrect.left=0; //文本块的左坐标
		myrect.right=500+myrect.left;
		myrect.bottom=100+myrect.top;
		pDxdevice->BeginScene();//开始绘制

		D3DXFONT_DESCA lf;
		ZeroMemory(&lf, sizeof(D3DXFONT_DESCA));
		lf.Height = 24; //字体高度
		lf.Width = 12; // 字体宽度
		lf.Weight = 100; 
		lf.Italic = false;
		lf.CharSet = DEFAULT_CHARSET;
		strcpy(lf.FaceName, "Times New Roman"); // 字型
		ID3DXFont* font=NULL;
		if(D3D_OK!=D3DXCreateFontIndirectA(pDxdevice, &lf, &font)) //创建字体对象
			return false;

		font->DrawTextA(
			NULL,
			strText, // 要绘制的文本
			nbuf, 
			&myrect, 
			DT_TOP | DT_LEFT, // 字符居中显示
			D3DCOLOR_ARGB(255,255,255,0)); 

		pDxdevice->EndScene();//结束绘制
	 
		font->Release();//释放对象
	}
	return true;
}





//当运行到Direct3DCreate9时跳转到这里
IDirect3D9 * _stdcall hookedDirect3DCreate9(
	UINT SDKVersion
	)
{
	__asm pushad
	printDebugMessage("Direct3DCreate9 已调用");
			ReSetByte5((DWORD)pC,d3dcen5bytes);			//首先还原入口的5个字节 	memcpy(pC,d3dcen5bytes,5);
	m_pD3D=Direct3DCreate9(SDKVersion);
	if(m_pD3D){//如果成功
		pCdev=(void*)*(DWORD*)(*(DWORD*)m_pD3D+0x40);//获得IDirect3D9::CreateDevice的地址指针
		
		DWORD oldpro=0;
		SetByte5Jmp((DWORD)pCdev,(DWORD)hookedCreateDevice,devcen5bytes);
		/*
		memcpy(devcen5bytes,pCdev,5);//保存IDirect3D9::CreateDevice入口5个字节
				VirtualProtect(pCdev,5,PAGE_EXECUTE_READWRITE,&oldpro);
				*(BYTE*)pCdev=0xe9;
				*(DWORD*)((BYTE*)pCdev+1)=(DWORD)hookedCreateDevice-(DWORD)pCdev-5;*/
		

	}else{//如果失败就再hook一次
		DWORD oldpro=0;
		VirtualProtect(pC,5,PAGE_EXECUTE_READWRITE,&oldpro);
		*(BYTE*)pC=0xe9;
		*(DWORD*)((BYTE*)pC+1)=(DWORD)hookedDirect3DCreate9-(DWORD)pC-5;
	}

	__asm popad
		return m_pD3D;
}

//当运行到IDirect3D9::CreateDevice的时候跳转到这里
HRESULT _stdcall hookedCreateDevice(
								  LPDIRECT3D9 pDx9,
								  UINT Adapter,
								  D3DDEVTYPE DeviceType,
								  HWND hFocusWindow,
								  DWORD BehaviorFlags,
								  D3DPRESENT_PARAMETERS * pPresentationParameters,
								  IDirect3DDevice9 ** ppReturnedDeviceInterface

								  )
{
	__asm pushad

	printDebugMessage("hookedCreateDevice 已调用");
		memcpy(pCdev,devcen5bytes,5);//先还原入口的5个字节


	HRESULT ret=pDx9->CreateDevice( //创建设备
		Adapter,
		DeviceType,
		hFocusWindow,
		BehaviorFlags,
		pPresentationParameters,
		ppReturnedDeviceInterface);


	if (ret==D3D_OK){//如果创建设备成功
	
		LPDIRECT3DDEVICE9 m_pDevice=*ppReturnedDeviceInterface;

		pPre=(void*)*(DWORD*)(*(DWORD*)m_pDevice+0x44);//获得IDirect3DDevice9::Present的地址指针
		pPID=(void*)*(DWORD*)(*(DWORD*)m_pDevice+0x148);//获得IDirect3DDevice9::DrawIndexedPrimitive的地址指针
		memcpy(pren5bytes,pPre,5);//保存IDirect3DDevice9::Present入口的5个字节
		DWORD oldpro=0;
/*
		VirtualProtect(pPre,5,PAGE_EXECUTE_READWRITE,&oldpro);
		*(BYTE*)pPre=0xe9;
		*(DWORD*)((BYTE*)pPre+1)=(DWORD)hookedPresent-(DWORD)pPre-5;*/

		SetByte5Jmp((DWORD)pPID,(DWORD)MYDrawIndexedPrimitive,PID5BYTE);
		SetByte5Jmp((DWORD)pPre,(DWORD)hookedPresent,pren5bytes);

	}else{//如果失败再hookIDirect3D9::CreateDevice一次
		DWORD oldpro=0;
		VirtualProtect(pCdev,5,PAGE_EXECUTE_READWRITE,&oldpro);
		*(BYTE*)pCdev=0xe9;
		*(DWORD*)((BYTE*)pCdev+1)=(DWORD)hookedCreateDevice-(DWORD)pCdev-5;
	}
	__asm popad
		return ret;
}


void OnHookInit()
{DWORD oldpro=0;
//这里只是hookDirect3DCreate9
	printDebugMessage("OnHookInit");
	pC=GetProcAddress(GetModuleHandleA("d3d9.dll"),"Direct3DCreate9");//获得内存地址
	memcpy(d3dcen5bytes,pC,5);

	VirtualProtect(pC,5,PAGE_EXECUTE_READWRITE,&oldpro);
	SetByte5Jmp((DWORD)pC,(DWORD)hookedDirect3DCreate9,d3dcen5bytes);
	
/*

 //D3DX=pC=GetProcAddress(GetModuleHandle("d3d9X_43.dll"),"D3DXCreateTextureFromFileA")
	OLD_D3DXCreateTextureFromFileA=(_OLD_D3DXCreateTextureFromFileA)GetProcAddress(GetModuleHandle("d3dX9D_43.dll"),"D3DXCreateTextureFromFileA");
	OLD_D3DXCreateTextureFromFileW=(_OLD_D3DXCreateTextureFromFileW)GetProcAddress(GetModuleHandle("d3dX9D_43.dll"),"D3DXCreateTextureFromFileW");
	OLD_D3DXCreateTextureFromFileExA=(_OLD_D3DXCreateTextureFromFileExA)GetProcAddress(GetModuleHandle("d3dX9D_43.dll"),"D3DXCreateTextureFromFileExA");
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	//DetourAttach(&(PVOID&)OLD_D3DXCreateTextureFromFileA, _NEW_D3DXCreateTextureFromFileA) ;
	//	DetourAttach(&(PVOID&)OLD_D3DXCreateTextureFromFileW, _NEW_D3DXCreateTextureFromFileW) ;
	DetourAttach(&(PVOID&)OLD_D3DXCreateTextureFromFileExA, NEW_D3DXCreateTextureFromFileExA) ;
	DetourTransactionCommit();*/

/*
	memcpy(d3dcen5bytes,pC,5);
	VirtualProtect(pC,5,PAGE_EXECUTE_READWRITE,&oldpro);
	*(BYTE*)pC=0xe9;//0xe9在汇编中是跳转指令操作码
	*(DWORD*)((BYTE*)pC+1)=(DWORD)hookedDirect3DCreate9-(DWORD)pC-5;//目标地址-原地址-5*/

	






/*
	OldDirect3DCreate9=(_OldDirect3DCreate9)(GetProcAddress(GetModuleHandle("d3d9.dll"),"Direct3DCreate9")); 
	if (OldDirect3DCreate9==0)
	{
		MessageBoxA(0,"==0","",0);
	}
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)OldDirect3DCreate9, hookedDirect3DCreate9) ;
	DetourTransactionCommit();
	MessageBoxA(0,"!0","",0);*/

}

void OnUnHook()
{

}

#ifdef _MANAGED
#pragma managed(pop)
#endif

