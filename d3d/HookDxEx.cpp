// HookDx.cpp : 定义 DLL 应用程序的入口点。
//

#include "stdafx.h"
#include <d3d9.h>
#include "../function/function.h"
#include <D3dx9core.h>
#include "../detour/detours.h"
#include <strsafe.h>
#include <fstream>
#include <iostream>

#ifdef _MANAGED
#pragma managed(push, off)
#endif

void OutLogEx( char *pStr )
{  
	return;
	using namespace std;
	//判断日志文件是否存在
	bool bFileExist = false;
	FILE *fp = NULL;
	fp = fopen( "C:\\log.txt", "r" );
	if( fp != NULL )
	{
		bFileExist = true;
		fclose(fp);
		fp = NULL;
	}

	//char lpPath[100] = {0};
	//GetCurrentDirectory( 100, lpPath );
	//MessageBox( NULL, lpPath, NULL ,MB_OK );

	//文件存在，写日志
	if( bFileExist )
	{
		ofstream out("C:\\log.txt", ios::app );
		if( out != NULL )
		{
			out<<pStr<<endl;
		}
		out.close();
	}
}

void SetByte5JmpEx(DWORD Oldadr,DWORD mubiao,BYTE* TheOld5Byte)
{
	DWORD oldpro;
	VirtualProtect((LPVOID)Oldadr,5,PAGE_EXECUTE_READWRITE,&oldpro);
	memcpy(TheOld5Byte,(LPVOID)Oldadr,5);
	*(BYTE*)Oldadr=0xe9;
	*(DWORD*)((BYTE*)Oldadr+1)=(DWORD)mubiao-(DWORD)Oldadr-5;

}

void ReSetByte5Ex(DWORD mubiao,BYTE* TheOld5Byte)
{
	DWORD oldpro;
	VirtualProtect((LPVOID)mubiao,5,PAGE_EXECUTE_READWRITE,&oldpro);
	memcpy((LPVOID)mubiao,(LPVOID)TheOld5Byte,5);


}

HRESULT _stdcall hookedDirect3DCreate9Ex(
	UINT SDKVersion,LPDIRECT3D9EX * ptr
	);

BOOL _stdcall DrawMyTextEx(LPDIRECT3DDEVICE9EX pDxdevice,char* strText ,int nbuf);
HRESULT _stdcall hookedCreateDeviceEx(
								  LPDIRECT3D9EX pDx9,
								  UINT Adapter,
								  D3DDEVTYPE DeviceType,
								  HWND hFocusWindow,
								  DWORD BehaviorFlags,
								  D3DPRESENT_PARAMETERS * pPreExsentationParameters,
								   D3DDISPLAYMODEEX* pFullscreenDisplayMode,
								  IDirect3DDevice9Ex ** pPreExturnedDeviceInterface

								  );

HRESULT _stdcall hookedPresentEx(
							 LPDIRECT3DDEVICE9EX pDxdevice,
							 CONST RECT * pSourceRect,
							 CONST RECT * pDestRect,
							 HWND hDestWindowOverride,
							 CONST RGNDATA * pDirtyRegion
							 );

LPDIRECT3D9EX m_pD3DEx=NULL; //Direct3D对象的接口指针

void * pCEx=NULL;//Direct3DCreate9Ex函数地址指针
void * pCExdev=NULL;//IDirect3D9::CreateDeviceEx函数地址指针
void * pPreEx=NULL;//IDirect3DDevice9Ex::Present函数地址指针
void * pPIDEx=NULL;//IDirect3DDevice9Ex::DrawIndexedPrimitive函数地址指针

BYTE d3dcen5bytesEx[5];//用于保存Direct3DCreate9Ex入口的5字节
BYTE devcen5bytesEx[5];//用于保存IDirect3D9::CreateDeviceEx入口的字节
BYTE pren5bytesEx[5];//用于保存IDirect3DDevice9Ex::Present入口的5字节
BYTE PID5BYTEEx[5];//用于保存IDirect3DDevice9Ex::DrawIndexedPrimitive入口的5字节

char buf1Ex[1024];
HRESULT _stdcall MYDrawIndexedPrimitiveEx( LPDIRECT3DDEVICE9EX pDxdevice,
										D3DPRIMITIVETYPE TYpe,
										INT BaseVertexIndex,
										UINT MinVertexIndex,
										UINT NumVertices,
										UINT startIndex,
										UINT primCount)
{
	__asm pushad
	
	//过滤掉0 28 8 42 4	 ,0 8	20  12  10,		0 0	8  0  4,
	
	//sprintf(buf1Ex," type=%d BaseVertexIndex=%d   MinVertexIndex=%d  NumVertices=%d  startIndex=%d  primCount=%d  \n",TYpe,BaseVertexIndex,MinVertexIndex,NumVertices,startIndex,primCount);
				// DrawMyTextEx(pDxdevice,buf1Ex,sizeof buf1Ex);
				OutLogEx(buf1Ex);
ReSetByte5Ex((DWORD)pPIDEx,PID5BYTEEx);
			if (primCount<100)
			{HRESULT RE=pDxdevice->DrawIndexedPrimitive(TYpe,BaseVertexIndex,MinVertexIndex,NumVertices,startIndex,primCount);
			SetByte5JmpEx((DWORD)pPIDEx,(DWORD)MYDrawIndexedPrimitiveEx,PID5BYTEEx);
			return RE;

			}
				
	

	

 
SetByte5JmpEx((DWORD)pPIDEx,(DWORD)MYDrawIndexedPrimitiveEx,PID5BYTEEx);
__asm popad
	return 0;
}
//当程序运行到IDirect3DDevice9Ex::Present入口处将跳转到这里
HRESULT _stdcall hookedPresentEx(
							 LPDIRECT3DDEVICE9EX pDxdevice,//类的this指针
							 CONST RECT * pSourceRect,//此参数请参考dx sdk
							 CONST RECT * pDestRect,//同上
							 HWND hDestWindowOverride,//同上
							 CONST RGNDATA * pDirtyRegion//同上

							 )
{
	__asm pushad
	printDebugMessage("hookedPresentEx 已调用");
		Sleep(100);

		if(pCExdev && pCEx && pPreEx){
			char strdraw[]="沫、D HOOK测试";
			DrawMyTextEx(pDxdevice,strdraw,sizeof strdraw-1);//绘制文本
			LPDIRECT3DTEXTURE9 LP1;
			
			//D3DXCreateTextureFromFileExA(pDxdevice,"C:\\TEST.BMP", D3DX_DEFAULT,  D3DX_DEFAULT,  D3DX_DEFAULT, 0,D3DFMT_A1R5G5B5, D3DPOOL_MANAGED, D3DX_FILTER_TRIANGLE,  D3DX_FILTER_TRIANGLE,D3DCOLOR_RGBA(0,0,0,255), NULL,NULL,NULL);
		
			//在这里写入您的其它绘图代码
		}

		if(pCEx && pCExdev && pPreEx)
			memcpy(pPreEx,pren5bytesEx,5);//先还原IDirect3DDevice9Ex::Present入口的5字节

		HRESULT retdata= pDxdevice->Present(pSourceRect,pDestRect,hDestWindowOverride,pDirtyRegion);

		if(pCEx && pCExdev && pPreEx){
			//DWORD oldpro=0;
			//VirtualProtect(pPreEx,5,PAGE_EXECUTE_READWRITE,&oldpro);
			//调用完IDirect3DDevice9Ex::Present后再hook一次
			*(BYTE*)pPreEx=0xe9;
			*(DWORD*)((BYTE*)pPreEx+1)=(DWORD)hookedPresentEx-(DWORD)pPreEx-5;
		}

		__asm popad
			return retdata;
}

//我自己的绘制文本的过程
BOOL _stdcall DrawMyTextEx(LPDIRECT3DDEVICE9EX pDxdevice,char* strText ,int nbuf)
{

	if(m_pD3DEx && pDxdevice){

		RECT myrect;
		myrect.top=150;  //文本块的y坐标
		myrect.left=0; //文本块的左坐标
		myrect.right=500+myrect.left;
		myrect.bottom=100+myrect.top;
		pDxdevice->BeginScene();//开始绘制

		D3DXFONT_DESCA lf;
		ZeroMemory(&lf, sizeof(D3DXFONT_DESCA));
		lf.Height = 24; //字体高度
		lf.Width = 12; // 字体宽度
		lf.Weight = 100; 
		lf.Italic = false;
		lf.CharSet = DEFAULT_CHARSET;
		strcpy(lf.FaceName, "Times New Roman"); // 字型
		ID3DXFont* font=NULL;
		if(D3D_OK!=D3DXCreateFontIndirectA(pDxdevice, &lf, &font)) //创建字体对象
			return false;

		font->DrawTextA(
			NULL,
			strText, // 要绘制的文本
			nbuf, 
			&myrect, 
			DT_TOP | DT_LEFT, // 字符居中显示
			D3DCOLOR_ARGB(255,255,255,0)); 

		pDxdevice->EndScene();//结束绘制
	 
		font->Release();//释放对象
	}
	return true;
}


//当运行到Direct3DCreate9Ex时跳转到这里
HRESULT _stdcall hookedDirect3DCreate9Ex(
	UINT SDKVersion,LPDIRECT3D9EX * ptr
	)
{
	__asm pushad
	printDebugMessage("Direct3DCreate9Ex 已调用");
			ReSetByte5Ex((DWORD)pCEx,d3dcen5bytesEx);			//首先还原入口的5个字节 	memcpy(pCEx,d3dcen5bytesEx,5);
	HRESULT ret =Direct3DCreate9Ex(SDKVersion,ptr);
//	MyMessageBoxA(0,"sfa","",0);
	if(ret == S_OK){//如果成功
		m_pD3DEx = *ptr;
		pCExdev=(void*)*(DWORD*)(*(DWORD*)m_pD3DEx+0x50);//获得IDirect3D9::CreateDeviceEx的地址指针
		DWORD oldpro=0;
		SetByte5JmpEx((DWORD)pCExdev,(DWORD)hookedCreateDeviceEx,devcen5bytesEx);
		/*
		memcpy(devcen5bytesEx,pCExdev,5);//保存IDirect3D9::CreateDeviceEx入口5个字节
				VirtualProtect(pCExdev,5,PAGE_EXECUTE_READWRITE,&oldpro);
				*(BYTE*)pCExdev=0xe9;
				*(DWORD*)((BYTE*)pCExdev+1)=(DWORD)hookedCreateDeviceEx-(DWORD)pCExdev-5;*/
		

	}else{//如果失败就再hook一次
		DWORD oldpro=0;
		VirtualProtect(pCEx,5,PAGE_EXECUTE_READWRITE,&oldpro);
		*(BYTE*)pCEx=0xe9;
		*(DWORD*)((BYTE*)pCEx+1)=(DWORD)hookedDirect3DCreate9Ex-(DWORD)pCEx-5;
	}

	__asm popad
		return ret;
}


//当运行到IDirect3D9::CreateDeviceEx的时候跳转到这里
HRESULT _stdcall hookedCreateDeviceEx(
								  LPDIRECT3D9EX pDx9,
								  UINT Adapter,
								  D3DDEVTYPE DeviceType,
								  HWND hFocusWindow,
								  DWORD BehaviorFlags,
								  D3DPRESENT_PARAMETERS * pPreExsentationParameters,
								  D3DDISPLAYMODEEX* pFullscreenDisplayMode,
								  IDirect3DDevice9Ex ** pPreExturnedDeviceInterface

								  )
{
	__asm pushad
	printDebugMessage("hookedCreateDeviceEx 已调用");
		memcpy(pCExdev,devcen5bytesEx,5);//先还原入口的5个字节


	HRESULT ret=pDx9->CreateDeviceEx
		( //创建设备
		Adapter,
		DeviceType,
		hFocusWindow,
		BehaviorFlags,
		pPreExsentationParameters,
		pFullscreenDisplayMode,
		pPreExturnedDeviceInterface);


	if (ret==D3D_OK){//如果创建设备成功
	
		LPDIRECT3DDEVICE9EX m_pDevice=*pPreExturnedDeviceInterface;

		pPreEx=(void*)*(DWORD*)(*(DWORD*)m_pDevice+0x44);//获得IDirect3DDevice9Ex::Present的地址指针
		pPIDEx=(void*)*(DWORD*)(*(DWORD*)m_pDevice+0x148);//获得IDirect3DDevice9Ex::DrawIndexedPrimitive的地址指针
		memcpy(pren5bytesEx,pPreEx,5);//保存IDirect3DDevice9Ex::Present入口的5个字节
		DWORD oldpro=0;
/*
		VirtualProtect(pPreEx,5,PAGE_EXECUTE_READWRITE,&oldpro);
		*(BYTE*)pPreEx=0xe9;
		*(DWORD*)((BYTE*)pPreEx+1)=(DWORD)hookedPresentEx-(DWORD)pPreEx-5;*/

		SetByte5JmpEx((DWORD)pPIDEx,(DWORD)MYDrawIndexedPrimitiveEx,PID5BYTEEx);
		SetByte5JmpEx((DWORD)pPreEx,(DWORD)hookedPresentEx,pren5bytesEx);

	}else{//如果失败再hookIDirect3D9::CreateDeviceEx一次
		DWORD oldpro=0;
		VirtualProtect(pCExdev,5,PAGE_EXECUTE_READWRITE,&oldpro);
		*(BYTE*)pCExdev=0xe9;
		*(DWORD*)((BYTE*)pCExdev+1)=(DWORD)hookedCreateDeviceEx-(DWORD)pCExdev-5;
	}
	__asm popad
		return ret;
}

void OnHookInitEx()
{DWORD oldpro=0;
//这里只是hookDirect3DCreate9Ex
	printDebugMessage("OnHookInitEx");
	pCEx=GetProcAddress(GetModuleHandleA("d3d9.dll"),"Direct3DCreate9Ex");//获得内存地址
	memcpy(d3dcen5bytesEx,pCEx,5);

	VirtualProtect(pCEx,5,PAGE_EXECUTE_READWRITE,&oldpro);
	SetByte5JmpEx((DWORD)pCEx,(DWORD)hookedDirect3DCreate9Ex,d3dcen5bytesEx);
	
/*

 //D3DX=pCEx=GetProcAddress(GetModuleHandle("d3d9X_43.dll"),"D3DXCreateTextureFromFileA")
	OLD_D3DXCreateTextureFromFileA=(_OLD_D3DXCreateTextureFromFileA)GetProcAddress(GetModuleHandle("d3dX9D_43.dll"),"D3DXCreateTextureFromFileA");
	OLD_D3DXCreateTextureFromFileW=(_OLD_D3DXCreateTextureFromFileW)GetProcAddress(GetModuleHandle("d3dX9D_43.dll"),"D3DXCreateTextureFromFileW");
	OLD_D3DXCreateTextureFromFileExA=(_OLD_D3DXCreateTextureFromFileExA)GetProcAddress(GetModuleHandle("d3dX9D_43.dll"),"D3DXCreateTextureFromFileExA");
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	//DetourAttach(&(PVOID&)OLD_D3DXCreateTextureFromFileA, _NEW_D3DXCreateTextureFromFileA) ;
	//	DetourAttach(&(PVOID&)OLD_D3DXCreateTextureFromFileW, _NEW_D3DXCreateTextureFromFileW) ;
	DetourAttach(&(PVOID&)OLD_D3DXCreateTextureFromFileExA, NEW_D3DXCreateTextureFromFileExA) ;
	DetourTransactionCommit();*/

/*
	memcpy(d3dcen5bytesEx,pCEx,5);
	VirtualProtect(pCEx,5,PAGE_EXECUTE_READWRITE,&oldpro);
	*(BYTE*)pCEx=0xe9;//0xe9在汇编中是跳转指令操作码
	*(DWORD*)((BYTE*)pCEx+1)=(DWORD)hookedDirect3DCreate9Ex-(DWORD)pCEx-5;//目标地址-原地址-5*/

	






/*
	OldDirect3DCreate9Ex=(_OldDirect3DCreate9Ex)(GetProcAddress(GetModuleHandle("d3d9.dll"),"Direct3DCreate9Ex")); 
	if (OldDirect3DCreate9Ex==0)
	{
		MessageBoxA(0,"==0","",0);
	}
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)OldDirect3DCreate9Ex, hookedDirect3DCreate9Ex) ;
	DetourTransactionCommit();
	MessageBoxA(0,"!0","",0);*/

}

void OnUnHookEx()
{

}

#ifdef _MANAGED
#pragma managed(pop)
#endif

