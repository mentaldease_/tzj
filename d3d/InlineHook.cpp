// InlineHook.cpp: implementation of the CInlineHook class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "InlineHook.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInlineHook::CInlineHook()
{

}

CInlineHook::~CInlineHook()
{

}
void CInlineHook::TestHookVlaue(LPInlineHook lpHook)
{
	BYTE code[5]={0x8b,0xff,0x55,0x8b,0xec};
	if (lpHook->CodeBuf[0]==0xe9||lpHook->CodeBuf[0]==0x00)
	{
		memcpy((void*)&lpHook->CodeBuf,(void*)&code,5);
	}
}
DWORD CInlineHook::GetMemValue(DWORD pMemAddres)
{
	DWORD	dwRet=0;
	try
	{
		dwRet=*(DWORD*)pMemAddres;	
		return dwRet;
	}
	catch(...)
	{
		return 0;
	}
}
BOOL CInlineHook::SetInlineHook(LPInlineHook hook)
{
	TOP5CODE jmpbuf={0};
	DWORD CodeLen=0,OldProctect=0;
	if (hook->HookCodeLen>8) hook->HookCodeLen=8;
	if (hook->HookCodeLen<5) hook->HookCodeLen=5;
	CodeLen=hook->HookCodeLen;
	if (hook->OldAddres==NULL||hook->NewAddres==NULL) 
	{
		return FALSE;
	}
	memset((void*)&hook->CodeBuf,0,8);
	jmpbuf.instruction=0xe9;
	jmpbuf.address=hook->NewAddres-hook->OldAddres-5;
	if ((CodeLen-5)>0) 
	{
		memset((void*)&jmpbuf.CodeNop,0x90,CodeLen-5);
	}
	//MySuspendOtherThreads((PBYTE)hook->OldAddres,hook->HookCodeLen);
	VirtualProtect((void*)hook->OldAddres,CodeLen,PAGE_EXECUTE_READWRITE,&OldProctect);
	if (hook->CodeBuf[0]==0&&hook->CodeBuf[1]==0)
	{
		memcpy((void*)&hook->CodeBuf,(void*)hook->OldAddres,CodeLen);
	}
	memcpy((void*)hook->OldAddres,(void*)&jmpbuf,CodeLen);
	VirtualProtect((void*)hook->OldAddres,CodeLen,OldProctect,&OldProctect);
	//MyResumeOtherThreads();
	return TRUE;
}
BOOL CInlineHook::UnInlineHook(LPInlineHook hook)
{
	DWORD CodeLen=0,OldProctect=0;
	if (hook->HookCodeLen>8) hook->HookCodeLen=8;
	if (hook->HookCodeLen<5) hook->HookCodeLen=5;
	CodeLen=hook->HookCodeLen;
	if (hook->OldAddres==NULL||hook->OldAddres>0X80000000) 
	{
		return FALSE;
	}
//	MySuspendOtherThreads((PBYTE)hook->OldAddres,hook->HookCodeLen);
	VirtualProtect((void*)hook->OldAddres,CodeLen,PAGE_EXECUTE_READWRITE,&OldProctect);
	memcpy((void*)hook->OldAddres,(void*)&hook->CodeBuf,CodeLen);
	VirtualProtect((void*)hook->OldAddres,CodeLen,OldProctect,&OldProctect);
//	MyResumeOtherThreads();
	return true;
}