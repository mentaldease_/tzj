// InlineHook.h: interface for the CInlineHook class.
//
//////////////////////////////////////////////////////////////////////

#ifndef INLINEHOOK_H
#define  INLINEHOOK_H
//////////////////////////////////////////////////////////////////////////
#ifndef ASM
#define ASM _asm
#endif
#ifndef NAKED
#define NAKED _declspec(naked)
#endif
//////////////////////////////////////////////////////////////////////////
//HOOK结构
#ifndef InlineHook
typedef struct _InlineHook
{
	ULONG OldAddres;//原来的地载
	ULONG NewAddres;
	ULONG HookCodeLen:5;
	BYTE  CodeBuf[8];//恢复代码用的缓存，不管
}InlineHook,*LPInlineHook;
#endif
//////////////////////////////////////////////////////////////////////////
//跳转结构
#ifndef _TOP5CODE
#pragma pack(1)//对齐指令，按字节对齐
typedef struct _TOP5CODE
{
	BYTE  instruction;//指令
	ULONG  address;//地址
	BYTE   CodeNop[3];
}TOP5CODE,*PTOP5CODE;
#pragma pack()//恢复
#endif
class CInlineHook  
{
public:
	CInlineHook();
	virtual ~CInlineHook();
	static	BOOL SetInlineHook(LPInlineHook hook);
	static	BOOL UnInlineHook(LPInlineHook hook);
	static  DWORD GetMemValue(DWORD pMemAddres);
	static  void TestHookVlaue(LPInlineHook lpHook);
};

#endif
