// DDrawBase.cpp: implementation of the CDDrawBase class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "DDrawBase.h"
#ifdef UNICODE
#undef UNICODE
#endif
//////////////////////////////////////////////////////////////////////////
static LPDIRECTDRAW			g_pMyDD=NULL; 
static LPDIRECTDRAWSURFACE	g_pMyDDSFront=NULL; 
static LPDIRECTDRAWCLIPPER	g_pMyClipper=NULL;
static DDSURFACEDESC	    ddsd; //这个结构描述"页"的特徵. 
//////////////////////////////////////////////////////////////////////////
static CDDrawBase			g_DDrawBase;
static CDDrawSurfaceHook    g_DDSurfacehook;
static int                  g_2dCPU=0;
static D2DGAMEBASE          g_2DGameBase;
//////////////////////////////////////////////////////////////////////////
void WINAPI InitD2dData()
{
	g_2DGameBase.DeleteAttachedSurface_off=0x20;
	g_2DGameBase.Blt_off=0x14;
	g_2DGameBase.BltBatch_off=0x18;
	g_2DGameBase.BltFast_off=0x1c;
	g_2DGameBase.SetColorKey_off=0x74;
	g_2DGameBase.Flip_off=0x2c;
}
void WINAPI Start2DBlack()
{
	InitD2dData();
	if (g_pMyDD==NULL)
	{
		g_DDrawBase.InitDDrawBase();
	}
	if (g_pMyDDSFront)
	{
		g_DDSurfacehook.Init((DWORD)g_pMyDDSFront);
		g_DDSurfacehook.SetAllHook();
	}
}
void WINAPI End2DBlack()
{
	g_DDSurfacehook.UnHook();
}
//////////////////////////////////////////////////////////////////////////
CDDrawBase::CDDrawBase()
{

}

CDDrawBase::~CDDrawBase()
{
	
}
BOOL CDDrawBase::InitDDrawBase()
{
	DirectDrawCreate( NULL,&g_pMyDD, NULL);
	if (g_pMyDD==NULL) return false;
	g_pMyDD->SetCooperativeLevel(AfxGetMainWnd()->GetSafeHwnd(),DDSCL_NORMAL);
	memset((void*)&ddsd,0,sizeof(ddsd));
	ddsd.dwFlags = DDSD_CAPS; 
	ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;//指定我们用的是前页. 
	ddsd.dwSize = sizeof(ddsd); //尺寸 
	g_pMyDD->CreateSurface(&ddsd, &g_pMyDDSFront, NULL); 
	return	(BOOL)g_pMyDDSFront;
}
//////////////////////////////////////////////////////////////////////////
CDDrawSurfaceHook::CDDrawSurfaceHook()
{
	
	m_Base=0;
	memset((void*)&IsLost_hook,0,sizeof(InlineHook));
	memset((void*)&Restore_hook,0,sizeof(InlineHook));
	memset((void*)&DeleteAttachedSurface_hook,0,sizeof(InlineHook));
	memset((void*)&Blt_hook,0,sizeof(InlineHook));
	memset((void*)&BltBatch_hook,0,sizeof(InlineHook));
	memset((void*)&BltFast_hook,0,sizeof(InlineHook));
	memset((void*)&ReleaseDC_hook,0,sizeof(InlineHook));
	memset((void*)&Flip_hook,0,sizeof(InlineHook));
	memset((void*)&SetColorKey_hook,0,sizeof(InlineHook));
	
}
CDDrawSurfaceHook::~CDDrawSurfaceHook()
{
}
//////////////////////////////////////////////////////////////////////////
void CDDrawSurfaceHook::SetAllHook()
{
	//CInlineHook::SetInlineHook(&IsLost_hook);
	//CInlineHook::SetInlineHook(&Restore_hook);
	CInlineHook::SetInlineHook(&DeleteAttachedSurface_hook);
	CInlineHook::SetInlineHook(&Blt_hook);
	CInlineHook::SetInlineHook(&BltBatch_hook);
	CInlineHook::SetInlineHook(&BltFast_hook);
	//CInlineHook::SetInlineHook(&ReleaseDC_hook);
	CInlineHook::SetInlineHook(&Flip_hook);
	CInlineHook::SetInlineHook(&SetColorKey_hook);

// 	g_pMyDDSFront->IsLost();
// 	g_pMyDDSFront->Restore();
// 	g_pMyDDSFront->Blt(NULL,NULL,NULL,NULL,NULL);
// 	g_pMyDDSFront->DeleteAttachedSurface(0,0);
// 
// 	g_pMyDDSFront->BltBatch(NULL,NULL,NULL);
// 	g_pMyDDSFront->BltFast(0,0,0,0,0);
// 	g_pMyDDSFront->SetColorKey(0,0);
// 	g_pMyDDSFront->ReleaseDC(NULL);
// 	g_pMyDDSFront->Flip(NULL,NULL);
}

void CDDrawSurfaceHook::UnHook()
{
// 	CInlineHook::UnInlineHook(&IsLost_hook);
// 	CInlineHook::UnInlineHook(&Restore_hook);
	CInlineHook::UnInlineHook(&DeleteAttachedSurface_hook);
	CInlineHook::UnInlineHook(&Blt_hook);
	CInlineHook::UnInlineHook(&BltBatch_hook);
	CInlineHook::UnInlineHook(&BltFast_hook);
//	CInlineHook::UnInlineHook(&ReleaseDC_hook);
	CInlineHook::UnInlineHook(&Flip_hook);
	CInlineHook::UnInlineHook(&SetColorKey_hook);

}

void CDDrawSurfaceHook::Init(DWORD Base)
{
	m_Base=Base;

	//////////////////////////////////////////////////////////////////////////
// 	IsLost_hook.NewAddres=(DWORD)naked_IsLost;
// 	IsLost_hook.OldAddres=GetFunOff(0x60);
// 	//////////////////////////////////////////////////////////////////////////
// 	Restore_hook.NewAddres=(DWORD)naked_Restore;
// 	Restore_hook.OldAddres=GetFunOff(0x6c);
	//////////////////////////////////////////////////////////////////////////
	DeleteAttachedSurface_hook.NewAddres=(DWORD)naked_DeleteAttachedSurface;
	DeleteAttachedSurface_hook.OldAddres=GetFunOff(g_2DGameBase.DeleteAttachedSurface_off);
	//////////////////////////////////////////////////////////////////////////
	Blt_hook.NewAddres=(DWORD)naked_Blt;
	Blt_hook.OldAddres=GetFunOff(g_2DGameBase.Blt_off);
	//////////////////////////////////////////////////////////////////////////
	BltBatch_hook.NewAddres=(DWORD)naked_BltBatch;
	BltBatch_hook.OldAddres=GetFunOff(g_2DGameBase.BltBatch_off);
	//////////////////////////////////////////////////////////////////////////
	BltFast_hook.NewAddres=(DWORD)naked_BltFast;
	BltFast_hook.OldAddres=GetFunOff(g_2DGameBase.BltFast_off);
	//////////////////////////////////////////////////////////////////////////
	SetColorKey_hook.NewAddres=(DWORD)naked_SetColorKey;
	SetColorKey_hook.OldAddres=GetFunOff(g_2DGameBase.SetColorKey_off);
	//////////////////////////////////////////////////////////////////////////
// 	ReleaseDC_hook.NewAddres=(DWORD)naked_ReleaseDC;
// 	ReleaseDC_hook.OldAddres=GetFunOff(0x68);
	//////////////////////////////////////////////////////////////////////////
	Flip_hook.NewAddres=(DWORD)naked_Flip;
	Flip_hook.OldAddres=GetFunOff(g_2DGameBase.Flip_off);
	////////////////////////////////////////////////////////////////////////// 

}

DWORD CDDrawSurfaceHook::GetFunOff(DWORD off)
{
	DWORD dwRet=0;
	dwRet=CInlineHook::GetMemValue(m_Base);
	dwRet=CInlineHook::GetMemValue(dwRet+off);
	return dwRet;
}

NAKED void CDDrawSurfaceHook::naked_IsLost()
{
	ASM
	{
		xor eax,eax;
		ret 4;
	}
}
NAKED void CDDrawSurfaceHook::naked_Restore()
{
	ASM
	{
		xor eax,eax;
		ret 4;
	}	
}
NAKED void CDDrawSurfaceHook::naked_DeleteAttachedSurface()
{
	ASM
	{
		xor eax,eax;
		ret 12;
	}
}
NAKED void CDDrawSurfaceHook::naked_Blt()
{
	ASM
	{
		xor eax,eax;
		ret 24 ;
	}
}
NAKED void CDDrawSurfaceHook::naked_BltBatch()
{
	ASM
	{
		xor eax,eax;
		ret 16;
	}
}
NAKED void CDDrawSurfaceHook::naked_BltFast()
{
	ASM
	{
		xor eax,eax;
		ret 24;
	}	
}
NAKED void CDDrawSurfaceHook::naked_SetColorKey()
{
	ASM
	{
		xor eax,eax;
		ret 12;
	}
}
NAKED void CDDrawSurfaceHook::naked_ReleaseDC()
{
	ASM
	{
		xor eax,eax;
		ret 8;
	}	
}
NAKED void CDDrawSurfaceHook::naked_Flip()
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	if (g_2dCPU>10)
	{
		g_2dCPU=0;
	}
	g_2dCPU++;
	ASM
	{
		mov esp,ebp;
		pop ebp;
		xor eax,eax;
		ret 12;
	}

}  