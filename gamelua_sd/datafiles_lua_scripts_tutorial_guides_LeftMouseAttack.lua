
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	self.counter = 0
	self.actions = {
		'LeftKey_Skill'
	}
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	ui.TutorialVideoFrame:ShowVideo('Attack')
	self.counter = 0
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	ui.TutorialVideoFrame:HideVideo()
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	local pGameInput = SD.CHSGameInput:Instance()
	for i, v in ipairs(self.actions) do
		if pGameInput:IsActionTriggered(v) then
			self.counter = self.counter + 1
			if self.counter >= 10 then
				self.completed = true
				break
			end
		end
	end
	
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

