
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	self.rightIndicator = nil
	self.windowAttached = false
	
	-- Server Completion
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	-- Create Indicator
	self.rightIndicator = ui.createFrame('RightIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.rightIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
		
	-- Attach to MainMenu_TempEquip Window
	self:TryAttachWindow()
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	if self.rightIndicator then
		ui.destroyFrame(self.rightIndicator)
		self.rightIndicator = nil
	end
	
	self.windowAttached = false
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function TryAttachWindow(self)
	if self.windowAttached then 
		return
	end

	local dragBtn = InitWindowPtr('NpcDialogHoz/Background/Content/Below')
	if  not dragBtn or not dragBtn:isVisible() then
		return
	end

	self.rightIndicator:Show()
	UiUtility.AttachWndToWnd(self.rightIndicator.pWindow, dragBtn, UiUtility.PUT_LEFT)
		
	-- Add Animation
	local animName = guideMgr.indicator_animations['RIGHT']
	SD.WndAnimManager:Instance():CreateAnim(self.rightIndicator.pWindow, animName, false, true, true)
	
	self.windowAttached = true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	self:TryAttachWindow()
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

