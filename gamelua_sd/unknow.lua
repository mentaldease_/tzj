
CEGUI.Point = CEGUI.Vector2 
function CEGUI.iterator_impl_next(p)
if p:isAtEnd() then
return nil;
end
local k,v = p:key(), p:value();
p:next();
return k,v;
end
function CEGUI.iterator_impl(self)
return CEGUI.iterator_impl_next, self;
end 
CEGUI.PropertyIterator.iterator = CEGUI.iterator_impl; 
CEGUI.EventIterator.iterator = CEGUI.iterator_impl; 
CEGUI.WindowIterator.iterator = CEGUI.iterator_impl; 
CEGUI.WindowFactoryIterator.iterator = CEGUI.iterator_impl; 
CEGUI.FalagardMappingIterator.iterator = CEGUI.iterator_impl; 
CEGUI.ImagesetIterator.iterator = CEGUI.iterator_impl; 
CEGUI.ImageIterator.iterator = CEGUI.iterator_impl; 
CEGUI.SchemeIterator.iterator = CEGUI.iterator_impl; 
CEGUI.FontIterator.iterator = CEGUI.iterator_impl; 
function CEGUI.toMouseCursorEventArgs(e)
return tolua.cast(e,"const CEGUI::MouseCursorEventArgs")
end
function CEGUI.toWindowEventArgs(e)
return tolua.cast(e,"const CEGUI::WindowEventArgs")
end
function CEGUI.toActivationEventArgs(e)
return tolua.cast(e,"const CEGUI::ActivationEventArgs")
end
function CEGUI.toHeaderSequenceEventArgs(e)
return tolua.cast(e,"const CEGUI::HeaderSequenceEventArgs")
end
function CEGUI.toMouseEventArgs(e)
return tolua.cast(e,"const CEGUI::MouseEventArgs")
end
function CEGUI.toKeyEventArgs(e)
return tolua.cast(e,"const CEGUI::KeyEventArgs")
end
function CEGUI.toDragDropEventArgs(e)
return tolua.cast(e,"const CEGUI::DragDropEventArgs")
end
function CEGUI.toTreeEventArgs(e)
return tolua.cast(e,"const CEGUI::TreeEventArgs")
end
function CEGUI.toRenderQueueEventArgs(e)
return tolua.cast(e,"const CEGUI::RenderQueueEventArgs")
end
CEGUI.EventArgs.toMouseCursorEventArgs = CEGUI.toMouseCursorEventArgs
CEGUI.EventArgs.toWindowEventArgs = CEGUI.toWindowEventArgs
CEGUI.EventArgs.toActivationEventArgs = CEGUI.toActivationEventArgs
CEGUI.EventArgs.toHeaderSequenceEventArgs = CEGUI.toHeaderSequenceEventArgs
CEGUI.EventArgs.toMouseEventArgs = CEGUI.toMouseEventArgs
CEGUI.EventArgs.toKeyEventArgs = CEGUI.toKeyEventArgs
CEGUI.EventArgs.toDragDropEventArgs = CEGUI.toDragDropEventArgs
CEGUI.EventArgs.toTreeEventArgs = CEGUI.toTreeEventArgs
CEGUI.EventArgs.toRenderQueueEventArgs = CEGUI.toRenderQueueEventArgs 
function CEGUI.toButtonBase(w)
return tolua.cast(w,"CEGUI::ButtonBase")
end
function CEGUI.toCheckbox(w)
return tolua.cast(w,"CEGUI::Checkbox")
end
function CEGUI.toCombobox(w)
return tolua.cast(w,"CEGUI::Combobox")
end
function CEGUI.toComboDropList(w)
return tolua.cast(w,"CEGUI::ComboDropList")
end
function CEGUI.toDragContainer(w)
return tolua.cast(w,"CEGUI::DragContainer")
end
function CEGUI.toEditbox(w)
return tolua.cast(w,"CEGUI::Editbox")
end
function CEGUI.toFrameWindow(w)
return tolua.cast(w,"CEGUI::FrameWindow")
end
function CEGUI.toGridLayoutContainer(w)
return tolua.cast(w,"CEGUI::GridLayoutContainer")
end
function CEGUI.toGUISheet(w)
return tolua.cast(w,"CEGUI::GUISheet")
end
function CEGUI.toHorizontalLayoutContainer(w)
return tolua.cast(w,"CEGUI::HorizontalLayoutContainer")
end
function CEGUI.toItemEntry(w)
return tolua.cast(w,"CEGUI::ItemEntry")
end
function CEGUI.toItemListBase(w)
return tolua.cast(w,"CEGUI::ItemListBase")
end
function CEGUI.toItemListbox(w)
return tolua.cast(w,"CEGUI::ItemListbox")
end
function CEGUI.toLayoutContainer(w)
return tolua.cast(w,"CEGUI::LayoutContainer")
end
function CEGUI.toListbox(w)
return tolua.cast(w,"CEGUI::Listbox")
end
function CEGUI.toListHeader(w)
return tolua.cast(w,"CEGUI::ListHeader")
end
function CEGUI.toListHeaderSegment(w)
return tolua.cast(w,"CEGUI::ListHeaderSegment")
end
function CEGUI.toMenubar(w)
return tolua.cast(w,"CEGUI::Menubar")
end
function CEGUI.toMenuBase(w)
return tolua.cast(w,"CEGUI::MenuBase")
end
function CEGUI.toMenuItem(w)
return tolua.cast(w,"CEGUI::MenuItem")
end
function CEGUI.toMultiColumnList(w)
return tolua.cast(w,"CEGUI::MultiColumnList")
end
function CEGUI.toMultiLineEditbox(w)
return tolua.cast(w,"CEGUI::MultiLineEditbox")
end
function CEGUI.toPopupMenu(w)
return tolua.cast(w,"CEGUI::PopupMenu")
end
function CEGUI.toProgressBar(w)
return tolua.cast(w,"CEGUI::ProgressBar")
end
function CEGUI.toPushButton(w)
return tolua.cast(w,"CEGUI::PushButton")
end
function CEGUI.toRadioButton(w)
return tolua.cast(w,"CEGUI::RadioButton")
end
function CEGUI.toScrollablePane(w)
return tolua.cast(w,"CEGUI::ScrollablePane")
end
function CEGUI.toScrollbar(w)
return tolua.cast(w,"CEGUI::Scrollbar")
end
function CEGUI.toScrolledContainer(w)
return tolua.cast(w,"CEGUI::ScrolledContainer")
end
function CEGUI.toScrolledItemListBase(w)
return tolua.cast(w,"CEGUI::ScrolledItemListBase")
end
function CEGUI.toSequentialLayoutContainer(w)
return tolua.cast(w,"CEGUI::SequentialLayoutContainer")
end
function CEGUI.toSlider(w)
return tolua.cast(w,"CEGUI::Slider")
end
function CEGUI.toSpinner(w)
return tolua.cast(w,"CEGUI::Spinner")
end
function CEGUI.toTabButton(w)
return tolua.cast(w,"CEGUI::TabButton")
end
function CEGUI.toTabControl(w)
return tolua.cast(w,"CEGUI::TabControl")
end
function CEGUI.toTabPane(w)
return tolua.cast(w,"CEGUI::TabPane")
end
function CEGUI.toThumb(w)
return tolua.cast(w,"CEGUI::Thumb")
end
function CEGUI.toTooltip(w)
return tolua.cast(w,"CEGUI::Tooltip")
end
function CEGUI.toTree(w)
return tolua.cast(w,"CEGUI::Tree")
end
function CEGUI.toVerticalLayoutContainer(w)
return tolua.cast(w,"CEGUI::VerticalLayoutContainer")
end
CEGUI.Window.toButtonBase = CEGUI.toButtonBase
CEGUI.Window.toCheckbox = CEGUI.toCheckbox
CEGUI.Window.toCombobox = CEGUI.toCombobox
CEGUI.Window.toComboDropList = CEGUI.toComboDropList
CEGUI.Window.toDragContainer = CEGUI.toDragContainer
CEGUI.Window.toEditbox = CEGUI.toEditbox
CEGUI.Window.toFrameWindow = CEGUI.toFrameWindow
CEGUI.Window.toGridLayoutContainer = CEGUI.toGridLayoutContainer
CEGUI.Window.toGUISheet = CEGUI.toGUISheet
CEGUI.Window.toHorizontalLayoutContainer = CEGUI.toHorizontalLayoutContainer
CEGUI.Window.toItemEntry = CEGUI.toItemEntry
CEGUI.Window.toItemListBase = CEGUI.toItemListBase
CEGUI.Window.toItemListbox = CEGUI.toItemListbox
CEGUI.Window.toLayoutContainer = CEGUI.toLayoutContainer
CEGUI.Window.toListbox = CEGUI.toListbox
CEGUI.Window.toListHeader = CEGUI.toListHeader
CEGUI.Window.toListHeaderSegment = CEGUI.toListHeaderSegment
CEGUI.Window.toMenubar = CEGUI.toMenubar
CEGUI.Window.toMenuBase = CEGUI.toMenuBase
CEGUI.Window.toMenuItem = CEGUI.toMenuItem
CEGUI.Window.toMultiColumnList = CEGUI.toMultiColumnList
CEGUI.Window.toMultiLineEditbox = CEGUI.toMultiLineEditbox
CEGUI.Window.toPopupMenu = CEGUI.toPopupMenu
CEGUI.Window.toProgressBar = CEGUI.toProgressBar
CEGUI.Window.toPushButton = CEGUI.toPushButton
CEGUI.Window.toRadioButton = CEGUI.toRadioButton
CEGUI.Window.toScrollablePane = CEGUI.toScrollablePane
CEGUI.Window.toScrollbar = CEGUI.toScrollbar
CEGUI.Window.toScrolledContainer = CEGUI.toScrolledContainer
CEGUI.Window.toScrolledItemListBase = CEGUI.toScrolledItemListBase
CEGUI.Window.toSequentialLayoutContainer = CEGUI.toSequentialLayoutContainer
CEGUI.Window.toSlider = CEGUI.toSlider
CEGUI.Window.toSpinner = CEGUI.toSpinner
CEGUI.Window.toTabButton = CEGUI.toTabButton
CEGUI.Window.toTabControl = CEGUI.toTabControl
CEGUI.Window.toTabPane = CEGUI.toTabPane
CEGUI.Window.toThumb = CEGUI.toThumb
CEGUI.Window.toTooltip = CEGUI.toTooltip
CEGUI.Window.toTree = CEGUI.toTree
CEGUI.Window.toVerticalLayoutContainer = CEGUI.toVerticalLayoutContainer local CLUASCRIPT_HOOKSTR_iCount = 1;   function CLUASCRIPT_HOOKSTR_trace(event, line)     CLUASCRIPT_HOOKSTR_iCount = (CLUASCRIPT_HOOKSTR_iCount + 1) % 100          if CLUASCRIPT_HOOKSTR_iCount == 0 then				    local iRunTime = CLUASCRIPT_CheckTime()         if (iRunTime > 2) then				            local INFINITE_LOOP_ERROR 			INFINITE_LOOP_ERROR = INFINITE_LOOP_ERROR + 1         end     end end  debug.sethook(CLUASCRIPT_HOOKSTR_trace, "n", 1000000)local CLUASCRIPT_HOOKSTR_iCount = 1;   function CLUASCRIPT_HOOKSTR_trace(event, line)     CLUASCRIPT_HOOKSTR_iCount = (CLUASCRIPT_HOOKSTR_iCount + 1) % 100          if CLUASCRIPT_HOOKSTR_iCount == 0 then				    local iRunTime = CLUASCRIPT_CheckTime()         if (iRunTime > 2) then				            local INFINITE_LOOP_ERROR 			INFINITE_LOOP_ERROR = INFINITE_LOOP_ERROR + 1         end     end end  debug.sethook(CLUASCRIPT_HOOKSTR_trace, "n", 1000000)local CLUASCRIPT_HOOKSTR_iCount = 1;   function CLUASCRIPT_HOOKSTR_trace(event, line)     CLUASCRIPT_HOOKSTR_iCount = (CLUASCRIPT_HOOKSTR_iCount + 1) % 100          if CLUASCRIPT_HOOKSTR_iCount == 0 then				    local iRunTime = CLUASCRIPT_CheckTime()         if (iRunTime > 2) then				            local INFINITE_LOOP_ERROR 			INFINITE_LOOP_ERROR = INFINITE_LOOP_ERROR + 1         end     end end  debug.sethook(CLUASCRIPT_HOOKSTR_trace, "n", 1000000)-- animation
Skeleton("PC_GB1")
Animation("HG")
Animation("HG_PA")
Animation("HG_life")
Animation("GB_ST")
Animation("GB_PA")
Animation("HIT")
Animation("HG_Arrow")
Animation("RideHorse_HG")
Animation("RideCamel_HG")
Animation("HG_FishingRod")
Animation("N1_life01")
Animation("N1_life02")
Animation("N1_life03")
Animation("THD_FL")
Animation("THD_SW")
Animation("SL_ST")
Animation("SL_SB")
Animation("HG_SL_ST")
Animation("QZ_SW")
Animation("QZ_PA")
Animation("RH_HG")
Animation("QZ_SW_NEW")
Animation("HG_CS02")
Animation("HG_CS_03")
Animation("HG_CS04")
Animation("BTS_FAN")
STake("total")


-- paperdoll
--Equip("PC1_NeiYi01_Body","",1)
--Equip("PC1_NeiYi01_Glove","",2)
--Equip("PC1_NeiYi01_Helmet","",3)
--Equip("PC1_NeiYi01_Leg","",4)
--Equip("W_Crutch_02_A1","",5)


Dimension(19, 43)
BoundingBox(40,40,43,-40,-40,0,0,0,0)


-- blend pattern
BlendPattern("PC_male")

package.path = package.path .. ";..\\char\\?.lua"
require("common")

------------------------------------------------通用Doodad对话头像待机------------------------------
STake_Start("NPC_wait_000_SLST_ui","char_wait_000")  --对话头像
	BlendMode(0)
	BlendTime(0)
	Loop()

	
STake_Start("char_wait_000","char_wait_000")  
	BlendMode(0)
	BlendTime(0)
	Loop()	
	
STake_Start("char_wait_00000","char_wait_000")  
	BlendMode(0)
	BlendTime(0)
	Loop()		
	Priority(0,"4")	
	
------------------------------------------------试炼之地的BUFF们在此集结-----------------------------

STake_Start("NPC_slzd_ljf","char_wait_006")        ---龙卷风子弹 
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"4")	
	
    --Fx(0.01,"FX_ZC_skill_02.fx","Reference",1.3,0,0,-15,25,0,0,0,1)	
    Fx(0.17,"FX_ZC_skill_02.fx","Reference",1.3,0,0,25,25,0,0,0,1)	
	Charge(0,0.216,50,0,16) 
	--Sound(0.01,"NB_TLKH_skill002start")
	
	AttackStart()
    Hurt(0,"hurt_81",0,1)
	DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)
	--HurtSound(0.05,"BTS_CR_HIT_new_2_1") 
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)	
	
	
STake_Start("NPC_slzd_bao","char_wait_006")         --试炼之地版的爆 521
    BlendMode(0)
    BlendTime(0) 
    Priority(0,"5") 
	
    Fx(0.01,"N_T1_JGQC_skill_000_hit","Reference",1.5,1,0,0,20,0,0,0,0.8)
	AttackStart()
	Hurt(0,"hurt_21",0,1) 
	
STake_Start("NPC_slzd_jian","char_wait_006")    --试炼之地放箭
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.01,"N_N1_XLGB01_skill_002_aoe","Reference")

  	AttackStart()
    Hurt(0.2,"hurt_21")
	DirFx(0.2,"N_N1_XLGB01_skill_002_hit",0,1)
    HurtBoneFx(0.2,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
	
	Hurt(0.4,"hurt_21")
	DirFx(0.4,"N_N1_XLGB01_skill_002_hit",0,1)
    HurtBoneFx(0.4,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
	
	Hurt(0.6,"hurt_21")
	DirFx(0.6,"N_N1_XLGB01_skill_002_hit",0,1)
    HurtBoneFx(0.6,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
	
	Hurt(0.8,"hurt_21")
	DirFx(0.8,"N_N1_XLGB01_skill_002_hit",0,1)
    HurtBoneFx(0.8,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
	
------------------------------------------------战场技能NPC技能--------------------------------------


STake_Start("NPC_life_ZC_006","char_wait_006")         --铁索拦江    16191
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(0.01,"")

	AttackStart()
    Hurt(0,"hurt_81",0,1)
    DirFx(0,"NB_HYXZ_skill_003_hit",25,1)		
    HurtBoneFx(0,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)	
	
STake_Start("NPC_life_ZC_012","char_wait_006")         --万蛇狂舞   16199
    BlendMode(0)
    BlendTime(0) 
    Priority(0,"5") 
	FrameTime(0,1)
	Loop()
    Fx(0.01,"HG_life_ZC_012","Reference",2,1,0,0,0,0,0,0,1,1)
    CampFx(0.01,"","","HG_QZ_SW_QXQJ_D","Reference",0.8,1,0,0,0,0,0,0,1)
    CampFx(2,"","","HG_QZ_SW_QXQJ_D_loop","Reference",0.8,1,0,0,0,0,0,0,1,3)
	
	AttackStart()
    Hurt(0,"hurt_21",0,1)
    HurtBoneFx(0,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)	
	
STake_Start("NPC_life_ZC_015_start","char_wait_006")         --黏黏药水 16203
    BlendMode(0)
    BlendTime(0) 
    Priority(0,"5") 
	Loop(0.4,1.4)
    Fx(0.4,"HG_life_emo015_endloop","Reference",1.5,1,0,0,0,0,0,0,1,1)	
    CampFx(0.01,"","","HG_QZ_SW_QXQJ_D","Reference",0.4,1,0,0,0,0,0,0,1)
    CampFx(2,"","","HG_QZ_SW_QXQJ_D_loop","Reference",0.4,1,0,0,0,0,0,0,1,3)
	
	AttackStart()
	Hurt(0,"hurt_21",0,1) 

STake_Start("NPC_life_ZC_015_fire","char_wait_006")         --黏黏药水 16203
    BlendMode(0)
    BlendTime(0) 
    Priority(0,"5") 	
    Fx(0.01,"HG_life_emo015_end","Reference",1,1,0,0,0,0,0,0,1,1)		

    
STake_Start("NPC_life_ZC_Equip2","char_wait_006")         --爆 65221/65225
    BlendMode(0)
    BlendTime(0) 
    Priority(0,"5") 
	
    Fx(0.01,"N_T1_JGQC_skill_000_hit","Reference",1.5,1,0,0,20,0,0,0,0.8)	
	AttackStart()
	Hurt(0,"hurt_21",0,1) 
	
STake_Start("NPC_nulei","char_wait_003")         --杀士怒雷强击   
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(0.01,"FX_ZC_Buff_SS_Thunder","Reference")

	AttackStart()
    Hurt(0,"hurt_21",0,1)
    DirFx(0,"NB_HYXZ_skill_003_hit",25,1)		
	HurtSound(0,"BTS_CR_HIT_new_e1")
    HurtBoneFx(0,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)		
	
STake_Start("NPC_SL_ZCskill_flag","char_wait_003")   --战场少林拔旗技能NPC
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(0.01,"N1_SBTR01_skill_002_tracker_1","Reference",1.6,0,0,0,15)
    Fx(1.01,"N1_SBTR01_skill_002_tracker_1","Reference",1.6,0,0,0,15)
    Fx(2.01,"N1_SBTR01_skill_002_tracker_1","Reference",1.6,0,0,0,15)
	Sound(0.1,"SL_RuLaiShenZhang_02")
	Sound(0.03,"HYHY_02_Skill02_02")
	Sound(1.0,"HYHY_02_Skill02_01")

	AttackStart()
    Hurt(0,"hurt_81",0,1)
    DirFx(0,"N1_SBTR01_skill_002_hit",0,1)		
	HurtSound(0,"BTS_CR_HIT_new_e1")
	
STake_Start("NPC_QZ_ZCskill_flag","char_wait_003")   --战场全真拔旗技能NPC
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(0.01,"NB_JB_skill_006_shuizhu","Reference",1,0,0,0,0)
	Sound(0.03,"XLJ_Skill_1")
	Sound(0.03,"QZ_SW_Skill012_Start3")

    AttackStart()
  	Hurt(0.1,"hurt_81")
    DirFx(0.1,"NB_JB_skill_hit",0,1) 
    HurtSound(0.1,"QZ_PA_Hit_11")	
--------------------------------------------------阵营战技能-----------------------------------------
STake_Start("ZYZC_tower_skill01_NPC","char_wait_003")   --阵营战场箭塔冰冻箭技能NPC  33459  引导1.2
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(1.2,"FX_GD_trigger01_A_att01_ground","Reference",1,0,0,0,0)

	AttackStart()
	Hurt(1.2,"hurt_21")
	DirFx(1.2,"FX_xihun_hit",0,1)
    HurtBoneFx(1.2,"FX_blood_jian_b","Spine1",1,0,0,0,0,0,0)	


STake_Start("ZYZC_tower_skill02_NPC","char_wait_003")   --阵营战场箭塔爆炸箭技能NPC  33461  引导1.2
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(1.2,"FX_GD_trigger01_A_att02_ground","Reference",1,0,0,0,0)

	AttackStart()
	Hurt(1.2,"hurt_21")
	DirFx(1.2,"N_T1_JGQC_skill_000_hit",0,1)
    HurtBoneFx(1.2,"FX_blood_jian_b","Spine1",1,0,0,0,0,0,0)	


STake_Start("ZYZC_tower_skill03_NPC","char_wait_003")   --阵营战场箭塔普通箭技能NPC  33463  引导1.2
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(1.2,"FX_GD_trigger01_A_att03_ground","Reference",1,0,0,0,0)

	AttackStart()
	Hurt(1.2,"hurt_21")
	DirFx(1.2,"N1_Dart_skill_001_hit",0,1)
    HurtBoneFx(1.2,"FX_blood_jian_b","Spine1",1,0,0,0,0,0,0)	


STake_Start("GD_PR_fireA_skill02_npc","char_wait_003")    --技能  531  自爆技能NPC
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,0.5)
	Fx(0.1,"FX_Common_Monster_baozha","Reference",2,0,0,0,0,0,0,0,1,2)

	AttackStart()
	Hurt(0.1,"hurt_81")
	DirFx(0.1,"N_N1_XLGB01_skill_002_hit",0,1)
    HurtBoneFx(0.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(0.1,"ShakeDistance = 300","ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

------------------------------------------------丐帮技能NPC技能--------------------------------------

--[[STake_Start("NPC_GB_PA_skill_020_fire","char_wait_006")        ---玩家丐帮龙砸地 17115
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	
    Fx(0.01,"HG_GB_PA_skill_020_all_c","Reference",1,1,0,0,0,0,0,0,1)	
	
	AttackStart()
    Hurt(0,"hurt_81",0,1)
    DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)	
   	HurtSound(0.05,"PA_Hit_002") 	]]--
	
STake_Start("NPC_GB_PA_skill_025A_Q2_skillnpc","char_wait_006")   --断头摔2
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5")	

	AttackStart()
    Hurt(0,"hurt_81",0,2)	
    AttackHurtFx(0,"HG_GB_PA_attack_B_001_hit","Spine1",1) 		
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,10,0,0,0)	
   	HurtSound(0.05,"BTS_CR_HIT_new_2_1")	

STake_Start("NPC_GB_PA_skill_025A_Q2_xuneng_skillnpc","char_wait_006")   --断头摔爆
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5")	
    Fx(0.01,"HG_GB_PA_skill_025A_bao")
	
	AttackStart()
    Hurt(0,"hurt_71",0,2)	
    AttackHurtFx(0,"HG_GB_PA_attack_B_001_hit","Spine1",1) 		
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,10,0,0,0)	
   	HurtSound(0.05,"BTS_CR_HIT_new_2_1")	
	
STake_Start("NPC_GB_PA_skill_yanjia_skillnpc","char_wait_006")   --岩甲爆   17320
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5")	
    Fx(0.01,"HG_GB_PA_skill_shikai_02","Reference",1,0,0,0,20,0,0,0,1)
	
	AttackStart()
    Hurt(0,"hurt_21",0,2)	
    AttackHurtFx(0,"HG_GB_PA_attack_B_001_hit","Spine1",1) 		
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,10,0,0,0)	
   	HurtSound(0.05,"BTS_CR_HIT_new_2_1")
	
STake_Start("NPC_GB_PA_skill_026_skillnpc","char_wait_006")   --擒贼擒王
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5")	

	AttackStart()
    Hurt(0,"hurt_81",0,2)
    AttackHurtFx(0,"HG_GB_PA_attack_B_001_hit","Spine1",1) 		
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,10,0,0,0)	
   	HurtSound(0.05,"BTS_CR_HIT_new_2_1")		
	
STake_Start("NPC_GB_PA_skill_025B_Q_skillnpc","char_wait_006")        ---饭刚落石头 
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")

    Fx(0.01,"HG_GB_PA_skill_025B_Q_b","",1.3,0,0,0,0,0,0,0,1)
	
	AttackStart()	
    Hurt(0,"hurt_81",0,1)
    AttackHurtFx(0,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,10,0,0,0)
    HurtSound(0.05,"BTS_CR_HIT_new_2_1") 		

STake_Start("NPC_GB_PA_skill_018_skillnpc","char_wait_006")        ---饭刚落石头 17203
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	--Sound(1.7,"XLJ_Down_Hurt3")

    --Fx(1.7,"HG_GB_PA_skill_025B_Q_b","",1.3,0,0,0,0,0,0,0,1)
	
	AttackStart()	
    Hurt(0,"hurt_81",0,1)
    AttackHurtFx(0,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,10,0,0,0)
    HurtSound(0.05,"BTS_CR_HIT_new_2_1") 	
	
STake_Start("NPC_GB_PA_skill_024_Q2_fire","char_wait_006")        ---神龙摆尾子弹 17531
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"4")	
	
    Fx(0.01,"HG_GB_PA_skill_024_E1","Reference",1.3,0,0,-25,25,0,0,0,1)	
    Fx(0.17,"HG_GB_PA_skill_024_E2","Reference",1.3,0,0,5,25,0,0,0,1)	
	Charge(0,0.216,50,0,16) 
	Sound(0.01,"NB_TLKH_skill002start")
	
	AttackStart()
    Hurt(0,"hurt_81",0,1)
	DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)
	HurtSound(0.05,"BTS_CR_HIT_new_2_1") 
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
	
	
--[[STake_Start("NPC_GB_PA_skill_025B_E1","char_wait_006")        ---空中砸皮球 17556
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")	
    Fx(0.2,"HG_GB_PA_skill_025AB_E_hit_a")	
    Fx(0.2,"HG_GB_PA_skill_025AB_E_hit_b")	
	
	AttackStart()
    Hurt(0,"hurt_81",0,1)
	DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)
	HurtSound(0,"BTS_CR_HIT_new_2_1") 
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
	
STake_Start("NPC_GB_PA_skill_023_Q2_fire","char_wait_006")        ---百烈拳2秘籍爆点 17506
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")	
	
    Fx(0.01,"HG_GB_PA_skill_023_Q2_1_fire","Reference",1.3,0,0,0,30,0,0,0,0.8)	
	
	AttackStart()
    Hurt(0.5,"hurt_81",0,1)
	DirFx(0.5,"HG_GB_PA_attack_B_001_hit",25,1)
	HurtSound(0.5,"BTS_CR_HIT_new_2_1") 
	HurtBoneFx(0.5,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)	


STake_Start("NPC_GB_PA_skill_023_E_fire1","char_wait_006")   --超级拳秘籍爆点 17511
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5")	

  	Fx(0.01,"HG_GB_PA_skill_023_E+","",1.5,0,0,-20,0,0,0,0,0.85,1,0,1) 
    HitGroundFx(0.64,"SFX_hitground",0.6,0,-40,0)   		 		
    HitGroundFx(0.64,"SFX_hitground2",0.6,0,-40,0)
    HitGroundFx(0.64,"SFX_hitground",0.6,0,-60,0)   		 		
    HitGroundFx(0.64,"SFX_hitground2",0.6,0,-60,0)
	
  	AttackStart()	
    Hurt(0,"hurt_81",0,1)	
	DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0,"BTS_CR_HIT_new_2_1") 
	HurtBoneFx(0,"FX_Violent_blood_Sharp_R3","Spine1",0.3,0,0,0,180,15,0)	
	
	
STake_Start("NPC_GB_PA_skill_023_npc1","char_wait_006")        ---履霜冰至1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	
	AttackStart()	
	Charge(0,0.15,25,270,8) 
	
STake_Start("NPC_GB_PA_skill_023_npc2","char_wait_006")        ---履霜冰至2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	
	AttackStart()	
	Charge(0,0.15,25,90,8) 	]]--
	
	
STake_Start("NPC_GB_PA_skill_028_npc","char_wait_006")        ---回手旋 17586
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")

	AttackStart()	
    Hurt(0,"hurt_81",0,1)
	DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)		
    HurtSound(0,"BTS_CR_HIT_new_2_1") 
    HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("NPC_GB_PA_skill_024_Q3_fire","char_wait_006")        ---神龙摆尾子弹残留 17532
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")

	--Fx(0.01,"HG_GB_PA_skill_024_Y_start","Reference",1,0,0,0,0,0,0,0,1,1)	
	Fx(0.01,"HG_GB_PA_skill_024_Y_loop","Reference",1,0,0,0,0,0,0,0,1,3)
	
	AttackStart()	
    Hurt(1,"hurt_21",0,1)
	DirFx(1,"FX_Buff_burns",25,1)	
	Hurt(2,"hurt_21",0,1)
	Hurt(3,"hurt_21",0,1)
	Hurt(4,"hurt_21",0,1)
	Hurt(5,"hurt_21",0,1)
	DirFx(2,"FX_Buff_burns",25,1)	
	DirFx(3,"FX_Buff_burns",25,1)	
	DirFx(4,"FX_Buff_burns",25,1)	
	DirFx(5,"FX_Buff_burns",25,1)	

STake_Start("GB_CD_SB","char_wait_000")      --丐帮冲荡闪避（乘风势）  17413
    BlendMode(0)
    BlendTime(0.0)   
	
    AttackStart()
    Hurt(0.05,"hurt_81")
    DirFx(0.05,"NB_MZWM_skill_hit",0,1) 
	RangeCameraShake(0.05,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.33","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")

	


-----------------------------------------------------------------桃花岛技能NPC技能--------------------------------------------

STake_Start("NPC_THD_SW_skill_006_npc","char_wait_000")   --燕返 
    BlendMode(0)
    BlendTime(0)
    AttackStart()
    Hurt(0.06,"hurt_62")
    Hurt(0.12,"hurt_62")
    Hurt(0.18,"hurt_62")
    Hurt(0.25,"hurt_82")
    HurtSound(0.06,"BTS_FAN_Hit_001")
    HurtSound(0.12,"BTS_FAN_Hit_001")
    HurtSound(0.18,"BTS_FAN_Hit_001")
    HurtSound(0.25,"BTS_FAN_Hit_001")
	DirFx(0.06,"HG_THD_SW_skill_006005_hit",25,1,1,0,0)
	DirFx(0.12,"HG_THD_SW_skill_006005_hit",25,1,1,0,0)
	DirFx(0.18,"HG_THD_SW_skill_006005_hit",25,1,1,0,0)	
	DirFx(0.25,"HG_THD_SW_skill_006005_hit",25,1,1,0,0)	
    HurtBoneFx(0.06,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.12,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.18,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.25,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


	
STake_Start("sifangzhan_npc1","char_wait_000")   --四方斩1
    BlendMode(0)
    BlendTime(0)
	
	Sound(0.01,"TY_Attack_015")
	Fx(0.01,"HG_THD_SW_skill_010_001")	
    AttackStart()	
	Hurt(0.06,"hurt_62")
	HurtSound(0.06,"Big_blade_Hit_002_1")
    HurtBoneFx(0.06,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
STake_Start("sifangzhan_npc2","char_wait_000")   --四方斩2
    BlendMode(0)
    BlendTime(0)
	Sound(0.01,"HG_THD_SW_skill_002")
	Fx(0.01,"HG_THD_SW_skill_010_002")	
    AttackStart()	
	Hurt(0.06,"hurt_62")
	HurtSound(0.03,"QZ_PA_Hit_12")
    HurtBoneFx(0.06,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

STake_Start("sifangzhan_npc3","char_wait_000")   --四方斩3
    BlendMode(0)
    BlendTime(0)
	Sound(0.01,"TY_Attack_018")
	Fx(0.01,"HG_THD_SW_skill_010_003")	
    AttackStart()	
	Hurt(0.06,"hurt_62")
	HurtSound(0.06,"T1_Bow_hit")
    HurtBoneFx(0.06,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

STake_Start("sifangzhan_npc4","char_wait_000")   --四方斩4
    BlendMode(0)
    BlendTime(0)
	Sound(0.01,"TY_Attack_001")
	Fx(0.01,"HG_THD_SW_skill_010_004")	
    AttackStart()	
	Hurt(0.06,"hurt_62")	
	HurtSound(0.06,"BTS_FAN_Hit_001")
    HurtBoneFx(0.06,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	

STake_Start("NPC_THD_SW_skill_005_npc","char_wait_000")   --隐秘一击 
    BlendMode(0)
    BlendTime(0)
    AttackStart()
    Hurt(0.3,"hurt_21")
    Hurt(0.32,"hurt_21")
    Hurt(0.34,"hurt_21")
    Hurt(0.36,"hurt_21")
    Hurt(0.38,"hurt_21")
    Hurt(0.40,"hurt_21")
    HurtSound(0.3,"QZ_SW_Hit_11")
    HurtSound(0.32,"QZ_SW_Hit_11")
    HurtSound(0.34,"QZ_SW_Hit_11")
    HurtSound(0.36,"QZ_SW_Hit_11")
    HurtSound(0.38,"QZ_SW_Hit_11")
    HurtSound(0.40,"QZ_SW_Hit_11")
        --Fx(0.5,"HG_QZ_SW_skill_013_hit02") 
--	Fx(0,"HG_THD_SW_skill_012_fire01","Reference",1,1,0,0,0) 
--    Fx(0,"HG_THD_SW_skill_012_fire02","Reference",1,1,0,0,0)


STake_Start("HG_THD_SW_attack_B_001_npc","char_wait_000")   --疾影刺三连发	12342
    BlendMode(0)
    BlendTime(0)
	
    AttackStart()
    Hurt(0.2,"hurt_21")
    Hurt(0.2,"hurt_21")
    Hurt(0.2,"hurt_21")
    Hurt(0.2,"hurt_21")
    HurtSound(0.2,"QZ_SW_Hit_11")
    HurtSound(0.2,"QZ_SW_Hit_11")
    HurtSound(0.2,"QZ_SW_Hit_11")
    HurtSound(0.2,"QZ_SW_Hit_11")

	Fx(0.01,"HG_THD_SW_attack_C_JA_001","",0.7,0,0,0,0,0,0,0,1.2)	

STake_Start("NPC_THD_SW_skill_012_001_npc","char_wait_006")   --烟幕  12462
    BlendMode(0)
    BlendTime(0)
	
	LoopFx(0,10,1,"HG_THD_SW_skill_012_01_loop","",1)

	
STake_Start("NPC_THD_SW_skill_012_002_npc","char_wait_000")   --影刃  12466
    BlendMode(0)
    BlendTime(0)
	
		
	Charge(0,0.5,200,0,16)
	Charge(0.01,0.5,0,0,0,15)
	
	
STake_Start("NPC_THD_SW_skill_010_007_npc","char_wait_006")   --暴雨梨花
    BlendMode(0)
    BlendTime(0)
	
	AttackStart()
	Hurt(0.2,"hurt_21",10,3) 	
	Hurt(0.4,"hurt_21",10,3) 	
	Hurt(0.6,"hurt_21",10,3) 	
	Hurt(0.8,"hurt_21",10,3) 	
	Hurt(1,"hurt_21",10,3) 	
	Hurt(1.2,"hurt_21",10,3) 	
	Hurt(1.4,"hurt_21",10,3) 	
	Hurt(1.6,"hurt_21",10,3) 	
	Sound(0.01,"SG_THD_SW_skill_01")
	Sound(0.6,"HG_THD_QMDJ_skill_002_Yehuo")
	DirFx(0.2,"SG_THD_SW_attack_hit",28,1,0.7,60,45,30)
	DirFx(0.4,"SG_THD_SW_attack_hit",28,1,0.7,60,90,60)
	DirFx(0.6,"SG_THD_SW_attack_hit",28,1,0.7,60,135,90)
	DirFx(0.8,"SG_THD_SW_attack_hit",28,1,0.7,60,-45,120)
	DirFx(1,"SG_THD_SW_attack_hit",28,1,0.7,60,-90,150)
	DirFx(1.2,"SG_THD_SW_attack_hit",28,1,0.7,60,-135)	
	DirFx(1.4,"SG_THD_SW_attack_hit",28,1,0.7,60,45,30)
	DirFx(1.6,"SG_THD_SW_attack_hit",28,1,0.7,60,90,60)	
	HurtSound(0.2,"THD_Hit_1")
	HurtSound(0.4,"THD_Hit_1")
	HurtSound(0.6,"THD_Hit_1")
	HurtSound(0.8,"THD_Hit_1")
	HurtSound(1,"THD_Hit_1")
	HurtSound(1.2,"THD_Hit_1")
	HurtSound(1.4,"THD_Hit_1")
	HurtSound(1.6,"THD_Hit_1")
    HurtBoneFx(0.2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.4,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.6,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.8,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.4,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.6,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("NPC_THD_SW_attack_A_JA_002_npc","char_wait_000")   --LJA+2技能Npc				12327
    BlendMode(0)
    BlendTime(0)
	
	AttackStart()
    --Pause(0.5,0.07,0,0.1) 
    Hurt(0.17,"hurt_62",10,3)   
	Hurt(0.23,"hurt_62",10,3) 
	Hurt(0.31,"hurt_62",10,3) 
	Hurt(0.38,"hurt_62",10,3) 
	Hurt(0.44,"hurt_62",10,3) 
	Hurt(0.5,"hurt_62",10,3) 
    DirFx(0.17,"SG_THD_SW_attack_hit",28,1,0.7,60,45,30)
	DirFx(0.23,"SG_THD_SW_attack_hit",28,1,0.7,60,90,60)
	DirFx(0.31,"SG_THD_SW_attack_hit",28,1,0.7,60,135,90)
	DirFx(0.38,"SG_THD_SW_attack_hit",28,1,0.7,60,-45,120)
	DirFx(0.44,"SG_THD_SW_attack_hit",28,1,0.7,60,-90,150)
	DirFx(0.5,"SG_THD_SW_attack_hit",28,1,0.7,60,-135)
    HurtSound(0.17,"BTS_FAN_Hit_001") 
	HurtSound(0.23,"BTS_FAN_Hit_001") 
	HurtSound(0.31,"BTS_FAN_Hit_001") 
	HurtSound(0.38,"BTS_FAN_Hit_001") 
	HurtSound(0.44,"BTS_FAN_Hit_001") 
	HurtSound(0.5,"BTS_FAN_Hit_001") 
    HurtBoneFx(0.17,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)	
    HurtBoneFx(0.23,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)	
    HurtBoneFx(0.31,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)	
    HurtBoneFx(0.38,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)	
    HurtBoneFx(0.44,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)	
    HurtBoneFx(0.5,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)	

	
	
	
STake_Start("HG_THD_SW_skill_019_001_npc","char_wait_006")   --机关重重技能Npc 12433
    BlendMode(0)
    BlendTime(0)
    AttackStart()
    Hurt(0.3,"hurt_21")
    Hurt(0.6,"hurt_21")
    Hurt(0.9,"hurt_21")
    Hurt(1.2,"hurt_21")
    Hurt(1.5,"hurt_21")
    Hurt(1.8,"hurt_21")
	Hurt(2.1,"hurt_21")
	Hurt(2.4,"hurt_21")
	DirFx(0.3,"SG_THD_SW_attack_hit",28,1,0.5,60,45)
	DirFx(0.6,"SG_THD_SW_attack_hit",28,1,0.5,50,65)
	DirFx(0.9,"SG_THD_SW_attack_hit",28,1,0.5,70,20)
	DirFx(1.2,"SG_THD_SW_attack_hit",28,1,0.5,15,30)
	DirFx(1.5,"SG_THD_SW_attack_hit",28,1,0.5,35,10)
	DirFx(1.8,"SG_THD_SW_attack_hit",28,1,0.5,0,0)
	DirFx(2.1,"SG_THD_SW_attack_hit",28,1,0.5,51,80)
	DirFx(2.4,"SG_THD_SW_attack_hit",28,1,0.5,30,10)
    HurtSound(0.3,"QZ_SW_Hit_11")
	HurtSound(0.6,"QZ_SW_Hit_11")
	HurtSound(0.9,"QZ_SW_Hit_11")
	HurtSound(1.2,"QZ_SW_Hit_11")
	HurtSound(1.5,"QZ_SW_Hit_11")
	HurtSound(1.8,"QZ_SW_Hit_11")
	HurtSound(2.1,"QZ_SW_Hit_11")
	HurtSound(2.4,"QZ_SW_Hit_11")
	
	
STake_Start("NPC_THD_SW_dodge_001_002+npc","char_wait_006")  		--替身术
    BlendMode(0)
    BlendTime(0)
    --Fx(0,"HG_QZ_SW_skill_013_fire01","Reference",1,1,0,0,28)
	Fx(0.01,"HG_THD_SW_SS_fj_loop","",1,1,0,0,0,0,0,0,0.8)

	
STake_Start("NPC_THD_SW_dodge_001_002+npc_fire","char_wait_006")  		--替身术fire
    BlendMode(0)
    BlendTime(0)
    --Fx(0,"HG_QZ_SW_skill_013_fire01","Reference",1,1,0,0,28)
	Fx(0.01,"HG_THD_SW_SS_fj_fire","",1.5,1,0,0,0,0,0,0,2)	
	
  	AttackStart()
    Hurt(0,"hurt_81")
	DirFx(0,"HG_THD_SW_attack_A_hit",28,1,1,-90,0)

	
STake_Start("test_airR_npc","char_wait_006")   --冥府震落
    BlendMode(0)
    BlendTime(0)
	
	AttackStart()

	Hurt(0.1,"hurt_71",10,3) 

	
STake_Start("HG_THD_SW_skill_009_005_npc","char_wait_006")   --爆震 12432
    BlendMode(0)
    BlendTime(0)
	
	AttackStart()
	Hurt(0.1,"hurt_62")
	
----------------------------------------------------------------少林技能NPC技能------------------------------------------------	

STake_Start("SG_SL_ST_skill_034_restore","char_wait_000")   --净地
    BlendMode(0)
    BlendTime(0)
	
	LoopFx(0.2,12, 1, "SG_SL_ST_skill_034_loop", "", 1, 1)
    CampFx(0.2,"","","HG_QZ_SW_QXQJ_D_loop","Reference",1.2,1,0,0,0,0,0,0,1,3)	

STake_Start("SG_SL_ST_skill_036_npc","char_wait_000")   --净地封锁
    BlendMode(0)
    BlendTime(0)
	Sound(0.03,"HG_SL_ST_Skill_016")
	Sound(0.03,"HG_SL_SB_Skill_006")
	Sound(0.03,"HG_THD_SW_skill_001")
	
	Fx(0.01,"SG_SL_ST_skill_035_fire")	
	Fx(0.01,"SG_SL_ST_skill_036_start")	
	LoopFx(0.2,7,1, "SG_SL_ST_skill_036_loop", "", 1, 1)
	
STake_Start("NPC_SL_ST_skill_027+npc","char_wait_000")   --14468 新月轮
    BlendMode(0)
    BlendTime(0)
	Sound(0.05,"HG_SL_ST_Skill_012")
	
	Fx(0.1,"SG_SL_ST_skill_016_fire","",2,1,0,0,20,0,0,0,0.5,0)
	
	AttackStart()
    Hurt(0.05,"hurt_21")
	Hurt(0.15,"hurt_21")
	Hurt(0.25,"hurt_21")
	Hurt(0.35,"hurt_21")
	Hurt(0.45,"hurt_21")
	Hurt(0.55,"hurt_21")
	Hurt(0.65,"hurt_21")
	Hurt(0.75,"hurt_21")
	Hurt(0.85,"hurt_21")
	Hurt(0.95,"hurt_21")
	Hurt(1.05,"hurt_21")
	Hurt(1.15,"hurt_21")
	Hurt(1.25,"hurt_21")
	DirFx(0.05,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(0.15,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(0.25,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(0.35,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(0.45,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(0.55,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(0.65,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(0.75,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(0.85,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(0.95,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(1.05,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(1.15,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
	DirFx(1.25,"SG_SL_ST_skill_016_hit",28,1,1,-90,0)
    HurtSound(0.05,"ST_Hit_001")
	HurtSound(0.15,"ST_Hit_001")
	HurtSound(0.25,"ST_Hit_001")
	HurtSound(0.35,"ST_Hit_001")
	HurtSound(0.45,"ST_Hit_001")
	HurtSound(0.55,"ST_Hit_001")
	HurtSound(0.65,"ST_Hit_001")
	HurtSound(0.75,"ST_Hit_001")
	HurtSound(0.85,"ST_Hit_001")
	HurtSound(0.95,"ST_Hit_001")
	HurtSound(1.05,"ST_Hit_001")
	HurtSound(1.15,"ST_Hit_001")
	HurtSound(1.25,"ST_Hit_001")
	
	
	
	
STake_Start("NPC_SL_ST_skill_028npc","char_wait_006")   --如来神掌npc 14402
    BlendMode(0)
    BlendTime(0)
	Sound(0.1,"SL_RuLaiShenZhang_01")
	Sound(0.12,"SHS_daodi_000")
	Sound(3.01,"SL_RuLaiShenZhang_Xiaoshi")
	
	Fx(0.1,"SG_SL_ST_skill_028_zhang_3s","",1.3,1,0,30,0)
	Charge(0.01,0.25,30,0,4)
	
	AttackStart()
    Hurt(0,"hurt_62")
	HurtSound(0,"PA_Hit_002")
	Charge(0.02,0.1,35,0,8)
	DirFx(0,"SG_SL_ST_skill_028_hit",28,1,1,-90,0)		
	
	
STake_Start("NPC_SL_ST_skill_028npc_2","char_wait_006")   --如来神掌自我销毁 14401
    BlendMode(0)
    BlendTime(0)
	
	Fx(0.5,"SG_SL_ST_skill_028_end","",3)	
	
	
STake_Start("NPC_SL_ST_skill_028npc_3","char_wait_006")   --如来神掌拍地 14476
    BlendMode(0)
    BlendTime(0)
		
	AttackStart()
    Hurt(0,"hurt_71")
	HurtSound(0.05,"PA_Hit_002")
	DirFx(0,"SG_SL_ST_skill_028_hit",28,1,1,-90,0)		
	
	
STake_Start("NPC_SL_ST_skill_028npc_4","char_wait_006")   --如来神掌拍地表现Npc 14477
    BlendMode(0)
    BlendTime(0)
	Sound(0.05,"SL_RuLaiShenZhang_02")
	Sound(0.06,"SHS_EXP_000")
	
	Fx(0.1,"SG_SL_ST_skill_029",1.3)		
	
	
STake_Start("NPC_SL_ST_skill_028npc_5","char_wait_006")   --如来神掌画圈 14479
    BlendMode(0)
    BlendTime(0)
		
	
STake_Start("NPC_SL_ST_skill_028npc_6","char_wait_006")   --如来神掌画圈表现 14480
    BlendMode(0)
    BlendTime(0)
	Sound(0.05,"SL_RuLaiShenZhang_03")
	
	Fx(0.1,"SG_SL_ST_skill_030","",1.5)	
	Fx(0.1,"SG_SL_ST_skill_028_end","",1.5)	
	
	
STake_Start("NPC_SL_ST_skill_022_npc","char_wait_006")   --锁喉功  14447
    BlendMode(0)
    BlendTime(0)
		
	AttackStart()
    Hurt(0.13,"hurt_81",0,1)	
	DirFx(0.13,"HG_SL_ST_skill_018_hit",25,1) 
	HurtBoneFx(0.13,"FX_blood_quan_b","Spine1",1.7,0,0,0,0,0,0)	
	HurtSound(1.69,"SL_ST_Hit_1-3")			
	
	
STake_Start("SG_SL_ST_skill_026_Q_npc","char_wait_006")   --奈落	  14464
    BlendMode(0)
    BlendTime(0)
	
	LoopFx(0.01,2.5,1,"SG_SL_ST_skill_026_Q_loop")
	Fx(3,"SG_SL_ST_skill_026_Q_fire","")	

	
	AttackStart()
    Hurt(3,"hurt_21",0,1)
	DirFx(0.13,"HG_SL_ST_skill_018_hit",25,1)
	HurtSound(3,"SL_ST_Hit_1-3")
	Charge(0,0.2,5,0,21)

-------------------------------------------------------全真技能NPC--------------------------------------------------------------------

STake_Start("NPC_QZ_SW_skill_jz_aoe","char_wait_006")   --棒子爆炸技能NPC 13854
    BlendMode(0)
    BlendTime(0)

    Fx(0.03,"HG_QZ_SW_skill_014_start")
	Fx(0.53,"HG_QZ_SW_skill_014_fire","",0.65)

    Sound(0.03,"QZ_SW_Skill012_Start3")
    Sound(0.07,"QZ_SW_Skill013_Fire_1")
    Sound(0.02,"WGZ_Attack_H")

    AttackStart()
    Hurt(0.55,"hurt_62")
    HurtSound(0.55,"QZ_SW_Hit_12")
    AttackHurtFx(0.55,"HG_THD_SW_skill_Bing", "Spine1",2)

STake_Start("NPC_QZ_SW_attack_C_JA_001_new+","char_wait_000")   --大冰剑技能NPC	13027---13034
    BlendMode(0)
    BlendTime(0)
    AttackStart()
	Sound(0.02,"QZ_SW_QXQJ_001")
    Hurt(0.2,"hurt_21")
    HurtSound(0.2,"QZ_SW_Hit_12")
	Fx(0.5,"HG_QZ_SW_skill_013_hit01") 
	Fx(0.01,"HG_QZ_SW_skill_018_fire") 

STake_Start("NPC_QZ_SW_skill_six_spaceswords","char_wait_000")   --无极剑技能NPC  	13100-13019
    BlendMode(0)
    BlendTime(0)
    AttackStart()
	Sound(0.05,"QZ_SW_QXQJ_002")
	Sound(0.03,"QZ_SW_Jian")
    Hurt(0.2,"hurt_21")
    Hurt(0.22,"hurt_21")
    Hurt(0.24,"hurt_21")
    Hurt(0.26,"hurt_21")
    Hurt(0.28,"hurt_21")
    Hurt(0.30,"hurt_21")
    HurtSound(0.2,"QZ_SW_Hit_11")
    HurtSound(0.22,"QZ_SW_Hit_11")
    HurtSound(0.24,"QZ_SW_Hit_11")
    HurtSound(0.26,"QZ_SW_Hit_11")
    HurtSound(0.28,"QZ_SW_Hit_11")
    HurtSound(0.30,"QZ_SW_Hit_11")
  --Fx(0.5,"HG_QZ_SW_skill_013_hit02") 
	Fx(0.01,"HG_QZ_SW_skill_013_hit02","Reference",3,1,0,0,27) 
    Fx(0.2,"QZ_LswordB_attack_aoe","Reference",1,0,0,0,10)

STake_Start("NPC_QZ_SW_skill_004_new","char_wait_006")	---全真剑雨	13017
	BlendMode(0)
    BlendTime(0)
    FrameTime(0,10)
    Loop()

    Fx(0.01,"HG_QZ_SW_QXQJ_003_loop","Reference",1,0,0,0,0,0,0)
	Fx(2,"HG_QZ_SW_QXQJ_003_loop","Reference",1,0,0,0,0,0,0)
	Fx(4,"HG_QZ_SW_QXQJ_003_loop","Reference",1,0,0,0,0,0,0)
	Fx(6,"HG_QZ_SW_QXQJ_003_loop","Reference",1,0,0,0,0,0,0)
	Fx(7.9,"HG_QZ_SW_QXQJ_003_end","Reference",1,0,0,0,0,0,0,0)
    CampFx(0.01, "", "", "HG_QZ_SW_QXQJ_D","Reference",1,1,0,0,0,0,0,0,1)
    CampFx(2, "", "", "HG_QZ_SW_QXQJ_D_loop","Reference",1,1,0,0,0,0,0,0,1,3)

    AttackStart()
    Hurt(0,"")
    AttackHurtFx(0,"HG_QZ_SW_attack_hit","Spine1",0.8)
    Sound(0.500,"QZ_SW_QXQJ_JianYu")        

STake_Start("NPC_QZ_SW_skill_004_new_end","char_wait_002")	---全真剑雨end	13017
	BlendMode(0)
    BlendTime(0.05)
--	Fx(0,"HG_QZ_SW_QXQJ_003_end","Reference",1,0,0,0,0,0,0)


STake_Start("NPC_QZ_SW_skill_004_new_fire","char_wait_006")	---炽焰火海	13565
	BlendMode(0)
    BlendTime(0)
    FrameTime(0,10)
    Loop()

    Fx(0.01,"HG_QZ_SW_QXQJ_003_huo_loop","Reference",1,0,0,0,0,0,0)
	Fx(2,"HG_QZ_SW_QXQJ_003_huo_loop","Reference",1,0,0,0,0,0,0)
	Fx(4,"HG_QZ_SW_QXQJ_003_huo_loop","Reference",1,0,0,0,0,0,0)
	Fx(6,"HG_QZ_SW_QXQJ_003_huo_loop","Reference",1,0,0,0,0,0,0)
	Fx(7.9,"HG_QZ_SW_QXQJ_003_huo_end","Reference",1,0,0,0,0,0,0,0)  
    CampFx(0.01,"","","HG_QZ_SW_QXQJ_D","Reference",1,1,0,0,0,0,0,0,1)
    CampFx(2,"","","HG_QZ_SW_QXQJ_D_loop","Reference",1,1,0,0,0,0,0,0,1,3)
	
    AttackStart()
    Hurt(0.100,"")
    --AttackHurtFx(0.8,"HG_QZ_SW_skill_000_hit","Reference",1)
    Sound(0.500,"QZ_SW_QXQJ_JianYu")

STake_Start("NPC_QZ_SW_skill_004_new_fire_end","char_wait_002")	---炽焰火海end	13565
	BlendMode(0)
    BlendTime(0)
--  Fx(0,"HG_QZ_SW_QXQJ_003_huo_end","Reference",1,0,0,0,0,0,0)   


STake_Start("NPC_QZ_SW_skill_021_new","char_wait_002")	---荧惑守心技能NPC/摇光黑洞    13456
	BlendMode(0)
    BlendTime(0)
	Sound(0.05,"QZ_Skill_Space_01")
  
    AttackStart()
    Hurt(0.100,"")
    Hurt(0.100,"")
    Hurt(0.100,"")
    Hurt(0.100,"")
    Hurt(0.100,"")
    Hurt(0.100,"")
    HurtSound(0.100,"QZ_PA_Hit_12")
    Fx(0.01,"SG_QZ_SW_skill_024_001","Reference",1,0,0,0,28,0,0,0,0.5)
    Charge(0,0.1,5,0,21)

STake_Start("NPC_QZ_SW_skill_023_new","char_wait_002")	---参星破	技能NPC    13466
	BlendMode(0)
    BlendTime(0)
	Sound(0.05,"QZ_Skill_Space_02")

    AttackStart()
    Hurt(0.200,"")
    HurtSound(0.200,"BTS_CR_HIT_new_s1")
    Fx(0.01,"SG_QZ_SW_skill_024_002","Reference",1,0,0,0,28,0,0)
		AttackStart()
	Hurt(0.01,"hurt_81",0,1)
	DirFx(0.05,"FX_Violent_blood_Sharp_R1",28,1,1.5,-70,30,0) 
	HurtSound(0.1,"QZ_SW_Hit_11")

STake_Start("NPC_QZ_SW_skill_022_new","char_wait_002")	---商星灭	技能NPC    13476
	BlendMode(0)
    BlendTime(0)
	Sound(0.05,"QZ_Skill_Space_03")
  
    AttackStart()
    Hurt(0.500,"")
    Hurt(1.00,"")
    Hurt(1.500,"")
    Hurt(2.000,"")
    Hurt(2.500,"")
    Hurt(3.00,"")
    Hurt(3.500,"")
    Hurt(4.00,"")
    HurtSound(0.500,"PAhit11")
    HurtSound(1.00,"PAhit11")
    HurtSound(1.500,"PAhit11")
    HurtSound(2.000,"PAhit11")
    HurtSound(2.500,"PAhit11")
    HurtSound(3.00,"PAhit11")
    HurtSound(3.500,"PAhit11")
    HurtSound(4.00,"PAhit11")
    Fx(0.01,"SG_QZ_SW_skill_024_003_start","Reference",1,0,0,0,28,0,0)

    Fx(0.5,"SG_QZ_SW_skill_024_003_loop","Reference",1,0,0,0,28,0,0)
    Fx(1.5,"SG_QZ_SW_skill_024_003_loop","Reference",1,0,0,0,28,0,0)
    Fx(2.5,"SG_QZ_SW_skill_024_003_loop","Reference",1,0,0,0,28,0,0)
    Fx(3.5,"SG_QZ_SW_skill_024_003_loop","Reference",1,0,0,0,28,0,0)
	AttackStart()
	Hurt(0.01,"hurt_62",0,1)
	DirFx(0.05,"FX_Violent_blood_Sharp_R1",28,1,1.5,-70,30,0) 
	HurtSound(0.1,"QZ_SW_Hit_11")

	STake_Start("NPC_QZ_SW_skill_027_001","char_wait_002")	---空明阵法技能NPC    13621
	BlendMode(0)
    BlendTime(0)
	Sound(0.05,"HG_QZ_SW_skill_005")
	Sound(0.22,"QZ_SW_Skill_006_2")

    Fx(0.01,"SG_QZ_SW_skill_027_001_fire","Reference",1.4,0,0,0,0,0,0)
	LoopFx(1,4,1,"SG_QZ_SW_skill_027_001_loop","",1.4)
    CampFx(0.01,"","","HG_QZ_SW_QXQJ_D_loop","Reference",0.8,1,0,0,0,0,0,0,1,3)
	
    AttackStart()
    Hurt(0.01,"hurt_81")
--	Hurt(0.5,"hurt_21")
--	Hurt(1.0,"hurt_21")
--	Hurt(1.5,"hurt_21")
--	Hurt(2.0,"hurt_21")
--	Hurt(2.5,"hurt_21")
--	Hurt(3.0,"hurt_21")
--	Hurt(3.5,"hurt_21")
    HurtSound(0.100,"QZ_PA_Hit_12")

	
	STake_Start("QZ_Lsword_attack_000_02_new","char_wait_006")	---格挡反击    13205
	BlendMode(0)
    BlendTime(0)
	Sound(0.1,"QZ_SW_QXQJ_001")
	Sound(0.6,"QZ_SW_Skill_006_2")

    Fx(0.01,"SG_QZ_SW_gedangfanji_01","Reference",1,0,0,0,0,0,0)

	
    AttackStart()
    Hurt(0.6,"hurt_21")
--	Hurt(0.5,"hurt_21")
--	Hurt(1.0,"hurt_21")
--	Hurt(1.5,"hurt_21")
--	Hurt(2.0,"hurt_21")
--	Hurt(2.5,"hurt_21")
--	Hurt(3.0,"hurt_21")
--	Hurt(3.5,"hurt_21")
    HurtSound(0.600,"QZ_PA_Hit_12")
	
	
	STake_Start("NPC_QZ_SW_skill_030_npc","char_wait_002")	---寒流侵蚀技能Npc	13771
	BlendMode(0)
    BlendTime(0)
	Sound(0.03,"QZ_SW_Skill013_Start2")
	Sound(0.09,"QZ_SW_Skill013_Fire_2")
	Sound(1.03,"QZ_SW_Skill012_Start3")

	Fx(0.01,"SG_QZ_SW_skill_030","Reference",1,0,0,0,0,0,0)
 
    AttackStart()
    Hurt(0.35,"hurt_21")
	Hurt(0.9,"hurt_62")
    HurtSound(0.35,"BTS_FAN_Hit_002")
	HurtSound(0.9,"QZ_PA_Hit_12")

STake_Start("QZ_dreaming_swords_lv1","char_wait_002")   --天外飞剑1剑 13185 
    BlendMode(0)
    BlendTime(0)
    AttackStart()
	Sound(0.03,"QZ_SW_Skill_000_2")
    Hurt(0.1,"hurt_21")
    HurtSound(0.1,"QZ_SW_Hit_12")
	Fx(0.1,"HG_QZ_SW_skill_018_fire","Spine1",0.5) 

STake_Start("QZ_dreaming_swords_lv2","char_wait_002")   --天外飞剑2剑 13186 
    BlendMode(0)
    BlendTime(0)
    AttackStart()
	Sound(0.03,"QZ_SW_Skill_000_2")
	Sound(0.13,"QZ_SW_QXQJ_001")

    Hurt(0.1,"hurt_21")
    Hurt(0.3,"hurt_21")
    HurtSound(0.1,"QZ_SW_Hit_12")
    HurtSound(0.3,"QZ_SW_Hit_12")

	Fx(0.1,"HG_QZ_SW_skill_018_fire","Reference",0.5) 
	Fx(0.3,"HG_QZ_SW_skill_018_fire","Reference",0.5) 

STake_Start("QZ_dreaming_swords_lv3","char_wait_002")   --天外飞剑3剑 13187 
    BlendMode(0)
    BlendTime(0)
    AttackStart()
	Sound(0.03,"QZ_SW_Skill_000_2")
	Sound(0.1,"QZ_SW_QXQJ_002")
	Sound(0.27,"QZ_SW_QXQJ_001")
    Hurt(0.1,"hurt_21")
    Hurt(0.3,"hurt_21")
    Hurt(0.5,"hurt_21")
    HurtSound(0.1,"QZ_SW_Hit_12")
    HurtSound(0.3,"QZ_SW_Hit_12")
    HurtSound(0.5,"QZ_SW_Hit_12")

	Fx(0.1,"HG_QZ_SW_skill_018_fire","Reference",0.5) 
	Fx(0.3,"HG_QZ_SW_skill_018_fire","Reference",0.5) 
	Fx(0.5,"HG_QZ_SW_skill_018_fire","Reference",0.5) 

STake_Start("QZ_dreaming_swords_lv4","char_wait_002")   --天外飞剑4剑 13188
    BlendMode(0)
    BlendTime(0)
	Sound(0.03,"QZ_SW_Skill_000_2")
	Sound(0.1,"QZ_SW_QXQJ_002")
	Sound(0.3,"QZ_SW_Attack_12")
	Sound(0.5,"QZ_SW_QXQJ_001")
	
	Fx(0.1,"HG_QZ_SW_skill_018_fire","Reference",0.5) 
	Fx(0.3,"HG_QZ_SW_skill_018_fire","Reference",0.5) 
	Fx(0.5,"HG_QZ_SW_skill_018_fire","Reference",0.5) 
	Fx(0.7,"HG_QZ_SW_skill_018_fire","Reference",0.5) 
	
    AttackStart()
    Hurt(0.1,"hurt_21")
    Hurt(0.3,"hurt_21")
    Hurt(0.5,"hurt_21")
    Hurt(0.7,"hurt_21")
    HurtSound(0.1,"QZ_SW_Hit_12")
    HurtSound(0.3,"QZ_SW_Hit_12")
    HurtSound(0.5,"QZ_SW_Hit_12")
    HurtSound(0.7,"QZ_SW_Hit_12")	

STake_Start("NPC_QZ_SW_fj","char_wait_000")      --全真反击
    BlendMode(0)
    BlendTime(0.0)
	Sound(0.03,"QZ_SW_QXQJ_002")

    AttackStart()
    Hurt(0.1,"hurt_21")
	DirFx(0.1,"NB_MZWM_skill_hit",0,1) 

	Fx(0,"SG_QZ_SW_fj_wm",1,1,0,0,0,0,0,0,1,1)		

STake_Start("NPC_QZ_SW_skill_aether_miji","char_wait_006")      --全真次元盾秘籍   13512
    BlendMode(0)
    BlendTime(0.0)

    AttackStart()
    Hurt(0.001,"hurt_21")
	Hurt(0.045,"hurt_21")
	Hurt(0.090,"hurt_21")
	Hurt(0.135,"hurt_21")
	DirFx(0.001,"SG_QA_SW_attack_B_JZ_hit",0,1) 
	DirFx(0.045,"SG_QA_SW_attack_B_JZ_hit",0,1) 
	DirFx(0.090,"SG_QA_SW_attack_B_JZ_hit",0,1) 
	DirFx(0.135,"SG_QA_SW_attack_B_JZ_hit",0,1) 
	HurtBoneFx(0,"FX_blood_jian_b","Spine1",1,0,0,0,0,0,0)

STake_Start("NPC_QZ_SW_skill_dead","char_wait_006")      --全真死亡爆
    BlendMode(0)
    BlendTime(0.0)
	Sound(0.03,"QZ_Skill_Space_02")
	Sound(0.03,"QZ_SW_Skill012_Start2")

	Fx(0.01,"SG_QZ_SW_skill_dead",1,1,0,0,0,0,0,0,0.25,1)

STake_Start("NPC_QZ_SW_skill_heal","char_wait_006")      --全真治疗
    BlendMode(0)
    BlendTime(0.0)
    FrameTime(0,2)
    Loop()
    Sound(0.500,"QZ_SW_QXQJ_JianYu")        
    Sound(0.500,"QZ_SW_Skill_004_Loop")

    Fx(0.01,"HG_QZ_SW_QXQJ_003_HP_loop","Reference",1,0,0,0,0,0,0)
	Fx(2,"HG_QZ_SW_QXQJ_003_HP_loop","Reference",1,0,0,0,0,0,0)
	
	
	
	
------------------------------------------------  长白山  ------------------------------------------------------------------	

	
STake_Start("N_JBBSL_skill_001_fire1","char_wait_000")    --26807孢子爆炸
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
   
    Fx(0.01,"N_JBBSL_skill_001_hit","Reference",1,0,0,0,25)

    AttackStart()
    Hurt(0,"hurt_21")
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1.2,0,0,0,0,0,0)
    HurtSound(0.05,"QZ_PA_Hit_12")

STake_Start("NB_XLJ_up_attack_003_fire","char_wait_000")  --玄鳞蛟普攻4   26624
    BlendMode(0)
    BlendTime(0.2)

    Fx(0.01,"NB_XLJ_up_skill_001_hit","Reference")

    AttackStart()
    Hurt(0,"hurt_21")
	HurtBoneFx(0,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)
    --DirFx(0.01,"NB_XLJ_up_skill_001_hit",0,1) 
	RangeCameraShake(0.001,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.9","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2")

    HurtSound(0.05,"BTS_CR_HIT_new_2_1")
	

	
STake_Start("NPC_HSJ_up_attack_003_fire","char_wait_000")  --蛟龙
    BlendMode(0)
    BlendTime(0.2)

    Fx(0.01,"NB_HSJ_up_skill_001_hit","Reference")

    AttackStart()
    Hurt(0,"hurt_21")
	HurtBoneFx(0,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)
    --DirFx(0.01,"NB_XLJ_up_skill_001_hit",0,1) 
	RangeCameraShake(0.001,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.9","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2")

    HurtSound(0.05,"BTS_CR_HIT_new_2_1")	
	
STake_Start("NPC_HSJ_up_attack_0021","char_wait_000")  --蛟龙水柱 
    BlendMode(0)
    BlendTime(0.2)

    Fx(0.01,"NB_HSJ_up_skill_shuizhu","Reference")

    AttackStart()
    Hurt(0,"hurt_81")
	HurtBoneFx(0,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)
    --DirFx(0.01,"NB_XLJ_up_skill_001_hit",0,1) 
	RangeCameraShake(0.001,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.9","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2")

    HurtSound(0.05,"BTS_CR_HIT_new_2_1")	

STake_Start("NB_XLJ_up_skill_002_fire","char_wait_000")  --咬住   26630
    BlendMode(0)
    BlendTime(0.2)

    Charge(0.1,1.5,5,0,3)
    Fx(0.015,"NB_XLJ_up_attack_start","Reference",0.5,0,0,0,0)
    Fx(0.7,"NB_XLJ_up_attack_start","Reference",0.5,0,0,0,0)

    HurtSound(0.05,"HG_THD_skillhit_000")
    HurtBoneFx(0,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)
	
STake_Start("NB_XLJ_up_skill_001_fire","char_wait_000")  --全屏消化液NPC   26626
    BlendMode(0)
    BlendTime(0.2)

    Fx(0.01,"NB_XLJ_up_skill_001_hit")
    AttackStart()
    Hurt(0,"hurt_21")
    --DirFx(0.01,"NB_XLJ_up_skill_001_hit",0,1) 
	RangeCameraShake(0.001,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.9","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2")

    HurtSound(0.05,"QZ_PA_Hit_12")		

STake_Start("NB_WGZ_skill_002_fire1","char_wait_000")   --无根子 连斩2   26832/26834
    BlendMode(0)
    BlendTime(0.2)

    AttackStart()
    Hurt(0.01,"hurt_61")
    Hurt(0.1,"hurt_61")
    HurtSound(0.01,"WGZ_Hit_L") 
    HurtSound(0.1,"WGZ_Hit_L") 
    DirFx(0.01,"NB_WGZ_skill_hit",25,1,1,-60,0)
    DirFx(0.1,"NB_WGZ_skill_hit",25,1,1,60,0)

STake_Start("NB_WGZ_skill_003_fire2","char_wait_000")    --无根子 升龙斩2   26836
    BlendMode(0)
    BlendTime(0.2)

    AttackStart()
    Hurt(0.01,"hurt_61")
    HurtSound(0.01,"WGZ_Hit_L") 
    DirFx(0.01,"NB_WGZ_skill_hit",25,1,1,-60,0)

STake_Start("NB_WGZ_skill_005_fire","char_wait_000")    --无根子 跳跃攻击   26839/26840
    BlendMode(0)
    BlendTime(0.2)    
    AttackStart()
    Hurt(0.01,"hurt_81")
    HurtSound(0.01,"WGZ_Hit_L") 
    DirFx(0.01,"NB_WGZ_skill_hit",25,1,1,-60,0)
	HurtBoneFx(0.01,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)

STake_Start("NB_WGZ_skill_009_fire","char_wait_000")     --天寒刀气   26848
    BlendMode(0)
    BlendTime(0.2)

    AttackStart()
    Hurt(0.01,"hurt_61")
    HurtSound(0.01,"WGZ_Hit_L") 
    DirFx(0.01,"NB_WGZ_skill_hit",25,1,1,-60,0)
    HurtBoneFx(0.01,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)

STake_Start("NB_YR_skill_001_fire","char_wait_000")     --药人撕咬NPC   26803
    BlendMode(0)
    BlendTime(0.2)

    Charge(0,0.3,15,0,3)

	
	
STake_Start("NB_LZW_skill_008_fire","char_wait_000")      --灵狐拳法B   26427
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 

    
    AttackStart()
    Hurt(0.01,"hurt_81")
    DirFx(0.01,"NB_LZW_attack_hit",0,1)  
	HurtBoneFx(0.01,"FX_blood_quan_b","Spine1",1.2,0,0,0,0,0,0)
    HurtSound(0.01,"BTS_FAN_Hit_003")
	
STake_Start("NB_XMS_skill_0041","char_wait_000")     --西门霜 普攻4毒雾   26514
    BlendMode(0)
    BlendTime(0.2)
	Loop()
	LoopFx(0.0, 30.1, 0, "Fx_common_rangemark_r","Reference",.4)	
	Fx(0,"NB_XMS_skill_004_loop","Reference")

	AttackStart()
    Hurt(0.01,"hurt_21")
	DirFx(0.01,"NB_XMS_skill_005_hit",25,1,2)		
    HurtBoneFx(0.01,"FX_blood_quan_b","Spine1",1.2,0,0,0,0,0,0)
    HurtSound(0.01,"QZ_PA_Hit_12")

STake_Start("NB_XMS_skill_008_message_start","char_wait_000")     --信息素范围   26507
    BlendMode(0)
    BlendTime(0.2)	
	Loop()
	--LoopFx(0.0, 30.1, 0, "Fx_common_rangemark_r","Reference",.25)

STake_Start("NB_XMS_skill_008_message_fire","")
    BlendMode(0)
    BlendTime(0.2)
	 
	AttackStart()
    Hurt(0,"hurt_21")	 
	
	
STake_Start("NB_XLJ_down_skill_005_fire","char_wait_000")  -- 玄灵蛟 尾刺26634  吟唱
    BlendMode(0)
    BlendTime(0.2)	
	Fx(0,"NB_XLJ_down_skill_005_fire","Reference")	
	
  	AttackStart()
  	Hurt(0,"hurt_81")    
    DirFx(0,"NB_WGZ_skill_hit",25,1,0.4,-60,0)	
	HurtBoneFx(0,"FX_blood_changqiang_c","Spine1",1,0,0,0,0,-90,0)
    HurtSound(0.05,"PA_Hit_004")		
	
	
STake_Start("Common_HardBossSkill_002_Npc","char_wait_000")  --技能 26636 Hard难度AOE技能NPC  引导一0.4
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,1)
    Fx(0.4,"NB_XLJ_up_skill_001_A_hit","Reference",1.3,0,0,0,0,0,0,0,1)
	Sound(0.00,"")

  	AttackStart()
  	Hurt(0.4,"hurt_21")    
    DirFx(0.4,"NB_XLJ_up_skill_001_hit",0,1)	
    HurtSound(0.4,"")	

	
STake_Start("NB_XSSL_skill_003_fire","char_wait_006")    --26863 雪山首领投掷冰块
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
   
    Fx(0.01,"NB_XSSL_skill_003_hit","Reference")

    AttackStart()
    Hurt(0,"hurt_21")
    HurtBoneFx(0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"")	
    DirFx(0,"HG_THD_SW_skill_Bing",25,1,0.6,0,0)		
	
STake_Start("NB_XSSL_skill_008_fire","char_wait_006")    --26871 雪山首领冰锥2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
   
    Fx(1.6,"NB_XSSL_skill_006_bingzhui","Reference",0.4,1,0,0,0,0,0,0,1)

    AttackStart()
    Hurt(0,"hurt_21")
    HurtBoneFx(0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"")	
    DirFx(0,"HG_THD_SW_skill_Bing",25,1,0.6,0,0)	
	
STake_Start("NB_XSSL_skill_008_fire2","char_wait_006")    -- 26878 场地冰锥
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
   
    Fx(0.01,"NB_XSSL_skill_006_bingzhui_001","Reference",0.2,1,0,0,0,0,0,0,1)

    AttackStart()
    Hurt(0.5,"hurt_21")
    HurtBoneFx(0.5,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.5,"")	
    DirFx(0.5,"HG_THD_SW_skill_Bing",25,1,0.6,0,0)	
	
STake_Start("NB_XSSL_skill_008_fire3","char_wait_006")    -- 镜头震动
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  	
	
	RangeCameraShake(0.01,"ShakeDistance = 600","ShakeMode = 3","ShakeTimes = 5","ShakeFrequency = 0.08","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 6")
	
	
STake_Start("NPC_char_wait_006_1","char_wait_006")  -- 雪山风雪
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
   
    Fx(0.01,"NB_XSSL_skill_aoe")	
	

STake_Start("NB_XR_BingZhuXueGuai1_skill_004_NPC","char_wait_006")    --26887  投掷冰块技能npc  引导0.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
   
    Fx(0.2,"NB_XSSL_skill_003_hit","Reference",1.8,0,0,0,0,0,0,0,1,1)

    AttackStart()
    Hurt(0.2,"hurt_21")
    HurtBoneFx(0.2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.2,"")	
    DirFx(0.2,"HG_THD_SW_skill_Bing",25,1,0.6,0,0)		
	
	
------------------------------------------------  蘑菇  ------------------------------------------------------------------



STake_Start("NPC_LZW_mogu_hong","char_wait_006")    --红蘑菇
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")  
    Loop(3,5)
	Sound(0.03,"LZW_Mogu_Up")		
	
    Fx(0.01,"NB_LZW_mogu_start_hong","Reference",1,0,0,0,0,0,0,0,0.33)		
    Fx(3,"NB_LZW_mogu_doudong_hong","Reference",1,0,0,0,0,0,0,0,1,3)		
	
	

STake_Start("NPC_LZW_mogu_zi","char_wait_006")    --紫蘑菇
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")  
    Loop(3,5)
	Sound(0.03,"LZW_Mogu_Up")		
	
    Fx(0.01,"NB_LZW_mogu_start_zi","Reference",1,0,0,0,0,0,0,0,0.33)		
    Fx(3,"NB_LZW_mogu_doudong_zi","Reference",1,0,0,0,0,0,0,0,1,3)		

STake_Start("NPC_LZW_mogu_lv","char_wait_006")    --绿蘑菇
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")  
    Loop(3,5)
	Sound(0.03,"LZW_Mogu_Up")		
	
	Fx(0.01,"NB_LZW_mogu_start_lv","Reference",1,0,0,0,0,0,0,0,0.33)		
    Fx(3,"NB_LZW_mogu_doudong_lv","Reference",1,0,0,0,0,0,0,0,1,3)	

STake_Start("NPC_LZW_mogu_lan","char_wait_006")    --蓝蘑菇
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")  	
    Loop(3,5)
	Sound(0.03,"LZW_Mogu_Up")		
	
    Fx(0.01,"NB_LZW_mogu_start_lan","Reference",1,0,0,0,0,0,0,0,0.33)		
    Fx(3,"NB_LZW_mogu_doudong_lan","Reference",1,0,0,0,0,0,0,0,1,3)	



STake_Start("NPC_LZW_mogu_hong_fire","char_wait_006")    --红蘑菇  fire      26440
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")  
	
    Fx(0.01,"NB_LZW_mogu_fire_hong","Reference",1.5,0,0,0,0,0,0,0,1)			
	Sound(0.03,"LZW_Mogu_Hong")		

    AttackStart()
    Hurt(0.01,"hurt_81")
    HurtBoneFx(0.01,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.01,"QZ_PA_Hit_12")	
    DirFx(0.01,"Bear_skill_000_hit",25,1,1,0,0)

STake_Start("NPC_LZW_mogu_zi_fire","char_wait_006")    --紫蘑菇  fire   26438
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")  

    Fx(0.01,"NB_LZW_mogu_fire_zi","Reference",1,0,0,0,0,0,0,0,1)			
	Sound(0.03,"LZW_Mogu_Zi")	
	

    AttackStart()
    Hurt(0.01,"hurt_21")
    HurtBoneFx(0.01,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.01,"QZ_PA_Hit_11")	

	
STake_Start("NPC_LZW_mogu_lv_fire","char_wait_006")    --绿蘑菇  fire    26436
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")  

	Fx(0.01,"NB_LZW_mogu_fire_lv","Reference",1,0,0,0,0,0,0,0,1)			
	Sound(0.03,"LZW_Mogu_Lv")


    AttackStart()
    Hurt(0.01,"hurt_21")
    HurtBoneFx(0.01,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.01,"QZ_PA_Hit_11")	

	
STake_Start("NPC_LZW_mogu_lan_fire","char_wait_006")    --蓝蘑菇  fire   26444
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")  	

    Fx(0.01,"NB_LZW_mogu_fire_lan","Reference",1,0,0,0,0,0,0,0,1)		
	Sound(0.03,"LZW_Mogu_Lan")


    AttackStart()
    Hurt(0.01,"hurt_21")
    HurtBoneFx(0.01,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.01,"QZ_PA_Hit_12")	


STake_Start("NPC_LZW_mogu_BUF_fire_zi","char_wait_006")    --玩家蘑菇  fire    26442
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")  	
    Loop(0,2.01)
    Fx(0.01,"NB_LZW_mogu_BUF_fire_zi","Reference",1,0,0,0,0,0,0,0,1)	
	Sound(0.03,"FX_Dead_Dark1")


    AttackStart()
    Hurt(0.01,"hurt_21")
    HurtBoneFx(0.01,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.01,"QZ_PA_Hit_11")


STake_Start("NPC_LZW_mogu_zhiliao","char_wait_006")    --绿蘑菇治疗
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Loop()	
	Fx(0.01,"HG_DL_BR_skill_007_qiu_HpUp_fire","Reference",0.6,0,0,0,0,0,0,0,1,3)	

------------------------------------------------  赵王府  ------------------------------------------------------------------	

STake_Start("NPC_STT_skill_007","char_wait_006")    --漩涡
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,6)
    Fx(0.01,"NB_STT_skill_007_loop","Reference",1,0,0,0,0,0,0,0,1,3)	
	
STake_Start("NPC_STT_skill_003_fire","char_wait_006")    --水牢fire
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.01,"NB_STT_skill_002_shuilao_end","Reference",1,0,0,0,0,0,0,0,1)	

	AttackStart()
	Hurt(0,"hurt_21")

STake_Start("NPC_STT_skill_004_big","char_wait_006")    --大水柱    38108
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.01,"NB_STT_skill_004_shuizhu_ground","Reference",1,0,0,0,0,0,0,0,1)
	Fx(1,"NB_STT_skill_004_shuizhu_1","Reference",1,0,0,0,0,0,0,0,1)	
	AttackStart()
	Hurt(1,"hurt_81")



STake_Start("NPC_STT_skill_004_small","char_wait_006")    --小水柱     38110
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.01,"NB_STT_skill_004_shuizhu_S_ground","Reference",1,0,0,0,0,0,0,0,1)
	Fx(1,"NB_STT_skill_004_shuizhu_S_1","Reference",1,0,0,0,0,0,0,0,1)
	AttackStart()
	Hurt(1,"hurt_81")


STake_Start("NPC_STT_skill_006","char_wait_006")    --黑水    38115
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Loop()
	Fx(0.01,"NB_STT_skill_006_loop","Reference",1.27,0,0,0,0,0,0,0,1,3)
	Fx(0.01,"NB_STT_skill_006_loop","Reference",1.27,0,0,0,5,30,0,0,1,3)
	
	AttackStart()
	Hurt(0,"hurt_21")


STake_Start("NPC_HTH_skill_006","char_wait_006")    --侯通海擒拿   38211
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
    Fx(0.01,"SG_QZ_SW_skill_024_001","Reference",1,0,0,0,20,0,0,0,2.5)
    Charge(0,0.1,5,0,21,0,0)  --1.1
	
	AttackStart()
	Hurt(1.2,"hurt_21")
 	HurtBoneFx(1.2,"FX_blood_siyao_c","Spine1",1,0,0,0,90,0,0)	


STake_Start("NPC_HTH_skill_009","char_wait_006")    --侯通海索命叉   38218
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
    Fx(0.45,"NB_HTH_skill_009_ground","Reference",1,0,0,0,0,0,0,0,1)	
	
	AttackStart()
	Hurt(0.65,"hurt_71")
 	HurtBoneFx(0.65,"FX_blood_siyao_c","Spine1",1,0,0,0,90,0,0)	


STake_Start("NPC_TZD_skill_012","char_wait_006")    --火旋风    37321
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	
	AttackStart()
	Hurt(0,"hurt_61")
 	HurtBoneFx(0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)	
	DirFx(0,"N_DMB_XSS_skill_005_hit",0,1)
	
	
------------------------------------------------  塞外  ------------------------------------------------------------------

STake_Start("QQJ_skill_003_fire","")  --跳起大压杀fire
    BlendMode(0)
    BlendTime(0.2) 

    Priority(0,"5")	

  	
    AttackStart()
  	Hurt(0.01,"hurt_81")
 
    DirFx(0.01,"L1_Unarmed_skill_hit",0,1) 

    HurtSound(0.01,"BTS_CR_HIT_new_2_1")

STake_Start("QQJ_skill_003_fire1","")  --跳起大压杀fire
    BlendMode(0)
    BlendTime(0.2) 

    Priority(0,"5")	

  	
    AttackStart()
  	Hurt(0.01,"hurt_21")
 
    DirFx(0.01,"L1_Unarmed_skill_hit",0,1) 

    HurtSound(0.01,"BTS_CR_HIT_new_2_1")

STake_Start("NB_MZWM_skill_001_fire1","char_wait_006")      --马贼王冲锋   27025
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 

    
    AttackStart()
    Hurt(0.01,"hurt_61")
    DirFx(0.01,"NB_MZWM_skill_hit",0,1)  
	
STake_Start("N_T1_JGQC_skill_006_fire1","")  --单发火炮fire   27083
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")     
  	
    AttackStart()
  	Hurt(0,"hurt_61")
    HurtBoneFx(0,"FX_blood_quan_b","Spine1",1.2,0,0,0,0,0,0)
	
	
STake_Start("T1_JGQC_skill_002_start1","char_wait_006")      --朝天发射   27087
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    Fx(0.01,"Fx_common_rangemark_r","Reference",0.3)	
    Fx(0.5,"Fx_common_rangemark_r","Reference",0.3)	
    Fx(1,"Fx_common_rangemark_r","Reference",0.3)
    Fx(1.5,"Fx_common_rangemark_r","Reference",0.3)
    Fx(1.8,"N_T1_JGQC_skill_002_hit","Reference",2)	
	
STake_Start("T1_JGQC_skill_002_fire1","char_wait_006")     
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    AttackStart()
    Hurt(0,"hurt_81")
	HurtBoneFx(0,"FX_blood_gongjian_c","Spine1",1.2,0,0,0,0,0,0)
    --Fx(0,"N_T1_JGQC_skill_002_hit","Reference",2)
	RangeCameraShake(0.001,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.9","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2.5")


	
STake_Start("N2_whip_skill_000_fire1","char_wait_006")    --渔网NPC27021
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
   
    AttackStart()
    Hurt(0.2,"hurt_21")	
	


	
STake_Start("Spider_skill_002_npc","char_wait_006")    --吐丝技能NPC 33303
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.3,"N_Spider_DuYaLangZhu_wang_ground","Reference",1,0,0,0,0,0,0,0,1,1)
    LoopFx(0.3,3,0,"N_Spider_DuYaLangZhu_wang_groundloop","Reference",1)


    AttackStart()
    Hurt(0.3,"hurt_21")
    DirFx(0.3,"N2_Crossbow_skill_000_hit",0,1)	
	

STake_Start("N1_MZGS_skill_003_npc","char_wait_006")  --技能  26045  跳跃射击NPC
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(1,"N1_MZGS_skill_003_bao","Reference")

  	AttackStart()
    Hurt(1,"hurt_21")
	DirFx(1,"N_N1_XLGB01_skill_002_hit",0,1)
    HurtSound(1.6,"Big_blade_Hit_002_1")
    HurtBoneFx(1,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)


STake_Start("Mouse_skill_001_NPC","char_wait_006")  --老鼠三方向风刃NPC  27237  引导0.1  动作总长2.666
    BlendMode(0)
    BlendTime(0.1)
    Priority(0,"5")

    AttackStart()
  	Hurt(0.1,"hurt_21")
    DirFx(0.1,"NB_JSSW_skill_002_hit",0,1,0.3)
    HurtSound(0.1,"Scorpion_XZW01_Hit_000")	
    HurtBoneFx(0.1,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)	
	
	
STake_Start("N_HYHY02_skill_002_MCF01_NPC","char_wait_006")    --鬼影落雷技能NPC  27283
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.1,"NB_YTGZT_skill_003_1","Reference")

  	AttackStart()
    Hurt(0.2,"hurt_21")
	DirFx(0.2,"",0,1)
    HurtBoneFx(0.2,"FX_blood_siyao_c","Spine1",1.2,0,0,0,0,0,0)
	
STake_Start("N_JGS_GCZC_skill_001_NPC","char_wait_006")    --燃烧弹  26893
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Loop(1.3,2.2)	
    Fx(1,"N_JGS_GCZC_skill_001_hit","Reference",1,0,0,0,0,0,0,0,1)	
    Fx(1.3,"N_JGS_GCZC_skill_001_hit_loop","Reference",1,0,0,0,0,0,0,0,1,3)	
	--LoopFx(1.3, 99999, 0, "N_JGS_GCZC_skill_001_hit_loop", "Reference", 1)
	Sound(0.4,"JGS_GCZC_NPCSkill_004_A")
	Sound(1.2,"JGS_GCZC_NPCSkill_004_B")

  	AttackStart()
    Hurt(0,"hurt_21")
	DirFx(0,"",0,1)	
	

STake_Start("N_JGS_GCZC_skill_004_NPC","char_wait_006")    --主炮发射 26901
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Sound(2.09,"JGS_GCZC_NPCSkill_004_A")
	Sound(2.9,"JGS_GCZC_NPCSkill_004_B")
	
    Fx(8.7,"N_JGS_GCZC_skill_004_hit","Reference",2.8,0,0,0,0,0,0,0,1)

  	AttackStart()
    Hurt(9,"hurt_81")
	DirFx(9,"",0,1)
	RangeCameraShake(9,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.9","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2.5")

STake_Start("N_JGS_GCZC_skill_003_left","char_wait_006")    --战车喷射火焰左  26898
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Loop(0,1)
    Fx(0,"N_JGS_GCZC_skill_003_2","Reference",1.3,0,0,0,0,0,0,0,1,3)	

  	AttackStart()
    Hurt(0,"hurt_21")
	DirFx(0,"",0,1)

	
STake_Start("N_JGS_GCZC_skill_003_right","char_wait_006")    --战车喷射火焰右  26899
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Loop(0,1)
    Fx(0,"N_JGS_GCZC_skill_003_1","Reference",1.3,0,0,0,0,0,0,0,1,3)	

  	AttackStart()
    Hurt(0,"hurt_21")
	DirFx(0,"",0,1)
	
------------------------------------------------------狼山-------------------------------------------------------

STake_Start("N1_Shaman_B_skill_001_fire1","char_wait_006")  --萨满伤害毒   27103
    BlendMode(0)
    BlendTime(0.2)
 
	Fx(0.01,"N1_Shaman_B_skill_000_aoe_purple","Reference")
 
    AttackStart()
  	Hurt(0.01,"hurt_21")
	DirFx(0.01,"FX_buff_poisoning",0,1) 

    HurtSound(0.05,"QZ_PA_Hit_12")


STake_Start("NB_Shaman_skill_001_fire1","char_wait_006")  --萨满精英麻痹毒素   27111
    BlendMode(0)
    BlendTime(0.2)
 
	Fx(0.01,"N1_Shaman_B_skill_000_aoe_yellow","Reference")
 
    AttackStart()
  	Hurt(0.01,"hurt_21")
    HurtSound(0.01,"PC_Gong_hit") 

STake_Start("N1_Shaman_skill_002_fire1","char_wait_006")  --萨满H伤害毒   27107
    BlendMode(0)
    BlendTime(0.2) 

	Fx(0.01,"N1_Shaman_B_skill_000_aoe_purple","Reference")

    AttackStart()	
  	Hurt(0.01,"hurt_21")
    HurtSound(0.01,"PC_Gong_hit")  
	DirFx(0.01,"FX_buff_poisoning",0,1)  
	

STake_Start("NB_Shaman_skill_001_fire2","char_wait_006")  --萨满精英H麻痹毒素   27117
    BlendMode(0)
    BlendTime(0.2) 

	Fx(0.01,"N1_Shaman_B_skill_000_aoe_yellow","Reference")

    AttackStart()	
  	Hurt(0.01,"hurt_21")
    HurtSound(0.01,"PC_Gong_hit")  
	DirFx(0.01,"FX_buff_poisoning",0,1)  	
	
	
STake_Start("NB_TY_skill_0062","char_wait_006")  --超级大旋风   27240
    BlendMode(0)
    BlendTime(0.2) 

    Fx(0,"SFX_LJXS_wind_att")	
	
    AttackStart()	
  	Hurt(0,"hurt_61")

    HurtSound(0.05,"BTS_CR_HIT_new_2_1")

	

---------------------------------------------邪尸3陨石3难度-------------------------------------------------------------------
-------------------------------------------------第一颗-----------------------------------------------------------------------
STake_Start("NB_XieShi_skill_003_NPCa","char_wait_006")    --技能  27406  砸地技能NPC  引导1.6  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(1.6,"NB_XieShi_skill_003_fire_a","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(1.23,"TY_Attack_006")
	Sound(1.63,"SZ03_skill_000_baozha")

	AttackStart()
	Hurt(1.8,"hurt_61")
	DirFx(1.8,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(1.8,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.8,"ShakeDistance = 200","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

STake_Start("NB_XieShi_skill_003_NPCa_level1","char_wait_006")    --技能  27406  砸地技能NPC  引导1.2  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(1.2,"NB_XieShi_skill_003_fire_a","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(0.83,"TY_Attack_006")
	Sound(1.183,"SZ03_skill_000_baozha")

	AttackStart()
	Hurt(1.4,"hurt_61")
	DirFx(1.4,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(1.4,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.4,"ShakeDistance = 200","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
	
STake_Start("NB_XieShi_skill_003_NPCa_level2","char_wait_006")    --技能  27406  砸地技能NPC  引导0.8  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(0.8,"NB_XieShi_skill_003_fire_a","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(0.53,"TY_Attack_006")
	Sound(0.783,"SZ03_skill_000_baozha")

	AttackStart()
	Hurt(1.0,"hurt_61")
	DirFx(1.0,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(1.0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.0,"ShakeDistance = 200","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
-------------------------------------------------第二颗-----------------------------------------------------------------------

STake_Start("NB_XieShi_skill_003_NPCb","char_wait_006")    --技能  27407  砸地技能NPC  引导1.6  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(1.6,"NB_XieShi_skill_003_fire_b","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(1.23,"TY_Attack_006")
	Sound(1.63,"SZ03_skill_000_baozha")

	AttackStart()
	Hurt(1.8,"hurt_61")
	DirFx(1.8,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(1.8,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.8,"ShakeDistance = 200","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

STake_Start("NB_XieShi_skill_003_NPCb_level1","char_wait_006")    --技能  27407  砸地技能NPC  引导1.2  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(1.6,"NB_XieShi_skill_003_fire_b","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(1.23,"TY_Attack_006")
	Sound(1.63,"SZ03_skill_000_baozha")

	AttackStart()
	Hurt(1.4,"hurt_61")
	DirFx(1.4,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(1.4,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.4,"ShakeDistance = 200","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

STake_Start("NB_XieShi_skill_003_NPCb_level2","char_wait_006")    --技能  27407  砸地技能NPC  引导0.8  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(1.6,"NB_XieShi_skill_003_fire_b","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(1.23,"TY_Attack_006")
	Sound(1.63,"SZ03_skill_000_baozha")

	AttackStart()
	Hurt(1.0,"hurt_61")
	DirFx(1.0,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(1.0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.0,"ShakeDistance = 200","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
-------------------------------------------------第三颗-----------------------------------------------------------------------
STake_Start("NB_XieShi_skill_003_NPCc","char_wait_006")    --技能  27408  砸地技能NPC  引导0.9  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(1.6,"NB_XieShi_skill_003_fire_c","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(1.23,"TY_Attack_006")
	Sound(1.63,"SZ03_skill_000_baozha")

	AttackStart()
	Hurt(1.7,"hurt_61")
	DirFx(1.7,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(1.7,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.7,"ShakeDistance = 200","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

STake_Start("NB_XieShi_skill_003_NPCc_level1","char_wait_006")    --技能  27408  砸地技能NPC  引导0.9  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(1.6,"NB_XieShi_skill_003_fire_c","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(1.23,"TY_Attack_006")
	Sound(1.63,"SZ03_skill_000_baozha")

	AttackStart()
	Hurt(1.7,"hurt_61")
	DirFx(1.7,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(1.7,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.7,"ShakeDistance = 200","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

STake_Start("NB_XieShi_skill_003_NPCc_level2","char_wait_006")    --技能  27408  砸地技能NPC  引导0.9  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(1.6,"NB_XieShi_skill_003_fire_c","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(1.23,"TY_Attack_006")
	Sound(1.63,"SZ03_skill_000_baozha")

	AttackStart()
	Hurt(1.7,"hurt_61")
	DirFx(1.7,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(1.7,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.7,"ShakeDistance = 200","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
--------------------------------------------------------以上是邪尸3陨石----------------------------------------------------------------------


STake_Start("wolfmontain_door_41_1","char_wait_006")   --副本boss门41
    BlendMode(0)
    BlendTime(0) 
	Priority(0,"4")	
    Loop(0,2)
    Fx(0,"FX_FB_Gate_01","Reference",1,0,0,0,-45,0,0,0,1,3)

STake_Start("wolfmontain_door_41_2","char_wait_006")   --副本有阻挡门41
    BlendMode(0)
    BlendTime(0) 
	Priority(0,"4")		
    Loop(0,2)
    Fx(0,"FX_FB_Gate_03","Reference",1,0,0,0,-45,0,0,0,1,3)
	
STake_Start("wolfmontain_door_21_1","char_wait_006")   --副本boss门21
    BlendMode(0)
    BlendTime(0) 
	Priority(0,"4")		
    Loop(0,2)
    Fx(0,"FX_FB_Gate_01","Reference",0.5,0,0,0,-32,0,0,0,1,3)

STake_Start("wolfmontain_door_21_2","char_wait_006")   --副本有阻挡门21
    BlendMode(0)
    BlendTime(0) 
	Priority(0,"4")		
    Loop(0,2)
    Fx(0,"FX_FB_Gate_03","Reference",0.5,0,0,0,-32,0,0,0,1,3)
	
STake_Start("wolfmontain_door3","char_wait_006")   --副本进入大门特效
    BlendMode(0)
    BlendTime(0) 
	Priority(0,"4")	
    Loop(0,4)
    Fx(0,"FX_Duplicate_Portal","Reference",1,1,0,0,0,0,0,0,1,3)	
	
	
STake_Start("wolfmontain_door4","char_wait_006")   --副本无阻挡门
    BlendMode(0)
    BlendTime(0) 
	Priority(0,"4")		
    Loop(0,2)
    Fx(0,"FX_FB_Gate_02","Reference",1,0,0,0,-45,0,0,0,1,3)	

STake_Start("wolfmontain_door5","char_wait_006")   --RAID副本进入大门特效
    BlendMode(0)
    BlendTime(0) 
	Priority(0,"4")		
    Loop(0,4)
    Fx(0,"FX_Duplicate_Portal_a","Reference",1,1,0,0,0,0,0,0,1,3)	
	
STake_Start("NB_Wolf_CYCL_skill_010_NPC","char_wait_006")    --苍狼火焰冲击NPC   27233
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.01,"NB_Wolf_CYCL_Fire","Reference",1,0,0,0,0,0,0,0,1.8,1)
	Sound(0.03,"NB_TLKH_skill002start")

	AttackStart()
	Hurt(0,"hurt_81")
	DirFx(0,"NB_Wolf_CYCL_skill_002_hit",0,1)
    HurtBoneFx(0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

	
STake_Start("NB_Wolf_CYCL_bloodball_aoe","char_wait_006")    --苍狼血球爆   27255
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.01,"NB_Wolf_CYCL_bloodball_bao","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(0.5,"NB_Wolf_CYCL_bloodball_bao","Reference",1.1,0,0,0,-1,0,0,0,1,1)	
	Fx(1,"NB_Wolf_CYCL_bloodball_bao","Reference",1.2,0,0,0,-2,0,0,0,1,1)
	Fx(1.5,"NB_Wolf_CYCL_bloodball_bao","Reference",1.3,0,0,0,-3,0,0,0,1,1)	
	Fx(2,"NB_Wolf_CYCL_bloodball_bao","Reference",1.4,0,0,0,-4,0,0,0,1,1)	
	Fx(2,"NB_Wolf_CYCL_bloodball_aoe","Reference",1,0,0,0,-20,0,0,0,6,1)
	Sound(0.03,"QZ_SW_Skill_000_FX")
	Sound(0.53,"QZ_SW_Skill_000_FX")
	Sound(1.03,"QZ_SW_Skill_000_FX")
	Sound(1.53,"QZ_SW_Skill_000_FX")
	Sound(0.03,"NB_TY_skill006_start")
	Sound(1.9,"MC_Cannon_Fire_1")
	
	AttackStart()
	Hurt(2,"hurt_21")
	DirFx(2,"NB_Wolf_CYCL_skill_002_hit",0,1)
    HurtBoneFx(2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)	
	
	
STake_Start("NB_Wolf_CYCL_bloodball_HPUP","char_wait_006")    --苍狼血球加血    27257
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.01,"NB_Wolf_CYCL_bloodball_HPUP","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(0.03,"HG_THD_SW_skill_009")
	Sound(0.03,"QZ_SW_Skill014_Start1")


	
	

	
------------------------------------------------------桃花岛门派本-------------------------------------------------------	

STake_Start("N1_GanXiong_skill_004_NPC","char_wait_006")    --技能  29851  旋风召唤NPC  引导0.1  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,10)
	Fx(0.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(1.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(2.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(3.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(4.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(5.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(6.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(7.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(8.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(9.1,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)

	AttackStart()
	Hurt(0.00,"hurt_61")
	DirFx(0.00,"N1_SBTR01_skill_001_hit",0,1)
    HurtBoneFx(0.00,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("NB_QP_skill_006_NPC","char_wait_006")    --技能  29643  迷雾NPC  引导0.1  动作总长10
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Loop()
    Fx(0.01,"FX_Common_yanwudan_01","Reference",4,0,0,0,0,0,0,0,1,1)
    Fx(0.25,"FX_Common_yanwudan_01","Reference",4,0,0,0,0,0,0,0,0.2,1)
    Fx(0.5,"FX_Common_yanwudan_01","Reference",4,0,0,0,0,0,0,0,0.2,1)
    Fx(0.5,"FX_Common_yanwudan_01","Reference",4,0,0,0,0,0,0,180,1,1)

------------------------------------------------------吃金币-------------------------------------------------------	

STake_Start("Fx_common_goldcion_loop_jin","char_wait_006")   --金币转
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,1.5)
    Loop()
    
    Fx(0.01,"Fx_common_goldcion_loop_jin","Reference",1,0,0,0,20,0,0,0,1,3)

STake_Start("Fx_common_goldcion_jin","char_wait_006")   --金币消失
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,3.5)
    
    Fx(0.01,"Fx_common_goldcion_jin","Reference",1,0,0,0,20,0,0,0,1)		

    Sound(0.01,"Item_coin")
	
STake_Start("Fx_common_goldcion_jin02","char_wait_006")   --补刀山寨特效
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,3.5)
    Fx(0.01,"Fx_common_goldcion_jin","Reference",0.5,0,0,0,20,0,0,0,0.25)		

    Sound(0.01,"Item_coin")
	
STake_Start("Fx_common_goldcion_loop_yin","char_wait_006")   --银币转
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,1.5)
    Loop()
    
    Fx(0.01,"Fx_common_goldcion_loop_yin","Reference",1,0,0,0,20,0,0,0,1,3)

STake_Start("Fx_common_goldcion_yin","char_wait_006")   --银币消失
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,3.5)

    Sound(0.01,"Item_coin")
	
    Fx(0.01,"Fx_common_goldcion_yin","Reference",1,0,0,0,20,0,0,0,1)  	

STake_Start("Fx_common_goldcion_loop_tong","char_wait_006")   --铜币转
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,1.5)
    Loop()
    
    Fx(0.01,"Fx_common_goldcion_loop_tong","Reference",1,0,0,0,20,0,0,0,1,3)

STake_Start("Fx_common_goldcion_tong","char_wait_006")   --铜币消失
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,3.5)
    
    Sound(0.01,"Item_coin")

    Fx(0.01,"Fx_common_goldcion_tong","Reference",1,0,0,0,20,0,0,0,1)

	
	
	
	
------------------------------------------------------小源村-------------------------------------------------------

STake_Start("N1_MHCM01_skill_002_fire","char_wait_000")  --村民毒液  29708
    BlendMode(0)
    BlendTime(0.2) 
	
    Fx(0,"N1_MHCM01_skill_002_loopstart","Reference")
	
    AttackStart()	
  	Hurt(0,"hurt_21")

    HurtSound(0.05,"QZ_PA_Hit_12")

STake_Start("XYC_skill_000_start","char_wait_006")  --火球  29721
    BlendMode(0)
    BlendTime(0.2) 
	
    Fx(1.42,"FX_Cutscene_MHXYC_Huoqiu","Reference",0.8)
    --Fx(0.01,"Fx_common_rangemark_r","Reference",0.3)	
    --Fx(0.5,"Fx_common_rangemark_r","Reference",0.3)	
    --Fx(1,"Fx_common_rangemark_r","Reference",0.3)
    --Fx(1.5,"Fx_common_rangemark_r","Reference",0.3)

    Sound(1.42,"QQJ_skilljump_down")	
    Sound(1.42,"NB_TY_fire_000")
    Sound(1.6,"NB_TY_fire_000")
    Sound(1.8,"NB_TY_fire_000")
    Sound(2.0,"QQJ_Body_Impact")
    Sound(2.1,"NB_TY_fire_000")	
    Sound(2.3,"NB_TY_fire_000")	
    Sound(2.5,"NB_TY_fire_000")
    Sound(2.9,"NB_TY_fire_000")	

STake_Start("XYC_skill_000_fire","")  
    BlendMode(0)
    BlendTime(0.2) 

    AttackStart()	
  	Hurt(0,"hurt_71")
	RangeCameraShake(0.001,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.9","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2.5")

    HurtSound(0.05,"BTS_CR_HIT_new_2_1")


STake_Start("XYC_skill_000_fireball2","char_wait_006")  
    BlendMode(0)
    BlendTime(0.2) 

    AttackStart()	
  	Hurt(0,"hurt_61")
    HurtSound(0.05,"BTS_CR_HIT_new_2_1")

STake_Start("NB_WYHL_skill_002_fire","char_wait_006")  --能量喷泉  29717
    BlendMode(0)
    BlendTime(0.2) 
	
    Fx(0.01,"NB_WYHL_skill_002_fire","Reference")
    AttackStart()	
  	Hurt(0,"hurt_21")
    HurtSound(0.05,"BTS_CR_HIT_new_2_1")	
	HurtBoneFx(0,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("FX_UI_tiaoyue_1","char_wait_006")   --跳跃线
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,0.8)
    Loop()
    
    Fx(0.01,"FX_UI_tiaoyue_1","Reference")	
	
STake_Start("FX_UI_tiaoyue_2","char_wait_006")   --跳跃落点
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,2)
    Loop()
    
    Fx(0.01,"FX_UI_tiaoyue_2","Reference")	

STake_Start("FX_UI_XYC_zhiying_01","char_wait_006")   --引导线1
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,2)
    Loop()
    
    Fx(0.01,"FX_UI_XYC_zhiying_01","Reference",1,0,0,0,0)	
	
STake_Start("FX_UI_XYC_zhiying_02","char_wait_006")   --引导线2
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,2)
    Loop()
    
    Fx(0.01,"FX_UI_XYC_zhiying_02","Reference",1,0,0,0,10)		
	
STake_Start("FX_UI_XYC_zhiying_03","char_wait_006")   --引导线3
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,2)
    Loop()
    
    Fx(0.01,"FX_UI_XYC_zhiying_03","Reference",0.9,0,0,0,20)		
	
STake_Start("FX_UI_XYC_zhiying_04","char_wait_006")   --引导线4
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,2)
    Loop()
    
    Fx(0.01,"FX_UI_XYC_zhiying_04","Reference",1,0,0,0,40)	
	
STake_Start("FX_UI_XYC_zhiying_05","char_wait_006")   --引导线5
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,2)
    Loop()
    
    Fx(0.01,"FX_UI_XYC_zhiying_05","Reference",1,0,0,0,40)			
	
------------------------------------------------------试剑崖-------------------------------------------------------
	
STake_Start("N1_QZRMDZ01_skill_001_fire","char_wait_006")        --31017/31019/31023/31025 全真飞剑普攻1
    BlendMode(0)
    BlendTime(0.2)
    Fx(0.01,"HG_QZ_SW_skill_018_fire","Reference",0.5,0,0,0,0,0,0,0,1,1)

    AttackStart()
    Hurt(0,"hurt_21")
    HurtBoneFx(0,"FX_blood_jian_b","Spine1",1,0,0,0,0,0,0)
	
STake_Start("N1_QZRMDZ01_skill_003_char_start","char_wait_006")  		--NPC无极剑（三把剑）
    BlendMode(0)
    BlendTime(0)
    Fx(0.1,"HG_QZ_SW_skill_013_fire01","Reference",1,1,0,0,28)

STake_Start("N1_QZRMDZ01_skill_003_fire","char_wait_006")  		--NPC无极剑（三把剑）
    BlendMode(0)
    BlendTime(0)
	Fx(0,"HG_QZ_SW_skill_013_hit02","Reference",3,1,0,0,27) 
    Fx(0,"QZ_LswordB_attack_aoe","Reference",1,0,0,0,10)
    Fx(0.1,"QZ_LswordB_attack_aoe","Reference",1,0,0,0,10)
    Fx(0.2,"QZ_LswordB_attack_aoe","Reference",1,0,0,0,10)
  	AttackStart()
    Hurt(0,"hurt_21")
    Hurt(0.1,"hurt_21")
    Hurt(0.2,"hurt_21")
    HurtBoneFx(0,"FX_blood_jian_b","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.1,"FX_blood_jian_b","Spine1",1,0,0,0,70,-20,0)
    HurtBoneFx(0.2,"FX_blood_jian_b","Spine1",1,0,0,0,-70,20,0)
	
	
STake_Start("NEW_DSX_GB_skill_001_npc","char_wait_006")        ---大师兄神龙摆尾子弹NPC  29819
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"4")	
	
    Fx(0.216,"HG_GB_PA_skill_024_E1","Reference",1.3,0,0,-15,25,0,0,0,1)	
    Fx(0.386,"HG_GB_PA_skill_024_E2","Reference",1.3,0,0,25,25,0,0,0,1)	
	Charge(0,0.216,50,0,16) 
	Sound(0.01,"NB_TLKH_skill002start")
	
	AttackStart()
    Hurt(0,"hurt_81",0,1)
	DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)
	HurtSound(0.05,"BTS_CR_HIT_new_2_1") 
	HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)

	
STake_Start("NEW_DSX_QZ_skill_002_npc","char_wait_006")	---大师兄玄冥阵法技能Npc	29833  引导一0.3  引导二0.9
	BlendMode(0)
    BlendTime(0)
	Fx(2.01,"SG_QZ_SW_skill_030","Reference",1,0,0,0,0,0,0)
    RangeCameraShake(2.3,"ShakeDistance = 150","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
    RangeCameraShake(2.9,"ShakeDistance = 150","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

 
    AttackStart()
    Hurt(2.05,"hurt_21")
	Hurt(2.9,"hurt_61")
	
	
STake_Start("NEW_DSX_QZ_skill_003_npc","char_wait_006")	---大师兄寒流侵蚀技能NPC    29835  
	BlendMode(0)
    BlendTime(0)
	Sound(2.05,"QZ_Skill_Space_01")	
    Fx(2.01,"SG_QZ_SW_skill_027_001_fire","Reference",1,0,0,0,0,0,0)
	LoopFx(2,6,1,"SG_QZ_SW_skill_027_001_loop","",1)
    RangeCameraShake(0.01,"ShakeDistance = 250","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

	
    AttackStart()
    Hurt(2.5,"hurt_21")
    Hurt(3.0,"hurt_21")
	Hurt(3.5,"hurt_21")
	Hurt(4.0,"hurt_21")
	Hurt(4.5,"hurt_21")
	Hurt(5.0,"hurt_21")
	Hurt(5.5,"hurt_21")
	Hurt(6.0,"hurt_21")
	Hurt(6.5,"hurt_21")
    HurtSound(2.100,"QZ_PA_Hit_12")
	
	
STake_Start("NEW_DSX_THD_skill_003_npc","char_wait_000")   --大师兄燕返伤害  29841  引导一0.06  引导二0.12  引导三0.18  引导四0.25
    BlendMode(0)
    BlendTime(0)
    AttackStart()
    Hurt(0.06,"hurt_61")
    Hurt(0.12,"hurt_61")
    Hurt(0.18,"hurt_61")
    Hurt(0.25,"hurt_81")
    HurtSound(0.06,"BTS_FAN_Hit_001")
    HurtSound(0.12,"BTS_FAN_Hit_001")
    HurtSound(0.18,"BTS_FAN_Hit_001")
    HurtSound(0.25,"BTS_FAN_Hit_001")
	DirFx(0.06,"HG_THD_SW_skill_006005_hit",25,1,1,0,0)
	DirFx(0.12,"HG_THD_SW_skill_006005_hit",25,1,1,0,0)
	DirFx(0.18,"HG_THD_SW_skill_006005_hit",25,1,1,0,0)	
	DirFx(0.25,"HG_THD_SW_skill_006005_hit",25,1,1,0,0)	
    HurtBoneFx(0.06,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.12,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.18,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.25,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

------------------------------------------------------草原-------------------------------------------------------

STake_Start("GD_MGCY_ID_sculpture_000","char_wait_000")    --马头像待机
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Loop()
    Fx(0,"FX_jiaxue","Reference",10,0,0,0,-2)

	
STake_Start("N_DMB_XSS_skill_007_NPC","char_wait_006")    --乃蛮部驯兽师目标标记技能NPC  35053  引导0.400
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Fx(0.01,"N_DMB_XSS_skill_007_hit","Reference",1,0,0,0,0,0,0,0,1,1)

  	AttackStart()
    Hurt(0.01,"hurt_21")
	DirFx(0.01,"",0,1)
    HurtBoneFx(0.01,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)


STake_Start("Bear_skill_003_npc","char_wait_006")    --棕熊投石机能NPC  35033  引导0.400
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  

  	AttackStart()
    Hurt(0.400,"hurt_81")
	DirFx(0.400,"Bear_skill_003_hit",0,1)
    HurtBoneFx(0.400,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)

	
STake_Start("L1_ZDLQFZ_skill_006","char_wait_006")   --技能  25643  3方向裂地斩npc  引导1.6  动作总长3.166
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3.166)
	
    AttackStart()
  	Hurt(1.6,"hurt_61")  	
    DirFx(1.6,"T1_ZDLBFZ_skill_002_hit",0,1)
    HurtBoneFx(1.6,"FX_blood_dao_c","Spine1",1,0,0,0,-90,-10,0)
    HurtSound(1.6,"Big_blade_Hit")
	
	
STake_Start("L1_ZDLQFZ_skill_006_level1_npc","char_wait_006")   --技能  25643  3方向裂地斩npc  引导1.2  动作总长2.766
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.4,3.166)

    AttackStart()
  	Hurt(1.2,"hurt_61")  	
    DirFx(1.2,"T1_ZDLBFZ_skill_002_hit",0,1)
    HurtBoneFx(1.2,"FX_blood_dao_c","Spine1",1,0,0,0,-90,-10,0)
    HurtSound(1.2,"Big_blade_Hit")

	
STake_Start("L1_ZDLQFZ_skill_006_level2_npc","char_wait_006")   --技能  25643  3方向裂地斩npc  引导0.8  动作总长2.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.8,3.166)
 
    AttackStart()
  	Hurt(0.8,"hurt_61")  	
    DirFx(0.8,"T1_ZDLBFZ_skill_002_hit",0,1)
    HurtBoneFx(0.8,"FX_blood_dao_c","Spine1",1,0,0,0,-90,-10,0)
    HurtSound(0.8,"Big_blade_Hit")
	
	
------------------------------------------------------少林衍生-------------------------------------------------------

STake_Start("T1_JGMHS_skill_001_NPC_fire","char_wait_006")  --技能 29386  佛珠四方向发射技能NPC  
    BlendMode(0)
    BlendTime(0.2) 

  	AttackStart()
    Hurt(0,"hurt_21")


STake_Start("N1_SBTR01_skill_002_NPC_fire","char_wait_006")  --技能 29401  瑜伽波动拳技能NPC  
    BlendMode(0)
    BlendTime(0.2) 

  	AttackStart()
    Hurt(0,"hurt_21")
	
	
STake_Start("N1_JGMHYS01_skill_002_NPC_start","char_wait_006")  --技能 29379  泼油技能NPC  
    BlendMode(0)
    BlendTime(0.2)
	
	Fx(0,"N1_JGMHYS01_skill_002_ground_start","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(1,"N1_JGMHYS01_skill_002_ground_loop","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(2,"N1_JGMHYS01_skill_002_ground_end","Reference",1,0,0,0,0,0,0,0,1,1)	

STake_Start("N1_JGMHYS01_skill_002_NPC_fire","")
    BlendMode(0)
    BlendTime(0.2)
	

STake_Start("N1_JGMZBS01_skill_000_start_npc","char_wait_006")  --技能 29413  金刚门自爆僧自爆技能NPC    引导3.665
    BlendMode(0)
    BlendTime(0.2)
	
  	AttackStart()
  	Hurt(3.665,"hurt_81")    
  	Hurt(3.765,"hurt_81")    
    DirFx(3.665,"N_T1_JGQC_skill_000_hit",0,1,0.8)
    DirFx(3.765,"N_T1_JGQC_skill_000_hit",0,1,0.8)
	HurtBoneFx(3.665,"FX_blood_siyao_c","Spine",1,0,0,0,0,0,0)
	
STake_Start("N1_JGMZBS01_skill_000_start_npc01","char_wait_006")  --技能 29413  金刚门自爆僧自爆技能NPC    引导2.749
    BlendMode(0)
    BlendTime(0.2)
	
  	AttackStart()
  	Hurt(2.749,"hurt_81")    
  	Hurt(2.849,"hurt_81")    
    DirFx(2.749,"N_T1_JGQC_skill_000_hit",0,1,0.8)
    DirFx(2.849,"N_T1_JGQC_skill_000_hit",0,1,0.8)
	HurtBoneFx(2.749,"FX_blood_siyao_c","Spine",1,0,0,0,0,0,0)
	
STake_Start("N1_JGMZBS01_skill_000_start_npc02","char_wait_006")  --技能 29413  金刚门自爆僧自爆技能NPC    引导1.832
    BlendMode(0)
    BlendTime(0.2)
	
  	AttackStart()
  	Hurt(1.832,"hurt_81")    
  	Hurt(1.932,"hurt_81")    
    DirFx(1.832,"N_T1_JGQC_skill_000_hit",0,1,0.8)
    DirFx(1.932,"N_T1_JGQC_skill_000_hit",0,1,0.8)
	HurtBoneFx(1.832,"FX_blood_siyao_c","Spine",1,0,0,0,0,0,0)
	
STake_Start("N1_JGMZBS01_skill_000_start_npc03","char_wait_006")  --技能 29413  金刚门自爆僧自爆技能NPC    引导1.221
    BlendMode(0)
    BlendTime(0.2)
	
  	AttackStart()
  	Hurt(1.221,"hurt_81")    
  	Hurt(1.321,"hurt_81")    
    DirFx(1.221,"N_T1_JGQC_skill_000_hit",0,1,0.8)
    DirFx(1.321,"N_T1_JGQC_skill_000_hit",0,1,0.8)
	HurtBoneFx(1.221,"FX_blood_siyao_c","Spine",1,0,0,0,0,0,0)
	
------------------------------------------------------吉永号-------------------------------------------------------

STake_Start("N2_Throw_skill_005_NPC_start","char_wait_006")    --炸弹技能NPC  29619
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.4,"N1_Blade1_skill_005_bao","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(0.4,"SZ03_skill_000_baozha")
	
STake_Start("N2_Throw_skill_005_NPC_fire","")
    BlendMode(0)
    BlendTime(0.2) 

  	AttackStart()
    Hurt(0.4,"hurt_21")
    DirFx(0.4,"N_N1_XLGB01_skill_002_hit",0,1,1)
	HurtBoneFx(0.4,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
	
STake_Start("N2_Throw_skill_007_NPC_start","char_wait_006")    --炸弹技能NPC  29623
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.4,"N2_Throw02_skill_007_baozha","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(0.4,"SZ03_skill_000_baozha")
	
STake_Start("N2_Throw_skill_007_NPC_fire","")
    BlendMode(0)
    BlendTime(0.2) 

  	AttackStart()
    Hurt(0.4,"hurt_71")
    DirFx(0.4,"N_N1_XLGB01_skill_002_hit",0,1,1)
	HurtBoneFx(0.4,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)

STake_Start("NB_JB_skill_006_fire","char_wait_006")    --蒋波泉涌  29685
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Fx(1.8,"NB_JB_skill_006_shuizhu","Reference",1,0,0,0,0,0,0,0,1,1)		
	
    AttackStart()
  	Hurt(1.8,"hurt_81")
    DirFx(1.8,"NB_JB_skill_hit",0,1) 
    HurtSound(1.8,"BTS_FAN_Hit_003")	
	
STake_Start("NB_TC_skill_008_fire","char_wait_006")     --童川力劈华山  29673
    BlendMode(0)
    BlendTime(0.2)
	Fx(0.01,"NB_TC_skill_008_start","Reference",1,0,0,0,0,0,0,0,1,1) 
	
    AttackStart()
    Hurt(0,"hurt_71")
    HurtSound(0,"") 
    DirFx(0,"NB_TC_skill_008_hit",0,1) 
    HurtBoneFx(0,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)
	
STake_Start("NB_LQS_skill_007_fire","char_wait_006")     --笼子   
    BlendMode(0)
    BlendTime(0.2)

    AttackStart()
    Hurt(0.01,"hurt_61")
    HurtSound(0.01,"") 
    DirFx(0.01,"",25,1,1,-60,0)
    --HurtBoneFx(0.01,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)

STake_Start("NB_LQS_skill_008_fire","char_wait_006")     --刀子   29658
    BlendMode(0)
    BlendTime(0.2)
    LineFx(1.6,"NB_LQS_skill_004_Trail_300",0,"Reference","Reference",1,800,0,0,100,0,0,0,1.2,1)
    LineFx(0.1,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,100,0,0,0,0.6,1)		
	--Fx(1.6,"NB_LQS_skill_004_Trail_300","Reference",1,0,0,0,100,0,0,0,1.2,1)	
	
    AttackStart()
    Hurt(0.01,"hurt_21")
    HurtSound(0.01,"") 
    DirFx(0.01,"NB_LQS_skill_hit")
    HurtBoneFx(0.01,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)
	
	
------------------------------------------------------起源之地-------------------------------------------------------


STake_Start("NB_Beetle_CH_skill_004_1","char_wait_006")  --虫后毒液   32149
    BlendMode(0)
    BlendTime(0.2)
 
	Fx(0.2,"NB_Beetle_CH_skill_004_ground","Reference")
 
    AttackStart()
  	Hurt(0.2,"hurt_61")
    DirFx(0.2,"Scorpion_XZW01_skill_001_2_hit",0,1)  
  	HurtSound(0.2,"HMW01_Hit_003")	
    HurtBoneFx(0.2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("NB_Beetle_CH_skill_012_NPC","char_wait_006")    --技能  32200  持续钻出NPC   引导0.1击飞
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	
    AttackStart()
  	Hurt(0.1,"hurt_81")
    DirFx(0.1,"Scorpion_XZW01_skill_001_2_hit",0,1)  
  	HurtSound(0.1,"HMW01_Hit_003")	
    HurtBoneFx(0.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("NB_YTGZT_skill_003_1","char_wait_006")  --陨铁落雷    32169
    BlendMode(0)
    BlendTime(0.2)

	Fx(0.01,"NB_YTGZT_skill_003_1","Reference")
	Sound(0.1,"YTGZT_1_Thunder")
	
    AttackStart()
  	Hurt(0.3,"hurt_21")
    DirFx(0.3,"",0,1)  
	--HurtBoneFx(1.6,"FX_blood_siyao_c","Spine1",1.2,0,0,0,0,0,0)
  	HurtSound(0.3,"BTS_CR_HIT_new_e1")
	

STake_Start("N_HYHY02_skill_002_NPC","char_wait_006")    --黑衣先知落雷技能NPC  32131/27283  引导1.675
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(1.475,"N_HYHY02_skill_001_fire","Reference",1,0,0,28,0,0,0,0,1,1)
	
    AttackStart()
	Hurt(1.675,"hurt_21")
	HurtSound(1.675,"BTS_CR_HIT_new_e1")
	DirFx(1.675,"",0,1)
    HurtBoneFx(1.675,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)
	
STake_Start("N_HYHY02_skill_002_NPC01","char_wait_006")    --黑衣先知落雷技能NPC  32131/27283  引导1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(1.057,"N_HYHY02_skill_001_fire","Reference",1,0,0,28,0,0,0,0,1,1)

    AttackStart()
	Hurt(1.2,"hurt_21")
	HurtSound(1.2,"BTS_CR_HIT_new_e1")
	DirFx(1.2,"",0,1)
    HurtBoneFx(1.2,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)

STake_Start("N_HYHY02_skill_002_NPC03","char_wait_006")    --黑衣先知落雷技能NPC  32131/27283  引导0.8
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.6,"N_HYHY02_skill_001_fire","Reference",1,0,0,28,0,0,0,0,1,1)

    AttackStart()
	Hurt(0.8,"hurt_21")
	HurtSound(0.8,"BTS_CR_HIT_new_e1")
	DirFx(0.8,"",0,1)
    HurtBoneFx(0.8,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)


STake_Start("NB_HYXZ_skill_001_NPC","char_wait_006")    --黑衣幻影空间斩技能NPC  32129/27279  引导1.667
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(1.667,"NB_HYXZ_skill_001_fire","Spine1",1,1,0,0,30)
	Sound(1.65,"QZ_SW_Skill014_Start2")
	Sound(1.65,"AM_Beetle_Skill_06_02")
	Sound(1.6,"QZ_SW_Skill012_Start2")

  	AttackStart()
    Hurt(1.667,"hurt_61")
	DirFx(1.667,"N2_XLCH_skill_001_hit",0,1)
	HurtSound(1.667,"BTS_FAN_Hit_003")
    HurtBoneFx(1.667,"FX_blood_siyao_c","Spine1",1.2,0,0,0,0,0,0)
	
STake_Start("NB_HYXZ_skill_001_NPC01","char_wait_006")    --黑衣幻影空间斩技能NPC  32129/27279  引导1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(1.2,"NB_HYXZ_skill_001_fire","Spine1",1,1,0,0,30)
	Sound(1.25,"QZ_SW_Skill014_Start2")
	Sound(1.25,"AM_Beetle_Skill_06_02")
	Sound(1.2,"QZ_SW_Skill012_Start2")

  	AttackStart()
    Hurt(1.2,"hurt_61")
	DirFx(1.2,"N2_XLCH_skill_001_hit",0,1)
	HurtSound(1.2,"BTS_FAN_Hit_003")
    HurtBoneFx(1.2,"FX_blood_siyao_c","Spine1",1.2,0,0,0,0,0,0)
	
STake_Start("NB_HYXZ_skill_001_NPC03","char_wait_006")    --黑衣幻影空间斩技能NPC  32129/27279  引导0.8
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.8,"NB_HYXZ_skill_001_fire","Spine1",1,1,0,0,30)
	Sound(0.8,"QZ_SW_Skill014_Start2")
	Sound(0.8,"AM_Beetle_Skill_06_02")
	Sound(0.7,"QZ_SW_Skill012_Start2")

  	AttackStart()
    Hurt(0.8,"hurt_61")
	DirFx(0.8,"N2_XLCH_skill_001_hit",0,1)
	HurtSound(0.8,"BTS_FAN_Hit_003")
    HurtBoneFx(0.8,"FX_blood_siyao_c","Spine1",1.2,0,0,0,0,0,0)

STake_Start("NB_HYXZ_skill_001_NPC_Boss","char_wait_006")    --☆☆☆☆☆黑衣先知空间斩技能NPC  32135  引导0.735  ☆☆☆☆BOSS专用☆☆☆☆
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.735,"NB_HYXZ_skill_001_fire","Spine1",1,1,0,0,30)
	Sound(0.7,"QZ_SW_Skill014_Start2")
	Sound(0.7,"AM_Beetle_Skill_06_02")
	Sound(0.65,"QZ_SW_Skill012_Start2")


    AttackStart()
	Hurt(0.893,"hurt_61")
	HurtSound(0.893,"BTS_FAN_Hit_002")
	DirFx(0.893,"N2_XLCH_skill_001_hit",0,1)
    HurtBoneFx(0.893,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)


STake_Start("N_N1_XLGB01_skill_002_NPC","char_wait_006")    --西辽弓手H箭雨技能NPC  32111
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(1.03,"N_N1_XLGB01_skill_002_aoe","Reference")

  	AttackStart()
    Hurt(1.03,"hurt_21")
	DirFx(1.03,"N_N1_XLGB01_skill_002_hit",0,1)
    HurtBoneFx(1.03,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("N_Beetle_SMJC_skill_002_NPC","char_wait_006")    --原生甲虫喷吐毒液技能NPC  32125  引导1.76
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(1.76,"NB_Beetle_CH_skill_004_ground","Reference",0.9,0,0,0,0,0,0,0,1,1)

  	AttackStart()
    Hurt(1.76,"hurt_21")
	DirFx(1.76,"Scorpion_XZW01_skill_001_2_hit",0,1)
    HurtBoneFx(1.76,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
	
STake_Start("N_Beetle_SMJC_skill_002_NPC01","char_wait_006")    --原生甲虫喷吐毒液技能NPC  32125  引导1.37
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(1.37,"NB_Beetle_CH_skill_004_ground","Reference",0.9,0,0,0,0,0,0,0,1,1)

  	AttackStart()
    Hurt(1.37,"hurt_21")
	DirFx(1.37,"Scorpion_XZW01_skill_001_2_hit",0,1)
    HurtBoneFx(1.37,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)

STake_Start("N_Beetle_SMJC_skill_002_NPC03","char_wait_006")    --原生甲虫喷吐毒液技能NPC  32125  引导0.72
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.72,"NB_Beetle_CH_skill_004_ground","Reference",0.9,0,0,0,0,0,0,0,1,1)

  	AttackStart()
    Hurt(0.72,"hurt_21")
	DirFx(0.72,"Scorpion_XZW01_skill_001_2_hit",0,1)
    HurtBoneFx(0.72,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)

	
STake_Start("N_DYJC_skill_000_NPC","char_wait_006")    --毒液甲虫喷射毒液技能NPC  32211  引导0.4
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(0.4,"NB_Beetle_CH_skill_004_ground","Reference",0.6,0,0,0,0,0,0,0,1,1)

  	AttackStart()
    Hurt(0.4,"hurt_21")
	DirFx(0.4,"Scorpion_XZW01_skill_001_2_hit",0,1)
    HurtBoneFx(0.4,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)

	
STake_Start("N_ZBJC_skill_002_NPC","char_wait_006")    --自爆甲虫自爆技能NPC  32209  引导0.001
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  

  	AttackStart()
    Hurt(0.001,"hurt_81")
	DirFx(0.001,"N_N1_XLGB01_skill_002_hit",0,1)
    HurtBoneFx(0.001,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
-----------------------------------------------------战场-----------------------------------------------
	
STake_Start("N_JGS_HYJZC_skill_001_npc","char_wait_006")  --战场战车火炮NPC   33415/33407
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    Fx(0.200,"N_JGS_LSFZC_skill_001_hit")  
	Sound(0.2,"JGS_GCZC_NPCSkill_004_B")
  
  	
    AttackStart()
  	Hurt(0.200,"hurt_61")
	DirFx(0.200,"N_T1_JGQC_skill_000_hit",0,1,0.4)
    HurtBoneFx(0.200,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)
	
STake_Start("NB_Monkey_BYKY_skill_003_npc","char_wait_006")  --变异狂猿投掷石块NPC   33431
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")     
  	
    AttackStart()
  	Hurt(0.2,"hurt_61")
	DirFx(0.2,"N_DMB_GB_skill_002_hit",0,1)
    HurtBoneFx(0.2,"FX_blood_quan_c","Spine1",1.2,0,0,0,0,0,0)
	
	

-----------------------------------------------------塞外衍生-黑风山-梅超风-----------------------------------------------

STake_Start("NB_MCF_skill_003_NPC_start","char_wait_006")   --九阴白骨爪技能NPC  33309  引导1秒后每秒造成1次伤害，共3秒
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(0.1,"NB_MCF_skill_003_quan","Reference",1,0,0,0,0,0,0,0,1,1)
    
    AttackStart()
    Hurt(0,"hurt_71")
    DirFx(0,"NB_MCF_skill_001_handwp_hit",0,1)	
    HurtBoneFx(0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
STake_Start("NB_MCF_skill_003_NPC_fire","char_wait_006")
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 


STake_Start("NPC_MCFraid_skill_006","char_wait_006")   --骨之丧歌    38013/38014/38015
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 

	AttackStart()
  	Hurt(5,"hurt_21")
  	Hurt(6,"hurt_21")
  	Hurt(7,"hurt_21")
  	Hurt(8,"hurt_21")
  	Hurt(9,"hurt_21")	
    HurtSound(0.5,"")
    DirFx(0.5,"",0,1) 
	HurtBoneFx(5,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)	
	HurtBoneFx(6,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)	
	HurtBoneFx(7,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)	
	HurtBoneFx(8,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)	
	HurtBoneFx(9,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)	
	
	
STake_Start("NPC_MCFraid_skill_005","char_wait_006")   --九阴白骨爪插槽   38010
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 

	AttackStart()
  	Hurt(0.2,"hurt_81")
    HurtSound(0.2,"")
    DirFx(0.2,"",0,1) 
	HurtBoneFx(0.2,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)	
	

STake_Start("NPC_MCFraid_skill_011","char_wait_006")   --白骨陷阱    38030
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
	
 	Fx(0.01,"NB_MCF_skill_003_fire","Reference",1,1,0,0,10,0,0,0,1.5)	   
	Fx(0.6,"NB_MCF_skill_003_quan","Reference",1.6,1,0,0,10,0,0,0,1)		
	--Fx(1.6,"NB_MCF_skill_003_fire","Reference",1,1,0,0,0,0,0,0,1)		
    AttackStart()
    Hurt(1.6,"hurt_71")
    DirFx(1.6,"",0,1)	
    HurtBoneFx(1.6,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)	
	
STake_Start("NPC_MCFraid_skill_0081","char_wait_006")   --近震环瞬发    38020
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
	Fx(0.01,"NB_MCFraid_skill_008_fire","Reference",1.15,1,0,0,5,0,0,0,1)	
	Fx(0.01,"NB_MCFraid_skill_008_fire","Reference",0.7,1,0,0,5,0,0,0,1)		
	Sound(0.01,"Lzw_Skill_04")
	Sound(0.01,"HYHY_Skill_Start")
	
	AttackStart()
  	Hurt(0,"hurt_81")
    HurtSound(0,"QZ_PA_Hit_11")
    DirFx(0,"",0,1) 
	HurtBoneFx(0,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)	
	RangeCameraShake(0.01,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
	
STake_Start("NPC_MCFraid_skill_0091","char_wait_006")   --中震环瞬发    38024
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
	Fx(0.01,"NB_MCFraid_skill_009_fire","Reference",0.55,1,0,0,0,0,0,0,1)	
	Fx(0.01,"NB_MCFraid_skill_009_fire","Reference",0.8,1,0,0,0,0,0,0,1)			
	Fx(0.01,"NB_MCFraid_skill_009_fire","Reference",1,1,0,0,0,0,0,0,1)		
	Sound(0.01,"Lzw_Skill_04")
	Sound(0.01,"HYHY_Skill_Start")

	
	AttackStart()
  	Hurt(0,"hurt_81")
    HurtSound(0,"QZ_PA_Hit_11")
    DirFx(0,"",0,1) 
	HurtBoneFx(0,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)	
	RangeCameraShake(0.01,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
	
STake_Start("NPC_MCFraid_skill_0101","char_wait_006")   --远震环瞬发    38028
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
	Fx(0.01,"NB_MCFraid_skill_010_fire","Reference",0.73,1,0,0,0,0,0,0,1)
	Fx(0.01,"NB_MCFraid_skill_010_fire","Reference",0.95,1,0,0,0,0,0,0,1)	
	Fx(0.01,"NB_MCFraid_skill_010_fire","Reference",1.2,1,0,0,0,0,0,0,1)	
	Sound(0.01,"Lzw_Skill_04")
	Sound(0.01,"HYHY_Skill_Start")

	
	AttackStart()
  	Hurt(0,"hurt_81")
    HurtSound(0,"QZ_PA_Hit_11")
    DirFx(0,"",0,1) 
	HurtBoneFx(0,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)		
	RangeCameraShake(0.01,"ShakeDistance = 400","ShakeMode = 3","ShakeTimes = 0.8","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
	
	
STake_Start("N_DMB_GB_skill_005_NPC","char_wait_006")    --梅超风召唤寒冰射手-寒冰乱射技能NPC  38043
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(3,"N_DMB_GB_skill_005_ground","Reference",1,0,0,0,0,0,0,0,1)

  	AttackStart()
    Hurt(3.2,"hurt_21")
	DirFx(3.2,"FX_xihun_hit",0,1)
    HurtBoneFx(3.2,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
	
	
	
	
--------------------------------------------------------中都衍生--------------------------------------------------

STake_Start("NNB_HYZ_skill_000_NPC","char_wait_006")   --火云子火焰符咒技能NPC  37058  引导0.1
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(0.1,"","Reference",1,0,0,0,0,0,0,0,1,1)
    
    AttackStart()
    Hurt(0.1,"hurt_21")
    DirFx(0.1,"N_N1_XLGB01_skill_002_hit",0,1)	
    HurtBoneFx(0.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("NNB_HYZ_skill_000_1_NPC","char_wait_006")   --火云子火焰符咒爆炸技能NPC  37060  引导0.1
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(0.1,"N_T1_JGQC_skill_000_hit","Reference",1,0,0,0,0,0,0,0,1,1)
    
    AttackStart()
    Hurt(0.1,"hurt_21")
    DirFx(0.1,"N_N1_XLGB01_skill_002_hit",0,1)	
    HurtBoneFx(0.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	

STake_Start("NB_HYZ_skill_006_NPC","char_wait_006")   --火云子火柱技能NPC  37069  引导2
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
	Loop(2,3)
    Fx(2.1,"NB_HYZ_skill_006_huozhu","Reference",1,0,0,0,0,0,0,0,1,1)
    
    AttackStart()
    Hurt(0,"hurt_21")
    DirFx(0,"N_N1_XLGB01_skill_002_hit",0,1)	
    HurtBoneFx(0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

	
STake_Start("NB_HYZ_skill_009_NPC","char_wait_006")   --火云子火焰符咒(改)NPC  37079  引导1.6
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(1.6,"NB_HYZ_skill_006_huozhu","Reference",1,0,0,0,0,0,0,0,1,1)
    
    AttackStart()
    Hurt(1.6,"hurt_21")
    DirFx(1.6,"N_N1_XLGB01_skill_002_hit",0,1)	
    HurtBoneFx(1.6,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


	
STake_Start("NB_Spider_skill_000_NPC_start","char_wait_006")   --伏地蛀冰刺技能NPC  37029  引导一0.001  引导二2  引导三4  引导四6
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
	FrameTime(0,6)
	Loop(0.2,1.2)
    Fx(0.1,"NB_Spider_skill_000_fire_start","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(0.2,"NB_Spider_skill_000_fire_loop","Reference",1,0,0,0,0,0,0,0,1,3)
    
    AttackStart()
    Hurt(0.1,"hurt_21")
    Hurt(2,"hurt_21")
    Hurt(4,"hurt_21")
    Hurt(6,"hurt_21")
    DirFx(0.1,"FX_xihun_hit",0,1)	
    DirFx(2,"FX_xihun_hit",0,1)	
    DirFx(4,"FX_xihun_hit",0,1)	
    DirFx(6,"FX_xihun_hit",0,1)	
    HurtBoneFx(0.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(4,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(6,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

STake_Start("NB_Spider_skill_000_NPC_end","char_wait_006")
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5") 
    Fx(0.1,"NB_Spider_skill_000_fire_end","Reference",1,0,0,0,0,0,0,0,1,1)


STake_Start("NB_WuJi_skill_004_NPC","char_wait_006")    --投矛，跳跃攻击技能NPC  37189  引导0.1
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.1,"NB_WuJi_skill_004_wuqi","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(0.1,"hurt_21")
	DirFx(0.1,"N_N1_RFZ01_attack_002_hit",0,1)
    HurtBoneFx(0.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("NB_PoXie_skill_002_NPC","char_wait_006")    --天箭技能NPC  37129    引导1.6
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.8,"NB_PoXie_skill_002_hit","Reference",1,0,0,0,200,0,0,0,1,1)
	Fx(1.9,"NB_PoXie_skill_002_a","Reference",1,0,0,0,0,0,0,0,1,0)
    Sound(1.800,"QZ_SW_Jian")
	Sound(1.8,"HG_GB_PA_Skill_000_Combo2")
	
	AttackStart()
	Hurt(1.9,"hurt_21")
	DirFx(1.9,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(1.9,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)	

STake_Start("NB_PoXie_skill_002_NPC_level1","char_wait_006")    --天箭技能NPC  37129    引导1.2
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.4,"NB_PoXie_skill_002_hit","Reference",1,0,0,0,200,0,0,0,1,1)
	Fx(1.5,"NB_PoXie_skill_002_a","Reference",1,0,0,0,0,0,0,0,1,0)
    Sound(1.400,"QZ_SW_Jian")
	Sound(1.4,"HG_GB_PA_Skill_000_Combo2")
	
	AttackStart()
	Hurt(1.5,"hurt_21")
	DirFx(1.5,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(1.5,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

STake_Start("NB_PoXie_skill_002_NPC_level2","char_wait_006")    --天箭技能NPC  37129    引导0.8
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.0,"NB_PoXie_skill_002_hit","Reference",1,0,0,0,200,0,0,0,1,1)
	Fx(1.1,"NB_PoXie_skill_002_a","Reference",1,0,0,0,0,0,0,0,1,0)
    Sound(1.100,"QZ_SW_Jian")
	Sound(1.0,"HG_GB_PA_Skill_000_Combo2")
	
	AttackStart()
	Hurt(1.1,"hurt_21")
	DirFx(1.1,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(1.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("NB_PoXie_skill_003_NPC","char_wait_006")    --天怒技能NPC  37131    引导0.1
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.55,"NB_PoXie_skill_002_hit","Reference",3,0,0,0,70,0,0,0,1,1)
	Fx(1.65,"NB_PoXie_skill_002_b","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(2.65,"NB_PoXie_skill_002_b","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(3.65,"NB_PoXie_skill_002_b","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(4.65,"NB_PoXie_skill_002_b","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(5.65,"NB_PoXie_skill_002_b","Reference",1,0,0,0,0,0,0,0,1,1)
    Sound(1.500,"QZ_SW_Jian")
	Sound(1.5,"HG_GB_PA_Skill_000_Combo2")
    Sound(1.500,"QZ_SW_QXQJ_JianYu")
    Sound(3.100,"QZ_SW_QXQJ_JianYu")
    Sound(4.500,"QZ_SW_QXQJ_JianYu")
	
	AttackStart()
	Hurt(0,"hurt_21")
	DirFx(0,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("N_N2_XueMan01_skill_002_NPC","char_wait_006")    --藤蔓捆缚NPC  37107    引导0.1
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.1,"NB_XMS_skill_002_3","Reference",2,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(0.1,"hurt_21")
	DirFx(0.1,"N1_Dart_skill_001_hit",0,1)
    HurtBoneFx(0.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

	
STake_Start("NB_FL_FeiLian_skill_002_NPC","char_wait_006")    --莲花攻击技能NPC  37077    引导0.001/0.2/0.4/0.6/0.8/1/1.2/1.4/1.6/1.8
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.01,"","",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(0.1,"hurt_61")
	Hurt(0.3,"hurt_61")
	Hurt(0.5,"hurt_61")
	Hurt(0.7,"hurt_61")
	Hurt(0.9,"hurt_61")
	Hurt(1.1,"hurt_61")
	Hurt(1.3,"hurt_61")
	Hurt(1.5,"hurt_61")
	Hurt(1.7,"hurt_61")
	Hurt(1.9,"hurt_61")
	DirFx(0.1,"N1_Dart_skill_001_hit",0,1)
	DirFx(0.3,"N1_Dart_skill_001_hit",0,1)
	DirFx(0.5,"N1_Dart_skill_001_hit",0,1)
	DirFx(0.7,"N1_Dart_skill_001_hit",0,1)
	DirFx(0.9,"N1_Dart_skill_001_hit",0,1)
	DirFx(1.1,"N1_Dart_skill_001_hit",0,1)
	DirFx(1.3,"N1_Dart_skill_001_hit",0,1)
	DirFx(1.5,"N1_Dart_skill_001_hit",0,1)
	DirFx(1.7,"N1_Dart_skill_001_hit",0,1)
	DirFx(1.9,"N1_Dart_skill_001_hit",0,1)
    HurtBoneFx(0.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.3,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.5,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.7,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.9,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.3,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.5,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.7,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.9,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)




-------------------------------------------------第一颗-----------------------------------------------------------
STake_Start("NB_TZD_skill_007_NPC01","char_wait_006")    --汤祖德召唤技能NPC  37251    引导1.600
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.580,"NB_XieShi_skill_003_fire_a","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(1.600,"hurt_21")
	DirFx(1.600,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(1.600,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
STake_Start("NB_TZD_skill_007_NPC01_level1","char_wait_006")    --汤祖德召唤技能NPC  37251    引导1.200
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.180,"NB_XieShi_skill_003_fire_a","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(1.200,"hurt_21")
	DirFx(1.200,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(1.200,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
STake_Start("NB_TZD_skill_007_NPC01_level2","char_wait_006")    --汤祖德召唤技能NPC  37251    引导0.8
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.780,"NB_XieShi_skill_003_fire_a","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(0.800,"hurt_21")
	DirFx(0.800,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(0.800,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
-------------------------------------------------第二颗-----------------------------------------------------------
STake_Start("NB_TZD_skill_007_NPC02","char_wait_006")    --汤祖德召唤技能NPC  37252    引导1.600
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.580,"NB_XieShi_skill_003_fire_b","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(1.600,"hurt_21")
	DirFx(1.600,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(1.600,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
STake_Start("NB_TZD_skill_007_NPC02_level1","char_wait_006")    --汤祖德召唤技能NPC  37252    引导1.200
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.180,"NB_XieShi_skill_003_fire_b","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(1.200,"hurt_21")
	DirFx(1.200,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(1.200,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
STake_Start("NB_TZD_skill_007_NPC02_level2","char_wait_006")    --汤祖德召唤技能NPC  37252    引导0.8
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.780,"NB_XieShi_skill_003_fire_b","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(0.800,"hurt_21")
	DirFx(0.800,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(0.800,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
-------------------------------------------------第三颗-----------------------------------------------------------
STake_Start("NB_TZD_skill_007_NPC03","char_wait_006")    --汤祖德召唤技能NPC  37253    引导1.600
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.580,"NB_XieShi_skill_003_fire_c","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(1.600,"hurt_21")
	DirFx(1.600,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(1.600,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

STake_Start("NB_TZD_skill_007_NPC03_level1","char_wait_006")    --汤祖德召唤技能NPC  37253    引导1.2
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(1.180,"NB_XieShi_skill_003_fire_c","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(1.2,"hurt_21")
	DirFx(1.2,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(1.2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

STake_Start("NB_TZD_skill_007_NPC03_level2","char_wait_006")    --汤祖德召唤技能NPC  37253    引导0.8
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.780,"NB_XieShi_skill_003_fire_c","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(0.8,"hurt_21")
	DirFx(0.8,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(0.8,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
-------------------------------------------------------------------------------------------------------------------	
STake_Start("NB_Lizard_skill_004_NPC","char_wait_006")    --BOSS大蜥蜴毒水弹技能NPC  37293    引导0.3
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
    Fx(0.3,"NB_Lizard_skill_004_ground","Reference",1,0,0,0,0,0,0,0,1,1)

  	AttackStart()
    Hurt(0.3,"hurt_61")
	DirFx(0.3,"Scorpion_XZW01_skill_001_2_hit",0,1)
    HurtBoneFx(0.3,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
	
STake_Start("NB_ML_skill_002_NPC","char_wait_006")    --美兰后跳射击技能NPC  37299    引导一0.45  引导二0.66
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.4,"NB_ML_skill_002_ground","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(0.45,"hurt_21")
	Hurt(0.66,"hurt_61")
	DirFx(0.45,"N1_Blade3_attack_hit",0,1)
	DirFx(0.66,"N1_Blade3_attack_hit",0,1)
    HurtBoneFx(0.45,"FX_blood_gongjian_c","Spine1",1,0,0,0,180,0,0)
    HurtBoneFx(0.66,"FX_blood_gongjian_c","Spine1",1,0,0,0,180,0,0)

--------------------------------------------------------怪物通用技能--------------------------------------------------
STake_Start("Common_Skill_LongJuanFeng","char_wait_006")    --召唤龙卷风NPC  302    引导0.1
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Loop(0,1)
	Fx(0.01,"FX_ZC_skill_02","Reference",1,0,0,0,0,0,0,0,1,1)
	
	AttackStart()
	Hurt(0.00,"hurt_81")
	DirFx(0.00,"",0,1)
    HurtBoneFx(0.00,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("Common_Skill_ShanDianQiu","char_wait_006")    --召唤闪电球NPC  310    引导1/1.2/1.4个一次伤害
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Fx(0.01,"FX_ZC_skill_04_loop","Reference",1,0,5,10,35,0,0,0,1,1)
	Fx(0.21,"FX_ZC_skill_04_loop","Reference",1,0,-3,4,45,0,0,0,1,1)
	Fx(0.41,"FX_ZC_skill_04_loop","Reference",1,0,10,-5,30,0,0,0,1,1)
	Fx(1.0,"FX_ZC_skill_04_bao","Reference",1,0,0,10,35,0,0,0,1,1)
	Fx(1.2,"FX_ZC_skill_04_bao","Reference",1,0,-3,4,45,0,0,0,1,1)
	Fx(1.4,"FX_ZC_skill_04_bao","Reference",1,0,10,-5,30,0,0,0,1,1)
	
	AttackStart()
	Hurt(1,"hurt_21")
	Hurt(1.2,"hurt_21")
	Hurt(1.4,"hurt_21")
	DirFx(1,"FX_xihun_Thunder_hit",0,1)
	DirFx(1.2,"FX_xihun_Thunder_hit",0,1)
	DirFx(1.4,"FX_xihun_Thunder_hit",0,1)
    HurtBoneFx(1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.4,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("Common_Skill_ZhaoZe","char_wait_006")    --召唤沼泽NPC  307    引导0.1
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	PlaySpeed(0,10,0.001)
	Loop()
	Fx(0.01,"FX_ZC_skill_01","Reference",1,0,0,0,0,0,0,0,1,3)

	AttackStart()
	Hurt(0.00,"hurt_21")
	DirFx(0.00,"",0,1)
    --HurtBoneFx(0.00,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("Common_Skill_YuNi","char_wait_006")    --召唤淤泥NPC  307    引导0.1
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Loop()
	Fx(0.01,"FX_ZC_skill_05","Reference",1,0,0,0,0,0,0,0,1,3)

	AttackStart()
	Hurt(0.00,"hurt_21")
	DirFx(0.00,"",0,1)
    --HurtBoneFx(0.00,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("Common_Skill_Men","char_wait_006")    --召唤门NPC
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Loop(0,2)
	Fx(0.01,"FX_ZC_skill_06","Reference",1,0,0,0,0,0,0,0,1,1)


STake_Start("Common_Skill_YinShenQiu","char_wait_006")    --召唤隐身球NPC
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Loop(0,1)
	Fx(0.01,"FX_ZC_skill_07","Reference",1,0,0,0,0,0,0,0,1,1)


STake_Start("Common_Skill_HuangQuan","char_wait_006")    --召唤黄色区域NPC
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Loop(0,1)
	Fx(0.01,"FX_ZC_skill_08","Reference",1,0,0,0,0,0,0,0,1,1)


STake_Start("Common_Skill_LanQuan","char_wait_006")    --召唤蓝色区域NPC
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Loop(0,1)
	Fx(0.01,"FX_ZC_skill_09","Reference",1,0,0,0,0,0,0,0,1,1)


STake_Start("ChenMin_FanFinFhen","char_wait_006")	--反隐技能NPC
    BlendMode(0)
    BlendTime(0) 
    Fx(0.01,"NB_MCF_GY_skill_002_fire","Reference",2,0,0,30,-15,0,0,0,1.2,1)
    Fx(0.01,"Fx_MXD_Ancestral spirit_Baozha","Reference",0.16,0,0,0,-80,0,0,0,1.7,1)
    Fx(0.01,"ZC_skill_fanyinxing_fire","Reference",1,0,0,0,10,0,0,0,1,1)

	
STake_Start("Common_Skill_BingDong","char_wait_006")	--变异怪冰冻技能NPC  40021  引导2
    BlendMode(0)
    BlendTime(0) 
    Fx(0.01,"NB_XSSL_skill_006_bingzhui","Reference",0.7,0,0,0,0,0,0,0,0.5,1)
    Fx(0.01,"NB_XSSL_skill_006","Reference",0.3,0,0,0,0,0,0,0,1.6,1)

	AttackStart()
	Hurt(2.00,"hurt_21")
	DirFx(2.00,"HG_THD_SW_skill_Bing",0,1)
    HurtBoneFx(2.00,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("Common_Skill_LuoLei","char_wait_006")    --变异怪黑落雷技能NPC  40023  引导2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    Fx(1.8,"N_HYHY02_skill_001_fire","Reference",1,0,0,28,0,0,0,0,1,1)

    AttackStart()
	Hurt(2.0,"hurt_21")
	HurtSound(2.0,"BTS_CR_HIT_new_e1")
	DirFx(2.0,"",0,1)
    HurtBoneFx(2.0,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)


STake_Start("Common_Skill_HuoHuan","char_wait_006")	--变异怪火环技能NPC  40025  引导2
    BlendMode(0)
    BlendTime(0) 
    Fx(2,"NB_HYZ_skill_004_fire_aoe","Reference",0.2,0,0,0,100,0,0,0,12,1)
    Fx(2,"NB_HYZ_skill_004_fire","Reference",2,0,0,0,-35,0,0,0,2,1)
    Fx(2,"NB_HYZ_skill_004_fire","Reference",2,0,0,0,-35,180,0,0,2,1)
    
    AttackStart()
    Hurt(2,"hurt_61")
    DirFx(2,"N_N1_XLGB01_skill_002_hit",0,1)	
    HurtBoneFx(2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("Common_Skill_ZiBao","char_wait_006")	--变异怪自爆技能NPC  40027  引导2
    BlendMode(0)
    BlendTime(0) 
    Fx(0.5,"SG_QZ_SW_skill_024_003_loop","Reference",3,0,0,0,0,0,0,0,1,1)
    Fx(1,"SG_QZ_SW_skill_024_003_loop","Reference",3,0,0,0,0,0,0,0,1,1)
    Fx(0.01,"SG_QZ_SW_skill_024_001","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(1.5,"SG_QZ_SW_skill_024_002","Reference",1.3,0,0,0,0,0,0,0,1,1)
    
    AttackStart()
    Hurt(2,"hurt_61")
    DirFx(2,"N_N1_XLGB01_skill_002_hit",0,1)	
    HurtBoneFx(2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)


STake_Start("Common_Skill_DongWuZiBao","char_wait_006")	--变异怪小动物自爆技能NPC 引导0.010
    BlendMode(0)
    BlendTime(0) 
    Fx(0.1,"FX_Common_Monster_baozha","Reference",2,0,0,0,-7,0,0,0,1,1)
    
    AttackStart()
    Hurt(0.1,"hurt_21")
    DirFx(0.1,"N_N1_XLGB01_skill_002_hit",0,1)	
    HurtBoneFx(0.1,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
---------------------------------------------------------------------------------------------------------------------------

STake_Start("Snow_ball","char_wait_006")   --雪球
    BlendMode(0)
    BlendTime(0) 
    
    Fx(0.01,"FX_SDJ_snowball","Reference",1,1,0,0,0,0,0,0,3.3)	
  
STake_Start("NPC_life_skill_snowball_004","char_wait_006")	--雪雷  16305
    BlendMode(0)
    BlendTime(0) 
    Fx(0.4,"FX_skill_Snowbattle_004_ground","Reference",1,1,0,0,0,0,0,0,1)
	Sound(0.33,"QZ_SW_Skill014_Start2")
	Sound(0.33,"SG_THD_SW_skill_010_Impact")
    
    AttackStart()
    Hurt(0,"hurt_71")
    DirFx(0,"FX_SDJ_snowball_hit",0,1)	
	
STake_Start("NPC_life_skill_zhadan_000","char_wait_006")	--炸弹
    BlendMode(0)
    BlendTime(0) 
    Fx(0.4,"FX_LT_HuoYaoTong_bao","Reference",1,1,0,0,0,0,0,0,1)
	Sound(0.33,"QZ_SW_Skill014_Start2")
	Sound(0.33,"SG_THD_SW_skill_010_Impact")
    
    AttackStart()
    Hurt(0,"hurt_71")
    DirFx(0,"FX_SDJ_snowball_hit",0,1)		

  
STake_Start("sand_char","char_wait_000")   --沙尘npc
    BlendMode(0)
    BlendTime(0) 
    Loop()
    
    Fx(0.01,"FX_common_shachen","Reference",1)
    
STake_Start("onfire_char","char_wait_000")   --帐篷着火npc
    BlendMode(0)
    BlendTime(0) 
    --Loop()

    AttackStart()
    Hurt(0,"hurt_21")
    
    Fx(0.01,"FX_common_onfire_fireloop","Reference")

STake_Start("box_A_dead_char","char_wait_006")   --箱子死亡
    BlendMode(0)
    BlendTime(0) 
    
    
    Fx(0.01,"Fx_GD_PR_box_A_dead","Reference",1)		

	
STake_Start("Npc_stone_char","char_wait_006")   --战场石头
    BlendMode(0)
    BlendTime(0) 
	
    Fx(0.01,"GD_buddha_BR_floor_falldrop","Reference",1,1,0,0,0)
    AttackStart()
    Hurt(1,"hurt_21")
	HurtBoneFx(1,"FX_blood_siyao_b","Spine1",1.2,0,0,0,0,0,0)	
	
STake_Start("area_char_05_35","char_wait_006")   --赵王府边界特效1
    BlendMode(0)
    BlendTime(0) 
    Loop()
    
    LoopFx(0.01, 10000, 1, "Fx_common_area_05_35","Reference",1,0,0,0,0,0,0)
    
STake_Start("area_char_05_75","char_wait_006")   --赵王府边界特效2
    BlendMode(0)
    BlendTime(0) 
    Loop()
    
    LoopFx(0.01, 10000, 1, "Fx_common_area_05_75","Reference",1,0,0,0,0,0,0)
    
STake_Start("area_char_05_200","char_wait_006")   --赵王府边界特效3
    BlendMode(0)
    BlendTime(0) 
    Loop()
    
    LoopFx(0.01, 10000, 1, "Fx_common_area_05_200","Reference",1,0,0,0,0,0,0)
    
STake_Start("area_char_05_420","char_wait_006")   --赵王府边界特效4
    BlendMode(0)
    BlendTime(0) 
    Loop()
    
    LoopFx(0.01, 10000, 1, "FX_common_area_05_420","Reference",1,0,0,0,0,0,0)	
	
STake_Start("area_char_05_820","char_wait_006")   --赵王府边界特效5
    BlendMode(0)
    BlendTime(0) 
    Loop()
    
    LoopFx(0.01, 10000, 1, "FX_common_area_05_820","Reference",1,0,0,0,0,0,0)

    
STake_Start("xunlu_end_char","char_wait_006")   --指示特效
    BlendMode(0)
    BlendTime(0) 
    
    Fx(0.01,"Fx_common_xunlu_end","Reference",1)		
    Fx(0.01,"Fx_common_xunlu_end","Reference",1)		
    Fx(0.01,"Fx_common_xunlu_end","Reference",1)	
    
STake_Start("patrolpoint_end","char_wait_000")		-- 寻路终点特效
    BlendMode(0)
    BlendTime(0) 
    Loop()
    Fx(0.01,"Fx_common_xunlu_end","Reference",1)		
    
STake_Start("patrolpoint_arrow","char_wait_000")	-- 寻路箭头特效
    BlendMode(0)
    BlendTime(0) 
    Loop()
    Fx(0.01,"Fx_common_xunlu_003","Reference",1)	

STake_Start("New_patrolpoint_arrow","char_wait_000")   --(新)寻路箭头特效
    BlendMode(0)
    BlendTime(0) 
    Loop()
    Fx(0.01,"Fx_common_xunlu_004","Reference",1,0,0,20,0,0,0,0,1)	
	
STake_Start("lifestream_holy_1","char_wait_000")	-- 宝箱提示特效
    BlendMode(0)
    BlendTime(0) 
    Loop()

    Fx(0.01,"HG_QZ_SW_QXQJ_001_buff","Reference",2, 0, 0, 0, -15)
    Fx(0.01,"Fx_MXD_Ancestral spirit_Baozha","Reference",0.16, 0, 0, 0, -120)
    Fx(0.01,"HG_QZ_PA_skill_009_Yin_hitloop","Reference",1.5, 0, 0, 0, 0)
    Fx(0.01,"Fx_MXD_Ancestral spirit_XS_poison","Reference",3, 0, 0, 0, 30)
    
 


-------------------------------蓝色区域特效（新）----------------------------------------
	 
 STake_Start("Fx_common_mission_Blue1","char_wait_002")  --问号NPC
	BlendMode(0)
	BlendTime(0)
	Loop()
	Fx(0,"FX_common_mission_public","Reference")

	
 STake_Start("NB_Shaman_skill_000_aoe_red","char_wait_005")  --红烟NPC
	BlendMode(0)
	BlendTime(0)
	Loop()
	Fx(0,"NB_Shaman_skill_000_aoe_red","Reference",1,0,0,0,0,0,0,0,1)
	
 STake_Start("N1_Shaman_B_skill_000_aoe_yellow","char_wait_005")  --黄烟NPC
	BlendMode(0)
	BlendTime(0)
	Loop()
	Fx(0,"N1_Shaman_B_skill_000_aoe_yellow","Reference",1,0,0,0,0,0,0,0,1)
	
 STake_Start("N1_Shaman_B_skill_000_aoe_purple","char_wait_005")  --紫烟NPC
	BlendMode(0)
	BlendTime(0)
	Loop()
	Fx(0,"N1_Shaman_B_skill_000_aoe_purple","Reference",1,0,0,0,0,0,0,0,1)
	
 STake_Start("Fx_common_smoke","char_wait_005")  --白烟NPC
	BlendMode(0)
	BlendTime(0)
	Loop()
	Fx(0,"Fx_common_smoke","Reference",1,0,0,0,0,0,0,0,1)
	
	
STake_Start("FX_common_itemlight","char_wait_000")      --拾取特效放大 
    BlendMode(0)
    BlendTime(0.0)
	Loop()
	Fx(0,"FX_common_itemlight","Reference",1,0,0,15,10)
	Fx(0,"FX_common_itemlight","Reference",1,0,0,-15,10)
	Fx(0,"FX_common_itemlight","Reference",1,0,0,0,10)
	

	
STake_Start("FX_common_firework01","char_wait_006")      --烟花
    BlendMode(0)
    BlendTime(0.0)
	Fx(0.2,"FX_common_firework01","Reference",0.5,1,0,0,0,0,0,0,1,1)	

	
STake_Start("FX_common_HPUP_fire","char_wait_006")      --通用任务用治疗特效fire
    BlendMode(0)
    BlendTime(0.0)
	Fx(0.1,"FX_HP_01","Reference",2,0,0,0,0,0,0,0,1,1)
	
	
STake_Start("GD_PR_fire1_A_dead","char_wait_006")      --火药桶爆炸
    BlendMode(0)
    BlendTime(0.0)
	Fx(0.1,"N_T1_JGQC_skill_000_hit","Reference",2,0,-4,10,0,0,0,0,1,1)
	Fx(0.15,"N_T1_JGQC_skill_000_hit","Reference",1,0,10,-3,10,0,0,0,1,1)
	Fx(0.2,"N_T1_JGQC_skill_000_hit","Reference",3,0,-3,0,5,0,0,0,1,1)
	Fx(0.25,"N_T1_JGQC_skill_000_hit","Reference",3.5,0,5,-5,10,0,0,0,1,1)

	
STake_Start("FX_archaeology_firework01","char_wait_006")      --挖宝普通的烟花
    BlendMode(0)
    BlendTime(0.0)
	Fx(0.2,"FX_common_firework01","Reference",0.8,1,0,-14,0,0,37,0,1,1)	

	
STake_Start("FX_archaeology_firework02","char_wait_006")      --挖宝漂亮的烟花
    BlendMode(0)
    BlendTime(0.0)
	Fx(0.2,"FX_common_firework01","Reference",0.8,1,0,-14,0,0,37,0,1,1)	
	Fx(0.4,"FX_common_firework01","Reference",0.7,1,0,-14,0,0,33,10,1,1)	
	Fx(0.6,"FX_common_firework01","Reference",0.7,1,0,-14,0,0,28,15,1,1)	
	Fx(0.8,"FX_common_firework01","Reference",0.7,1,0,-14,0,0,41,-8,1,1)	
	Fx(1.0,"FX_common_firework01","Reference",0.7,1,0,-14,0,0,35,-14,1,1)	

	
----------------------------------------------------挖宝指引特效-------------------------------------------
----------------------------------------------------挖宝指引特效-------------------------------------------
----------------------------------------------------挖宝指引特效-------------------------------------------

STake_Start("FX_Xunbaozhishi_01","char_wait_000")	-- 挖宝指示特效远
    BlendMode(0)
    BlendTime(0) 
    Loop()
    Fx(0.01,"FX_Xunbaozhishi_e","Reference",1)	


STake_Start("FX_Xunbaozhishi_02","char_wait_000")	-- 挖宝指示特效较远
    BlendMode(0)
    BlendTime(0) 
    Loop()
    Fx(0.01,"FX_Xunbaozhishi_d","Reference",1)	

	
STake_Start("FX_Xunbaozhishi_03","char_wait_000")	-- 挖宝指示特效中
    BlendMode(0)
    BlendTime(0) 
    Loop()
    Fx(0.01,"FX_Xunbaozhishi_c","Reference",1)	
	

STake_Start("FX_Xunbaozhishi_04","char_wait_000")	-- 挖宝指示特效较近
    BlendMode(0)
    BlendTime(0) 
    Loop()
    Fx(0.01,"FX_Xunbaozhishi_b","Reference",1)	
	
	
STake_Start("FX_Xunbaozhishi_05","char_wait_000")	-- 挖宝指示特效近 
    BlendMode(0)
    BlendTime(0) 
    Loop()
    Fx(0.01,"FX_Xunbaozhishi_a","Reference",1)	
	
	
STake_Start("N_DZ1_skill_001_npc","char_wait_006")   --挖宝哥布林烟幕
    BlendMode(0)
    BlendTime(0)
	FrameTime(0,6)
	Fx(0.01,"HG_THD_SW_skill_012_01_loop","Reference",1,0,0,0,0,0,0,0,1,3)

    AttackStart()
    Hurt(0,"hurt_21")
    DirFx(0,"",0,1)	
	
----------------------------------------------------圣诞节活动技能-------------------------------------------
STake_Start("N_SDLR_skill_002_NPC","char_wait_006")   --召唤驯鹿技能NPC  33467  引导1.2
    BlendMode(0)
    BlendTime(0)
	FrameTime(0,10)
	Fx(1.2,"N_SDLR_skill_002_fire","Reference",1,0,0,0,0,0,0,0,1,2)
	Sound(1.13,"Santa_SleighBells")
	Sound(1.53,"Santa_SleighBells")
	Sound(1.10,"HYHY_01_Skill01")

	AttackStart()
	Hurt(1.2,"hurt_81")
	HurtSound(1.2,"PA_Hit_001")
	DirFx(1.2,"Bear_skill_000_hit",0,1)
    HurtBoneFx(1.2,"FX_blood_jian_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.2,"ShakeDistance = 300","ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.08","ShakeAmplitudeX = 2","ShakeAmplitudeY = 6","ShakeAmplitudeZ = 2")

	
STake_Start("N_SDLR_skill_003_NPC","char_wait_006")   --天将礼物技能NPC  33469  引导一1.2  引导二6.2
    BlendMode(0)
    BlendTime(0)
	FrameTime(0,10)
	Fx(1.0,"N_SDLR_skill_003_fire1","Reference",1,0,0,0,0,0,0,0,1,2)
	Fx(6.2,"N_SDLR_skill_003_fire2","Reference",1,0,0,0,0,0,0,0,1,2)
	Sound(1.0,"HYHY_01_Skill01")
	Sound(6.2,"Lzw_Skill_04")
	Sound(6.2,"HYHY_01_Skill01")


	AttackStart()
	Hurt(1.2,"hurt_21")
	Hurt(6.2,"hurt_21")
	DirFx(1.2,"FX_xihun_hit",0,1)
	DirFx(6.2,"FX_xihun_hit",0,1)
    RangeCameraShake(6.2,"ShakeDistance = 300","ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.08","ShakeAmplitudeX = 2","ShakeAmplitudeY = 6","ShakeAmplitudeZ = 2")
package.path = package.path .. ";..\\char\\?.lua"
require("common")

STake_Start("C_wait_000","C_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_wait_001","C_wait_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_dead_001","C_dead_001")
    BlendMode(0)
    BlendTime(0.2) 	
	
STake_Start("C_wait_002","C_wait_002")  --卖花的小女孩吆喝动作
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_wait_003","C_wait_003")  --跪在地上的乞丐男孩
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_wait_003_emo_001","C_wait_003_emo_001")  --跪在地上的乞丐男孩乞讨
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_wait_003_emo_002","C_wait_003_emo_002")  --跪在地上的乞丐男孩左右观望
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_wait_004","C_wait_004")  --站着的乞丐孩子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_wait_004_emo_001","C_wait_004_emo_001")  --站着的乞丐孩子向人乞讨
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_wait_004_emo_002","C_wait_004_emo_002")  --站着的乞丐孩子跪下磕头起身
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_wait_004_move_001","C_wait_004_move_001")  --站着的乞丐孩子走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_bout_001","C_bout_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_move_001","C_move_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_move_002","C_move_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_move_003","C_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_move_005","C_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2)
	Priority(0,"5") 
    FrameTime(0,0.634)

STake_Start("C_move_006","C_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"7") 
    Loop()

STake_Start("C_move_007","C_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"6") 
    FrameTime(0.634,1.166)	
	
	
	
STake_Start("C_life_000","C_life_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_life_001","C_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_life_002","C_life_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_FRS_wait_000","C_FRS_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_FRS_emo_000","C_FRS_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_FRS_emo_001","C_FRS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_CYN_wait_000","C_CYN_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_CYN_emo_001","C_CYN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_CYN_emo_002","C_CYN_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_MGNvHai_wait_000","C_MGNvHai_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_MGNvHai_emo_001","C_MGNvHai_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_MGNvHai_emo_002","C_MGNvHai_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_MGNanHai_wait_000","C_MGNanHai_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("C_MGNanHai_emo_001","C_MGNanHai_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("C_MGNanHai_emo_002","C_MGNanHai_emo_002")
    BlendMode(0)
    BlendTime(0.2) 








------------------------------------------------------------------------------------------------------------------------------
--小源村CS用

STake_Start("CS_HuDie_C_XiaoNvHai01","N_C_CS_XiNH01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_HaiPa_C_XiaoNvHai01","N_C_CS_XNH01_emo_001(80-103)")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(2.667,3.433)

STake_Start("CS_HaiPa_loop_C_XiaoNvHai01","N_C_CS_XNH01_emo_001(80-103)")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(2.667,3.433)
    Loop()

STake_Start("CS_HaiPa_S_C_XiaoNvHai01","N_C_CS_XNH01_emo_001(80-103)")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.0001)
    Loop()

STake_Start("CS_BeiBao_C_XiaoNvHai01","N_C_CS_XNH01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_S_BeiBao_C_XiaoNvHai01","N_C_CS_XNH01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.0001)
    Loop()

STake_Start("CS_BeiBao_c_C_XiaoNvHai01","N_C_CS_XNH01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(3.2,8.333)
    Loop()

STake_Start("CS_BeiBao_c2_C_XiaoNvHai01","N_C_CS_XNH01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(2.7,8.333)
    Loop()

STake_Start("CS_BeiBao_c3_C_XiaoNvHai01","N_C_CS_XNH01_emo_002")
    BlendMode(0)
    BlendTime(0.3) 
    FrameTime(2,8.333)
    Loop()

STake_Start("CS_jump_start_BeiBao_C_XiaoNvHai01","N_C_CS_XNH01_jump_start")
    BlendMode(0)
    BlendTime(0.3) 
    Loop()

STake_Start("CS_jump_loop_BeiBao_C_XiaoNvHai01","N_C_CS_XNH01_jump_loop")
    BlendMode(0)
    BlendTime(0.3) 
    Loop()







------------------------------------------------------------------------------------------------------------------------------
--CS7场景预览
--程譞配
STake_Start("CS_C_CYN_emo_002","C_CYN_emo_002")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_C_FRS_emo_001","C_FRS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 



STake_Start("CS_C_CYN_emo_001","C_CYN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


------------------------
--牛家村开场CS
------------------------

STake_Start("CS_run_XiaoNanHai01","C_MGNanHai_run")
    BlendMode(0)
    BlendTime(0.3) 
    Loop()



STake_Start("CS_run_XiaoNvHai01","C_MGNvHai_run")
    BlendMode(0)
    BlendTime(0.3) 
    Loop()
-----------------------------------------------------------------------------------

--牛家村CUTSCENE用动作

--CS4金兵来袭

--张阳配

STake_Start("CS_NJC_jblx_emo1_C_life","C_CS_XNH01_jblx_emo1")
    BlendMode(0)
    BlendTime(0.3) 
    Loop()




		
STake_Start("Diaoluo","Diaoluo")
    BlendMode(0)
    BlendTime(0.2)  
    Loop()		
		
-- animation
Skeleton("PC_GB1")
Animation("HG")
Animation("HG_PA")
Animation("HG_life")
Animation("GB_ST")
Animation("GB_PA")
Animation("HIT")
Animation("HG_Arrow")
Animation("RideHorse_HG")
Animation("RideCamel_HG")
Animation("HG_FishingRod")
Animation("N1_life01")
Animation("N1_life02")
Animation("N1_life03")
Animation("THD_FL")
Animation("THD_SW")
Animation("SL_ST")
Animation("SL_SB")
Animation("HG_SL_ST")
Animation("QZ_SW")
Animation("QZ_PA")
Animation("RH_HG")
Animation("QZ_SW_NEW")
Animation("HG_CS02")
Animation("HG_CS_03")
Animation("HG_CS04")
Animation("HG_CS06")
STake("total")

-- paperdoll
PaperDollsTable("Paperdoll")
--Equip("PC_GB1_03A1_leg","",1)
--Equip("PC_GB1_03A1_head","",2)
--Equip("PC_GB1_03A1_body","PCM_008",3)


Dimension(19, 43)
BoundingBox(40,40,43,-40,-40,0,0,0,0)


-- blend pattern
BlendPattern("PC_male")

--FoldingBone("RightFoot",3.0,0,0)
--FoldingBone("LeftFoot",3.0,0,0)package.path = package.path .. ";..\\char\\?.lua"
require("common")

STake_Start("Horse_brake_004","Horse_brake_004")
    BlendMode(0)
    BlendTime(0.2) 
    
    
    GroundFx(0.096, "SFX_step", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(0.096, "SFX_step", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)

    Sound(0.096,"Horse_VOC_008")
    GroundSound(0.010,"Sound_brake")


STake_Start("Horse_brake_005","Horse_brake_005")
    BlendMode(0)
    BlendTime(0.2) 
    
    GroundFx(0.212, "SFX_stepon", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)   
    GroundFx(0.212, "SFX_stepon", "LeftFoot", 1.0, 0.0,  0,0,0,0,180,0)
    GroundFx(0.212, "SFX_stepon", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)        
    GroundFx(0.589, "SFX_stepon", "RightFoot", 1.0, 0.0,  0,0,0,0,-170,0)   
    GroundFx(0.77, "SFX_stepon", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0) 
    
    GroundFx(0.067, "SFX_step", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(0.077, "SFX_step", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)
    GroundFx(0.473, "SFX_step", "LeftFoot", 1.0, 0.0,  0,0,0,0,180,0)
    GroundFx(0.52, "SFX_step", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(0.67, "SFX_step", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)
    GroundFx(0.801, "SFX_step", "RightFoot", 1.0, 0.0,  0,0,0,0,-170,0)   
      
    Sound(0.038,"Horse_VOC_008")
    GroundSound(0.010,"Sound_brake")    
    

STake_Start("Horse_brake_006","Horse_brake_006")
    BlendMode(0)
    BlendTime(0.2)    
    
      
    GroundFx(0.1, "SFX_stepon", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(0.212, "SFX_stepon", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)   
    GroundFx(0.212, "SFX_stepon", "LeftFoot", 1.0, 0.0,  0,0,0,0,180,0)
    GroundFx(0.589, "SFX_stepon", "RightFoot", 1.0, 0.0,  0,0,0,0,-170,0)  
    GroundFx(0.77, "SFX_stepon", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)   
    
    GroundFx(0.077, "SFX_step", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(0.077, "SFX_step", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)
    GroundFx(0.473, "SFX_step", "RightFoot", 1.0, 0.0,  0,0,0,0,-170,0)
    GroundFx(0.52, "SFX_step", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)
    GroundFx(0.69, "SFX_step", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(0.801, "SFX_step", "LeftFoot", 1.0, 0.0,  0,0,0,0,180,0) 

    Sound(0.038,"Horse_VOC_008")
    GroundSound(0.010,"Sound_brake")
   
STake_Start("Horse_brake_007","Horse_brake_007")
    BlendMode(0)
    BlendTime(0.2) 
    
    GroundFx(0.501, "SFX_step", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(0.571, "SFX_step", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)
    GroundFx(0.501, "SFX_step", "LeftFoot", 1.0, 0.0,  0,0,0,0,180,0)
    GroundFx(0.501, "SFX_step", "RightFoot", 1.0, 0.0,  0,0,0,0,-170,0) 

    Sound(0.038,"Horse_VOC_000")
    GroundSound(0.010,"Sound_brake")

STake_Start("Horse_brake_008","Horse_brake_008")
    BlendMode(0)
    BlendTime(0.2) 
    
    GroundFx(0.326, "SFX_stepon", "LeftFoot", 1.0, 0.0,  0,0,0,0,180,0)    
    GroundFx(0.594, "SFX_stepon", "RightFoot", 1.0, 0.0,  0,0,0,0,-170,0)    
    
    GroundFx(0.151, "SFX_step", "LeftFoot", 1.0, 0.0,  0,0,0,0,180,0)
    GroundFx(0.151, "SFX_step", "RightFoot", 1.0, 0.0,  0,0,0,0,-170,0)  
    GroundFx(0.501, "SFX_step", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(0.711, "SFX_step", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)
    
    Sound(0.038,"Horse_VOC_000")
    GroundSound(0.010,"Sound_brake")    

STake_Start("Horse_brake_009","Horse_brake_009")
    BlendMode(0)
    BlendTime(0.2)     
    
    GroundFx(0.408, "SFX_stepon", "RightFoot", 1.0, 0.0,  0,0,0,0,-170,0)
    GroundFx(0.617, "SFX_stepon", "LeftFoot", 1.0, 0.0,  0,0,0,0,180,0)     
    
    GroundFx(0.151, "SFX_step", "LeftFoot", 1.0, 0.0,  0,0,0,0,180,0)
    GroundFx(0.151, "SFX_step", "RightFoot", 1.0, 0.0,  0,0,0,0,-170,0)  
    GroundFx(0.501, "SFX_step", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(0.676, "SFX_step", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)

    Sound(0.038,"Horse_VOC_000")
    GroundSound(0.010,"Sound_brake")    
    
STake_Start("Horse_emo_000","Horse_emo_000")
    BlendMode(0)
    BlendTime(0.2)     

    Sound(0.675,"Horse_VOC_004")    
    
STake_Start("Horse_emo_001","Horse_emo_001")
    BlendMode(0)
    BlendTime(0.2)     
    Fx(0.48, "Fx_horsefootprint", "Reference",1,1,-3,-10,0,0,0,0,1)  
    Fx(0.88, "Fx_horsefootprint", "Reference",1,1,3,-16,0,0,0,0,1)  
    Sound(0.613,"Horse_VOC_004")
    GroundSound(0.319,"Sound_step") 
    GroundSound(0.719,"Sound_step")  
    
STake_Start("Horse_emo_001_wait","Horse_emo_001")
    BlendMode(0)
    BlendTime(0.2)     
    Loop()
    Fx(0.48, "Fx_horsefootprint", "Reference",1,1,-3,-10,0,0,0,0,1)  
    Fx(0.88, "Fx_horsefootprint", "Reference",1,1,3,-16,0,0,0,0,1) 
    Sound(0.613,"Horse_VOC_004")
    GroundSound(0.319,"Sound_step") 
    GroundSound(0.719,"Sound_step")  
    
STake_Start("Horse_emo_002","Horse_emo_002")
    BlendMode(0)
    BlendTime(0.2)     
    Fx(1.08, "Fx_horsefootprint", "Reference",1,1,-3,-11,0,0,0,0,1)   
    Fx(2.52, "Fx_horsefootprint", "Reference",1,1,-3,-10,0,0,0,0,1) 
    GroundFx(1.08, "SFX_step", "RightHandThumb")
    GroundFx(2.52, "SFX_step", "RightHandThumb")	
    GroundSound(1.004,"Sound_step")
    GroundSound(2.400,"Sound_step")
	
	
STake_Start("Horse_emo_003","Horse_emo_003")
    BlendMode(0)
    BlendTime(0.2)     
    
    GroundFx(2.48, "SFX_step", "LeftHandThumb")
    GroundFx(2.54, "SFX_step", "RightHandThumb")
    Fx(2.48, "SFX_stepon", "Reference",1,1,3,-15,0,0,0,0,1)    
    Fx(2.54, "SFX_stepon", "Reference",1,1,-3,-10,0,0,0,0,1)

    Sound(0.391,"Horse_VOC_001")
    GroundSound(0.391,"Sound_jump") 
    GroundSound(2.416,"Sound_fall")  
    
STake_Start("Horse_skill_000","Horse_emo_003")
    BlendMode(0)
    BlendTime(0.2)     
    
    PlaySpeed(2,2.4,3) 
    --Fx(2.42,"N_T1_JGQC_skill_002_hit")
    --Fx(2.42,"SG_THD_SW_skill_000_new_fire2")
    Fx(2.25,"Fx_round_sand", "Reference", 1.3, 0,0,-7,0,0,0)
    --Fx(2.42,"NB_SHJL_B_wait_yan", "", 2, 0,0,-7,0,0,0)

    GroundFx(2.48, "SFX_step", "LeftHandThumb", 1.0, 0.0,  0,0,0,0,-60,0)
    GroundFx(2.54, "SFX_step", "RightHandThumb", 1.0, 0.0,  0,0,0,0,-55,0)

    Sound(0.391,"Horse_VOC_001")
    GroundSound(0.391,"Sound_jump") 
    GroundSound(2.416,"Sound_fall") 

    AttackStart()
  	Hurt(0.01,"hurt_81")

STake_Start("Horse_hit_000","Horse_hit_000")
    BlendMode(0)
    BlendTime(0.2)    


STake_Start("Horse_hit_001","Horse_hit_001")
    BlendMode(0)
    BlendTime(0.2) 

    Sound(0.021,"Horse_VOC_008") 
    
STake_Start("Horse_move_000","Horse_move_000")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()      
        
    Fx(0.16, "Fx_horsefootprint", "Reference",1,1,-3,10,0,0,0,0,1)   
    Fx(0.37, "Fx_horsefootprint", "Reference",1,1,-3,-20,0,0,0,0,1)
    Fx(1.1, "Fx_horsefootprint", "Reference",1,1,3,-20,0,0,0,0,1)
    Fx(0.76, "Fx_horsefootprint", "Reference",1,1,3,4,0,0,0,0,1)
    GroundFx(0.16, "SFX_step", "RightFoot",1,1,0,0,0,0,0,0,1)   
    GroundFx(0.37, "SFX_step", "RightHandThumb",1,1,0,0,0,0,0,0,1)
    GroundFx(1.1, "SFX_step", "LeftFoot",1,1,0,0,0,0,0,0,1)
    GroundFx(0.76, "SFX_step", "LeftHandThumb",1,1,0,0,0,0,0,0,1)
	
    GroundSound(0.293,"Sound_step")
    GroundSound(0.679,"Sound_step")
    GroundSound(0.906,"Sound_step")
    GroundSound(1.186,"Sound_step")
    
STake_Start("Horse_move_001","Horse_move_001")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()      
    GroundFx(0.55, "SFX_step", "RightHandThumb")    
    GroundFx(0.61, "SFX_step", "RightFoot")    
    GroundFx(0.99, "SFX_step", "LeftHandThumb")  
    GroundFx(0.99, "SFX_step", "LeftFoot")

    GroundSound(0.210,"Sound_step")
    GroundSound(0.440,"Sound_step")
    GroundSound(0.760,"Sound_step")
    GroundSound(0.960,"Sound_step")
    
STake_Start("Horse_move_002","Horse_move_002")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()      
    
    GroundFx(0.10, "SFX_step", "RightFoot")    
    GroundFx(0.33, "SFX_step", "RightHandThumb")
    GroundFx(0.71, "SFX_step", "LeftFoot")
    GroundFx(1.10, "SFX_step", "LeftHandThumb")

    GroundSound(0.293,"Sound_step")
    GroundSound(0.679,"Sound_step")
    GroundSound(0.906,"Sound_step")
    GroundSound(1.186,"Sound_step")
    
STake_Start("Horse_move_003","Horse_move_003")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()      
    
    GroundFx(0.10, "SFX_step", "RightFoot")    
    GroundFx(0.33, "SFX_step", "RightHandThumb")
    GroundFx(0.71, "SFX_step", "LeftFoot")
    GroundFx(1.10, "SFX_step", "LeftHandThumb")

    GroundSound(0.293,"Sound_step")
    GroundSound(0.679,"Sound_step")
    GroundSound(0.906,"Sound_step")
    GroundSound(1.186,"Sound_step")
    
STake_Start("Horse_move_004","Horse_move_004")
    BlendMode(1)
    BlendTime(0)     
    Loop()      
    
    Fx(0.22, "Fx_horsefootprint", "Reference",1,1,3,4,0,0,0,0,1)  
    Fx(0.3, "Fx_horsefootprint", "Reference",1,1,-3,0,0,0,0,0,1)
    Fx(0.36, "Fx_horsefootprint", "Reference",1,1,3,-30,0,0,0,0,1)  
    Fx(0.45, "Fx_horsefootprint", "Reference",1,1,3,-20,0,0,0,0,1)  
    GroundFx(0.02, "SFX_step", "LeftHandThumb")
    GroundFx(0.22, "SFX_step", "LeftFoot")
    GroundFx(0.28, "SFX_step", "RightFoot")   
    GroundFx(0.34, "SFX_step", "RightHandThumb")

    GroundSound(0.167,"Sound_run")
    --Sound(0.405,"Horse_VOC_run_000")    
    
STake_Start("Horse_move_005","Horse_move_005")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()      
    
    GroundFx(0.02, "SFX_step", "LeftHandThumb")
    GroundFx(0.22, "SFX_step", "LeftFoot")
    GroundFx(0.28, "SFX_step", "RightFoot")   
    GroundFx(0.34, "SFX_step", "RightHandThumb")

    GroundSound(0.167,"Sound_run")
    Sound(0.405,"Horse_VOC_run_000")
    
STake_Start("Horse_move_006","Horse_move_006")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()      

    GroundFx(0.02, "SFX_step", "LeftHandThumb")
    GroundFx(0.22, "SFX_step", "LeftFoot")
    GroundFx(0.28, "SFX_step", "RightFoot")   
    GroundFx(0.34, "SFX_step", "RightHandThumb")

    GroundSound(0.167,"Sound_run")
    Sound(0.405,"Horse_VOC_run_000")
    
STake_Start("Horse_move_007","Horse_move_007")
    BlendMode(1)
    BlendTime(0)     
    Loop()         

    Fx(0.02, "Fx_horsefootprint", "Reference",1,1,-3,-30,0,0,0,0,1)    
    Fx(0.242, "Fx_horsefootprint", "Reference",1,1,3,0,0,0,0,0,1)      
    Fx(0.31, "Fx_horsefootprint", "Reference",1,1,-3,3,0,0,0,0,1)   
    Fx(0.377, "Fx_horsefootprint", "Reference",1,1,3,-20,0,0,0,0,1)     
    
    GroundFx(0.02, "SFX_step", "RightHandThumb")    
    GroundFx(0.242, "SFX_step", "LeftFoot")        
    GroundFx(0.31, "SFX_step", "RightFoot")    
    GroundFx(0.377, "SFX_step", "LeftHandThumb")

	
    GroundSound(0.246,"Sound_run")
    --Sound(0.386,"Horse_VOC_run_000")
    
STake_Start("Horse_move_008","Horse_move_008")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()      
    
    GroundFx(0.018, "SFX_step", "RightHandThumb")    
    GroundFx(0.246, "SFX_step", "LeftFoot")        
    GroundFx(0.330, "SFX_step", "RightFoot")    
    GroundFx(0.363, "SFX_step", "LeftHandThumb")

    GroundSound(0.246,"Sound_run")
    Sound(0.386,"Horse_VOC_run_000")
    
STake_Start("Horse_move_009","Horse_move_009")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()      

    GroundFx(0.018, "SFX_step", "RightHandThumb")    
    GroundFx(0.246, "SFX_step", "LeftFoot")        
    GroundFx(0.330, "SFX_step", "RightFoot")    
    GroundFx(0.363, "SFX_step", "LeftHandThumb")

    GroundSound(0.246,"Sound_run")
    Sound(0.386,"Horse_VOC_run_000")
    
STake_Start("Horse_move_012","Horse_move_012")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()  
    
    Fx(0.66, "Fx_horsefootprint", "Reference",1,1,0,-12,0,0,0,0,1)
    Fx(1.1, "Fx_horsefootprint", "Reference",1,1,3,-12,0,0,0,0,1) 
    Fx(1.5, "Fx_horsefootprint", "Reference",1,1,3,-15,0,0,0,0,1) 
    GroundFx(0.66, "SFX_step", "LeftHandThumb")  
    GroundFx(1.1, "SFX_step", "RightHandThumb")
    GroundFx(1.5, "SFX_step", "LeftHandThumb")    

	
    GroundSound(0.582,"Sound_step")
    GroundSound(0.996,"Sound_step")
    GroundSound(1.456,"Sound_step") 
    
STake_Start("Horse_move_013","Horse_move_013")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()          
    
    Fx(0.64, "Fx_horsefootprint", "Reference",1,1,10,-10,0,0,0,0,1)
    Fx(1.165, "Fx_horsefootprint", "Reference",1,1,0,-8,0,0,0,0,1)
    Fx(1.5, "Fx_horsefootprint", "Reference",1,1,3,-15,0,0,0,0,1)  
    GroundFx(0.64, "SFX_step", "RightHandThumb")  
    GroundFx(1.165, "SFX_step", "RightHandThumb")
    GroundFx(1.5, "SFX_step", "LeftHandThumb")
 
	
    GroundSound(0.582,"Sound_step")
    GroundSound(0.996,"Sound_step")
    GroundSound(1.456,"Sound_step")
   
STake_Start("Horse_move_015_up","Horse_move_015") --前跳跃
    BlendTime(0.2)
    BlendMode(0)
    Sound(0.000,"PC_jump_normal")
      
    FrameTime(0,0.8)  

    GroundSound(0.010,"Sound_jump")
    Sound(0.010,"Horse_VOC_008")
  
STake_Start("Horse_move_015_down","Horse_move_015") --前跳跃
    BlendTime(0.2)
    BlendMode(0)
    Sound(0.000,"PC_jump_normal")
    
    FrameTime(0.8,1.799)
    
    GroundFx(0.89, "SFX_step", "LeftHandThumb")
    GroundFx(0.97, "SFX_step", "RightHandThumb")
    GroundFx(1.475, "SFX_step", "LeftFoot")  
    GroundFx(1.56, "SFX_step", "RightFoot")
    
    Fx(0.89, "Fx_horsefootprint", "Reference",1,1,-3,-20,0,0,0,0,1) 
    Fx(0.97, "Fx_horsefootprint", "Reference",1,1,3,18,0,0,0,0,1)   
    Fx(1.475, "Fx_horsefootprint", "Reference",1,1,-3,-12,0,0,0,0,1)   
    Fx(1.56, "Fx_horsefootprint", "Reference",1,1,3,-16,0,0,0,0,1)	
  
    GroundSound(0.774,"Sound_fall")
    GroundSound(1.386,"Sound_step")
            
STake_Start("Horse_move_015_wait","Horse_move_015_wait") --前跳跃
    BlendMode(0)
    BlendTime(0.2)     
    Loop()    
        
        
   
    
STake_Start("Horse_move_017_up","Horse_move_017") --左跳跃
    BlendTime(0.2)
    BlendMode(0)
    Sound(0.000,"PC_jump_normal")
      
    FrameTime(0,0.866)  

    GroundSound(0.010,"Sound_jump")
    Sound(0.010,"Horse_VOC_008")
  
STake_Start("Horse_move_017_down","Horse_move_017") --左跳跃
    BlendTime(0.2)
    BlendMode(0)
    Sound(0.000,"PC_jump_normal")
       
    FrameTime(0.866,1.800)      
    
    
    
    GroundFx(0.846, "SFX_step", "LeftHandThumb")
    GroundFx(0.882, "SFX_step", "RightHandThumb")
    GroundFx(0.99, "SFX_step", "LeftFoot")
    GroundFx(1.134, "SFX_step", "RightFoot")  
    GroundFx(1.466, "SFX_step", "RightHandThumb") 
    GroundFx(1.566, "SFX_step", "LeftHandThumb")

    
    GroundSound(0.774,"Sound_fall")
    GroundSound(1.386,"Sound_step")    
           
STake_Start("Horse_move_017_wait","Horse_move_017_wait")  --左跳跃
    BlendMode(0)
    BlendTime(0.2)     
    Loop()      
        
        
        
        
STake_Start("Horse_move_018_up","Horse_move_018") --右跳跃
    BlendTime(0.2)
    BlendMode(0)
    Sound(0.000,"PC_jump_normal")
      
    FrameTime(0,0.866)  

    GroundSound(0.010,"Sound_jump")
    Sound(0.010,"Horse_VOC_008")
  
STake_Start("Horse_move_018_down","Horse_move_018") --右跳跃
    BlendTime(0.2)
    BlendMode(0)
    Sound(0.000,"PC_jump_normal")
  
    FrameTime(0.866,1.800)         
    
    GroundFx(0.846, "SFX_step", "LeftHandThumb")
    GroundFx(0.882, "SFX_step", "RightHandThumb")
    GroundFx(0.99, "SFX_step", "LeftFoot")
    GroundFx(1.134, "SFX_step", "RightFoot")  
    GroundFx(1.466, "SFX_step", "RightHandThumb") 
    GroundFx(1.566, "SFX_step", "LeftHandThumb")


    GroundSound(0.774,"Sound_fall")
    GroundSound(1.386,"Sound_step")     
        
STake_Start("Horse_move_018_wait","Horse_move_018_wait") --右跳跃
    BlendMode(0)
    BlendTime(0.2)     
    Loop()            
        
        
STake_Start("Horse_move_23","Horse_move_23")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()    
 
    GroundFx(0.018, "SFX_step", "RightHandThumb")    
    GroundFx(0.246, "SFX_step", "LeftFoot")        
    GroundFx(0.330, "SFX_step", "RightFoot")    
    GroundFx(0.363, "SFX_step", "LeftHandThumb")

    GroundSound(0.205,"Sound_run")
    Sound(0.386,"Horse_VOC_run_000")
        
STake_Start("Horse_move_24","Horse_move_24")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()  

    GroundFx(0.018, "SFX_step", "RightHandThumb")    
    GroundFx(0.246, "SFX_step", "LeftFoot")        
    GroundFx(0.330, "SFX_step", "RightFoot")    
    GroundFx(0.363, "SFX_step", "LeftHandThumb")  

    GroundSound(0.205,"Sound_run")
    Sound(0.386,"Horse_VOC_run_000")

STake_Start("Horse_move_23_45degree","Horse_move_23")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()    

    GroundFx(0.018, "SFX_step", "RightHandThumb")    
    GroundFx(0.246, "SFX_step", "LeftFoot")        
    GroundFx(0.330, "SFX_step", "RightFoot")    
    GroundFx(0.363, "SFX_step", "LeftHandThumb")

    GroundSound(0.205,"Sound_run")
    Sound(0.386,"Horse_VOC_run_000")
        
STake_Start("Horse_move_24_45degree","Horse_move_24")
    BlendMode(1)
    BlendTime(0.2)           
    Loop()    
    
    GroundFx(0.018, "SFX_step", "RightHandThumb")    
    GroundFx(0.246, "SFX_step", "LeftFoot")        
    GroundFx(0.330, "SFX_step", "RightFoot")    
    GroundFx(0.363, "SFX_step", "LeftHandThumb")

    GroundSound(0.205,"Sound_run")
    Sound(0.386,"Horse_VOC_run_000")
        
STake_Start("Horse_wait_000","Horse_wait_000")
    BlendMode(0)
    BlendTime(0.2)     
    Loop()  

STake_Start("Horse_wait_001","Horse_wait_001")
    BlendMode(0)
    BlendTime(0.2)     
    Loop()  
        
STake_Start("Horse_wait_002","Horse_wait_002")
    BlendMode(0)
    BlendTime(0.2)     
    Loop()           
    
STake_Start("N_Horse_H01_emo_001","N_Horse_H01_emo_001")
    BlendMode(2)
    BlendTime(0.2)



STake_Start("N_Horse_H01_emo_002","N_Horse_H01_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("N_Horse_H01_emo_003","N_Horse_H01_emo_003")
    BlendMode(2)
    BlendTime(0.2)     
	

STake_Start("Monster_B_emo_001","GJ_Horse_CS_emo_004")
    BlendMode(0)
    BlendTime(0.2)

    
STake_Start("Horse_att_000","Horse_move_004")   --马群撞人
    BlendMode(0)
    BlendTime(0.2)      
    Priority(0,"5")     
    
    AttackStart()
  	Hurt(0.01,"hurt_81")





--CutScene用
-----------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS2黑豹惊马

--俞章廷配

STake_Start("CS_scared_Horse","RideHorse_CS_emo_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_scared_GJ_Horse","GJ_Horse_CS_emo_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_scared_TL_Horse","TL_Horse_CS_emo_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_scared&escape_Horse","Horse_move_004")
    BlendMode(2)
    BlendTime(0.2)
    PlaySpeed(0,0.466,1.3)
    Loop()

STake_Start("CS_run&turnleft_Horse","Horse_move_23")
    BlendMode(2)
    BlendTime(0.2)
    PlaySpeed(0,0.466,1.2)
    Loop()


STake_Start("CS_scared_down_GJ_Horse","GJ_Horse_CS_emo_004") -------------受惊动作落地
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.54,2.5) 
    Loop()

STake_Start("CS_scared_down_PC_Horse","RideHorse_CS_emo_004") -------------受惊动作落地
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(2.8,4.66) 
    Loop()


STake_Start("CS_scared_up_PC_Horse","RideHorse_CS_emo_004") 
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.8,2.8) 
    Loop()

STake_Start("CS_scared_down_TL_Horse","TL_Horse_CS_emo_004") -------------受惊动作落地
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.54,2.5) 
    Loop()



--CS5结拜

--单晓箫配


STake_Start("CS_stop1_Horse","Horse_brake_004")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_run1_Horse","Horse_move_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_walk1_Horse","Horse_move_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait1_Horse","Horse_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_emo1_Horse","Horse_emo_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_emo2_Horse","Horse_emo_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_emo3_Horse","Horse_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()



-----------------------------------------------------------------------------------
--塞外CUTSCENE用动作

--CS4大军冲

--单晓箫配


STake_Start("CS_wait_MHL_Horse","NB_MHL_CS_Horse_B_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_run_MHL_Horse","NB_MHL_CS_Horse_B_emo_002_loop")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_FangJian_MHL_Horse","NB_MHL_CS_Horse_B_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0.8,4.66)   
    Loop()

STake_Start("CS_attack_MHL_Horse","NB_MHL_CS_Horse_B_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_HuanHu_MHL_Horse","NB_MHL_CS_Horse_B_emo_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_Jia_MHL_Horse","NB_MHL_CS_Horse_B_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(3.5,6.9)   
    Loop()

STake_Start("CS_CaiRen1_MHL_Horse","Horse_brake_007")
    BlendMode(0)
    BlendTime(0.2)  
    Loop()


package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
    

STake_Start("L1_Hammer_B_move_003","L1_Hammer_B_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("L1_Hammer_B_move_004","L1_Hammer_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("L1_Hammer_B_wait_000","L1_Hammer_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","L1_Hammer_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Hammer_dead","L1_Hammer_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.311,"NB_KSK_dead_LuoDI")
    Sound(0.415,"NB_KSK_emo_001")
    Sound(0.727,"L1_LDPClaw_dead_000_VOI")
    Sound(2.564,"L1_LDPClaw_dead_DAODI")


STake_Start("L1_Hammer_dead_000","L1_Hammer_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.195,"NB_KSK_dead_LuoDI")
    Sound(0.570,"NB_KSK_emo_001")
    Sound(0.345,"L1_LDPClaw_dead_000_VOI")
    Sound(2.525,"L1_LDPClaw_dead_DAODI")

STake_Start("L1_Hammer_dead_001","L1_Hammer_dead_001")
    BlendMode(0)
    BlendTime(0.2)

   
    Sound(0.208,"NB_KSK_dead_LuoDI")
    Sound(0.221,"NB_KSK_emo_001")
    Sound(0.325,"L1_LDPClaw_dead_000_VOI")
    Sound(2.520,"L1_LDPClaw_dead_DAODI")

STake_Start("L1_Hammer_dead_002","L1_Hammer_dead_002")
    BlendMode(0)
    BlendTime(0.2)

       
    Sound(0.208,"NB_KSK_dead_LuoDI")
    Sound(0.221,"NB_KSK_emo_001")
    Sound(0.325,"L1_LDPClaw_dead_000_VOI")
    Sound(2.520,"L1_LDPClaw_dead_DAODI")


STake_Start("L1_Hammer_dead_003","L1_Hammer_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.208,"NB_KSK_dead_LuoDI")
    Sound(0.221,"NB_KSK_emo_001")
    Sound(0.325,"L1_LDPClaw_dead_000_VOI")
    Sound(2.520,"L1_LDPClaw_dead_DAODI")

STake_Start("L1_Hammer_def_000","L1_Hammer_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.173,"NB_KSK_def_000")
    Sound(0.173,"SZ03_def_000")

STake_Start("L1_Hammer_dodge_000","L1_Hammer_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.135,"SZ03_def_000")

STake_Start("L1_Hammer_emo_001","L1_Hammer_emo_001")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("L1_Hammer_emo_002","L1_Hammer_emo_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.435,"SZ03_def_000")
    Sound(0.812,"NB_KSK_emo_001")

STake_Start("L1_Hammer_move_000","L1_Hammer_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("L1_Hammer_wait_000","L1_Hammer_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()





























package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("L1_life")package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("L1_life")


STake_Start("L1_SBlade_B_wait_000","L1_SBlade_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("Monster_B_emo_001","L1_Maces_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("L1_SBlade_B_move_003","L1_SBlade_B_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    Sound(0.039,"Big_Move_1")
    Sound(0.706,"Big_Move_1")

STake_Start("L1_SBlade_B_move_004","L1_SBlade_B_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    Sound(0.010,"Big_Move_2")
    Sound(0.522,"Big_Move_2")

STake_Start("L1_SBlade_dead","L1_SBlade_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.084,"L1_SBlade_dead")

STake_Start("L1_SBlade_dead_000","L1_SBlade_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.086,"L1_SBlade_dead_000")

STake_Start("L1_SBlade_dead_001","L1_SBlade_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.084,"L1_SBlade_dead")

STake_Start("L1_SBlade_dead_002","L1_SBlade_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.064,"L1_SBlade_dead_002")

STake_Start("L1_SBlade_dead_003","L1_SBlade_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.086,"L1_SBlade_dead_000")

STake_Start("L1_SBlade_def_000","L1_SBlade_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.046,"L1_SBlade_def_000")

STake_Start("L1_SBlade_dodge_000","L1_SBlade_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.013,"L1_SBlade_dodge_000")

STake_Start("L1_SBlade_emo_001","L1_SBlade_emo_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.429,"L1_SBlade_emo_002_1")

STake_Start("L1_SBlade_emo_002","L1_SBlade_emo_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.605,"L1_SBlade_emo_002")
    Sound(1.540,"L1_SBlade_emo_002_1")
    Sound(2.805,"L1_SBlade_emo_002_1")
    Sound(4.235,"L1_SBlade_emo_002_2")

STake_Start("L1_SBlade_move_000","L1_SBlade_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    Sound(0.039,"Big_Move_1")
    Sound(0.706,"Big_Move_1")

STake_Start("L1_SBlade_wait_000","L1_SBlade_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()



--CutScene用
-----------------------------------------------------------------------------------

--塞外CUTSCENE用动作

--CS4大军冲

--单晓箫配

STake_Start("CS_ZhaoJi_L1_SBlade","L1_Maces_B_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_attack1_L1_SBlade","L1_SBlade_skill_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead_L1_SBlade","L1_SBlade_dead")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,2.8)   
    Loop(2.79999,2.8)

STake_Start("L1_Maces_B_wait_000","L1_Maces_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("L1_Maces_B_move_003","L1_Maces_B_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("L1_Maces_B_move_004","L1_Maces_B_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("L1_Maces_B_move_005","L1_Maces_B_move_005")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("L1_Maces_B_move_006","L1_Maces_B_move_006")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("L1_Maces_attack_000","L1_Maces_attack_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("L1_Maces_attack_001","L1_Maces_attack_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("L1_Maces_attack_002","L1_Maces_attack_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("L1_Maces_dead","L1_Maces_dead")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Maces_dead_000","L1_Maces_dead_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Maces_dead_001","L1_Maces_dead_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Maces_dead_002","L1_Maces_dead_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Maces_dead_003","L1_Maces_dead_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Maces_def_000","L1_Maces_def_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Maces_dodge_000","L1_Maces_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Maces_emo_001","L1_Maces_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Maces_emo_002","L1_Maces_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Maces_move_000","L1_Maces_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("L1_Maces_wait_000","L1_Maces_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("L1_Maces_skill_000","L1_Maces_skill_000")  --砸地板
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("L1_Maces_skill_001","L1_Maces_skill_001")  --顺劈
    BlendMode(0)
    BlendTime(0.2) 





--塞外CUTSCENE用动作

--CS4大军冲

--单晓箫配

STake_Start("CS_attack2_L1_SBlade","L1_SBlade_CS_aLLack_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_walk1_L1_SBlade","L1_SBlade_CS_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_walk2_L1_SBlade","L1_SBlade_CS_B_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_run1_L1_SBlade","L1_SBlade_CS_B_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait1_L1_SBlade","L1_SBlade_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")


STake_Start("L1_Unarmed_B_Move_003","L1_Unarmed_B_Move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("L1_Unarmed_B_Move_003_L","L1_Unarmed_B_Move_003_L")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("L1_Unarmed_B_Move_003_R","L1_Unarmed_B_Move_003_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("L1_Unarmed_B_move_004","L1_Unarmed_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("L1_Unarmed_B_move_005","L1_Unarmed_B_move_005")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("L1_Unarmed_B_Wait_000","L1_Unarmed_B_Wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","L1_Unarmed_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_bout_000","L1_Unarmed_bout_001")    --出生POSE
    BlendMode(0)
    BlendTime(0.2)

	
STake_Start("L1_Unarmed_dead","L1_Unarmed_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.140,"DaoDi_001")
	Sound(0.03,"HM_MaleVoc_03_Dead")
    
STake_Start("L1_Unarmed_dead_wait","L1_Unarmed_dead_001")
    BlendMode(0)
    BlendTime(0)
    FrameTime(1.31,1.32)

STake_Start("L1_Unarmed_dead_000","L1_Unarmed_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.639,"DaoDi_000")
	Sound(0.03,"HM_MaleVoc_03_Dead")

STake_Start("L1_Unarmed_dead_001","L1_Unarmed_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.479,"DaoDi_000")
	Sound(0.03,"HM_MaleVoc_03_Dead")

STake_Start("L1_Unarmed_dead_002","L1_Unarmed_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.493,"DaoDi_000")
	Sound(0.03,"HM_MaleVoc_03_Dead")

STake_Start("L1_Unarmed_dead_003","L1_Unarmed_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.493,"DaoDi_000")
	Sound(0.03,"HM_MaleVoc_03_Dead")

STake_Start("L1_Unarmed_def_000","L1_Unarmed_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.121,"L1_Unarmed_def_000")

STake_Start("L1_Unarmed_dodge_000","L1_Unarmed_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.100,"L1_Unarmed_dodge_000")

STake_Start("L1_Unarmed_emo_001","L1_Unarmed_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Unarmed_emo_002","L1_Unarmed_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Unarmed_link_000","L1_Unarmed_link_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.504,"L1_Unarmed_dodge_000")

STake_Start("L1_Unarmed_link_001","L1_Unarmed_link_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("L1_Unarmed_Move_000","L1_Unarmed_Move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("L1_Unarmed_Wait_000","L1_Unarmed_Wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("NB_TL_wait_000","NB_TL_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()


STake_Start("NB_TL_emo_000","NB_TL_emo_000")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("NB_TL_emo_001","NB_TL_emo_001")
    BlendMode(0)
    BlendTime(0.2)
	
---------------------------------------------表演动作--------------------------------------------------
---------------------------------------------表演动作--------------------------------------------------
---------------------------------------------表演动作--------------------------------------------------
STake_Start("L1_Unarmed_B_emo_001","L1_Unarmed_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()


STake_Start("L1_Unarmed_attack_000","L1_Unarmed_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
  	Fx(0,"L1_Unarmed_attack_001_fire","",1,0,0,0,0,0,0,0,2,1)
    Sound(0.36,"L1_Unarmed_dodge_000")
    Sound(0.36,"L1_Unarmed_attack_001")
	Sound(0.36,"HM_MaleVoc_03_attack02")


STake_Start("L1_Unarmed_attack_001","L1_Unarmed_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
  	Fx(0,"L1_Unarmed_attack_001_fire","",1,0,0,0,0,0,0,0,1.2,1)
    Sound(0.65,"L1_Unarmed_dodge_000")
    Sound(0.65,"L1_Unarmed_attack_001")
	Sound(0.65,"HM_MaleVoc_03_attack02")


STake_Start("L1_Unarmed_attack_002","L1_Unarmed_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
  	Fx(0,"L1_Unarmed_attack_002_fire","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.79,"T1_XLZZB_skill_001_fire","",1,0,0,0,0,0,0,0,1,1)
    Sound(0.65,"L1_Unarmed_dodge_000")
    Sound(0.65,"L1_Unarmed_attack_001")
	Sound(0.65,"HM_MaleVoc_03_attack02")


STake_Start("L1_Unarmed_attack_003","L1_Unarmed_attack_003")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
  	Fx(0.466,"N1_SBTR01_skill_001_hit","Reference",1,0,0,-40,20,0,0,0,1,1)
    Sound(0.65,"L1_Unarmed_dodge_000")
    Sound(0.65,"L1_Unarmed_attack_001")
	Sound(0.65,"HM_MaleVoc_03_attack02")
------------------------------------------------正式技能-----------------------------------------------
------------------------------------------------正式技能-----------------------------------------------
------------------------------------------------正式技能-----------------------------------------------


STake_Start("L1_Unarmed_skill_001_start","L1_Unarmed_skill_001")     --技能25624普通挥拳  打击点1.639  动作总时间2.733
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    FrameTime(0,2.733)
  	Fx(0.9,"L1_Unarmed_attack_001_fire")
	Fx(0.2,"FX_Warning","Head")
    Sound(1.639,"L1_Unarmed_dodge_000")
    Sound(1.475,"L1_Unarmed_attack_001")
	Sound(1.45,"HM_MaleVoc_03_attack02")

	
STake_Start("L1_Unarmed_skill_001_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     

    AttackStart()
  	Hurt(0.021,"hurt_21")
    DirFx(0.021,"L1_Unarmed_attack_hit",0,1)
    HurtBoneFx(0.021,"FX_blood_quan_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.021,"PAhit12")
	
STake_Start("L1_Unarmed_skill_002_start","L1_Unarmed_skill_002")    --技能25626双拳合握砸地  打击点1.631  动作总时间2.933
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,2.933)
    Pause(1.690,0.16,1,0.012)  	
  	Fx(0.9,"L1_Unarmed_attack_002_fire")
	Fx(1.671,"T1_XLZZB_skill_001_fire")
	Fx(0.2,"FX_Warning","Head")
    Sound(1.13,"L1_Unarmed_attack_002")
	Sound(1.05,"HM_MaleVoc_03_attack02")
	Sound(1.6,"SLNPC_ST_Skill010_Impact")
	
STake_Start("L1_Unarmed_skill_002_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    Pause(1.690,0.16,1,0.012)  		
  	
    AttackStart()
  	Hurt(0.04,"hurt_61")
    DirFx(0.04,"L1_Unarmed_skill_hit",0,1)
    HurtBoneFx(0.04,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.04,"PAhit12")


	
	
---------------------------------------------------吟唱时间减1/4红眼版本-----------------------------------------------------------			
	
STake_Start("L1_Unarmed_skill_001_level1_start","L1_Unarmed_skill_001")     --技能25624普通挥拳  打击点1.2  动作总时间2.324
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    FrameTime(0.409,2.733)	

  	Fx(0.9,"L1_Unarmed_attack_001_fire")
	Fx(0.609,"FX_Warning","Head")
	Sound(0.6,"HM_MaleVoc_03_attack01")
    Sound(1.639,"L1_Unarmed_dodge_000")
    Sound(1.475,"L1_Unarmed_attack_001")
	Sound(1.4,"HM_MaleVoc_03_attack02")

	
STake_Start("L1_Unarmed_skill_001_level1_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     

    AttackStart()
  	Hurt(0.021,"hurt_21")
    DirFx(0.021,"L1_Unarmed_attack_hit",0,1)
    HurtBoneFx(0.021,"FX_blood_quan_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.021,"PAhit12")
	
STake_Start("L1_Unarmed_skill_002_level1_start","L1_Unarmed_skill_002")    --技能25626双拳合握砸地  打击点1.214  动作总时间2.516
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.690,0.16,1,0.012)  	
    FrameTime(0.417,2.933)	
  	Fx(0.9,"L1_Unarmed_attack_002_fire")
	Fx(1.671,"T1_XLZZB_skill_001_fire")
	Fx(0.617,"FX_Warning","Head")
    Sound(1.13,"L1_Unarmed_attack_002")
	Sound(1.05,"HM_MaleVoc_03_attack02")
	Sound(1.6,"SLNPC_ST_Skill010_Impact")
	
STake_Start("L1_Unarmed_skill_002_level1_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.690,0.16,1,0.012)  	
  	
    AttackStart()
  	Hurt(0.04,"hurt_61")
    DirFx(0.04,"L1_Unarmed_skill_hit",0,1)
    HurtBoneFx(0.04,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.04,"PAhit12")

	
---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		

STake_Start("L1_Unarmed_skill_001_level2_start","L1_Unarmed_skill_001")     --技能25624普通挥拳  打击点0.819  动作总时间1.914
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.819,2.733)

  	Fx(0.9,"L1_Unarmed_attack_001_fire")
	Fx(1.019,"FX_Warning","Head")
	Sound(0.9,"HM_MaleVoc_03_attack01")
    Sound(1.639,"L1_Unarmed_dodge_000")
    Sound(1.475,"L1_Unarmed_attack_001")
	Sound(1.45,"HM_MaleVoc_03_attack02")

	
STake_Start("L1_Unarmed_skill_001_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     

    AttackStart()
  	Hurt(0.021,"hurt_21")
    DirFx(0.021,"L1_Unarmed_attack_hit",0,1)
    HurtBoneFx(0.021,"FX_blood_quan_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.021,"PAhit12")
	
STake_Start("L1_Unarmed_skill_002_level2_start","L1_Unarmed_skill_002")    --技能25626双拳合握砸地  打击点0.795  动作总时间2.098
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.690,0.16,1,0.012)  	
	FrameTime(0.835,2.933)
  	Fx(0.9,"L1_Unarmed_attack_002_fire")
	Fx(1.671,"T1_XLZZB_skill_001_fire")
	Fx(1.035,"FX_Warning","Head")
    Sound(1.13,"L1_Unarmed_attack_002")
	Sound(1.05,"HM_MaleVoc_03_attack02")
	Sound(1.6,"SLNPC_ST_Skill010_Impact")
	
STake_Start("L1_Unarmed_skill_002_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.690,0.16,1,0.012)  	
  	
    AttackStart()
  	Hurt(0.04,"hurt_61")
    DirFx(0.04,"L1_Unarmed_skill_hit",0,1)
    HurtBoneFx(0.04,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.04,"PAhit12")


---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	
	
STake_Start("L1_Unarmed_skill_001_level3_start","L1_Unarmed_skill_001")     --技能25624普通挥拳  打击点0.546  动作总时间1.641
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(1.092,2.733)	

  	Fx(1.11,"L1_Unarmed_attack_001_fire","",1,0,0,0,0,0,0,0,1.5)
	Fx(1.292,"FX_Warning","Head")
    Sound(1.639,"L1_Unarmed_dodge_000")
    Sound(1.475,"L1_Unarmed_attack_001")
	Sound(1.45,"HM_MaleVoc_03_attack02")

	
STake_Start("L1_Unarmed_skill_001_level3_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     

    AttackStart()
  	Hurt(0.021,"hurt_21")
    DirFx(0.021,"L1_Unarmed_attack_hit",0,1)
    HurtBoneFx(0.021,"FX_blood_quan_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.021,"PAhit12")
	
STake_Start("L1_Unarmed_skill_002_level3_start","L1_Unarmed_skill_002")    --技能25626双拳合握砸地  打击点0.517  动作总时间1.819
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.690,0.16,1,0.012)  	
	FrameTime(1.114,2.933)
  	Fx(1.114,"L1_Unarmed_attack_002_fire","",1,0,0,0,0,0,0,0,1.3)
	Fx(1.671,"T1_XLZZB_skill_001_fire")
	Fx(1.314,"FX_Warning","Head")
    Sound(1.13,"L1_Unarmed_attack_002")
	Sound(1.14,"HM_MaleVoc_03_attack02")
	Sound(1.6,"SLNPC_ST_Skill010_Impact")
	
STake_Start("L1_Unarmed_skill_002_level3_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.690,0.16,1,0.012)  	
  	
    AttackStart()
  	Hurt(0.04,"hurt_61")
    DirFx(0.04,"L1_Unarmed_skill_hit",0,1)
    HurtBoneFx(0.04,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.04,"PAhit12")


---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
STake_Start("Common_skill_jumpB_80","L1_Unarmed_skill_jumpB")  --技能 40009 近战通用后跳  引导0.01charge0.4  动作总长0.899
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,0.899)
	Fx(0.4,"FX_common_skill_jumpB_02","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.08,"char_05_cloth_flap_02")	
	Sound(0.38,"SZ03_dodge_000")	

	
STake_Start("Common_skill_jumpF","L1_Unarmed_skill_jumpF")  --技能 40001 通用追击  引导一0.01charge0.4  引导二0.46  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,1.2)
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
    Fx(0.41,"FX_common_skill_jumpF","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.28,"TY_Attack_011")
	
  	AttackStart()
  	Hurt(0.46,"hurt_21")    
    DirFx(0.46,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.46,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.46,"HMW01_Hit_002")
    RangeCameraShake(0.46,"ShakeDistance = 80","ShakeMode = 3","ShakeTimes = 0.14","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")



STake_Start("Common_skill_charge_120","L1_Unarmed_skill_attack")   --技能  40004  通用charge技能120距离  引导1.6charge0.2  动作总长2.666
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,2.666)
	Fx(1.4,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.61,"TY_Attack_009")
 
    AttackStart()
  	Hurt(1.7,"hurt_21")
    DirFx(1.7,"Bear_skill_000_hit",0,1)
    HurtBoneFx(1.7,"FX_blood_quan_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(1.7,"Big_blade_Hit")


STake_Start("Common_skill_charge_120_level1","L1_Unarmed_skill_attack")   --技能  40004  通用charge技能120距离  引导1.2charge0.2  动作总长2.266
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.4,2.666)
	Fx(1.4,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.6,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.61,"TY_Attack_009")
 
    AttackStart()
  	Hurt(1.3,"hurt_21")
    DirFx(1.3,"Bear_skill_000_hit",0,1)
    HurtBoneFx(1.3,"FX_blood_quan_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(1.3,"Big_blade_Hit")


STake_Start("Common_skill_charge_120_level2","L1_Unarmed_skill_attack")   --技能  40004  通用charge技能120距离  引导0.8charge0.2  动作总长1.866
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.8,2.666)
	Fx(1.4,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
	Fx(1,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.61,"TY_Attack_009")
 
    AttackStart()
  	Hurt(0.9,"hurt_21")
    DirFx(0.9,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.9,"FX_blood_quan_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.9,"Big_blade_Hit")












package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("N1_life")

STake_Start("Monster_B_emo_001","N1_Bow_B_emo_001")  --战斗嘲讽
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_def_000","N1_Bow_def_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_dodge_000","N1_Bow_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_B_wait_000","N1_Bow_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_dead","N1_Bow_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.420,"T1_Bow_dead_002")
    Sound(1.360,"DaoDi_000")
    Sound(0.351,"ZhadalanGongbing_004")

STake_Start("Monster_dead_000","N1_Bow_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.666,"T1_Bow_dead_002")
    Sound(0.400,"DaoDi_000")
    Sound(0.233,"ZhadalanGongbing_004")

STake_Start("Monster_dead_001","N1_Bow_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.386,"DaoDi_000")
    Sound(0.813,"DaoDi_000")
    Sound(0.813,"T1_Bow_dead_002")
    Sound(0.213,"ZhadalanGongbing_004")

STake_Start("Monster_dead_002","N1_Bow_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.476,"DaoDi_000")
    Sound(0.700,"T1_Bow_dead_002")
    Sound(0.135,"ZhadalanGongbing_004")

STake_Start("Monster_dead_003","N1_Bow_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.453,"DaoDi_000")
    Sound(0.533,"T1_Bow_dead_002")
    Sound(0.144,"ZhadalanGongbing_004")

STake_Start("Monster_emo_001","N1_Bow_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_emo_002","N1_Bow_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_link_000","N1_Bow_link_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_link_001","N1_Bow_link_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_move_000","N1_Bow_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_move_003","N1_Bow_move_003")  --战斗走
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_move_003_L","N1_Bow_move_003_L")  --战斗左走
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_move_003_R","N1_Bow_move_003_R")  --战斗右走
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_move_000_L","N1_Bow_move_003_L")  --左走
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_move_000_R","N1_Bow_move_003_R")  --右走
    BlendMode(0)
    BlendTime(0.2)
    Loop()	
	
STake_Start("Monster_B_move_004","N1_Bow_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_jump_start","N1_Bow_jump_start")  --前跳起跳
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_jump_loop","N1_Bow_jump_loop")  --前跳loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_down","N1_Bow_jump_down")  --前跳落地
    BlendMode(0)
    BlendTime(0.2)	

STake_Start("Monster_wait_000","N1_Bow_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_bout_000","N_N1_XLGB01_bout_001")   --出生
    BlendMode(0)
    BlendTime(0.2) 
	

STake_Start("N1_Bow_skill_000_start","N1_Bow_skill_000")   ----技能 27006  吟唱射击   吟唱1.64    动作总长2.333
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")     
    FrameTime(0,2.333)
    Fx(0,"FX_M_att_charge_YC")
    Fx(0.5,"FX_M_att_charge_YC")
    Sound(0.55,"PC_LaGong_long")
	Sound(1.5,"PC_Gong_Shot")
    LineFx(0.1,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,28,0,0,0,0.60,1)	
	
	LineFx(1.54,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)

	
STake_Start("N1_Bow_skill_000_fire","")
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")     
    
    AttackStart()
	Hurt(0,"hurt_21")
    HurtSound(0,"Big_blade_Hit_002_1")
    AttackHurtFx(0, "N1_Dart_skill_001_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)



	
STake_Start("N1_Bow_skill_001","N1_Bow_skill_000")   --射箭技能  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    FrameTime(0,2.333)
    BlendPattern("no_sync")
    LineFx(0.01,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,0,0,0,0,0.66,1)	
    
    --Hard(0.00,2.3)
  	
    AttackStart()
  	Hurt(1.509,"hurt_11")
  	Hurt(1.509,"hurt_11")

  	--Fx(0.000,"")
    --DragFx(1.509,0.1,"Bow_qiyan_skill_000_tracker","Bow_qiyan_skill_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0)
    --DragFx(1.509,0.1,"Bow_qiyan_skill_000_tracker","Bow_qiyan_skill_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0) 
    --DirFx(1.563,"Bow_qiyan_skill_hit",0,1) 
    --DirFx(1.563,"Bow_qiyan_skill_hit",0,1) 

    Sound(0.723,"T1_Bow_skill_000")
    HurtSound(1.609,"T1_Bow_hit")


STake_Start("N1_Bow_skill_002_start","N1_Bow_skill_002")  --跳跃反击  27008
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,2.666)
    Fx(0,"FX_M_att_charge_YC")
    Fx(0.3,"FX_M_att_charge_YC")
    Sound(0.27,"PC_LaGong_long")
	Sound(1.14,"PC_Gong_Shot")

    LineFx(0.01,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,0,0,0,0,1.33,1)	
    Fx(1.14,"FX_Archery_Tracker","Reference",1,0,0,-25,22,0,-20)	
    Charge(0.83,0.31,120,180,4)
	
STake_Start("N1_Bow_skill_002_fire","")  --跳跃反击  27008
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	
    AttackStart()
	Hurt(0,"hurt_21")
    HurtSound(0,"Big_blade_Hit_002_1")
    AttackHurtFx(0, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,-22,0)
	

---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		
	

STake_Start("N1_Bow_skill_000_level2_start","N1_Bow_skill_000")   --技能 27006  吟唱射击   吟唱0.82    动作总长1.513
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.82,2.333)
    Fx(0.82,"FX_M_att_charge_YC","",1,1,0,0,0,0,0,0,1.5)
    Sound(0.55,"PC_LaGong_long")
	Sound(1.5,"PC_Gong_Shot")
    LineFx(0.82,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,28,0,0,0,1.21,1)	
	LineFx(1.02,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)


STake_Start("N1_Bow_skill_000_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    
    AttackStart()
	Hurt(0,"hurt_21")
    HurtSound(0,"Big_blade_Hit_002_1")
    AttackHurtFx(0, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N1_Bow_skill_002_level2_start","N1_Bow_skill_002")  --技能 27008  跳跃反击    吟唱0.57    动作总长2.096
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.57,2.666)
    Fx(0.57,"FX_M_att_charge_YC","",1,1,0,0,0,0,0,0,2)
    Sound(0.27,"PC_LaGong_long")
	Sound(1.14,"PC_Gong_Shot")

    LineFx(0.57,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,0,0,0,0,1.75,1)	
    Fx(1.14,"FX_Archery_Tracker","Reference",1,0,0,-25,22,0,-20)	
    Charge(0.83,0.31,120,180,4)
	
STake_Start("N1_Bow_skill_002_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	
    AttackStart()
	Hurt(0,"hurt_21")
    HurtSound(0,"Big_blade_Hit_002_1")
    AttackHurtFx(0, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,-22,0)

	
---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	
	

STake_Start("N1_Bow_skill_000_level3_start","N1_Bow_skill_000")   --技能 27006  吟唱射击   吟唱0.546    动作总长1.239
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.094,2.333)
    Fx(1.094,"FX_M_att_charge_YC","",1,1,0,0,0,0,0,0,2.5)
    Sound(0.55,"PC_LaGong_long")
	Sound(1.5,"PC_Gong_Shot")
    LineFx(1.094,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,28,0,0,0,1.83,1)	
	LineFx(1.54,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)
	
STake_Start("N1_Bow_skill_000_level3_fire","")
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")
    
    AttackStart()
	Hurt(0,"hurt_21")
    HurtSound(0,"Big_blade_Hit_002_1")
    AttackHurtFx(0, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N1_Bow_skill_002_level3_start","N1_Bow_skill_002")  --技能 27008  跳跃反击    吟唱0.38    动作总长1.906
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.76,2.666)
    Fx(0.76,"FX_M_att_charge_YC","",1,1,0,0,0,0,0,0,2.3)
    Sound(0.27,"PC_LaGong_long")
	Sound(1.14,"PC_Gong_Shot")

    LineFx(0.76,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,0,0,0,0,2.63,1)	
    Fx(1.14,"FX_Archery_Tracker","Reference",1,0,0,-25,22,0,-20)	
    Charge(0.83,0.31,120,180,4)
	
STake_Start("N1_Bow_skill_002_level3_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	
    AttackStart()
	Hurt(0,"hurt_21")
    HurtSound(0,"Big_blade_Hit_002_1")
    AttackHurtFx(0, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,-22,0)


--------------------------------------------------------------------西辽弓兵---------------------------------------------------------------------
--------------------------------------------------------------------西辽弓兵---------------------------------------------------------------------
--------------------------------------------------------------------西辽弓兵---------------------------------------------------------------------


STake_Start("N_N1_XLGB01_skill_001","N_N1_XLGB01_skill_001")   ----技能  32106  射箭   吟唱3.04    动作总长3.8
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")     
	FrameTime(0,3.8)
    --Fx(0,"Fx_Aim_Line","",1,0,0,0,30,0,0,0,0.32)
    Fx(0,"N_N1_XLGB01_skill_001_start","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(0.04,"N_N1_XLGB01_skill_001_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(1.04,"N_N1_XLGB01_skill_001_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.002,"N_N1_XLGB01_skill_001_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.964,"N_N1_XLGB01_skill_001_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    LineFx(0,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,30,0,0,0,0.32,1)
	LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,0,30)
	Sound(0.87,"PC_LaGong_long")
	Sound(3.05,"PC_Gong_Shot")
    
    AttackStart()
	Hurt(3.04,"hurt_21")
    HurtSound(3.04,"Big_blade_Hit_002_1")
    AttackHurtFx(3.04, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(3.04,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)

	
STake_Start("N_N1_XLGB01_skill_001_2","N_N1_XLGB01_skill_001")   ----技能  32108  火箭   吟唱3.04    动作总长3.8
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")     
	FrameTime(0,3.8)
    --Fx(0,"Fx_Aim_Line","",1,0,0,0,30,0,0,0,0.32)
    Fx(0,"N_N1_XLGB01_skill_001a_start","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(0.04,"N_N1_XLGB01_skill_001a_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(1.04,"N_N1_XLGB01_skill_001a_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.002,"N_N1_XLGB01_skill_001a_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.964,"N_N1_XLGB01_skill_001a_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    LineFx(0,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,30,0,0,0,0.32,1)
	LineFx(3.04,"N_N1_XLGB01_skill_001a_Tracker",0,"Reference", "Reference",1,800,0,0,30)
	Sound(0.87,"PC_LaGong_long")
	Sound(0.01,"FX_Dead_Brun_Loop1")
	Sound(0.87,"FX_Dead_Brun_Loop1")
	Sound(1.87,"FX_Dead_Brun_Loop1")
	Sound(2.87,"FX_Dead_Brun_Loop1")
	Sound(3.05,"FX_Dead_Brun_End")
	Sound(3.05,"PC_Gong_Shot")
    
    AttackStart()
	Hurt(3.04,"hurt_21")
    HurtSound(3.04,"Big_blade_Hit_002_1")
    AttackHurtFx(3.04, "N_N1_XLGB01_skill_002_hit", "Spine1",1)	
    HurtBoneFx(3.04,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N_N1_XLGB01_skill_002","N_N1_XLGB01_skill_002")   ----技能  32110  H箭雨   吟唱3.04    动作总长3.8
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")     
	FrameTime(0,3.8)
    Fx(0,"N_N1_XLGB01_skill_002_start","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(0.04,"N_N1_XLGB01_skill_002_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(1.04,"N_N1_XLGB01_skill_002_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.002,"N_N1_XLGB01_skill_002_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.964,"N_N1_XLGB01_skill_002_fire","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(0.87,"PC_LaGong_long")
	Sound(0.01,"FX_Dead_Brun_Loop1")
	Sound(0.87,"FX_Dead_Brun_Loop1")
	Sound(1.87,"FX_Dead_Brun_Loop1")
	Sound(2.87,"FX_Dead_Brun_Loop1")
	Sound(3.05,"FX_Dead_Brun_End")
	Sound(3.05,"PC_Gong_Shot")


STake_Start("N1_Bow_skill_003","N1_Bow_skill_003")  --技能  32109  散射  引导3  动作总长3.799
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,3.799)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Fx(0,"N1_Bow_skill_003_start","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(3,"N1_Bow_skill_003_fire","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(0.87,"PC_LaGong_long")
	Sound(2.95,"PC_Gong_Shot")
	Sound(3.05,"XMS_Skill_001_2")

    AttackStart()
	Hurt(3,"hurt_21")
    HurtSound(3,"Big_blade_Hit_002_1")
    DirFx(3,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(3,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N1_Bow_skill_004","N1_Bow_skill_004")  --技能  32107  射箭  引导2  动作总长2.9
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,2.9)
    Fx(0,"N1_Bow_skill_004_start","Reference",1,0,0,0,0,0,0,0,1.05,1)
    --Fx(1.943,"N1_Bow_skill_004_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    --Fx(0,"Fx_Aim_Line","Reference",1,0,0,0,20,0,0,0,0.5,1)
    LineFx(0,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,20,0,0,0,0.5,1)
	LineFx(1.943,"N1_Bow_skill_004_fire",0,"Reference", "Reference",1,800,0,0,0)
	Sound(0.74,"PC_LaGong_long")
	Sound(0.52,"MQX_Skill_000_1")
	Sound(1.95,"PC_Gong_Shot")

    AttackStart()
	Hurt(2,"hurt_21")
    HurtSound(2,"Big_blade_Hit_002_1")
    DirFx(2,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(2,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
	

STake_Start("N1_Bow_skill_005","N1_Bow_skill_005")  --技能  32105  斗气风暴  引导2  动作总长3.666
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,3.666)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Fx(0,"N1_Bow_skill_005_start","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(1.979,"N1_Bow_skill_005_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    Sound(0.03,"Bbsl_Skill_start")
	Sound(1.9,"Lzw_Skill_04")
	Sound(1.84,"HM_MaleVoc_02_attack02")

    AttackStart()
	Hurt(2,"hurt_21")
    HurtSound(2,"Big_blade_Hit_002_1")
    DirFx(2,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

---------------------------------------------------吟唱时间减1/4红眼版本-----------------------------------------------------------		
STake_Start("N_N1_XLGB01_skill_001_level1","N_N1_XLGB01_skill_001")   ----技能  32106  射箭   吟唱1.2    动作总长1.96
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")     
	FrameTime(1.84,3.8)
    Fx(1.84,"N_N1_XLGB01_skill_001_start","Reference",1,0,0,0,0,0,0,0,1.3,1)
    Fx(1.84,"N_N1_XLGB01_skill_001_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.002,"N_N1_XLGB01_skill_001_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.964,"N_N1_XLGB01_skill_001_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    LineFx(1.84,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,30,0,0,0,0.833,1)
	LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,0,30)
	Sound(1.85,"PC_LaGong_long")
	Sound(3.05,"PC_Gong_Shot")
    
    AttackStart()
	Hurt(1.3,"hurt_21")
    HurtSound(1.3,"Big_blade_Hit_002_1")
    AttackHurtFx(1.3, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(1.3,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N_N1_XLGB01_skill_001_2_level1","N_N1_XLGB01_skill_001")   ----技能  32108  火箭   吟唱1.2    动作总长1.96
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")     
	FrameTime(1.84,3.8)
    Fx(1.84,"N_N1_XLGB01_skill_001a_start","Reference",1,0,0,0,0,0,0,0,1.3,1)
    Fx(1.84,"N_N1_XLGB01_skill_001a_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.002,"N_N1_XLGB01_skill_001a_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.964,"N_N1_XLGB01_skill_001a_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    LineFx(1.84,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,30,0,0,0,0.833,1)
	LineFx(3.04,"N_N1_XLGB01_skill_001a_Tracker",0,"Reference", "Reference",1,800,0,0,30)
	Sound(1.84,"PC_LaGong_long")
	Sound(1.87,"FX_Dead_Brun_Loop1")
	--Sound(2.87,"FX_Dead_Brun_Loop1")
	Sound(3.05,"FX_Dead_Brun_End")
	Sound(3.05,"PC_Gong_Shot")
    
    AttackStart()
	Hurt(1.3,"hurt_21")
    HurtSound(1.3,"Big_blade_Hit_002_1")
    AttackHurtFx(1.3, "N_N1_XLGB01_skill_002_hit", "Spine1",1)	
    HurtBoneFx(1.3,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)



STake_Start("N_N1_XLGB01_skill_002_level1","N_N1_XLGB01_skill_002")   ----技能   H箭雨  32110  吟唱2.28    动作总长3.04
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.76,3.8)	
    Fx(0.76,"N_N1_XLGB01_skill_002_start","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(0.76,"N_N1_XLGB01_skill_002_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(1.04,"N_N1_XLGB01_skill_002_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.002,"N_N1_XLGB01_skill_002_loop","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(2.964,"N_N1_XLGB01_skill_002_fire","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(0.87,"PC_LaGong_long")
	Sound(0.87,"FX_Dead_Brun_Loop1")
	Sound(1.87,"FX_Dead_Brun_Loop1")
	Sound(2.87,"FX_Dead_Brun_Loop1")
	Sound(3.05,"FX_Dead_Brun_End")
	Sound(3.05,"PC_Gong_Shot")


STake_Start("N1_Bow_skill_003_level1","N1_Bow_skill_003")  --技能  32109  散射  引导1.2  动作总长1.999
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(1.8,3.799)
	Fx(2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Fx(1.8,"N1_Bow_skill_003_start","Reference",1,0,0,0,0,0,0,0,2.5,1)
    Fx(3,"N1_Bow_skill_003_fire","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(1.81,"PC_LaGong_long")
	Sound(2.95,"PC_Gong_Shot")
	Sound(3.05,"XMS_Skill_001_2")

    AttackStart()
	Hurt(1.2,"hurt_21")
    HurtSound(1.2,"Big_blade_Hit_002_1")
    DirFx(1.2,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(1.2,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
	

STake_Start("N1_Bow_skill_004_level1","N1_Bow_skill_004")  --技能  32107  射箭  引导1.2  动作总长2.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0.8,2.9)
    Fx(0.8,"N1_Bow_skill_004_start","Reference",1,0,0,0,0,0,0,0,1.7,1)
    LineFx(0.8,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,20,0,0,0,0.83,1)
	LineFx(1.943,"N1_Bow_skill_004_fire",0,"Reference", "Reference",1,800,0,0,0)
	Sound(0.81,"PC_LaGong_long")
	Sound(0.81,"MQX_Skill_000_1")
	Sound(1.95,"PC_Gong_Shot")

    AttackStart()
	Hurt(1.2,"hurt_21")
    HurtSound(1.2,"Big_blade_Hit_002_1")
    DirFx(1.2,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(1.2,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("N1_Bow_skill_005_level1","N1_Bow_skill_005")  --技能  32105  斗气风暴  引导1.5  动作总长3.166
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0.5,3.666)
	Fx(0.7,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Fx(0.5,"N1_Bow_skill_005_start","Reference",1,0,0,0,0,0,0,0,1.4,1)
    Fx(1.979,"N1_Bow_skill_005_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    Sound(0.51,"Bbsl_Skill_start")
	Sound(1.9,"Lzw_Skill_04")
	Sound(1.84,"HM_MaleVoc_02_attack02")

    AttackStart()
	Hurt(1.5,"hurt_21")
    HurtSound(1.5,"Big_blade_Hit_002_1")
    DirFx(1.5,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(1.5,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)

---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------	
STake_Start("N_N1_XLGB01_skill_001_level2","N_N1_XLGB01_skill_001")   ----技能  32106  射箭   吟唱0.8    动作总长1.56
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")     
	FrameTime(2.24,3.8)
    Fx(2.24,"N_N1_XLGB01_skill_001_start","Reference",1,0,0,0,0,0,0,0,1.6,1)
    Fx(2.24,"N_N1_XLGB01_skill_001_loop","Reference",1,0,0,0,0,0,0,0,2,1)
    Fx(2.964,"N_N1_XLGB01_skill_001_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    LineFx(2.24,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,30,0,0,0,1.2,1)
	LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,0,30)
	Sound(2.25,"PC_LaGong_long")
	Sound(3.05,"PC_Gong_Shot")
    
    AttackStart()
	Hurt(0.9,"hurt_21")
    HurtSound(0.9,"Big_blade_Hit_002_1")
    AttackHurtFx(0.9, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(0.9,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N_N1_XLGB01_skill_001_2_level2","N_N1_XLGB01_skill_001")   ----技能  32108  火箭   吟唱0.8    动作总长1.56
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")     
	FrameTime(2.24,3.8)
    Fx(2.24,"N_N1_XLGB01_skill_001a_start","Reference",1,0,0,0,0,0,0,0,1.6,1)
    Fx(2.24,"N_N1_XLGB01_skill_001a_loop","Reference",1,0,0,0,0,0,0,0,2,1)
    Fx(2.964,"N_N1_XLGB01_skill_001a_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    LineFx(2.24,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,30,0,0,0,1.2,1)
	LineFx(3.04,"N_N1_XLGB01_skill_001a_Tracker",0,"Reference", "Reference",1,800,0,0,30)
	Sound(2.24,"PC_LaGong_long")
	Sound(2.24,"FX_Dead_Brun_Loop1")
	Sound(3.05,"FX_Dead_Brun_End")
	Sound(3.05,"PC_Gong_Shot")
    
    AttackStart()
	Hurt(0.9,"hurt_21")
    HurtSound(0.9,"Big_blade_Hit_002_1")
    AttackHurtFx(0.9, "N_N1_XLGB01_skill_002_hit", "Spine1",1)	
    HurtBoneFx(0.9,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N1_Bow_skill_003_level2","N1_Bow_skill_003")  --技能  32109  散射  引导0.8  动作总长1.599
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(2.2,3.799)
	Fx(2.4,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Fx(2.2,"N1_Bow_skill_003_start","Reference",1,0,0,0,0,0,0,0,4,1)
    Fx(3,"N1_Bow_skill_003_fire","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(2.2,"PC_LaGong_long")
	Sound(2.95,"PC_Gong_Shot")
	Sound(3.05,"XMS_Skill_001_2")

    AttackStart()
	Hurt(0.8,"hurt_21")
    HurtSound(0.8,"Big_blade_Hit_002_1")
    DirFx(0.8,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(0.8,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N1_Bow_skill_004_level2","N1_Bow_skill_004")  --技能  32107  射箭  引导0.8  动作总长1.7
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(1.2,2.9)
    Fx(1.2,"N1_Bow_skill_004_start","Reference",1,0,0,0,0,0,0,0,2.6,1)
    LineFx(1.2,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,20,0,0,0,1.2,1)
	LineFx(1.943,"N1_Bow_skill_004_fire",0,"Reference", "Reference",1,800,0,0,0)
	Sound(1.21,"PC_LaGong_long")
	Sound(1.21,"MQX_Skill_000_1")
	Sound(1.95,"PC_Gong_Shot")

    AttackStart()
	Hurt(0.8,"hurt_21")
    HurtSound(0.8,"Big_blade_Hit_002_1")
    DirFx(0.8,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(0.8,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	

STake_Start("N_N1_XLGB01_skill_001_level3","N_N1_XLGB01_skill_001")   ----技能  32106  射箭   吟唱1.013    动作总长1.774
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(2.026,3.8)    
    Fx(2.026,"N_N1_XLGB01_skill_001_start","",1,0,0,0,0,0,0,0,1.27)
    Fx(2.026,"N_N1_XLGB01_skill_001_loop","",1,0,0,0,0,0,0,0,1.59)
    Fx(2.964,"N_N1_XLGB01_skill_001_fire")
    --Fx(2.026,"Fx_Aim_Line","",1,0,0,0,30,0,0,0,0.98)
    LineFx(2.026,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,30,0,0,0,0.98,1)
	LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,0,30)
	Sound(2.03,"PC_LaGong_long")
	Sound(3.05,"PC_Gong_Shot")
    
    AttackStart()
	Hurt(1.013,"hurt_21")
    HurtSound(1.013,"Big_blade_Hit_002_1")
    AttackHurtFx(1.013, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(1.013,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)

	
STake_Start("N_N1_XLGB01_skill_001_2_level3","N_N1_XLGB01_skill_001")   ----技能  32108  吟唱1.013    动作总长1.774
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(2.026,3.8)
    Fx(2.026,"N_N1_XLGB01_skill_001a_start","",1,0,0,0,0,0,0,0,1.27)
    Fx(2.026,"N_N1_XLGB01_skill_001a_loop","",1,0,0,0,0,0,0,0,1.59)
    Fx(2.964,"N_N1_XLGB01_skill_001a_fire")
    --Fx(2.026,"Fx_Aim_Line","",1,0,0,0,30,0,0,0,0.98)
    LineFx(2.026,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,30,0,0,0,0.98,1)
	LineFx(3.04,"N_N1_XLGB01_skill_001a_Tracker",0,"Reference", "Reference",1,800,0,0,30)
	Sound(2.03,"PC_LaGong_long")
	Sound(2.03,"FX_Dead_Brun_Loop1")
	Sound(3.05,"FX_Dead_Brun_End")
	Sound(3.05,"PC_Gong_Shot")

    AttackStart()
	Hurt(1.013,"hurt_21")
    HurtSound(1.013,"Big_blade_Hit_002_1")
    AttackHurtFx(1.013, "N_N1_XLGB01_skill_002_hit", "Spine1",1)
    HurtBoneFx(1.013,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N_N1_XLGB01_skill_002_level3","N_N1_XLGB01_skill_002")   ----技能   H箭雨  32110  吟唱吟唱1.013    动作总长1.774
    BlendMode(2)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(2.026,3.8)
    Fx(2.026,"N_N1_XLGB01_skill_002_start","",1,0,0,0,0,0,0,0,1.27)
    Fx(2.026,"N_N1_XLGB01_skill_002_loop","",1,0,0,0,0,0,0,0,1.59)
    Fx(2.964,"N_N1_XLGB01_skill_002_fire")
	Sound(2.03,"PC_LaGong_long")
	Sound(2.03,"FX_Dead_Brun_Loop1")
	Sound(3.05,"FX_Dead_Brun_End")
	Sound(3.05,"PC_Gong_Shot")


STake_Start("N1_Bow_skill_003_level3","N1_Bow_skill_003")  --技能  32109  散射  引导1  动作总长1.799
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(2,3.799)
	Fx(2.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Fx(2,"N1_Bow_skill_003_start","Reference",1,0,0,0,0,0,0,0,3,1)
    Fx(3,"N1_Bow_skill_003_fire","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(2.07,"PC_LaGong_long")
	Sound(2.95,"PC_Gong_Shot")
	Sound(2.99,"XMS_Skill_001_2")

    AttackStart()
	Hurt(1,"hurt_21")
    HurtSound(1,"Big_blade_Hit_002_1")
    DirFx(1,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(1,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N1_Bow_skill_004_level3","N1_Bow_skill_004")  --技能  32107  射箭  引导0.666  动作总长1.568
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(1.332,2.9)
    Fx(1.332,"N1_Bow_skill_004_start","Reference",1,0,0,0,0,0,0,0,3.2,1)
    --Fx(1.943,"N1_Bow_skill_004_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    LineFx(1.332,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,20,0,0,0,1.501,1)
	LineFx(1.943,"N1_Bow_skill_004_fire",0,"Reference", "Reference",1,800,0,0,0)
	Sound(1.334,"PC_LaGong_long")
	Sound(1.95,"PC_Gong_Shot")

    AttackStart()
	Hurt(0.666,"hurt_21")
    HurtSound(0.666,"Big_blade_Hit_002_1")
    DirFx(0.666,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(0.666,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)


STake_Start("N1_Bow_skill_005_level3","N1_Bow_skill_005")  --技能  32105  斗气风暴  引导0.666  动作总长2.334
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(1.332,3.666)
	Fx(1.532,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Fx(1.332,"N1_Bow_skill_005_start","Reference",1,0,0,0,0,0,0,0,3,1)
    Fx(1.979,"N1_Bow_skill_005_fire","Reference",1,0,0,0,0,0,0,0,1,1)
	Sound(1.4,"Lzw_Skill_05")
	Sound(1.9,"Lzw_Skill_04")
	Sound(1.84,"HM_MaleVoc_02_attack02")

    AttackStart()
	Hurt(0.666,"hurt_21")
    HurtSound(0.666,"Big_blade_Hit_002_1")
    DirFx(0.666,"N1_Bow_skill_003_hit",0,1) 
    HurtBoneFx(0.666,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("N1_Bow_skill_jumpF","N1_Bow_skill_jumpF")  --技能 32103 跳跃  引导一0.01charge0.4  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,1.2)
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.28,"TY_Attack_011")
	

---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------

STake_Start("Common_skill_jumpB_120","N1_Bow_skill_jumpB")  --技能 40000 远程通用后跳  引导0.01charge0.4  动作总厂0.899
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,0.899)
	Fx(0.4,"FX_common_skill_jumpB_02","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.08,"char_05_cloth_flap_02")	
	Sound(0.38,"SZ03_dodge_000")	


STake_Start("Common_skill_jumpF","N1_Bow_skill_jumpF")  --技能 40001 通用追击  引导一0.01charge0.4  引导二0.46  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,1.2)
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
    Fx(0.41,"FX_common_skill_jumpF","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.28,"TY_Attack_011")
	
  	AttackStart()
  	Hurt(0.46,"hurt_21")    
    DirFx(0.46,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.46,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.46,"HMW01_Hit_002")
    RangeCameraShake(0.46,"ShakeDistance = 80","ShakeMode = 3","ShakeTimes = 0.14","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")



package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("N1_life")


STake_Start("Monster_B_emo_001","N1_Axe_B_emo_001")  --战斗嘲讽
    BlendMode(0)
    BlendTime(0.2)
  
STake_Start("Monster_B_move_004","N1_Axe_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_Axe_move_005","N1_Axe_move_005")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_wait_000","N1_Axe_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_dead","N1_Axe_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.246,"DaoDi_002")

STake_Start("Monster_dead_000","N1_Axe_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.513,"DaoDi_000")

STake_Start("Monster_dead_001","N1_Axe_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.300,"DaoDi_002")
    Sound(0.825,"DaoDi_001")

STake_Start("Monster_dead_002","N1_Axe_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.434,"DaoDi_000")

STake_Start("Monster_dead_003","N1_Axe_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.434,"DaoDi_000")

STake_Start("Monster_def_000","N1_Axe_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.039,"N1_Axe_def_000")

STake_Start("Monster_dodge_000","N1_Axe_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.039,"N1_Axe_dodge_000")

STake_Start("Monster_emo_001","N1_Axe_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_emo_002","N1_Axe_emo_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(2.519,"N1_Axe_emo_002_1")
    Sound(3.032,"N1_Axe_emo_002_1")
    Sound(3.592,"N1_Axe_emo_002_1")

STake_Start("Monster_link_000","N1_Axe_link_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_link_001","N1_Axe_link_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_move_000","N1_Axe_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_start","N1_move_005-007") --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("Monster_jump_loop","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_down","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)	
	
STake_Start("Monster_B_move_003","N1_Axe_move_003")  --战斗走
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_move_003_L","N1_Axe_B_move_003_L")  --战斗左走
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_move_003_R","N1_Axe_B_move_003_R")  --战斗右走
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_move_000_L","N1_Axe_B_move_003_L")  --左走
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_move_000_R","N1_Axe_B_move_003_R")  --右走
    BlendMode(0)
    BlendTime(0.2)
    Loop()	
	
STake_Start("Monster_wait_000","N1_Axe_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_bout_000","N1_bout_001")   --出生
    BlendMode(0)
    BlendTime(0.2) 	

	
STake_Start("N1_Axe_skill_001_start","N1_Axe_skill_001")  --挥砍 27014  吟唱1.680  动作总长3.066
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3.066)
	Pause(1.750,0.16,1,0.012)
    Sound(1.35,"N1_Axe_attack_001")
	Fx(1.2,"L1_ZDLQFZ_skill_004_fire","",0.5,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)

STake_Start("N1_Axe_skill_001_fire","")  
    BlendMode(0)
    BlendTime(0.2)	
	Priority(0,"5")  
	Pause(1.750,0.16,1,0.012)
		
    AttackStart()
  	Hurt(0.07,"hurt_21")
    HurtSound(0.07,"Big_blade_Hit_002_1")	
    DirFx(0.07,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37)
	HurtBoneFx(0.07,"FX_blood_dao_b","Spine",1.2,0,90,0,0,-70,0)


STake_Start("N1_Axe_skill_002_start","N1_Axe_skill_002")  --冲撞 27016
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,3.9)
    Fx(1,"Scorpion_XZW01_bout_001","Reference",0.8)
	Fx(0.2,"FX_Warning","Head")
    Sound(1.443,"QQJ_B_move_000")
    Sound(1.713,"QQJ_B_move_000")
    Sound(1.98,"QQJ_B_move_000")
    Sound(2.143,"QQJ_B_move_000")
    Sound(2.263,"QQJ_skilljump_down")
	
    PlaySpeed(2.2,2.6,2)	

	
STake_Start("N1_Axe_skill_002_fire","")  
    BlendMode(0)
    BlendTime(0.2)	
    Priority(0,"5")  
	
    Charge(0,0.25,120,20,12)
	
    AttackStart()
  	Hurt(0,"hurt_81")
    HurtSound(0,"Big_blade_Hit_002_1")	
    DirFx(0,"L1_Unarmed_skill_hit",0,1,0.3)
	HurtBoneFx(0,"FX_blood_quan_c","Spine",1,0,0,0,0,0,0)



---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		
	

STake_Start("N1_Axe_skill_001_level2_start","N1_Axe_skill_001")  --挥砍 27014   吟唱0.84  动作总长2.226
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	Pause(1.750,0.16,1,0.012)
	FrameTime(0.84,3.066)
    Sound(1.35,"N1_Axe_attack_001")
	Fx(1.2,"L1_ZDLQFZ_skill_004_fire","",0.5,0,0,0,0,0,0,0,1,1)
	Fx(1.04,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	
STake_Start("N1_Axe_skill_001_level2_fire","")  
    BlendMode(0)
    BlendTime(0.2)	
	Priority(0,"5")
	Pause(1.750,0.16,1,0.012)
		
    AttackStart()
  	Hurt(0.07,"hurt_21")
    HurtSound(0.07,"Big_blade_Hit_002_1")	
    DirFx(0.07,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37)
	HurtBoneFx(0.07,"FX_blood_dao_b","Spine",1.2,0,90,0,0,-70,0)

	
STake_Start("N1_Axe_skill_002_level2_start","N1_Axe_skill_002")  --冲撞 27016    吟唱1.05    动作总长2.85
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(1.05,3.9)
    Fx(1.4,"Scorpion_XZW01_bout_001","Reference",0.8)
	Fx(1.25,"FX_Warning","Head")
    Sound(1.443,"QQJ_B_move_000")
    Sound(1.713,"QQJ_B_move_000")
    Sound(1.98,"QQJ_B_move_000")
    Sound(2.143,"QQJ_B_move_000")
    Sound(2.263,"QQJ_skilljump_down")
    PlaySpeed(2.2,2.6,2)	
	
STake_Start("N1_Axe_skill_002_level2_fire","")  
    BlendMode(0)
    BlendTime(0.2)	
    Priority(0,"5")  
	
    Charge(0,0.25,120,20,12)
	
    AttackStart()
  	Hurt(0,"hurt_81")
    HurtSound(0,"Big_blade_Hit_002_1")	
    DirFx(0,"L1_Unarmed_skill_hit",0,1,0.3)
	HurtBoneFx(0,"FX_blood_quan_c","Spine",1,0,0,0,0,0,0)
	
	
	

---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	
	

STake_Start("N1_Axe_skill_001_level3_start","N1_Axe_skill_001")  --挥砍 27014   吟唱0.56  动作总长1.946
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	Pause(1.750,0.16,1,0.012)
	FrameTime(1.12,3.066)
    Sound(1.35,"N1_Axe_attack_001")
	Fx(1.2,"L1_ZDLQFZ_skill_004_fire","",0.5,0,0,0,0,0,0,0,1,1)
	Fx(1.32,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	
STake_Start("N1_Axe_skill_001_level3_fire","")  
    BlendMode(0)
    BlendTime(0.2)	
	Priority(0,"5")
	Pause(1.750,0.16,1,0.012)
		
    AttackStart()
  	Hurt(0.07,"hurt_21")
    HurtSound(0.07,"Big_blade_Hit_002_1")	
    DirFx(0.07,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37)
	HurtBoneFx(0.07,"FX_blood_dao_b","Spine",1.2,0,90,0,0,-70,0)

	
STake_Start("N1_Axe_skill_002_level3_start","N1_Axe_skill_002")  --冲撞 27016    吟唱0.7    动作总长2.5
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(1.4,3.9)
    Fx(1.4,"Scorpion_XZW01_bout_001","Reference",0.8)
	Fx(1.6,"FX_Warning","Head")
    Sound(1.443,"QQJ_B_move_000")
    Sound(1.713,"QQJ_B_move_000")
    Sound(1.98,"QQJ_B_move_000")
    Sound(2.143,"QQJ_B_move_000")
    Sound(2.263,"QQJ_skilljump_down")
    PlaySpeed(2.2,2.6,2)	
	
STake_Start("N1_Axe_skill_002_level3_fire","")  
    BlendMode(0)
    BlendTime(0.2)	
    Priority(0,"5")  
	
    Charge(0,0.25,120,20,12)
	
    AttackStart()
  	Hurt(0,"hurt_81")
    HurtSound(0,"Big_blade_Hit_002_1")	
    DirFx(0,"L1_Unarmed_skill_hit",0,1,0.3)  	
	HurtBoneFx(0,"FX_blood_quan_c","Spine",1,0,0,0,0,0,0)


---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
STake_Start("Common_skill_jumpB_80","N1_Axe_skill_jumpB")  --技能 40009 近战通用后跳  引导0.01charge0.4
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,0.899)
	Fx(0.4,"FX_common_skill_jumpB_02","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.03,"L1_SBlade_dodge_000")
	Sound(0.03,"MQX_Skill_000_4")

	
STake_Start("Common_skill_jumpF","N1_Axe_skill_jumpF")  --技能 40001 通用追击  引导一0.01charge0.4  引导二0.46  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,1.2)
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
    Fx(0.41,"FX_common_skill_jumpF","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.03,"MQX_Skill_000_1")
	Sound(0.4,"SG_THD_SW_skill_010_Impact")
	
  	AttackStart()
  	Hurt(0.46,"hurt_21")    
    DirFx(0.46,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.46,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.46,"HMW01_Hit_001")
    RangeCameraShake(0.46,"ShakeDistance = 80","ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
	

package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")


STake_Start("N1_Blade_B_move_003","N1_Blade_B_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_Blade_B_move_004","N1_Blade_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_Blade_B_wait_000","N1_Blade_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","N1_Blade_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Blade_dead","N1_Blade_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.437,"TongYong_DaoDi_005")
    Sound(1.406,"TongYong_DaoDi")
    Sound(0.456,"Sword_dead_VOC_000")
    Sound(1.406,"DiaoLuo_Ren")
    Sound(1.406,"DiaoLuo_TieGun")

STake_Start("N1_Blade_dead_000","N1_Blade_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.883,"TongYong_DaoDi")
    Sound(0.335,"Sword_dead_VOC_000")
    Sound(0.883,"DiaoLuo_Ren")
    Sound(0.883,"DiaoLuo_TieGun")

STake_Start("N1_Blade_dead_001","N1_Blade_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.315,"TongYong_DaoDi_005")
    Sound(0.840,"TongYong_DaoDi")
    Sound(0.165,"Sword_dead_VOC_000")
    Sound(0.840,"DiaoLuo_Ren")
    Sound(0.840,"DiaoLuo_TieGun")


STake_Start("N1_Blade_dead_002","N1_Blade_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.315,"TongYong_DaoDi_005")
    Sound(0.840,"TongYong_DaoDi")
    Sound(0.165,"Sword_dead_VOC_000")
    Sound(0.840,"DiaoLuo_Ren")
    Sound(0.840,"DiaoLuo_TieGun")


STake_Start("N1_Blade_dead_003","N1_Blade_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.883,"TongYong_DaoDi")
    Sound(0.335,"Sword_dead_VOC_000")
    Sound(0.883,"DiaoLuo_Ren")
    Sound(0.883,"DiaoLuo_TieGun")

STake_Start("N1_Blade_def_000","N1_Blade_def_000")
    BlendMode(0)
    BlendTime(0.2)  

    Sound(0.065,"T1_ShieldSpear2_def_000")

STake_Start("N1_Blade_dodge_000","N1_Blade_dodge_000")
    BlendMode(2)
    BlendTime(0.2)

    Sound(0.080,"SZ03_emo_001")

STake_Start("N1_Blade_emo_001","N1_Blade_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Blade_emo_002","N1_Blade_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Blade_move_000","N1_Blade_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_Blade_wait_000","N1_Blade_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
-------------------------------------------------------------------------------------------------------------------------------
--CutScene用

--完整基础动作

STake_Start("CS_dodge1_HWBS","YP_dodge_000") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead1_HWBS","N1_Blade_dead_000") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead3_HWBS","N1_wait_004") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead5_HWBS","N1_life_034") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead6_HWBS","N1_Blade_dead") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait1_HWBS","N1_Blade_B_wait_000") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_attack1_HWBS","N1_Blade_attack_000") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_attack2_HWBS","N1_Blade_attack_002") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down1_HWBS","N1_HAJ01_wait_000") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down2_HWBS","N1_MGSB_01_wait_001") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down3_HWBS","N1_wait_006") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down4_HWBS","N1_wait_007") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down5_HWBS","N1_MGSB_01_emo_000") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_run1_HWBS","N1_Blade_B_move_004") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_run2_HWBS","N1_move_002") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look1_HWBS","N1_HAJJL_emo_001") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_talk1_HWBS","N1_life_026") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead1_ShuiShou_HWBS","N1_Blade_dead_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead1_ShuiShou","N1_Blade_dead_001")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

--动作片段

STake_Start("CS_dead1_stop_HWBS","N1_Blade_dead_000") 
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.765,1.766)
    Loop()

STake_Start("CS_dead2_stop_HWBS","N1_Blade_dead_001") 
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.499,1.5)
    Loop()

STake_Start("CS_dead5_stop_HWBS","N1_life_034") 
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.001)
    Loop()
package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("N1_life")



STake_Start("N1_Blade1_def_000","N1_Blade1_def_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Blade1_dodge_000","N1_Blade1_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Blade1_B_wait_000","N1_Blade1_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_Blade1_dead","N1_Blade1_dead")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Blade1_dead_000","N1_Blade1_dead_000")
    BlendMode(0)
    BlendTime(0.2)

     Sound(0.050,"Male_LS_D_Death01")

STake_Start("N1_Blade1_dead_001","N1_Blade1_dead_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Blade1_dead_002","N1_Blade1_dead_002")
    BlendMode(0)
    BlendTime(0.2)

     Sound(0.050,"Male_LS_D_Death01")

STake_Start("N1_Blade1_dead_003","N1_Blade1_dead_003")
    BlendMode(0)
    BlendTime(0.2)

     Sound(0.050,"Male_LS_D_Death01")

STake_Start("N1_Blade1_emo_001","N1_Blade1_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Blade1_emo_002","N1_Blade1_emo_002")
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("Monster_B_emo_001","N1_Blade1_emo_001")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("N1_Blade1_move_000","N1_Blade1_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_Blade1_B_move_003","N1_Blade1_B_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_Blade1_B_move_004","N1_Blade1_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()


STake_Start("N1_Blade1_wait_000","N1_Blade1_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()





-------------------------------------------------------------------------------------------------------------------------------
--CutScene用

--完整基础动作

STake_Start("CS_roar1_SHHDB","N1_Blade1_skill_001") 
    BlendMode(0)
    BlendTime(0.4) 
    Loop()

STake_Start("CS_roar1_ready_SHHDB","N1_Blade1_skill_003") 
    BlendMode(0)
    BlendTime(0.4) 
    Loop()

STake_Start("CS_roar2_SHHDB","N1_Blade1_skill_000") 
    BlendMode(0)
    BlendTime(0.4) 
    Loop()

STake_Start("CS_run1_SHHDB","N1_Blade1_B_move_004") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait1_SHHDB","N1_Blade1_B_wait_000") 
    BlendMode(0)
    BlendTime(0.4) 
    Loop()

STake_Start("CS_wait2_SHHDB","N1_Blade1_emo_001") 
    BlendMode(0)
    BlendTime(0.4) 
    Loop()

STake_Start("CS_wait3_SHHDB","N1_Blade1_wait_000") 
    BlendMode(0)
    BlendTime(0.4) 
    Loop()

STake_Start("CS_attack1_SHHDB","N1_Blade1_attack_000") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_attack2_SHHDB","N1_Blade1_attack_001") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_attack3_SHHDB","N1_Blade1_attack_002") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dodge_SHHDB","N1_Blade1_dodge_000") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_dead1_SHHDB","N1_Blade1_dead") 
    BlendMode(0)
    BlendTime(0.4) 
    Loop()

--动作片段
STake_Start("CS_roar1_clip_SHHDB","N1_Blade1_skill_001") 
    BlendMode(0)
    BlendTime(0.4) 
    FrameTime(0.55,0.58)
    Loop()

STake_Start("CS_roar1_stop_SHHDB","N1_Blade1_skill_001") 
    BlendMode(0)
    BlendTime(0.4) 
    FrameTime(0.55,0.5501)
    Loop()


STake_Start("CS_attack2_stop_SHHDB","N1_Blade1_attack_000") 
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0.5,0.5001)
    Loop()

STake_Start("CS_dead1_stop_SHHDB","N1_Blade1_dead") 
    BlendMode(0)
    BlendTime(0.4) 
    FrameTime(2.4999,2.5)
    Loop()








package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("N1_life")


  
STake_Start("Monster_B_move_003","N1_DoubleDagger_B_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_move_004","N1_DoubleDagger_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_wait_000","N1_DoubleDagger_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_jump_start_B","N1_DoubleDagger_jump_start_B(0-19)")  --后跳START
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.629)
	
STake_Start("Monster_jump_loop_B","N1_DoubleDagger_jump_loop_B")          --后跳LOOP
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_jump_down_B","N1_DoubleDagger_jump_start_B(0-19)")   --后跳END
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.629,1.166)
	
STake_Start("Monster_jump_start_L","N1_DoubleDagger_jump_start_L(0-19)")  --左跳START
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.629)
	
STake_Start("Monster_jump_loop_L","N1_DoubleDagger_jump_loop_L")          --左跳LOOP
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_jump_down_L","N1_DoubleDagger_jump_start_L(0-19)")   --左跳END
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.629,1.166)
	
STake_Start("Monster_jump_start_R","N1_DoubleDagger_jump_start_R(0-19)")  --左跳START
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.629)
	
STake_Start("Monster_jump_loop_R","N1_DoubleDagger_jump_loop_R")          --左跳LOOP
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_jump_down_R","N1_DoubleDagger_jump_start_R(0-19)")   --左跳END
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.629,1.166)


STake_Start("Monster_dead","N1_DoubleDagger_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.120,"Sword_dead_VOC_000")
    Sound(0.420,"Martialsword_def_000")
    Sound(1.440,"TongYong_DaoDi_005")
    Sound(1.620,"DiaoLuo_TieGun")
   
 
    
STake_Start("Monster_dead_000","N1_DoubleDagger_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.580,"Sword_dead_VOC_000")
    Sound(1.360,"TongYong_DaoDi_001")
    Sound(1.080,"TongYong_DaoDi_004")
    Sound(1.460,"DiaoLuo_TieGun")

    
STake_Start("Monster_dead_001","N1_DoubleDagger_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    
    Sound(0.120,"Sword_dead_VOC_000")
    Sound(0.420,"Martialsword_def_000")
    Sound(1.440,"TongYong_DaoDi_005")
    Sound(1.620,"DiaoLuo_TieGun")
   


STake_Start("Monster_dead_002","N1_DoubleDagger_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.580,"Sword_dead_VOC_000")
    Sound(1.360,"TongYong_DaoDi_001")
    Sound(1.080,"TongYong_DaoDi_004")
    Sound(1.460,"DiaoLuo_TieGun")


STake_Start("Monster_dead_003","N1_DoubleDagger_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.580,"Sword_dead_VOC_000")
    Sound(1.360,"TongYong_DaoDi_001")
    Sound(1.080,"TongYong_DaoDi_004")
    Sound(1.460,"DiaoLuo_TieGun")


STake_Start("Monster_def_000","N1_DoubleDagger_def_000")
    BlendMode(0)
    BlendTime(0.2)
    
    Sound(0.019,"LZW_B_def_000")

STake_Start("Monster_dodge_000","N1_DoubleDagger_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.046,"NB_LZW_dodge")
    Sound(0.166,"NB_Singlestick_def_000")

STake_Start("Monster_emo_001","N1_DoubleDagger_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_emo_002","N1_DoubleDagger_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_emo_003","N1_DoubleDagger_emo_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.666,"DSword_attack_002")

STake_Start("Monster_B_emo_001","N1_DoubleDagger_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_link_001","N1_DoubleDagger_link_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_link_002","N1_DoubleDagger_link_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_move_000","N1_DoubleDagger_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_wait_000","N1_DoubleDagger_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_start","N1_move_005-007")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.8333)	
	
STake_Start("Monster_jump_loop","N1_move_006")
    BlendMode(0)
    BlendTime(0.2)
	Loop()

STake_Start("Monster_jump_down","N1_move_005-007")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)	



STake_Start("Monster_attack_003","N1_DoubleDagger_attack_003")   --技能  26026  近身平砍  引导一1.74  引导二2.00  动作总长2.9
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,2.9)
    Pause(1.800,0.13,1,0.015)
    Pause(2.700,0.13,1,0.015)
    Sound(1.82,"Martialsword_attack_001")  
    Sound(2.06,"Martialsword_attack_VOC")  
	Fx(0.2,"FX_Warning","Head")	
    Fx(1.8,"N_T1_JGMJBS01_skill_001_fire","Reference",0.5,1,0,0,10,0,0,-30,1.8,1)  
    Fx(2.08,"N_T1_JGMJBS01_skill_001_fire","Reference",0.5,1,0,0,10,0,0,0,1.8,1) 

    AttackStart()
  	Hurt(1.800,"hurt_21")
  	Hurt(2.200,"hurt_21")
    DirFx(1.800,"T1_DoubleBlade_skill_hit",0,1,0.5)      
    DirFx(2.200,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(1.800,"Big_blade_Hit")
    HurtSound(2.200,"Big_blade_Hit")
	HurtBoneFx(1.800,"FX_blood_bishou_b","Spine",1,0,0,0,-90,-30,0)
	HurtBoneFx(2.200,"FX_blood_bishou_b","Spine",1,0,0,0,90,-30,0)


STake_Start("N1_DoubleDagger_skill_001","N1_DoubleDagger_skill_001")   --技能  26038   跳砍  引导一1.58charge0.443  引导二2.043  动作总长2.9
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,2.933)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(0,"N1_DoubleDagger_skill_001_start","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(1.58,"N1_DoubleDagger_skill_001_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    Sound(1.82,"Martialsword_attack_001")  
    Sound(2.06,"Martialsword_attack_VOC")  

    AttackStart()
  	Hurt(2.043,"hurt_21")   
    DirFx(2.043,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(2.043,"Big_blade_Hit")
	HurtBoneFx(2.043,"FX_blood_bishou_b","Spine",1,0,0,0,-90,0,0)


STake_Start("N1_DoubleDagger_skill_002","N1_DoubleDagger_skill_002")   --技能  26040  飞镖  引导3  动作总长4
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,4)
    --Fx(0,"Fx_Aim_Line","",1,0,0,0,20,0,0,0,0.33,1)
    Fx(0,"N1_DoubleDagger_skill_002_start","",1,0,0,0,0,0,0,0,1,1)
    Fx(1,"N1_DoubleDagger_skill_002_start","",1,0,0,0,0,0,0,0,1,1)
    Fx(2,"N1_DoubleDagger_skill_002_start","",1,0,0,0,0,0,0,0,1,1)
    --Fx(3,"N1_DoubleDagger_skill_002_Trail","",1,0,0,0,20,0,0,0,1,1)
    LineFx(0,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,20,0,0,0,0.33,1)
    LineFx(3.0,"N1_DoubleDagger_skill_002_Trail",0,"Reference", "Reference",1,800,0,0,20)
    Sound(2.9,"TY_Attack_001")  
    Sound(2.88,"Martialsword_attack_VOC")  

    AttackStart()
  	Hurt(3.1,"hurt_21")   
    DirFx(3.1,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(3.1,"Big_blade_Hit")
	HurtBoneFx(3.1,"FX_blood_bishou_b","Spine",1,0,0,0,-90,0,0)
	
	
STake_Start("N1_DoubleDagger_skill_003","N1_DoubleDagger_skill_003(loop54-57)")   --技能  26042  环刺  引导一1.6  动作总长3.333
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3.333)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(0,"N1_DoubleDagger_skill_003_start","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(1.6,"N1_DoubleDagger_skill_003_loop","Reference",1.3,0,0,0,0,0,0,0,1,1)
    Sound(1.42,"Martialsword_attack_001")  
    Sound(1.46,"TY_Attack_008")  
	Sound(1.96,"TY_Attack_014")
    Sound(1.36,"Martialsword_attack_VOC")  

    AttackStart()
  	Hurt(1.6,"hurt_81")   
    DirFx(1.6,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(1.6,"Big_blade_Hit")
	HurtBoneFx(1.6,"FX_blood_bishou_b","Spine",1,0,0,0,-90,0,0)



---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		
	
STake_Start("Monster_attack_003_level2","N1_DoubleDagger_attack_003")   --技能  26026  近身平砍  引导一0.87  引导二1.13  动作总长2.03
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.87,2.9)
    Pause(1.800,0.13,1,0.015)
    Pause(2.700,0.13,1,0.015)
    Sound(1.82,"Martialsword_attack_001")  
    Sound(2.06,"Martialsword_attack_VOC")  
	Fx(1.07,"FX_Warning","Head")	
    Fx(1.8,"N_T1_JGMJBS01_skill_001_fire","Reference",0.5,1,0,0,10,0,0,-30,1.8,1)  
    Fx(2.08,"N_T1_JGMJBS01_skill_001_fire","Reference",0.5,1,0,0,10,0,0,0,1.8,1) 

    AttackStart()
  	Hurt(0.93,"hurt_21")
  	Hurt(1.33,"hurt_21")
    DirFx(0.93,"T1_DoubleBlade_skill_hit",0,1,0.5)      
    DirFx(1.33,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(0.93,"Big_blade_Hit")
    HurtSound(1.33,"Big_blade_Hit")
	HurtBoneFx(0.93,"FX_blood_bishou_b","Spine",1,0,0,0,-90,-30,0)
	HurtBoneFx(1.33,"FX_blood_bishou_b","Spine",1,0,0,0,90,-30,0)

	
STake_Start("N1_DoubleDagger_skill_001_level2","N1_DoubleDagger_skill_001")   --技能  26038   跳砍  引导一0.79charge0.443  引导二1.253  动作总长2.11
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.79,2.9)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(0.79,"N1_DoubleDagger_skill_001_start","Reference",1,0,0,0,0,0,0,0,1.8,1)
	Fx(1.58,"N1_DoubleDagger_skill_001_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    Sound(1.82,"Martialsword_attack_001")  
    Sound(2.06,"Martialsword_attack_VOC")  

    AttackStart()
  	Hurt(1.253,"hurt_21")   
    DirFx(1.253,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(1.253,"Big_blade_Hit")
	HurtBoneFx(1.253,"FX_blood_bishou_b","Spine",1,0,0,0,-90,0,0)
	

STake_Start("N1_DoubleDagger_skill_002_level2","N1_DoubleDagger_skill_002")   --技能  26040  飞镖  引导1.5  动作总长2.5
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.5,4)
    Fx(1.5,"N1_DoubleDagger_skill_002_start","",1,0,0,0,0,0,0,0,1.333,1)
    Fx(2.25,"N1_DoubleDagger_skill_002_start","",1,0,0,0,0,0,0,0,1.333,1)
    LineFx(1.5,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,20,0,0,0,0.5,1)
    LineFx(3.0,"N1_DoubleDagger_skill_002_Trail",0,"Reference", "Reference",1,800,0,0,20)
    Sound(2.9,"TY_Attack_001")  
    Sound(2.88,"Martialsword_attack_VOC")  

    AttackStart()
  	Hurt(1.6,"hurt_21")   
    DirFx(1.6,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(1.6,"Big_blade_Hit")
	HurtBoneFx(1.6,"FX_blood_bishou_b","Spine",1,0,0,0,-90,0,0)
	
	
STake_Start("N1_DoubleDagger_skill_003_level2","N1_DoubleDagger_skill_003(loop54-57)")   --技能  26042  环刺  引导一0.8  动作总长2.533
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.8,3.333)
	Fx(1.0,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(0.8,"N1_DoubleDagger_skill_003_start","Reference",1,0,0,0,0,0,0,0,2,1)
	Fx(1.6,"N1_DoubleDagger_skill_003_loop","Reference",1.3,0,0,0,0,0,0,0,1,1)
    Sound(1.42,"Martialsword_attack_001")  
    Sound(1.46,"TY_Attack_008")  
	Sound(1.96,"TY_Attack_014")
    Sound(1.36,"Martialsword_attack_VOC")  

    AttackStart()
  	Hurt(0.8,"hurt_81")   
    DirFx(0.8,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(0.8,"Big_blade_Hit")
	HurtBoneFx(0.8,"FX_blood_bishou_b","Spine",1,0,0,0,-90,0,0)
	

	

	
---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	
	

STake_Start("Monster_attack_003_level3","N1_DoubleDagger_attack_003")   --技能  26026  近身平砍  引导一0.58  引导二0.84  动作总长1.74
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.16,2.9)
    Pause(1.800,0.13,1,0.015)
    Pause(2.700,0.13,1,0.015)
    Sound(1.82,"Martialsword_attack_001")  
    Sound(2.06,"Martialsword_attack_VOC")  
	Fx(0.2,"FX_Warning","Head")	
    Fx(1.8,"N_T1_JGMJBS01_skill_001_fire","Reference",0.5,1,0,0,10,0,0,-30,1.8,1)  
    Fx(2.08,"N_T1_JGMJBS01_skill_001_fire","Reference",0.5,1,0,0,10,0,0,0,1.8,1) 

    AttackStart()
  	Hurt(0.64,"hurt_21")
  	Hurt(1.04,"hurt_21")
    DirFx(0.64,"T1_DoubleBlade_skill_hit",0,1,0.5)      
    DirFx(1.04,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(0.64,"Big_blade_Hit")
    HurtSound(1.04,"Big_blade_Hit")
	HurtBoneFx(0.64,"FX_blood_bishou_b","Spine",1,0,0,0,-90,-30,0)
	HurtBoneFx(1.04,"FX_blood_bishou_b","Spine",1,0,0,0,90,-30,0)


STake_Start("N1_DoubleDagger_skill_001_level3","N1_DoubleDagger_skill_001")   --技能  26038   跳砍  引导一0.526charge0.443  引导二0.991  动作总长1.848
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.052,2.9)
	Fx(1.252,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(1.052,"N1_DoubleDagger_skill_001_start","Reference",1,0,0,0,0,0,0,0,2,1)
	Fx(1.58,"N1_DoubleDagger_skill_001_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    Sound(1.82,"Martialsword_attack_001")  
    Sound(2.06,"Martialsword_attack_VOC")  

    AttackStart()
  	Hurt(0.991,"hurt_21")   
    DirFx(0.991,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(0.991,"Big_blade_Hit")
	HurtBoneFx(0.991,"FX_blood_bishou_b","Spine",1,0,0,0,-90,0,0)
	
	
STake_Start("N1_DoubleDagger_skill_002_level3","N1_DoubleDagger_skill_002")   --技能  26040  飞镖  引导1  动作总长2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(2,4)
    Fx(2,"N1_DoubleDagger_skill_002_start","",1,0,0,0,0,0,0,0,1,1)
    LineFx(2,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,20,0,0,0,1,1)
    LineFx(3.0,"N1_DoubleDagger_skill_002_Trail",0,"Reference", "Reference",1,800,0,0,20)
    Sound(2.9,"TY_Attack_001")  
    Sound(2.88,"Martialsword_attack_VOC")  

    AttackStart()
  	Hurt(1,"hurt_21")   
    DirFx(1,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(1,"Big_blade_Hit")
	HurtBoneFx(1,"FX_blood_bishou_b","Spine",1,0,0,0,-90,0,0)
	
	
STake_Start("N1_DoubleDagger_skill_003_level3","N1_DoubleDagger_skill_003(loop54-57)")   --技能  26042  环刺  引导一0.533  动作总长2.267
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.066,3.333)
	Fx(1.266,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(1.066,"N1_DoubleDagger_skill_003_start","Reference",1,0,0,0,0,0,0,0,3,1)
	Fx(1.6,"N1_DoubleDagger_skill_003_loop","Reference",1.3,0,0,0,0,0,0,0,1,1)
    Sound(1.42,"Martialsword_attack_001")  
    Sound(1.46,"TY_Attack_008")  
	Sound(1.96,"TY_Attack_014")
    Sound(1.36,"Martialsword_attack_VOC")  

    AttackStart()
  	Hurt(0.533,"hurt_81")   
    DirFx(0.533,"T1_DoubleBlade_skill_hit",0,1,0.5)       
    HurtSound(0.533,"Big_blade_Hit")
	HurtBoneFx(0.533,"FX_blood_bishou_b","Spine",1,0,0,0,-90,0,0)

---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
STake_Start("Common_skill_jumpB_80","N1_DoubleDagger_skill_jumpB")  --技能 40009 近战通用后跳  引导0.01charge0.4
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,0.899)
	Fx(0.4,"FX_common_skill_jumpB_02","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.03,"L1_SBlade_dodge_000")
	Sound(0.03,"MQX_Skill_000_4")

	
STake_Start("Common_skill_jumpF","N1_DoubleDagger_skill_jumpF")  --技能 40001 通用追击  引导一0.01charge0.4  引导二0.46  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,1.2)
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
    Fx(0.41,"FX_common_skill_jumpF","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.03,"MQX_Skill_000_1")
	Sound(0.4,"SG_THD_SW_skill_010_Impact")
	
  	AttackStart()
  	Hurt(0.46,"hurt_21")    
    DirFx(0.46,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.46,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.46,"HMW01_Hit_001")
    RangeCameraShake(0.46,"ShakeDistance = 80","ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
	

---------------------------------------------------------------------------------------------------------------------------------------
--CutScene用

--塞外古城


STake_Start("CS_attack_R_MZDS","N1_DoubleDagger_attack_000") 
    BlendMode(0)
    BlendTime(0.2)
    Loop()


STake_Start("CS_attack_L_MZDS","N1_DoubleDagger_attack_001") 
    BlendMode(0)
    BlendTime(0.2)
    Loop()


















package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("N1_Fighter")
require("YP_hurt")package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("N1_life")

STake_Start("S1_DC01_wait_000","S1_DC01_wait_000")  --大厨01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S1_DC01_emo_000","S1_DC01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_DC01_emo_001","S1_DC01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

---------------------------------------------------------------------------------------------------------------------------------------
--CutScene用

--塞外古城


STake_Start("CS_walk","N1_move_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_escape","N1_move_020")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_front","N1_XinShi_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_aroundA","N1_JHYY01_emo_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_aroundB","N1_SLSJQS_emo_001")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_aroundC","N1_QZRMDZ01_emo_001")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_afraidA","N1_life_009")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


--吉永号


STake_Start("CS_beg1_ZHLB","N1_life_017")  --吉永号上商人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead1_ZHLB","N1_wait_004")  --吉永号上商人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down1_ZHLB","N1_life_023")  --吉永号上商人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_escape1_ZHLB","N1_move_020")  --吉永号上商人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_afraid1_ShuiShou","N1_wait_027")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_afraid2_ShuiShou","N1_life_009")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down1_ShuiShou","N1_BTSDJDZ_emo_003")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down2_ShuiShou","N1_MGSB_01_emo_001")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down3_ShuiShou","N1_life_025")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down4_ShuiShou","N1_life_024")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down5_ShuiShou","N1_life_034")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down6_ShuiShou","N1_life_023")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down7_ShuiShou","N1_wait_010")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_down8_ShuiShou","N1_BTSZJDZ_emo_003")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_down9_ShuiShou","N1_CM03_life_000")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead1_ShuiShou","N1_Blade_dead_001")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead2_ShuiShou","N1_wait_008")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_talk1_ShuiShou","N1_WQS01_emo_001")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_walk1_ShuiShou","N1_move_021")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_walk2_B_ShuiShou","N1_move_001_B") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead1_HWBS_ShuiShou","N1_Blade_dead_000")    --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_fall1_ShuiShou","N1_HAJJL_emo_001")    --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_dead1_HWBS","N1_Blade_dead_000")   --护卫镖师
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


--动作片段
STake_Start("CS_dead1_stop_ShuiShou","N1_Blade_dead_001")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.45,1.451)
    Loop()

STake_Start("CS_dead1_stop_HWBS_ShuiShou","N1_Blade_dead_000")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.765,1.766)
    Loop()

STake_Start("CS_dead2_stop_HWBS_ShuiShou","N1_Blade_dead_001")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.499,1.5)
    Loop()

STake_Start("CS_dead1_stop_HWBS","N1_Blade_dead_000")    --护卫镖师
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.765,1.766)
    Loop()

STake_Start("CS_dead2_stop_HWBS","N1_Blade_dead_001")    --护卫镖师
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.499,1.5)
    Loop()

STake_Start("CS_down3_stop_ShuiShou","N1_life_025")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.7) 
    FrameTime(0,0.0001)
    Loop()

STake_Start("CS_down5_stop_ShuiShou","N1_life_034")  --吉永号上水手
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.0001)
    Loop()
    
    

-------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
    

--牛家村开头CS
--俞章廷配

STake_Start("CS_talk_emo_002_N1_Life","N1_XinShi_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_wait_emo_002_N1_Life","N1_XinShi_wait_000")  
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_wait_018_emo_N1_Life","N1_wait_018_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_life_026_N1","N1_life_026")  
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_LR01_emo_N1","N1_LR01_emo_000")  
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_WQS01_emo_N1","N1_WQS01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("CS_life_007_N1","N1_life_007")  
    BlendMode(0)
    BlendTime(0.2) 

-----------------------------------------------------------------------------------

--牛家村CUTSCENE用动作

--CS4金兵来袭

--张阳配


STake_Start("CS_C_emo_003","N1_C_emo_003")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_C_emo_004","N1_C_emo_004")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_life_017","N1_life_017")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_move_021","N1_move_021")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_move_022","N1_move_022")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait_008","N1_wait_008")  
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.001) 
    Loop()

STake_Start("CS_Blade_dead_001","N1_Blade_dead_001")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_Blade1_dead_001","N1_Blade1_dead_001")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_CM_CS_jblx_emo1","N1_CM_CS_jblx_emo1")  
    BlendMode(0)
    BlendTime(0) 
    Loop()

STake_Start("CS_CM_CS_jblx_emo1_01","N1_CM_CS_jblx_emo1")  
    BlendMode(0)
    BlendTime(0) 
    FrameTime(0.3,1.43)
    Loop(1.429,1.43)

STake_Start("CS_CM_CS_jblx_emo1_02","N1_CM_CS_jblx_emo1")  
    BlendMode(0)
    BlendTime(0) 
    FrameTime(1.43,1.6)
    PlaySpeed(1.43,1.6,0.5)
    Loop(1.599,1.6)

STake_Start("CS_dead_stop","N1_Blade_dead")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.799,1.8) 
    Loop()

STake_Start("CS_dead1_001_stop","N1_Blade1_dead_001")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.499,1.5) 
    Loop()

STake_Start("CS_hit_016_a","YP_hit_016")  
    BlendMode(0)
    BlendTime(0.4)
    FrameTime(0.013,0.373) 
    PlaySpeed(0.013,0.373,0.5)
    Loop(0.373,0.373)

STake_Start("CS_hit_016_b","YP_hit_016")  
    BlendMode(0)
    BlendTime(0)
    FrameTime(0.019,0.373) 
    PlaySpeed(0.019,0.373,0.22)
    Loop(0.373,0.373)

STake_Start("CS_hit_021_a","YP_hit_021")  
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,0.08) 
    PlaySpeed(0,0.08,0.4)
    Loop(0,0.08)

STake_Start("CS_hit_021_b","YP_hit_021")  
    BlendMode(0)
    BlendTime(0)
    FrameTime(0.08,0.224) 
    PlaySpeed(0.08,0.24,0.22)
    Loop(0.223,0.224)
package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")

STake_Start("N1_Staff_B_wait_000","N1_Staff_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_Staff_B_move_003","N1_Staff_B_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_Staff_B_move_004","N1_Staff_B_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()



STake_Start("N1_Staff_dead","N1_Staff_dead")
    BlendMode(0)
    BlendTime(0.2)
    
   
    Sound(1.749,"T1_Polearm_dead_000")

STake_Start("N1_Staff_dead_000","N1_Staff_dead_000")
    BlendMode(0)
    BlendTime(0.2)
    
    Sound(0.013,"SZ03_hurt_3")
    Sound(0.893,"T1_Polearm_dead_000")
    Sound(1.726,"DaoDi_001")



STake_Start("N1_Staff_dead_001","N1_Staff_dead_001")
    BlendMode(0)
    BlendTime(0.2)
    
   
    Sound(1.527,"T1_Polearm_dead_000")
    Sound(1.449,"DaoDi_001")

STake_Start("N1_Staff_dead_002","N1_Staff_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.292,"T1_Polearm_dead_000")
    Sound(1.207,"DaoDi_001")


STake_Start("N1_Staff_dead_003","N1_Staff_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.879,"T1_Polearm_dead_000")
    Sound(0.773,"DaoDi_001")

STake_Start("N1_Staff_def_000","N1_Staff_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.106,"T1_Polearm_def_000")

STake_Start("N1_Staff_dodge_000","N1_Staff_dodge_000")
    BlendMode(0)
    BlendTime(0.2) 

    Sound(0.019,"SZ03_hurt_3")

STake_Start("N1_Staff_emo_001","N1_Staff_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Staff_emo_002","N1_Staff_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Staff_emo_003","N1_Staff_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_Staff_move_000","N1_Staff_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_Staff_wait_000","N1_Staff_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()



package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")

STake_Start("N2_Arrow_B_wait_000","N2_Arrow_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_Arrow_B_move_003","N2_Arrow_B_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_Arrow_B_move_004","N2_Arrow_B_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
	
STake_Start("Monster_B_emo_001","N2_Arrow_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("N2_Arrow_dead","N2_Arrow_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.469,"N2_BYJQ01_deadVOC_001")
    Sound(0.117,"SZ03_dodge_000")
    Sound(1.671,"Big_Move_02")
    Sound(2.082,"Daodi_001")
   


STake_Start("N2_Arrow_dead_000","N2_Arrow_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.349,"N2_BYJQ01_deadVOC_001")
    Sound(0.166,"SZ03_dodge_000")
    Sound(0.916,"Daodi_001")
    Sound(0.916,"Big_Move_02")

    

STake_Start("N2_Arrow_dead_001","N2_Arrow_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.053,"N2_BYJQ01_deadVOC_001")
    Sound(0.053,"SZ03_dodge_000")
    Sound(0.586,"Daodi_001")
    Sound(0.586,"Big_Move_02")

STake_Start("N2_Arrow_dead_002","N2_Arrow_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.105,"N2_BYJQ01_deadVOC_001")
    Sound(0.105,"SZ03_dodge_000")
    Sound(0.450,"Daodi_001")
    Sound(0.450,"Big_Move_02")

STake_Start("N2_Arrow_dead_003","N2_Arrow_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    
    Sound(0.349,"N2_BYJQ01_deadVOC_001")
    Sound(0.166,"SZ03_dodge_000")
    Sound(0.916,"Daodi_001")
    Sound(0.916,"Big_Move_02")


STake_Start("N2_Arrow_def_000","N2_Arrow_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.086,"L1_Unarmed_def_000")
   
STake_Start("N2_Arrow_dodge_000","N2_Arrow_dodge_000")
    BlendMode(0)
    BlendTime(0.2)
    
    Sound(0.126,"L1_SBlade_dodge_000")
    Sound(0.288,"L1_SBlade_dodge_000")
    Sound(0.693,"L1_SBlade_emo_002_1")

STake_Start("N2_Arrow_emo_001","N2_Arrow_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N2_Arrow_emo_002","N2_Arrow_emo_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.617,"PC_LaGong_Duan")
    Sound(2.499,"PC_LaGong_long")
    Sound(3.381,"PC_Gong_shot")

STake_Start("N2_Arrow_move_000","N2_Arrow_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_Arrow_wait_000","N2_Arrow_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")

STake_Start("N2_Cast_wait_000","N2_Cast_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N2_Cast_B_wait_000","N2_Cast_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","N2_Cast_emo_001")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("N2_Cast_dead","N2_Cast_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.447,"Fighter_dead_VOC_000")
    Sound(1.016,"TongYong_DaoDi_005")
    Sound(1.341,"TongYong_DaoDi_007")
    Sound(1.545,"N1_Axe_def_000")
    Sound(1.595,"N1_Axe_def_000")
    Sound(1.504,"T1_Polearm_dead_000")

STake_Start("N2_Cast_dead_000","N2_Cast_dead_000")
    BlendMode(0)
    BlendTime(0.2)

  
    Sound(0.084,"Fighter_dead_VOC_000")
    Sound(0.420,"TongYong_DaoDi_007")
    Sound(0.882,"N1_Axe_def_000")
    Sound(0.896,"N1_Axe_def_000")
    Sound(0.854,"T1_Polearm_dead_000")

STake_Start("N2_Cast_dead_001","N2_Cast_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.251,"Fighter_dead_VOC_000")
    Sound(0.381,"TongYong_DaoDi_007")
    Sound(0.476,"N1_Axe_def_000")
    Sound(0.727,"N1_Axe_def_000")
    Sound(0.727,"T1_Polearm_dead_000")

STake_Start("N2_Cast_dead_002","N2_Cast_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.251,"Fighter_dead_VOC_000")
    Sound(0.381,"TongYong_DaoDi_007")
    Sound(0.476,"N1_Axe_def_000")
    Sound(0.727,"N1_Axe_def_000")
    Sound(0.727,"T1_Polearm_dead_000")

STake_Start("N2_Cast_dead_003","N2_Cast_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.084,"Fighter_dead_VOC_000")
    Sound(0.420,"TongYong_DaoDi_007")
    Sound(0.882,"N1_Axe_def_000")
    Sound(0.896,"N1_Axe_def_000")
    Sound(0.854,"T1_Polearm_dead_000")

STake_Start("N2_Cast_def_000","N2_Cast_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.007,"NB_TZD_def_000")

STake_Start("N2_Cast_dodge_000","N2_Cast_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.042,"NB_JB_dedge_000")

STake_Start("N2_Cast_emo_001","N2_Cast_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N2_Cast_emo_002","N2_Cast_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N2_Cast_move_000","N2_Cast_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N2_Cast_B_move_003","N2_Cast_B_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N2_Cast_B_move_004","N2_Cast_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N2_Cast_bout_001","N2_Cast_bout_001")
    BlendMode(0)
    BlendTime(0.2)



























package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("N2_life")Blend_Start("Upper")
     --SetBoneWeight("Head",0.01)
     SetBoneWeight("Spine2",1.0)
     SetBoneWeight("Spine",1.0)
     SetBoneWeight("Hips",0.0)
     SetBoneWeight("LeftArm",0.2)
     SetBoneWeight("RightArm",0.2)
Blend_End()

Blend_Start("Down")
     --SetBoneWeight("Head",0.99)
     SetBoneWeight("Spine1",0.0)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()
     
Blend_Start("no_sync")
     SetBoneWeight("Spine",2.0)
     SetBoneWeight("Spine1",2.0)
     SetBoneWeight("Hips",2.0)
     SetBoneWeight("Hipsd",2.0)
Blend_End()          

Blend_Start("wait")    
     SetBoneWeight("Spine1",0.01)
     SetBoneWeight("Spine",0.01)
     SetBoneWeight("Hips",0.01)     
     SetBoneWeight("Hipsd",0.01)
Blend_End()

Blend_Start("Upper")
     --SetBoneWeight("Head",0.01)
     SetBoneWeight("Spine1",1.0)
     SetBoneWeight("Spine",5.0)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("LeftArm",0.2)
     SetBoneWeight("RightArm",0.2)
Blend_End()

Blend_Start("Down")
     --SetBoneWeight("Head",0.99)
     SetBoneWeight("Spine1",0.0)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()

Blend_Start("no_sync")
     SetBoneWeight("Spine",2.0)
     SetBoneWeight("Spine1",2.0)
     SetBoneWeight("Hips",2.0)
     SetBoneWeight("Hipsd",2.0)
Blend_End()


Blend_Start("NB_GJ")
		 SetBoneWeight("T_L",0.01)
     SetBoneWeight("Spine2",1.0)
     SetBoneWeight("Spine",1.0)
     SetBoneWeight("Hips",0.0)
     SetBoneWeight("LeftArm",0.2)
     SetBoneWeight("RightArm",0.2)
Blend_End()


Blend_Start("Dragon")
     SetBoneWeight("Spine",2.0)
     SetBoneWeight("Spine1",2.0)
     SetBoneWeight("Hips",2.0)
Blend_End()


Blend_Start("UP_blend")
     SetBoneWeight("LeftArm",100000)
     SetBoneWeight("RightArm",100000)
     SetBoneWeight("RightShoulder",100000)
     SetBoneWeight("LeftShoulder",100000)
     SetBoneWeight("Hips",0)
     SetBoneWeight("Hipsd",0)	 
     SetBoneWeight("Neck",100000)	 
     SetBoneWeight("Spine",0)	 
     SetBoneWeight("Spine1",1)	 	 
     SetBoneWeight("Spine2",3)	 	 
Blend_End()-- animation
Skeleton("PC_GB2_new")
Animation("SG")
Animation("SG_PA")
Animation("SG_life")
Animation("SG_QZ_SW")
Animation("SG_QZ_PA")
Animation("SG_HIT")
Animation("SG_Arrow")
Animation("RideHorse_SG")
Animation("RideCamel_SG")
Animation("SG_FishingRod")
Animation("SG_GB_ST")
Animation("SG_GB_PA")
Animation("SG_THD_FL")
Animation("SG_THD_SW")
Animation("SG_THD_SW_NEW")
Animation("SG_BTS_CR")
Animation("SG_SL_ST")
Animation("N2_life")
Animation("QZ_Lsword")
Animation("RH_SG")
Animation("SG_CS02")
Animation("SG_CS_03")
Animation("SG_CS04")
Animation("nvBTS_FAN")
STake("total")

-- paperdoll
PaperDollsTable("Paperdoll")
--Equip("PC2_NeiYi01_Body","",1)
--Equip("PC2_NeiYi01_Glove","",2)
--Equip("PC2_NeiYi01_Helmet","",3)
--Equip("PC2_NeiYi01_Leg","",4)
--Equip("W_Crutch_02_A1","",5)

Dimension(19,41)
--BoundingBox(40,40,43,-40,-40,0,0,0,0)


-- blend pattern
BlendPattern("PC_female")

--FoldingBone("RightFoot",2.6,0,0)
--FoldingBone("LeftFoot",2.7,0,0)-- animation
Skeleton("PC_GB2_new")
Animation("SG")
Animation("SG_PA")
Animation("SG_life")
Animation("SG_QZ_SW")
Animation("SG_QZ_PA")
Animation("SG_HIT")
Animation("SG_Arrow")
Animation("RideHorse_SG")
Animation("RideCamel_SG")
Animation("SG_FishingRod")
Animation("SG_GB_ST")
Animation("SG_GB_PA")
Animation("SG_THD_FL")
Animation("SG_THD_SW")
Animation("SG_THD_SW_NEW")
Animation("SG_SL_ST")
Animation("N2_life")
Animation("QZ_Lsword")
Animation("RH_SG")
Animation("SG_CS02")
Animation("SG_CS_03")
Animation("SG_CS04")
Animation("SG_CS06")
Animation("N1_life01")
STake("total")

-- paperdoll
PaperDollsTable("Paperdoll")
--Equip("PC2_NeiYi01_Body","",3)
--Equip("PC2_NeiYi01_Glove","",2)
--Equip("PC2_NeiYi01_Helmet","",5)
--Equip("PC2_NeiYi01_Leg","",4)
--Equip("W_LongStick_01_A1","",1)




Dimension(19,41)
--BoundingBox(40,40,43,-40,-40,0,0,0,0)


-- blend pattern
BlendPattern("PC_female")

-- animation
Skeleton("PC_GB2_new")
Animation("SG")
Animation("SG_PA")
Animation("SG_life")
Animation("SG_QZ_SW")
Animation("SG_QZ_PA")
Animation("SG_HIT")
Animation("SG_Arrow")
Animation("RideHorse_SG")
Animation("RideCamel_SG")
Animation("SG_FishingRod")
Animation("SG_GB_ST")
Animation("SG_GB_PA")
Animation("SG_THD_FL")
Animation("SG_THD_SW")
Animation("SG_THD_SW_NEW")
Animation("SG_SL_ST")
Animation("N2_life")
Animation("QZ_Lsword")
Animation("RH_SG")
Animation("SG_CS02")
Animation("SG_CS_03")
Animation("SG_CS04")
Animation("SG_CS06")
Animation("N1_life01")
STake("total")

-- paperdoll
PaperDollsTable("Paperdoll")
--Equip("PC2_NeiYi01_Body","",1)
--Equip("PC2_NeiYi01_Glove","",2)
--Equip("PC2_NeiYi01_Helmet","",3)
--Equip("PC2_NeiYi01_Leg","",4)
--Equip("W_Lsword_01_A1","",5)




Dimension(19,41)
--BoundingBox(40,40,43,-40,-40,0,0,0,0)


-- blend pattern
BlendPattern("PC_female")

--FoldingBone("RightFoot",3.0,0,0)
--FoldingBone("LeftFoot",3.0,0,0)-- animation
Skeleton("PC_GB2_new")
Animation("SG")
Animation("SG_PA")
Animation("SG_life")
Animation("SG_QZ_SW")
Animation("SG_QZ_PA")
Animation("SG_HIT")
Animation("SG_Arrow")
Animation("RideHorse_SG")
Animation("RideCamel_SG")
Animation("SG_FishingRod")
Animation("SG_GB_ST")
Animation("SG_GB_PA")
Animation("SG_THD_FL")
Animation("SG_THD_SW")
Animation("SG_THD_SW_NEW")
Animation("SG_SL_ST")
Animation("N2_life")
Animation("QZ_Lsword")
Animation("RH_SG")
Animation("SG_CS02")
Animation("SG_CS_03")
Animation("SG_CS04")
Animation("SG_CS06")
Animation("N1_life01")
STake("total")

-- paperdoll
PaperDollsTable("Paperdoll")
--Equip("PC2_NeiYi01_Body","",3)
--Equip("PC2_NeiYi01_Glove","",2)
--Equip("PC2_NeiYi01_Helmet","",5)
--Equip("PC2_NeiYi01_Leg","",4)
--Equip("W_LongStick_01_A1","",1)




Dimension(19,41)
--BoundingBox(40,40,43,-40,-40,0,0,0,0)


-- blend pattern
BlendPattern("PC_female")

--FoldingBone("RightFoot",2.2,0,0)
--FoldingBone("LeftFoot",3.2,0,0)-- animation
Skeleton("PC_GB2_new")
Animation("SG")
Animation("SG_PA")
Animation("SG_life")
Animation("SG_QZ_SW")
Animation("SG_QZ_PA")
Animation("SG_HIT")
Animation("SG_Arrow")
Animation("RideHorse_SG")
Animation("RideCamel_SG")
Animation("SG_FishingRod")
Animation("SG_GB_ST")
Animation("SG_GB_PA")
Animation("SG_THD_FL")
Animation("SG_THD_SW")
Animation("SG_THD_SW_NEW")
Animation("SG_SL_ST")
Animation("N2_life")
Animation("QZ_Lsword")
Animation("RH_SG")
Animation("SG_CS02")
Animation("SG_CS_03")
Animation("SG_CS04")
Animation("SG_CS06")
Animation("N1_life01")
STake("total")

-- paperdoll
PaperDollsTable("Paperdoll")
--Equip("PC2_NeiYi01_Body","",1)
--Equip("PC2_NeiYi01_Glove","",2)
--Equip("PC2_NeiYi01_Helmet","",3)
--Equip("PC2_NeiYi01_Leg","",4)
--Equip("W_Ssword_03_A1","",5)




Dimension(19,41)
--BoundingBox(40,40,43,-40,-40,0,0,0,0)


-- blend pattern
BlendPattern("PC_female")

--FoldingBone("RightFoot",2.2,0,0)
--FoldingBone("LeftFoot",3.2,0,0)Blend_Start("Upper_hurt")
     SetBoneWeight("Hips",0)            
     SetBoneWeight("Spine1",5)
     SetBoneWeight("Spine",1)
     SetBoneWeight("Hipsd",0)
Blend_End()

Blend_Start("Upper_att")
     SetBoneWeight("Spine1",1.0)
     SetBoneWeight("Spine",1.0)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()

Blend_Start("wait")    
     SetBoneWeight("Spine1",0.01)
     SetBoneWeight("Spine",0.01)
     SetBoneWeight("Hips",0.01)     
     SetBoneWeight("Hipsd",0.01)
Blend_End()

Blend_Start("wait_none")    
     SetBoneWeight("Spine1",0.0000001)
     SetBoneWeight("Spine",0.0000001)
     SetBoneWeight("Hips",0.0000001)     
     SetBoneWeight("Hipsd",0.0000001)
	 SetBoneWeight("LeftArm",0.0000001)
     SetBoneWeight("RightArm",0.0000001)
     SetBoneWeight("RightShoulder",0.0000001)
     SetBoneWeight("LeftShoulder",0.0000001)
     SetBoneWeight("Neck",0.0000001)	 
     SetBoneWeight("Spine2",0.0000001)	 
Blend_End()


Blend_Start("Down")
     --SetBoneWeight("Head",0.99)
     SetBoneWeight("Spine1",0.5)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()
     
     
Blend_Start("no_sync")
     SetBoneWeight("Spine",2.0)
     SetBoneWeight("Spine1",2.0)
     SetBoneWeight("Hips",2.0)
     SetBoneWeight("Hipsd",2.0)
Blend_End()     

Blend_Start("right_left_move")
     SetBoneWeight("Spine",1.0)
     SetBoneWeight("Spine1",1.0)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()  

Blend_Start("Arrow_attack")
     SetBoneWeight("LeftArm",1.0)
     SetBoneWeight("RightArm",1.0)
     --SetBoneWeight("Spine",1.0)
     --SetBoneWeight("Spine1",1.0)
     SetBoneWeight("Hips",0.1)
     SetBoneWeight("Hipsd",0.1)
     SetBoneWeight("Reference",0.0)
Blend_End()  

Blend_Start("Arrow_move")
     SetBoneWeight("LeftArm",0.1)
     SetBoneWeight("RightArm",0.1)
     --SetBoneWeight("Spine",0.1)
     --SetBoneWeight("Spine1",0.1)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()  

Blend_Start("Arrow_jump")
     SetBoneWeight("LeftArm",0.01)
     SetBoneWeight("RightArm",0.01)
     --SetBoneWeight("Spine",1.00)
     --SetBoneWeight("Spine1",1.00)
     SetBoneWeight("Hips",1.00)
     SetBoneWeight("Hipsd",1.00)
Blend_End()  


Blend_Start("air_attack")
     --SetBoneWeight("RightShoulder",10)
     --SetBoneWeight("LeftShoulder",10)
     --SetBoneWeight("Spine2",0.3)
     SetBoneWeight("Neck",3)
     SetBoneWeight("Head",3)
     SetBoneWeight("Spine",10)
     --SetBoneWeight("Spine2",3)
     SetBoneWeight("Hips",10)
     SetBoneWeight("Hipsd",10)
     SetBoneWeight("LeftUpLeg",0)
     SetBoneWeight("RightUpLeg",0)
     SetBoneWeight("RightLeg",0)
     SetBoneWeight("LeftLeg",0)
     SetBoneWeight("d_Cskirt01",0)
     SetBoneWeight("B_Bskirt01",0)
     SetBoneWeight("B_Fskirt01",0)
Blend_End()  


Blend_Start("rush_attack")
     SetBoneWeight("LeftArm",100000)
     SetBoneWeight("RightArm",100000)
     SetBoneWeight("RightShoulder",100000)
     SetBoneWeight("LeftShoulder",100000)
     SetBoneWeight("Hips",0.6)
     SetBoneWeight("Hipsd",0.0001)	 
     SetBoneWeight("Neck",1000)	 
     SetBoneWeight("Spine",100000)	 
     SetBoneWeight("Spine1",100000)	 	 
Blend_End() 


Blend_Start("steady")
     SetBoneWeight("Hips",1000)
     SetBoneWeight("Hipsd",10000)
     SetBoneWeight("LeftUpLeg",10000)
     SetBoneWeight("RightUpLeg",10000)
     SetBoneWeight("RightLeg",10000)
     SetBoneWeight("LeftLeg",10000)
     SetBoneWeight("d_Cskirt01",10000)
     SetBoneWeight("B_Bskirt01",10000)
     SetBoneWeight("B_Fskirt01",10000)
Blend_End()  

Blend_Start("steady_charging")
     SetBoneWeight("Hips",1000000)
     SetBoneWeight("Hipsd",1000000)
     SetBoneWeight("LeftUpLeg",1000000)
     SetBoneWeight("RightUpLeg",1000000)
     SetBoneWeight("RightLeg",1000000)
     SetBoneWeight("LeftLeg",1000000)
     SetBoneWeight("d_Cskirt01",1000000)
     SetBoneWeight("B_Bskirt01",1000000)
     SetBoneWeight("B_Fskirt01",1000000)
Blend_End() 



Blend_Start("moveable_laser")
     SetBoneWeight("Hips",0.01)
     SetBoneWeight("Hipsd",0.01)
     SetBoneWeight("LeftUpLeg",0.01)
     SetBoneWeight("RightUpLeg",0.01)
     SetBoneWeight("RightLeg",0.01)
     SetBoneWeight("LeftLeg",0.01)
     SetBoneWeight("d_Cskirt01",0.01)
     SetBoneWeight("B_Bskirt01",0.01)
     SetBoneWeight("B_Fskirt01",0.01)
Blend_End() 

Blend_Start("QZ_stars")
     SetBoneWeight("LeftArm",100000)
     SetBoneWeight("RightArm",100000)
     SetBoneWeight("RightShoulder",100000)
     SetBoneWeight("LeftShoulder",100000)
     SetBoneWeight("Hips",0.0001)
     SetBoneWeight("Hipsd",0.0001)	 
     SetBoneWeight("Neck",0.0001)	 
     SetBoneWeight("Spine",0.0001)	 
     SetBoneWeight("Spine1",0.0001)	 	 
     SetBoneWeight("Spine2",0.0001)	 
Blend_End()  
Blend_Start("Upper_hurt")
     SetBoneWeight("Hips",0)            
     SetBoneWeight("Spine1",5)
     SetBoneWeight("Spine",1)
     SetBoneWeight("Hipsd",0)
Blend_End()

Blend_Start("Upper_att")

     SetBoneWeight("Spine1",1.0)
     SetBoneWeight("Spine",1.0)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()

Blend_Start("wait")    
     SetBoneWeight("Spine1",0.01)
     SetBoneWeight("Spine",0.01)
     SetBoneWeight("Hips",0.01)     
     SetBoneWeight("Hipsd",0.01)
Blend_End()

Blend_Start("Down")
     --SetBoneWeight("Head",0.99)
     SetBoneWeight("Spine1",0.5)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()
     
     
Blend_Start("no_sync")
     SetBoneWeight("Spine",2.0)
     SetBoneWeight("Spine1",2.0)
     SetBoneWeight("Hips",2.0)
     SetBoneWeight("Hipsd",2.0)
Blend_End()     

Blend_Start("right_left_move")
     SetBoneWeight("Spine",1.0)
     SetBoneWeight("Spine1",1.0)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()  


Blend_Start("Arrow_attack")
     SetBoneWeight("LeftArm",1.0)
     SetBoneWeight("RightArm",1.0)
     --SetBoneWeight("Spine",1.0)
     --SetBoneWeight("Spine1",1.0)
     SetBoneWeight("Hips",0.1)
     SetBoneWeight("Hipsd",0.1)
     SetBoneWeight("Reference",0.0)
Blend_End()  

Blend_Start("Arrow_move")
     SetBoneWeight("LeftArm",0.1)
     SetBoneWeight("RightArm",0.1)
     --SetBoneWeight("Spine",0.1)
     --SetBoneWeight("Spine1",0.1)
     SetBoneWeight("Hips",1.0)
     SetBoneWeight("Hipsd",1.0)
Blend_End()  

Blend_Start("Arrow_jump")
     SetBoneWeight("LeftArm",0.01)
     SetBoneWeight("RightArm",0.01)
     --SetBoneWeight("Spine",1.00)
     --SetBoneWeight("Spine1",1.00)
     SetBoneWeight("Hips",1.00)
     SetBoneWeight("Hipsd",1.00)
Blend_End()  

Blend_Start("air_attack")
     --SetBoneWeight("LeftArm",10)
     --SetBoneWeight("RightArm",10)
     SetBoneWeight("Spine",10)
     --SetBoneWeight("Spine1",10)
     SetBoneWeight("Hips",0.1)
     SetBoneWeight("Hipsd",0.1)
     --SetBoneWeight("Reference",0.0)
Blend_End()  

Blend_Start("rush_attack")
     SetBoneWeight("LeftArm",100000)
     SetBoneWeight("RightArm",100000)
     SetBoneWeight("RightShoulder",100000)
     SetBoneWeight("LeftShoulder",100000)
     SetBoneWeight("Hips",0.6)
     SetBoneWeight("Hipsd",0.0001)	 
     SetBoneWeight("Neck",1000)	 
     SetBoneWeight("Spine",100000)	 
     SetBoneWeight("Spine1",100000)	 	 
Blend_End() 


Blend_Start("steady")
     SetBoneWeight("Hips",1000)
     SetBoneWeight("Hipsd",10000)
     SetBoneWeight("LeftUpLeg",10000)
     SetBoneWeight("RightUpLeg",10000)
     SetBoneWeight("RightLeg",10000)
     SetBoneWeight("LeftLeg",10000)
     SetBoneWeight("d_Cskirt01",10000)
     SetBoneWeight("B_Bskirt01",10000)
     SetBoneWeight("B_Fskirt01",10000)
Blend_End() 





-- animation
Skeleton("PC_GB1")
Animation("HG")
Animation("HG_PA")
Animation("HG_life")
Animation("GB_ST")
Animation("GB_PA")
Animation("HIT")
Animation("HG_Arrow")
Animation("RideHorse_HG")
Animation("RideCamel_HG")
Animation("HG_FishingRod")
Animation("N1_life01")
Animation("N1_life02")
Animation("N1_life03")
Animation("THD_FL")
Animation("THD_SW")
Animation("SL_ST")
Animation("SL_SB")
Animation("HG_SL_ST")
Animation("QZ_SW")
Animation("QZ_PA")
Animation("RH_HG")
Animation("QZ_SW_NEW")
Animation("HG_CS02")
Animation("HG_CS_03")
Animation("HG_CS04")
Animation("HG_CS06")
STake("total")


-- paperdoll
PaperDollsTable("Paperdoll")
--Equip("PCM_009","",1)




Dimension(19,43)
BoundingBox(40,40,43,-40,-40,0,0,0,0)


-- blend pattern
BlendPattern("PC_male")

--FoldingBone("RightFoot",3.0,0,0)
--FoldingBone("LeftFoot",3.0,0,0)package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("N1_life")

STake_Start("S1_wait_000","S1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S1_wait_001","S1_wait_001")  --趴在地上的老年乞丐
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S1_wait_001_emo_001","S1_wait_001_emo_001")  --趴在地上的老年乞丐痛苦表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_wait_001_emo_002","S1_wait_001_emo_002")  --趴在地上的老年乞丐磕头
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_wait_002","S1_wait_002")  --卖糖葫芦的老人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S1_wait_002_emo_001","S1_wait_002_emo_001")  --卖糖葫芦的老人吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_wait_002_emo_002","S1_wait_002_emo_002")  --卖糖葫芦的老人捶背
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_wait_002_move_001","S1_wait_002_move_001")  --卖糖葫芦的老人走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S1_ZFLB_wait_000","S1_ZFLB_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S1_ZFLB_emo_000","S1_ZFLB_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_ZFLB_emo_001","S1_ZFLB_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_TBYY_wait_000","S1_TBYY_wait_000")  --驼背爷爷
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    Fx(0,"N_S1_TBYY_wait_000")

STake_Start("S1_TBYY_emo_000","S1_TBYY_emo_000")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0,"N_S1_TBYY_emo_000")

STake_Start("S1_TBYY_emo_001","S1_TBYY_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0,"N_S1_TBYY_emo_001")

STake_Start("S1_DC01_wait_000","S1_DC01_wait_000")  --大厨01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S1_DC01_emo_000","S1_DC01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_DC01_emo_001","S1_DC01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_MGSR_wait_000","S1_MGSR_wait_000")  --蒙古商人
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("S1_MGSR_emo_001","S1_MGSR_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_MGSR_emo_002","S1_MGSR_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("S1_SBA_wait_000","S1_SBA_wait_000")  --村长萨本安
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("S1_SBA_emo_001","S1_SBA_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S1_SBA_emo_002","S1_SBA_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("S1_ZFLB_move_000","S1_ZFLB_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

	
STake_Start("S1_MengGuShangRen_dead_000","YP_dead_002")  --死亡待机
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.33,1.344)
	Loop()
	
STake_Start("N1_wait_027_new","N1_wait_027")  --大厨01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

---------------------------------------------------------------------------------------------------------------------------------------
--CutScene用

--塞外古城

STake_Start("CS_walk","S1_ZFLB_move_000")  --蒙古商人
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("CS_look","S1_ZFLB_emo_000")  --蒙古商人
    BlendMode(0)
    BlendTime(0.2)
    Loop() 


STake_Start("CS_wait_MGSR","S1_MGSR_wait_000")  --蒙古商人
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("CS_wait","S1_wait_002")  --老乞丐
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("CS_ChuiYao","S1_wait_002_emo_002")  --老乞丐
    BlendMode(0)
    BlendTime(0.2)
    Loop() 


STake_Start("CS_slow_escape","S1_wait_002_move_001")  --老乞丐
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("CS_beg1_LQG","S1_wait_001")  --老乞丐
    BlendMode(0)
    BlendTime(0.2)
    Loop() 


STake_Start("CS_beg2_LQG","S1_wait_001_emo_002")  --老乞丐
    BlendMode(0)
    BlendTime(0.2)
    Loop() 


-------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_S1_DC01_emo_000","S1_DC01_emo_000")  --大厨01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_N1_MGSR_move_000","N1_MGSR_move_000")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_N1_MGSR_def_000","N1_MGSR_def_000")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")

STake_Start("S2_wait_000","S2_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S2_GZLF_wait_000","S2_GZLF_wait_000")  --拐杖老妇01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S2_GZLF_emo_000","S2_GZLF_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S2_GZLF_emo_001","S2_GZLF_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S2_GZLF_move_000","S2_GZLF_move_000")  --走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S2_MP_wait_000","S2_MP_wait_000")  --媒婆01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("S2_MP_emo_001","S2_MP_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S2_MP_emo_002","S2_MP_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("S2_MP_emo_003","S2_MP_emo_003")
    BlendMode(0)
    BlendTime(0.2) 










-------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_S2_GZLF_emo_000","S2_GZLF_emo_000")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("CS_S2_GZLF_emo_001","S2_GZLF_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

----------------------
--Cutscene用
----------------------
STake_Start("CS_move_000_S2_GZLF","S2_GZLF_move_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("CS_emo_003_S2_MP","S2_MP_emo_003")
    BlendMode(0)
    BlendTime(0.2)package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("Scorpion_hurt")



STake_Start("Scorpion_XZW01_B_wait_000","Scorpion_XZW01_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("Scorpion_XZW01_B_move_004","Scorpion_XZW01_B_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    Sound(0.010,"Scorpion_XZW01_B_Move_001")
    Sound(0.229,"Scorpion_XZW01_B_Move_001")


STake_Start("Scorpion_XZW01_B_move_005","Scorpion_XZW01_B_move_005")---战斗左侧移
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    Fx(0,"Scorpion_XZW01_bout_001","",0.3,1,-11,3,0)
    Fx(0,"Scorpion_XZW01_bout_001","",0.2,1,6,3,0)
    Sound(0.010,"Scorpion_XZW01_B_Move_001")
    Sound(0.229,"Scorpion_XZW01_B_Move_001")

STake_Start("Scorpion_XZW01_B_move_006","Scorpion_XZW01_B_move_006")---战斗右侧移
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    Fx(0,"Scorpion_XZW01_bout_001","",0.3,1,-6,3,0)
    Fx(0,"Scorpion_XZW01_bout_001","",0.2,1,11,3,0)
    Sound(0.010,"Scorpion_XZW01_B_Move_001")
    Sound(0.229,"Scorpion_XZW01_B_Move_001")

STake_Start("Scorpion_XZW01_B_move_007","Scorpion_XZW01_B_move_007")---战斗左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    Sound(0.010,"Scorpion_XZW01_B_Move_001")
    Sound(0.229,"Scorpion_XZW01_B_Move_001")

STake_Start("Scorpion_XZW01_B_move_008","Scorpion_XZW01_B_move_008")---战斗右踏转
    BlendMode(0) 
    BlendTime(0.2) 
    Loop()

    Sound(0.010,"Scorpion_XZW01_B_Move_001")
    Sound(0.229,"Scorpion_XZW01_B_Move_001")

    
    

STake_Start("Scorpion_XZW01_dead_000","Scorpion_XZW01_dead_000")
    BlendMode(0)
    BlendTime(0.2) 

    Sound(1.206,"Scorpion_XZW01_Dead_000")
    Sound(0.378,"Scorpion_XZW01_DeadVOC_001")

STake_Start("Scorpion_XZW01_dead_001","Scorpion_XZW01_dead_001")
    BlendMode(0)
    BlendTime(0.2) 

    Sound(0.050,"Scorpion_XZW01_Dead_001")
    Sound(0.050,"Scorpion_XZW01_DeadVOC_002")

STake_Start("Scorpion_XZW01_dodge_000","Scorpion_XZW01_dodge_000")
    BlendMode(0)
    BlendTime(0.2) 

    Sound(0.155,"Scorpion_XZW01_B_Move_001")
    Sound(0.675,"Scorpion_XZW01_B_Move_001")

STake_Start("Monster_emo_001","Scorpion_XZW01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

   Sound(0.458,"Scorpion_XZW01_Emo_001")
   Sound(0.494,"Scorpion_XZW01_Emo_001_1")
   Sound(1.521,"Scorpion_XZW01_Emo_001_1")

STake_Start("Monster_emo_002","Scorpion_XZW01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

   Sound(0.320,"Scorpion_XZW01_Emo_002")
   Sound(0.120,"Scorpion_XZW01_B_Move_001")
   Sound(1.521,"Scorpion_XZW01_B_Move_001")

STake_Start("Monster_B_emo_001","Scorpion_XZW01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

   Sound(0.458,"Scorpion_XZW01_Emo_001")
   Sound(0.494,"Scorpion_XZW01_Emo_001_1")
   Sound(1.521,"Scorpion_XZW01_Emo_001_1")



STake_Start("Scorpion_XZW01_hit_000","Scorpion_XZW01_hit_000")
    BlendMode(0)
    BlendTime(0.2) 

   Sound(0.051,"Scorpion_XZW01_B_Move_001")
   Sound(0.359,"Scorpion_XZW01_B_Move_001")
   Sound(0.001,"Scorpion_XZW01_AttackVOC_1_2")

STake_Start("Scorpion_XZW01_hit_006","Scorpion_XZW01_hit_006")
    BlendMode(0)
    BlendTime(0.2) 

   Sound(0.110,"Scorpion_XZW01_B_Move_001")
   Sound(0.690,"Scorpion_XZW01_B_Move_001")
   Sound(0.001,"Scorpion_XZW01_AttackVOC_1_2")

STake_Start("Scorpion_XZW01_hit_011","Scorpion_XZW01_hit_011")
    BlendMode(0)
    BlendTime(0.2) 
    
    Charge(0.000,0.277,30,0,5,0.5)

   Sound(0.233,"Scorpion_XZW01_B_Move_001")
   Sound(0.562,"Scorpion_XZW01_B_Move_001")
   Sound(0.077,"Scorpion_XZW01_AttackVOC_1_2")

STake_Start("Scorpion_XZW01_hit_016","Scorpion_XZW01_hit_016")
    BlendMode(0)
    BlendTime(0.2) 

    Charge(0.000,0.542,100,0,5,1.0)

   Sound(0.099,"Scorpion_XZW01_Hurt_016")
   Sound(0.562,"Scorpion_XZW01_B_Move_001")
   Sound(0.033,"Scorpion_XZW01_HurtVOC")

STake_Start("Scorpion_XZW01_move_004","Scorpion_XZW01_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

   Sound(0.053,"Scorpion_XZW01_Emo_001_1")
   Sound(0.426,"Scorpion_XZW01_Emo_001_1")

STake_Start("Scorpion_XZW01_wait_000","Scorpion_XZW01_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("Scorpion_XZW01_skill_000","Scorpion_XZW01_skill_000")
    BlendMode(0)
    BlendTime(0.2) 

   Sound(0.140,"Scorpion_XZW01_Skill_000")
   Sound(0.720,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_dead_001_1","Scorpion_XZW01_dead_001")  --献祭
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"1")   
    FrameTime(0,2.666)
	  Hard(0.000,1.00)  	

    Sound(0.050,"Scorpion_XZW01_Dead_001")
    Sound(0.050,"Scorpion_XZW01_DeadVOC_002")
    
  	AttackStart()
  	Hurt(0.000,"hurt_21")  
    
    FadeOut(0,0.333,0.1)    
    
    Fx(0.300,"HG_BTS_baozha_zi","",1,0,0,0,0,0,0)    
    Fx(0.01,"HG_BTS_FAN_skill_xianji")    
   
   
STake_Start("Scorpion_XZW01_skill_001","Scorpion_XZW01_skill_001")  --蝎王穿刺
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   
	FrameTime(0,2.333)
    --BlendPattern("no_sync")       		
    
		--Hard(0.000,2.000)
		  		
  	AttackStart()
  	Hurt(0.676,"hurt_21")

    Fx(0.000,"Scorpion_XZW01_skill_001_fire")
    DirFx(0.75,"Scorpion_XZW01_skill_001_2_hit",0,0)
    HurtBoneFx(0.75,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
   Sound(0.050,"Scorpion_XZW01_Skill_001")
   Sound(0.116,"Scorpion_XZW01_AttackVOC_1_1")
   HurtSound(0.676,"Scorpion_XZW01_Hit_001")


STake_Start("Scorpion_XZW01_attack_long_start","Scorpion_XZW01_skill_000")  --技能  26025  尾巴扫  引导1.6  动作总长2
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   
	FrameTime(0,2)
    PlaySpeed(0,0.6,0.416)
	--PlaySpeed(0.820,0.822,0.012)
	Pause(0.820,0.16,1,0.012)

    Fx(0.7,"Scorpion_XZW01_attack_000_fire_a","",1.5)
    Fx(0.7,"Fx_common_skill_ground_stone_Rbanyuan","Reference",1,1,0,0,0,-160,0)

   Sound(0.25,"Scorpion_XZW01_Skill_001")
   Sound(0.25,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_attack_long_fire","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")  
	Pause(0.820,0.16,1,0.012)	

  	AttackStart()
  	Hurt(0.06,"hurt_21")
    DirFx(0.06,"Scorpion_XZW01_skill_002_hit_a",0,1,1,-90,0,0)
	HurtBoneFx(0.06,"FX_blood_zhua_b","Spine1",1,0,0,0,-90,0,0)
    HurtSound(0.06,"Scorpion_XZW01_Hit_001")


STake_Start("Scorpion_XZW01_skill_002_start","Scorpion_XZW01_skill_002")  --技能  26031  尾巴扎前方  引导1.6  动作总长2.666
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,2.666)
    Fx(0.0,"Scorpion_XZW01_skill_002_loop_a","",1) 
    Fx(1.2,"Scorpion_XZW01_skill_002_fire_a","",1)  
    Sound(1.4,"Scorpion_XZW01_Skill_002")
    Sound(1.4,"Scorpion_XZW01_AttackVOC_1_2")

STake_Start("Scorpion_XZW01_skill_002_fire","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   

    AttackStart()
    Hurt(0.01,"hurt_21")
    DirFx(0.01,"Scorpion_XZW01_skill_002_hit_a",0,1,1,0,0,0)
	HurtBoneFx(0.01,"FX_blood_zhua_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.01,"Scorpion_XZW01_Hit_001")


STake_Start("Scorpion_XZW01_skill_001_2_start","Scorpion_XZW01_skill_001_2")  --技能  11166  尾巴插地  引导1.6  动作总长3.833
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0,3.833)
    Fx(0.000,"Scorpion_XZW01_skill_001_2_loop")
    Fx(1.6,"Scorpion_XZW01_skill_001_2_fire")
    Sound(0.6,"Scorpion_XZW01_Skill_001")
    Sound(0.6,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_skill_001_2_fire","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

   AttackStart()
   Hurt(0.01,"hurt_81")
   DirFx(0.01,"Scorpion_XZW01_skill_001_2_hit",0,1)
   HurtBoneFx(0.01,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
   HurtSound(0.01,"Scorpion_XZW01_Hit_001")

 
STake_Start("Scorpion_XZW01_skill_004_start","Scorpion_XZW01_skill_004")  --技能  11160/11161  左挥抓  引导1.6  动作总长3.333
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0,3.333)
    Fx(1.6,"Scorpion_XZW01_attack_001_fire","Reference",1,1,0,0,0,0,0,0,1.5)

    Sound(1.6,"Scorpion_XZW01_Attack_001")
    Sound(1.6,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_skill_004_fire","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

    AttackStart()
    Charge(0.1,0.36,120,0,4)
    Hurt(0.23,"hurt_21")
    DirFx(0.23,"Scorpion_XZW01_skill_001_2_hit",0,1)
	HurtBoneFx(0.23,"FX_blood_zhua_b","Spine1",1,0,0,0,90,0,0)
    HurtSound(0.23,"Scorpion_XZW01_Hit_000")


STake_Start("Scorpion_XZW01_skill_005_start","Scorpion_XZW01_skill_005")  --技能  11162/11163  右抓抓  引导1.6  动作总长3.333
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0,3.333)
    Fx(1.65,"Scorpion_XZW01_attack_000_fire","Reference",1,1,0,0,0,0,0,0,1.5)
    Sound(1.65,"Scorpion_XZW01_Attack_001")
    Sound(1.65,"Scorpion_XZW01_AttackVOC_1_1")


STake_Start("Scorpion_XZW01_skill_005_fire","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

    AttackStart()
    Charge(0.1,0.33,120,0,4)
    Hurt(0.23,"hurt_21")
    DirFx(0.23,"Scorpion_XZW01_skill_001_2_hit",0,1,1,-90,0,0)
	HurtBoneFx(0.23,"FX_blood_zhua_b","Spine1",1,0,0,0,-90,0,0)
    HurtSound(0.23,"Scorpion_XZW01_Hit_000")


STake_Start("Scorpion_XZW01_B_move_012","Scorpion_XZW01_B_move_012")  --蝎子钻地
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,1.2)
    PlaySpeed(0.76,1.1,0.1)   
    Sound(0.466,"Scorpion_XZW01_Bout_002")
    Fx(0,"Scorpion_XZW01_bout_002")

STake_Start("Scorpion_XZW01_B_move_011_start","Scorpion_XZW01_B_move_011")  --蝎子出来start
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,1.5)
    Sound(0.466,"Scorpion_XZW01_Bout_002")
    Fx(0,"Scorpion_XZW01_bout_001")

STake_Start("Scorpion_XZW01_B_move_011_fire","")  --蝎子出来fire
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

    AttackStart()

    Hurt(0.01,"hurt_21")
    DirFx(0.01,"Scorpion_XZW01_skill_001_2_hit",0,1)
    HurtBoneFx(0.01,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)	
    HurtSound(0.01,"Scorpion_XZW01_Hit_000")


STake_Start("Scorpion_XZW01_catch_try","Scorpion_XZW01_skill_007(5-35)")  --蝎子没戳中人
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,1.35)  

STake_Start("Scorpion_XZW01_catch_failed","Scorpion_XZW01_skill_007(5-35)")  --蝎子没戳中人
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(1.35,2.5)  

STake_Start("Scorpion_XZW01_catch_success","Scorpion_XZW01_skill_006(5-35)")  --蝎子戳中人
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(1.35,4.866)  
	Hurt(0.2,"hurt_21")
	Hurt(0.3,"hurt_21")
	Hurt(0.4,"hurt_21")
	Hurt(0.5,"hurt_21")
	Hurt(0.65,"hurt_21")
	Hurt(0.75,"hurt_21")
	Hurt(0.85,"hurt_21")
	Hurt(0.95,"hurt_21")


STake_Start("Scorpion_XZW01_bout_002","Scorpion_XZW01_bout_002")  --蝎子慢速钻地
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,2.666)
    PlaySpeed(2.15,2.3,0.05)   
    Sound(0.466,"Scorpion_XZW01_Bout_002")
    Fx(0.5,"Scorpion_XZW01_B_move_012")

STake_Start("Scorpion_XZW01_bout_001_start","Scorpion_XZW01_bout_001")  --蝎子慢速出来start
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,2)
    PlaySpeed(0.05,0.6,0.5)  
    Sound(0.466,"Scorpion_XZW01_Bout_002")
    Fx(0,"Scorpion_XZW01_bout_001")

STake_Start("Scorpion_XZW01_bout_001_fire","")  --蝎子慢速出来fire
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

    AttackStart()
    Hurt(0.01,"hurt_81")
    DirFx(0.01,"Scorpion_XZW01_skill_001_2_hit",0,1)
    HurtBoneFx(0.01,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)	
    HurtSound(0.01,"Scorpion_XZW01_Hit_000")



STake_Start("Scorpion_XZW01_bout_001","Scorpion_XZW01_bout_001")  --蝎子出生
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,2)
    PlaySpeed(0.05,0.6,0.5)  
    Sound(0.466,"Scorpion_XZW01_Bout_002")
    Fx(0,"Scorpion_XZW01_bout_001")


STake_Start("Scorpion_XZW01_skill_008","Scorpion_XZW01_skill_008")  --技能  26048  沙王穿刺  引导1.6charge0.22  动作总长2.299
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,2.299)
	PlaySpeed(0,1.3,0.812)
    Fx(0,"")
	
	AttackStart()
	Hurt(1.71,"hurt_81")
	DirFx(1.71,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(1.71,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.6,"ShakeDistance = 300","ShakeMode = 3","ShakeTimes = 0.22","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")



STake_Start("Scorpion_XZW01_skill_009","Scorpion_XZW01_skill_009(62-95)")  --技能  26050  沙王震地  引导5均匀引导0.5秒一次  动作总长4.8
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,4.8)
	PlaySpeed(0,1.6,0.32)
    Fx(0,"")

	AttackStart()
	Hurt(0,"hurt_21")
	DirFx(0,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0,"","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(5,"ShakeDistance = 300","ShakeMode = 3","ShakeTimes = 2","ShakeFrequency = 0.5","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
	
---------------------------------------------------吟唱时间减1/4红眼版本-----------------------------------------------------------		
STake_Start("Scorpion_XZW01_skill_004_start_level1","Scorpion_XZW01_skill_004")  --技能  11160/11161  左挥抓  引导1.2  动作总长2.933
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0.4,3.333)
    Fx(1.6,"Scorpion_XZW01_attack_001_fire","Reference",1,1,0,0,0,0,0,0,1.5)

    Sound(1.6,"Scorpion_XZW01_Attack_001")
    Sound(1.6,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_skill_004_fire_level1","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

    AttackStart()
    Charge(0.1,0.36,120,0,4)
    Hurt(0.23,"hurt_21")
    DirFx(0.23,"Scorpion_XZW01_skill_001_2_hit",0,1)
	HurtBoneFx(0.23,"FX_blood_zhua_b","Spine1",1,0,0,0,90,0,0)
    HurtSound(0.23,"Scorpion_XZW01_Hit_000")


STake_Start("Scorpion_XZW01_skill_005_start_level1","Scorpion_XZW01_skill_005")  --技能  11162/11163  右抓抓  引导1.2  动作总长2.933
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0.4,3.333)
    Fx(1.65,"Scorpion_XZW01_attack_000_fire","Reference",1,1,0,0,0,0,0,0,1.5)
    Sound(1.65,"Scorpion_XZW01_Attack_001")
    Sound(1.65,"Scorpion_XZW01_AttackVOC_1_1")


STake_Start("Scorpion_XZW01_skill_005_fire_level1","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

    AttackStart()
    Charge(0.1,0.33,120,0,4)
    Hurt(0.23,"hurt_21")
    DirFx(0.23,"Scorpion_XZW01_skill_001_2_hit",0,1,1,-90,0,0)
	HurtBoneFx(0.23,"FX_blood_zhua_b","Spine1",1,0,0,0,-90,0,0)
    HurtSound(0.23,"Scorpion_XZW01_Hit_000")

	
STake_Start("Scorpion_XZW01_skill_001_2_start_level1","Scorpion_XZW01_skill_001_2")  --技能  11166  尾巴插地  引导1.2  动作总长3.433
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0.4,3.833)
    Fx(0.4,"Scorpion_XZW01_skill_001_2_loop")
    Fx(1.6,"Scorpion_XZW01_skill_001_2_fire")
    Sound(0.6,"Scorpion_XZW01_Skill_001")
    Sound(0.6,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_skill_001_2_fire_level1","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

    AttackStart()
    Hurt(0.01,"hurt_81")
    DirFx(0.01,"Scorpion_XZW01_skill_001_2_hit",0,1)
    HurtBoneFx(0.01,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.01,"Scorpion_XZW01_Hit_001")

  
STake_Start("Scorpion_XZW01_skill_002_start_level1","Scorpion_XZW01_skill_002")  --技能  26031  尾巴扎前方  引导1.2  动作总长2.266
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0.4,2.666)
    Fx(0.4,"Scorpion_XZW01_skill_002_loop_a","",1) 
    Fx(1.2,"Scorpion_XZW01_skill_002_fire_a","",1)  
    Sound(1.4,"Scorpion_XZW01_Skill_002")
    Sound(1.4,"Scorpion_XZW01_AttackVOC_1_2")

STake_Start("Scorpion_XZW01_skill_002_fire_level1","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   

    AttackStart()
    Hurt(0.01,"hurt_21")
    DirFx(0.01,"Scorpion_XZW01_skill_002_hit_a",0,1,1,0,0,0)
	HurtBoneFx(0.01,"FX_blood_zhua_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.01,"Scorpion_XZW01_Hit_001")
	

STake_Start("Scorpion_XZW01_attack_long_start_level1","Scorpion_XZW01_skill_000")  --技能  26025  尾巴扫  引导1.2  动作总长1.6
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   
	FrameTime(0,2)
    PlaySpeed(0,0.6,0.575)
	Pause(0.820,0.16,1,0.012)
    Fx(0.7,"Scorpion_XZW01_attack_000_fire_a","",1.5)
    Fx(0.7,"Fx_common_skill_ground_stone_Rbanyuan","Reference",1,1,0,0,0,-160,0)
    Sound(0.050,"Scorpion_XZW01_Skill_001")
    Sound(0.116,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_attack_long_fire_level1","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")  
	Pause(0.820,0.16,1,0.012)	

  	AttackStart()
  	Hurt(0.06,"hurt_21")
    DirFx(0.06,"Scorpion_XZW01_skill_002_hit_a",0,1,1,-90,0,0)
	HurtBoneFx(0.06,"FX_blood_zhua_b","Spine1",1,0,0,0,-90,0,0)
    HurtSound(0.06,"Scorpion_XZW01_Hit_001")


STake_Start("Scorpion_XZW01_skill_008_level1","Scorpion_XZW01_skill_008")  --技能  26048  沙王穿刺  引导1.2charge0.22  动作总长1.899
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0.1,2.299)
    Fx(0,"")
	
	AttackStart()
	Hurt(1.31,"hurt_81")
	DirFx(1.31,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(1.31,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(1.2,"ShakeDistance = 300","ShakeMode = 3","ShakeTimes = 0.22","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")


---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		
STake_Start("Scorpion_XZW01_skill_004_start_level2","Scorpion_XZW01_skill_004")  --技能  11160/11161  左挥抓  引导0.8  动作总长2.533
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0.4,3.333)
    Fx(1.6,"Scorpion_XZW01_attack_001_fire","Reference",1,1,0,0,0,0,0,0,1.5)

    Sound(1.6,"Scorpion_XZW01_Attack_001")
    Sound(1.6,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_skill_004_fire_level2","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

    AttackStart()
    Charge(0.1,0.36,120,0,4)
    Hurt(0.23,"hurt_21")
    DirFx(0.23,"Scorpion_XZW01_skill_001_2_hit",0,1)
	HurtBoneFx(0.23,"FX_blood_zhua_b","Spine1",1,0,0,0,90,0,0)
    HurtSound(0.23,"Scorpion_XZW01_Hit_000")

	
STake_Start("Scorpion_XZW01_skill_005_start_level2","Scorpion_XZW01_skill_005")  --技能  11162/11163  右抓抓  引导0.8  动作总长2.533
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0.8,3.333)
    Fx(1.65,"Scorpion_XZW01_attack_000_fire","Reference",1,1,0,0,0,0,0,0,1.5)
    Sound(1.65,"Scorpion_XZW01_Attack_001")
    Sound(1.65,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_skill_005_fire_level2","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

    AttackStart()
    Charge(0.1,0.33,120,0,4)
    Hurt(0.23,"hurt_21")
    DirFx(0.23,"Scorpion_XZW01_skill_001_2_hit",0,1,1,-90,0,0)
	HurtBoneFx(0.23,"FX_blood_zhua_b","Spine1",1,0,0,0,-90,0,0)
    HurtSound(0.23,"Scorpion_XZW01_Hit_000")
	

STake_Start("Scorpion_XZW01_skill_001_2_start_level2","Scorpion_XZW01_skill_001_2")  --技能  11166  尾巴插地  引导0.8  动作总长3.033
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0.8,3.833)
    Fx(0.8,"Scorpion_XZW01_skill_001_2_loop")
    Fx(1.6,"Scorpion_XZW01_skill_001_2_fire")
    Sound(0.8,"Scorpion_XZW01_Skill_001")
    Sound(0.8,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_skill_001_2_fire_level2","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")

   AttackStart()
   Hurt(0.01,"hurt_81")
   DirFx(0.01,"Scorpion_XZW01_skill_001_2_hit",0,1)
   HurtBoneFx(0.01,"FX_blood_gongjian_c","Spine1",1,0,0,0,0,0,0)
   HurtSound(0.01,"Scorpion_XZW01_Hit_001")


STake_Start("Scorpion_XZW01_skill_002_start_level2","Scorpion_XZW01_skill_002")  --技能  26031  尾巴扎前方  引导0.8  动作总长2.266
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0.8,2.666)
    Fx(0.8,"Scorpion_XZW01_skill_002_loop_a","",1) 
    Fx(1.2,"Scorpion_XZW01_skill_002_fire_a","",1)  
    Sound(1.4,"Scorpion_XZW01_Skill_002")
    Sound(1.4,"Scorpion_XZW01_AttackVOC_1_2")

STake_Start("Scorpion_XZW01_skill_002_fire_level2","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   

    AttackStart()
    Hurt(0.01,"hurt_21")
    DirFx(0.01,"Scorpion_XZW01_skill_002_hit_a",0,1,1,0,0,0)
	HurtBoneFx(0.01,"FX_blood_zhua_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.01,"Scorpion_XZW01_Hit_001")


STake_Start("Scorpion_XZW01_attack_long_start_level2","Scorpion_XZW01_skill_000")  --技能  26025  尾巴扫  引导0.8  动作总长1.2
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   
	FrameTime(0,2)
    PlaySpeed(0,0.6,0.934)
	Pause(0.820,0.16,1,0.012)
    Fx(0.7,"Scorpion_XZW01_attack_000_fire_a","",1.5)
    Fx(0.7,"Fx_common_skill_ground_stone_Rbanyuan","Reference",1,1,0,0,0,-160,0)
    Sound(0,"Scorpion_XZW01_Skill_001")
    Sound(0,"Scorpion_XZW01_AttackVOC_1_1")

STake_Start("Scorpion_XZW01_attack_long_fire_level2","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")  
	Pause(0.820,0.16,1,0.012)	

  	AttackStart()
  	Hurt(0.06,"hurt_21")
    DirFx(0.06,"Scorpion_XZW01_skill_002_hit_a",0,1,1,-90,0,0)
	HurtBoneFx(0.06,"FX_blood_zhua_b","Spine1",1,0,0,0,-90,0,0)
    HurtSound(0.06,"Scorpion_XZW01_Hit_001")


STake_Start("Scorpion_XZW01_skill_008_level2","Scorpion_XZW01_skill_008")  --技能  26048  沙王穿刺  引导0.8charge0.22  动作总长1.499
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0.5,2.299)
    Fx(0,"")
	
	AttackStart()
	Hurt(0.91,"hurt_81")
	DirFx(0.91,"T1_DoubleBlade_skill_hit",0,1)
    HurtBoneFx(0.91,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    RangeCameraShake(0.8,"ShakeDistance = 300","ShakeMode = 3","ShakeTimes = 0.22","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")


STake_Start("Scorpion_XZW01_attack_long_level2_start","Scorpion_XZW01_skill_000")  --蝎子普攻吟唱  26025  吟唱0.8  动作总长2
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,2)
	PlaySpeed(0,0.6,0.937)
	Pause(0.820,0.16,1,0.012)
    Fx(0.6,"Scorpion_XZW01_attack_000_fire_a","",1.5,1,0,0,0,0,0,0,1)
    Fx(0.6,"Fx_common_skill_ground_stone_Rbanyuan","Reference",1,1,0,0,0,-160,0,0,1)
    Sound(0.050,"Scorpion_XZW01_Skill_001")
    Sound(0.116,"Scorpion_XZW01_AttackVOC_1_1")


STake_Start("Scorpion_XZW01_attack_long_level2_fire","")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	Pause(0.820,0.16,1,0.012)

  	AttackStart()
  	Hurt(0.01,"hurt_21")
    DirFx(0.01,"Scorpion_XZW01_skill_002_hit_a",0,1,1,-90,0,0)
	HurtBoneFx(0.01,"FX_blood_zhua_b","Spine1",1,0,0,0,-90,0,0)
    HurtSound(0.01,"Scorpion_XZW01_Hit_001")

	
---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	
	

STake_Start("Scorpion_XZW01_attack_long_level3_start","Scorpion_XZW01_skill_000")  --蝎子普攻吟唱  26025  吟唱0.533  动作总长1.5
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	FrameTime(0,2)
	Pause(0.820,0.16,1,0.012)
    PlaySpeed(0,0.6,1.608)
    Fx(0.7,"Scorpion_XZW01_attack_000_fire_a","",1.5,1,0,0,0,0,0,0,2)
    Fx(0.7,"Fx_common_skill_ground_stone_Rbanyuan","Reference",1,1,0,0,0,-160,0,0,2)

    Sound(0.050,"Scorpion_XZW01_Skill_001")
    Sound(0.116,"Scorpion_XZW01_AttackVOC_1_1")


STake_Start("Scorpion_XZW01_attack_long_level3_fire","")  --蝎子普攻fire
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
	Pause(0.820,0.16,1,0.012)

  	AttackStart()
  	Hurt(0.01,"hurt_21")
    DirFx(0.01,"Scorpion_XZW01_skill_002_hit_a",0,1,1,-90,0,0)
	HurtBoneFx(0.01,"FX_blood_zhua_b","Spine1",1,0,0,0,-90,0,0)
    HurtSound(0.01,"Scorpion_XZW01_Hit_001")

----------------------------=============================Raid沈青刚召唤怪技能================================----------------------------------
----------------------------=============================Raid沈青刚召唤怪技能================================----------------------------------
----------------------------=============================Raid沈青刚召唤怪技能================================----------------------------------

STake_Start("RaidXXSW_skill_001","Scorpion_XZW01_skill_004")  --技能  37339  Raid蝎子左挥抓  引导0.8  动作总长3.333
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")
    FrameTime(0,3.333)
	PlaySpeed(0,1.813,2.266)
	PlaySpeed(1.813,3.333,1.623)
    Fx(1.666,"Scorpion_XZW01_attack_001_fire","Reference",1,1,0,0,0,0,0,0,2.266)
    Sound(1.666,"Scorpion_XZW01_Attack_001")
    Sound(1.666,"Scorpion_XZW01_AttackVOC_1_1")

    AttackStart()
    Hurt(0.8,"hurt_21")
    DirFx(0.8,"Scorpion_XZW01_skill_001_2_hit",0,1)
	HurtBoneFx(0.8,"FX_blood_zhua_b","Spine1",1,0,0,0,90,0,0)
    HurtSound(0.8,"Scorpion_XZW01_Hit_000")


---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
STake_Start("Common_skill_jumpB_80","Scorpion_XZW01_skill_jumpB")  --技能 40009 近战通用后跳  引导0.01charge0.4
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,0.899)
	Fx(0.4,"FX_common_skill_jumpB_02","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.03,"L1_SBlade_dodge_000")
	Sound(0.03,"MQX_Skill_000_4")

	
STake_Start("Common_skill_jumpF","Scorpion_XZW01_skill_jumpF")  --技能 40001 通用追击  引导一0.01charge0.4  引导二0.46  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,1.2)
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
    Fx(0.41,"FX_common_skill_jumpF","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.03,"MQX_Skill_000_1")
	Sound(0.4,"SG_THD_SW_skill_010_Impact")
	
  	AttackStart()
  	Hurt(0.46,"hurt_21")    
    DirFx(0.46,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.46,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.46,"HMW01_Hit_001")
    RangeCameraShake(0.46,"ShakeDistance = 80","ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
	

	
	
	-- animation
Skeleton("PC_GB1")
Animation("HG")
Animation("HG_PA")
Animation("HG_life")
Animation("GB_ST")
Animation("GB_PA")
Animation("HIT")
Animation("HG_Arrow")
Animation("RideHorse_HG")
Animation("RideCamel_HG")
Animation("HG_FishingRod")
Animation("N1_life01")
Animation("N1_life02")
Animation("N1_life03")
Animation("THD_FL")
Animation("THD_SW")
Animation("SL_ST")
Animation("SL_SB")
Animation("HG_SL_ST")
Animation("QZ_SW")
Animation("QZ_PA")
Animation("RH_HG")
Animation("QZ_SW_NEW")
Animation("HG_CS02")
Animation("HG_CS_03")
Animation("HG_CS04")
Animation("HG_CS06")
STake("total")

-- paperdoll
PaperDollsTable("Paperdoll")
--Equip("PC_GB1_03A1_leg","",1)
--Equip("PC_GB1_03A1_head","",2)
--Equip("PC_GB1_03A1_body","PCM_008",3)

Dimension(19, 43)
BoundingBox(40,40,43,-40,-40,0,0,0,0)


-- blend pattern
BlendPattern("PC_male")

--FoldingBone("RightFoot",3.0,0,0)
--FoldingBone("LeftFoot",3.0,0,0)package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("N1_life")


STake_Start("Monster_B_emo_001","SZ02_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("SZ02_dead","SZ02_dead")
    BlendMode(0)
    BlendTime(0.2)  
    Sound(0.1,"voc_YP_dead")		
		
STake_Start("SZ02_dead_000","SZ02_dead_000")
    BlendMode(0)
    BlendTime(0.2)  
    Sound(0.1,"voc_YP_dead")		

    Charge(0.000,0.264,30,0,5)
		
STake_Start("SZ02_dead_001","SZ02_dead_001")
    BlendMode(0)
    BlendTime(0.2)  
    Sound(0.1,"voc_YP_dead")		

    Charge(0.000,0.264,30,0,5)
		
STake_Start("SZ02_dead_002","SZ02_dead_002")
    BlendMode(0)
    BlendTime(0.2)  
    Sound(0.1,"voc_YP_dead")		

    Charge(0.000,0.264,30,0,5)
		
STake_Start("SZ02_dead_003","SZ02_dead_003")
    BlendMode(0)
    BlendTime(0.2)  
    Sound(0.1,"voc_YP_dead")		

    Charge(0.000,0.264,30,0,5)
		
STake_Start("SZ02_hit_000","SZ02_hit_000")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_light")

STake_Start("SZ02_hit_001","SZ02_hit_001")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_light")

STake_Start("SZ02_hit_002","SZ02_hit_002")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_light")

STake_Start("SZ02_hit_003","SZ02_hit_003")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_light")

STake_Start("SZ02_hit_006","SZ02_hit_006")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_mid")

STake_Start("SZ02_hit_007","SZ02_hit_007")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_mid")

STake_Start("SZ02_hit_008","SZ02_hit_008")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_mid")

STake_Start("SZ02_hit_009","SZ02_hit_009")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_mid")
		
STake_Start("SZ02_hit_011","SZ02_hit_011")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_mid")

    Charge(0.000,0.264,30,0,5)
		
STake_Start("SZ02_hit_012","SZ02_hit_012")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_mid")

    Charge(0.000,0.264,30,0,5)

STake_Start("SZ02_hit_013","SZ02_hit_013")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_mid")

    Charge(0.000,0.264,30,0,5)

STake_Start("SZ02_hit_014","SZ02_hit_014")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_mid")

    Charge(0.000,0.264,30,0,5)

STake_Start("SZ02_hit_015","YP_hit_016")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_heavy")

    Charge(0.000,0.542,100,0,5)

STake_Start("SZ02_hit_016","YP_hit_017")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_heavy")

    Charge(0.000,0.542,100,0,5)
		
STake_Start("SZ02_hit_017","YP_hit_018")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_heavy")

    Charge(0.000,0.542,100,0,5)
		
STake_Start("SZ02_hit_018","YP_hit_019")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_heavy")

    Charge(0.000,0.542,100,0,5)
		
STake_Start("SZ02_hit_021","YP_hit_021")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_heavy")

    Charge(0.000,0.350,30,0,5)

STake_Start("SZ02_hit_022","YP_hit_022")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_heavy")

    Charge(0.000,0.350,30,0,5)

STake_Start("SZ02_hit_023","YP_hit_023")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_heavy")

    Charge(0.000,0.350,30,0,5)

STake_Start("SZ02_hit_024","YP_hit_024")
    BlendMode(0)
    BlendTime(0.02) 
		Sound(0.02,"voc_YP_hurt_heavy")

    Charge(0.000,0.350,30,0,5)
		

STake_Start("SZ02_B_move_003","SZ02_B_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 


STake_Start("SZ02_B_move_004","SZ02_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("SZ02_B_move_005","SZ02_B_move_005")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("SZ02_B_move_006","SZ02_B_move_006")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("SZ02_B_move_007","SZ02_B_move_007")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("SZ02_wait_000","SZ02_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("SZ02_B_wait_000","SZ02_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()  

STake_Start("SZ02_move_000","SZ02_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("SZ02_move_001","SZ02_move_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("SZ02_emo_001","SZ02_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("SZ02_emo_002","SZ02_emo_002")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("SZ02_link_000","SZ02_link_000")
    BlendMode(0)
    BlendTime(0.2)  

STake_Start("SZ02_link_001","SZ02_link_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("SZ02_def_000","SZ02_def_000")
    BlendMode(0)
    BlendTime(0.1)

    Fx(0,"Fx_gedang_wuqi")     
		Sound(0.02,"weap_Blade_block")

STake_Start("SZ02_dodge_000","SZ02_dodge_000")
    BlendMode(0)
    BlendTime(0.1)

STake_Start("SZ02_bout_001","SZ02_bout_001")
    BlendMode(0)
    BlendTime(0.1)

    Fx(0,"SZ02_bout_001")   
package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")


STake_Start("T1_Bow_attack_001","T1_Bow_attack_001")  --向天射雕
    BlendMode(0)
    BlendTime(0.2)
    Fx(0,"NB_TL_emo_002")

    Sound(0.500,"T1_Bow_attack_000")
  

STake_Start("T1_Bow_B_move_005_L","T1_Bow_B_move_005")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Bow_B_move_006_R","T1_Bow_B_move_006")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Bow_B_wait_000","T1_Bow_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","T1_Bow_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Bow_dead","T1_Bow_dead")   --死亡
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.420,"T1_Bow_dead_002")
    Sound(1.360,"DaoDi_000")
    Sound(0.351,"ZhadalanGongbing_004")

STake_Start("T1_Bow_dead_000","T1_Bow_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.666,"T1_Bow_dead_002")
    Sound(0.400,"DaoDi_000")
    Sound(0.233,"ZhadalanGongbing_004")

STake_Start("T1_Bow_dead_001","T1_Bow_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.386,"DaoDi_000")
    Sound(0.813,"DaoDi_000")
    Sound(0.813,"T1_Bow_dead_002")
    Sound(0.213,"ZhadalanGongbing_004")

STake_Start("T1_Bow_dead_002","T1_Bow_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.476,"DaoDi_000")
    Sound(0.700,"T1_Bow_dead_002")
    Sound(0.135,"ZhadalanGongbing_004")

STake_Start("T1_Bow_dead_003","T1_Bow_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.453,"DaoDi_000")
    Sound(0.533,"T1_Bow_dead_002")
    Sound(0.144,"ZhadalanGongbing_004")
	
STake_Start("T1_Bow_dead_004","T1_Bow_dead_004")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0,"DaoDi_000")
    Sound(0,"T1_Bow_dead_002")
    Sound(0.1,"ZhadalanGongbing_004")
	
STake_Start("T1_Bow_dead_005","YP_Monster_hit_021_loop")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0,"DaoDi_000")
    Sound(0,"T1_Bow_dead_002")
    Sound(0,"ZhadalanGongbing_004")
	
STake_Start("T1_Bow_hit_021_start","T1_Bow_hit_021")  --击倒start
    BlendMode(0)
    BlendTime(0.2)
	Sound(0.01,"ZhadalanDaobing_004")
    Sound(0.03,"DaoDi_000")
	
STake_Start("T1_Bow_hit_021_loop","T1_Bow_hit_021_loop")   --击倒loop
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Bow_hit_021_end","T1_Bow_hit_021_down")   --击倒end
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Bow_hit_016_start","T1_Bow_hit_016")  --击飞start
    BlendMode(0)
    BlendTime(0.2)
	Sound(0.01,"ZhadalanDaobing_004")
	
STake_Start("T1_Bow_hit_016_loop","T1_Bow_hit_016_loop")   --击飞loop
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Bow_hit_016_end","T1_Bow_hit_016_down")   --击飞end
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.03,"DaoDi_000")
	
STake_Start("T1_Bow_hit_033","T1_Bow_hit_033")   --晕眩
    BlendMode(0)
    BlendTime(0.2)
	Loop()
	
STake_Start("T1_Bow_hit_034","T1_Bow_hit_034")   --插槽技插中loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()
	
STake_Start("T1_Bow_jump_start_B","T1_Bow_jump_start_B")   --战斗后跳start
    BlendMode(0)
    BlendTime(0.2)
	Sound(0.01,"char_05_cloth_flap_02")
	
STake_Start("T1_Bow_jump_loop_B","T1_Bow_jump_loop_B")   --战斗后跳loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()
	
STake_Start("T1_Bow_jump_down_B","T1_Bow_jump_down_B")   --战斗后跳end
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("T1_Bow_def_000","T1_Bow_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.253,"T1_Bow_dead")
    Sound(0.253,"L1_Unarmed_def_000")

STake_Start("T1_Bow_dodge_000","T1_Bow_dodge_000")
    BlendMode(0)
    BlendTime(0.2)
	Sound(0.03,"SZ03_dodge_000")

STake_Start("T1_Bow_emo_001","T1_Bow_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Bow_emo_002","T1_Bow_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Bow_emo_006","T1_Bow_emo_006")  --斜45度射箭
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.423,"T1_Bow_skill_000")

    Fx(0,"T1_Bow_emo_006")

STake_Start("T1_Bow_link_000","T1_Bow_link_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Bow_link_001","T1_Bow_link_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Bow_move_000","T1_Bow_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Bow_move_003","T1_Bow_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Bow_B_move_004","T1_Bow_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Bow_B_move_003_L","T1_Bow_B_move_003_L")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Bow_B_move_003_R","T1_Bow_B_move_003_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Bow_B_move_005","T1_Bow_B_move_005")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Bow_B_move_006","T1_Bow_B_move_006")
    BlendMode(0)
    BlendTime(0.2)
    Loop()	

STake_Start("T1_Bow_wait_000","T1_Bow_wait_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Bow_bout_000","T1_Bow_bout_000")    --出生POSE
    BlendMode(0)
    BlendTime(0.2)
	
---------------------------------------塞外特殊待机↓↓↓↓↓↓↓↓------------------------------------------------------
	
STake_Start("T1_Bow_wait_001","T1_Bow_bout_000")    --蹲的待机
    BlendMode(0)
    BlendTime(0.017)
	FrameTime(0.630,0.687)
	PlaySpeed(0.630,0.687,0.085)
	Direction(2)
	Loop()
	
STake_Start("T1_Bow_emo_003","T1_Bow_bout_000")    --蹲的待机小动作
    BlendMode(0)
    BlendTime(0.02)
	FrameTime(0.573,1.518)

---------------------------------------塞外特殊待机↑↑↑↑↑↑↑↑------------------------------------------------------

STake_Start("N1_MGSB_01_wait_000_doodad","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵  doodad
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
    

STake_Start("N1_wait_008_doodad","N1_wait_008")  --受伤躺地待机 doodad
    BlendMode(0)
    BlendTime(0.2) 
    Loop()    
	
	
	
	
STake_Start("T1_Bow_skill_000_start","T1_Bow_skill_000")    --技能 25618   吟唱1.539    动作总长2.333
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
	FrameTime(0,2.333)
    Sound(0.723,"T1_Bow_skill_000")
  	Fx(0.000,"Bow_qiyan_skill_000_fire")
  	--Fx(0.000,"Fx_Aim_Line","",1,0,0,-20,34,0,0,0,0.64)
    LineFx(0.1,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,34,0,0,0,0.64,1)
	LinkFx(1.539,"FX_Archery_Tracker",0,"Reference","Reference",1,800,0,0,33)
	
STake_Start("T1_Bow_skill_000_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     

    AttackStart()
  	Hurt(0,"hurt_11")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	
STake_Start("T1_Bow_skill_001_start","T1_Bow_skill_001")   --技能 25620 减速射击   吟唱3.041  动作总长4.166
    BlendMode(0)
    BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,4.166)
	Fx(1,"FX_M_att_charge_YC")	
	Fx(2,"FX_M_att_charge_YC")	
	--Fx(0,"Fx_Aim_Line","",1,0,0,-20,24,0,0,0,0.32)	
	Sound(0.000,"char_05_cloth_flap_02")
    Sound(0.54,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

    LineFx(0.1,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,24,0,0,0,0.32,1)
    LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)
	
STake_Start("T1_Bow_skill_001_fire","")
    BlendMode(0)
    BlendTime(0.2)
	
	AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	

STake_Start("T1_Bow_skill_002_start","T1_Bow_skill_001")   --技能 25622 流血射击   吟唱3.041  动作总长4.166
    BlendMode(0)
    BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,4.166)
	Fx(1,"FX_M_att_charge_YC")	
	Fx(2,"FX_M_att_charge_YC")	
	Sound(0.000,"char_05_cloth_flap_01")
    Sound(0.54,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

    LineFx(0.1,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,24,0,0,0,0.32,1)
    LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)
	
STake_Start("T1_Bow_skill_002_fire","")
    BlendMode(0)
    BlendTime(0.2)
	
	AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	

---------------------------------------------------吟唱时间减1/4红眼版本-----------------------------------------------------------		
	
STake_Start("T1_Bow_skill_000_level1_start","T1_Bow_skill_000")    --技能 25618   吟唱1.155    动作总长1.949
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    FrameTime(0.384,2.333)	
    Sound(0.723,"T1_Bow_skill_000")
  	Fx(0.384,"Bow_qiyan_skill_000_fire","",1,0,0,0,0,0,0,0,1.35)
  	--Fx(0.384,"Fx_Aim_Line","",1,0,0,0,34,0,0,0,0.9)
    LineFx(0.384,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,34,0,0,0,0.86,1)
	LinkFx(1.539,"FX_Archery_Tracker",0,"Reference","Reference",1,800,0,0,33)
	
STake_Start("T1_Bow_skill_000_level1_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     

    AttackStart()
  	Hurt(0,"hurt_11")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	
STake_Start("T1_Bow_skill_001_level1_start","T1_Bow_skill_001")   --技能 25620 减速射击   吟唱2.281  动作总长3.406
    BlendMode(0)
    BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0.76,4.166)
	Fx(0.76,"FX_M_att_charge_YC")	
	Fx(2,"FX_M_att_charge_YC")	
	--Fx(0.76,"Fx_Aim_Line","",1,0,0,0,24,0,0,0,0.4)
    Sound(0.77,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

    LineFx(0.76,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,24,0,0,0,0.43,1)
    LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)
	
STake_Start("T1_Bow_skill_001_level1_fire","")
    BlendMode(0)
    BlendTime(0.2)
	
	AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	

STake_Start("T1_Bow_skill_002_level1_start","T1_Bow_skill_001")   --技能 25622 流血射击   吟唱2.281  动作总长3.406
    BlendMode(0)
    BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0.76,4.166)
	Fx(0.76,"FX_M_att_charge_YC")	
	Fx(2,"FX_M_att_charge_YC")	
    Sound(0.77,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

    LineFx(0.76,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,24,0,0,0,0.43,1)
    LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)
	
STake_Start("T1_Bow_skill_002_level1_fire","")
    BlendMode(0)
    BlendTime(0.2)
	
	AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	
---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		

STake_Start("T1_Bow_skill_000_level2_start","T1_Bow_skill_000")    --技能 25618   吟唱0.769    动作总长1.564
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.769,2.333)
    Sound(0.77,"T1_Bow_skill_000")
  	Fx(0.769,"Bow_qiyan_skill_000_fire","",1,0,0,0,0,0,0,0,2.35)
  	--Fx(0.769,"Fx_Aim_Line","",1,0,0,0,34,0,0,0,1.2)
    LineFx(0.769,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,34,0,0,0,1.3,1)
	LinkFx(1.539,"FX_Archery_Tracker",0,"Reference","Reference",1,800,0,0,33)
	
STake_Start("T1_Bow_skill_000_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     

    AttackStart()
  	Hurt(0,"hurt_11")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	
STake_Start("T1_Bow_skill_001_level2_start","T1_Bow_skill_001")   --技能 25620 减速射击   吟唱1.52  动作总长2.646
    BlendMode(0)
    BlendTime(0.2)
	Priority(0,"5")
	FrameTime(1.52,4.166)
	Fx(1.52,"FX_M_att_charge_YC")	
	Fx(2,"FX_M_att_charge_YC")	
	--Fx(1.52,"Fx_Aim_Line","",1,0,0,0,24,0,0,0,0.75)
    Sound(1.53,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

    LineFx(1.52,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,24,0,0,0,0.65,1)
    LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)
	
STake_Start("T1_Bow_skill_001_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
	
	AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	

STake_Start("T1_Bow_skill_002_level2_start","T1_Bow_skill_001")   --技能 25622 流血射击   吟唱1.52  动作总长2.646
    BlendMode(0)
    BlendTime(0.2)
	Priority(0,"5")
	FrameTime(1.52,4.166)
	Fx(1.52,"FX_M_att_charge_YC")	
	Fx(2,"FX_M_att_charge_YC")	
    Sound(1.53,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

    LineFx(1.52,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,24,0,0,0,0.65,1)
    LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)
	
STake_Start("T1_Bow_skill_002_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
	
	AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)		
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	
	
---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	
	
STake_Start("T1_Bow_skill_000_level3_start","T1_Bow_skill_000")    --技能 25618   吟唱0.513    动作总长1.307
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.026,2.333)
    Sound(1.026,"T1_Bow_skill_000")
  	Fx(1.026,"Bow_qiyan_skill_000_fire","",1,0,0,0,0,0,0,0,3)
    LineFx(1.026,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,34,0,0,0,1.94,1)
	LinkFx(1.539,"FX_Archery_Tracker",0,"Reference","Reference",1,800,0,0,33)
	
STake_Start("T1_Bow_skill_000_level3_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     

    AttackStart()
  	Hurt(0,"hurt_11")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	
STake_Start("T1_Bow_skill_001_level3_start","T1_Bow_skill_001")   --技能 25620 减速射击   吟唱1.013  动作总长2.14
    BlendMode(0)
    BlendTime(0.2)
	Priority(0,"5")
	FrameTime(2.026,4.166)
	Fx(2.026,"FX_M_att_charge_YC")	
    Sound(2.03,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

    LineFx(2.026,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,24,0,0,0,0.98,1)
    LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)
	
STake_Start("T1_Bow_skill_001_level3_fire","")
    BlendMode(0)
    BlendTime(0.2)
	
	AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)	
    HurtSound(0,"T1_Bow_hit")
	

STake_Start("T1_Bow_skill_002_level3_start","T1_Bow_skill_001")   --技能 25622 流血射击   吟唱1.013  动作总长2.14
    BlendMode(0)
    BlendTime(0.2)
	Priority(0,"5")
	FrameTime(2.026,4.166)	
	Fx(2.026,"FX_M_att_charge_YC")	
    Sound(2.034,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

    LineFx(2.026,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,24,0,0,0,0.98,1)
    LineFx(3.04,"FX_Archery_Tracker",0,"Reference", "Reference",1,800,0,-25,30)
	
STake_Start("T1_Bow_skill_002_level3_fire","")
    BlendMode(0)
    BlendTime(0.2)
	
	AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "Bow_qiyan_skill_hit", "Spine1",1)		
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")
	
	
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------


STake_Start("N1_Bow_skill_002_start","N1_Bow_skill_002")  --跳跃反击  27008
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,2.666)
    Fx(0,"FX_M_att_charge_YC","",1,0,0,0,0,0,0,0,1,1)
    Sound(0.27,"PC_LaGong_long")
	Sound(1.14,"PC_Gong_Shot")

    LineFx(0.01,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,0,0,0,0,0,1.33,1)	
    Fx(1.14,"FX_Archery_Tracker","Reference",1,0,0,-25,22,0,-20)	
    Charge(0.83,0.31,120,180,4)
	
STake_Start("N1_Bow_skill_002_fire","")  --跳跃反击  27008
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	
    AttackStart()
	Hurt(0,"hurt_21")
    HurtSound(0,"Big_blade_Hit_002_1")
    AttackHurtFx(0, "N1_Dart_skill_001_hit", "Spine1",1)	
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,-22,0)
	
	
STake_Start("N_DMB_GB_skill_003","N_DMB_GB_skill_003")    --技能  35038  凿击  引导1.69  动作总长3.3
    BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	FrameTime(0,3.3)
	Pause(1.75,0.16,1,0.012)
    Fx(1.604,"N_DMB_GB_skill_003_fire","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.56,"L1_SBlade_attack_002")
	
	AttackStart()
	Hurt(1.75,"hurt_21")
	HurtSound(1.75,"Big_blade_Hit_002_1")
	DirFx(1.75,"N_DMB_GB_skill_003_hit",0,1)
    HurtBoneFx(1.75,"FX_blood_bishou_c","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("Monster_common_charge_skill_B_start","T1_Bow_jump_start_B")  --技能  40000  引导0.001后charge0.666秒    动作总长1.332
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")

STake_Start("Monster_common_charge_skill_B_fire","T1_Bow_jump_down_B")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")

	


---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------

STake_Start("Common_skill_jumpB_120","T1_Bow_skill_jumpB")  --技能 40000 远程通用后跳  引导0.01charge0.4  动作总厂0.899
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,0.899)
	Fx(0.4,"FX_common_skill_jumpB_02","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.08,"char_05_cloth_flap_02")	
	Sound(0.38,"SZ03_dodge_000")	


STake_Start("Common_skill_jumpF","T1_Bow_skill_jumpF")  --技能 40001 通用追击  引导一0.01charge0.4  引导二0.46  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,1.2)
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
    Fx(0.41,"FX_common_skill_jumpF","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.28,"TY_Attack_011")
	
  	AttackStart()
  	Hurt(0.46,"hurt_21")    
    DirFx(0.46,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.46,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.46,"HMW01_Hit_002")
    RangeCameraShake(0.46,"ShakeDistance = 80","ShakeMode = 3","ShakeTimes = 0.14","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")



STake_Start("Common_skill_archer_attack01","T1_Bow_skill_attack")  --技能 40003 远程通用近战普攻01  引导1.6  动作总长2.5
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,2.5)
    Fx(0,"","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.28,"TY_Attack_004")

  	AttackStart()
  	Hurt(1.6,"hurt_21")    
    DirFx(1.6,"Bear_skill_000_hit",0,1)
    HurtBoneFx(1.6,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(1.6,"blade_Hit")


STake_Start("Common_skill_archer_attack01_level1","T1_Bow_skill_attack")  --技能 40003 远程通用近战普攻01  引导1.2   动作总长2.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0.4,2.5)
    Fx(0,"","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.6,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.28,"TY_Attack_004")
	
  	AttackStart()
  	Hurt(1.2,"hurt_21")    
    DirFx(1.2,"Bear_skill_000_hit",0,1)
    HurtBoneFx(1.2,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(1.2,"blade_Hit")


STake_Start("Common_skill_archer_attack01_level2","T1_Bow_skill_attack")  --技能 40003 远程通用近战普攻01  引导0.8   动作总长1.7
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0.8,2.5)
    Fx(0,"","",1,0,0,0,0,0,0,0,1,1)
	Fx(1,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.28,"TY_Attack_004")
	
  	AttackStart()
  	Hurt(0.8,"hurt_21")    
    DirFx(0.8,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.8,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.8,"blade_Hit")



--蒙古草原CUTSCENE用动作

--CS3活捉八海

--俞章廷配

STake_Start("CS_T1_walk_qiyangongbing_000","T1_Bow_move_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_run_qiyangongbing","T1_Bow_B_move_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_walk2_qiyangongbing","T1_Bow_move_003")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

--单晓箫配

STake_Start("CS_wait1_T1_Bow","T1_Bow_B_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()




-----------------------------------------------------------------------------------
--塞外CUTSCENE用动作

--CS4大军冲

--单晓箫配

STake_Start("CS_attack1_T1_Bow","T1_Bow_attack_000")
    BlendMode(2)
    BlendTime(0.2)
    Fx(0,"FX_Cutscene_stand_fire1_fx_T1_BowRider_a")
    Loop()

STake_Start("CS_attack2_T1_Bow","T1_Bow_emo_006")
    BlendMode(2)
    BlendTime(0.2)
    Fx(0,"FX_Cutscene_stand_fire1_fx_T1_BowRider_a")
    Loop()

STake_Start("CS_wait2_T1_Bow","T1_Bow_emo_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait3_T1_Bow","T1_Bow_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_look_T1_Bow","T1_Bow_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_hurted1_T1_Bow","T1_Bow_emo_003")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_hurted2_T1_Bow","T1_Bow_emo_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_hurted3_T1_Bow","T1_Bow_emo_005")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_dodge_T1_Bow","T1_Bow_dodge_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_link_T1_Bow","T1_Bow_link_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_dead1_T1_Bow","T1_Bow_dead")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,1.9)  
    Loop(1.899,1.9)

STake_Start("CS_dead1_S_T1_Bow","T1_Bow_dead")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.899,1.9)  
    Loop()

STake_Start("CS_dead2_T1_Bow","T1_Bow_dead_000")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,1.3)  
    Loop(1.299,1.3)

STake_Start("CS_dead2_S_T1_Bow","T1_Bow_dead_000")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.299,1.3)  
    Loop()

STake_Start("CS_dead3_T1_Bow","T1_Bow_dead_002")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,1.3)  
    Loop(1.299,1.3)

STake_Start("CS_dead3_S_T1_Bow","T1_Bow_dead_002")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.299,1.3)  
    Loop()

STake_Start("CS_dead4_T1_Bow","T1_Bow_dead_003")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,1.3)  
    Loop(1.299,1.3)

STake_Start("CS_dead4_S_T1_Bow","T1_Bow_dead_003")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.299,1.3)  
    Loop()

STake_Start("CS_dead5_T1_Bow","YP_Monster_hit_016_down")
    BlendMode(2)
    BlendTime(0.2)
    Direction(1)
    Loop()

STake_Start("CS_dead5_S_T1_Bow","YP_Monster_hit_016_down")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0.67,0.67001)  
    Loop()

STake_Start("CS_Dao1_T1_Bow","YP_hit_021")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.768)   
    Loop(0.76799,0.768)

STake_Start("CS_Dao2_T1_Bow","YP_hit_023")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.73304)   
    Loop(0.733039,0.73304)

STake_Start("CS_Dao3_T1_Bow","YP_hit_024")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.73304)   
    Loop(0.733039,0.73304)


STake_Start("CS_wait4_escape01_T1_Blade","N1_move_020")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait4_escape02_T1_Blade","N1_move_023")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_escape1_T1_Blade","N1_move_020")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_escape2_T1_Blade","N1_move_023")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_escape3_T1_Blade","N1_move_022")
    BlendMode(2)
    BlendTime(0.2)
    Loop()	package.path = package.path .. ";..\\char\\?.lua"
require("common")


STake_Start("T1_BowRider_B_move_004","T1_BowRider_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    
    GroundSound(0.167,"Sound_run")
    Sound(0.316,"Horse_VOC_run_000")
    Sound(0.214,"Pc_Horse_runnoise_000")
  
STake_Start("T1_BowRider_B_wait_000","T1_BowRider_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","T1_BowRider_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_BowRider_dead","T1_BowRider_dead")
    BlendMode(0)
    BlendTime(0.2)

    GroundSound(1.213,"Sound_daodi")
    Sound(1.213,"TongYong_DaoDi_001")
    Sound(0.279,"Horse_VOC_000")
	Sound(0.28,"ZhadalanGongbing_004")

STake_Start("T1_BowRider_def_000","T1_BowRider_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.213,"Horse_VOC_run_000")
    Sound(0.050,"L1_Unarmed_def_000")

STake_Start("T1_BowRider_dodge_000","T1_BowRider_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    GroundSound(0.106,"Sound_jump")
    GroundSound(0.493,"Sound_fall")
    Sound(0.213,"Horse_VOC_008")
    GroundSound(0.959,"Sound_step")
    GroundSound(1.146,"Sound_step")

STake_Start("T1_BowRider_emo_001","T1_BowRider_emo_001")
    BlendMode(0)
    BlendTime(0.2)

    GroundSound(0.773,"Sound_step")
    Sound(0.959,"Horse_VOC_010")

STake_Start("T1_BowRider_emo_002","T1_BowRider_emo_002")
    BlendMode(0)
    BlendTime(0.2)

    GroundSound(0.373,"Sound_step")
    GroundSound(0.606,"Sound_step")
    Sound(1.306,"Horse_VOC_010")

STake_Start("T1_BowRider_emo_003","T1_BowRider_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_BowRider_hit_000","T1_BowRider_hit_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_BowRider_hit_011","T1_BowRider_hit_011")
    BlendMode(0)
    BlendTime(0.2)
    
    Charge(0.000,0.294,15,0,5)

STake_Start("T1_BowRider_link_000","T1_BowRider_link_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_BowRider_link_001","T1_BowRider_link_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_BowRider_move_000","T1_BowRider_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

    GroundSound(0.293,"Sound_step")
    GroundSound(0.679,"Sound_step")
    GroundSound(0.906,"Sound_step")
    GroundSound(1.186,"Sound_step")

STake_Start("T1_BowRider_move_001","T1_BowRider_move_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    
    GroundSound(0.167,"Sound_run")
    Sound(0.316,"Horse_VOC_run_000")
    Sound(0.214,"Pc_Horse_runnoise_000")

STake_Start("T1_BowRider_move_003","T1_BowRider_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
 
    GroundSound(0.293,"Sound_step")
    GroundSound(0.679,"Sound_step")
    GroundSound(0.906,"Sound_step")
    GroundSound(1.186,"Sound_step")   
	
STake_Start("T1_BowRider_move_003_L","T1_BowRider_move_003_L")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
 
    GroundSound(0.293,"Sound_step")
    GroundSound(0.679,"Sound_step")
    GroundSound(0.906,"Sound_step")
    GroundSound(1.186,"Sound_step")   
	
STake_Start("T1_BowRider_move_003_R","T1_BowRider_move_003_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
 
    GroundSound(0.293,"Sound_step")
    GroundSound(0.679,"Sound_step")
    GroundSound(0.906,"Sound_step")
    GroundSound(1.186,"Sound_step")   
	
STake_Start("T1_BowRider_B_move_007","T1_BowRider_move_007")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
 
    GroundSound(0.293,"Sound_step")
    GroundSound(0.679,"Sound_step")
    GroundSound(0.906,"Sound_step")
    GroundSound(1.186,"Sound_step")   
	
STake_Start("T1_BowRider_B_move_008","T1_BowRider_move_008")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
 
    GroundSound(0.293,"Sound_step")
    GroundSound(0.679,"Sound_step")
    GroundSound(0.906,"Sound_step")
    GroundSound(1.186,"Sound_step")  

STake_Start("T1_BowRider_wait_000","T1_BowRider_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

	
	
STake_Start("T1_BowRider_skill_001_start","T1_BowRider_skill_001")  --技能25628吟唱射击start  吟唱2.964  动作长3.8
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3.8)
    --Fx(0.1,"Fx_Aim_Line","",1,0,0,0,54,0,0,0,0.3)
    --Fx(2.964,"FX_Archery_Tracker","",1,0,0,0,53,0,0,0,1)
  	Fx(0.000,"T1_BowRider_skill_001_start")
  	Fx(0.000,"T1_BowRider_skill_001_loop")
  	Fx(1.000,"T1_BowRider_skill_001_loop")
  	Fx(2.000,"T1_BowRider_skill_001_loop")
  	Fx(2.964,"T1_BowRider_skill_001_fire")
    LineFx(0.1,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,54,0,0,0,0.33,1)
    LineFx(2.964,"FX_Archery_Tracker",0,"Reference","Reference",1,800,0,-15,53,0,0,0,1,1) 
    Sound(0.54,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

STake_Start("T1_BowRider_skill_001_fire","")  --技能25628吟唱射击fire
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
  	
    AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "N1_Bow2_attack_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")


STake_Start("T1_BowRider_skill_002_start","T1_BowRider_skill_002")  --技能25630冲锋start  吟唱1.613  动作长3.433
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
	FrameTime(0,3.433)
  	Fx(0.000,"T1_BowRider_skill_002_fire")
	Fx(0.2,"FX_Warning","Head")
    Sound(0.513,"Horse_VOC_000")
	Sound(1.64,"HYHY_01_Skill01")

STake_Start("T1_BowRider_skill_002_fire","")  --技能25630冲锋fire
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Charge(0,0.3,300,0,4)
  	
    AttackStart()
  	Hurt(0.2,"hurt_61")
  	DirFx(0.2,"T1_ZDLBFZ_skill_002_hit",0,1)
    HurtBoneFx(0.2,"FX_blood_quan_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.2,"T1_Bow_hit")




	
---------------------------------------------------吟唱时间减1/4红眼版本-----------------------------------------------------------			
	
STake_Start("T1_BowRider_skill_001_level1_start","T1_BowRider_skill_001")  --技能25628吟唱射击start  吟唱2.223  动作长3.059
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.741,3.8)
  	Fx(0.741,"T1_BowRider_skill_001_start")
  	Fx(0.741,"T1_BowRider_skill_001_loop")
  	Fx(1.741,"T1_BowRider_skill_001_loop")
  	Fx(2.000,"T1_BowRider_skill_001_loop")
  	Fx(2.964,"T1_BowRider_skill_001_fire")
    LineFx(0.741,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,54,0,0,0,0.44,1)
    LineFx(2.964,"FX_Archery_Tracker",0,"Reference","Reference",1,800,0,-15,53,0,0,0,1,1) 
    Sound(0.743,"PC_LaGong_long")
	Sound(2.99,"PC_Gong_Shot")

STake_Start("T1_BowRider_skill_001_level1_fire","")  --技能25628吟唱射击fire
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
  	
    AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "N1_Bow2_attack_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")


STake_Start("T1_BowRider_skill_002_level1_start","T1_BowRider_skill_002")  --技能25630冲锋start  吟唱1.21  动作长3.03
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")    
    FrameTime(0.403,3.433)	
  	Fx(0.403,"T1_BowRider_skill_002_fire","",1,0,0,0,0,0,0,0,1.5)
  	Fx(0.403,"T1_BowRider_skill_002_fire")
	Fx(0.603,"FX_Warning","Head")
    Sound(0.513,"Horse_VOC_000")
	Sound(1.64,"HYHY_01_Skill01")

STake_Start("T1_BowRider_skill_002_level1_fire","")  --技能25630冲锋fire
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Charge(0,0.3,300,0,4)
  	
    AttackStart()
  	Hurt(0.2,"hurt_61")
  	DirFx(0.2,"T1_ZDLBFZ_skill_002_hit",0,1)
    HurtBoneFx(0.2,"FX_blood_quan_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.2,"T1_Bow_hit")

	
---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		

STake_Start("T1_BowRider_skill_001_level2_start","T1_BowRider_skill_001")  --技能25628吟唱射击start  吟唱1.482  动作长2.318
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.482,3.8)
  	Fx(1.482,"T1_BowRider_skill_001_start")
  	Fx(1.482,"T1_BowRider_skill_001_loop")
  	Fx(2.000,"T1_BowRider_skill_001_loop")
  	Fx(2.964,"T1_BowRider_skill_001_fire")
    LineFx(1.482,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,54,0,0,0,0.67,1)
    LineFx(2.964,"FX_Archery_Tracker",0,"Reference","Reference",1,800,0,-15,53,0,0,0,1,1) 
    Sound(1.976,"T1_Bow_skill_000")

STake_Start("T1_BowRider_skill_001_level2_fire","")  --技能25628吟唱射击fire
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
  	
    AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "N1_Bow2_attack_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")


STake_Start("T1_BowRider_skill_002_level2_start","T1_BowRider_skill_002")  --技能25630冲锋start  吟唱0.806  动作长2.627
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.806,3.433)
  	Fx(0.806,"T1_BowRider_skill_002_fire","",1,0,0,0,0,0,0,0,1.8)
  	Fx(0.806,"T1_BowRider_skill_002_fire","",1,0,0,0,0,0,0,0,1.3)
	Fx(1.006,"FX_Warning","Head")
    Sound(0.813,"Horse_VOC_000")
	Sound(1.64,"HYHY_01_Skill01")

STake_Start("T1_BowRider_skill_002_level2_fire","")  --技能25630冲锋fire
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Charge(0,0.3,300,0,4)
  	
    AttackStart()
  	Hurt(0.2,"hurt_61")
  	DirFx(0.2,"T1_ZDLBFZ_skill_002_hit",0,1)
    HurtBoneFx(0.2,"FX_blood_quan_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.2,"T1_Bow_hit")


---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	
	
STake_Start("T1_BowRider_skill_001_level3_start","T1_BowRider_skill_001")  --技能25628吟唱射击start  吟唱0.988  动作长1.824
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.976,3.8)
  	Fx(1.976,"T1_BowRider_skill_001_start")
  	Fx(1.976,"T1_BowRider_skill_001_loop")
  	Fx(2.964,"T1_BowRider_skill_001_fire")
    LineFx(1.976,"Fx_Aim_Line",0,"Reference","Reference",1,800,0,-15,54,0,0,0,1.01,1)
    LineFx(2.964,"FX_Archery_Tracker",0,"Reference","Reference",1,800,0,-15,53,0,0,0,1,1) 
    Sound(1.976,"T1_Bow_skill_000")

STake_Start("T1_BowRider_skill_001_level3_fire","")  --技能25628吟唱射击fire
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
  	
    AttackStart()
  	Hurt(0,"hurt_21")
    AttackHurtFx(0, "N1_Bow2_attack_hit", "Spine1",1)
    HurtBoneFx(0,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"T1_Bow_hit")


STake_Start("T1_BowRider_skill_002_level3_start","T1_BowRider_skill_002")  --技能25630冲锋start  吟唱0.537  动作长2.359
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.074,3.433)
  	Fx(1.074,"T1_BowRider_skill_002_fire","",1,0,0,0,0,0,0,0,2.5)
  	Fx(1.074,"T1_BowRider_skill_002_fire","",1,0,0,0,0,0,0,0,1.5)
	Fx(1.274,"FX_Warning","Head")
    Sound(1.082,"Horse_VOC_000")
	Sound(1.64,"HYHY_01_Skill01")

STake_Start("T1_BowRider_skill_002_level3_fire","")  --技能25630冲锋fire
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Charge(0,0.3,300,0,4)
  	
    AttackStart()
  	Hurt(0.2,"hurt_61")
  	DirFx(0.2,"T1_ZDLBFZ_skill_002_hit",0,1)
    HurtBoneFx(0.2,"FX_blood_quan_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.2,"T1_Bow_hit")









-----------------------------------------------------------------------------------
--CutScene用
-----------------------------------------------------------------------------------
--塞外CUTSCENE用动作

--CS4大军冲

--单晓箫配


STake_Start("CS_wait1_T1_BowRider","T1_BowRider_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait2_T1_BowRider","T1_BowRider_emo_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait3_T1_BowRider","T1_BowRider_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait4_T1_BowRider","T1_BowRider_emo_003")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_run_fire1_T1_BowRider","T1_BowRider_CS_skill_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_run_fire1a_fx_T1_BowRider","T1_BowRider_CS_skill_001")
    BlendMode(2)
    BlendTime(0.2)
    Fx(0,"FX_Cutscene_run_fire1_fx_T1_BowRider_d")
    Loop()

STake_Start("CS_run_fire1b_fx_T1_BowRider","T1_BowRider_CS_skill_001")
    BlendMode(2)
    BlendTime(0.2)
    Fx(0,"FX_Cutscene_run_fire1_fx_T1_BowRider_d")
    Loop()

STake_Start("CS_run_fire2_T1_BowRider","T1_BowRider_CS_skill_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_run_fire2a_fx_T1_BowRider","T1_BowRider_CS_skill_002")
    BlendMode(2)
    BlendTime(0.2)
    Fx(0,"FX_Cutscene_run_fire2_T1_BowRider_start_a")
    Loop()

STake_Start("CS_run_fire2b_fx_T1_BowRider","T1_BowRider_CS_skill_002")
    BlendMode(2)
    BlendTime(0.2)
    Fx(0,"FX_Cutscene_run_fire2_T1_BowRider_start_b")
    Loop()

STake_Start("CS_walk1_T1_BowRider","T1_BowRider_move_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_walk2_T1_BowRider","T1_BowRider_move_003")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_run_T1_BowRider","T1_BowRider_move_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait_fire1_T1_BowRider","T1_BowRider_skill_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait_fire2_T1_BowRider","T1_BowRider_skill_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_dodge_T1_BowRider","T1_BowRider_dodge_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_dead_T1_BowRider","T1_BowRider_dead")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,2.3)  
    Loop(2.299,2.3)

STake_Start("CS_dead_S_T1_BowRider","T1_BowRider_dead")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(2.299,2.3)  
    Loop()





-----------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_run_T1_BowRider_CX","T1_BowRider_B_move_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()


STake_Start("CS_T1_BowRider_attack_000","T1_BowRider_attack_000")
    BlendMode(2)
    BlendTime(0.2)


STake_Start("CS_T1_BowRider_B_move_004","T1_BowRider_B_move_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()
  
STake_Start("CS_T1_BowRider_B_wait_000","T1_BowRider_B_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_BowRider_dead","T1_BowRider_dead")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_def_000","T1_BowRider_def_000")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_dodge_000","T1_BowRider_dodge_000")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_emo_001","T1_BowRider_emo_001")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_emo_002","T1_BowRider_emo_002")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_emo_003","T1_BowRider_emo_003")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_hit_000","T1_BowRider_hit_000")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_hit_011","T1_BowRider_hit_011")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_link_000","T1_BowRider_link_000")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_link_001","T1_BowRider_link_001")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_move_000","T1_BowRider_move_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_BowRider_move_001","T1_BowRider_move_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_BowRider_move_003","T1_BowRider_move_003")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_BowRider_skill_000","T1_BowRider_skill_000")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_BowRider_wait_000","T1_BowRider_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()




package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("T1_life")
require("N1_life")

STake_Start("T1_Blade_wait_000","T1_Blade_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Blade_B_wait_000","T1_Blade_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","T1_Blade_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)

	
STake_Start("T1_Blade_dead","T1_Blade_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.02,"T1_Blade_dead")
    Sound(0.320,"HM_MaleVoc_02_Dead")

STake_Start("T1_Blade_dead_pose","T1_Blade_dead_004")  --死亡pose
    BlendMode(0)
    BlendTime(0.2)
	FrameTime(2.865,2.866)
	Loop()

STake_Start("T1_Blade_dead_000","T1_Blade_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.518,"T1_Blade_dead_000")
    Sound(0.266,"HM_MaleVoc_02_Dead")

STake_Start("T1_Blade_dead_001","T1_Blade_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.350,"T1_Blade_dead_001")
    Sound(0.182,"HM_MaleVoc_02_Dead")

STake_Start("T1_Blade_dead_002","T1_Blade_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.434,"T1_Blade_dead")
    Sound(0.168,"HM_MaleVoc_02_Dead")

STake_Start("T1_Blade_dead_003","T1_Blade_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.439,"T1_Blade_dead")
    Sound(0.182,"HM_MaleVoc_02_Dead")
	
STake_Start("T1_Blade_dead_004","T1_Blade_dead_004")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.143,"T1_Blade_dead_000")
    Sound(0.001,"HM_MaleVoc_02_Dead")
	
STake_Start("T1_Blade_dead_005","T1_Blade_hit_021_loop")  --击倒中死亡
    BlendMode(0)
    BlendTime(0.2)
	
	Sound(0.001,"T1_Blade_dead_000")
    Sound(0.001,"HM_MaleVoc_02_Dead")

STake_Start("T1_Blade_def_000","T1_Blade_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.046,"L1_SBlade_def_000")

STake_Start("T1_Blade_dodge_000","T1_Blade_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.104,"N1_Axe_dodge_000")

STake_Start("T1_Blade_emo_001","T1_Blade_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Blade_emo_002","T1_Blade_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Blade_link_000","T1_Blade_link_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Blade_link_001","T1_Blade_link_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Blade_move_000","T1_Blade_move_000")  --移动
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Blade_move_003","T1_Blade_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Blade_B_move_003_L","T1_Blade_B_move_003_L")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Blade_B_move_003_R","T1_Blade_B_move_003_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Blade_move_004","T1_Blade_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Blade_B_move_005","T1_Blade_B_move_005")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Blade_B_move_006","T1_Blade_B_move_006")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Blade_jump_start_B","T1_Blade_jump_start_B")   --后跳跃
    BlendMode(0)
    BlendTime(0.2)
		Sound(0.03,"char_05_cloth_flap_02")
	
STake_Start("T1_Blade_jump_loop_B","T1_Blade_jump_loop_B")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Blade_jump_down_B","T1_Blade_jump_down_B")
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("T1_Blade_jump_start_L","T1_Blade_jump_start_L")   --左跳跃
    BlendMode(0)
    BlendTime(0.2)
		Sound(0.03,"char_05_cloth_flap_02")
	
STake_Start("T1_Blade_jump_loop_L","T1_Blade_jump_loop_L")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Blade_jump_down_L","T1_Blade_jump_down_L")
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("T1_Blade_jump_start_R","T1_Blade_jump_start_R")   --右跳跃
    BlendMode(0)
    BlendTime(0.2)
		Sound(0.03,"char_05_cloth_flap_02")
	
STake_Start("T1_Blade_jump_loop_R","T1_Blade_jump_loop_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("T1_Blade_jump_down_R","T1_Blade_jump_down_R")
    BlendMode(0)
    BlendTime(0.2)



STake_Start("T1_Blade_wait_001","T1_Blade_wait_001")  --拿灯笼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Blade_move_001","T1_Blade_move_001")  --拿灯笼巡逻
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Blade_emo_003","T1_Blade_emo_003")  --敲锣吆喝
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    Priority(0,"5")     
    BlendPattern("no_sync")
    
STake_Start("T1_Blade_emo_004","T1_Blade_hit_021_loop")  --受伤倒地待机
    BlendMode(0)
    BlendTime(0.2)

	
STake_Start("T1_Blade_hit_016_start","T1_Blade_hit_016")   --击飞start
    BlendMode(0)
    BlendTime(0.2)
		Sound(0.03,"HM_MaleVoc_02_Hurt02")
	
STake_Start("T1_Blade_hit_016_loop","T1_Blade_hit_016_loop")   --击飞loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()
	
STake_Start("T1_Blade_hit_016_end","T1_Blade_hit_016_down")   --击飞end
    BlendMode(0)
    BlendTime(0.2)
		Sound(0.03,"DaoDi_000")
	
STake_Start("T1_Blade_hit_021_start","T1_Blade_hit_021")   --击倒start
    BlendMode(0)
    BlendTime(0.2)
		Sound(0.03,"HM_MaleVoc_02_Hurt02")
		Sound(0.03,"DaoDi_000")
	
STake_Start("T1_Blade_hit_021_loop","T1_Blade_hit_021_loop")   --击倒loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()
	
STake_Start("T1_Blade_hit_021_end","T1_Blade_hit_021_down")   --击倒end
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("T1_Blade_hit_033","T1_Blade_hit_033")   --昏迷
    BlendMode(0)
    BlendTime(0.2)
	Loop()
	
STake_Start("T1_Blade_hit_034","T1_Blade_hit_034")   --插槽技插上
    BlendMode(0)
    BlendTime(0.2)
	Loop()
	
	
STake_Start("T1_Blade_emo_sleep_001","N1_life_034")   --睡觉
    BlendMode(0)
    BlendTime(0.2)
	Loop(0,0.537)
    Direction(2) 
	PlaySpeed(0,0.537,0.4)
	Fx(0.1,"Sleep","Head",0.6,1,-3,-2,0,0,0,0,1,1)

	
STake_Start("T1_Blade_skill_001_start","T1_Blade_skill_001")   --技能25612 普通平砍   吟唱1.53   动作总长2.5
    BlendMode(0)
    BlendTime(0.2)
	FrameTime(0,2.5)
	Pause(1.600,0.16,1,0.012)
	Fx(0,"T1_Blade_skill_001_a")
	Fx(1.600,"T1_Blade_skill_001_b")
	Fx(0.2,"FX_Warning","Head")
	Sound(1.55,"T1_Blade_attack_000")
	Sound(1.46,"HM_MaleVoc_02_attack01")
	 	
STake_Start("T1_Blade_skill_001_fire","")
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.600,0.16,1,0.012)
	
    AttackStart()
  	Hurt(0.06,"hurt_21")
    DirFx(0.06,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.06,"FX_blood_dao_b","Spine1",1,0,0,0,-90,-45,0)
    HurtSound(0.06,"Big_blade_Hit")
		
STake_Start("T1_Blade_skill_002","T1_Blade_skill_002")   --技能25614 普攻二连斩  引导一1.631   引导二1.927  动作总长2.966
    BlendMode(0)
    BlendTime(0.2)
	FrameTime(0,2.966)
    Pause(1.987,0.16,1,0.012)
	Fx(0,"T1_Blade_skill_002_a")
	Fx(1.631,"T1_Blade_skill_002_b")
	Fx(0.2,"FX_Warning","Head")
	Sound(1.55,"T1_Blade_attack_000")
	Sound(1.85,"T1_Blade_attack_000")
	Sound(1.46,"HM_MaleVoc_02_attack01")
	Sound(1.86,"HM_MaleVoc_02_attack02")
	 
    AttackStart()
  	Hurt(1.631,"hurt_21")
	Hurt(1.987,"hurt_21")
    DirFx(1.631,"T1_ZDLBFZ_skill_000_hit",0,1)
	DirFx(1.987,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(1.631,"FX_blood_dao_b","Spine1",1,0,0,0,90,75,0)
    HurtBoneFx(1.987,"FX_blood_dao_b","Spine1",1,0,0,0,-90,-75,0)
    HurtSound(1.631,"Big_blade_Hit")
	HurtSound(1.987,"Big_blade_Hit")

STake_Start("T1_Blade_skill_003_start","T1_Blade_skill_003")   --技能25616 破甲刺  吟唱1.614  动作总长2.266
    BlendMode(0)
    BlendTime(0.2)
	FrameTime(0,2.266)
	Pause(1.676,0.16,1,0.012)
	Fx(0,"T1_Blade_skill_003_a")
	Fx(1.518,"T1_Blade_skill_003_b")
	Fx(0.2,"FX_Warning","Head")
	Sound(1.440,"T1_Blade_attack_000")
	Sound(1.40,"HM_MaleVoc_02_attack02")
	
STake_Start("T1_Blade_skill_003_fire","")
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.676,0.16,1,0.012)
	
	AttackStart()
  	Hurt(0.062,"hurt_21")
    DirFx(0.062,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.062,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.062,"Big_blade_Hit")
	
STake_Start("T1_Blade_skill_000_25638","T1_Blade_skill_000")   --技能25638 攻击提升
    BlendMode(0)
    BlendTime(0.2)
	FrameTime(0,1.733)
	Sound(0.53,"HM_MaleVoc_01_attack03")	
	
	
	
	

---------------------------------------------------吟唱时间减1/4红眼版本-----------------------------------------------------------		
	
STake_Start("T1_Blade_skill_001_level1_start","T1_Blade_skill_001")   --技能25612 普通平砍   吟唱1.145   动作总长2.105
    BlendMode(0)
    BlendTime(0.2)
	FrameTime(0.395,2.5)
	Pause(1.600,0.16,1,0.012)
	Fx(0.4,"T1_Blade_skill_001_a","",1,0,0,0,0,0,0,0,1.5)
	Fx(1.600,"T1_Blade_skill_001_b")
	Fx(0.6,"FX_Warning","Head")
	Sound(1.55,"T1_Blade_attack_000")
	Sound(1.46,"HM_MaleVoc_02_attack01")
	 	
STake_Start("T1_Blade_skill_001_level1_fire","")
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.600,0.16,1,0.012)
	
    AttackStart()
  	Hurt(0.06,"hurt_21")
    DirFx(0.06,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.06,"FX_blood_dao_b","Spine1",1,0,0,0,-90,-45,0)
    HurtSound(0.06,"Big_blade_Hit")
		
STake_Start("T1_Blade_skill_002_level1","T1_Blade_skill_002")   --技能25614 普攻二连斩  引导一1.224   引导二1.52  动作总长2.559
    BlendMode(0)
    BlendTime(0.2)
    Pause(1.987,0.16,1,0.012)
	FrameTime(0.407,2.966)
	Fx(0.407,"T1_Blade_skill_002_a","",1,0,0,0,0,0,0,0,1.5)
	Fx(1.631,"T1_Blade_skill_002_b")
	Fx(0.607,"FX_Warning","Head")
	Sound(1.55,"T1_Blade_attack_000")
	Sound(1.85,"T1_Blade_attack_000")
	Sound(1.46,"HM_MaleVoc_02_attack01")
	Sound(1.86,"HM_MaleVoc_02_attack02")
	 
    AttackStart()
  	Hurt(1.224,"hurt_21")
	Hurt(1.58,"hurt_21")
    DirFx(1.224,"T1_ZDLBFZ_skill_000_hit",0,1)
	DirFx(1.58,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(1.224,"FX_blood_dao_b","Spine1",1,0,0,0,90,75,0)
    HurtBoneFx(1.58,"FX_blood_dao_b","Spine1",1,0,0,0,-90,-75,0)
    HurtSound(1.224,"Big_blade_Hit")
	HurtSound(1.58,"Big_blade_Hit")

STake_Start("T1_Blade_skill_003_level1_start","T1_Blade_skill_003")   --技能25616 破甲刺  吟唱1.201  动作总长1.853
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.676,0.16,1,0.012)
	FrameTime(0.413,2.266)
	Fx(0.413,"T1_Blade_skill_003_a","",1,0,0,0,0,0,0,0,1.5)
	Fx(1.518,"T1_Blade_skill_003_b")
	Fx(0.613,"FX_Warning","Head")
	Sound(1.440,"T1_Blade_attack_000")
	Sound(1.40,"HM_MaleVoc_02_attack02")
	
STake_Start("T1_Blade_skill_003_level1_fire","")
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.676,0.16,1,0.012)
	
	AttackStart()
  	Hurt(0.062,"hurt_21")
    DirFx(0.062,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.062,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.062,"Big_blade_Hit")

---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		

STake_Start("T1_Blade_skill_001_level2_start","T1_Blade_skill_001")   --技能25612 普通平砍   吟唱0.75   动作总长1.71
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.600,0.16,1,0.012)
	FrameTime(0.79,2.5)
	Fx(0.8,"T1_Blade_skill_001_a","",1,0,0,0,0,0,0,0,2)
	Fx(1.600,"T1_Blade_skill_001_b")
	Fx(1,"FX_Warning","Head")
	Sound(1.55,"T1_Blade_attack_000")
	Sound(1.46,"HM_MaleVoc_02_attack01")
	 	
STake_Start("T1_Blade_skill_001_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.600,0.16,1,0.012)
	
    AttackStart()
  	Hurt(0.06,"hurt_21")
    DirFx(0.06,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.06,"FX_blood_dao_b","Spine1",1,0,0,0,-90,-45,0)
    HurtSound(0.06,"Big_blade_Hit")
		
STake_Start("T1_Blade_skill_002_level2","T1_Blade_skill_002")   --技能25614 普攻二连斩  引导一0.815   引导二1.112  动作总长2.151
    BlendMode(0)
    BlendTime(0.2)
    Pause(1.987,0.16,1,0.012)
	FrameTime(0.815,2.966)
	Fx(0.815,"T1_Blade_skill_002_a","",1,0,0,0,0,0,0,0,2)
	Fx(1.631,"T1_Blade_skill_002_b")
	Fx(1.015,"FX_Warning","Head")
	Sound(1.55,"T1_Blade_attack_000")
	Sound(1.85,"T1_Blade_attack_000")
	Sound(1.46,"HM_MaleVoc_02_attack01")
	Sound(1.86,"HM_MaleVoc_02_attack02")
	 
    AttackStart()
  	Hurt(0.815,"hurt_21")
	Hurt(1.172,"hurt_21")
    DirFx(0.815,"T1_ZDLBFZ_skill_000_hit",0,1)
	DirFx(1.172,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.815,"FX_blood_dao_b","Spine1",1,0,0,0,90,75,0)
    HurtBoneFx(1.172,"FX_blood_dao_b","Spine1",1,0,0,0,-90,-75,0)
    HurtSound(0.815,"Big_blade_Hit")
	HurtSound(1.172,"Big_blade_Hit")

STake_Start("T1_Blade_skill_003_level2_start","T1_Blade_skill_003")   --技能25616 破甲刺  吟唱0.787  动作总长1.439
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.676,0.16,1,0.012)
	FrameTime(0.827,2.266)
	Fx(0.827,"T1_Blade_skill_003_a","",1,0,0,0,0,0,0,0,2)
	Fx(1.518,"T1_Blade_skill_003_b")
	Fx(1.027,"FX_Warning","Head")
	Sound(1.440,"T1_Blade_attack_000")
	Sound(1.40,"HM_MaleVoc_02_attack02")
	
STake_Start("T1_Blade_skill_003_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.676,0.16,1,0.012)
	
	AttackStart()
  	Hurt(0.062,"hurt_21")
    DirFx(0.062,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.062,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.062,"Big_blade_Hit")
	
---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	
	
STake_Start("T1_Blade_skill_001_level3_start","T1_Blade_skill_001")   --技能25612 普通平砍   吟唱0.486   动作总长1.448
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.600,0.16,1,0.012)
	FrameTime(1.052,2.5)
	Fx(1.066,"T1_Blade_skill_001_a","",1,0,0,0,0,0,0,0,3)
	Fx(1.600,"T1_Blade_skill_001_b")
	Fx(1.266,"FX_Warning","Head")
	Sound(1.55,"T1_Blade_attack_000")
	Sound(1.46,"HM_MaleVoc_02_attack01")
	 	
STake_Start("T1_Blade_skill_001_level3_fire","")
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.600,0.16,1,0.012)
	
    AttackStart()
  	Hurt(0.06,"hurt_21")
    DirFx(0.06,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.06,"FX_blood_dao_b","Spine1",1,0,0,0,-90,-45,0)
    HurtSound(0.06,"Big_blade_Hit")
		
STake_Start("T1_Blade_skill_002_level3","T1_Blade_skill_002")   --技能25614 普攻二连斩  引导一0.543   引导二0.839  动作总长1.88
    BlendMode(0)
    BlendTime(0.2)
    Pause(1.987,0.16,1,0.012)
	FrameTime(1.088,2.96)
	Fx(1.088,"T1_Blade_skill_002_a","",1,0,0,0,0,0,0,0,3)
	Fx(1.631,"T1_Blade_skill_002_b")
	Fx(1.288,"FX_Warning","Head")
	Sound(1.55,"T1_Blade_attack_000")
	Sound(1.85,"T1_Blade_attack_000")
	Sound(1.46,"HM_MaleVoc_02_attack01")
	Sound(1.86,"HM_MaleVoc_02_attack02")
	 
    AttackStart()
  	Hurt(0.543,"hurt_21")
	Hurt(0.899,"hurt_21")
    DirFx(0.543,"T1_ZDLBFZ_skill_000_hit",0,1)
	DirFx(0.899,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.543,"FX_blood_dao_b","Spine1",1,0,0,0,90,75,0)
    HurtBoneFx(0.899,"FX_blood_dao_b","Spine1",1,0,0,0,-90,-75,0)
    HurtSound(0.543,"Big_blade_Hit")
	HurtSound(0.899,"Big_blade_Hit")

STake_Start("T1_Blade_skill_003_level3_start","T1_Blade_skill_003")   --技能25616 破甲刺  吟唱0.511  动作总长1.164
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.676,0.16,1,0.012)
	FrameTime(1.102,2.266)
	Fx(1.102,"T1_Blade_skill_003_a","",1,0,0,0,0,0,0,0,3)
	Fx(1.518,"T1_Blade_skill_003_b")
	Fx(1.302,"FX_Warning","Head")
	Sound(1.440,"T1_Blade_attack_000")
	Sound(1.40,"HM_MaleVoc_02_attack02")
	
STake_Start("T1_Blade_skill_003_level3_fire","")
    BlendMode(0)
    BlendTime(0.2)
	Pause(1.676,0.16,1,0.012)
	
	AttackStart()
  	Hurt(0.062,"hurt_21")
    DirFx(0.062,"T1_ZDLBFZ_skill_000_hit",0,1)
    HurtBoneFx(0.062,"FX_blood_gongjian_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.062,"Big_blade_Hit")
	

---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
	
	
STake_Start("Common_skill_jumpB_80","T1_Blade_skill_jumpB")  --技能 40009 近战通用后跳  引导0.01charge0.4  动作总长0.899
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,0.899)
	Fx(0.4,"FX_common_skill_jumpB_02","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.08,"char_05_cloth_flap_02")	
	Sound(0.38,"SZ03_dodge_000")	
	
STake_Start("Common_skill_jumpF","T1_Blade_skill_jumpF")  --技能 40001 通用追击  引导一0.01charge0.4  引导二0.46  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,1.2)
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
    Fx(0.41,"FX_common_skill_jumpF","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.28,"TY_Attack_011")
	
  	AttackStart()
  	Hurt(0.46,"hurt_21")    
    DirFx(0.46,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.46,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.46,"HMW01_Hit_002")
    RangeCameraShake(0.46,"ShakeDistance = 80","ShakeMode = 3","ShakeTimes = 0.14","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")


STake_Start("Common_skill_charge_200","T1_Blade_skill_attack")    --技能  40011  通用charge技能200距离  引导1.6charge0.2  动作总长2.666
    BlendMode(0)
	BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,2.666)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.68,"TY_Attack_011")
	
    AttackStart()
	Hurt(1.7,"hurt_21")
	HurtSound(1.7,"Big_blade_Hit_002_1")
	DirFx(1.7,"",0,1)
    HurtBoneFx(1.7,"","Spine1",1,0,0,0,0,0,0)
	

STake_Start("Common_skill_charge_200_level1","T1_Blade_skill_attack")    --技能  40011  通用charge技能200距离  引导1.2charge0.2  动作总长2.266
    BlendMode(0)
	BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.4,2.666)
	Fx(0.6,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.68,"TY_Attack_011")
	
    AttackStart()
	Hurt(1.3,"hurt_21")
	HurtSound(1.3,"Big_blade_Hit_002_1")
	DirFx(1.3,"",0,1)
    HurtBoneFx(1.3,"","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("Common_skill_charge_200_level2","T1_Blade_skill_attack")    --技能  40011  通用charge技能200距离  引导0.8charge0.2  动作总长1.866
    BlendMode(0)
	BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.8,2.666)
	Fx(1,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(1.68,"TY_Attack_011")
	
    AttackStart()
	Hurt(0.9,"hurt_21")
	HurtSound(0.9,"Big_blade_Hit_002_1")
	DirFx(0.9,"",0,1)
    HurtBoneFx(0.9,"","Spine1",1,0,0,0,0,0,0)
	
	
	
-------------------------------------------------------------------------------------------------------------------------------
--CutScene用

--完整基础动作

STake_Start("CS_walk1_DengLongBing","T1_Blade_move_001") 
    Fx(0.0000,"FX_CSLight_4s")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait1_DengLongBing","T1_Blade_emo_001") 
    Fx(0.0000,"FX_CSLight_3s")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()




--CS3活捉八海

--单晓箫配

STake_Start("CS_dead1_T1_Blade","T1_Blade_dead")
    BlendMode(2)
    BlendTime(0.2)
    Loop(1.96999,1.97)

STake_Start("CS_dead_S_T1_Blade","T1_Blade_dead")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.96999,1.97)
    Loop()

STake_Start("CS_dead2_T1_Blade","T1_Blade_dead_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop(1.37299,1.373)

STake_Start("CS_dead2_S_T1_Blade","T1_Blade_dead_000")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.37299,1.373)
    Loop()

STake_Start("CS_dead3_T1_Blade","T1_Blade_dead_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop(1.37299,1.373)

STake_Start("CS_dead3_S_T1_Blade","T1_Blade_dead_001")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.37299,1.373)
    Loop()


STake_Start("CS_beath_slow_T1_Blade","T1_Blade_B_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    PlaySpeed(0,1,0.7)
    Loop()

STake_Start("CS_walk1_T1_Blade","T1_Blade_move_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_walk_fight1_T1_Blade","T1_Blade_move_003")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_walk2_T1_Blade","T1_Blade_move_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_run1_T1_Blade","T1_Blade_move_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_run2_T1_Blade","T1_DoubleBlade_B_move_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_run3_T1_Blade","T1_SBlade_move_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait_fight_T1_Blade","T1_Blade_B_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_attack1_T1_Blade","T1_Blade_attack_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_attack2_T1_Blade","T1_Blade_attack_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_def1_T1_Blade","T1_Blade_def_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

--CS4克什克

--俞章廷配


STake_Start("CS_attack_000_copy_T1_Blade","T1_Blade_attack_000")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_dead_002_copy_T1_Blade","T1_Blade_dead_002")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_dead_pose1_T1_Blade","T1_Blade_dead_pose")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_dead_pose2_T1_Blade","T1_Blade_dead_003")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.26635,1.26636) 
    Loop(1.26635,1.26636)  

STake_Start("CS_dead_pose3_T1_Blade","T1_Blade_dead_002")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.26635,1.26636) 
    Loop(1.26635,1.26636)   

STake_Start("CS_Jump_001_T1_Blade","N1_Blade3_B_move_014")  ----------起跳动作
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_Jump_002_T1_Blade","N1_Blade3_B_move_015")  ----------滞空动作
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_Jump_003_T1_Blade","N1_Blade3_B_move_016")  ----------落地动作
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_jumpkill_T1_Blade","N1_Blade3_skill_000")  ----------跳刺动作
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_def_sand_001_T1_Blade","N1_Blade3_def_000")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,0.18) 
    Loop(0.17,0.18)   

STake_Start("CS_B_wait_000_T1_Blade","T1_Blade_B_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_B_dead_000_T1_Blade","T1_Blade_dead_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_move_004_T1_Blade","T1_Blade_move_004")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_dead_003_T1_Blade","T1_Blade_dead_003")
    BlendMode(2)
    BlendTime(0.2)
    Loop()
STake_Start("CS_B_dead_002_T1_Blade","T1_Blade_dead_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_def_000_T1_Blade","T1_Blade_def_000")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_attack_001_T1_Blade","T1_Blade_attack_001")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_attack_002_T1_Blade","T1_Blade_attack_002")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_move_003_T1_Blade","T1_Blade_move_003")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_dead_000_T1_Blade","T1_Blade_dead_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop(1.3999,1.4)


STake_Start("CS_def2_T1_Blade","T1_Blade_def_000")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,0.2) 
    Loop(0.19,0.2)





-----------------------------------------------------------------------------------
--CutScene用
-----------------------------------------------------------------------------------
--塞外CUTSCENE用动作

--CS4大军冲

--单晓箫配

STake_Start("CS_SW_look_around1_T1_Blade","N_T1_LieHu02_emo_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_look_around2_T1_Blade","T1_ZDLBFZ_emo_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_link_T1_Blade","T1_Blade_link_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_link2_T1_Blade","T1_wait_001_emo_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_wait1_T1_Blade","T1_Blade_B_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_wait2_T1_Blade","T1_Blade_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_ZhiHui1_T1_Blade","T1_Blade_B_emo_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_ZhiHui2_T1_Blade","T1_Blade_skill_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_ZhiHui2a_T1_Blade","T1_Blade_skill_002")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0.08,1.1)   
    Loop()

STake_Start("CS_SW_ZhiHui2b_T1_Blade","T1_Blade_skill_002")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0.08,1.1)   
    Loop()

STake_Start("CS_SW_attack1_T1_Blade","T1_Blade_skill_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_Tui_T1_Blade","YP_move_005")
    BlendMode(2)
    BlendTime(0.2)
    PlaySpeed(0,2,1.35)
    Loop()

STake_Start("CS_SW_def_T1_Blade","N1_Blade3_def_000")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0.03463,0.03464) 
    Loop()

STake_Start("CS_SW_dead1_T1_Blade","T1_Blade_dead_002")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,1.26636) 
    Loop(1.26635,1.26636)  

STake_Start("CS_SW_dead1_S_T1_Blade","T1_Blade_dead_002")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.26635,1.26636) 
    Loop()  

STake_Start("CS_SW_dead2_T1_Blade","T1_Blade_dead_003")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,1.26636) 
    Loop(1.26635,1.26636)  

STake_Start("CS_SW_dead2_S_T1_Blade","T1_Blade_dead_003")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(1.26635,1.26636) 
    Loop() 

STake_Start("CS_SW_Dao1_T1_Blade","YP_hit_021")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.768)   
    Loop(0.76799,0.768)

STake_Start("CS_SW_Dao2_T1_Blade","YP_hit_023")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.73304)   
    Loop(0.73303,0.73304)

STake_Start("CS_SW_Dao3_T1_Blade","YP_hit_024")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.73304)   
    Loop(0.73303,0.73304)



-------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_T1_Blade_emo_002","T1_Blade_emo_002")
    BlendMode(2)
    BlendTime(0.2)

STake_Start("CS_T1_Blade_dodge_000","T1_Blade_dodge_000")
    BlendMode(2)
    BlendTime(0.2)

-----------------------------------------------------------------------------------

--牛家村CUTSCENE用动作

--CS4金兵来袭

--张阳配

STake_Start("CS_NJC_move_003_T1_Blade","T1_Blade_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_move_004_T1_Blade","T1_Blade_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_wait_000_T1_Blade","T1_Blade_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_attack_000_a_T1_Blade","T1_Blade_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_attack_000_b_T1_Blade","T1_Blade_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    PlaySpeed(0.16,0.21,0.2)
    Loop(1.9999,2)

STake_Start("CS_NJC_attack_001_a_T1_Blade","T1_Blade_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_attack_001_b_T1_Blade","T1_Blade_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    PlaySpeed(0.23,0.28,0.15)
    Loop(1.9999,2)

STake_Start("CS_NJC_attack_002_a_T1_Blade","T1_Blade_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_attack_002_b_T1_Blade","T1_Blade_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.1,1) 
    Loop()

STake_Start("CS_NJC_skill_002_T1_Blade","T1_SBlade_skill_002")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_dead_002_T1_Blade","T1_Blade_dead_002")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,1.4)
    Loop(1.3999,1.4)

STake_Start("CS_NJC_dead_000_T1_Blade","T1_Blade_dead_000")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,1.386)
    Loop(1.3859,1.386) 

-----------------------------------------------------------------------------------

--哑仆受击

--单晓箫配
	
STake_Start("CS_YP_wait_000_T1_Blade","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 	
	
STake_Start("CS_YP_wait2_T1_Blade","YP_hit_033")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()			   
 				 
STake_Start("CS_YP_dead_T1_Blade","YP_dead")
    BlendMode(0)
    BlendTime(0.2)   
    Loop(1.69999,1.7) 
 				 
STake_Start("CS_YP_dead_S_T1_Blade","YP_dead")
    BlendMode(0)
    BlendTime(0.2)   
    FrameTime(1.69999,1.7) 
    Loop()
		
STake_Start("CS_YP_dead_000_T1_Blade","YP_dead_000")
    BlendMode(0)
    BlendTime(0.2)  	
    Loop(1.29999,1.3) 		
		
STake_Start("CS_YP_dead_000_S_T1_Blade","YP_dead_000")
    BlendMode(0)
    BlendTime(0.2)  	
    FrameTime(1.29999,1.3) 	
    Loop()	
		
STake_Start("CS_YP_dead_001_T1_Blade","YP_dead")
    BlendMode(0)
    BlendTime(0.2)  
    Loop(1.29999,1.3) 		
		
STake_Start("CS_YP_dead_001_S_T1_Blade","YP_dead")
    BlendMode(0)
    BlendTime(0.2)  
    FrameTime(1.29999,1.3) 	
    Loop()		
		
STake_Start("CS_YP_dead_002_T1_Blade","YP_dead_002")
    BlendMode(0)
    BlendTime(0.2)  	
    Loop(1.29999,1.3) 		
		
STake_Start("CS_YP_dead_002_S_T1_Blade","YP_dead_002")
    BlendMode(0)
    BlendTime(0.2)  	
    FrameTime(1.29999,1.3) 	
    Loop()		
		
STake_Start("CS_YP_dead_003_T1_Blade","YP_dead_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.29999,1.3) 		
		
STake_Start("CS_YP_dead_S_003_T1_Blade","YP_dead_003")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.29999,1.3) 
    Loop()
	
STake_Start("CS_YP_hit_000_T1_Blade","YP_hit_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_001_T1_Blade","YP_hit_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()  

STake_Start("CS_YP_hit_002_T1_Blade","YP_hit_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_003_T1_Blade","YP_hit_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_006_T1_Blade","YP_hit_006")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_007_T1_Blade","YP_hit_007")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_008_T1_Blade","YP_hit_008")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_009_T1_Blade","YP_hit_009")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
		
STake_Start("CS_YP_hit_011_T1_Blade","YP_hit_011")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 	
		
STake_Start("CS_YP_hit_012_T1_Blade","YP_hit_012")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_013_T1_Blade","YP_hit_013")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_014_T1_Blade","YP_hit_014")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_016_T1_Blade","YP_hit_016")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.49999,0.5) 

STake_Start("CS_YP_hit_017_T1_Blade","YP_hit_017")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
		
STake_Start("CS_YP_hit_018_T1_Blade","YP_hit_018")
    BlendMode(0)
    BlendTime(0.2) 	
    Loop() 	
	
STake_Start("CS_YP_hit_019_T1_Blade","YP_hit_019")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_021_T1_Blade","YP_hit_021")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.69999,0.7) 
	
STake_Start("CS_YP_hit_022_T1_Blade","YP_hit_022")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.64999,0.65) 
	
STake_Start("CS_YP_hit_023_T1_Blade","YP_hit_023")
    BlendMode(0)
    BlendTime(0.02) 
    Loop(0.59999,0.6) 
	
STake_Start("CS_YP_hit_024_T1_Blade","YP_hit_024")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.69999,0.7) 
	
STake_Start("CS_YP_hit_026_T1_Blade","YP_hit_026")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_027_T1_Blade","YP_hit_027")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_028_T1_Blade","YP_hit_028")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_030_T1_Blade","YP_hit_030")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_031_T1_Blade","YP_hit_031")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_032_T1_Blade","YP_hit_032")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 



-----------------------------------------------------------------------------------
	
--牛家村CUTSCENE用动作

--CS7黄药师帅

--单晓箫配

STake_Start("CS_NJC_TuXi_T1_Blade","T1_Blade_skill_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_NJC_XuLi_T1_Blade","T1_Blade_skill_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_NJC_wait_T1_Blade","T1_SBlade_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_attack_T1_Blade","T1_SBlade_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_NJC_attack1_T1_Blade","T1_SBlade_attack_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_NJC_attack2_T1_Blade","T1_SBlade_attack_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_NJC_attack3_T1_Blade","T1_SBlade_attack_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_NJC_attack4_T1_Blade","T1_SJMJS_attack_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 


STake_Start("CS_NJC_skill_T1_Blade","T1_SBlade_skill_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_NJC_skill1_T1_Blade","T1_SBlade_skill_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_NJC_skill2_T1_Blade","T1_SBlade_skill_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_NJC_look_around1_T1_Blade","T1_Saw_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_look_down1_T1_Blade","T1_SJMJS_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_roar1_T1_Blade","Boss_T1_skill_002_30")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_roar1_wait_T1_Blade","T1_DoubleMace_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_walk1_slow_T1_Blade","T1_DoubleBlade_B_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,1.133,0.333)

STake_Start("CS_NJC_walk2_slow_T1_Blade","T1_Blade_move_003")
    BlendMode(0)
    BlendTime(0.1) 
    Loop()
    PlaySpeed(0,1.133,0.333)

STake_Start("CS_NJC_walk3_slow_T1_Blade","T1_CS_NJC_walk_001")
    BlendMode(0)
    BlendTime(0.1) 
    Loop()
    PlaySpeed(0,1.133,0.5)

STake_Start("CS_NJC_air_dead1_T1_Blade","T1_Blade_CS_hit01")
    BlendMode(0)
    BlendTime(0.1) 
    Loop(4.8659,4.866)

STake_Start("CS_NJC_BeiDian_T1_Blade","T1_CS_NJC_hit_001")
    BlendMode(0)
    BlendTime(0.1) 
    Loop(1.5999,1.6)

STake_Start("CS_NJC_look_right_T1_Blade","N_T1_LieHu02_emo_001")
    BlendMode(2)
    BlendTime(0.2)
    FrameTime(0,1.2)  
    PlaySpeed(0.8,1.2,0.2)
    Loop(1.1999,1.2)

STake_Start("CS_NJC_end_attack_T1_Blade","T1_SBlade_skill_000")
    BlendMode(0)
    BlendTime(0.1) 
    FrameTime(0,0.365) 
    Loop(0.3649,0.365)

STake_Start("CS_NJC_walk3_T1_Blade","T1_CS_NJC_walk_001")
    BlendMode(0)
    BlendTime(0.1) 
    Loop()

package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")



STake_Start("Monster_wait_000","T1_DoubleBlade_wait_000")     --待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
	
STake_Start("Monster_emo_001","T1_DoubleBlade_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_emo_002","T1_DoubleBlade_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_B_wait_000","T1_DoubleBlade_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
	
STake_Start("Monster_B_emo_001","T1_DoubleBlade_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_move_000","T1_DoubleBlade_move_000")    --移动
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("Monster_B_move_003","T1_DoubleBlade_B_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
	
STake_Start("Monster_B_move_003_L","T1_DoubleBlade_B_move_003_L")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_move_003_R","T1_DoubleBlade_B_move_003_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_move_004","T1_DoubleBlade_B_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("Monster_B_move_005","T1_DoubleBlade_B_move_003_L")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
	
STake_Start("Monster_B_move_006","T1_DoubleBlade_B_move_003_R")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
	
STake_Start("Monster_B_move_007","T1_DoubleBlade_B_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
	
STake_Start("Monster_B_move_008","T1_DoubleBlade_B_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
	
STake_Start("Monster_dead","T1_DoubleBlade_dead")    --死亡
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.475,"T1_DoubleBlade_dead")
    Sound(1.775,"DaoDi_003")

STake_Start("Monster_dead_000","T1_DoubleBlade_dead_000")
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.180,"T1_DoubleBlade_dead_000")

STake_Start("Monster_dead_001","T1_DoubleBlade_dead_001")
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.312,"T1_DoubleBlade_dead")
    Sound(1.451,"DaoDi_003") 

STake_Start("Monster_dead_002","T1_DoubleBlade_dead_002")
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.219,"T1_DoubleBlade_dead_000")

STake_Start("Monster_dead_003","T1_DoubleBlade_dead_003")
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.256,"T1_DoubleBlade_dead_000")

STake_Start("Monster_bout_000","T1_DoubleBlade_bout_000")   --出生
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("Monster_jump_start_L","T1_DoubleBlade_jump_start_L")   --左跳开始
    BlendMode(0)
    BlendTime(0.2) 
	
STake_Start("Monster_jump_loop_L","T1_DoubleBlade_jump_loop_L")   --左跳loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()

STake_Start("Monster_jump_down_L","T1_DoubleBlade_jump_down_L")   --左跳结束
    BlendMode(0)
    BlendTime(0.2) 
	
STake_Start("Monster_jump_start_R","T1_DoubleBlade_jump_start_R")   --右跳开始
    BlendMode(0)
    BlendTime(0.2) 
	
STake_Start("Monster_jump_loop_R","T1_DoubleBlade_jump_loop_R")   --右跳loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()

STake_Start("Monster_jump_down_R","T1_DoubleBlade_jump_down_R")   --右跳结束
    BlendMode(0)
    BlendTime(0.2) 
	
STake_Start("Monster_jump_start_B","T1_DoubleBlade_jump_start_B")   --后跳开始
    BlendMode(0)
    BlendTime(0.2) 
	
STake_Start("Monster_jump_loop_B","T1_DoubleBlade_jump_loop_B")   --后跳loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()

STake_Start("Monster_jump_down_B","T1_DoubleBlade_jump_down_B")   --后跳结束
    BlendMode(0)
    BlendTime(0.2) 




STake_Start("T1_DoubleBlade_skill_000","T1_DoubleBlade_skill_000")    --技能  普攻平砍  引导一1.770  动作总长3.2
    BlendMode(0)
	BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3.2)
	--PlaySpeed(1.778,1.780,0.00012)
	Pause(1.778,0.16,1,0.012)
	Fx(0.2,"FX_Warning","Head")
	--Fx(1.770,"FX_Violent_blood_Blunt_R1","Spine1",1,0,0,0,0,0,0,0)

    AttackStart()
	Hurt(1.770,"hurt_21")
	DirFx(1.770,"",0,1)
	
	
STake_Start("T1_DoubleBlade_skill_001","T1_DoubleBlade_skill_001")    --技能  普攻二连斩  引导一1.840  引导二2.340  动作总长3.333
    BlendMode(0)
	BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3.333)
	--PlaySpeed(1.848,1.850,0.012)
	--PlaySpeed(2.348,2.350,0.012)
	Pause(1.848,0.16,1,0.012)
	Pause(2.348,0.16,1,0.012)
	Fx(0.2,"FX_Warning","Head")
	--Fx(1.840,"FX_Violent_blood_Blunt_R1","Spine1",1,0,0,0,0,0,0,0)
	--Fx(2.340,"FX_Violent_blood_Blunt_R1","Spine1",1,0,0,0,0,0,0,0)

    AttackStart()
	Hurt(1.840,"hurt_21")
	Hurt(2.340,"hurt_21")
	DirFx(1.840,"",0,1)
	DirFx(2.340,"",0,1)
	

STake_Start("T1_DoubleBlade_skill_002","T1_DoubleBlade_skill_002")    --技能  普攻四连斩  引导一1.71  引导二2.025  引导三2.43  引导四3.120  动作总长4.5
    BlendMode(0)
	BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,4.5)
	--PlaySpeed(3.128,3.130,0.012)
	Pause(3.128,0.16,1,0.012)
	Fx(0.2,"FX_Warning","Head")
	--Fx(1.71,"FX_Violent_blood_Blunt_R1","Spine1",1,0,0,0,0,0,0,0)
	--Fx(2.025,"FX_Violent_blood_Blunt_R1","Spine1",1,0,0,0,0,0,0,0)
	--Fx(2.43,"FX_Violent_blood_Blunt_R1","Spine1",1,0,0,0,0,0,0,0)
	--Fx(3.12,"FX_Violent_blood_Blunt_R1","Spine1",1,0,0,0,0,0,0,0)

    AttackStart()
	Hurt(1.71,"hurt_21")
	Hurt(2.025,"hurt_21")
	Hurt(2.43,"hurt_21")
	Hurt(3.120,"hurt_21")
	DirFx(1.71,"",0,1)
	DirFx(2.025,"",0,1)
	DirFx(2.43,"",0,1)
	DirFx(3.120,"",0,1)


STake_Start("T1_DoubleBlade_skill_003","T1_DoubleBlade_skill_003")    --技能  攻击提升  瞬发  动作总长1.766
    BlendMode(0)
	BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,1.766)
    Fx(0,"")package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")

    
    
STake_Start("T1_Hammer_def_000","T1_Hammer_def_000")
    BlendMode(0)
    BlendTime(0.2)


    Sound(0.021,"NB_KSK_def_000")

STake_Start("T1_Hammer_dodge_000","T1_Hammer_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.080,"NB_TC_dedge_000")

STake_Start("T1_Hammer_B_wait_000","T1_Hammer_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","T1_Hammer_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Hammer_dead","T1_Hammer_dead")
    BlendMode(0)
    BlendTime(0.2)


    Sound(1.540,"TongYong_DaoDi_001")
    Sound(0.203,"NB_KSK_emo_001")
    Sound(0.317,"Martialsword_dead_VOC")
    Sound(1.631,"NB_KSK_emo_001") 


STake_Start("T1_Hammer_dead_000","T1_Hammer_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.578,"TongYong_DaoDi_001")
    Sound(1.587,"NB_KSK_emo_001")
    Sound(0.368,"Martialsword_dead_VOC")
    Sound(1.771,"NB_KSK_emo_001") 


STake_Start("T1_Hammer_dead_001","T1_Hammer_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.539,"TongYong_DaoDi_001")
    Sound(1.656,"NB_KSK_emo_001")
    Sound(0.326,"Martialsword_dead_VOC")
    Sound(1.489,"NB_KSK_emo_001") 

STake_Start("T1_Hammer_dead_002","T1_Hammer_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.539,"TongYong_DaoDi_001")
    Sound(1.656,"NB_KSK_emo_001")
    Sound(0.326,"Martialsword_dead_VOC")
    Sound(1.489,"NB_KSK_emo_001") 

STake_Start("T1_Hammer_dead_003","T1_Hammer_dead_003")
    BlendMode(0)
    BlendTime(0.2)


    Sound(1.578,"TongYong_DaoDi_001")
    Sound(1.587,"NB_KSK_emo_001")
    Sound(0.368,"Martialsword_dead_VOC")
    Sound(1.771,"NB_KSK_emo_001") 


STake_Start("T1_Hammer_emo_001","T1_Hammer_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Hammer_emo_002","T1_Hammer_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Hammer_move_000","T1_Hammer_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Hammer_B_move_003","T1_Hammer_B_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Hammer_B_move_004","T1_Hammer_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Hammer_wait_000","T1_Hammer_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()



























package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("T1_life")
require("YP_hurt")
package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("T1_life")
require("N1_life")


STake_Start("Monster_B_emo_001","T1_Polearm_B_emo_001")  --战斗嘲讽
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("Monster_B_wait_000","T1_Polearm_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_dead","T1_Polearm_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.386,"T1_Polearm_dead")
    Sound(0.03,"T1_SBlade_Dead")
    Sound(0.850,"DaoDi_000")
    Sound(1.391,"DaoDi_002")

STake_Start("Monster_dead_000","T1_Polearm_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.390,"DaoDi_000")
    Sound(0.03,"T1_SBlade_Dead")
    Sound(0.540,"T1_Polearm_dead_000")

STake_Start("Monster_dead_001","T1_Polearm_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.416,"DaoDi_000")
    Sound(0.03,"T1_SBlade_Dead")
    Sound(0.416,"T1_Polearm_dead_000")

STake_Start("Monster_dead_002","T1_Polearm_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.500,"DaoDi_000")
    Sound(0.03,"T1_SBlade_Dead")
    Sound(0.602,"T1_Polearm_dead_000")

STake_Start("Monster_dead_003","T1_Polearm_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.479,"DaoDi_000")
    Sound(0.03,"T1_SBlade_Dead")
    Sound(0.613,"T1_Polearm_dead_000")

STake_Start("Monster_def_000","T1_Polearm_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.600,"T1_Polearm_def_000")

STake_Start("Monster_dodge_000","T1_Polearm_dodge_000")
    BlendMode(0)
    BlendTime(0.2)
    
    Sound(0.048,"SZ03_dodge_000")

STake_Start("Monster_emo_001","T1_Polearm_emo_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.368,"N1_Axe_emo_002_1")
    Sound(0.764,"N1_Axe_emo_002_1")
    Sound(1.161,"N1_Axe_emo_002_1")
    Sound(1.699,"N1_Axe_emo_002_1")

STake_Start("Monster_emo_002","T1_Polearm_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_link_000","T1_Polearm_link_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_link_001","T1_Polearm_link_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_move_000","T1_Polearm_move_000")  --普通走
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_move_003","T1_Polearm_move_003")  --战斗前走
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_move_003_L","T1_Polearm_B_move_003_R")  --战斗左走
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_move_003_R","T1_Polearm_B_move_003_L")  --战斗右走
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_move_000_L","T1_Polearm_B_move_003_L")  --左走
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_move_000_R","T1_Polearm_B_move_003_R")  --右走
    BlendMode(0)
    BlendTime(0.2)
    Loop()	
	
	
STake_Start("Monster_B_move_004","T1_Polearm_move_004")  --战斗跑
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_start","N1_move_005-007") --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("Monster_jump_loop","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_down","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)	
	
STake_Start("Monster_jump_start_B","T1_Polearm_jump_start_B")  --后跳起跳
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("Monster_jump_loop_B","T1_Polearm_jump_loop_B")  --后跳loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()

STake_Start("Monster_jump_down_B","T1_Polearm_jump_down_B")  --后跳落地
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("Monster_jump_start_L","T1_Polearm_jump_start_L")  --左跳起跳
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("Monster_jump_loop_L","T1_Polearm_jump_loop_L")  --左跳loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()

STake_Start("Monster_jump_down_L","T1_Polearm_jump_down_L")  --左跳落地
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("Monster_jump_start_R","T1_Polearm_jump_start_R")  --右跳起跳
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("Monster_jump_loop_R","T1_Polearm_jump_loop_R")  --右跳loop
    BlendMode(0)
    BlendTime(0.2)
	Loop()

STake_Start("Monster_jump_down_R","T1_Polearm_jump_down_R")  --右跳落地
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("Monster_wait_000","T1_Polearm_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()


STake_Start("Monster_bout_000","T1_bout_001")   --出生
    BlendMode(0)
    BlendTime(0.2) 	



STake_Start("T1_Polearm_skill_004_start","T1_Polearm_skill_001_new")  --普攻  27010  吟唱1.826  动作总长2.666
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,2.666)
    --PlaySpeed(1.892,1.895,0.012)
	Pause(1.892,0.25,1,0.012)
	
	Sound(0.32,"T1_Blade_move_1")
    Sound(1.8,"T1_Polearm_skill_000")
    Fx(1.839,"T1_Polearm_skill_001_new_fire","Reference",0.8,0,0,-15,35,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)

	
STake_Start("T1_Polearm_skill_004_fire","")  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Pause(1.892,0.25,1,0.012)
	
    AttackStart()	
	Hurt(0.066,"hurt_21")
    HurtSound(0.066,"blade_Hit")
    DirFx(0.066,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.066,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,15,0)
	
	
STake_Start("T1_Polearm_skill_005","T1_Polearm_skill_002_new")  --六连刺    27012  引导一1.76  引导二2.1  引导三2.46  引导四2.87  引导五3.15  引导六3.425  动作总长4.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,4.1)
	--PlaySpeed(3.485,3.488,0.012)
	Pause(3.485,0.25,1,0.012)
	Fx(1.763,"WQL_skill_004_fire","T_R",0.6,1,10,40,0,0,0,0,2.5,1)
	Fx(2.132,"WQL_skill_004_fire","T_R",0.6,1,10,40,9,0,0,0,2.5,1)
	Fx(2.46,"WQL_skill_004_fire","T_R",0.6,1,10,40,1,0,0,0,2.5,1)
	Fx(2.829,"WQL_skill_004_fire","T_R",0.6,1,5,40,17,0,0,0,2.5,1)
	Fx(3.116,"WQL_skill_004_fire","T_R",0.6,1,10,40,-5,0,0,0,2.5,1)
	Fx(3.444,"WQL_skill_004_fire","T_R",0.6,1,10,40,0,0,0,0,2.5,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Sound(1.7,"TY_Attack_013")
    Sound(2.1,"TY_Attack_013")
    Sound(2.4,"TY_Attack_013")
    Sound(2.8,"TY_Attack_013")
    Sound(3.1,"TY_Attack_013")
    Sound(3.4,"TY_Attack_013")

	
    AttackStart()	
	Hurt(1.76,"hurt_21")
	Hurt(2.1,"hurt_21")	
	Hurt(2.46,"hurt_21")	
	Hurt(2.87,"hurt_21")	
	Hurt(3.15,"hurt_21")	
	Hurt(3.5,"hurt_21")	
    HurtSound(1.76,"blade_Hit")	
    HurtSound(2.1,"blade_Hit")	
    HurtSound(2.46,"blade_Hit")	
    HurtSound(2.87,"blade_Hit")	
    HurtSound(3.15,"blade_Hit")
	HurtSound(3.5,"blade_Hit")
    DirFx(1.76,"T1_Polearm_skill_001_hit",0,1,0.2)  
	DirFx(2.1,"T1_Polearm_skill_001_hit",0,1,0.2)  
	DirFx(2.46,"T1_Polearm_skill_001_hit",0,1,0.25)  
	DirFx(2.87,"T1_Polearm_skill_001_hit",0,1,0.25)  
	DirFx(3.15,"T1_Polearm_skill_001_hit",0,1,0.3)  
	DirFx(3.5,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(1.76,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,20,0)
    HurtBoneFx(2.1,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(2.46,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(2.87,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,-10,0)
    HurtBoneFx(3.15,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,15,0)
    HurtBoneFx(3.5,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,0,0)

	
STake_Start("T1_Polearm_skill_004_2_start","T1_Polearm_skill_001_new")  --技能  29709  前刺  吟唱1.826  动作总长2.666
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,2.666)
    --PlaySpeed(1.892,1.895,0.012)
	Pause(1.892,0.25,1,0.012)
	
	Sound(0.32,"T1_Blade_move_1")
    Sound(1.5,"MQX_Skill_000_1")
	Sound(1.4,"QQJ_attack_001")
    Fx(0,"T1_Polearm_skill_001","Reference",0.5,0,0,-15,35,0,0,0,1,2)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	
STake_Start("T1_Polearm_skill_004_2_fire","")  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Pause(1.892,0.25,1,0.012)
	
    AttackStart()	
	Hurt(0.066,"hurt_21")
    HurtSound(0.066,"blade_Hit")
    DirFx(0.066,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.066,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,15,0)


STake_Start("T1_Polearm_skill_005_1","T1_Polearm_skill_005")  --技能  29710  虎炎  吟唱1.6charge0.2  动作总长2.5
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	FrameTime(0,2.5)
    Fx(1.6,"T1_Polearm_skill_005_fire","Reference",1,0,0,0,0,0,0,0,1,2)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.32,"T1_Blade_move_1")
    Sound(1.5,"MQX_Skill_000_1")
	Sound(1.4,"QQJ_Skill000")
	
    AttackStart()	
	Hurt(1.7,"hurt_61")
    HurtSound(1.7,"blade_Hit")
    DirFx(1.7,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(1.7,"FX_blood_changqiang_b","Spine1",1,0,0,0,-80,0,0)
	

STake_Start("T1_Polearm_skill_006_start","T1_Polearm_skill_004")  --技能  29711  横扫  吟唱1.610  动作总长3.166
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	FrameTime(0,3.166)
    --PlaySpeed(1.670,1.672,0.012)	
	Pause(1.670,0.16,1,0.012)
    Fx(0,"T1_Polearm_skill_004","Reference",1,0,0,0,0,0,0,0,1,2)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.32,"WH_attack_001")
    Sound(1.4,"MQX_Skill_000_1")
	Sound(1.3,"QQJ_attack_001")
	
STake_Start("T1_Polearm_skill_006_fire","")  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	Pause(1.670,0.16,1,0.012)	
	
    AttackStart()	
	Hurt(0.06,"hurt_21")
    HurtSound(0.06,"blade_Hit")
    DirFx(0.06,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.06,"FX_blood_changqiang_b","Spine1",1,0,0,0,-80,0,0)
	
	
---------------------------------------------------吟唱时间减1/4红眼版本-----------------------------------------------------------		
STake_Start("T1_Polearm_skill_004_2_start_level1","T1_Polearm_skill_001_new")  --技能  29709  前刺  吟唱1.2  动作总长2.04
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0.626,2.666)
    --PlaySpeed(1.892,1.895,0.012)
	Pause(1.892,0.25,1,0.012)
	
	Sound(0.32,"T1_Blade_move_1")
    Sound(1.5,"MQX_Skill_000_1")
	Sound(1.4,"QQJ_attack_001")
    Fx(0.626,"T1_Polearm_skill_001","Reference",0.5,0,0,-15,35,0,0,0,1.5,2)
	Fx(1.226,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)

STake_Start("T1_Polearm_skill_004_2_fire_level1","")  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Pause(1.892,0.25,1,0.012)
	
    AttackStart()	
	Hurt(0.066,"hurt_21")
    HurtSound(0.066,"blade_Hit")
    DirFx(0.066,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.066,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,15,0)
	
	
STake_Start("T1_Polearm_skill_005_1_level1","T1_Polearm_skill_005")  --技能  29710  虎炎  吟唱1.2charge0.2  动作总长2.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	FrameTime(0.4,2.5)
    Fx(1.6,"T1_Polearm_skill_005_fire","Reference",1,0,0,0,0,0,0,0,1,2)
	Fx(0.6,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.32,"T1_Blade_move_1")
    Sound(1.5,"MQX_Skill_000_1")
	Sound(1.4,"QQJ_Skill000")

    AttackStart()	
	Hurt(1.3,"hurt_61")
    HurtSound(1.3,"blade_Hit")
    DirFx(1.3,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(1.3,"FX_blood_changqiang_b","Spine1",1,0,0,0,-80,0,0)
	  		
	
STake_Start("T1_Polearm_skill_006_start_level1","T1_Polearm_skill_004")  --技能  29711  横扫  吟唱1.2  动作总长2.756
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	FrameTime(0.41,3.166)
    --PlaySpeed(1.670,1.672,0.012)	
	Pause(1.670,0.16,1,0.012)
    Fx(0.41,"T1_Polearm_skill_004","Reference",1,0,0,0,0,0,0,0,1.3,2)
	Fx(0.61,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.32,"WH_attack_001")
    Sound(1.4,"MQX_Skill_000_1")
	Sound(1.3,"QQJ_attack_001")
	
STake_Start("T1_Polearm_skill_006_fire_level1","")  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	Pause(1.670,0.16,1,0.012)	
	
    AttackStart()	
	Hurt(0.06,"hurt_21")
    HurtSound(0.06,"blade_Hit")
    DirFx(0.06,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.06,"FX_blood_changqiang_b","Spine1",1,0,0,0,-80,0,0)


---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		
	

STake_Start("T1_Polearm_skill_004_level2_start","T1_Polearm_skill_001_new")  --技能 27010  近身刺    吟唱0.88    动作总长1.72
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Pause(1.892,0.25,1,0.012)
	FrameTime(0.946,2.666)
    Sound(1.8,"T1_Polearm_skill_000")
    Fx(1.839,"T1_Polearm_skill_001_new_fire","Reference",0.8,0,0,-15,35,0,0,0,1,1)
	Fx(1.155,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	
STake_Start("T1_Polearm_skill_004_level2_fire","")  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	Pause(1.892,0.25,1,0.012)	
	
    AttackStart()	
	Hurt(0.066,"hurt_21")
    HurtSound(0.066,"blade_Hit")
    DirFx(0.066,"T1_Polearm_skill_001_hit",0,1,0.3) 
    HurtBoneFx(0.066,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,15,0)
	
	
STake_Start("T1_Polearm_skill_005_level2","T1_Polearm_skill_002_new")  --六连刺    27012  引导一0.880  引导二1.220  引导三1.580  引导四1.990  引导五2.270  引导六2.545  动作总长3.22
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	Pause(3.485,0.25,1,0.012)
	FrameTime(0.88,4.1)
	Fx(1.763,"WQL_skill_004_fire","T_R",0.6,1,10,40,0,0,0,0,2.5,1)
	Fx(2.132,"WQL_skill_004_fire","T_R",0.6,1,10,40,9,0,0,0,2.5,1)
	Fx(2.46,"WQL_skill_004_fire","T_R",0.6,1,10,40,1,0,0,0,2.5,1)
	Fx(2.829,"WQL_skill_004_fire","T_R",0.6,1,5,40,17,0,0,0,2.5,1)
	Fx(3.116,"WQL_skill_004_fire","T_R",0.6,1,10,40,-5,0,0,0,2.5,1)
	Fx(3.444,"WQL_skill_004_fire","T_R",0.6,1,10,40,0,0,0,0,2.5,1)
	Fx(1.08,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Sound(1.7,"TY_Attack_013")
    Sound(2.1,"TY_Attack_013")
    Sound(2.4,"TY_Attack_013")
    Sound(2.8,"TY_Attack_013")
    Sound(3.1,"TY_Attack_013")
    Sound(3.4,"TY_Attack_013")

    AttackStart()	
	Hurt(0.880,"hurt_21")
	Hurt(1.220,"hurt_21")	
	Hurt(1.580,"hurt_21")	
	Hurt(1.990,"hurt_21")	
	Hurt(2.270,"hurt_21")	
	Hurt(2.620,"hurt_21")	
    HurtSound(0.880,"blade_Hit")	
    HurtSound(1.220,"blade_Hit")	
    HurtSound(1.580,"blade_Hit")	
    HurtSound(1.990,"blade_Hit")	
    HurtSound(2.270,"blade_Hit")
	HurtSound(2.620,"blade_Hit")
    DirFx(0.880,"T1_Polearm_skill_001_hit",0,1,0.2)  
	DirFx(1.220,"T1_Polearm_skill_001_hit",0,1,0.2)  
	DirFx(1.580,"T1_Polearm_skill_001_hit",0,1,0.25)  
	DirFx(1.990,"T1_Polearm_skill_001_hit",0,1,0.25)  
	DirFx(2.270,"T1_Polearm_skill_001_hit",0,1,0.3)  
	DirFx(2.620,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.880,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,20,0)
    HurtBoneFx(1.220,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.580,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.990,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,-10,0)
    HurtBoneFx(2.270,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,15,0)
    HurtBoneFx(2.620,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,0,0)
	
	
STake_Start("T1_Polearm_skill_004_2_start_level2","T1_Polearm_skill_001_new")  --技能  29709  前刺  吟唱0.8  动作总长1.64
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(1.026,2.666)
    --PlaySpeed(1.892,1.895,0.012)
	Pause(1.892,0.25,1,0.012)
	
	Sound(0.32,"T1_Blade_move_1")
    Sound(1.5,"MQX_Skill_000_1")
	Sound(1.4,"QQJ_attack_001")
    Fx(1.026,"T1_Polearm_skill_001","Reference",0.5,0,0,-15,35,0,0,0,2.2,2)
	Fx(1.226,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	
STake_Start("T1_Polearm_skill_004_2_fire_level2","")  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Pause(1.892,0.25,1,0.012)
	
    AttackStart()	
	Hurt(0.066,"hurt_21")
    HurtSound(0.066,"blade_Hit")
    DirFx(0.066,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.066,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,15,0)
	
	
STake_Start("T1_Polearm_skill_005_1_level2","T1_Polearm_skill_005")  --技能  29710  虎炎  吟唱0.8charge0.2  动作总长1.7
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	FrameTime(0.8,2.5)
    Fx(1.6,"T1_Polearm_skill_005_fire","Reference",1,0,0,0,0,0,0,0,1,2)
	Fx(1,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.32,"T1_Blade_move_1")
    Sound(1.5,"MQX_Skill_000_1")
	Sound(1.4,"QQJ_Skill000")

    AttackStart()	
	Hurt(0.9,"hurt_61")
    HurtSound(0.9,"blade_Hit")
    DirFx(0.9,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.9,"FX_blood_changqiang_b","Spine1",1,0,0,0,-80,0,0)
	  	

STake_Start("T1_Polearm_skill_006_start_level2","T1_Polearm_skill_004")  --技能  29711  横扫  吟唱0.8  动作总长2.356
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	FrameTime(0.81,3.166)
    --PlaySpeed(1.670,1.672,0.012)	
	Pause(1.670,0.16,1,0.012)
    Fx(0.81,"T1_Polearm_skill_004","Reference",1,0,0,0,0,0,0,0,2,2)
	Fx(1.01,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.32,"WH_attack_001")
    Sound(1.4,"MQX_Skill_000_1")
	Sound(1.3,"QQJ_attack_001")
	
STake_Start("T1_Polearm_skill_006_fire_level2","")  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
	Pause(1.670,0.16,1,0.012)	
	
    AttackStart()	
	Hurt(0.06,"hurt_21")
    HurtSound(0.06,"blade_Hit")
    DirFx(0.06,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.06,"FX_blood_changqiang_b","Spine1",1,0,0,0,-80,0,0)

---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	
	


STake_Start("T1_Polearm_skill_004_level3_start","T1_Polearm_skill_001_new")  --技能 27010  近身刺    吟唱0.564    动作总长1.406
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	Pause(1.892,0.25,1,0.012)
	FrameTime(1.26,2.666)
    Sound(1.8,"T1_Polearm_skill_000")
    Fx(1.839,"T1_Polearm_skill_001_new_fire","Reference",0.8,0,0,-15,35,0,0,0,1,1)
	Fx(1.474,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	
STake_Start("T1_Polearm_skill_004_level3_fire","")  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	Pause(1.892,0.25,1,0.012)
	
    AttackStart()	
	Hurt(0.066,"hurt_21")
    HurtSound(0.066,"blade_Hit")
    DirFx(0.066,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.066,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,15,0)
	
	
STake_Start("T1_Polearm_skill_005_level3","T1_Polearm_skill_002_new")  --六连刺    27012  引导一0.586  引导二0.926  引导三1.286  引导四1.696  引导五1.976  引导六2.251  动作总长2.926
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	Pause(3.485,0.25,1,0.012)
	FrameTime(1.174,4.1)
	Fx(1.763,"WQL_skill_004_fire","T_R",0.6,1,10,40,0,0,0,0,2.5,1)
	Fx(2.132,"WQL_skill_004_fire","T_R",0.6,1,10,40,9,0,0,0,2.5,1)
	Fx(2.46,"WQL_skill_004_fire","T_R",0.6,1,10,40,1,0,0,0,2.5,1)
	Fx(2.829,"WQL_skill_004_fire","T_R",0.6,1,5,40,17,0,0,0,2.5,1)
	Fx(3.116,"WQL_skill_004_fire","T_R",0.6,1,10,40,-5,0,0,0,2.5,1)
	Fx(3.444,"WQL_skill_004_fire","T_R",0.6,1,10,40,0,0,0,0,2.5,1)
	Fx(1.374,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Sound(1.7,"TY_Attack_013")
    Sound(2.1,"TY_Attack_013")
    Sound(2.4,"TY_Attack_013")
    Sound(2.8,"TY_Attack_013")
    Sound(3.1,"TY_Attack_013")
    Sound(3.4,"TY_Attack_013")

    AttackStart()	
	Hurt(0.586,"hurt_21")
	Hurt(0.926,"hurt_21")	
	Hurt(1.286,"hurt_21")	
	Hurt(1.696,"hurt_21")	
	Hurt(1.976,"hurt_21")	
	Hurt(2.326,"hurt_21")	
    HurtSound(0.586,"blade_Hit")	
    HurtSound(0.926,"blade_Hit")	
    HurtSound(1.286,"blade_Hit")	
    HurtSound(1.696,"blade_Hit")	
    HurtSound(1.976,"blade_Hit")
	HurtSound(2.326,"blade_Hit")
    DirFx(0.586,"T1_Polearm_skill_001_hit",0,1,0.2)  
	DirFx(0.926,"T1_Polearm_skill_001_hit",0,1,0.2)  
	DirFx(1.286,"T1_Polearm_skill_001_hit",0,1,0.25)  
	DirFx(1.696,"T1_Polearm_skill_001_hit",0,1,0.25)  
	DirFx(1.976,"T1_Polearm_skill_001_hit",0,1,0.3)  
	DirFx(2.326,"T1_Polearm_skill_001_hit",0,1,0.3)
    HurtBoneFx(0.586,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,20,0)
    HurtBoneFx(0.926,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.286,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.696,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,-10,0)
    HurtBoneFx(1.976,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,15,0)
    HurtBoneFx(2.326,"FX_blood_changqiang_b","Spine1",1,0,0,0,0,0,0)
	
	

	
	
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
STake_Start("Common_skill_jumpB_80","T1_Polearm_skill_jumpB")  --技能 40009 近战通用后跳 引导0.01charge0.4
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,0)
	Fx(0.4,"FX_common_skill_jumpB_02","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.08,"char_05_cloth_flap_02")	
	Sound(0.38,"SZ03_dodge_000")	
	

STake_Start("Common_skill_jumpF","T1_Polearm_skill_jumpF")  --技能 40001 通用追击  引导一0.01charge0.4  引导二0.46  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
    Fx(0.41,"FX_common_skill_jumpF","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.12,"QQJ_attack_001")
	
  	AttackStart()
  	Hurt(0.46,"hurt_21")    
    DirFx(0.46,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.46,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.46,"HMW01_Hit_002")
    RangeCameraShake(0.46,"ShakeDistance = 80","ShakeMode = 3","ShakeTimes = 0.14","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")



------------------------------------------------------------------------------------
--小源村CUTSCENE单晓箫配



STake_Start("CS_wait1_T1_Polearm","T1_MHMGB_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_wait1_MoHuaJinGuoQiangBing01 ","T1_Polearm_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_walk1_MoHuaJinGuoQiangBing01 ","T1_Polearm_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_run1_MoHuaJinGuoQiangBing01 ","T1_Polearm_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_DaoXia1_T1_Polearm","T1_Polearm_dead_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.498,1.499)

STake_Start("CS_DaoXia1_T1_JGQC","N_T1_JGQC_dead_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.531,1.532)

STake_Start("CS_attack1_MoHuaJinGuoQiangBing01","T1_Polearm_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_roar1_MoHuaJinGuoQiangBing01","T1_Polearm_skill_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_roar2_MoHuaJinGuoQiangBing01","T1_Polearm_skill_002")
    BlendMode(0)
    BlendTime(0.2)
    Loop()




-----------------------------------------------------------------------------------

--牛家村CUTSCENE用动作

--CS4金兵来袭

--张阳配

STake_Start("CS_attack_000_T1_Polearm","T1_Polearm_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_attack_002_T1_Polearm","T1_Polearm_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_attack_002_a_T1_Polearm","T1_Polearm_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.18) 
    PlaySpeed(0,0.18,0.5)
    Loop(0.1799,0.18)

STake_Start("CS_attack_002_b_T1_Polearm","T1_Polearm_attack_002")
    BlendMode(0)
    BlendTime(0)
    FrameTime(0.18,1) 
    PlaySpeed(0.18,0.24,0.11)
    Loop(0.9999,1)

STake_Start("CS_skill_004_T1_Polearm","T1_Polearm_skill_004")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,2.8177) 
    PlaySpeed(1.83,1.93,0.15)
    Loop(2.8176,2.8177)

STake_Start("CS_walk2_T1_Polearm ","T1_Polearm_move_003")
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,1) 
    PlaySpeed(0,1,0.8)
    Loop()

-----------------------------------------------------------------------------------

--牛家村CUTSCENE用动作

--CS8尾声

--张阳配

STake_Start("CS_NJC_def_000_T1_Polearm","T1_Polearm_def_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_dead_003_T1_Polearm","T1_Polearm_dead_003")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,1.33) 
    Loop(1.3299,1.33)

STake_Start("CS_NJC_dead_000_T1_Polearm","T1_Polearm_dead_000")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,1.485)
    Loop(1.4849,1.485)

STake_Start("CS_NJC_fennu_01_T1_Polearm","T1_Polearm_B_emo_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_fennu_02_T1_Polearm","T1_Polearm_skill_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_stand_wait_02_T1_Polearm","T1_Polearm_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------------------------------------
--CutScene用
-----------------------------------------------------------------------------------
--CUTSCENE基本动作

STake_Start("CS_T1_Polearm_attack_000","T1_Polearm_attack_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_attack_001","T1_Polearm_attack_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_attack_002","T1_Polearm_attack_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_B_wait_000","T1_Polearm_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_Polearm_dead","T1_Polearm_dead")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_dead_000","T1_Polearm_dead_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_dead_001","T1_Polearm_dead_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_dead_002","T1_Polearm_dead_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_dead_003","T1_Polearm_dead_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_def_000","T1_Polearm_def_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_dodge_000","T1_Polearm_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_emo_001","T1_Polearm_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_emo_002","T1_Polearm_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_link_000","T1_Polearm_link_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_link_001","T1_Polearm_link_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_move_000","T1_Polearm_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_Polearm_move_003","T1_Polearm_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_Polearm_move_004","T1_Polearm_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_Polearm_wait_000","T1_Polearm_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_T1_Polearm_skill_000","T1_Polearm_skill_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_skill_001","T1_Polearm_skill_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("CS_T1_Polearm_skill_002","T1_Polearm_skill_002")
    BlendMode(0)
    BlendTime(0.2)



-----------------------------------------------------------------------------------
--牛家村CUTSCENE用动作

--CS6玩家变身

--单晓箫配

STake_Start("CS_Zou_slow_T1_Polearm","T1_Polearm_CS_bs_move000")  
    BlendMode(0)
    BlendTime(0.2)
    PlaySpeed(0,1.32,0.6)
    Loop()

STake_Start("CS_Ti_T1_Polearm","T1_Polearm_CS_bs_emo1")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1,2.9)   
    PlaySpeed(2.6,2.7,0.8)
    PlaySpeed(2.7,2.8,0.4)
    PlaySpeed(2.8,2.9,0.2)
    Loop(2.89999,2.9)

STake_Start("CS_Ci_T1_Polearm","T1_Polearm_CS_bs_emo1")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.8,7.8)   
    Loop()

STake_Start("CS_look_right_T1_Polearm","T1_Polearm_CS_bs_emo1")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(4,7.8) 
    Loop()

STake_Start("CS_TiaoXin1_T1_Polearm","T1_Polearm_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_TiaoXin2_T1_Polearm","T1_Polearm_B_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_TiaoXin3_T1_Polearm","T1_Polearm_CS_bs_laugh")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.325,2.5) 
    Loop()

STake_Start("CS_attack2_T1_Polearm","T1_Polearm_skill_000")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NaoTou_T1_Polearm","T1_Polearm_emo_002")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()


--哑仆受击

--单晓箫配
	
STake_Start("CS_YP_wait_000_T1_Polearm","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 	
	
STake_Start("CS_YP_wait2_T1_Polearm","YP_hit_033")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()			   
 				 
STake_Start("CS_YP_dead_T1_Polearm","YP_dead")
    BlendMode(0)
    BlendTime(0.2)   
    Loop(1.69999,1.7) 
 				 
STake_Start("CS_YP_dead_S_T1_Polearm","YP_dead")
    BlendMode(0)
    BlendTime(0.2)   
    FrameTime(1.69999,1.7) 
    Loop()
		
STake_Start("CS_YP_dead_000_T1_Polearm","YP_dead_000")
    BlendMode(0)
    BlendTime(0.2)  	
    Loop(1.29999,1.3) 		
		
STake_Start("CS_YP_dead_000_S_T1_Polearm","YP_dead_000")
    BlendMode(0)
    BlendTime(0.2)  	
    FrameTime(1.29999,1.3) 	
    Loop()	
		
STake_Start("CS_YP_dead_001_T1_Polearm","YP_dead")
    BlendMode(0)
    BlendTime(0.2)  
    Loop(1.29999,1.3) 		
		
STake_Start("CS_YP_dead_001_S_T1_Polearm","YP_dead")
    BlendMode(0)
    BlendTime(0.2)  
    FrameTime(1.29999,1.3) 	
    Loop()		
		
STake_Start("CS_YP_dead_002_T1_Polearm","YP_dead_002")
    BlendMode(0)
    BlendTime(0.2)  	
    Loop(1.29999,1.3) 		
		
STake_Start("CS_YP_dead_002_S_T1_Polearm","YP_dead_002")
    BlendMode(0)
    BlendTime(0.2)  	
    FrameTime(1.29999,1.3) 	
    Loop()		
		
STake_Start("CS_YP_dead_003_T1_Polearm","YP_dead_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.29999,1.3) 		
		
STake_Start("CS_YP_dead_S_003_T1_Polearm","YP_dead_003")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.29999,1.3) 
    Loop()
	
STake_Start("CS_YP_hit_000_T1_Polearm","YP_hit_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_001_T1_Polearm","YP_hit_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()  

STake_Start("CS_YP_hit_002_T1_Polearm","YP_hit_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_003_T1_Polearm","YP_hit_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_006_T1_Polearm","YP_hit_006")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_007_T1_Polearm","YP_hit_007")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_008_T1_Polearm","YP_hit_008")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_009_T1_Polearm","YP_hit_009")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
		
STake_Start("CS_YP_hit_011_T1_Polearm","YP_hit_011")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 	
		
STake_Start("CS_YP_hit_012_T1_Polearm","YP_hit_012")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_013_T1_Polearm","YP_hit_013")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_014_T1_Polearm","YP_hit_014")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 

STake_Start("CS_YP_hit_016_T1_Polearm","YP_hit_016")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.49999,0.5) 

STake_Start("CS_YP_hit_017_T1_Polearm","YP_hit_017")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
		
STake_Start("CS_YP_hit_018_T1_Polearm","YP_hit_018")
    BlendMode(0)
    BlendTime(0.2) 	
    Loop() 	
	
STake_Start("CS_YP_hit_019_T1_Polearm","YP_hit_019")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_021_T1_Polearm","YP_hit_021")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.69999,0.7) 
	
STake_Start("CS_YP_hit_022_T1_Polearm","YP_hit_022")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.64999,0.65) 
	
STake_Start("CS_YP_hit_023_T1_Polearm","YP_hit_023")
    BlendMode(0)
    BlendTime(0.02) 
    Loop(0.59999,0.6) 
	
STake_Start("CS_YP_hit_024_T1_Polearm","YP_hit_024")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.69999,0.7) 
	
STake_Start("CS_YP_hit_026_T1_Polearm","YP_hit_026")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_027_T1_Polearm","YP_hit_027")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_028_T1_Polearm","YP_hit_028")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_030_T1_Polearm","YP_hit_030")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_031_T1_Polearm","YP_hit_031")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
	
STake_Start("CS_YP_hit_032_T1_Polearm","YP_hit_032")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()  

--CS7黄药师帅

--单晓箫配

STake_Start("CS_dead_T1_Polearm","T1_Polearm_dead_000")  
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.39999,1.4)

STake_Start("CS_dead_S_T1_Polearm","T1_Polearm_dead_000")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.39999,1.4) 
    Loop()

STake_Start("CS_dead1_T1_Polearm","T1_Polearm_dead_001")  
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.59999,1.6)

STake_Start("CS_dead1_S_T1_Polearm","T1_Polearm_dead_001")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.59999,1.6) 
    Loop()

STake_Start("CS_dead2_T1_Polearm","T1_Polearm_dead_002")  
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.29999,1.3)

STake_Start("CS_dead2_S_T1_Polearm","T1_Polearm_dead_002")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.29999,1.3) 
    Loop()

STake_Start("CS_dead3_T1_Polearm","T1_Polearm_dead_003")  
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.29999,1.3)

STake_Start("CS_dead3_S_T1_Polearm","T1_Polearm_dead_003")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.29999,1.3) 
    Loop()

STake_Start("CS_dead4_T1_Polearm","T1_Polearm_dead")  
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.89999,1.9)

STake_Start("CS_dead4_S_T1_Polearm","T1_Polearm_dead")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.89999,1.9) 
    Loop()

STake_Start("CS_look_around1_T1_Polearm","T1_Saw_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_down1_T1_Polearm","T1_SJMJS_B_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait1_ShieldSpear_T1_Polearm","T1_ShieldSpear_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ShuangQiang_ShieldSpear_T1_Polearm","T1_ShieldSpear_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_walk_slow_T1_Polearm","T1_Polearm_move_003")
    BlendMode(0)
    BlendTime(0.1) 
    Loop()
    PlaySpeed(0,1,0.5)

STake_Start("CS_TiaoJi_T1_Polearm","T1_Polearm_skill_001_new")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_hit1_T1_Polearm","YP_hit_001")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0.09,0.6)   
    Loop()

STake_Start("CS_DiZihit_T1_Polearm","T1_Polearm_CS_hit04")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.3659,1.366)

STake_Start("CS_end_attack_T1_Polearm","T1_ShieldSpear_attack_001")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.5)   
    Loop(0.4999,0.5)






package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")

 
STake_Start("T1_Saw_def_000","T1_Saw_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.025,"NB_JB_def_000")

    
STake_Start("T1_Saw_dodge_000","T1_Saw_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.046,"NB_JB_dedge_000")

STake_Start("T1_Saw_B_wait_000","T1_Saw_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","T1_Saw_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Saw_dead","T1_Saw_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(1.246,"TongYong_DaoDi_001")
    Sound(0.513,"Martialswod_dead_VOC")
    Sound(0.824,"DiaoLuo_Ren")
    Sound(0.861,"L1_LDPClaw_dead_000")

STake_Start("T1_Saw_dead_000","T1_Saw_dead_000")
    BlendMode(0)
    BlendTime(0.2)

   
    Sound(0.546,"TongYong_DaoDi_001")
    Sound(0.308,"Martialswod_dead_VOC")
    Sound(0.728,"DiaoLuo_Ren")
 

STake_Start("T1_Saw_dead_001","T1_Saw_dead_001")
    BlendMode(0)
    BlendTime(0.2)

   
    Sound(0.850,"TongYong_DaoDi_001")
    Sound(0.293,"Martialswod_dead_VOC")
    Sound(0.175,"DiaoLuo_Ren")
    Sound(0.439,"L1_LDPClaw_dead_000")


STake_Start("T1_Saw_dead_002","T1_Saw_dead_002")
    BlendMode(0)
    BlendTime(0.2)

     
    Sound(0.462,"TongYong_DaoDi_001")
    Sound(0.196,"Martialswod_dead_VOC")
    Sound(0.728,"DiaoLuo_Ren")

STake_Start("T1_Saw_dead_003","T1_Saw_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.462,"TongYong_DaoDi_001")
    Sound(0.196,"Martialswod_dead_VOC")
    Sound(0.728,"DiaoLuo_Ren")



STake_Start("T1_Saw_emo_001","T1_Saw_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Saw_emo_002","T1_Saw_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_Saw_move_000","T1_Saw_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Saw_B_move_003","T1_Saw_B_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Saw_B_move_004","T1_Saw_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Saw_wait_000","T1_Saw_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_Saw_bout_001","T1_Saw_bout_001")
    BlendMode(0)
    BlendTime(0.2)





























package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")
require("T1_life")
require("N1_life")


STake_Start("Monster_def_000","T1_SBlade_B_def_000")
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.03,"Blade_def")  

STake_Start("Monster_dodge_000","T1_SBlade_B_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_B_wait_000","T1_SBlade_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_emo_001","T1_SBlade_B_emo_001")  --战斗嘲讽
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_dead","T1_SBlade_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.16,"T1_SBlade_Dead_VOL_Long_1")  
    Sound(0.81,"T1_SBlade_Dead_VOL_Long_2")  
    Sound(1.01,"TongYong_DaoDi_004")  



STake_Start("Monster_dead_000","T1_SBlade_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.1,"T1_SBlade_Dead")  
    Sound(0.48,"TongYong_DaoDi_002")  


STake_Start("Monster_dead_001","T1_SBlade_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.1,"T1_SBlade_Dead")  
    Sound(0.1,"TongYong_DaoDi_004") 


STake_Start("Monster_dead_002","T1_SBlade_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.09,"T1_SBlade_Dead")  
    Sound(0.44,"TongYong_DaoDi_002") 

STake_Start("Monster_dead_003","T1_SBlade_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.09,"T1_SBlade_Dead")  
    Sound(0.44,"TongYong_DaoDi_002")

STake_Start("Monster_emo_000","T1_SBlade_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_emo_001","T1_SBlade_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("Monster_link_000","T1_SBlade_link_000")
    BlendMode(0)
    BlendTime(0.2)

    GroundSound(0.29,"Sound_step")
    GroundSound(0.69,"Sound_step")
STake_Start("Monster_link_001","T1_SBlade_link_001")
    BlendMode(0)
    BlendTime(0.2)

    GroundSound(0.55,"Sound_step")  
    GroundSound(0.85,"Sound_step")

STake_Start("Monster_move_000","T1_SBlade_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

    GroundSound(0.13,"Sound_step")  
    GroundSound(0.71,"Sound_step")
STake_Start("Monster_B_move_003","T1_SBlade_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

    GroundSound(0.13,"Sound_step")  
    GroundSound(0.71,"Sound_step")
STake_Start("Monster_B_move_004","T1_SBlade_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    GroundSound(0.08,"Sound_step")  
    GroundSound(0.46,"Sound_step")

STake_Start("Monster_wait_000","T1_SBlade_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_bout_000","T1_bout_001")   --出生
    BlendMode(0)
    BlendTime(0.2) 




STake_Start("T1_SBlade_skill_000","T1_SBlade_skill_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	Fx(0.2,"FX_Warning","Head")	
    Sound(0.24,"T1_SBlade_Attack_VOL_1")  
    Sound(0.26,"Blade_Attack")  
    Sound(0.58,"Sound_step_Jump_001")  
    Sound(0.81,"Blade_Attack")  
    Sound(0.89,"T1_SBlade_Attack_VOL_2") 

    AttackStart()
  	Hurt(0.426,"hurt_11")
  	Hurt(0.955,"hurt_11")
    --DirFx(0.426,"",0,1)  
    --DirFx(0.955,"",0,1)
    GroundSound(0.08,"Sound_step")  
    GroundSound(0.44,"Sound_step")  
    GroundSound(0.48,"Sound_step")  
    GroundSound(0.93,"Sound_step")  
    GroundSound(0.95,"Sound_step")  
    GroundSound(1.76,"Sound_step")  
    GroundSound(1.95,"Sound_step")  


STake_Start("Monster_jump_start","N1_move_005-007") --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("Monster_jump_loop","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_down","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("Monster_jump_start_B","N1_move_005-007") --后起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("Monster_jump_loop_B","N1_move_006")  --后空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_down_B","N1_move_005-007")  --后落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

	
STake_Start("Monster_jump_start_L","T1_SBlade_jump_start_L") --左跳起跳
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("Monster_jump_loop_L","T1_SBlade_jump_loop_L")  --左跳空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_down_L","T1_SBlade_jump_down_L")  --左跳落地
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("Monster_jump_start_R","T1_SBlade_jump_start_R") --右跳起跳
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("Monster_jump_loop_R","T1_SBlade_jump_loop_R")  --右跳空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_jump_down_R","T1_SBlade_jump_down_R")  --右跳落地
    BlendMode(0)
    BlendTime(0.2)


STake_Start("Monster_move_000_L","T1_SBlade_move_003_L")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_move_000_R","T1_SBlade_move_003_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_move_000_B","T1_SBlade_move_003_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()	
	
STake_Start("Monster_B_move_003_L","T1_SBlade_move_003_L")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_move_003_R","T1_SBlade_move_003_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("Monster_B_move_003_B","T1_SBlade_move_003_R")
    BlendMode(0)
    BlendTime(0.2)
    Loop()	
	
STake_Start("Monster_B_jump_004_L","T1_SBlade_PY_move_004_L")
    BlendMode(0)
    BlendTime(0.05)

    Charge(0,0.38,90,90,4)

STake_Start("Monster_B_jump_004_R","T1_SBlade_PY_move_004_R")
    BlendMode(0)
    BlendTime(0.05)

    Charge(0,0.38,90,270,4)

STake_Start("Monster_B_jump_004_B","T1_SBlade_PY_move_004_B")
    BlendMode(0)
    BlendTime(0.05)

    Charge(0,0.4,90,180,4)




STake_Start("Monster_B_jump_004_L_short","T1_SBlade_PY_move_004_L")
    BlendMode(0)
    BlendTime(0.05)

    Charge(0,0.38,40,90,4)

STake_Start("Monster_B_jump_004_R_short","T1_SBlade_PY_move_004_R")
    BlendMode(0)
    BlendTime(0.05)

    Charge(0,0.38,40,270,4)

STake_Start("Monster_B_jump_004_B_short","T1_SBlade_PY_move_004_B")
    BlendMode(0)
    BlendTime(0.05)

    Charge(0,0.4,35,180,4)

	


STake_Start("T1_SBlade_attack_003_start","T1_SBlade_attack_003")        --26022/27002  吟唱1.6     动作总长3.566
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3.566)
	--PlaySpeed(1.732,1.734,0.012)
    Pause(1.732,0.16,1,0.012)	 
    Fx(1.74,"FX_M_att_locus_RQ","Reference",1,1,0,0,20,0,0)
	Fx(0.2,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_1")
    Sound(1.62,"Blade_Attack")
    Sound(1.64,"T1_SBlade_Attack_VOL_2") 

STake_Start("T1_SBlade_attack_003_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    Pause(1.732,0.17,1,0.012)	
  	
    AttackStart()
  	Hurt(0.132,"hurt_61")
    DirFx(0.132,"FX_M_skill_hit_RQ",0,1,1,90,0) 
    HurtSound(0.132,"Big_blade_Hit_002_1")
	HurtBoneFx(0.132,"FX_blood_dao_b","Spine",1,0,0,0,-80,0,0)


STake_Start("T1_SBlade_attack_003_slow_start","T1_SBlade_attack_003")        --27003  吟唱1.6     动作总长3.566
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3.566)
	PlaySpeed(1.732,1.738,0.012)
    Pause(1.732,0.16,1,0.012)	 
    Fx(1.734,"FX_M_att_locus_RQ","Reference",1,1,0,0,20,0,0)
	Fx(0.2,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_1")
    Sound(1.62,"Blade_Attack")
    Sound(1.64,"T1_SBlade_Attack_VOL_2") 

STake_Start("T1_SBlade_attack_003_slow_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    Pause(1.732,0.17,1,0.012)	
  	
    AttackStart()
  	Hurt(0.132,"hurt_21")
    DirFx(0.132,"FX_M_skill_hit_RQ",0,1,1,90,0) 
    HurtSound(0.132,"Big_blade_Hit_002_1")
	HurtBoneFx(0.132,"FX_blood_dao_b","Spine",1,0,0,0,-90,0,0)




STake_Start("T1_SBlade_skill_001","T1_SBlade_skill_001")  --跳斩平挥   26027  引导1.600  动作总长3.333
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3.333)
    --Fx(1.5,"FX_Warning","Head",1,1,0,0,0)
    Fx(1.8,"FX_M_att_locus_RQ","Reference",1,1,0,0,20,180,180)
	Fx(0.2,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_xuli")
    Sound(1.73,"Blade_Attack")
    Sound(1.75,"T1_SBlade_Attack_VOL_1")
	
    AttackStart()
  	Hurt(1.845,"hurt_61")
    DirFx(1.845,"FX_M_skill_hit_RQ",20,1,1.5,90,0) 
    HurtSound(1.845,"Big_blade_Hit") 
	HurtBoneFx(1.845,"FX_blood_dao_b","Spine",1,0,0,0,80,0,0)

 

STake_Start("T1_SBlade_skill_002","T1_SBlade_skill_002")  --跳斩竖砍  27004  引导一1.140charge0.45  引导二1.600  动作总长3
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
	FrameTime(0,3)
    Pause(1.66,0.16,1,0.012)
	Fx(1.6,"FX_M_att_locus_RQ","Reference",1,1,0,0,0,-180,135,90,1.5,1)
	Fx(0.2,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_xuli")
    Sound(1.55,"Blade_Attack")
    Sound(1.6,"T1_SBlade_Attack_VOL_1")
	
    AttackStart()
  	Hurt(1.66,"hurt_61")
    DirFx(1.66,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37) 
    HurtSound(1.66,"Big_blade_Hit")
	HurtBoneFx(1.66,"FX_blood_dao_b","Spine",1,0,0,0,0,-70,90)
    RangeCameraShake(1.68,"ShakeDistance = 150","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

	
STake_Start("T1_SBlade_skill_003","T1_SBlade_skill_003")  --蓄力刀气  26037  引导1.77  动作总长3
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(0,"T1_SBlade_skill_001_A_start","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(1.77,"T1_SBlade_skill_001_A_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    Sound(0.2,"T1_SBlade_Attack_VOL_xuli")
    Sound(1.55,"Blade_Attack")
    Sound(1.6,"T1_SBlade_Attack_VOL_1")
	
    AttackStart()
  	Hurt(1.77,"hurt_21")
    DirFx(1.77,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37) 
    HurtSound(1.77,"Big_blade_Hit")
	HurtBoneFx(1.77,"FX_blood_dao_b","Spine",1,0,0,0,0,-70,90)
	

STake_Start("T1_SBlade_skill_004","T1_SBlade_skill_004")  --旋转刀锋  26039  引导1.6  动作总长3
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,3)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(1.6,"T1_XLZZB_skill_002_fire","Reference",0.65,0,0,0,0,0,0,0,2,1)
	Sound(0.1,"TY_Attack_017")
    Sound(1.55,"Blade_Attack")
    Sound(1.68,"TY_Attack_016")
    Sound(1.5,"T1_SBlade_Attack_VOL_1")
	
    AttackStart()
  	Hurt(1.6,"hurt_61")
    DirFx(1.6,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37) 
    HurtSound(1.6,"Big_blade_Hit")
	HurtBoneFx(1.6,"FX_blood_dao_b","Spine",1,0,0,0,-90,0,0)
	

---------------------------------------------------吟唱时间减1/2红眼版本-----------------------------------------------------------		
	

STake_Start("T1_SBlade_attack_003_level2_start","T1_SBlade_attack_003")        --26022/27002   吟唱0.8    动作总长2.766
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.732,0.16,1,0.012)
	FrameTime(0.8,3.566)
    Fx(1.74,"FX_M_att_locus_RQ","Reference",1,1,0,0,20,0,0)
	Fx(1,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_1")
    Sound(1.62,"Blade_Attack")
    Sound(1.64,"T1_SBlade_Attack_VOL_2") 

STake_Start("T1_SBlade_attack_003_level2_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.732,0.16,1,0.012)
    
    AttackStart()
  	Hurt(0.132,"hurt_21")
    DirFx(0.132,"FX_M_skill_hit_RQ",0,1,1,90,0) 
    HurtSound(0.132,"Big_blade_Hit_002_1")
	HurtBoneFx(0.132,"FX_blood_dao_b","Spine",1,0,0,0,-80,0,0)


STake_Start("T1_SBlade_skill_001_level2","T1_SBlade_skill_001")  --跳斩平挥   26027  引导一0.8    动作总长2.533
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.8,3.333)
    Fx(1.8,"FX_M_att_locus_RQ","Reference",1,1,0,0,20,180,180)
	Fx(1,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_xuli")
    Sound(1.73,"Blade_Attack")
    Sound(1.75,"T1_SBlade_Attack_VOL_1")
    Charge(0,0.23,120,20,12) 
	
    AttackStart()
  	Hurt(1.045,"hurt_61")
    DirFx(1.045,"FX_M_skill_hit_RQ",20,1,1.5,90,0) 
    HurtSound(1.045,"Big_blade_Hit") 
	HurtBoneFx(1.045,"FX_blood_dao_b","Spine",1,0,0,0,80,0,0)
 
 
 STake_Start("T1_SBlade_skill_002_level2","T1_SBlade_skill_002")  --跳斩竖砍  27004   0.57charge0.45  引导二1.03  动作总长2.43
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.65,0.416,1,0.012)	
	FrameTime(0.57,3)
	Fx(1.6,"FX_M_att_locus_RQ","Reference",1,1,0,0,0,-180,135,90,1.5,1)
	Fx(0.77,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_xuli")
    Sound(1.55,"Blade_Attack")
    Sound(1.6,"T1_SBlade_Attack_VOL_1")
	Charge(1.14,0.48,120,20,12)	
	
    AttackStart()
  	Hurt(1.09,"hurt_61")
    DirFx(1.09,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37) 
    HurtSound(1.09,"Big_blade_Hit")
	HurtBoneFx(1.09,"FX_blood_dao_b","Spine",1,0,0,0,0,-70,90)
    RangeCameraShake(1.11,"ShakeDistance = 150","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

	
STake_Start("T1_SBlade_skill_003_level2","T1_SBlade_skill_003")  --蓄力刀气  26037  引导0.885  动作总长2.115
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.885,3)
	Fx(1.085,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(0.885,"T1_SBlade_skill_001_A_start","Reference",1,0,0,0,0,0,0,0,2,1)
	Fx(1.77,"T1_SBlade_skill_001_A_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    Sound(0.2,"T1_SBlade_Attack_VOL_xuli")
    Sound(1.55,"Blade_Attack")
    Sound(1.6,"T1_SBlade_Attack_VOL_1")
	
    AttackStart()
  	Hurt(0.885,"hurt_21")
    DirFx(0.885,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37) 
    HurtSound(0.885,"Big_blade_Hit")
	HurtBoneFx(0.885,"FX_blood_dao_b","Spine",1,0,0,0,0,-70,90)
	
	
STake_Start("T1_SBlade_skill_004_level2","T1_SBlade_skill_004")  --旋转刀锋  26039  引导0.8  动作总长2.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0.8,3)
	Fx(1.6,"T1_XLZZB_skill_002_fire","Reference",0.65,0,0,0,0,0,0,0,2,1)
	Fx(1,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.2,"TY_Attack_017")
    Sound(1.55,"Blade_Attack")
    Sound(1.75,"TY_Attack_016")
    Sound(1.5,"T1_SBlade_Attack_VOL_1")
	
    AttackStart()
  	Hurt(0.8,"hurt_61")
    DirFx(0.8,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37) 
    HurtSound(0.8,"Big_blade_Hit")
	HurtBoneFx(0.8,"FX_blood_dao_b","Spine",1,0,0,0,-90,0,0)

	
---------------------------------------------------吟唱时间减2/3红眼版本-----------------------------------------------------------	
--吟唱的时间为吟唱动作时间除3，若有余下时间划归在FrameTime以外	




STake_Start("T1_SBlade_attack_003_level3_start","T1_SBlade_attack_003")        --26022/27002   吟唱0.533    动作总长2.499
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.732,0.16,1,0.012)
	FrameTime(1.067,3.566)
    Fx(1.74,"FX_M_att_locus_RQ","Reference",1,1,0,0,20,0,0)
	Fx(1.267,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_1")
    Sound(1.62,"Blade_Attack")
    Sound(1.64,"T1_SBlade_Attack_VOL_2") 

STake_Start("T1_SBlade_attack_003_level3_fire","")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.732,0.16,1,0.012)
    
    AttackStart()
  	Hurt(0.132,"hurt_21")
    DirFx(0.132,"FX_M_skill_hit_RQ",0,1,1,90,0) 
    HurtSound(0.132,"Big_blade_Hit_002_1")
	HurtBoneFx(0.132,"FX_blood_dao_b","Spine",1,0,0,0,-80,0,0)


STake_Start("T1_SBlade_skill_001_level3","T1_SBlade_skill_001")  --跳斩平挥   26027  引导一0.533    动作总长2.266
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.067,3.333)
    Fx(1.8,"FX_M_att_locus_RQ","Reference",1,1,0,0,20,180,180)
	Fx(1.267,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_xuli")
    Sound(1.73,"Blade_Attack")
    Sound(1.75,"T1_SBlade_Attack_VOL_1")
    Charge(0,0.23,120,20,12) 
	
    AttackStart()
  	Hurt(0.778,"hurt_61")
    DirFx(0.778,"FX_M_skill_hit_RQ",20,1,1.5,90,0) 
    HurtSound(0.778,"Big_blade_Hit") 
	HurtBoneFx(0.778,"FX_blood_dao_b","Spine",1,0,0,0,80,0,0)
 
 
 STake_Start("T1_SBlade_skill_002_level3","T1_SBlade_skill_002")  --跳斩竖砍  27004   0.38charge0.45  引导二0.84  动作总长2.24
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    Pause(1.65,0.416,1,0.012)
	FrameTime(0.76,3)
	Fx(1.6,"FX_M_att_locus_RQ","Reference",1,1,0,0,0,-180,135,90,1.5,1)
	Fx(0.96,"FX_Warning","Head")
    Sound(0.2,"T1_SBlade_Attack_VOL_xuli")
    Sound(1.55,"Blade_Attack")
    Sound(1.6,"T1_SBlade_Attack_VOL_1")
	Charge(1.14,0.48,120,20,12)	
	
    AttackStart()
  	Hurt(0.9,"hurt_61")
    DirFx(0.9,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37) 
    HurtSound(0.9,"Big_blade_Hit")
	HurtBoneFx(0.9,"FX_blood_dao_b","Spine",1,0,0,0,0,-70,90)
    RangeCameraShake(0.92,"ShakeDistance = 150","ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")

	
	
STake_Start("T1_SBlade_skill_003_level3","T1_SBlade_skill_003")  --蓄力刀气  26037  引导0.59  动作总长1.82
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.18,3)
	Fx(1.38,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Fx(1.18,"T1_SBlade_skill_001_A_start","Reference",1,0,0,0,0,0,0,0,3,1)
	Fx(1.77,"T1_SBlade_skill_001_A_fire","Reference",1,0,0,0,0,0,0,0,1,1)
    Sound(0.2,"T1_SBlade_Attack_VOL_xuli")
    Sound(1.68,"Blade_Attack")
    Sound(1.7,"T1_SBlade_Attack_VOL_1")
	
    AttackStart()
  	Hurt(0.59,"hurt_21")
    DirFx(0.59,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37) 
    HurtSound(0.59,"Big_blade_Hit")
	HurtBoneFx(0.59,"FX_blood_dao_b","Spine",1,0,0,0,0,-70,90)
	

STake_Start("T1_SBlade_skill_004_level3","T1_SBlade_skill_004")  --旋转刀锋  26039  引导0.533  动作总长3
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(1.066,3)
	Fx(1.6,"T1_XLZZB_skill_002_fire","Reference",0.65,0,0,0,0,0,0,0,2,1)
	Fx(1.266,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
    Sound(1.55,"Blade_Attack")
    Sound(1.68,"TY_Attack_016")
    Sound(1.5,"T1_SBlade_Attack_VOL_1")
	
    AttackStart()
  	Hurt(0.533,"hurt_61")
    DirFx(0.533,"FX_M_skill_hit_RQ",20,1,1.5,0,0,37) 
    HurtSound(0.533,"Big_blade_Hit")
	HurtBoneFx(0.533,"FX_blood_dao_b","Spine",1,0,0,0,-90,0,0)
	

---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
---------------------------------------------------------------------通用技能------------------------------------------------------------------
STake_Start("Common_skill_jumpB_80","T1_SBlade_skill_jumpB")  --技能 40009 近战通用后跳  引导0.01charge0.4
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0,0.899)
	Fx(0.4,"FX_common_skill_jumpB_02","",1,0,0,0,0,0,0,0,1,1)
	Sound(0.03,"L1_SBlade_dodge_000")
	Sound(0.03,"MQX_Skill_000_4")

	
STake_Start("Common_skill_jumpF","T1_SBlade_skill_jumpF")  --技能 40001 通用追击  引导一0.01charge0.4  引导二0.46  动作总长1.2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
	FrameTime(0,1.2)
    Fx(0.1,"Fx_Common_Body_Trail_01","",1,0,0,0,0,0,0,0,1,1)
    Fx(0.41,"FX_common_skill_jumpF","",1,0,0,0,0,0,0,0,1,1)
	Fx(0.2,"FX_Warning","Head",1,0,0,0,0,0,0,0,1,1)
	Sound(0.03,"MQX_Skill_000_1")
	Sound(0.4,"SG_THD_SW_skill_010_Impact")
	
  	AttackStart()
  	Hurt(0.46,"hurt_21")    
    DirFx(0.46,"Bear_skill_000_hit",0,1)
    HurtBoneFx(0.46,"FX_blood_siyao_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.46,"HMW01_Hit_001")
    RangeCameraShake(0.46,"ShakeDistance = 80","ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 3","ShakeAmplitudeZ = 0")
	
----------------------------------------------------------测试统一charge技能-------------------------------------------------

STake_Start("T1_SBlade_skill_000_test","T1_SBlade_skill_000")    --技能  通用charge测试  引导一0.1秒 charge1.5秒  引导二1.6秒伤害  动作总长2.663
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")
    FrameTime(0.2,2.033)	
	PlaySpeed(0.3,0.93,1.26)
	PlaySpeed(0.93,0.97,0.6)
	PlaySpeed(0.97,2.033,1.063)
	Fx(0.3,"FX_Warning","Head")
	Fx(0.97,"T1_XLZZB_skill_003_fire","Reference",1.4,0,0,0,0,0,0,0,1,1)
	
    AttackStart()
  	Hurt(0.5,"hurt_61")
    DirFx(0.5,"FX_M_skill_hit_RQ",20,1,1.5,90,0) 
    HurtSound(0.5,"Big_blade_Hit")	
	HurtBoneFx(0.5,"FX_blood_dao_b","Spine",1,0,0,0,-90,-75,0)

	
	
	package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("YP_hurt")

STake_Start("T1_ShieldSpear_wait_000","T1_ShieldSpear_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_ShieldSpear_B_wait_000","T1_ShieldSpear_B_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("Monster_B_emo_001","T1_ShieldSpear_emo_001")
    BlendMode(0)
    BlendTime(0.2)
    
    
STake_Start("T1_ShieldSpear_dead","T1_ShieldSpear_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.324,"T1_ShieldSpear2_dead_VOI")
    Sound(0.216,"T1_Polearm_dead_000")
    Sound(1.494,"DaoDi_001")
    Sound(0.281,"DiaoLuo_Ren")
    Sound(0.411,"Big_Move_1")


STake_Start("T1_ShieldSpear_dead_000","T1_ShieldSpear_dead_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.165,"T1_ShieldSpear2_dead_VOI")
    Sound(0.495,"T1_Polearm_dead_000")
    Sound(1.094,"DaoDi_001")
    Sound(0.454,"DiaoLuo_Ren")


STake_Start("T1_ShieldSpear_dead_001","T1_ShieldSpear_dead_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.255,"T1_ShieldSpear2_dead_VOI")
    Sound(0.225,"T1_Polearm_dead_000")
    Sound(0.510,"DaoDi_001")
    Sound(0.375,"DiaoLuo_Ren")


STake_Start("T1_ShieldSpear_dead_002","T1_ShieldSpear_dead_002")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.255,"T1_ShieldSpear2_dead_VOI")
    Sound(0.225,"T1_Polearm_dead_000")
    Sound(0.510,"DaoDi_001")
    Sound(0.375,"DiaoLuo_Ren")


 STake_Start("T1_ShieldSpear_dead_003","T1_ShieldSpear_dead_003")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.185,"T1_ShieldSpear2_dead_VOI")
    Sound(0.475,"T1_Polearm_dead_000")
    Sound(1.033,"DaoDi_001")
    Sound(0.475,"DiaoLuo_Ren")


STake_Start("T1_ShieldSpear_def_000","T1_ShieldSpear_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.046,"T1_ShieldSpear2_def_000")

STake_Start("T1_ShieldSpear_dodge_000","T1_ShieldSpear_dodge_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.106,"L1_Unamed_dodge_000")

STake_Start("T1_ShieldSpear_emo_001","T1_ShieldSpear_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_ShieldSpear_emo_002","T1_ShieldSpear_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_ShieldSpear_move_000","T1_ShieldSpear_move_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_ShieldSpear_B_move_003","T1_ShieldSpear_B_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("T1_ShieldSpear_B_move_004","T1_ShieldSpear_B_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()





























-- animation
Skeleton("PC_GB1")
Animation("HG")
Animation("HG_PA")
Animation("HG_life")
Animation("GB_ST")
Animation("GB_PA")
Animation("HIT")
Animation("HG_Arrow")
Animation("RideHorse_HG")
Animation("RideCamel_HG")
Animation("HG_FishingRod")
Animation("N1_life01")
Animation("N1_life02")
Animation("N1_life03")
Animation("THD_FL")
Animation("THD_SW")
Animation("SL_ST")
Animation("SL_SB")
Animation("HG_SL_ST")
Animation("QZ_SW")
Animation("QZ_PA")
Animation("RH_HG")
Animation("QZ_SW_NEW")
Animation("HG_CS02")
Animation("HG_CS_03")
Animation("HG_CS04")
Animation("HG_CS06")
STake("total")

-- paperdoll
PaperDollsTable("Paperdoll")
--Equip("PC_GB1_03A1_leg","",5)
--Equip("PC_GB1_03A1_body","PCM_008",3)
--Equip("PC_GB1_03A1_Glove","",4)
--Equip("PC_GB1_03A1_Helmet","",2)
--Equip("W_Ssword_03_A1","",1)




Dimension(19,43)
BoundingBox(40,40,43,-40,-40,0,0,0,0)



-- blend pattern
BlendPattern("PC_male")

--FoldingBone("RightFoot",3.0,0,0)
--FoldingBone("LeftFoot",3.0,0,0)package.path = package.path .. ";..\\char\\?.lua"
require("common")
require("HG")
require("HG_PA")
require("HG_life")
require("SG")
require("SG_PA")
require("SG_life")
require("GB_PA")
require("GB_ST")
require("nvGB_PA")
require("nvGB_ST")
require("THD_FL")
require("THD_SW")
require("nvTHD_FL")
require("nvTHD_SW")
require("qmdj")
require("QZ_SW")
require("QZ_PA")
require("nvQZ_PA")
require("nvQZ_SW")
require("BTS_FAN")
require("BTS_CR")
require("nvBTS_FAN")
require("nvBTS_CR")
require("SL_ST")
require("SL_SB")
require("DL_DA")
require("DL_BR")
require("HG_Arrow")
require("SG_Arrow")
require("RideHorse")
require("RideCamel")
require("HG_CS")
require("SG_CS")
require("HG_FishingRod")
require("SG_FishingRod")
require("nvSLS_ST")
-- animation
Skeleton("Accessory_Back_116_A1_test")
Animation("Accessory_Back_116_A1_anim")


STake("Accessory_Back_116_A1_anim")


-- paperdoll
Equip("Accessory_Back_116_A1_test","",3)




Dimension(12, 12)



STake_Start("Monster_wait_000","Accessory_Back_116_A1_anim")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("Monster_emo_001","Accessory_Back_116_A1_anim01")  
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("Monster_emo_002","Accessory_Back_116_A1_anim02")  
    BlendMode(0)
    BlendTime(0.2) 
Skeleton("Accessory_Waist_40_A1_skin")
Animation("Accessory_Waist_40_A1_skin")

STake("Accessory_Waist_40_A1_skin")

Equip("Accessory_Waist_40_A1_skin","",3)


Dimension(500, 150)
BoundingBox(500,500,200,-500,-500,0,0,0,0)STake_Start("Accessory_Waist_40_A1_GJ_emo_022","Accessory_Waist_40_A1_GJ_emo_022")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(5.33,5.3301)

STake_Start("Accessory_Waist_40_A1_GJ_emo_022_start","Accessory_Waist_40_A1_GJ_emo_022")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.0001)   
    Loop()

STake_Start("Accessory_Waist_40_A1_GJ_emo_023","Accessory_Waist_40_A1_GJ_emo_023")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


-- animation
Skeleton("CA_char_fish_A")
Animation("CA_char_fish_A")


STake("CA_char_fish_A")


-- paperdoll
Equip("CA_char_fish_A","",3)




Dimension(400, 20)
BoundingBox(20,20,10,-20,-40,0,0,0,0)

STake_Start("CA_char_fish_A_loop01","CA_char_fish_A_loop01")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CA_char_fish_A_loop02","CA_char_fish_A_loop02")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CA_char_fish_A_loop03","CA_char_fish_A_loop03")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()-- animation
Skeleton("CA_char_fish_ALL")
Animation("CA_char_fish_ALL")


STake("CA_char_fish_ALL")


-- paperdoll
Equip("CA_char_fish_ALL","",3)




Dimension(300, 20)
BoundingBox(150,150,10,-150,-150,0,0,0,0)

STake_Start("CA_char_fish_ALL_loop01","CA_char_fish_ALL_loop01")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CA_char_fish_ALL_loop02","CA_char_fish_ALL_loop02")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CA_char_fish_ALL_loop03","CA_char_fish_ALL_loop03")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()-- animation
Skeleton("CA_char_fish_A")
Animation("CA_char_fish_A")


STake("CA_char_fish_A")


-- paperdoll
Equip("CA_char_fish_B","",3)




Dimension(400, 20)
BoundingBox(20,20,10,-20,-40,0,0,0,0)-- animation
Skeleton("CA_char_fish_A")
Animation("CA_char_fish_A")


STake("CA_char_fish_A")


-- paperdoll
Equip("CA_char_fish_C","",3)




Dimension(400, 20)
BoundingBox(20,20,10,-20,-40,0,0,0,0)
Skeleton("CS_PR_baoz01_SKIN")
Animation("CS_PR_baoz01_SKIN")

STake("CS_PR_baoz01_SKIN_CS")

Equip("CS_PR_baoz01_SKIN","",3)


Dimension(500, 150)
BoundingBox(500,500,200,-500,-500,0,0,0,0)
STake_Start("CS_PR_baoz01_emo01_start","CS_PR_baoz01_emo01")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.0001)   
    Loop()

STake_Start("CS_PR_baoz01_emo01","CS_PR_baoz01_emo01")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()



Skeleton("CS_PR_money01_skin")
Animation("CS_PR_money01_skin_CS")

STake("CS_PR_money01_skin_CS")

Equip("CS_PR_money01_skin","",3)


Dimension(500, 150)
BoundingBox(500,500,200,-500,-500,0,0,0,0)
STake_Start("CS_PR_money01_emo001_start","CS_PR_money01_emo001")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.0001)   
    Loop()

STake_Start("CS_PR_money01_emo001","CS_PR_money01_emo001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


-- animation
Skeleton("FX_Lsword_A")
Animation("")


STake("")


-- paperdoll
Equip("FX_Lsword_A","",3)




Dimension(12, 12)
-- animation
Skeleton("FX_Lsword_B")
Animation("")


STake("")


-- paperdoll
Equip("FX_Lsword_B","",3)




Dimension(12, 12)
-- animation
Skeleton("FX_Lsword_C")
Animation("")


STake("")


-- paperdoll
Equip("FX_Lsword_C","",3)




Dimension(12, 12)
-- animation
Skeleton("FX_Lsword_D")
Animation("")


STake("")


-- paperdoll
Equip("FX_Lsword_D","",3)




Dimension(12, 12)
-- animation
Skeleton("FX_T_Bianzi")
Animation("FX_T_Bianzi")


STake("FX_T_Bianzi")


-- paperdoll
Equip("FX_T_Bianzi","T_Bianzi",3)



Dimension(30,45)


Rebound(10,10);
Dampper(5);



STake_Start("FX_T_Bianzi","FX_T_Bianzi")
    BlendMode(0)
    BlendTime(0.02) 
    Loop()
		
-- animation
Skeleton("FX_box_A")
Animation("FX_box_A")


STake("FX_box_A")



-- paperdoll
Equip("FX_box_A","",3)



Dimension(25, 17)


STake_Start("GD_PR_box_A_wait","GD_PR_box_A_wait")  --待机
    BlendMode(0)
    BlendTime(0.0) 


-- animation
Skeleton("Fx_DLdagger01")
Animation("Fx_DLdagger01")


STake("Fx_DLdagger01")


-- paperdoll
Equip("Fx_DLdagger01","",1)




Dimension(19, 50)
package.path = package.path .. ";..\\char\\?.lua"
require("common")


STake_Start("Fx_DLdagger01_skill_011_tracker","Fx_DLdagger01_skill_011_tracker")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0,"Fx_DLdagger01_skill_011_tracker")

STake_Start("Fx_DLdagger01_skill_011_tracker+","Fx_DLdagger01_skill_011_tracker+")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    Fx(0,"Fx_DLdagger01_skill_011_tracker")

STake_Start("Fx_DLdagger01_skill_011_hit","Fx_DLdagger01_skill_011_hit")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.666,1)
    Fx(0,"Fx_DLdagger01_skill_011_hit")
    Fx(0.366,"HG_DL_DA_skill_011_wind_hitloop")
    Fx(0.666,"HG_DL_DA_skill_011_wind_hitloop")

STake_Start("Fx_DLdagger01_skill_006_fire","Fx_DLdagger01_skill_006_fire")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0,"Fx_DLdagger01_skill_006_tracker") 

STake_Start("Fx_DLdagger01_skill_006_fire+","Fx_DLdagger01_skill_006_fire+")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0,"Fx_DLdagger01_skill_006_fire") 

STake_Start("Fx_DLdagger01_skill_006_fire+_new","Fx_DLdagger01_skill_006_fire+_new")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0,"Fx_DLdagger01_skill_006_fire") 
-- animation
Skeleton("Fx_MaHuang")
Animation("Fx_MaHuang")


STake("Fx_MaHuang")


-- paperdoll
Equip("Fx_MaHuang","",3)



Dimension(10, 10)


STake_Start("Fx_MaHuang_anim01","Fx_MaHuang_anim01")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("Fx_MaHuang_anim02","Fx_MaHuang_anim02")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-- animation
Skeleton("Fx_QZjian01")
Animation("FX_SW_skill")


STake("FX_SW_skill")


-- paperdoll
Equip("Fx_QZjian01","",1)




Dimension(10, 30)
package.path = package.path .. ";..\\char\\?.lua"
require("common")

STake_Start("FX_skill_001","FX_skill_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("FX_skill_002_start","FX_skill_002_start")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.4,2.4)
    Fx(0.000,"HG_QZ_SW_skill_002_Start_Sword")
    Fx(1.000,"HG_QZ_SW_skill_002_Start_Sword")

STake_Start("FX_skill_002_fire","FX_skill_002_start")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.2,2.4) 
    Loop()
    Fx(0.000,"HG_QZ_SW_skill_002_Start_Sword")

STake_Start("FX_skill_004","FX_skill_004")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0.000,"HG_QZ_SW_skill_004_sword")

STake_Start("FX_skill_005","FX_skill_005")
    BlendMode(0)
    BlendTime(0.2) 
    Loop() 
    --Fx(0.000,"HG_QZ_SW_skill_010_Start_Sword")

STake_Start("FX_skill_005_new","FX_skill_005_new")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    Fx(0.000,"HG_QZ_SW_skill_005_loop_Sword")

STake_Start("FX_skill_007","FX_skill_007")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("FX_skill_007_fire","FX_skill_007")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0.799,0.8) 
    Loop()

STake_Start("FX_skill_010_start","FX_skill_010_start")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0.000,"HG_QZ_SW_skill_010_Start_Sword")

STake_Start("FX_skill_010_startloop","FX_skill_010_start")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0.8,1) 
    Loop()
    Fx(0.000,"HG_QZ_SW_skill_010_Startloop_Sword")

STake_Start("FX_skill_010_fire","FX_skill_010_fire")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0.000,"HG_QZ_SW_skill_010_Fire_Sword")


-- animation
Skeleton("Fx_Yuwang")
Animation("Fx_Yuwang")


STake("Fx_Yuwang")


-- paperdoll
Equip("Fx_Yuwang","",3)




Dimension(100, 20)


STake_Start("Fx_Yuwang_anim_tail","Fx_Yuwang_anim_tail")  --轨迹
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("Fx_Yuwang_anim_hit","Fx_Yuwang_anim_hit")  --打开
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("Fx_Yuwang_anim_wait","Fx_Yuwang_anim_wait")  --loop
    BlendMode(0)
    BlendTime(0.0) 
    Loop()
-- animation
Skeleton("GD_BR_pot_FX_anim")
Animation("GD_BR_pot_FX_anim")


STake("GD_BR_pot_FX_anim")


-- paperdoll
Equip("GD_BR_pot_FX_anim","",3)




Dimension(22, 22)


STake_Start("GD_BR_pot_FX_anim","GD_BR_pot_FX_anim")  
    BlendMode(0)
    BlendTime(0.2) 



Skeleton("GD_CS_PR_kuaizi01_skin")
Animation("GD_CS_PR_kuaizi01")

STake("GD_CS_PR_kuaizi01")

Equip("GD_CS_PR_kuaizi01_skin","",3)



Dimension(500, 150)
BoundingBox(500,500,200,-500,-500,0,0,0,0)STake_Start("GD_CS_PR_kuaizi01_GJ_emo_024","GD_CS_PR_kuaizi01_GJ_emo_024")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("GD_CS_PR_kuaizi01_GJ_emo_025","GD_CS_PR_kuaizi01_GJ_emo_025")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("GD_CS_PR_kuaizi01_HR_emo_027","GD_CS_PR_kuaizi01_HR_emo_027")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_stop_kuaizi01_GJ","GD_CS_PR_kuaizi01_GJ_emo_024")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.0001)  
    Loop()

STake_Start("CS_stop_kuaizi01_HR","GD_CS_PR_kuaizi01_HR_emo_027")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.0001)  
    Loop()
-- animation
Skeleton("GD_PR_giving01_A")
Animation("..\\char\\char")


STake("GD_PR_giving01_A")


-- paperdoll
Equip("GD_PR_giving01_A","",3)



Dimension(20, 20)
STake_Start("Monster_wait_000","char_wait_000")     --待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()-- animation
Skeleton("GD_buddha_BR_floor_falldrop")
Animation("GD_buddha_BR_floor_falldrop")


STake("GD_buddha_BR_floor_falldrop")


-- paperdoll
Equip("GD_buddha_BR_floor_falldrop","",3)



Dimension(440, 1)
BoundingBox(300,300,300,-300,-300,0,0,0,0)

STake_Start("GD_buddha_BR_floor_falldrop","GD_buddha_BR_floor_falldrop")
    BlendMode(0)
    BlendTime(0.2)
    Loop()-- animation
Skeleton("HG_THD_FL_skill_006_canying")
Animation("HG_THD_FL_skill_006_canying")
Animation("THD_SW")
STake("HG_THD_FL_skill_006_canying")




Equip("HG_THD_FL_skill_006_canying","HG_THD_FL_skill_006_canying",3)


Dimension(20,32)

Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);

function Fx(time, name, bone, scale, mode, x, y, z, rh, rv)
    -- CHARACTER_EVENT_FX
	Event(1, time, name, bone, scale, mode, x, y, z, rh, rv)
end

STake_Start("HG_THD_FL_skill_006_canying_001","HG_THD_FL_skill_006_canying_001")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0,"HG_THD_FL_skill_010_CTZ")

STake_Start("HG_THD_FL_skill_006_canying_002","HG_THD_FL_skill_006_canying_002")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0,"HG_THD_FL_skill_010_CTZ")

STake_Start("HG_THD_FL_skill_006_canying_003","HG_THD_FL_skill_006_canying_003")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0,"HG_THD_FL_skill_010_CTZ2")

--THD--------------------------------------------------------------------

STake_Start("HG_THD_SW_attack_C_JA_yingzi_001","HG_THD_SW_attack_C_JA_yingzi_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_attack_C_JA_yingzi_002","HG_THD_SW_attack_C_JA_yingzi_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_attack_C_JA_yingzi_003","HG_THD_SW_attack_C_JA_yingzi_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_attack_C_JA_yingzi_004","HG_THD_SW_attack_C_JA_yingzi_004")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_attack_C_yingzi_005","HG_THD_SW_attack_C_yingzi_005")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("HG_THD_SW_attack_C_yingzi_001","HG_THD_SW_attack_C_yingzi_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_attack_C_yingzi_002","HG_THD_SW_attack_C_yingzi_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_attack_C_yingzi_003","HG_THD_SW_attack_C_yingzi_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_attack_C_yingzi_004","HG_THD_SW_attack_C_yingzi_004")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_skill_010_001","HG_THD_SW_skill_010_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_skill_010_002","HG_THD_SW_skill_010_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_skill_010_003","HG_THD_SW_skill_010_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_skill_010_004","HG_THD_SW_skill_010_004")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_skill_010_005","HG_THD_SW_skill_010_005")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_skill_010_006","HG_THD_SW_skill_010_006")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("HG_THD_SW_skill_010_007","HG_THD_SW_skill_010_007")
    BlendMode(0)
    BlendTime(0.2) -- animation
Skeleton("HG_THD_canying1")
Animation("HG_THD_FL_skill_006_canying")
Animation("THD_SW")
STake("HG_THD_FL_skill_006_canying")




Equip("HG_THD_canying1","HG_THD_FL_skill_006_canying",3)


Dimension(20,32)

Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);

-- animation
Skeleton("HG_THD_canying2")
Animation("HG_THD_FL_skill_006_canying")
Animation("THD_SW")
STake("HG_THD_FL_skill_006_canying")




Equip("HG_THD_canying2","HG_THD_FL_skill_006_canying",3)


Dimension(20,32)

Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);

-- animation
Skeleton("HG_THD_canying3")
Animation("HG_THD_FL_skill_006_canying")
Animation("THD_SW")
STake("HG_THD_FL_skill_006_canying")




Equip("HG_THD_canying3","HG_THD_FL_skill_006_canying",3)


Dimension(20,32)

Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);

-- animation
Skeleton("HG_THD_canying4")
Animation("HG_THD_FL_skill_006_canying")
Animation("THD_SW")
STake("HG_THD_FL_skill_006_canying")




Equip("HG_THD_canying4","HG_THD_FL_skill_006_canying",3)


Dimension(20,32)

Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);

-- animation
Skeleton("Mount_Deer01")
Animation("Mount_Deer01")

STake("Mount_Deer01")

-- paperdoll
Equip("Mount_Deer01","Mount_Deer01",3)



Dimension(20, 50)
Fx(0,"Mount_Deer01_wait_001","Reference",0,0,0)
Rebound(3, 3)
Dampper(3)package.path = package.path .. ";..\\char\\?.lua;..\\char\\?.cstake"
require("common")

STake_Start("RideDeer_wait_000","Mount_Deer01_wait_000")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

	
STake_Start("RideDeer_move_000","Mount_Deer01_move_000")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()      
	GroundSound(0.05,"Sound_step")
	GroundSound(0.28,"Sound_step")
	GroundSound(0.51,"Sound_step")
	GroundSound(0.74,"Sound_step")
	GroundSound(0.97,"Sound_step")
	GroundSound(1.227,"Sound_step")
        

    Fx(0.319, "", "Reference",1,1,-5,-27,0,0,0,0,1)   
    Fx(0.74, "", "Reference",1,1,7,2,0,0,0,0,1)   
    Fx(0.999, "", "Reference",1,1,5,-27,0,0,0,0,1)   
    Fx(1.3, "", "Reference",1,1,-5,0,0,0,0,0,1)    	
	
STake_Start("RideDeer_move_004","Mount_Deer01_move_004")  
    BlendMode(1)
    BlendTime(0)     
    Loop()      
	GroundSound(0.05,"Sound_Run")
    

    Fx(0.239, "", "Reference",1,1,5,-27,0,0,0,0,1) 
    Fx(0.333, "", "Reference",1,1,-2,-30,0,0,0,0,1)   
    Fx(0.499, "", "Reference",1,1,8,-8,0,0,0,0,1)   
    Fx(0.566, "", "Reference",1,1,-8,-7,0,0,0,0,1)  

STake_Start("RideDeer_move_007","Mount_Deer01_move_004")  
    BlendMode(1)
    BlendTime(0)     
    Loop()         
	GroundSound(0.05,"Sound_Run")


    Fx(0.069, "", "Reference",1,1,-5,-31,0,0,0,0,1)  
    Fx(0.133, "", "Reference",1,1,2,-29,0,0,0,0,1) 
    Fx(0.271, "", "Reference",1,1,8,-7,0,0,0,0,1)   
    Fx(0.335, "", "Reference",1,1,-9,-9,0,0,0,0,1)
	
	
STake_Start("RideDeer_move_012","Mount_Deer01_move_012")  
    BlendMode(1)
    BlendTime(0.2)     
    Loop()  
	GroundSound(0.42,"Sound_step")
	GroundSound(0.873,"Sound_step")
	GroundSound(0.95,"Sound_step")
	GroundSound(1.33,"Sound_step")


    Fx(0.398, "", "Reference",1,1,17,1,0,-50,0,0,1) 
    Fx(0.812, "", "Reference",1,1,12,-21,0,-50,0,0,1)  
    Fx(0.843, "", "Reference",1,1,-15,5,0,10,0,0,1)   
    Fx(1.165, "", "Reference",1,1,5,8,0,-30,0,0,1)   

STake_Start("RideDeer_move_013","Mount_Deer01_move_013")  
    BlendMode(1)
    BlendTime(0.2)     
    Loop()          
	GroundSound(0.42,"Sound_step")
	GroundSound(0.873,"Sound_step")
	GroundSound(0.95,"Sound_step")
	GroundSound(1.33,"Sound_step")


    Fx(0.015, "", "Reference",1,1,8,8,0,-30,0,0,1)
    Fx(0.475, "", "Reference",1,1,-24,-21,0,20,0,0,1) 
    Fx(0.950, "", "Reference",1,1,-4,-16,0,30,0,0,1)   	
    Fx(1.073, "", "Reference",1,1,-4,14,0,30,0,0,1) 	
	
STake_Start("RideDeer_move_015_up","Mount_Deer01_move_015+015_down") --前跳跃
    BlendTime(0.2)
    BlendMode(0)
	GroundSound(0.05,"Sound_Jump")
	Sound(0.05,"DeerBell")
      
    FrameTime(0,0.8)  

    Fx(0.839, "", "Reference",1,1,7,-19,0,0,0,0,1) 
    Fx(0.879, "", "Reference",1,1,-9,-12,0,0,0,0,1)  
    Fx(0.949, "", "Reference",1,1,6,11,0,0,0,0,1)   
    Fx(0.989, "", "Reference",1,1,-6,10,0,0,0,0,1)	
    Fx(1.779, "", "Reference",1,1,7,-17,0,0,0,0,1)	
    Fx(1.779, "", "Reference",1,1,-5,-24,0,0,0,0,1)	
	
STake_Start("RideDeer_move_015_down","Mount_Deer01_move_015+015_down") --前跳跃
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.8,1.6)
	GroundSound(0.98,"Sound_Fall")
	Sound(0.98,"DeerBell")
    

            
STake_Start("RideDeer_move_015_wait","Mount_Deer01_move_015_wait") --前跳跃
    BlendMode(0)
    BlendTime(0.2)     
    Loop()    	
	
	
STake_Start("RideDeer_move_23","RideDeer_move_23")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()    


    Fx(0.069, "", "Reference",1,1,-3,-31,0,0,0,0,1)  
    Fx(0.133, "", "Reference",1,1,4,-26,0,0,0,0,1) 
    Fx(0.271, "", "Reference",1,1,5,-7,0,0,0,0,1)   
    Fx(0.335, "", "Reference",1,1,-7,-9,0,0,0,0,1) 
        
STake_Start("RideDeer_move_24","RideDeer_move_24")
    BlendMode(1)
    BlendTime(0.2)     
    Loop()  


    Fx(0.069, "", "Reference",1,1,-6,-30,0,0,0,0,1)  
    Fx(0.138, "", "Reference",1,1,0,-30,0,0,0,0,1) 
    Fx(0.271, "", "Reference",1,1,9,-7,0,0,0,0,1)   
    Fx(0.335, "", "Reference",1,1,-5,-9,0,0,0,0,1)
	
STake_Start("RideDeer_emo_001","Mount_Deer01_emo_001")
    BlendMode(0)
    BlendTime(0.2)     
	Sound(0.6,"AM_Deer_EmoVoc")
	Sound(0.08,"Wolf_L_move_004_1")
	Sound(0.18,"Wolf_L_move_004_1")
	Sound(2.13,"Wolf_L_move_004_1")	
	Sound(3.18,"Wolf_L_move_004_1")


    
    
STake_Start("RideDeer_emo_002","Mount_Deer01_emo_002")
    BlendMode(0)
    BlendTime(0.2)     
	Sound(1.28,"DeerBell")
	Sound(2.23,"DeerBell")	
	GroundSound(1.28,"Sound_step")
	GroundSound(2.23,"Sound_step")
	GroundSound(3.33,"Sound_step")



STake_Start("RideDeer_emo_003","Mount_Deer01_emo_003")
    BlendMode(0)
    BlendTime(0.2)     
	Sound(0.08,"Wolf_L_move_004_1")
	Sound(0.58,"Wolf_L_move_004_1")
	Sound(1.58,"Wolf_L_move_004_1")
	GroundSound(0.44,"Sound_step")
	GroundSound(0.8,"Sound_step")
	GroundSound(3.36,"Sound_step")
    
-- animation
Skeleton("Mount_Deer01")
Animation("Mount_Deer01")

STake("Mount_Deer01")

-- paperdoll
Equip("Mount_Deer01","Mount_Deer01",3)



Dimension(20, 50)
Fx(0,"Mount_Deer01_wait_000","Reference",0,0,0)
Rebound(3, 3)
Dampper(3)-- animation
Skeleton("NB_CXF_ChenXuanFeng_hand")
Animation("NB_CXF_ChenXuanFeng_hand_anim")


STake("NB_CXF_ChenXuanFeng_hand_anim")


-- paperdoll
Equip("NB_CXF_ChenXuanFeng_hand","",3)



Dimension(10, 10)




STake_Start("NB_CXF_ChenXuanFeng_hand_anim","NB_CXF_ChenXuanFeng_hand_anim")
    BlendMode(0)
    BlendTime(0.2) 




-- animation
Skeleton("NB_MCF_MeiChaoFeng_Handwp")
Animation("NB_MCFHand")


STake("NB_MCFHand")


-- paperdoll
Equip("NB_MCF_MeiChaoFeng_Handwp","",1)




Dimension(19, 40)
package.path = package.path .. ";..\\char\\?.lua"
require("common")



STake_Start("NB_MCFHand_att01","NB_MCFHand_att01")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0.37,"NB_MCF_skill_001_hand")


STake_Start("NB_MCFHand_att02","NB_MCFHand_att02")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0.37,"NB_MCF_skill_001_hand")


STake_Start("NB_MCFHand_att03","NB_MCFHand_att03")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0.51,"NB_MCF_skill_001_hand")


STake_Start("NB_MCFHand_att04","NB_MCFHand_att04")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0.58,"NB_MCF_skill_001_hand")


STake_Start("NB_MCFHand_att05","NB_MCFHand_att05")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0.6,"NB_MCF_skill_001_hand")


STake_Start("NB_MCFHand_att06","NB_MCFHand_att06")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0.6,"NB_MCF_skill_001_hand")


STake_Start("NB_MCFHand_att07","NB_MCFHand_att07")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0.01,"NB_MCF_skill_001_hand")
	
STake_Start("NB_MCFHand_att08","NB_MCFHand_att08")
    BlendMode(0)
    BlendTime(0.2)

	
STake_Start("NB_MCFraid_skill_011_fire","NB_MCFHand_att02")    --38030
    BlendMode(0)
    BlendTime(0.2)
    PlaySpeed(0,0.5,0.3125)	
		
	AttackStart()
  	Hurt(1.6,"hurt_71")
    HurtSound(1.6,"QZ_PA_Hit_11")
    DirFx(1.6,"",0,1) 	
	HurtBoneFx(1.6,"FX_blood_zhua_c","Spine1",1,0,0,0,0,0,0)		
	

STake_Start("Monster_B_wait_000","NB_MCFHand_att02")  --战斗待机
    BlendMode(0)
    BlendTime(0.2)
    Loop(0,0.31)	
	
	
	
	
	
	-- animation
Skeleton("NB_MCF_MeiChaoFeng_W")
Animation("NB_MCF_MeiChaoFeng_W_anim")


STake("NB_MCF_MeiChaoFeng_W_anim")


-- paperdoll
Equip("NB_MCF_MeiChaoFeng_W","",1)



Dimension(19, 40)




STake_Start("NB_MCF_MeiChaoFeng_W_anim","NB_MCF_MeiChaoFeng_W_anim")  
    BlendMode(0)
    BlendTime(0.1) 

STake_Start("NB_MCF_MeiChaoFeng_W_anim_006_start","NB_MCF_MeiChaoFeng_W_anim_006_start")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("NB_MCF_MeiChaoFeng_W_anim_006_fire","NB_MCF_MeiChaoFeng_W_anim_006_fire")  
    BlendMode(0)
    BlendTime(0.2) 


-- animation
Skeleton("N_Fx_Mohuachibang01")
Animation("N_Fx_Mohuachibang01")


STake("N_Fx_Mohuachibang01")


-- paperdoll
Equip("N_Fx_Mohuachibang01","",3)





Dimension(20, 20)

STake_Start("N_Fx_Mohuachibang01_wait01","N_Fx_Mohuachibang01_wait01")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N_Fx_Mohuachibang01_wait02","N_Fx_Mohuachibang01_wait02")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N_Fx_Mohuachibang01_wait03","N_Fx_Mohuachibang01_wait03")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N_Fx_Mohuachibang01_wait01a","N_Fx_Mohuachibang01_wait01")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,2,0.7)

STake_Start("N_Fx_Mohuachibang01_wait02a","N_Fx_Mohuachibang01_wait02")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,2,0.7)

STake_Start("N_Fx_Mohuachibang01_wait03a","N_Fx_Mohuachibang01_wait03")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,2,0.7)
STake_Start("N_Fx_M01_wait01b","N_Fx_Mohuachibang01_wait01")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,2,3)

STake_Start("N_Fx_M01_wait02b","N_Fx_Mohuachibang01_wait02")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,2,3)

STake_Start("N_Fx_M01_wait03b","N_Fx_Mohuachibang01_wait03")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,2,3)
-- animation
Skeleton("N_Fx_Mohuachibang01")
Animation("N_Fx_Mohuachibang01")


STake("N_Fx_Mohuachibang01")


-- paperdoll
Equip("N_Fx_Mohuachibang01_A","",3)





Dimension(20, 20)
-- animation
Skeleton("N_Fx_Mohuachibang01")
Animation("N_Fx_Mohuachibang01")


STake("N_Fx_Mohuachibang01")


-- paperdoll
Equip("N_Fx_Mohuachibang01_B","",3)





Dimension(20, 20)
-- animation
Skeleton("N_Fx_Mohuachibang01")
Animation("N_Fx_Mohuachibang01")


STake("N_Fx_Mohuachibang01")


-- paperdoll
Equip("N_Fx_Mohuachibang01_C","",3)





Dimension(20, 20)
-- animation
Skeleton("N_Fx_Mohuachibang01")
Animation("N_Fx_Mohuachibang01")


STake("N_Fx_Mohuachibang01")


-- paperdoll
Equip("N_Fx_Mohuachibang01_D","",3)





Dimension(20, 20)
-- animation
Skeleton("N_Fx_Mohuachibang01_E")
Animation("N_Fx_Mohuachibang01_E_Anim")
Animation("N_Fx_Mohuachibang01")

STake("N_Fx_Mohuachibang01_E_Anim")


-- paperdoll
Equip("N_Fx_Mohuachibang01_E","N_Fx_Mohuachibang01_E",3)





Dimension(20, 20)
Rebound(10,1)
Dampper(20)
STake_Start("N_Fx_Mohuachibang01_wait01","N_Fx_Mohuachibang01_wait01")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,2,5)

STake_Start("N_Fx_Mohuachibang01_wait02","N_Fx_Mohuachibang01_wait02")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N_Fx_Mohuachibang01_wait03","N_Fx_Mohuachibang01_wait03")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N_Fx_Mohuachibang01_wait04","N_Fx_Mohuachibang01_wait03")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N_Fx_Mohuachibang01_E_Anim","N_Fx_Mohuachibang01_E_Anim")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()-- animation
Skeleton("N_Fx_Mohuachibang01_F")
Animation("N_Fx_Mohuachibang01_E_Anim")
Animation("N_Fx_Mohuachibang01")

STake("N_Fx_Mohuachibang01_E_Anim")


-- paperdoll
Equip("N_Fx_Mohuachibang01_F","N_Fx_Mohuachibang01_F",3)





Dimension(20, 20)
Rebound(10,1)
Dampper(20)-- animation
Skeleton("N_Fx_Mohuachibang01_G")
Animation("N_Fx_Mohuachibang01_E_Anim")
Animation("N_Fx_Mohuachibang01")

STake("N_Fx_Mohuachibang01_E_Anim")


-- paperdoll
Equip("N_Fx_Mohuachibang01_G","N_Fx_Mohuachibang01_G",3)





Dimension(20, 20)
Rebound(10,1)
Dampper(20)-- animation
Skeleton("N_Fx_Mohuachibang01_H")
Animation("N_Fx_Mohuachibang01_E_Anim")
Animation("N_Fx_Mohuachibang01")

STake("N_Fx_Mohuachibang01_E_Anim")


-- paperdoll
Equip("N_Fx_Mohuachibang01_H","N_Fx_Mohuachibang01_H",3)





Dimension(20, 20)
Rebound(10,1)
Dampper(20)-- animation
Skeleton("N_Fx_Mohuachibang01")
Animation("N_Fx_Mohuachibang01")


STake("N_Fx_Mohuachibang01")


-- paperdoll
Equip("N_Fx_Mohuachibang02","",3)





Dimension(20, 20)
-- animation
Skeleton("N_Longjuanfeng")
Animation("N_Longjuanfeng")


STake("N_Longjuanfeng")


-- paperdoll
Equip("N_Longjuanfeng","",3)



-- attack, hurt sphere
--Sphere("bone",radius,x,y,z,group,part)
Sphere("Hips",6,2,0,0,0,0)
Sphere("T_feng03",20,2,0,0,0,0)
Sphere("T_feng05",20,-4,0,0,8,1)

-- hurt group
HurtedCheckGroup(0)
HurtedCheckGroup(1)
HurtedCheckGroup(2)
HurtedCheckGroup(3)
HurtedCheckGroup(4)

-- render bounding
RenderCheckGroup(0)
RenderCheckGroup(1)
RenderCheckGroup(2)
RenderCheckGroup(3)
RenderCheckGroup(4)
RenderCheckDynaGroup(10, 15) -- weapon render sphere (part 10, group 15)

-- collide scene size
Dimension(19, 50)
-- collide character size
ColSize(0, 0, 0)
ColSize(1, 17, 36)
ColSize(2, 17, 18)
ColSize(3, 17, 9)
-- hurt box size
HurtBoxSize(19, 50)

STake_Start("N_Longjuanfeng_wait_000","N_Longjuanfeng_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-- animation
Skeleton("N_Longjuanfeng_yang")
Animation("N_Longjuanfeng")


STake("N_Longjuanfeng")


-- paperdoll
Equip("N_Longjuanfeng_yang","",3)


Dimension(19, 50)
-- animation
Skeleton("N_Scorpion_XieZiWang01")
Animation("N_Scorpion_XieZiWang01_weiba")


STake("N_Scorpion_XieZiWang01_weiba")


-- paperdoll
Equip("N_Scorpion_XieZiWang01_weiba","",3)



Dimension(19, 50)


STake_Start("N_Scorpion_XieZiWang01_weiba","N_Scorpion_XieZiWang01_weiba")
    BlendMode(0)
    BlendTime(0.2) 

-- animation
Skeleton("N_Snake_She")
Animation("N_Snake_She")


STake("N_Snake_She")


-- paperdoll
Equip("N_Snake_She","",1)



Dimension(15, 10)


STake_Start("N_Snake_She01_skill","N_Snake_She01_skill")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("N_Snake_She01_skill_hit","N_Snake_She01_skill_hit")
    BlendMode(2)
    BlendTime(0.2)
    Loop()


-- animation
Skeleton("N_THDJG_Fire")
Animation("N_THDJG")

STake("N_THDJG")

Equip("N_THDJG_Fire","",3)

Dimension(20,23)
package.path = package.path .. ";..\\char\\?.lua"
require("common")


STake_Start("N_THDJG_Fire_start","N_THDJG_Fire_start")  --出生
    BlendMode(0)
    BlendTime(0) 
    Fx(0.0,"N_THDJG_Fire_start","Reference",1)
    Fx(0.0,"HG_THD_WUXING_HUO_fire","Reference",0.4)
    Fx(0.0,"N_THDJG_Fire_range_start","Reference",1)


STake_Start("N_THDJG_Fire_fire","N_THDJG_Fire_fire")  --攻击loop
    BlendMode(0)
    BlendTime(0) 
    Loop()
    
    
    LoopFx(0.0, 30.0, 0, "N_THDJG_Fire_loop","Reference",1)
    LoopFx(0.0, 30.0, 0, "N_THDJG_Fire_range","Reference",1)
    LoopFx(0.0, 30.0, 0, "HG_THD_WUXING_HUO_loop2","Reference",0.4)

  	AttackStart()
  	Hurt(0.2600,"")

  	DragFx(0.26, 0.4, "N_THDJG_Fire_tracker01", "FX_buff_firehurt", 0, 0, 0, "T_h2", "Spine1",0,0,0,0,0,1)

  
  Sound(0.500,"fire_wall")  

STake_Start("N_THDJG_Fire_end","N_THDJG_Fire_end")  --消失
    BlendMode(0)
    BlendTime(0.2) 
    
    Fx(0.0,"N_THDJG_Fire_end","Reference",1)
    Fx(0.0,"HG_THD_WUXING_HUO_end","Reference",0.4)
    Fx(0.0,"N_THDJG_Fire_range_end","Reference",1)
-- animation
Skeleton("N_quanzhencanying")
Animation("SG_QZ_SWfx")
Animation("SG_QZ_SW")

STake("N_quanzhencanying")


-- paperdoll
Equip("N_quanzhencanying","",3)




Dimension(19, 30)
					  


STake_Start("FX_QZ_Yishan","SG_QZ_SW_skill_029")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("SG_QZ_SW_skill_012_fire01","SG_QZ_SW_skill_012_fire01")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("SG_QZ_SW_skill_012_fire01+","SG_QZ_SW_skill_012_fire01+")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("SG_QZ_SW_skill_013_fire_new","SG_QZ_SW_skill_013_fire_new")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("SG_QZ_SW_skill_023_001","SG_QZ_SW_skill_023_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("SG_QZ_SW_skill_023_002","SG_QZ_SW_skill_023_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("SG_QZ_SW_skill_023_003","SG_QZ_SW_skill_023_003")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("SG_QZ_SW_skill_022_001","SG_QZ_SW_skill_022_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("SG_QZ_SW_skill_022_002","SG_QZ_SW_skill_022_002")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("SG_QZ_SW_skill_022_003","SG_QZ_SW_skill_022_003")
    BlendMode(0)
    BlendTime(0.2)-- animation
Skeleton("PC_GB1_CanYing")
Animation("PC_GB1_CanYing_ST")


STake("PC_GB1_CanYing_ST")


-- paperdoll
Equip("PC_GB1_CanYing","",3)



Dimension(19,32)


STake_Start("HG_GB_ST_skill_003_trail01","HG_GB_ST_skill_003_trail")
    BlendMode(0)
    BlendTime(0.02) 

STake_Start("HG_GB_ST_skill_003_hit01","HG_GB_ST_skill_003_hit")
    BlendMode(0)
    BlendTime(0.02) 

STake_Start("HG_GB_ST_skill_002_combo301","HG_GB_ST_skill_002_combo3")
    BlendMode(0)
    BlendTime(0.02) -- animation
Skeleton("long")
Animation("long")


STake("long")


-- paperdoll
Equip("long","",1)



Dimension(20,32)
package.path = package.path .. ";..\\char\\?.lua"
require("common")

STake_Start("HG_GB_PA_skill_003_long","HG_GB_PA_skill_003_long")
    BlendMode(0)
    BlendTime(0.02) 
    Loop()
		
STake_Start("long_skill_1222","long_skill_1222")
    BlendMode(0)
    BlendTime(0.02) 

			
STake_Start("long_skill_1223","long_skill_1223")
    BlendMode(0)
    BlendTime(0.02) 

STake_Start("long_skill_1224","long_skill_1224")
    BlendMode(0)
    BlendTime(0.02) 	
    Loop()

STake_Start("long_skill_1225","long_skill_1225")
    BlendMode(0)
    BlendTime(0.02) 	
    Loop()

STake_Start("long_skill_1226","long_skill_1226")
    BlendMode(0)
    BlendTime(0.02) 	
    Loop()

STake_Start("long_skill_1227","long_skill_1227")
    BlendMode(0)
    BlendTime(0.02) 	
    Loop()

STake_Start("long_skill_1228","long_skill_1228")
    BlendMode(0)
    BlendTime(0.02) 	


STake_Start("long_skill_1229","long_skill_1229")
    BlendMode(0)
    BlendTime(0.02) 	

    Sound(0.050,"HG_GB_PA_Skill_002_Combo3_D")

STake_Start("long_skill_1230","long_skill_1229")
    BlendMode(0)
    BlendTime(0.02) 	

    Sound(0.050,"HG_GB_PA_Skill_002_Combo3_D")
    PlaySpeed(0,2.2,2.4)
    PlaySpeed(2.5,3.033,2.5)	

STake_Start("long_skill_1231","long_skill_1231")
    BlendMode(0)
    BlendTime(0.02) 	
    FrameTime(0.2,2)
    Sound(0.050,"HG_GB_PA_Skill_002_Combo3_D")	

STake_Start("long_skill_1232","long_skill_1232")
    BlendMode(0)
    BlendTime(0.02) 	
    Loop()
-- animation
Skeleton("long")
Animation("long")


STake("long")


-- paperdoll
Equip("long_head","",1)



Dimension(20,32)
-- animation
Skeleton("long_B")
Animation("long")


STake("long")


-- paperdoll
Equip("long_B","",3)



Dimension(30,45)

Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);


Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);


Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);


Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);


Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);


Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);


Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);


Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);


Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);


Box("Neck",0,0.5,0,5,0.5,5,true) 

Sphere("Spine1",3.5,3,0,0,true)
Sphere("Neck",3,0,1,0,true)




ForceField(0,0,-196);

Rebound(2,10);
Dampper(2);

