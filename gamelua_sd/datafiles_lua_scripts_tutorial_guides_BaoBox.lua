
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local boxBtnSpec = indicatorFlow.createIndicatorSpec()
	boxBtnSpec.frameName     = 'boxBtnSpec'
	boxBtnSpec.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	boxBtnSpec.frameText     = '别忘了开启副本宝箱哦'
	boxBtnSpec.triggerWin    = 'Level_Reward/PlayerDetail'
	boxBtnSpec.attachWin     = 'Level_Reward/PlayerDetail'
	boxBtnSpec.attachWinRoot = 'Level_Score'
	boxBtnSpec.priority      = 0
	
	self.boxBtnSpec = boxBtnSpec
	
	self.indicatorSpecs = {
		boxBtnSpec, 
	}
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

