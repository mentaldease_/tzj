
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	
	-- Server Completion
	self.DazuoSkills = {
		[WM_PROF_XIANGLONG] = '17640',
		[WM_PROF_JIYING]    = '17640',
		[WM_PROF_YAOGUANG]  = '13720',
		[WM_PROF_BAIZHANG]  = '14720',
	}
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end


function Update(self, fTime)
	if self.completed then
		return
	end

	local playerEntity = Utility.GetPlayerEntity()
	if playerEntity then
		local playerUnit = playerEntity:GetUnit()
		if playerUnit then
			local eProf = playerUnit:GetProfession()
			local dwSkillID = self.DazuoSkills[eProf]
			if dwSkillID and playerEntity.GetCurSkillID and playerEntity:GetCurSkillID() == dwSkillID then
				self.completed = true
			end
		end
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

