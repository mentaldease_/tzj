local modname = ...

module(modname, package.seeall)

-- Listener for some Game Events from LuaEventManager.h/LuaGameEvents

eventConns = {}
function OnEnterScene()
	lout('SubscribeGameEvents: ' .. GameEvents.ConsoleCommand)
	local conn = gameEventMgr:subscribeEvent(GameEvents.ConsoleCommand, OnConsoleCommand)
	table.insert(eventConns, conn)
	
	-- ... listen to more

end

function OnLeaveScene()
	for _, v in ipairs(eventConns) do
		if v then v:disconnect() end
	end
	eventConns = {}
end

function OnConsoleCommand(evtArgs)
	local wrappedArgs = tolua.cast(evtArgs, 'SD::LuaWrappedEvtArg')
	local realArgs = tolua.cast(wrappedArgs.realArgs, 'SD::EventClientCmd')
	
	local command = realArgs.m_cmd:c_str()
	if command == 'play_huangrong_video' then
		PlayHuangRongCG()
		
	elseif command == 'xx' then
		
	end
end

function PlayHuangRongCG()
	lout('Start PlayHuangRongCG')
	if not ui.FullScreenVideoFrame then
		ui.createFrame('FullScreenVideoFrame')
	end
	ui.FullScreenVideoFrame:ShowVideo()

end