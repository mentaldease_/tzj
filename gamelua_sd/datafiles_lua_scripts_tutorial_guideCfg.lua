local modname = ...
module(modname, package.seeall)
function InitGuideCfg(tutorialModule)

	-- All constants referenced below are defined in guide_constants.lua

	local guide_list = {
		['Move'] = {
			Name = '移动',
			Text = '按 【W】【A】【S】【D】 移动到村长老爹面前',
			DelayShowTime = 3.0, 
		 },
		['CameraControl'] = {
			Name = '旋转镜头',
			Mode1Text = '试着移动鼠标旋转镜头吧',
			Mode2Text = '试着移动鼠标旋转镜头吧',
			Mode3Text = '试着按住鼠标并且移动来旋转镜头吧',
			DelayShowTime = 5.0, 
			AutoCompleteType = EAutoCompleteByTaskAccept,
			AutoCompleteTaskID = 853, -- 853.贪玩误正事
		},
		['FNpcDlg'] = {
			Name = '对话',
			Text = '靠近村长按【F】就可以和他对话了',
			DelayShowTime = 0.0, 
		 },
		['DoodadGuide'] = {
			Name = '操作doodad-采集',
			Text = '靠近野花按【F】就可以采集了哦',
			DelayShowTime = 0.0,
		},
		['FirstTalk2'] = {
			Name = '对话选项',
			Text = '小乞丐好像有话要和你说哦',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 1593, -- 1593.好奇的小乞丐
		},
		['DoodadGuide2'] = {
			Name = '操作doodad-互动操作',
			Text = '看见四叔身后的香炉了吧？去拜拜郭杨二位前辈吧！',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 856, -- 856.义士之墓
		}, 
		['LeftMouseAttack'] = {
			Name = '攻击',
			Mode1Text = '试着按左键攻击敌人吧！',
			Mode2Text = '试着按左键攻击敌人吧！',
			Mode3Text = '试着按【1】键攻击敌人吧！',
			DelayShowTime = 6.0, 
		 },
		['EvadeAttack'] = {
			Name = '闪避',
			Text = '避开红线！双击任意移动键可以快速移动闪避哦',
			DelayShowTime = 6.0, 
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10,
		 },
		['TwoPhaseJump'] = {
			Name = '二段跳',
			Text = '跳起后在空中再次按下空格键可以二段跳哦',
			DelayShowTime = 0.0, 
			AutoCompleteType = EAutoCompleteByTaskAccept,
			AutoCompleteTaskID = 879, -- 879.四妹和小寒
		 },
		['BreakScene'] = {
			Name = '破坏物件',
			Text = '豆腐叔被树枝压住了！试着打断树枝替他解围吧！',
			DelayShowTime = 0.0, 
		 },
		['KeyboardSkill'] = {
			Name = '门派技能',
			Mode1Text = '在战斗中领悟了新技能！按【1】使用门派技能试试吧！',
			Mode2Text = '在战斗中领悟了新技能！按【1】使用门派技能试试吧！',
			Mode3Text = '在战斗中领悟了新技能！按【3】使用门派技能试试吧！',
			DelayShowTime = 5.0,
		},
		['EvadeCircle'] = {
			Name = '闪避攻击圈',
			Text = '避开红圈！双击任意移动键快速闪避范围攻击',
			DelayShowTime = 5.0, 
		 },
		['TaskSubmitGuide'] = {
			Name = '交任务',
			OnlyIndicator = true,
			IndicatorText = '完成任务点这里（快捷键F）',
			DelayShowTime = 0.0, 
		 },
		['TaskAcceptGuide'] = {
			Name = '接任务',
			OnlyIndicator = true,
			IndicatorText = '接受任务点这里（快捷键F）',
			DelayShowTime = 0.0, 
		 },
		['EquipGuide'] = {
			Name = '快捷装备',
			OnlyIndicator = true,
			IndicatorText = '快点击近期装备栏中的山神石的碎片把它戴上（按住Alt唤出鼠标）',
			DelayShowTime = 0.0,
		},
		['MiniMapGuide'] = {
			Name = '雷达教学',
			OnlyIndicator = true,
			IndicatorText = '雷达地图上的任务点标记会指示任务相关的地点哦',
			DelayShowTime = 0.0,
		},
		['FirstTalk'] = {
			Name = '对话选项',
			OnlyIndicator = true,
			IndicatorText = '小乞丐好像有话要和你说哦',
			DelayShowTime = 0.0,
		},
		['MedicineGuide'] = {
			Name = '吃药教学',
			OnlyIndicator = true,
			IndicatorText = '前方凶险！按F1或者鼠标点击神秘疗伤药使用它吧',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskAccept,
			AutoCompleteTaskID = 878, -- 878.突生变故
		},
		['OperationMode'] = {
			Name = '操作模式',
			OnlyIndicator = true,
			IndicatorText = [[操作不习惯？可以看看其他操作模式哦!
可点击打开查看操作模式详细说明]],
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10,
		},
		['EquipGuide2'] = {
			Name = '装备山神石',
			Text = '按住【alt】呼出鼠标，点击包裹按钮或按 【B】 打开包裹',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 849, -- 849.成人仪式
		},
		
		---- 以下教程在 试剑崖
		['QingGong'] = {
			Name = '使用轻功',
			Text = '双击并按住【W】【A】【S】【D】任意键使用轻功快跑吧',
			DelayShowTime = 0.0, 
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 1435, -- 1435.试剑台上会师兄
		 },
		['LearnNewSkill'] = {
			Name = '技能学习',
			Text = '快按【K】键打开技能书学习新技能吧',
			DelayShowTime = 0.0, 
		 },
		['UseJaSkill'] = {
			Name = '变招',
			Mode1Text = '对木桩按【1】攻击后立马按【Q】即可施放对应变招技能哦',
			Mode2Text = '对木桩按【1】攻击后立马按【Q】即可施放对应变招技能哦',
			Mode3Text = '对木桩按【3】攻击后立马按【Q】即可施放对应变招技能哦',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 1432, -- 1433.备战论剑展筋骨
		},
		['Dazuo'] = {
			Name = '调息',
			Text = '按[image="set:Total_Tutorial image:douhao"]逗号键进行调息回血',
			DelayShowTime = 0.0, 
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 1433, -- 1433.凝神调息理血气
		 },
		['DungeonGate'] = {
			Name = '副本门',
			Text = '瞧见大师兄身后的橙色光门了吗？你懂的！',
			DelayShowTime = 0.0,
		},
		['DungeonReward'] = {
			Name = '副本奖励',
			Text = '首次副本胜利！恭喜恭喜！按【F5】看看奖励与结算吧',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 5,
		},
		['TalkShiJianYa'] = {
			Name = '对话3',
			Text = '就要离开试剑崖了，去找掌门说话辞别吧',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 1446, -- 1446.忽闻双煞现踪迹
		},
		['LearnNewSkill2'] = {
			Name = '技能学习气泡',
			OnlyIndicator = true,
			DelayShowTime = 0.0, 
		 },
		['FirstWeapon'] = {
			Name = '新装备',
			OnlyIndicator = true,
			IndicatorText = '新获得的装备会在这里显示哦',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskAccept,
			AutoCompleteTaskID = 1445, -- 1445.同门交手试炼场
		},
		['DungeonChoose'] = {
			Name = '选择并创建副本',
			OnlyIndicator = true,
			DelayShowTime = 0.0,
		},
		['MiniMapGuide2'] = {
			Name = '副本标记',
			OnlyIndicator = true,
			IndicatorText = '雷达地图上会标记出副本任务相关地点以及BOSS位置',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10,
		},
		['BaoBox'] = { -- 还是这个名字牛B
			Name = '副本宝箱',
			OnlyIndicator = true,
			IndicatorText = '别忘了开启副本宝箱哦',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10,
		},
		----- 以下教程在 子午谷
		['JumpPoint'] = {
			Name = '跳跃点',
			Text = '看见橙色光柱了吗？它可以把你送到山顶哦',
			DelayShowTime = 0.0,
		},
		['TalkZiWuGu'] = {
			Name = '对话4',
			Text = '百姓被关押在附近的房屋内，找到并且救出他们吧',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 1922, -- 1922.护送出关
		},
		['JASkill'] = {
			Name = '学技能2',
			OnlyIndicator = true,
			DelayShowTime = 0.0, 
		 },
		 ['TaskTrace'] = {
			Name = '变招教学',
			OnlyIndicator = true,
			IndicatorText = '鼠标放到追踪上可以快速在雷达地图上找到任务对应的地点哦',
			DelayShowTime = 0.0, 
		 },
		
		----- 以下教程在 大青山下
		['EquipDecompose'] = {
			Name = '分解装备',
			Text = '绿色品质以上的装备可被分解，打开包裹试试吧',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 1974, -- 1974.妙手拆解废变宝
		},
		['JoinGuild'] = {
			Name = '家族',
			Text = '在家族管家陈楠生那儿可以创建或加入家族哦',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 5, 
		},
		['EquipWaiZhuang'] = {
			Name = '穿时装',
			Text = '获得一件新时装~右键点击背包中的碧竹书箱将其放入衣柜',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTaskFinish,
			AutoCompleteTaskID = 1973, --1973.云想衣裳花想容
		},
		['Wardrobe'] = {
			Name = '穿时装',
			OnlyIndicator = true,
			DelayShowTime = 0.0, 
		 },
		['FishingPrepare'] = {
			Name = '钓鱼准备',
			Text = '一起来钓鱼吧~首先要在钓鱼人那里买钓鱼技能书和鱼竿哦',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10, 
		},
		['FishingLearn'] = {
			Name = '钓鱼技能',
			Text = '使用钓鱼技能书学会钓鱼技能，然后装备上鱼竿',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10, 
		},
		['FishingStart'] = {
			Name = '学习钓鱼',
			Text = '现在靠近河边点击垂钓就可以开始钓鱼啦',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10, 
		},
		['FishingWait'] = {
			Name = '观察鱼漂',
			Text = '鱼漂有动静说明鱼儿上钩了，这时右击鱼漂(红色箭头所指)收杆',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 20, 
		},
		['EnterBattleField'] = {
			Name = '进战场',
			OnlyIndicator = true,
			DelayShowTime = 0.0,
		},
		['UnlockNewBag'] = {
			Name = '解锁背包新格子',
			OnlyIndicator = true,
			DelayShowTime = 0.0, 
		 },
		['EquipDecompose2'] = {
			Name = '分解装备',
			OnlyIndicator = true,
			DelayShowTime = 0.0, 
		},
		['FishingStart2'] = {
			Name = '学习钓鱼',
			OnlyIndicator = true,
			DelayShowTime = 0.0, 
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10, 
		},
		-- 以下教程用于战场中
		['OpenBattleShop'] = {
			Name = '打开商店',
			Text = '先按【F7】打开战场商店，购买战场装备吧！',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByUIOpen,
			AutoCompleteUI = 'BattleShop', 
		},
		['ReturnBattleHome'] = {
			Name = '调息回城',
			Text = '按【，】调息完后可直接传送回我方据点',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10, 
		},
		['OpenBattleScore'] = {
			Name = '记分板',
			Text = '按【F6】可以打开战场记分板查看战场人员信息',
			DelayShowTime = 0.0,
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 10,
			AutoCompleteType = EAutoCompleteByUIOpen,
			AutoCompleteUI = 'BattleScore', 
		},
		['BuyEquipInBattle'] = {
			Name = '购买装备',
			OnlyIndicator = true,
			IndicatorText = '右键点击想买的装备进行购买',
			DelayShowTime = 0.0, 
		},
		['UpgradeSkillInBattle'] = {
			Name = '升级技能',
			OnlyIndicator = true,
			IndicatorText = '点击此处升级战魂技能',
			DelayShowTime = 0.0, 
		},
		
		---------------
		['TitanGuide'] = {
			Name = '泰坦条按钮引导',
			Text = '查看信息栏设置界面',
			DelayShowTime = 0.0, 
		 },

		['LootGuide'] = {
			Name = '拾取物品',
			Text = '靠近掉落的物品按Ｆ键拾取',
			DelayShowTime = 0.0, 
		 },
		['MoDao'] = {
			Name = '磨刀',
			Text = '按Ｙ键使用磨刀石打磨武器',
			DelayShowTime = 0.0, 
		 },
		['LeftJA'] = {
			Name = '左键JA',
			Text = '成功使用一次左键JA',
			DelayShowTime = 0.0, 
		 },
		['ActiveCimelia'] = {
			Name = '激活宝物',
			Text = '激活一个宝物',
			DelayShowTime = 0.0, 
		 },

		['SkillUpgrade'] = {
			Name = '技能升级',
			Text = '升级一次技能',
			DelayShowTime = 0.0, 
		 },

		 ['ModeChange'] = {
			Name = '模式切换',
			Text = 'F10，F11，F12键可在不同操作模式切换',
			DelayShowTime = 0.0, 
		 },
		['Fishing'] = {
			Name = '垂钓教学',
			Text = '学习垂钓技能',
			DelayShowTime = 0.0,
		},
		['Identify'] = {
			Name = '道具鉴定',
			Text = '完成一次道具鉴定',
			DelayShowTime = 0.0,
		},
		['Strengthen'] = {
			Name = '道具强化',
			Text = '完成一次道具强化',
			DelayShowTime = 0.0,
		},
		['Evolution'] = {
			Name = '道具进化',
			Text = '完成一次道具进化',
			DelayShowTime = 0.0,
		},
		['Compound'] = {
			Name = '道具突破',
			Text = '完成一次道具突破',
			DelayShowTime = 0.0,
		},
		['Pharmacy'] = {
			Name = '制药教学',
			Text = '完成一次制药',
			DelayShowTime = 0.0,
		},
		['Cook'] = {
			Name = '烹饪教学',
			Text = '完成一次烹饪',
			DelayShowTime = 0.0,
		},
		['GuardVideoGuide'] = {
			Name = '格挡教学',
			Text = '按下[image="set:Total_Tutorial image:Shift_2"]键进行格挡',
			DelayShowTime = 0.0,
		},
		['EmergencyEvadeVideoGuide'] = {
			Name = '紧急闪避教学',
			Text = '在受击状态下进行闪避',
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 15,
			DelayShowTime = 0.0,
		},
		['QinggongAttackVideoGuide'] = {
			Name = '轻功衔接技教学',
			Text = '轻功中按Tab键攻击',
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 13,
			DelayShowTime = 0.0,
		},
		['FinalAttackVideoGuide'] = {
			Name = '终结技教学',
			Text = '目标处于受控状态时按下“F”键',
			AutoCompleteType = EAutoCompleteByTime,
			AutoCompleteTime = 15,
			DelayShowTime = 0.0,
		},
		['StardustVideoGuide'] = {
			Name = '吸星教学',
			Text = '按下“G”键使用吸星',
			DelayShowTime = 0.0,
		},
		['ModeSelect'] = {
			Name = '模式选择教学',
			Text = '选择一个操作模式',
			DelayShowTime = 0.0,
		},
		['CimeliaGuide'] = {
			Name = '打开宝物界面',
			OnlyIndicator = true,
			IndicatorText = '宝物都在收集界面中查看，按这里可以快速打开宝物收集界面',
			DelayShowTime = 0.0,
		},
		['RandomLevel'] = {
			Name = '随机副本',
			OnlyIndicator = true,
			DelayShowTime = 0.0,
		},
		['ExpandLevel'] = {
			Name = '扩展等级',
			OnlyIndicator = true,
			DelayShowTime = 0.0,
		},
		['JAVideoGuide'] = {
			Name = 'JA视频教学',
			DelayShowTime = 0.0,
		},
		['EnterList'] = {
			Name = '进入队列',
			OnlyIndicator = true,
			IndicatorText = '点击进入队列开始排队，等待自动组队进入选中副本',
			DelayShowTime = 0.0,
		},
		['CimeliaGuide2'] = {
			Name = '宝物开光引导',
			OnlyIndicator = true,
			IndicatorText = '宝物都在收集界面中查看，按这里可以快速打开宝物收集界面',
			DelayShowTime = 0.0,
		},
	}
	-- register them to tutorial module
	tutorialModule['GuideCfgs'] = guide_list
	
end
