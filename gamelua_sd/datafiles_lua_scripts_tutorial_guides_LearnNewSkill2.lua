
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	self.skillBookName = 'SkillBook'

	self.nStartFrame = 0
	
	self.SkillBtns = 
	{
	[WM_PROF_XIANGLONG]      = 'SkillBook/KongfuBG/SchoolSkillBG/AutoHighBG/tempelate/AutoHighBG/template5/skill__auto_dragcontainer__',
	[WM_PROF_BAIZHANG]      = 'SkillBook/KongfuBG/SchoolSkillBG/AutoHighBG/tempelate/AutoHighBG/template5/skill__auto_dragcontainer__',
	[WM_PROF_YAOGUANG] = 'SkillBook/KongfuBG/SchoolSkillBG/AutoHighBG/tempelate/AutoHighBG/template7/skill__auto_dragcontainer__',
	[WM_PROF_JIYING] = 'SkillBook/KongfuBG/SchoolSkillBG/AutoHighBG/tempelate/AutoHighBG/template4/skill__auto_dragcontainer__',
	}
	
	self.QtePositions = 
	{
	[WM_PROF_XIANGLONG] = {x = 260, y = 32},
	[WM_PROF_BAIZHANG] = {x = 260, y = 32},
	[WM_PROF_YAOGUANG] = {x = 260, y = 32},
	[WM_PROF_JIYING] = {x = 260, y = 32},
	}
	
	local skillBtnSpec = indicatorFlow.createIndicatorSpec()
	skillBtnSpec.frameName     = 'skillBtnSpec'
	skillBtnSpec.frameType     = indicatorFlow.INDICATOR_FRAME_UP
	skillBtnSpec.frameText     = '点击技能图标选中技能'
	skillBtnSpec.triggerWin    = nil
	skillBtnSpec.attachWin     = nil
	skillBtnSpec.attachWinRoot = 'SkillBook'
	skillBtnSpec.priority      = 0
	
	local qteBtnSpec = indicatorFlow.createIndicatorSpec()
	qteBtnSpec.frameName     = 'qteBtnSpec'
	qteBtnSpec.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	qteBtnSpec.frameText     = '点击技能图标上的”+“按钮可以学习新技能'
	qteBtnSpec.triggerWin    = 'SkillBook/Improve/Zhaoshi'
	qteBtnSpec.attachWin     = 'SkillBook/Improve/Zhaoshi'
	qteBtnSpec.attachWinRoot = 'SkillBook'
	qteBtnSpec.attachOffset  = nil
	qteBtnSpec.triggerFunc      = self.OnQteBtnTriggered
	qteBtnSpec.triggerFuncParam = self
	qteBtnSpec.priority      = 1
	
	self.skillBtnSpec = skillBtnSpec
	self.qteBtnSpec   = qteBtnSpec
	
	self.indicatorSpecs = {
		skillBtnSpec, qteBtnSpec,
	}
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	local unit = GetPlayersUnit()
	if not unit or not unit.GetProfession then
		lout('LearnNewSkill2:Enter - GetProfession failed')
		return
	end
	
	local eProf = unit:GetProfession()
	self.skillBtnSpec.attachWin = self.SkillBtns[eProf]
	self.skillBtnSpec.triggerWin = self.skillBtnSpec.attachWin
	
	self.qteBtnSpec.attachOffset = self.QtePositions[eProf]
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)

	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end

	self.nStartFrame = self.nStartFrame + 1
	if self.nStartFrame < 5 then
		if self.nStartFrame == 1 then
			SD.GenericFunc('ShowCWindow', self.skillBookName)
		end
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
	
	local nSkillCount = SD.GetAllCanLearnSkillCount()
	if nSkillCount == 0 then
		self.completed = true
	end
end

function OnQteBtnTriggered(self, indicator)
	
	return false
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

