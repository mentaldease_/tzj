local modname = ...

module(modname, package.seeall)

-- 气泡的类型
INDICATOR_FRAME_INVAILD = 0
INDICATOR_FRAME_UP    = 1
INDICATOR_FRAME_DOWN  = 2
INDICATOR_FRAME_LEFT  = 3
INDICATOR_FRAME_RIGHT = 4

INDICATOR_FRAME_LEFT_UP    = 5
INDICATOR_FRAME_LEFT_DOWN  = 6
INDICATOR_FRAME_RIGHT_UP   = 7
INDICATOR_FRAME_RIGHT_DOWN = 8

-- 气泡挂接的类型
INDICATOR_ATTACH_NORMAL_WINDOW = 1   -- 气泡挂接到普通窗口
INDICATOR_ATTACH_BAGITEM       = 2   -- 气泡挂接到背包中的格子

-- 所有类型的气泡定义（UI窗口，动画名，放置方位）
local IndicatorFrameTypes = {
	[INDICATOR_FRAME_UP] = {
		frameClass = 'UpIndicatorFrame',
		animName   = 'UP',
		winDir     = UiUtility.PUT_BOTTOM,
	},
	[INDICATOR_FRAME_DOWN] = {
		frameClass = 'DownIndicatorFrame',
		animName   = 'DOWN',
		winDir     = UiUtility.PUT_TOP,
	},
	[INDICATOR_FRAME_LEFT] = {
		frameClass = 'LeftIndicatorFrame',
		animName   = 'LEFT',
		winDir     = UiUtility.PUT_RIGHT,
	},
	[INDICATOR_FRAME_RIGHT] = {
		frameClass = 'RightIndicatorFrame',
		animName   = 'RIGHT',
		winDir     = UiUtility.PUT_LEFT,
	},
	[INDICATOR_FRAME_LEFT_UP] = {
		frameClass = 'LeftUpIndicatorFrame',
		animName   = 'LEFT_UP',
		winDir     = UiUtility.PUT_BOTTOM,
	},
	[INDICATOR_FRAME_LEFT_DOWN] = {
		frameClass = 'LeftDownIndicatorFrame',
		animName   = 'LEFT_DOWN',
		winDir     = UiUtility.PUT_TOP,
	},
	[INDICATOR_FRAME_RIGHT_UP] = {
		frameClass = 'RightUpIndicatorFrame',
		animName   = 'RIGHT_UP',
		winDir     = UiUtility.PUT_RIGHT,
	},
	[INDICATOR_FRAME_RIGHT_DOWN] = {
		frameClass = 'RightDownIndicatorFrame',
		animName   = 'RIGHT_DOWN',
		winDir     = UiUtility.PUT_LEFT,
	},
}

-- 构造一个气泡的规格说明
function createIndicatorSpec()
	local spec = {
		frameName     = nil,            -- 该气泡指示器的名字
		frameType     = INDICATOR_FRAME_INVAILD, -- 气泡的类型
		frameText     = nil,            -- 气泡显示的位置
		frameWidth    = 107,            -- 气泡宽度
		attachFunc    = nil,            -- 挂接后的自定义回调函数
		attachFuncParam = nil,          -- 挂接后的自定义回调函数的参数
		triggerFunc   = nil,            -- 自定义触发条件判断函数
		triggerFuncParam = nil,         -- 自定义触发条件判断的参数
		triggerWin    = nil,            -- 用来判断UI点击事件是否触发的窗口，由indicatorFlow注册
		triggerKey    = nil,            -- 用来判断热键是否按下的具体热键，由indicatorFlow查询
		attachType    = INDICATOR_ATTACH_NORMAL_WINDOW, -- 挂接类型
		attachItemID  = nil,            -- 如果挂接类型为背包中的道具格子，用该参数指定道具模板ID
		attachWin     = nil,            -- 如果挂接类型为普通窗口，用该参数指定挂接窗口名
		attachWinRoot = nil,            -- 挂接对象的根窗口名字，气泡需要根据它确定放置位置，保证不被其他窗口遮挡
		attachOffset  = nil,            -- attachWin在attachWinRoot的偏移量
		dependWin     = nil,            -- 气泡显示与否需要参考dependWin是否显示（例如，从背包往突破界面放材料，需要突破界面也打开）
		fallbackFunc  = nil,            -- 判断indicatorFlow是否需要回退到之前步骤的函数
		fallbackFuncParam = nil,        -- 判断indicatorFlow是否需要回退到之前步骤的函数参数
		fallbackPri   = 0,              -- 回退点
		priority      = 0,              -- 该气泡的序号
	}
	return spec
end

local function createIndicatorInst(spec)
	local frameType = IndicatorFrameTypes[spec.frameType]
	if not frameType then
		lout('Invalid FrameType '..spec.frameType)
		return nil
	end
	
	local indicator = {
		pFrame        = nil,
		bAttached     = false,
		bTriggered    = false,
		triggerConn   = nil,
		triggerFunc   = nil,
		priority      = spec.priority,
		specInst      = spec,
		OnAfterTriggered  = function(self)
			self.bTriggered = true
			if self.pFrame then self.pFrame:Hide() end
		end,
	}
	indicator.pFrame = ui.createFrame(frameType.frameClass, spec.frameName)
	
	local strText, _ = string.gsub(spec.frameText, "【(%w+)】", " [image='set:Total_Tutorial image:%1'] ")
	indicator.pFrame.pWindow:setText(strText)
	lout('Indicator: Create '..frameType.frameClass ..'_' .. spec.frameName..'Priority: ' ..spec.priority) 
	return indicator
end

-- 控制气泡在教程Update中的显示与否
local function toggleIndicatorVisible(indicator)
	local spec = indicator.specInst
	local bVisible = true
	
	if spec.dependWin then
		local pDependWin = InitWindowPtr(spec.dependWin)
		if pDependWin then
			bVisible = pDependWin:isVisible(true)
		end
	end
	
	if bVisible then
		if spec.attachType == INDICATOR_ATTACH_BAGITEM then
			local pBagItemWin = SD.GetFirstBagItemButtonContainer(spec.attachItemID)
			if not pBagItemWin then
				bVisible = false
			end
		end
	end
	--lout(spec.frameName..' '..tostring(bVisible))
	indicator.pFrame.pWindow:setVisible(bVisible)
end

-- 挂接气泡
local function attachIndicator(indicator)
	if indicator.bAttached then
		return true
	end
	local spec = indicator.specInst
	local frameType = IndicatorFrameTypes[spec.frameType]
	
	-- 确定挂接的窗口
	if spec.attachType == INDICATOR_ATTACH_BAGITEM then
		local pBagItemWin = SD.GetFirstBagItemButtonContainer(spec.attachItemID)
		if not pBagItemWin then
			lout('Indicator: Cannot find bag item '..spec.attachItemID..' to attach')
			return false
		end
		spec.triggerWin = pBagItemWin:getName():c_str()
		spec.attachWin = pBagItemWin:getName():c_str()
		spec.attachWinRoot = 'Root/PackageFrame'
	end
	
	if not spec.triggerWin then
		lout('spec.triggerWin is nil')
		return
	end
	
	if not spec.attachWin then
		lout('spec.attachWin is nil')
		return
	end
	
	if not spec.attachWinRoot then
		lout('spec.attachWinRoot is nil')
		return
	end
	
	local pAttachWin = InitWindowPtr(spec.attachWin)
	local pAttachWinRoot = InitWindowPtr(spec.attachWinRoot)
	local pTriggerWin = nil
	if type(spec.triggerWin) == 'string' then
		pTriggerWin = InitWindowPtr(spec.triggerWin)
	elseif type(spec.triggerWin) == 'table' then
		pTriggerWin = {}
		for k, v in ipairs(spec.triggerWin) do
			local pWin = InitWindowPtr(v)
			table.insert(pTriggerWin, pWin)
		end
	end
	
	lout('Indicator: Attach '..spec.frameName..
		' To '..spec.attachWinRoot..' Related to '..spec.attachWin ..
		' At '..frameType.animName)
		
	if not pAttachWin then 
		lout('Indicator: Attach Target '..spec.attachWin..' not found')
		return false
	end
	if not pAttachWin:isVisible() then
		lout('Indicator: Attach Target '..spec.attachWin..' not visible')
		return false
	end
	
	if not pAttachWinRoot then 
		lout('Indicator: Attach Target Root '..spec.attachWinRoot..' not found')
		return false
	end
	if not pAttachWinRoot:isVisible() then
		lout('Indicator: Attach Target Root '..spec.attachWinRoot..' not visible')
		return false
	end
	
	lout('Indicator: Attaching, set width')
	indicator.pFrame.pWindow:setProperty('AutoWidth', 'False')
	indicator.pFrame.pWindow:setWidth(CEGUI.UDim(0, spec.frameWidth))
	
	-- 确定挂接的位置
	-- Calculate the indicator's position
	-- 1. Compute the position relate to pAttachWin as uvWinPos
	-- 2. Compute the position relate to Screen as vWinPos using uvWinPos
	-- 3. Compute the position relate to pAttachWinRoot as vPosInRoot using vWinPos
	local indiSize = indicator.pFrame.pWindow:getPixelSize()
	
	local uvWinPos = nil
	if frameType.winDir == UiUtility.PUT_TOP then
		uvWinPos = CEGUI.UVector2(CEGUI.UDim(0.5, -(indiSize.width/2) ),CEGUI.UDim(0.0, -(indiSize.height) ))
	
	elseif frameType.winDir == UiUtility.PUT_BOTTOM then
		uvWinPos = CEGUI.UVector2(CEGUI.UDim(0.5, -(indiSize.width/2) ),CEGUI.UDim(1.0, 0))
	
	elseif frameType.winDir == UiUtility.PUT_LEFT then
		uvWinPos = CEGUI.UVector2(CEGUI.UDim(0.0, -(indiSize.width) ),CEGUI.UDim(0.5, -(indiSize.height/2) ))
					
	elseif frameType.winDir == UiUtility.PUT_RIGHT then
		uvWinPos = CEGUI.UVector2(CEGUI.UDim(1.0, 0.0),CEGUI.UDim(0.5, -(indiSize.height/2) ))
	end
	lout('Indicator: WinDir '..tostring(frameType.winDir)..tostring(uvWinPos))
	
	if spec.attachOffset then
		local attachWinSize = pAttachWin:getPixelSize()
		uvWinPos.x.offset = spec.attachOffset.x - uvWinPos.x.scale * attachWinSize.width
		uvWinPos.y.offset = spec.attachOffset.y - uvWinPos.y.scale * attachWinSize.height
	end
	lout('Indicator: Apply Offset '..tostring(frameType.winDir)..tostring(uvWinPos))
		
	local vWinPos  = CEGUI.CoordConverter:windowToScreen(pAttachWin, uvWinPos)
	lout('Indicator: Pos In Screen {'..vWinPos.x..','..vWinPos.y..'}')
	local vRootPos = 
		CEGUI.CoordConverter:windowToScreen(pAttachWinRoot, CEGUI.UVector2(CEGUI.UDim(0, 0), CEGUI.UDim(0, 0)))
	lout('Indicator: Root Pos In Screen {'..vRootPos.x..','..vRootPos.y..'}')
	
	local pTitleBar = InitWindowPtr(spec.attachWinRoot..'__auto_titlebar__')
	if pTitleBar then
		local titleBarSize = pTitleBar:getPixelSize()
		vRootPos.y = vRootPos.y + titleBarSize.height
		vRootPos.x = vRootPos.x + 10
	end
	
	local uvPosInRoot = 
		CEGUI.UVector2(CEGUI.UDim(0, vWinPos.x - vRootPos.x), CEGUI.UDim(0, vWinPos.y - vRootPos.y))
	
	indicator.pFrame:Show()
	
	-- Set the position of indicator and other properties
	lout('Indicator: Attaching, set properties')
	local pIndicatorWin = indicator.pFrame.pWindow
	pIndicatorWin:setClippedByParent(false)
	pIndicatorWin:setAlwaysOnTop(true)
	pIndicatorWin:setHorizontalAlignment(CEGUI.HA_LEFT)
	pIndicatorWin:setVerticalAlignment(CEGUI.VA_TOP)
	pAttachWinRoot:addChildWindow(pIndicatorWin)
	
	lout('Indicator: Attaching, set position')
	pIndicatorWin:setPosition(uvPosInRoot)
	
	-- 注册触发响应函数
	lout('Indicator: Attaching, Register Trigger Function')
	-- Register Trigger Function
	indicator.triggerFunc = function (self, evtArgs)
		lout('Indicator: Indicator '..self.specInst.frameName..' triggered by event')
		
		-- 外界条件触发后，不一定代表教程真的完成了，教程还有一次机会判断是否真正完成。
		-- 例如装备突破教程，点击突破后，可能突破成功，也可能突破失败，进入该函数，
		-- 只代表突破按钮被点击了，但并不知道是否突破成功。因此，教程自身还可以判断一次
		-- 是否真正满足了完成的条件.
		local bRealTriggered = true
		if self.specInst.triggerFunc then
			lout('Indicator: Call custom triggerFunc')
			local triggerResult = self.specInst.triggerFunc(self.specInst.triggerFuncParam, self)
			if triggerResult == false then 
				bRealTriggered = false
			end
		end
		
		if bRealTriggered then
			self:OnAfterTriggered()
		end
	end
	if type(pTriggerWin) == 'table' then
		indicator.triggerConn = {}
		for k, win in ipairs(pTriggerWin) do
			local conn = win:subscribeEvent('MouseClick', indicator.triggerFunc, indicator)
			table.insert(indicator.triggerConn, conn)
		end
	else
		indicator.triggerConn = pTriggerWin:subscribeEvent('MouseClick', indicator.triggerFunc, indicator)
	end
	
	lout('Indicator: Attaching, Add Animation')
	-- Add Animation
	local animName = guideMgr.indicator_animations[frameType.animName]
	SD.WndAnimManager:Instance():CreateAnim(
			indicator.pFrame.pWindow, animName, false, true, true)
	
	if indicator.specInst.attachFunc then
		lout('Indicator: Call custom attachFunc')
		indicator.specInst.attachFunc(indicator.specInst.attachFuncParam, indicator)
	end
		
	indicator.bAttached = true
	return true
end

function createIndicatorFlow(indicator_spec)
	local indicator_flow = {
		bDone = false,
		bFailed = false,
		priorities = {},
		priority_index = 1,
		-- ['1'] = {...},
		-- ['pri_1_attached'] = true/false,
		-- ['pri_1_triggered'] = true/false,
		-- ...
	}
	
	lout('Indicator: Create Indicator Flow ')
	
	indicator_flow.bDone = false
	indicator_flow.priorities = {}
	for i, v in ipairs(indicator_spec) do
		local indicatorInst = createIndicatorInst(v)
		if indicatorInst then
			if not indicator_flow[tostring(v.priority)] then
				indicator_flow[tostring(v.priority)] = {}
				indicator_flow['pri_' ..tostring(v.priority)..'_attached'] = false
				indicator_flow['pri_' ..tostring(v.priority)..'_triggered'] = false
				
				table.insert(indicator_flow.priorities, v.priority)
			end
			table.insert(indicator_flow[tostring(v.priority)], indicatorInst)
		end
	end
	
	table.sort(indicator_flow.priorities)
	
	return indicator_flow
end

function updateIndicatorFlow(indicator_flow)
	if not indicator_flow or indicator_flow.bDone then
		return
	end
	
	--lout('Indicator: Update Indicator Flow ')
	local pGameInput = SD.CHSGameInput:Instance()
		
		
	local priority = indicator_flow.priorities[indicator_flow.priority_index]
	
	local priorityKey = tostring(priority)
	local attachedKey = 'pri_' ..priorityKey..'_attached'
	local triggeredKey = 'pri_' ..priorityKey..'_triggered'
	
	local indicators = indicator_flow[priorityKey]
	if indicator_flow[attachedKey] == false then
		-- Attach
		local bAllAttached = true
		lout('Indicator: Attach Indicators with priority '..priorityKey..' size: ' ..(#indicators))
		for i, v in ipairs(indicators) do
			local ret = attachIndicator(v)
			if not ret then bAllAttached = false end
		end
		indicator_flow[attachedKey] = bAllAttached
	else
		-- Check whether need fallback
		local bNeedFallback, minFallbackPri = shouldFlowFallback(indicator_flow, priorityKey)
		
		-- Do fallback
		if bNeedFallback then
			local bSuccess = fallbackIndicatorFlow(indicator_flow, minFallbackPri)
			if bSuccess then return end
		end
		
		-- Check Triggered
		local bAllTriggered = true
		for i, v in ipairs(indicators) do
			if v.specInst.triggerKey and not v.bTriggered then
				-- 判断按键触发
				local bTriggered = false
				if type(v.specInst.triggerKey) == 'string' then
					bTriggered = pGameInput:IsActionTriggered(v.specInst.triggerKey)
				elseif type(v.specInst.triggerKey) == 'table' then
					for i, _key in ipairs(v.specInst.triggerKey) do 
						bTriggered = pGameInput:IsActionTriggered(_key)
						if bTriggered then
							break
						end
					end
				end
				if bTriggered then v:OnAfterTriggered() end
			end
			if not v.bTriggered then
				bAllTriggered = false
				toggleIndicatorVisible(v)
			end
		end
		-- Move to Next
		if bAllTriggered then
			lout('Indicator: Indicators with priority '..priorityKey ..' all triggered, increase priority')
			indicator_flow.priority_index = indicator_flow.priority_index + 1
		end
	end
	
	-- Check if done
	if indicator_flow.priority_index > table.getn(indicator_flow.priorities) then
		lout('Indicator: Indicator Flow all triggered, Done!')
		indicator_flow.bDone = true
	end
end

-- 判断是否需要回退
function shouldFlowFallback(indicator_flow, cur_pri_key)
	local bNeedFallback = false
	local minFallbackPri = 100
	
	local indicators = indicator_flow[cur_pri_key]
	
	for i, v in ipairs(indicators) do
		if v.specInst.fallbackFunc then
			local bRes = v.specInst.fallbackFunc(v.specInst.fallbackFuncParam, v)
			if bRes then 
				bNeedFallback = true
				if v.specInst.fallbackPri and minFallbackPri > v.specInst.fallbackPri then
					minFallbackPri = v.specInst.fallbackPri
				end
			end
		end
	end
	--lout('Indicator: Need to fallback to priority ' .. minFallbackPri)
	return bNeedFallback, minFallbackPri
end

-- 执行回退处理
function fallbackIndicatorFlow(indicator_flow, minFallbackPri)
	lout('Indicator: Try to fallback to priority ' .. minFallbackPri)
	local fallbackPriIndex = nil
	
	for i=1, table.getn(indicator_flow.priorities) do
		if indicator_flow.priorities[i] == minFallbackPri then
			fallbackPriIndex = i
			break
		end
	end

	if not fallbackPriIndex then
		lout('Indicator: Fallback to priority ' .. minFallbackPri .. ' failed, cannot find priority index')
		return false
	end
	
	for i = fallbackPriIndex, indicator_flow.priority_index do
		local pri = indicator_flow.priorities[i]
		if pri then
			indicator_flow['pri_' ..tostring(pri)..'_attached'] = false
			indicator_flow['pri_' ..tostring(pri)..'_triggered'] = false
			local list = indicator_flow[tostring(pri)] 
			for _, v in ipairs(list) do
				v.bAttached = false
				v.bTriggered = false
				v.pFrame:Hide()
			end
		end
	end
	indicator_flow.priority_index = fallbackPriIndex
	lout('Indicator: Finished to fallback to priority ' .. minFallbackPri)
	return true
end

function destroyIndicatorFlow(indicator_flow)
	lout('Indicator: Destroy Indicator Flow ')
	if not indicator_flow then return end
	
	local priorities = indicator_flow.priorities
	
	for i, pri in ipairs(priorities) do
		local priorityKey = tostring(pri)
		local indicators = indicator_flow[priorityKey]
		
		for _, v in ipairs(indicators) do
			if v.pFrame then
				ui.destroyFrame(v.pFrame)
				v.pFrame = nil
			end
			if v.triggerConn then
				if type(v.triggerConn) == 'table' then
					for _, conn in ipairs(v.triggerConn) do
						conn:disconnect()
					end
				else
					v.triggerConn:disconnect()
				end
				v.triggerConn = nil
			end
		end
	end
end

-- 回退判断依据的模板函数
function getFallbackFunc_UIDependency(winName, visibleState)
	return function (self, indicator)
		local pWin = InitWindowPtr(winName)
		if pWin then
			local bVisible = pWin:isVisible(true)
			return bVisible == visibleState
		end
		return false
	end
end
