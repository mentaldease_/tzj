
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local startKaiGuang = indicatorFlow.createIndicatorSpec()
	startKaiGuang.frameName     = 'startKaiGuang'
	startKaiGuang.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	startKaiGuang.frameText     = '点这里的开光按钮开始进行宝物开光！'
	startKaiGuang.triggerWin    = 'Journey/CimeliaBG/Pane/Container/Templete_0/Kaiguang/Btn'
	startKaiGuang.attachWin     = 'Journey/CimeliaBG/Pane/Container/Templete_0/Kaiguang/Btn'
	startKaiGuang.attachWinRoot = 'Journey'
	startKaiGuang.attachFunc    = nil
	startKaiGuang.attachFuncParam = nil
	startKaiGuang.priority      = 1

	self.indicatorSpecs = {
		startKaiGuang, 
	}
	
	self.indicatorFlows = nil
	
	-- Server Completion
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	-- Create Indicator
	self.downIndicator = ui.createFrame('RightIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.downIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
	
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then
		return
	end
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end
