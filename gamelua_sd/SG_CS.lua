
STake_Start("SG_CS_000","SG_CS_000")  --空中挂着
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("SG_CS_001","SG_CS_001")  --被人抱着跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("SG_CS_002","SG_CS_002")  --被人抱着跳
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


--试剑崖CUTSCENE用动作

--CS2坐白雕下山

--张阳配

STake_Start("CS_SJY_BDXS_002_SG","SG_CS_BDXS_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_SJY_BDXS_003_SG","SG_CS_BDXS_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_SJY_jump_SG","HG_QG_move_014(0,22)(23,(40,60))")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.9999,2)

STake_Start("CS_SJY_jump01_SG","SG_QG_move_014(0,22)(23,(40,60))")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.9999,2)

STake_Start("CS_SJY_stand_SG","SG_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_SJY_run_SG","SG_QG_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_SJY_look_SG","SG_emo_002")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.22,4)
    PlaySpeed(0,4,0.5) 
    Loop(3.9999,4)

STake_Start("CS_SJY_look01_SG","SG_CS_BDXS_001")
    BlendMode(0)
    BlendTime(0.2)
    PlaySpeed(0,3.1343,0.9)  
    Loop(3.1342,3.1343)

------------------------------------------------------------------------------------
--马贼王出场CUTSCENE用动作        

--于震轩配


STake_Start("CS_CS_SWGC_NB_WOMAN_wait001","CS_SWGC_NB_WOMAN_wait001")
    BlendMode(0)
    BlendTime(0.4) 
    Loop()

STake_Start("CS_SWGC_NB_WOMAN_emo001_","CS_SWGC_NB_WOMAN_emo001")
    BlendMode(0)
    BlendTime(0.4) 
    FrameTime(0,0.575)
    PlaySpeed(0,0.575,0.75)
    Loop(0.574,0.575)

STake_Start("CS_SWGC_NB_WOMAN_emo001_part","CS_SWGC_NB_WOMAN_emo001")
    BlendMode(0)
    BlendTime(0.4) 
    FrameTime(0.43,2.133)
    Loop(2.132,2.133)

STake_Start("CS_SWGC_NB_WOMAN_taitou","CS_SWGC_NB_WOMAN_emo001")
    BlendMode(0)
    BlendTime(0.4) 
    FrameTime(1.4,2.133) 
    Loop(2.132,2.133)

--蟠龙府邸CUTSCENE用动作

--CS1西毒施毒

--张阳配

STake_Start("CS_PLFD_wait_000_SG","CS_PLFD_SG_GB_wait_001")
    BlendMode(0)
    BlendTime(0.4) 
    Loop(0)

STake_Start("CS_PLFD_gedang_01_SG","SG_GB_PA_def_wm")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_back_001_SG","SG_C_back_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_back_002_SG","SG_C_back_002")
    BlendMode(0)
    BlendTime(0) 
    FrameTime(0,0.0001)
    Loop(0,0.0001)

STake_Start("CS_PLFD_back_001_loop_SG","SG_C_back_001_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_HAJJL_emo_duobi_SG","CS_PLFD_SG_GB_emo_001")
    BlendMode(0)
    BlendTime(0.4) 
    Loop(3.4659,3.466)

STake_Start("CS_PLFD_HAJJL_emo_zhengtuo_SG","CS_PLFD_SG_GB_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(4.1579,4.158)

STake_Start("CS_PLFD_HAJJL_emo_taopao1_SG","CS_PLFD_SG_GB_emo_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_HAJJL_emo_taopao2_SG","CS_PLFD_SG_GB_emo_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_huanshi_SG","SG_CS_BDXS_001")
    BlendMode(0)
    BlendTime(0) 
    Loop(3.1342,3.1343)

STake_Start("CS_PLFD_gedang_02_SG","SG_CS_emo_dang_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.5175,1.5176)

STake_Start("CS_PLFD_gedang_03_SG","SG_CS_emo_xr_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.9999,1)

STake_Start("CS_PLFD_look_stop_SG","SG_emo_002")
    BlendMode(0)
    BlendTime(0)
    FrameTime(0.22,0.2201)
    Loop(0.22,0.2201)
------------------------------------
--牛家村开场
--于震轩配


STake_Start("CS_NJC_move-wait_SG","PC_SG_CS_move-wait_000")
    BlendMode(0)
    BlendTime(0.1) 
    PlaySpeed(0,2.658,0.6)
    Loop()

-----------------------------------------------------------------------------------
--赵王府预览

--王林配

STake_Start("CS_life_001_N1_CM03_SG","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ZWF_NB_SG_emo_001_SG","SG_ZWF_NB_SG_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ZWF_NB_SG_emo_002_SG","SG_ZWF_NB_SG_emo_002")
    BlendMode(0)
    BlendTime(0)    
    FrameTime(1,3.3)                                 
    Loop()

STake_Start("CS_ZWF_NB_SG_emo_002_wait_SG","SG_wait_000")
    BlendMode(0)
    BlendTime(0) 
    Loop()

STake_Start("CS_ZWF_NB_SG_emo_002_loop_SG","SG_ZWF_NB_SG_emo_002")
    BlendMode(0)
    BlendTime(0)  
    FrameTime(0,0.0001)                                 
    Loop()

STake_Start("CS_ZWF_NB_SG_emo_003_SG","SG_ZWF_NB_SG_emo_003")
    BlendMode(0)
    BlendTime(0)                                  
    Loop()

STake_Start("CS_ZWF_NB_SG_wait_001_SG","SG_ZWF_NB_SG_wait_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_SG_QRZWF_001_SG","SG_CS_QRZWF_001")
    BlendMode(0)
    BlendTime(0.2) 
    PlaySpeed(0,1.86,0.7)

STake_Start("jump_start","SG_WS_move_004_start")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("SG_WS_jump_004","SG_WS_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("HG_QG_run_004_endpose3","SG_move_014(0,22)(23,51)(53,72)_2")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("endpose6","SG_move_014(0,25)(26,(50,90))(92,112)_3")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()