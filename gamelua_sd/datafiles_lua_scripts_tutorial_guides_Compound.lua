
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	
	local openBag = indicatorFlow.createIndicatorSpec()
	openBag.frameName     = 'openBag'
	openBag.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openBag.frameText     = '点击打开包裹进行装备突破'
	openBag.attachFunc    = self.OnOpenBagAttached
	openBag.attachFuncParam = self
	openBag.triggerFunc   = nil
	openBag.triggerFuncParam = nil
	openBag.triggerWin    = 'Root/MainMenu/PackageBtn'
	openBag.attachWin     = 'Root/MainMenu/PackageBtn'
	openBag.attachWinRoot = 'MainMenu_FunctionAndEXP'
	openBag.triggerKey    = 'PackageUI'
	openBag.priority      = 1
	
	local openBagGeneralPage = indicatorFlow.createIndicatorSpec()
	openBagGeneralPage.frameName     = 'openBagGeneralPage'
	openBagGeneralPage.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openBagGeneralPage.frameText     = '点击切换至综合包裹'
	openBagGeneralPage.attachFunc    = self.OnOpenBagGeneralPageAttached
	openBagGeneralPage.attachFuncParam = self
	openBagGeneralPage.triggerFunc   = nil
	openBagGeneralPage.triggerFuncParam = nil
	openBagGeneralPage.triggerWin    = 'Root/PackageFrame/TagContainer1/Btn_0_0'
	openBagGeneralPage.attachWin     = 'Root/PackageFrame/TagContainer1/Btn_0_0'
	openBagGeneralPage.attachWinRoot = 'Root/PackageFrame'
	openBagGeneralPage.priority      = 2
	
	local openMaterialBox = indicatorFlow.createIndicatorSpec()
	openMaterialBox.frameName     = 'openMaterialBox'
	openMaterialBox.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openMaterialBox.frameText     = '右键点击打开材料箱'
	openMaterialBox.attachFunc    = nil
	openMaterialBox.attachFuncParam = nil
	openMaterialBox.triggerFunc   = nil
	openMaterialBox.triggerFuncParam = nil
	openMaterialBox.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	openMaterialBox.attachItemID  = nil
	openMaterialBox.priority      = 3
	
	local openCompoundUI = indicatorFlow.createIndicatorSpec()
	openCompoundUI.frameName     = 'openCompoundUI'
	openCompoundUI.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openCompoundUI.frameText     = '打开突破界面'
	openCompoundUI.attachFunc    = self.OnCompoundUIAttached
	openCompoundUI.attachFuncParam = self
	openCompoundUI.triggerFunc   = nil
	openCompoundUI.triggerFuncParam = nil
	openCompoundUI.triggerWin    = 'Craft_Evolution/Confirm'
	openCompoundUI.attachWin     = 'Craft_Evolution/Confirm'
	openCompoundUI.attachWinRoot = 'Craft_Evolution'
	openCompoundUI.priority      = 4
	
	local openBagEquipPage = indicatorFlow.createIndicatorSpec()
	openBagEquipPage.frameName     = 'openBagEquipPage'
	openBagEquipPage.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openBagEquipPage.frameText     = '点击切换至装备包裹'
	openBagEquipPage.attachFunc    = self.OnOpenBagPageEquipAttached
	openBagEquipPage.attachFuncParam = self
	openBagEquipPage.triggerFunc   = nil
	openBagEquipPage.triggerFuncParam = nil
	openBagEquipPage.triggerWin    = 'Root/PackageFrame/TagContainer1/Btn_1_0'
	openBagEquipPage.attachWin     = 'Root/PackageFrame/TagContainer1/Btn_1_0'
	openBagEquipPage.attachWinRoot = 'Root/PackageFrame'
	openBagEquipPage.priority      = 5

	
	local placeEquipItem = indicatorFlow.createIndicatorSpec()
	placeEquipItem.frameName     = 'placeEquipItem'
	placeEquipItem.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeEquipItem.frameText     = '点击放置需要突破的装备'
	placeEquipItem.attachFunc    = nil
	placeEquipItem.attachFuncParam = nil
	placeEquipItem.triggerFunc   = nil
	placeEquipItem.triggerFuncParam = nil
	placeEquipItem.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeEquipItem.attachItemID  = nil
	placeEquipItem.priority      = 6
	placeEquipItem.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Evolution', false)
	placeEquipItem.fallbackFuncParam = self
	placeEquipItem.fallbackPri   = 4
	
	local placeMaterialItem = indicatorFlow.createIndicatorSpec()
	placeMaterialItem.frameName     = 'placeMaterialItem'
	placeMaterialItem.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeMaterialItem.frameText     = '点击放置材料 '
	placeMaterialItem.attachFunc    = nil
	placeMaterialItem.attachFuncParam = nil
	placeMaterialItem.triggerFunc   = nil
	placeMaterialItem.triggerFuncParam = nil
	placeMaterialItem.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeMaterialItem.attachItemID  = nil
	placeMaterialItem.priority      = 7
	placeMaterialItem.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Evolution', false)
	placeMaterialItem.fallbackFuncParam = self
	placeMaterialItem.fallbackPri   = 4	
		
	local confirmCompund = indicatorFlow.createIndicatorSpec()
	confirmCompund.frameName     = 'confirmCompund'
	confirmCompund.frameType     = indicatorFlow.INDICATOR_FRAME_UP
	confirmCompund.frameText     = '点击开始突破'
	confirmCompund.attachFunc    = nil
	confirmCompund.attachFuncParam = nil
	confirmCompund.triggerFunc   = nil
	confirmCompund.triggerFuncParam = nil
	confirmCompund.triggerWin    = 'Craft_Evolution/Confirm'
	confirmCompund.attachWin     = 'Craft_Evolution/Confirm'
	confirmCompund.attachWinRoot = 'Craft_Evolution'
	confirmCompund.priority      = 8
	confirmCompund.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Evolution', false)
	confirmCompund.fallbackFuncParam = self
	confirmCompund.fallbackPri   = 4	
	
	self.indicatorSpecs = {
		openBag, openBagGeneralPage, openMaterialBox,  openCompoundUI, 
		openBagEquipPage, placeEquipItem, placeMaterialItem, confirmCompund
	}

	self.openMaterialBoxSpec = openMaterialBox
	self.placeEquipItemSpec = placeEquipItem
	self.placeMaterialItemSpec = placeMaterialItem
	
	self.BoxItems = {
		[WM_PROF_XIANGLONG] = '1_1612',
		[WM_PROF_BAIZHANG] = '1_1613',
		[WM_PROF_YAOGUANG] = '1_1610',	
		[WM_PROF_JIYING] = '1_1611',	
	}
	
	self.EquipItems = {
		[WM_PROF_XIANGLONG] = '5_75',
		[WM_PROF_BAIZHANG] = '5_76',
		[WM_PROF_YAOGUANG] = '5_73',	
		[WM_PROF_JIYING] = '5_74',	
	}
	
	self.indicatorFlows = nil
end
function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	local unit = GetPlayersUnit()
	if not unit or not unit.GetProfession then
		lout('Compound:Enter - GetProfession failed')
		self.indicatorFlows = {}
		return
	end
	local eProf = unit:GetProfession()
	self.openMaterialBoxSpec.attachItemID = self.BoxItems[eProf]
	self.placeEquipItemSpec.attachItemID = self.EquipItems[eProf]
	self.placeMaterialItemSpec.attachItemID = self.EquipItems[eProf]
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnOpenBagAttached(self, indicator)
	local pWin = InitWindowPtr('Root/PackageFrame')
	if pWin and pWin:isVisible() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenBagGeneralPageAttached(self, indicator)
	local pBagItemWin = SD.GetFirstBagItemButtonContainer(self.openMaterialBoxSpec.attachItemID)
	if pBagItemWin then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenBagPageEquipAttached(self, indicator)
	local pBagItemWin = SD.GetFirstBagItemButtonContainer(self.placeEquipItemSpec.attachItemID)
	if pBagItemWin then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnCompoundUIAttached(self, indicator)
	indicator.bTriggered = true
	indicator.pFrame:Hide()
end	

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

