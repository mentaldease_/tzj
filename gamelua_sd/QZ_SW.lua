
 
----------------------------------长剑------------------------------------------ 
STake_Start("HG_QZ_SW_wait_000","HG_QZ_SW_wait_000")   --战斗待机
	BlendMode(0)
	BlendTime(0.2)
	Loop()
	BlendPattern("wait_none")

STake_Start("HG_QZ_floating_wait_nonbattle","HG_QZ_Lsword_wait_000")   --非战斗漂浮待机
	BlendMode(0)
	BlendTime(0.2)
	Loop()
	BlendPattern("wait")
	Fx(0,"QZ_Lsword_B_wait_000")

STake_Start("HG_QZ_floating_move","HG_QZ_Lsword_B_move_004") 		--漂浮移动动作
	BlendMode(0)
	BlendTime(0.2)
	Loop()
	Fx(0,"QZ_Lsword_B_wait_000")	

STake_Start("HG_QZ_floating_wait","HG_QZ_Lsword_B_wait_000")
	BlendMode(0)
	BlendTime(0.2)
	Loop()
	BlendPattern("wait")
	Fx(0,"QZ_Lsword_B_wait_000")	

STake_Start("HG_QZ_SW_wait_000_ui","HG_QZ_SW_wait_000")   --战斗待机
	BlendMode(0)
	BlendTime(0)    
	Loop()
	BlendPattern("wait_none")

STake_Start("HG_wait_000_QZSW","HG_wait_000_QZSW")   --一般待机
	BlendMode(0)
	BlendTime(0.2)
	Loop()
	BlendPattern("wait_none") 

STake_Start("HG_wait_000_QZSW_ui","HG_wait_000_QZSW")   --一般待机
	BlendMode(0)
	BlendTime(0)    
	Loop()
	BlendPattern("wait_none")

STake_Start("HG_move_009_QZSW","HG_move_009_QZSW")   --战斗左踏转
	BlendMode(1)
	BlendTime(0.2)
	Loop()

STake_Start("HG_move_010_QZSW","HG_move_010_QZSW")   --战斗右踏转
	BlendMode(1)
	BlendTime(0.2)
	Loop()	
	
STake_Start("HG_QZ_SW_link_000","HG_QZ_SW_link_000")  
	BlendMode(0)
	BlendTime(0.2)
	SkeletonMod(0.1, 0.266, "QZ_SW_WOMAN_2")


STake_Start("HG_QZ_SW_link_001","HG_QZ_SW_link_001")  
	BlendMode(0)
	BlendTime(0.2)
	SkeletonMod(0.1, 0.850, "QZ_SW_WOMAN_1")

STake_Start("HG_emo_000_QZSW","HG_emo_000_QZSW") --表情
	BlendMode(0)
	BlendTime(0.2)


STake_Start("HG_emo_001_QZSW","HG_emo_001_QZSW") --表情
	BlendMode(0)
	BlendTime(0.2)                  


STake_Start("HG_QZ_SW_dodge","HG_QZ_SW_dodge")  		--  全真瞬移闪避（新）
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")   
	BlendPattern("steady")    
	FrameTime(0.066,0.666) 


	Sound(0.070,"char_05_cloth_flap_02")
	Sound(0.070,"BTS_FAN_skill_008_att02")
	Sound(0.070,"N2_BYJQ01_dodge_001")
	Sound(0.070,"HG_SoundVOC_Jump_1")
	GroundFx(0.2, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.417, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
	GroundFx(0.2, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.417, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
	GroundSound(0.01,"Sound_step")
	GroundSound(0.2,"Sound_step") 
	GroundSound(0.417,"Sound_step") 
	EffectStart(0.0,"MB",4)

	--Fx(0.066,"Fx_common_JuseAtt_02")
	Fx(0.066,"FX_move_QG_common")
	Fx(0.066,"FX_QZ_Duoshan","Reference")	
	--Fx(0.066,"QZ_Lsword_baby_02")  
	--Fx(0.066,"QZ_teleport","Spine1",0.6)  
	-- Fx(0.066,"QZ_Lsword_baby_02")  

STake_Start("HG_QZ_SW_jf","HG_QZ_SW_jf")  --前击退
	BlendTime(0.2)
	BlendMode(0)
	Sound(0.070,"char_05_cloth_flap_02")
	Sound(0.03,"HG_SoundVOC_Hurt2_1")

	HurtHard(0,0.5)
	Priority(0.5,"5")
	GroundSound(0.32,"Sound_step")
	GroundSound(0.34,"Sound_step")	
	
STake_Start("HG_QZ_SW_jf_B","HG_QZ_SW_jf_B")  --后击退
	BlendTime(0.2)
	BlendMode(0)
	Sound(0.070,"char_05_cloth_flap_02")
	Sound(0.03,"HG_SoundVOC_Hurt2_1")

	HurtHard(0,0.5)
	Priority(0.5,"5")
	GroundSound(0.32,"Sound_step")
	GroundSound(0.34,"Sound_step")

STake_Start("HG_QZ_SW_jf_L","HG_QZ_SW_jf_L")  --左击退
	BlendTime(0.2)
	BlendMode(0)
	Sound(0.070,"char_05_cloth_flap_02")
	Sound(0.03,"HG_SoundVOC_Hurt2_1")

	HurtHard(0,0.5)
	Priority(0.5,"5")
	GroundSound(0.32,"Sound_step")
	GroundSound(0.34,"Sound_step")

STake_Start("HG_QZ_SW_jf_R","HG_QZ_SW_jf_R")  --右击退
	BlendTime(0.2)
	BlendMode(0)
	Sound(0.070,"char_05_cloth_flap_02")
	Sound(0.03,"HG_SoundVOC_Hurt2_1")

	HurtHard(0,0.5)
	Priority(0.5,"5")
	GroundSound(0.32,"Sound_step")
	GroundSound(0.34,"Sound_step")	
	

STake_Start("HG_QZ_SW_SS","HG_QZ_SW_SS")    --受身     13051
	BlendMode(0)
	BlendTime(0.03)
	Priority(0,"1")     
	Priority(0.38,"5")
	BlendPattern("steady") 	
	Sound(0.070,"N2_BYJQ01_dodge_001")
	Sound(0.03,"HG_SL_SB_Skill_006")
	Sound(0.03,"HG_SL_SB_Skill_012_1")
	Sound(0.070,"HG_SoundVOC_Jump_1")

	Fx(0,"HG_GB_QZ_ss_a","Reference",1,0,0,0,0,0,0,0,1) 
	Fx(0,"HG_ALL_ss_b","Reference",1,0,0,0,0,0,0,0,1) 


STake_Start("HG_QZ_SW_getup_air","HG_QZ_SW_PY_move_004_F+++")  		--空中受身  13052
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"1") 
	Priority(0.1,"5") 	
	BlendPattern("steady")    

	Charge(0,0.1,100,180,14)	
	Sound(0.010,"N2_BYJQ01_dodge_001")
	Sound(0.013,"HG_SL_SB_Skill_012_1")
	Sound(0.010,"HG_SoundVOC_Jump_1")
	GroundFx(0.2, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.417, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
	GroundFx(0.2, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.417, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
	GroundSound(0.01,"Sound_step")
	GroundSound(0.2,"Sound_step") 
	GroundSound(0.417,"Sound_step") 
	EffectStart(0.0,"MB",4)
	--Fx(0.066,"Fx_common_JuseAtt_02")
	Fx(0.066,"FX_move_QG_common")
	--Fx(0.066,"FX_QZ_Duoshan")  
	--Fx(0.066,"QZ_teleport","Spine1",0.6)  
	 Fx(0.066,"FX_QZ_Duoshan_2")  
	ChangeMaterial(0,0.3,"/PC/Material/yinshen")

--------------------------------------------------------------------------

STake_Start("HG_QZ_SW_QG_move_004_start","HG_QZ_SW_QG_move_004_start")    --轻功前加速跑起步
	BlendTime(0.05)
	BlendMode(0)
	Fx(0.066,"FX_move_QG_common")
	Fx(0.066,"FX_QZ_Duoshan","Reference")	
	Fx(0,"FX_move_QG_fire","Reference") 	
	GroundFx(0.18, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.34, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
	GroundFx(0.18, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.34, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      
	EffectStart(0, "MB",0)

	Sound(0.08,"char_05_cloth_flap_01")
	Sound(0.08,"QZ_SW_Skill_000_FX")
	GroundSound(0.18,"Sound_step")
	GroundSound(0.34,"Sound_step")

STake_Start("HG_QZ_SW_QG_move_004_end","HG_QZ_SW_QG_move_004_end")    --轻功前加速跑刹车
	BlendTime(0.02)
	BlendMode(0)
	GroundFx(0.1, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.52, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
	GroundFx(0.1, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.52, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      
	GroundSound(0.090,"Sound_brake")
	GroundSound(0.1,"Sound_step")
	GroundSound(0.52,"Sound_step")
	EffectStart(0, "FOV", 0)

STake_Start("HG_QZ_SW_QG_move_004","HG_QZ_SW_QG_move_004")    --轻功前加速跑
	BlendTime(0.2)
	BlendMode(1)
	GroundFx(0.28, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.06, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
	GroundFx(0.28, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.06, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

	Loop()    
	Fx(0.01,"FX_move_QG_common")
	EffectStart(0, "FOV", 5)
	EffectStart(0, "MB",0)
	--EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 
	GroundSound(0.06,"Sound_step")
	GroundSound(0.28,"Sound_step")

STake_Start("HG_QZ_SW_move_014_up","HG_QZ_SW_move_014(0,22)(23,(40,60))1")  --轻功前起跳1段
	BlendTime(0.1)
	BlendMode(0)
	FrameTime(0,0.733)
	Fx(0,"FX_move_QG_01") 
	EffectStart(0, "FOV", 15)
	EffectStart(0, "MB",0)
	GroundSound(0.140,"Sound_jump")
	Sound(0.140,"char_05_cloth_flap_01") 
	Sound(0.110,"HG_SoundVOC_Jump_1")	

STake_Start("HG_QZ_SW_move_014_loop","HG_QZ_SW_move_014(0,22)(23,(40,60))1")  --轻功前跳待机1段
	BlendTime(0.2)
	BlendMode(0)
	FrameTime(0.766,2.0) 
	Loop(1.333,2.000)
	EffectStart(0.766, "FOV", 15)
	EffectStart(0.766, "MB",0)
	Sound(0.900,"char_05_cloth_flap_02")

STake_Start("HG_QZ_SW_move_014_up_2","HG_QZ_SW_move_014(0,22)(23,(40,60))2")  --轻功前起跳2段
	BlendTime(0.1)
	BlendMode(0)
	FrameTime(0,0.733)
    
	Fx(0,"FX_move_QG_02")
	EffectStart(0, "FOV", 15)
	EffectStart(0, "MB",0)
	GroundSound(0.140,"Sound_jump")
	Sound(0.140,"char_05_cloth_flap_01") 
	Sound(0.110,"HG_SoundVOC_Jump_1")	   

STake_Start("HG_QZ_SW_move_014_loop_2","HG_QZ_SW_move_014(0,22)(23,(40,60))2")  --轻功前跳待机2段
	BlendTime(0.2)
	BlendMode(0)
	FrameTime(0.766,2.0) 
	Loop(1.333,1.999)
	EffectStart(0.766, "FOV", 15)
	EffectStart(0.766, "MB",0)
	Sound(0.900,"char_05_cloth_flap_02")	
	
STake_Start("HG_QZ_SW_move_014_up_3","HG_QZ_SW_move_014(0,22)(23,(40,60))3")  --轻功前起跳3段
	BlendTime(0.1)
	BlendMode(0)
	FrameTime(0,0.733)
    
	Fx(0,"FX_move_QG_03") 
	EffectStart(0, "FOV", 15)
	EffectStart(0, "MB",0)
	GroundSound(0.140,"Sound_jump")
	Sound(0.140,"char_05_cloth_flap_01") 
	Sound(0.110,"HG_SoundVOC_Jump_1")	   

STake_Start("HG_QZ_SW_move_014_loop_3","HG_QZ_SW_move_014(0,22)(23,(40,60))3")  --轻功前跳待机3段
	BlendTime(0.2)
	BlendMode(0)
	FrameTime(0.766,2.0) 
	Loop(1.333,2.000)
	EffectStart(0.766, "FOV", 15)
	EffectStart(0.766, "MB",0)
	Sound(0.900,"char_05_cloth_flap_02")

----------------------------------------------------------------------------- 
STake_Start("HG_QZ_SW_SU_move_004_F","HG_QZ_SW_move_004_F")  --加速前跑
	BlendMode(1)
	BlendTime(0.2)
	Loop()  

	GroundFx(0.25, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
	GroundFx(0.56, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)  
	GroundFx(0.25, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
	GroundFx(0.56, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)	
	GroundSound(0.25,"Sound_step")
	GroundSound(0.56,"Sound_step") 
	--EffectStart(0, "FOV", 15)  

STake_Start("HG_QZ_SW_move_000_F","HG_QZ_SW_move_003")   --前走
	BlendTime(0.2)
	BlendMode(1) 
	Loop()

	GroundFx(0.650, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.127, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.650, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.127, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
	GroundSound(0.042,"Sound_step")
	GroundSound(0.501,"Sound_step")     

STake_Start("HG_QZ_SW_move_004_F","HG_QZ_SW_move_004")  --前跑
	BlendTime(0.2)
	BlendMode(1) 
	Loop()
	BlendPattern("no_sync")   
	GroundFx(0.432, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.432, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
	GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)
	GroundSound(0.021,"Sound_step")
	GroundSound(0.381,"Sound_step")

STake_Start("HG_QZ_SW_dead","HG_QZ_SW_dead")
	BlendMode(0)
	BlendTime(0.2)
    
	--Sound(0.059,"voc_Male_death")  
	GroundSound(0.136,"Sound_step")
	GroundSound(0.300,"Sound_step")
	GroundSound(1.175,"Sound_fall")
	GroundSound(2.000,"Sound_daodi")
	Sound(0.100,"HG_SoundVOC_Dead_1")
	StopChannel(0,2)

STake_Start("HG_QZ_SW_hit","HG_QZ_SW_hit")
	BlendMode(0)
	BlendTime(0.05)
	Sound(0.02,"HG_SoundVOC_Hurt2_1")
	BlendPattern("Upper_hurt")    


STake_Start("HG_QZ_SW_dodge_000","HG_QZ_SW_dodge_000")
	BlendMode(0)
	BlendTime(0.05)
	EffectStart(0, "FOV", 5)
	Fx(0.015,"HG_THD_SW_dodge_001")
	Sound(0.064,"N1_Axe_dodge_000")
	Sound(0.06,"HG_SoundVOC_Jump_1")
	GroundSound(0.280,"Sound_fall")
	GroundSound(0.584,"Sound_step")
	GroundSound(0.720,"Sound_step") 

STake_Start("HG_QZ_SW_def_000","HG_QZ_SW_def_000")
	BlendMode(0)
	BlendTime(0.05)
	Sound(0.00,"weap_Sword_block")
	Fx(0.015,"Fx_gedang_wuqi")   

STake_Start("HG_QZ_SW_SYJwait_000","HG_QZ_SW_SYJwait_000")    --试衣间待机动作1  
	BlendMode(0)
	BlendTime(0.2)
	Loop()

STake_Start("HG_QZ_SW_SYJwait_001","HG_QZ_SW_SYJwait_001")     --试衣间待机动作2     
	BlendMode(0)
	BlendTime(0.2)
	Loop()
 
STake_Start("HG_QZ_SW_SYJwait_002","HG_QZ_SW_SYJwait_002")     --试衣间待机动作3     
	BlendMode(0)
	BlendTime(0.2)
	Loop()

-----------------------------------------------------------------------全真鼠标技能----------------------------------------------------------------------------------
-----------------------------------------------------------------------全真鼠标技能----------------------------------------------------------------------------------
-----------------------------------------------------------------------全真鼠标技能----------------------------------------------------------------------------------

STake_Start("HG_QZ_SW_attack_A_001_new","HG_QZ_SW_attack_A_001_new")   --寒冰刺1	13641
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
	Fx(0,"SG_QZ_SW_emo_001_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	LineFx(0.1,"SG_QZ_SW_attack_A_001_new",0,"Reference","",1,400)
	Sound(0.13,"QZ_SW_Skill_000_2")
	Sound(0.165,"HG_SoundVOC_attack1_1")
	AttackStart()
	Hurt(0.35,"hurt_21",0,1)
	HurtSound(0.35,"QZ_SW_Hit_11")
	AttackHurtFx(0.35, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.35,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_A_002_new","HG_QZ_SW_attack_A_002_new")   --寒冰刺2		13642
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
	LineFx(0.1,"SG_QZ_SW_attack_A_002_new",0,"Reference","",1,400)
	Sound(0.16,"HG_SoundVOC_attack1_1")
	Sound(0.13,"TY_Attack_014")
	AttackStart()
	Hurt(0.35,"hurt_21",0,1)
	HurtSound(0.35,"QZ_SW_Hit_11")
	AttackHurtFx(0.35, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.35,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_A_003_new","HG_QZ_SW_attack_A_003_new")   --寒冰刺3		13643
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
	LineFx(0.1,"SG_QZ_SW_attack_A_003_new",0,"Reference","",1,400)
	Sound(0.26,"HG_SoundVOC_attack1_1")
	Sound(0.14,"QZ_SW_Attack_11")
	Sound(0.36,"TY_Attack_014")
	AttackStart()
	Hurt(0.11,"hurt_21",0,1)
	Hurt(0.4,"hurt_21",0,1)
	HurtSound(0.35,"QZ_SW_Hit_11")
	AttackHurtFx(0.25, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	AttackHurtFx(0.4, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.25,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")
	
STake_Start("HG_QZ_SW_attack_A_004_new","HG_QZ_SW_attack_A_004_new")   --寒冰刺4	13644
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
	Sound(0.12,"HG_SoundVOC_attack2_1")
	Sound(0.03,"QZ_SW_Skill_007")

	LineFx(0.1,"SG_QZ_SW_attack_A_004_new",0,"Reference","",1,400)
	AttackStart()
	Hurt(0.22,"hurt_21",0,1)
	Hurt(0.38,"hurt_21",0,1)
	Hurt(0.45,"hurt_21",0,1)
	HurtSound(0.22,"QZ_SW_Hit_11")
	HurtSound(0.38,"QZ_SW_Hit_11")
	HurtSound(0.45,"QZ_SW_Hit_11")
	AttackHurtFx(0.2, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	AttackHurtFx(0.38, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	AttackHurtFx(0.45, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_JA_001_new","HG_QZ_SW_attack_B_JA_001_new")   --寒冰刺5 JA		13645
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("steady")
	Sound(0.182,"HG_SoundVOC_attack2_1")
	Sound(0.63,"QZ_SW_Skill_007")
	Sound(0.03,"QZ_SW_Skill014_Start1")
	Sound(0.72,"HG_SoundVOC_attack3_1")
	Sound(0.69,"QZ_SW_NewAttack_2")
	LineFx(0.1,"SG_QZ_SW_attack_B_JA_001_new",0,"Reference","",1,400)
	AttackStart()
	Hurt(0.42,"hurt_21",0,1)
	Hurt(0.56,"hurt_21",0,1)
	Hurt(0.7,"hurt_21",0,1)
	HurtSound(0.22,"QZ_SW_Hit_11")
	HurtSound(0.38,"QZ_SW_Hit_11")
	HurtSound(0.45,"QZ_SW_Hit_11")
	AttackHurtFx(0.2, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	AttackHurtFx(0.38, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	AttackHurtFx(0.45, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 1","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 6")
	CameraShake(0.7,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.035","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 6")

STake_Start("HG_QZ_SW_attack_C_001_new+","HG_QZ_SW_attack_C_001_new+")   --柔云1	13751
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("steady")
	Sound(0.03,"HG_SoundVOC_attack1_1")
	Sound(0.03,"QZ_SW_NewAttack_1")
	  
	--Fx(0.16,"SG_QZ_SW_attack_004","Reference",0.8,1,0,20,3,0,0,0,1)  
	LineFx(0.22,"SG_QZ_SW_attack_003",0,"Reference","",0.8,400,0,20,3,0,0,0,1.2) 

	AttackStart()
	Hurt(0.2,"hurt_21",0,1)
	HurtSound(0.2,"QZ_SW_Hit_11") 
	AttackHurtFx(0.2, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_C_002_new+","HG_QZ_SW_attack_C_002_new+")   --柔云2	13752
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
	--Fx(0.16,"SG_QZ_SW_attack_005","Reference",0.8,1,0,20,3,0,0,0,1)  
	--Fx(0.3,"SG_QZ_SW_attack_003","Reference",0.8,1,0,20,3,0,0,0,1)  	
	LineFx(0.2,"SG_QZ_SW_attack_003",0,"Reference","",0.8,400,0,20,3,0,0,0,1.2)  
	LineFx(0.44,"SG_QZ_SW_attack_004",0,"Reference","",0.8,400,0,20,3,0,0,0,1.2) 
	Sound(0.03,"HG_SoundVOC_attack1_1")
	Sound(0.03,"QZ_SW_NewAttack_2")
	
	AttackStart()
	Hurt(0.18,"hurt_21",0,1)
	Hurt(0.4,"hurt_21",0,1)
	HurtSound(0.18,"QZ_SW_Hit_11")
	HurtSound(0.4,"QZ_SW_Hit_11")
	AttackHurtFx(0.18, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	AttackHurtFx(0.4, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.03","ShakeAmplitudeX = 1","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_C_003_new+","HG_QZ_SW_attack_C_003_new+")   --柔云3	13753
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
	--Fx(0.55,"NB_HZT_skill_003_fire","Reference",0.8,1,15,0,15,0,0,90,2)   
	LineFx(0.58,"SG_QZ_SW_attack_004",0,"Reference","",0.8,400,15,0,15,0,0,90,1.2)   
	Sound(0.582,"HG_SoundVOC_attack2_1")
	Sound(0.03,"QZ_SW_NewAttack_3")
	Fx(0.1,"SG_QZ_SW_attack_B_001")
	Fx(0.15,"Trail01_QZ")
	
	AttackStart()
	Hurt(0.56,"hurt_21",0,1)	
	HurtSound(0.56,"QZ_SW_Hit_11") 
	AttackHurtFx(0.56, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.56,"ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")
	
STake_Start("HG_QZ_SW_attack_C_004_new+","HG_QZ_SW_attack_C_004_new+")   --柔云4	13754
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
	--Fx(0.4,"SG_QZ_SW_attack_005","Reference",0.8,1,0,20,3,0,0,0,1)  
	LineFx(0.45,"SG_QZ_SW_attack_005",0,"Reference","",0.8,400,0,20,3,0,0,0,1.2)  
	Sound(0.382,"HG_SoundVOC_attack1_1")
	Sound(0.13,"TY_Attack_014")

	AttackStart()
	Hurt(0.42,"hurt_21",0,1)
	HurtSound(0.42,"QZ_SW_Hit_11")
	AttackHurtFx(0.42, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.42,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_C_005_new+","HG_QZ_SW_attack_C_005_new+")   --柔云8	13758
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
	--Fx(0.57,"NB_HZT_skill_003_fire","Reference",0.8,1,0,0,5,0,0,0,2) 
	--Fx(0.7,"NB_HZT_skill_003_fire","Reference",0.8,1,0,0,5,0,0,0,2) 
	--Fx(0.85,"NB_HZT_skill_003_fire","Reference",0.8,1,0,0,5,0,0,0,2) 
	--Fx(1,"NB_HZT_skill_003_fire","Reference",0.8,1,0,0,5,0,0,0,2) 
	LineFx(0.59,"SG_QZ_SW_attack_005",0,"Reference","",0.8,400,0,0,5,0,0,0,1.2) 
	LineFx(0.72,"SG_QZ_SW_attack_A_air_002",0,"Reference","",0.8,400,0,0,5,0,0,0,1.2)
	LineFx(0.87,"SG_QZ_SW_attack_C_004_new",0,"Reference","",0.8,400,0,0,5,0,0,0,1.2)
	LineFx(1.02,"SG_QZ_SW_attack_C_003_new_002",0,"Reference","",0.8,400,0,0,5,0,0,0,1.2)
	Sound(0.13,"QZ_SW_Jian")
	Sound(0.05,"QZ_SW_QXQJ_fire")
	Sound(0.15,"HG_SoundVOC_attack2_1")

	AttackStart()
	Hurt(0.57,"hurt_21",0,1)	
	Hurt(0.7,"hurt_21",0,1)	
	Hurt(0.85,"hurt_21",0,1)	
	Hurt(1,"hurt_21",0,1)
	HurtSound(0.57,"QZ_SW_Hit_11") 
	HurtSound(0.7,"QZ_SW_Hit_11") 
	HurtSound(0.85,"QZ_SW_Hit_11") 
	HurtSound(1,"QZ_SW_Hit_11") 	
	AttackHurtFx(0.57, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)	
	AttackHurtFx(0.7, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	AttackHurtFx(0.85, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	AttackHurtFx(1, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.9,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 6")

STake_Start("HG_QZ_SW_attack_C_JA_001_new+","HG_QZ_SW_attack_C_JA_001_new+")  --柔云大冰剑，13027，目标位置砸下大冰剑/
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("steady_charging")
	Channel(0, "0")
	StopChannel(0,3)
	PlaySpeed(0,0.4,2)

	Fx(0.01,"HG_QZ_SW_skill_018_start")
	Fx(0.01,"Trail01_QZ","",3)
	Fx(0.7,"Trail01_QZ","",3)
	Fx(1.3,"Trail01_QZ","",3)
	Fx(0.656,"HG_QZ_SW_skill_Sanqi","Reference",0.7,1,0,0,0,0,0,0,1,1,0,1)
	Sound(0.15,"QZ_SW_Skill014_Fire")
	Sound(0.15,"QZ_SW_Skill013_Fire_2")
	Sound(0.15,"HG_SoundVOC_attack1_1")
	CameraShake(0.74,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.038","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 6")
	ContralShake(0.33,0.17,0,0.9)

STake_Start("HG_QZ_SW_attack_E_001","HG_QZ_SW_attack_E_001")   --定阳针1	13413   --0.1,0.15,0.4,0.6,1.1
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")

  	LineFx(0.115,"HG_QZ_SW_attack_JA_R_fire",0,"Reference","",1,400,0,-10,19)
	--Fx(0.115,"HG_QZ_SW_attack_JA_R_fire","Reference","Reference",1,0,0,-10,19)
	Fx(0.01,"HG_BTS_CR_attack_003")
	Sound(0.1,"QZ_SW_Skill_000_2")
	Sound(0.1,"HG_SoundVOC_attack1_1")
	Charge(0,0.05,10,30,12)
	
  	AttackStart()
	Hurt(0.165,"hurt_62",0,1)
	AttackHurtFx(0.165,"HG_QZ_SW_skill_015_hit", "Spine1",1)
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")


STake_Start("HG_QZ_SW_attack_E_002","HG_QZ_SW_attack_E_002")   --定阳针2	13414   --0.1,0.15,0.4,0.65,1.3
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  

	LineFx(0.2,"HG_QZ_SW_attack_JA_R_fire",0,"Reference","",1,400,0,-10,20)
	--Fx(0.2,"HG_QZ_SW_attack_JA_R_fire","Reference","Reference",1,0,0,-10,20)
	Fx(0.01,"HG_BTS_CR_attack_003")
	Sound(0.1,"QZ_SW_Skill_000_2")
	Sound(0.1,"HG_SoundVOC_attack1_1")
	Charge(0,0.07,15,30,12)  
	Pause(0.3,0.04,1,0.1)   ---开始时间，持续时间，1，慢速播放速率

	AttackStart()
	Hurt(0.25,"hurt_62",0,1)
	AttackHurtFx(0.25,"HG_QZ_SW_skill_015_hit", "Spine1",1)
	CameraShake(0.25,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_E_003","HG_QZ_SW_attack_E_003")   --定阳针3	13415   --0.03,0.19,0.56,0.63,0.9,1.466
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  

	Fx(0.01,"HG_BTS_CR_attack_003")
	Sound(0.1,"QZ_SW_Skill_000_2")
	Sound(0,"QZ_SW_Skill_001_1")
	Sound(0.1,"HG_SoundVOC_attack2_1")
	Charge(0,0.1,10,30,12)  

	local hurt_time1=0.19
	local hurt_time2=0.315
	local hurt_time3=0.440
	local hurt_time4=0.565
	LineFx(hurt_time1-0.05,"HG_QZ_SW_attack_JA_R_fire_1",0,"Reference","",1,400,0,-10,24)
	LineFx(hurt_time2-0.05,"HG_QZ_SW_attack_JA_R_fire_1",0,"Reference","",1,400,5,-5,20)
	LineFx(hurt_time3-0.05,"HG_QZ_SW_attack_JA_R_fire_1",0,"Reference","",1,400,-5,5,20)
	LineFx(hurt_time4-0.05,"HG_QZ_SW_attack_JA_R_fire_1",0,"Reference","",1,400,0,-10,16)

	AttackStart()
	Hurt(hurt_time1,"hurt_21")
	Hurt(hurt_time2,"hurt_21")
	Hurt(hurt_time3,"hurt_21")
	Hurt(hurt_time4,"hurt_21")
	AttackHurtFx(hurt_time1, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(hurt_time3, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	CameraShake(0.15,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_E_004","HG_QZ_SW_attack_E_004")   --定阳针4	13416   --0.1,0.2,0.41,0.733,1.466
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  
	
	LineFx(0.36,"HG_QZ_SW_attack_JA_R_fire_2",0,"Reference","",1.2,400,0,-10,16)
	Fx(0.01,"HG_BTS_CR_attack_003")
	Sound(0.1,"QZ_SW_Skill_000_2")
	Sound(0,"QZ_SW_Jian")
	Sound(0.1,"HG_SoundVOC_attack2_1")
	Charge(0,0.06,20,30,12)  


	AttackStart()
	Hurt(0.41,"hurt_62",0,1)
	AttackHurtFx(0.41, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	CameraShake(0.41,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_E_JA_001","HG_QZ_SW_attack_E_JA_001")   --定阳针JA	13417   --0.1,0.2,0.41,0.733,1.466
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  
	
	LineFx(1.10,"HG_QZ_SW_attack_JA_R_fire_3",0,"Reference","",1,400)

	Fx(0.01,"HG_BTS_CR_attack_003")
	Sound(1.05,"QZ_SW_Skill_000_2")
	Sound(0.1,"QZ_SW_NewAttack_3")
	Sound(1.05,"QZ_SW_Jian")
	Sound(0.1,"HG_SoundVOC_attack2_1")
	Sound(1.1,"HG_SoundVOC_attack3_1")
	Charge(0,0.06,20,30,12)  

	Pause(0.5,0.15,1,0.07)   ---开始时间，持续时间，1，慢速播放速率

	AttackStart()
	Hurt(1.05,"hurt_62",0,1)
	AttackHurtFx(1.05, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	CameraShake(1.05,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_new_001","HG_QZ_SW_attack_B_new_001")           --雷霆剑1		13093  0.1  0.2  /0.15  0.8333
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")    
	BlendPattern("steady")
	AttackStart()

	Hurt(0.3,"hurt_21")
	Hurt(0.3,"hurt_21")
	Hurt(0.3,"hurt_21")
	Hurt(0.3,"hurt_62")

	AttackHurtFx(0.3, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(0.3, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(0.3, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(0.3, "HG_QZ_SW_skill_015_hit", "Spine1",1)

	LineFx(0.11,"HG_QZ_SW_attack_JA_R_fire",0,"Reference","",1,400,-12,10,21)
	LineFx(0.11,"HG_QZ_SW_attack_JA_R_fire",0,"Reference","",1,400,4,-10,33)
	LineFx(0.11,"HG_QZ_SW_attack_JA_R_fire",0,"Reference","",1,400,12,10,21)
	LineFx(0.11,"HG_QZ_SW_attack_JA_R_fire",0,"Reference","",1,400,-4,-10,33)

	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	Sound(0.1,"QZ_SW_Skill_000_2")
	Sound(0.15,"QZ_SW_NewAttack_1")
	Sound(0.25,"HG_SoundVOC_attack1_1")
	CameraShake(0.25,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_new_002","HG_QZ_SW_attack_B_new_002")         --雷霆剑2	13092  0.2  0.3 /0.2   0.4   0.7  0.833
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")    
	BlendPattern("steady")
	AttackStart()

	Hurt(0.3,"hurt_21")
	Hurt(0.3,"hurt_21")
	Hurt(0.3,"hurt_21")

	AttackHurtFx(0.3, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(0.3, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(0.3, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	LineFx(0.11,"HG_QZ_SW_attack_JA_R_fire",0,"Reference","",1,400,-10,10,21)
	LineFx(0.11,"HG_QZ_SW_attack_JA_R_fire",0,"Reference","",1,400,10,10,21)
	LineFx(0.11,"HG_QZ_SW_attack_JA_R_fire",0,"Reference","",1,400,0,-10,42)

	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	Sound(0.15,"QZ_SW_Jian")
	Sound(0.25,"HG_SoundVOC_attack1_1")
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_new_003","HG_QZ_SW_attack_B_new_003")          --雷霆剑3		13095
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("steady")
	AttackStart()

	local hurt_time=0.6				--动作从0.6~0.9
	Hurt(hurt_time,"hurt_21")
	Hurt(hurt_time+0.03,"hurt_21")
	Hurt(hurt_time+0.06,"hurt_21")
	Hurt(hurt_time+0.09,"hurt_21")

	AttackHurtFx(hurt_time, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(hurt_time+0.03, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(hurt_time+0.06, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(hurt_time+0.09, "HG_QZ_SW_skill_015_hit", "Spine1",1)

	LineFx(0.1, "HG_QZ_SW_skill_JA2_R_loop",0,"Reference","",1,400)
	LineFx(0.55, "HG_QZ_SW_skill_JA2_R_fire",0,"Reference","",1,400)
	LineFx(0.55, "HG_QZ_SW_skill_JA3_R_start",0,"Reference","",1,400)
	
	ContralShake(0.55,0.2,0,0.5)

	HurtSound(hurt_time,"QZ_PA_Skill_000_hit")
	HurtSound(hurt_time+0.03,"QZ_PA_Skill_000_hit")
	HurtSound(hurt_time+0.06,"QZ_PA_Skill_000_hit")
	HurtSound(hurt_time+0.09,"QZ_PA_Skill_000_hit")

	Sound(0.1,"QZ_SW_Skill_001_2")
	Sound(0.1,"HG_SoundVOC_attack2_1") 
	Sound(0.1,"QZ_SW_Skill_004_1")
	Sound(0.15,"HG_SoundVOC_attack1_1")
	CameraShake(0.6,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_new_004","HG_QZ_SW_attack_B_new_004")            --雷霆剑4	13096
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")    
	BlendPattern("steady")
	AttackStart()

	local hurt_time=0.48
	Hurt(hurt_time,"hurt_21")
	Hurt(hurt_time+0.05,"hurt_21")
	Hurt(hurt_time+0.1,"hurt_21")
	Hurt(hurt_time+0.15,"hurt_21")
	Hurt(hurt_time+0.2,"hurt_21")
	Hurt(hurt_time+0.25,"hurt_21")

	AttackHurtFx(hurt_time, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(hurt_time+0.05, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(hurt_time+0.1, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(hurt_time+0.15, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(hurt_time+0.2, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	AttackHurtFx(hurt_time+0.25, "HG_QZ_SW_skill_015_hit", "Spine1",1)

	LineFx(0, "HG_QZ_SW_skill_JA3_R_loop",0,"Reference","",1,400)
	LineFx(0.1, "HG_QZ_SW_skill_JA3_R_fire",0,"Reference","",1,400)
	LineFx(1.01, "HG_QZ_SW_skill_JA_R_end",0,"Reference","",1,400)

	ContralShake(0.15,0.25,0,0.5)	
	HurtSound(hurt_time,"QZ_PA_Skill_000_hit")
	HurtSound(hurt_time+0.05,"QZ_PA_Skill_000_hit")
	HurtSound(hurt_time+0.1,"QZ_PA_Skill_000_hit")
	HurtSound(hurt_time+0.15,"QZ_PA_Skill_000_hit")
	HurtSound(hurt_time+0.2,"QZ_PA_Skill_000_hit")
	HurtSound(hurt_time+0.25,"QZ_PA_Skill_000_hit")	

	Sound(0.1,"QZ_SW_Skill_005")
	Sound(0.15,"HG_SoundVOC_attack2_1")
	Sound(0.1,"QZ_SW_Skill_002_2")
	CameraShake(0.47,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_A_JA_001_new","HG_QZ_SW_attack_A_JA_001_new")   --雷霆剑收式	13097 SG_QZ_SW_attack_A_JA002+/SG_QZ_SW_skill_015
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")    
	BlendPattern("steady")
	AttackStart()

	local hurt_time=0.65
	Hurt(hurt_time,"hurt_21")
	Charge(0,0.15,180,30,16)
	AttackHurtFx(hurt_time, "HG_QZ_SW_skill_015_hit", "Spine1",1)
	--Fx(0.11, "HG_QZ_SW_skill_JA_R_loop","Reference",1,0,0,0,0, 0,0,0,1)

	LineFx(0.21, "HG_QZ_SW_skill_JA4_R_start",0,"Reference","",1,400)
	LineFx(1.13, "HG_QZ_SW_skill_JA4_R_fire",0,"Reference","",1,400)
	HurtSound(hurt_time,"QZ_PA_Skill_000_hit")
	ContralShake(0.65,0.25,0,0.5)
	Sound(0.03,"HG_QZ_SW_skill_005")
	Sound(0.175,"HG_SoundVOC_attack1_1")
	Sound(1.175,"HG_SoundVOC_attack2_1")
	Sound(1.25,"QZ_SW_Skill_002_2")
	CameraShake(1.15,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_JZ_001","HG_QZ_SW_attack_B_JZ_001")   --回风闪1 start  13405    --0.06~0.15,0.2,0.3，0.63,1.5
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")

	Fx(0.195,"SG_QZ_SW_attack_B_JZ_001",1.3)
	Sound(0.1,"QZ_SW_NewAttack_1")
	Sound(0.1,"HG_SoundVOC_attack1_1")
	Charge(0,0.05,15,30,12)

	Pause(0.25,0.05,1,0.1)   ---开始时间，持续时间，1，慢速播放速率
	AttackStart()
	Hurt(0.2,"hurt_62",0,3)
	DirFx(0.22,"SG_QA_SW_attack_B_JZ_hit",28,1,0.8,90,-45)
	HurtBoneFx(0.22,"FX_blood_jian_a","Spine1",1,0,0,0,90, -30,0)
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.22,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_JZ_002","HG_QZ_SW_attack_B_JZ_002")   --回风闪2 start  13406    --0.02~0.15,0.2,0.4，0.644,1.4
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")

	Fx(0.194,"SG_QZ_SW_attack_B_JZ_002",1.3)
	Sound(0.1,"QZ_SW_NewAttack_1")
	Sound(0.1,"HG_SoundVOC_attack2_1")
	Charge(0,0.07,15,30,12)
	
	Pause(0.25,0.05,1,0.1)   ---开始时间，持续时间，1，慢速播放速率
	AttackStart()
	Hurt(0.2,"hurt_62",0,4)
	DirFx(0.25,"SG_QA_SW_attack_B_JZ_hit",28,1,0.8,-90,0)
	HurtBoneFx(0.22,"FX_blood_jian_a","Spine1",1,0,0,0,-90, -30,0)
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.22,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_JZ_003","HG_QZ_SW_attack_B_JZ_003")   --回风闪3 start  13407    --0.05~0.15,0.21,,0.65,0.9,1.233
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  


	Fx(0.186,"SG_QZ_SW_attack_B_JZ_003",1.3)
	Sound(0.1,"QZ_SW_NewAttack_1")
	Sound(0.1,"HG_SoundVOC_attack2_1")
	Charge(0,0.05,15,30,12)  
	
	Pause(0.25,0.2,1,0.1)   ---开始时间，持续时间，1，慢速播放速率
	AttackStart()
	Hurt(0.21,"hurt_21",0,2)
	DirFx(0.25,"SG_QA_SW_attack_B_JZ_hit",28,1,0.8,-90,45)
	HurtBoneFx(0.22,"FX_blood_jian_a","Spine1",1,0,0,0,-120, -120,0)
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.22,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_JZ_004","HG_QZ_SW_attack_B_JZ_004")   --回风闪4 start  13408    --0.38,0.9，1.433
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")

	Fx(0.16,"SG_QZ_SW_attack_B_JZ_004")
	Sound(0.1,"QZ_SW_NewAttack_2")
	Sound(0.1,"HG_SoundVOC_attack1_1")
	Charge(0,0.08,20,30,12)  
	Pause(0.25,0.2,1,0.1)   ---开始时间，持续时间，1，慢速播放速率

	AttackStart()
	Hurt(0.2,"hurt_81",0,1)
	DirFx(0.2,"SG_QA_SW_attack_B_JZ_hit",28,1,0.8,90,75)
	HurtBoneFx(0.22,"FX_blood_jian_a","Spine1",1.15,0,0,0,0,-130,0)
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.23,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_B_JZ_JA","HG_QZ_SW_skill_031_new")   	--回风闪JA		13860
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")


	Fx(0.126,"HG_QZ_SW_skill_Sanqi","Reference",0.65)
	Fx(0.225,"NB_WX_skill_004_fire","Reference",1,1,0,0,0,0,0,-15,1,1,0,1)
	Fx(0.22,"FX_Common_Fire_Sword_L_B","Reference",1.4,1,0,0,24,0,0,-7,1,1,0,1)

	--Fx(1.406,"FX_Common_Fire_Sword_L_B","Reference",1.8,1,0,0,24,0,0,10,1,1,0,1) --蓝光半月
	--Fx(1.406,"NB_WX_skill_006_fire1","Reference",1,1,0,0,0,260,0,0,1,1,0,1)--线性冰块
	--Fx(1.406,"NB_WX_skill_006_fire1","Reference",1,1,0,0,0,15,0,0,1,1,0,1)
	Sound(0.2,"QZ_SW_NewAttack_2")
	Sound(0.2,"HG_SoundVOC_attack3_1")
	
	AttackStart()
	Hurt(0.25,"hurt_62",0,4)
	DirFx(0.25,"SG_QA_SW_attack_B_JZ_hit",28,1,1.3,-130,0)
	HurtBoneFx(0.25,"FX_blood_jian_a","Spine1",1,0,0,0,-90, -30,0)
	HurtSound(0.25,"QZ_SW_Hit_11")

	CameraShake(0.25,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.035","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 6")


STake_Start("HG_QZ_SW_attack_F_001","HG_QZ_SW_attack_F_001")   --三仙剑诀一段，13421   0.1,0.47,0.6,0.9,1.566  C（0.16,0.36）/SG_QZ_SW_skill_022_001/SG_QZ_SW_skill_023_002
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  
	Fx(0.01,"SG_QZ_SW_skill_022_001")
	Sound(0.1,"QZ_SW_Skill013_QGXJ")
	Sound(0.08,"QZ_SW_Skill014_Start1")
	Sound(0.1,"HG_SoundVOC_attack2_1")
	AttackStart()
	Hurt(0.4,"hurt_62",0,3)
	HurtBoneFx(0.4,"FX_blood_jian_b","Spine1",1,0,0,0,-90, -45,0)
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.4,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")

STake_Start("HG_QZ_SW_attack_F_002","HG_QZ_SW_attack_F_002")   --三仙剑诀二段，13422   0.1,0.39,0.6,0.9,1.566  C（0.16,0.36）/SG_QZ_SW_skill_022_002/SG_QZ_SW_skill_023_001
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  

	Fx(0.01,"SG_QZ_SW_skill_022_002")
	Sound(0.1,"QZ_SW_NewAttack_1")
	Sound(0.08,"QZ_SW_Jian")
	Sound(0.1,"HG_SoundVOC_attack2_1")

	AttackStart()
	Hurt(0.39,"hurt_62",0,4)
	HurtBoneFx(0.39,"FX_blood_jian_b","Spine1",1,0,0,0,90, -50,0)
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.39,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")
	
STake_Start("HG_QZ_SW_attack_F_003","HG_QZ_SW_attack_F_003")   --三仙剑诀三段，13423   0.15,0.57,1,1.3,1.733   C（0.22,0.48）SG_QZ_SW_skill_031/SG_QZ_SW_skill_022_003/SG_QZ_SW_skill_022_002
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  


	Fx(0.01,"SG_QZ_SW_skill_022_003")
	Sound(0.01,"QZ_SW_Skill_011_2")
	Sound(0.4,"QZ_SW_Skill012_Fire_1")
	Sound(0.1,"HG_SoundVOC_attack2_1")

	AttackStart()
	Hurt(0.57,"hurt_62",0,1)
	HurtBoneFx(0.57,"FX_blood_jian_a","Spine1",1,0,0,0,90,0,0)
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.57,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")
	
----------------------------------------------------------空中的普攻技能----------------------------------------------------------
----------------------------------------------------------空中的普攻技能----------------------------------------------------------
----------------------------------------------------------空中的普攻技能----------------------------------------------------------

STake_Start("HG_QZ_SW_air_attack01","HG_QZ_SW_air_attack_001")   --空中普攻一段		0.2  0.36    0.6   0.8    1.133
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")   
	Priority(0.3,"6")	
	BlendPattern("steady")

	Fx(0.09,"Trail01_QZ")
	Sound(0.1,"QZ_SW_NewAttack_2")
	Sound(0.1,"HG_SoundVOC_attack1_1")
	LineFx(0.2,"SG_QZ_SW_attack_003",-1,"Reference","Reference",1,800,0,0,15)
	
	AttackStart()
	Hurt(0.25,"hurt_21")
	HurtSound(0.1,"QZ_SW_Hit_11")
	AttackHurtFx(0.25, "HG_QZ_SW_skill_000_hit", "Spine1",1)
	CameraShake(0.25,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_air_attack02","HG_QZ_SW_air_attack_002")   --空中普攻二段   	 0.2  0.36   0.75  1.133
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Priority(0.24,"6")
	BlendPattern("steady")

	Fx(0.09,"Trail01_QZ")
	Sound(0.1,"QZ_SW_NewAttack_2")
	Sound(0.1,"HG_SoundVOC_attack1_1")

	AttackStart()
	Hurt(0.3,"hurt_21")
	HurtSound(0.1,"QZ_SW_Hit_11") 
	AttackHurtFx(0.3, "HG_QZ_SW_skill_000_hit", "Spine1",1)
	LineFx(0.25,"SG_QZ_SW_attack_004",-1,"Reference","Reference",1,800,0,0,15)
	CameraShake(0.25,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_air_attack03","HG_QZ_SW_air_attack_003")   --空中普攻三段		 0.64  1.4  1.6	
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Priority(0.65,"6")
	BlendPattern("steady")

	Fx(0.09,"Trail01_QZ") 
	Sound(0.1,"QZ_SW_NewAttack_3")
	Sound(0.305,"QZ_SW_Attack_12")
	Sound(0.1,"HG_SoundVOC_attack3_1") 

	AttackStart()
	Hurt(0.64,"hurt_21")
	HurtSound(0.1,"QZ_SW_Hit_11") 
	AttackHurtFx(0.64, "HG_QZ_SW_skill_000_hit", "Spine1",1)
	LineFx(0.6,"SG_QZ_SW_attack_005",-1,"Reference","Reference",1,800,0,0,15)
	CameraShake(0.6,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_air_excalibur","HG_QZ_SW_attack_A_air_002")   --空中右键--EXCALIBUR	 0.32  0.64  1.02,1.366 13165
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	Priority(0.65,"6")
	BlendPattern("steady")

	Fx(0.01,"SG_QZ_SW_attack_A_air_002_new") 
	Sound(0.1,"QZ_SW_NewAttack_3")
	Sound(0.305,"QZ_SW_Attack_12")
	Sound(0.1,"HG_SoundVOC_attack2_1")

	AttackStart()
	Hurt(0.321,"hurt_71",0,1)
	HurtSound(0.28,"QZ_SW_Hit_11") 
	AttackHurtFx(0.321, "HG_QZ_SW_skill_000_hit", "Spine1",1)
	CameraShake(0.32,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

--------------------------------------------------------------------------键盘技能-----------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------键盘技能-----------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------键盘技能-----------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------键盘技能-----------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------键盘技能-----------------------------------------------------------------------------------------------------------------------

STake_Start("HG_QZ_SW_skill_002_new","HG_QZ_SW_skill_002_new")   	--可移动星河碎	13862
	BlendMode(0)
	BlendTime(0.01)
	Priority(0,"5")
	BlendPattern("QZ_stars")
	Channel(0, "3")

	Fx(0.01,"SG_QZ_SW_skill_025_fire","",1)
	Sound(0.03,"QZ_SW_Skill012_Start3")
	Sound(0.07,"QZ_SW_Skill013_Fire_1")
	Sound(0.05,"HG_SoundVOC_attack1_1")
	Sound(0.02,"WGZ_Attack_H")

	AttackStart()
	Hurt(0.20,"hurt_21",0,1)
	HurtSound(0.1,"QZ_SW_Hit_12")
	AttackHurtFx(0.1,"HG_THD_SW_skill_Bing", "Spine1",2)
	CameraShake(0.25,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_skill_001_new","HG_QZ_SW_skill_001_new")   	--玄冰掌		13860
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")

	PlaySpeed(0.01,0.63,1.533)
	Fx(0.63,"HG_QZ_SW_skill_014_start","",1)
	Fx(1.20,"HG_QZ_SW_skill_014_fire","",1.2)

	Sound(0.01,"QZ_SW_Skill_011_2")
	Sound(0.02,"QZ_SW_Skill014_Start1")
	Sound(0.4,"QZ_SW_Skill012_Fire_1")
	Sound(0.1,"HG_SoundVOC_attack2_1")

	Sound(1.12,"WGZ_Attack_H")
	Sound(1.2,"HG_SoundVOC_attack3_1")
	
	AttackStart()
	Hurt(1.20,"hurt_21",0,1)
	HurtSound(1.20,"QZ_SW_Hit_12")
	AttackHurtFx(1.2,"HG_THD_SW_skill_Bing", "Spine1",2)

STake_Start("HG_QZ_SW_skill_003_new","HG_QZ_SW_skill_003_new")   --烟罗步，13511   0.066,0.366,0.4，0.7，1.1
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  
	Sound(0.1,"QZ_SW_NewAttack_1")
	Sound(0.1,"QZ_SW_Skill014_Start2")
	Sound(0.1,"HG_SoundVOC_attack1_1")

	AttackStart()
	Charge(0,0.18,180,0,16)
	Fx(0.01,"SG_QZ_SW_skill_020_002_start")

STake_Start("HG_QZ_SW_skill_004_new","HG_QZ_SW_skill_004_new")  --星辰剑阵 13002
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("no_sync")

	AttackStart()
	Hurt(0.5,"")

	Fx(0.1,"Trail01_QZ")
	Fx(0.1,"Trail01_QZ")

	Sound(0.1,"QZ_SW_Skill_000_1")
	Sound(0.1,"QZ_SW_Skill012_Fire_2")
	Sound(0.1500,"HG_SoundVOC_attack1_1")

STake_Start("HG_QZ_SW_skill_005_new","HG_QZ_SW_skill_005_new")  --空明阵法	13620
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
	Sound(0.010,"QZ_SW_Skill012_Start1")
	Sound(0.03,"HG_QZ_SW_skill_005")
	Sound(0.080,"HG_SoundVOC_attack1_1")
	Fx(0.000,"HG_QZ_PA_skill_001_fire")

STake_Start("HG_QZ_SW_skill_020_new","HG_QZ_SW_skill_020_new")   --连锁七星，13436  0.06,0.12,0.15,0.3,0.9,1.5
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")

	Fx(0.01,"SG_SL_ST_skill_023_002","Reference",1.8,1,0,-40,-20,0,0,0,1,1,0,1)
	Sound(0.1,"QZ_SW_Skill013_Start1")
	Sound(0.25,"QZ_SW_Skill_011_3")
	Sound(0.1,"HG_SoundVOC_attack2_1")
	AttackStart()
	Hurt(0.13,"hurt_62",0,1)
	Hurt(0.15,"hurt_62",0,1)
	Hurt(0.17,"hurt_62",0,1)
	Hurt(0.19,"hurt_62",0,1)
	Hurt(0.21,"hurt_62",0,1)
	Hurt(0.23,"hurt_62",0,1)
	Hurt(0.25,"hurt_62",0,1)
	--   Pause(0.18,0.06,1,0.1)   ---开始时间，持续时间，1，慢速播放速率
	DirFx(0.05,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.07,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.09,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.11,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.13,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.15,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.17,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	HurtSound(0.08,"QZ_SW_Hit_11")
	HurtSound(0.1,"QZ_SW_Hit_11")
	HurtSound(0.12,"QZ_SW_Hit_11")
	HurtSound(0.14,"QZ_SW_Hit_11")
	HurtSound(0.16,"QZ_SW_Hit_11")
	HurtSound(0.18,"QZ_SW_Hit_11")
	HurtSound(0.20,"QZ_SW_Hit_11")
	CameraShake(0.25,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_attack_C_JA_001_new+_2","HG_QZ_SW_attack_C_JA_001_new+")      --空明剑，13624，目标位置砸下大冰剑/
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("steady_charging")
	Channel(0, "0")
	StopChannel(0,3)
	PlaySpeed(0,0.4,2)

	Fx(0.01,"HG_QZ_SW_skill_018_start")
	Fx(0.01,"Trail01_QZ","",3)
	Fx(0.7,"Trail01_QZ","",3)
	Fx(1.3,"Trail01_QZ","",3)
	Sound(0.15,"QZ_SW_Skill014_Fire")
	Sound(0.15,"QZ_SW_Skill013_Fire_2")
	Sound(0.15,"HG_SoundVOC_attack2_1")
	CameraShake(0.33,"ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")
	ContralShake(0.33,0.17,0,0.9)
	CameraShake(0.74,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_skill_021_new","HG_QZ_SW_skill_021_new")   --荧惑守心/黑洞	13451  0,0.1,0.15,0.5,1.033
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")

	Fx(0.53,"HG_QZ_PA_skill_001_fire")
	Fx(0.01,"HG_BTS_CR_attack_003")
	Sound(0.5,"QZ_SW_Skill012_Start1")
	Sound(0.1,"QZ_SW_NewAttack_1")
	Sound(0.1,"HG_SoundVOC_attack1_1")

	AttackStart()
	Hurt(0.2,"hurt_62",0,1)
	DirFx(0.05,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	HurtSound(0.1,"QZ_SW_Hit_11")

STake_Start("HG_QZ_SW_skill_022_new","HG_QZ_SW_skill_022_new")   --凝星掌，13476  0,0.42,0.7,1.4
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")

	Fx(0.01,"HG_QZ_PA_skill_001_fire")
	Fx(0.01,"HG_BTS_CR_attack_003")
	Sound(0.1,"QZ_SW_Skill012_Start1")
	Sound(0.15,"QZ_SW_Skill_001_2")
	Sound(0.1,"HG_SoundVOC_attack2_1")

	AttackStart()
	Hurt(0.01,"hurt_62",0,1)
	DirFx(0.05,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.25,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_skill_023_new","HG_QZ_SW_skill_023_new")   --碎星掌，13466  0,0.2,0.33,0.7,1.1
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  
	Fx(0.01,"HG_QZ_PA_skill_001_fire")

	Fx(0.01,"HG_BTS_CR_attack_003")
	Sound(0.1,"QZ_SW_Skill012_Start1")
	Sound(0.03,"QZ_SW_QXQJ_fire")
	Sound(0.1,"HG_SoundVOC_attack2_1")

	AttackStart()
	Hurt(0.01,"hurt_81",0,1)
	DirFx(0.05,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.95,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_skill_020_new_2","HG_QZ_SW_skill_020_new")   --回风斩，13431  0,0.06,0.14,0.2,0.4,0.7,1.166
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")
	BlendPattern("steady")

	Fx(0.01,"SG_SL_ST_skill_023_001","Reference",1,1,0,0,0,0,0,0,1,1,0,1)
	Sound(0.1,"QZ_SW_Skill_007")
	Sound(0.25,"QZ_SW_Skill_011_3")
	Sound(0.1,"HG_SoundVOC_attack2_1")

	AttackStart()
	Hurt(0.06,"hurt_81",0,1)
	Hurt(0.06,"hurt_81",0,1)
	Hurt(0.06,"hurt_81",0,1)
	Hurt(0.06,"hurt_81",0,1)
	Hurt(0.06,"hurt_81",0,1)
	Hurt(0.06,"hurt_81",0,1)
	Hurt(0.06,"hurt_81",0,1)
	DirFx(0.05,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.07,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.09,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.11,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.13,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.15,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	DirFx(0.17,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0)
	HurtSound(0.08,"QZ_SW_Hit_11")
	HurtSound(0.1,"QZ_SW_Hit_11")
	HurtSound(0.12,"QZ_SW_Hit_11")
	HurtSound(0.14,"QZ_SW_Hit_11")
	HurtSound(0.16,"QZ_SW_Hit_11")
	HurtSound(0.18,"QZ_SW_Hit_11")
	HurtSound(0.2,"QZ_SW_Hit_11")
	CameraShake(0.20,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_skill_008_new","HG_QZ_SW_skill_028_new+")   --玄冰掌		13860/SG_QZ_SW_skill_008_new/SG_QZ_SW_skill_028_new
	BlendMode(0)
	BlendTime(0.2)
	PlaySpeed(0.2,0.54,1.733)
	Priority(0,"5")
	BlendPattern("no_sync")
	--Fx(0.000,"HG_QZ_PA_skill_011_start")
	
	
	
	Fx(0.526,"HG_QZ_PA_skill_011_fire","Reference",0.7,1,0,0,0,0,0,0,1,1,0,1)
	Fx(0.526,"HG_QZ_SW_skill_Sanqi")--暴气的特效，留后用
	Fx(0.486,"QZ_Lsword_attack_002","Reference",1.45,1,0,-5,-27,25,0,0,1,1,0,1)
	Fx(0.456,"QZ_Lsword_attack_002","Reference",1.45,1,0,-5,-27,12,0,0,1,1,0,1)
	Fx(0.306,"QZ_Lsword_attack_002","Reference",1.45,1,0,-5,-27,0,0,0,1,1,0,1)
	Fx(0.486,"QZ_Lsword_attack_002","Reference",1.45,1,0,-5,-27,-12,0,0,1,1,0,1)
	Fx(0.306,"QZ_Lsword_attack_002","Reference",1.45,1,0,-5,-27,-25,0,0,1,1,0,1)
	
	AttackStart()
	Hurt(0.3,"hurt_21",0,1)
	Hurt(0.35,"hurt_21",0,1)
	Hurt(0.4,"hurt_21",0,1)
	Hurt(0.45,"hurt_21",0,1)
	Hurt(0.5,"hurt_21",0,1)
	Hurt(0.55,"hurt_21",0,1)
	Hurt(0.65,"hurt_81",0,1)
	
	Sound(0.050,"QZ_SW_Skill_011_2")
	Sound(0.750,"QZ_SW_Skill013_Start2")
	Sound(0.300,"HG_SoundVOC_attack2_1")
	--CutScene(0,"SG_SL_ST_skill_033_new1")
	CameraShake(0.75,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")


STake_Start("HG_QZ_SW_skill_024_new","HG_QZ_SW_skill_024_new")   --次元盾/天罡真气13501   0.25,0.3，0.812，1.4
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  
	Sound(0.1,"QZ_SW_NewAttack_1")
	Sound(0.1,"QZ_SW_Skill014_Start1")
	Sound(0.1,"HG_SoundVOC_attack2_1")

	AttackStart()

	STake_Start("HG_QZ_SW_skill_013_new","HG_QZ_SW_skill_013_new")   --原地1/升阳		13861
	FrameTime(0.192,2.066)
	PlaySpeed(0.192,2.066,1.333)
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")   
	BlendPattern("no_sync")
	Hard(0.000,0.702)
	--Fx(0.000,"HG_QZ_PA_skill_011_start")
	--Fx(0.746,"HG_QZ_PA_skill_011_fire")
	Fx(0.3,"SG_QZ_SW_attack_B_001")
	Fx(0.45,"Trail01_QZ")
	Fx(0.41,"SG_QZ_SW_skill_012_fire03")
	Fx(0.65,"SG_QZ_SW_skill_012_fire")
	Sound(0.050,"HG_QZ_PA_skill_002_new")
	Sound(0.300,"HG_SoundVOC_attack1_1")
	Sound(0.23,"QZ_SW_Skill_006_2")

	AttackStart()
	Hurt(0.22,"hurt_81",0,1)
	DirFx(0.25,"SG_QA_SW_attack_B_JZ_hit",28,1,0.8,-90,45)
	HurtBoneFx(0.25,"FX_blood_jian_a","Spine1",1,0,0,0,-120, -120,0)
	HurtSound(0.25,"QZ_SW_Hit_11")

STake_Start("HG_QZ_SW_skill_019_new","HG_QZ_SW_skill_019_new")   --雷霆怒斩     13516，次元剑  ，0.1,0.2,0.3,0.5,0.8,1.333
	BlendMode(0)
	BlendTime(0.0)
	Priority(0,"5")     
	BlendPattern("steady")  
	
	Fx(0.01,"SG_QZ_SW_skill_020_003") 
	Sound(0.1,"QZ_SW_NewAttack_2")
	Sound(0.05,"QZ_SW_QXQJ_001")
	Sound(0.1,"QZ_SW_Skill013_Fire_2")
	Sound(0.1,"HG_SoundVOC_attack3_1") 

	AttackStart()
	Hurt(0.1,"hurt_21")
	Hurt(0.2,"hurt_21")
	Hurt(0.3,"hurt_21")
	HurtSound(0.1,"QZ_SW_Hit_11") 
	HurtSound(0.2,"QZ_SW_Hit_11") 
	HurtSound(0.3,"QZ_SW_Hit_11") 
	AttackHurtFx(0.1, "HG_QZ_SW_skill_000_hit", "Spine1",1)	
	AttackHurtFx(0.2, "HG_QZ_SW_skill_000_hit", "Spine1",1)	
	AttackHurtFx(0.3, "HG_QZ_SW_skill_000_hit", "Spine1",1)	
	CameraShake(0.1,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_skill_025_new","HG_QZ_SW_skill_025_new")  	--无极	13600
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("steady")
	Sound(0.3,"QZ_SW_Skill_000_2")
	Sound(0.35,"HG_SoundVOC_attack1_1") 

STake_Start("HG_QZ_SW_skill_027_new","HG_QZ_SW_skill_027_new")  	--瑶光	13605
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("steady")
	Fx(0,"SG_QZ_SW_emo_001_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	Sound(0.23,"QZ_SW_Jian")
	Sound(0.25,"QZ_SW_Skill_000_2")
	Sound(0.25,"HG_SoundVOC_attack2_1") 

STake_Start("HG_QZ_SW_skill_016_new_jz","HG_QZ_SW_skill_016_new")   --13851	插棒子
	BlendMode(0)
	BlendTime(0.0)
	Priority(0,"5")     
	BlendPattern("steady")  
	Fx(0.01,"Trail01_QZ")
	Fx(0.65,"Trail01_QZ")
	Fx(0.65,"Trail01_QZ")
	Fx(0.65,"Trail01_QZ")
	Fx(0.65,"Trail01_QZ")

	Sound(0.1,"HG_SoundVOC_attack2_1")
	Sound(0.1,"QZ_SW_Skill_001_2")
	Sound(0.1,"QZ_SW_NewAttack_2")
	Sound(0.75,"QZ_SW_Skill_000_FX")

	Fx(0.05,"SG_QZ_SW_attack_B_JA_002_start")
	Fx(0.05,"QZ_LswordB_attack_aoe","Reference",0.5,0,0,0,-28)

	ContralShake(0.3,0.17,0,0.8)
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 3")

	STake_Start("HG_QZ_SW_skill_016_new_jz2","HG_QZ_SW_skill_016_new")   --/13852/13855 回棒子
	
	BlendTime(0.0)
	Priority(0,"5")
	BlendPattern("steady")
	Charge(0,0.2,1,0,22)
	--Fx(0,"HG_THD_SW_skill_011_02_a","Reference",1,1,0,0,0) 
	--Fx(0.3,"HG_THD_SW_skill_011_02_b","Reference",1,1,0,0,0)
	ChangeMaterial(0,0.3,"/PC/Material/FX_common_touming_2")

	Fx(0.01,"Trail01_QZ")
	Fx(0.65,"Trail01_QZ")
	Fx(0.65,"Trail01_QZ")
	Fx(0.65,"Trail01_QZ")
	Fx(0.65,"Trail01_QZ")
	Sound(0.1,"HG_SoundVOC_attack1_1") 
	Sound(0.02,"QZ_SW_Skill_008_2")
	Sound(0.95,"QZ_SW_Skill_000_FX")

	Fx(0.05,"SG_QZ_SW_attack_B_JA_002_start")
	Fx(0.05,"QZ_LswordB_attack_aoe","Reference",0.5,0,0,0,-28)	
	ContralShake(0.3,0.17,0,0.8)
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 3")

STake_Start("HG_QZ_SW_skill_018_new","HG_QZ_SW_skill_018_new")   --轻功衔接技	13891
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"1")
	BlendPattern("steady")
	FrameTime(0,1.38)

	Fx(0.23,"SG_SL_ST_skill_023_002","Reference",1.8,1,0,-40,-20,0,0,0,1,1,0,1)
	Sound(0.1,"QZ_SW_Skill013_Start1")
	Sound(0.25,"QZ_SW_Skill_011_3")
	Sound(0.1,"HG_SoundVOC_attack2_1")
	Charge(0,0.33,80,0,16) 
	AttackStart()
	Hurt(0.13,"hurt_62",0,1)
	Hurt(0.15,"hurt_62",0,1)
	Hurt(0.17,"hurt_62",0,1)
	Hurt(0.19,"hurt_62",0,1)
	Hurt(0.21,"hurt_62",0,1)
	Hurt(0.23,"hurt_62",0,1)
	Hurt(0.25,"hurt_62",0,1)
	--   Pause(0.18,0.06,1,0.1)   ---开始时间，持续时间，1，慢速播放速率
	DirFx(0.05,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	DirFx(0.07,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	DirFx(0.09,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	DirFx(0.11,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	DirFx(0.13,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	DirFx(0.15,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	DirFx(0.17,"FX_Violent_blood_Sharp_R1_red",28,1,1.5,-70,30,0) 
	HurtSound(0.08,"QZ_SW_Hit_11")
	HurtSound(0.1,"QZ_SW_Hit_11")
	HurtSound(0.12,"QZ_SW_Hit_11")
	HurtSound(0.14,"QZ_SW_Hit_11")
	HurtSound(0.16,"QZ_SW_Hit_11")
	HurtSound(0.18,"QZ_SW_Hit_11")
	HurtSound(0.20,"QZ_SW_Hit_11")

	STake_Start("HG_QZ_SW_skill_029","HG_QZ_SW_skill_029_new")  	-- 一闪	13630
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")   
	BlendPattern("steady")  

	Charge(0,0.33,80,0,16) 
	Charge(0.01,0.15,50,0,16)
	Charge(0.02,0.09,50,0,16) 
	Charge(0.03,0.15,60,0,16) 
	Charge(0.04,0.15,50,0,16) 
	Sound(0.03,"QZ_SW_Skill012_Fire_1")
	Sound(0.41,"QZ_SW_Skill012_Fire_2")
	Sound(0.41,"HG_SoundVOC_attack2_1")
	Fx(0,"SG_QZ_SW_skill_029_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	ChangeMaterial(0,0.8,"/PC/Material/yinshen")
	
	AttackStart()
	Hurt(0.1,"hurt_21",0,1)
	Hurt(0.35,"hurt_21",0,1)
	Hurt(0.53,"hurt_21",0,1)
	Hurt(0.75,"hurt_21",0,1)
	Hurt(1.1,"hurt_21",0,1)
	--Pause(0.12,0.04,1,0.1)
	--Pause(0.37,0.04,1,0.1)
	--Pause(0.55,0.04,1,0.1)
	--Pause(0.77,0.04,1,0.1)
	--Pause(1.12,0.04,1,0.1)
	HurtSound(0.1,"QZ_SW_Hit_12")
	HurtSound(0.35,"QZ_SW_Hit_12")
	HurtSound(0.53,"QZ_SW_Hit_12")
	HurtSound(0.75,"QZ_SW_Hit_12")
	HurtSound(1.1,"QZ_SW_Hit_12")
	DirFx(0.1,"SG_QA_SW_attack_B_JZ_hit",25,1,1,0,0)
	DirFx(0.35,"SG_QA_SW_attack_B_JZ_hit",25,1,1,0,0)
	DirFx(0.53,"SG_QA_SW_attack_B_JZ_hit",25,1,1,0,0)
	DirFx(0.75,"SG_QA_SW_attack_B_JZ_hit",25,1,1,0,0)
	DirFx(1.1,"SG_QA_SW_attack_B_JZ_hit",25,1,1,0,0)

STake_Start("HG_QZ_SW_skill_030","HG_QZ_SW_skill_028_new+")  	--寒流侵蚀	13770
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("steady")
	CutScene(0,"SG_SL_ST_skill_033_new1")
	Sound(0.03,"HG_QZ_SW_skill_005")
	Sound(0.23,"QZ_SW_NewAttack_2")
	Sound(0.85,"QZ_SW_QXQJ_fire")
	Sound(0.5,"HG_SoundVOC_attack2_1")
	CameraShake(0.55,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_QZ_SW_skill_011_fire","HG_QZ_SW_skill_011_fire")   --终结技，13772   0.15,0.57,1,1.3,1.733   C（0.22,0.48）SG_QZ_SW_skill_031/SG_QZ_SW_skill_022_003/SG_QZ_SW_skill_022_002
	BlendMode(0)
	BlendTime(0.1)
	Priority(0,"5")     
	BlendPattern("steady")  


	Fx(0.01,"SG_QZ_SW_skill_zhongjie_start")
	Fx(0.59,"SG_QZ_SW_skill_zhongjie_01_all")

	Sound(0.01,"QZ_SW_Skill_011_2")
	Sound(0.4,"QZ_SW_Skill012_Fire_1")
	Sound(0.1,"HG_SoundVOC_attack2_1")

	AttackStart()
	Hurt(0.4,"hurt_62",0,1)
	HurtBoneFx(0.4,"FX_blood_jian_a","Spine1",1,0,0,0,90,0,0)
	HurtSound(0.1,"QZ_SW_Hit_11")
	CameraShake(0.4,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")
	
	
STake_Start("HG_QZ_SW_skill_027_new+","HG_QZ_SW_skill_027_new")   --霜寒 解控反击	13057    
	BlendMode(0)
	BlendTime(0.0)
	Priority(0,"5")     
	BlendPattern("steady")  
	
	Fx(0.01,"SG_QZ_SW_skill_027_new_A") 

	AttackStart()
	Hurt(0.6,"hurt_21")
	HurtSound(0.6,"QZ_SW_Hit_11") 
	AttackHurtFx(0.6, "HG_QZ_SW_skill_000_hit", "Spine1",1)	
	CameraShake(0.6,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")	

STake_Start("HG_QZ_SW_def","HG_QZ_SW_def")   --格挡     13199
	BlendMode(0)
	BlendTime(0.0)
	Priority(0,"5")     
	BlendPattern("steady")  
	Loop()

STake_Start("HG_QZ_SW_def_pt","HG_QZ_SW_def_pt")   --格挡普通     13199
	BlendMode(0)
	BlendTime(0.0)
	Priority(0,"5")
	BlendPattern("steady")
	Sound(0.01,"ssw_def_pt")
	Fx(0,"SG_QZ_SW_def","Reference",1,1,0,0,0,0,0,0,1,1)

STake_Start("HG_QZ_SW_def_wm","HG_QZ_SW_def_wm")   --格挡完美     13199
	BlendMode(0)
	BlendTime(0.0)
	Priority(0,"5")
	BlendPattern("steady")
	Sound(0.01,"ssw_def_wm")
	Fx(0,"SG_QZ_SW_def_perfect","Reference",1,1,0,0,0,0,0,0,1,1)
	
STake_Start("HG_QZ_SW_def_air","HG_QZ_SW_def_air")   --air格挡     13204
	BlendMode(0)
	BlendTime(0.0)
	Priority(0,"5")     
	BlendPattern("steady")  
	Loop()

STake_Start("HG_QZ_SW_def_air_pt","HG_QZ_SW_def_air_pt")   --air格挡普通     13204
	BlendMode(0)
	BlendTime(0.0)
	Priority(0,"5")     
	BlendPattern("steady")
	Sound(0.01,"ssw_def_pt")
	Fx(0,"SG_QZ_SW_def","Reference",1,1,0,0,0,0,0,0,1,1)

STake_Start("HG_QZ_SW_def_air_wm","HG_QZ_SW_def_air_wm")   --air格挡完美     13204
	BlendMode(0)
	BlendTime(0.0)
	Priority(0,"5")     
	BlendPattern("steady")
	Sound(0.01,"ssw_def_wm")
	Fx(0,"SG_QZ_SW_def_perfect","Reference",1,1,0,0,0,0,0,0,1,1)

STake_Start("HG_QZ_SW_def_001","HG_QZ_SW_wait_000")   --格挡待机
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("wait")
	FrameTime(0,0.001)

STake_Start("HG_QZ_SW_skill_030_new","HG_QZ_SW_skill_030_new")   --吸星 13570
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")
	Fx(0.01,"FX_xihun_QZ_01","Reference",1,1,0,0,0,0,0,0,1,3)
	--Fx(0.01,"HG_QZ_SW_attack_JA_R_fire","Reference",1,1,0,0,0,0,0,0,0.1,1)
	Loop()	
	Sound(0.011,"Skill_XiXing")

STake_Start("HG_QZ_SW_HX_start","HG_QZ_SW_HX")   --打坐Loop 10607
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")
	Loop(1.1,3.1)
	Fx(0.9,"FX_tiaoxi_QZ","Reference",1,1,0,0,0,0,0,0,1,3)	
	Sound(0.01,"QZ_SW_Skill_009")
	Sound(0.96,"Life_Recover_Loop_1")

STake_Start("HG_QZ_SW_HX_fire","HG_QZ_SW_HX")   --打坐fire 10608
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")
	FrameTime(3.1,3.533)
	HurtHard(3.1,3.5)

-------------------------------------------------------------全真浮空女神模式-------------------------------------------------------------
-------------------------------------------------------------全真浮空女神模式-------------------------------------------------------------
-------------------------------------------------------------全真浮空女神模式-------------------------------------------------------------

STake_Start("HG_QZ_SW_skill_017_new","HG_QZ_SW_skill_017_new")   --浮空起手式start  13180    --0.1  0.6   0.933
	BlendMode(0)
	BlendTime(0.04)
	Priority(0,"7")
	FrameTime(0,0.65)
	BlendPattern("steady")

	Sound(0.1,"HG_SoundVOC_attack2_1") 
	Sound(0.1,"QZ_SW_Skill_002_2")
	Sound(0.5,"WGZ_Skill04_2")
	Sound(0.08,"QZ_SW_Skill014_Start2")
	Fx(0.05,"SG_QZ_SW_attack_B_JA_001_start")

STake_Start("HG_QZ_goddess_moveable_laser","QZ_Lsword_attack_002")               --激光 13181 
	BlendMode(0)
	BlendTime(0.2)
	AttackStart()
	Hurt(0.3,"hurt_21")
	Hurt(0.3,"hurt_21")
	Hurt(0.3,"hurt_21")
	Hurt(0.3,"hurt_21")
	Hurt(0.3,"hurt_21")
	--DrawDragFx(time, index, flytime, fxname, hitfx, hittime, mode, overrange, bone, fadein, fadeout, r, a, b, loop, shift,dirmode,fxscale,hitfxscale,x,y,z,useZ,isTurn)
	DrawDragFx(0.01, 0, 0.3, "SG_QZ_SW_air_skill_001", "SG_QA_SW_attack_B_JZ_hit", 0, 0, 0, "Spine1", 0, 0, 150, 45, 45, 0, 0,0,1,1,0,0,0,1)
	DrawDragFx(0.01, 1, 0.3, "SG_QZ_SW_air_skill_001", "SG_QA_SW_attack_B_JZ_hit", 0, 0, 0, "Spine1", 0, 0, 150, -45,45, 0, 0,0,1,1,0,0,0,1)  
	DrawDragFx(0.01, 2, 0.3, "SG_QZ_SW_air_skill_001", "SG_QA_SW_attack_B_JZ_hit", 0, 0, 0, "Spine1", 0, 0, 150, 0,63.64, 0, 0,0,1,1,0,0,0,1)  
	DrawDragFx(0.01, 3, 0.3, "SG_QZ_SW_air_skill_001", "SG_QA_SW_attack_B_JZ_hit", 0, 0, 0, "Spine1", 0, 0, 150, 63.64,0, 0, 0,0,1,1,0,0,0,1)  
	DrawDragFx(0.01, 4, 0.3, "SG_QZ_SW_air_skill_001", "SG_QA_SW_attack_B_JZ_hit", 0, 0, 0, "Spine1", 0, 0, 150,-63.64,0, 0, 0,0,1,1,0,0,0,1)  

	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	Sound(0.3,"QZ_SW_Skill_000_2")
	Sound(0.3,"QZ_SW_Skill_000_2")
	Sound(0.3,"QZ_SW_Skill_000_2")
	Sound(0.3,"QZ_SW_Skill_000_2")
	Sound(0.3,"QZ_SW_Skill_000_2")

STake_Start("HG_QZ_Lsword_attack_002","HG_QZ_Lsword_attack_002")               --激光 13184
	BlendMode(0)
	BlendTime(0.2)
	FrameTime(0,0.766) 
	Charge(0,0.15,180,10,16)
	LineFx(0.1,"QZ_Lsword_attack_002",0,"Reference","",1,400,0,0,-10)
	--LineFx(0.11,"QZ_Lsword_attack_002",0,"Reference","",1,400,0,0,-10)
	--LineFx(0.21,"QZ_Lsword_attack_002",0,"Reference","",1,400,0,0,-10)

	AttackStart()
	Hurt(0.3,"hurt_21")
	HurtSound(0.3,"QZ_PA_Skill_000_hit")
	Sound(0.3,"QZ_SW_Skill_000_2")
	AttackHurtFx(0.3, "HG_QZ_SW_skill_000_hit", "Spine1",0.7)
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")

STake_Start("HG_QZ_goddess_dreaming_swords_fire_lv1","HG_QZ_Lsword_attack_003")	--幻想飞剑1	13195
	BlendMode(0)
	BlendTime(0.2)

	Fx(0.2,"QZ_Lsword_attack_buff_fire")
	--Fx(0.2,"QZ_Lsword_attack_003_start")
	Fx(0.2,"QZ_Lsword_attack_buff01_fire")
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")

STake_Start("HG_QZ_goddess_dreaming_swords_fire_lv2","HG_QZ_Lsword_attack_003")	--幻想飞剑2	13196
	BlendMode(0)
	BlendTime(0.2)

	Fx(0.2,"QZ_Lsword_attack_buff_fire")
	Fx(0.4,"QZ_Lsword_attack_buff_fire")	
	--Fx(0.2,"QZ_Lsword_attack_003_start")
	--Fx(0.4,"QZ_Lsword_attack_003_start")
	Fx(0.2,"QZ_Lsword_attack_buff01_fire")
	Fx(0.4,"QZ_Lsword_attack_buff02_fire")
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")

STake_Start("HG_QZ_goddess_dreaming_swords_fire_lv3","HG_QZ_Lsword_attack_003")	--幻想飞剑3
	BlendMode(0)
	BlendTime(0.2)
	Fx(0.2,"QZ_Lsword_attack_buff_fire")
	Fx(0.4,"QZ_Lsword_attack_buff_fire")
	Fx(0.6,"QZ_Lsword_attack_buff_fire")
	--Fx(0.2,"QZ_Lsword_attack_003_start")
	--Fx(0.4,"QZ_Lsword_attack_003_start")
	--Fx(0.6,"QZ_Lsword_attack_003_start")
	Fx(0.2,"QZ_Lsword_attack_buff01_fire")
	Fx(0.4,"QZ_Lsword_attack_buff02_fire")
	Fx(0.6,"QZ_Lsword_attack_buff03_fire")
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")
	CameraShake(0.5,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")

STake_Start("HG_QZ_goddess_dreaming_swords_fire_lv4","HG_QZ_Lsword_attack_003")	--幻想飞剑4
	BlendMode(0)
	BlendTime(0.2)

	Fx(0.2,"QZ_Lsword_attack_buff_fire")
	Fx(0.4,"QZ_Lsword_attack_buff_fire")
	Fx(0.6,"QZ_Lsword_attack_buff_fire")
	Fx(0.8,"QZ_Lsword_attack_buff_fire")
	--Fx(0.2,"QZ_Lsword_attack_003_start")
	Fx(0.2,"QZ_Lsword_attack_buff01_fire")
	Fx(0.4,"QZ_Lsword_attack_buff02_fire")
	Fx(0.6,"QZ_Lsword_attack_buff03_fire")
	Fx(0.8,"QZ_Lsword_attack_buff04_fire")
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")
	CameraShake(0.6,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")

STake_Start("HG_QZ_SW_attack_D_001","HG_QZ_SW_attack_D_001")   -- 移动中普攻1
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")
	BlendPattern("rush_attack")  
	Channel(0, "2")
	Fx(0.01,"SG_QZ_SW_attack_B_JZ_001")
	Sound(0.1,"QZ_SW_NewAttack_1")
	Sound(0.1,"HG_SoundVOC_attack1_1")
	AttackStart()
	Hurt(0.1,"hurt_21",0,3)
	DirFx(0.1,"SG_QA_SW_attack_B_JZ_hit",28,1,0.8,90,-45)
	HurtSound(0.1,"QZ_SW_Hit_11")
	HurtBoneFx(0.22,"FX_blood_jian_a","Spine1",1,0,0,0,90, -30,0)

STake_Start("HG_QZ_SW_attack_D_002","HG_QZ_SW_attack_D_002")   -- 移动中普攻22
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("rush_attack")  
	Channel(0, "2")	
	Fx(0.01,"SG_QZ_SW_attack_B_JZ_002")
	Sound(0.1,"QZ_SW_NewAttack_1")
	Sound(0.1,"HG_SoundVOC_attack1_1")
	AttackStart()
	Hurt(0.12,"hurt_21",0,4)
	DirFx(0.12,"SG_QA_SW_attack_B_JZ_hit",28,1,0.8,-90,0)
	HurtBoneFx(0.12,"FX_blood_jian_a","Spine1",1,0,0,0,-90, -30,0)
	HurtSound(0.12,"QZ_SW_Hit_11")
