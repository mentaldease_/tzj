
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	self.fishingSkillBookItemID = '1_599' -- 钓鱼技能书道具ID
	self.fishRodItemID = '5_10000'        -- 鱼竿道具ID
	self.hasBookNow = false
	self.hasRodNow = false
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	--lout('Update ' .. self.name)
	if not self.hasBookNow and ItemHelper.ContainsItem(ItemHelper.COMPOSITE_BAG_RANGE, self.fishingSkillBookItemID) then
		self.hasBookNow = true
	end
	
	if not self.hasRodNow and ItemHelper.ContainsItem(ItemHelper.EQUIPMENT_BAG_RANGE, self.fishRodItemID) then
		self.hasRodNow = true
	end
	
	if self.hasBookNow and self.hasRodNow then
		self.completed = true
	end

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

