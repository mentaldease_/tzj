-----------------------------------------------------------------------少林棍操控------------------------------------------------------------------	
-----------------------------------------------------------------------少林棍操控------------------------------------------------------------------	
-----------------------------------------------------------------------少林棍操控------------------------------------------------------------------

STake_Start("HG_SL_ST_PY_move_004","HG_SL_ST_dodge")         --前闪避
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5")   

	EffectStart(0, "FOV", 5)


    Fx(0.1,"SG_SL_ST_skill_move","",1.2,0,0,5,-3,0,0,0,1.4)	
	
    Sound(0.010,"char_05_cloth_flap_02")
    Sound(0.010,"N2_BYJQ01_dodge_001")
	Sound(0.02,"HG_SoundVOC_Jump_1")
    GroundFx(0.3, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.38, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.3, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.38, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundSound(0.01,"Sound_step")
    GroundSound(0.3,"Sound_step") 
    GroundSound(0.38,"Sound_step") 
    EffectStart(0.0,"MB",4)
	
STake_Start("HG_SL_ST_ss","HG_SL_ST_SU_ss")    --受身     
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"1")     
	Priority(0.38,"5")
    BlendPattern("steady") 	
    Sound(0.010,"char_05_cloth_flap_02")
	Sound(0.02,"HG_SoundVOC_Jump_1")

  	Fx(0,"HG_GB_SL_ss_a","Reference",1,0,0,0,0,0,0,0,1) 
	Fx(0,"HG_ALL_ss_b","Reference",1,0,0,0,0,0,0,0,1) 

STake_Start("HG_SL_ST_jf","HG_SL_ST_jf")  --前击退
    BlendTime(0.2)
    BlendMode(0)
    HurtHard(0,0.5)
    Priority(0.5,"5")
    GroundSound(0.24,"Sound_step")
    GroundSound(0.27,"Sound_step")
	
STake_Start("HG_SL_ST_jf_B","HG_SL_ST_jf_B")  --后击退
    BlendTime(0.2)
    BlendMode(0)
    HurtHard(0,0.5)
    Priority(0.5,"5")
    GroundSound(0.24,"Sound_step")
    GroundSound(0.27,"Sound_step")

STake_Start("HG_SL_ST_jf_L","HG_SL_ST_jf_L")  --左击退
    BlendTime(0.2)
    BlendMode(0)
    HurtHard(0,0.5)
    Priority(0.5,"5")
    GroundSound(0.24,"Sound_step")
    GroundSound(0.27,"Sound_step")

STake_Start("HG_SL_ST_jf_R","HG_SL_ST_jf_R")  --右击退
    BlendTime(0.2)
    BlendMode(0)
    HurtHard(0,0.5)
    Priority(0.5,"5")
    GroundSound(0.24,"Sound_step")
    GroundSound(0.27,"Sound_step")	
	
--------------------------------------------------------------------------

STake_Start("HG_SL_ST_SU_QG_move_004_start","HG_SL_ST_SU_QG_move_004_start")    --轻功前加速跑起步
    BlendTime(0.05)
    BlendMode(0)
    Fx(0.1,"SG_SL_ST_skill_move","",1.2,0,0,5,-3,0,0,0,1.4)
  	Fx(0,"FX_move_QG_fire","Reference") 	
    GroundFx(0.165, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.04, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.165, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.04, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      


    EffectStart(0, "MB",0)
 
	Sound(0.08,"char_05_cloth_flap_01")
	Sound(0.08,"HG_SL_SB_Skill_011_loop")
    GroundSound(0.165,"Sound_step")
    GroundSound(0.04,"Sound_step")


STake_Start("HG_SL_ST_SU_QG_move_004_end","HG_SL_ST_SU_QG_move_004_end")    --轻功前加速跑刹车
    BlendTime(0.02)
    BlendMode(0)


    GroundFx(0.21, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.06, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.21, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.06, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    GroundSound(0.090,"Sound_brake")
    GroundSound(0.21,"Sound_step")
    GroundSound(0.06,"Sound_step")

    EffectStart(0, "FOV", 0)
	
STake_Start("HG_SL_ST_SU_QG_move_004","HG_SL_ST_SU_QG_move_004")    --轻功前加速跑
    BlendTime(0.2)
    BlendMode(1)

    GroundFx(0.27, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.04, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.27, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.04, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    Loop()    
    
    Fx(0.01,"FX_move_QG_common")


    EffectStart(0, "FOV", 5)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.04,"Sound_step")
    GroundSound(0.27,"Sound_step")

STake_Start("HG_SL_ST_move_014_up","HG_SL_ST_move_014(0,22)(23,(40,60))1")  --轻功前起跳1段
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_01") 
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    
	Sound(0.01,"HG_SoundVOC_Jump_1")

STake_Start("HG_SL_ST_move_014_loop","HG_SL_ST_move_014(0,22)(23,(40,60))1")  --轻功前跳待机1段
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.733,1.333) 
    Loop()

    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    Sound(0.900,"char_05_cloth_flap_02")

STake_Start("HG_SL_ST_move_014_up_2","HG_SL_ST_move_014(0,22)(23,(40,60))2")  --轻功前起跳2段
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_02")
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    
	Sound(0.01,"HG_SoundVOC_Jump_1")

STake_Start("HG_SL_ST_move_014_loop_2","HG_SL_ST_move_014(0,22)(23,(40,60))2")  --轻功前跳待机2段
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.733,1.333) 
    Loop()
    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    Sound(0.900,"char_05_cloth_flap_02")	
	
STake_Start("HG_SL_ST_move_014_up_3","HG_SL_ST_move_014(0,22)(23,(40,60))3")  --轻功前起跳3段
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_03") 
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    
	Sound(0.01,"HG_SoundVOC_Jump_1")

STake_Start("HG_SL_ST_move_014_loop_3","HG_SL_ST_move_014(0,22)(23,(40,60))3")  --轻功前跳待机3段
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.733,1.333) 
    Loop()
    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    Sound(0.900,"char_05_cloth_flap_02")	
	
----------------------------------------------------------------------------- 	
	
	
	
	
STake_Start("HG_SL_ST_SU_move_004","HG_SL_ST_SU_move_004")  --前加速跑 
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    --EffectStart(0, "FOV", 15)
    GroundSound(0.17,"Sound_step")
    GroundSound(0.39,"Sound_step")
    GroundFx(0.17, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.39, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.17, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.39, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	

STake_Start("HG_SL_ST_SU_move_001","HG_SL_ST_SU_move_001")  --后加速跑 
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    --EffectStart(0, "FOV", 15)
    GroundSound(0.17,"Sound_step")
    GroundSound(0.3,"Sound_step")
    GroundFx(0.17, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.3, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.17, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.3, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 

STake_Start("HG_SL_ST_SU_move_005","HG_SL_ST_SU_move_005")  --左加速跑 
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    --EffectStart(0, "FOV", 15)
    GroundSound(0.2,"Sound_step")
    GroundSound(0.46,"Sound_step")
    GroundFx(0.2, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.46, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.2, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.46, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 

STake_Start("HG_SL_ST_SU_move_006","HG_SL_ST_SU_move_006")  --右加速跑 
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    --EffectStart(0, "FOV", 15)
    GroundSound(0.2,"Sound_step")
    GroundSound(0.46,"Sound_step")
    GroundFx(0.2, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.46, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.2, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.46, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
	
STake_Start("HG_SL_ST_move_004","HG_SL_ST_move_004")    --前跑 
    BlendMode(1)
    BlendPattern("Down")   
    Channel(0, "0") 	
    Loop()    
    GroundSound(0.24,"Sound_step")
    GroundSound(0.5,"Sound_step")
    GroundFx(0.24, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.5, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.24, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.5, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
  	
STake_Start("HG_SL_ST_move_001","HG_SL_ST_move_001")    --后跑
    BlendTime(0.2)
    BlendMode(1)
    BlendPattern("Down")     
    Loop()    
    GroundSound(0.22,"Sound_step")
    GroundSound(0.4,"Sound_step")	
    GroundFx(0.22, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.4, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.22, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.4, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
	
STake_Start("HG_SL_ST_move_005","HG_SL_ST_move_005")    --左跑
    BlendTime(0.2)
    BlendMode(1)
    BlendPattern("Down")     
    Loop()    
    GroundSound(0.22,"Sound_step")
    GroundSound(0.5,"Sound_step")	
    GroundFx(0.22, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.5, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.22, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.5, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
	
STake_Start("HG_SL_ST_move_006","HG_SL_ST_move_006")    --右跑
    BlendTime(0.2)
    BlendMode(1)
    BlendPattern("Down")     
    Loop()    
    GroundSound(0.23,"Sound_step")
    GroundSound(0.55,"Sound_step")		
    GroundFx(0.23, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.55, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.23, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.55, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 

STake_Start("HG_SL_ST_move_003","HG_SL_ST_move_003")    --前走 
    BlendMode(1)
    BlendPattern("Down")     
    Loop()    
    GroundSound(0.485,"Sound_step")
    GroundSound(0.951,"Sound_step")
    GroundFx(0.951, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.485, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.951, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.485, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 		
	
	
STake_Start("HG_SL_ST_wait_000","HG_SL_ST_wait_000")      
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait_none")  
	
STake_Start("HG_wait_000_SLST","HG_wait_000_SLST")      
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait_none")  	

STake_Start("HG_SL_ST_hit","HG_SL_ST_hit")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_hurt")
    Sound(0.02,"HG_SoundVOC_Hurt2_1")


STake_Start("HG_SL_ST_SYJwait_000","HG_SL_ST_SYJwait_000")    --试衣间待机动作1  
    BlendMode(0)
    BlendTime(0.2)
    Loop()
 

STake_Start("HG_SL_ST_SYJwait_001","HG_SL_ST_SYJwait_001")     --试衣间待机动作2     
    BlendMode(0)
    BlendTime(0.2)
    Loop()
 

STake_Start("HG_SL_ST_SYJwait_002","HG_SL_ST_SYJwait_002")     --试衣间待机动作3     
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("HG_emo_000_SLST","HG_emo_000_SLST")     --表情动作1     
    BlendMode(0)
    BlendTime(0.2)

	
STake_Start("HG_emo_001_SLST","HG_emo_001_SLST")     --表情动作2     
    BlendMode(0)
    BlendTime(0.2)

	

	
-----------------------------------------------------------------------少林棍地面------------------------------------------------------------------	
-----------------------------------------------------------------------少林棍地面------------------------------------------------------------------	
-----------------------------------------------------------------------少林棍地面------------------------------------------------------------------

--------------------------------------------------------------------------左键---------------------------------------------------------------------	
	
STake_Start("HG_SL_ST_attack_003","HG_SL_ST_attack_003")     -------普攻1顿  13931    (SOFT建议时间0.2秒）   动作打击点时间 0.18
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  
    Fx(0.195,"SG_SL_ST_attack003_smoke","",1.2)
		Fx(0.195,"FX_SL_ST_attack_000","",0.8,0,-15,-25,18,0,0,-150,1)
	Sound(0.15,"HG_SL_ST_attack_000")
        Sound(0.17,"HG_GB_ST_Skill_000")
        Sound(0.19,"Wolf_L_move_004_1")
        Sound(0.12,"HG_SoundVOC_attack1_1")
    GroundSound(0.25,"Sound_step")
	
	

    Charge(0,0.09,15,30,12) --0.1~0.19
    
    AttackStart()
			Pause(0.2,0.1,0,0.01)
    Hurt(0.19,"hurt_62",0,2)
	DirFx(0.19,"FX_SL_ST_attack_000_hit",28,1,1,90,30,0)	
--	HurtBoneFx(0.19,"FX_Violent_blood_dun1","Spine1",1,0,0,0,0,0,0)	
    HurtSound(0.19,"BTS_CR_HIT_new_1_1")	

STake_Start("HG_SL_ST_attack_004","HG_SL_ST_attack_004")     -------普攻2顿  13932   (SOFT建议时间0.25秒）   动作打击点时间 0.23
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  

    Fx(0.23,"SG_SL_ST_attack004_smoke","",1.2)	
		Fx(0.23,"FX_SL_ST_attack_000","",0.8,0,0,-25,25,0,0,35,1)
	Sound(0.14,"HG_SL_ST_attack_000")
        Sound(0.16,"HG_GB_ST_Skill_000")
        Sound(0.19,"Wolf_L_move_004_1")
        Sound(0.12,"HG_SoundVOC_attack1_1")
    GroundSound(0.24,"Sound_step") 
	ContralShake(0.55,0.3,0,0.9)		
    Charge(0,0.07,15,30,12) --0.13
	
    AttackStart()
			Pause(0.24,0.1,0,0.01)
    Hurt(0.23,"hurt_62",0,1)
	DirFx(0.23,"FX_SL_ST_attack_000_hit",28,1,1,-90,-35,0)	
	HurtBoneFx(0.23,"FX_blood_gun_b","Spine1",1,0,0,0,90,-45,0)	
    HurtSound(0.23,"BTS_CR_HIT_new_1_1")	
	

STake_Start("HG_SL_ST_attack_005","HG_SL_ST_attack_005")     -------普攻3顿  13933   (SOFT建议时间0.5秒）   动作打击点时间 0.21|0.46
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  

--	PlaySpeed(0.13,0.26,3)
--	PlaySpeed(0.406,0.626,3)	
    Fx(0.34,"SG_SL_ST_attack005_smoke","",1.3)	
	Fx(0.3,"FX_SL_ST_attack_000","",0.8,0,0,-25,25,0,0,-90,1) 	
        Sound(0.12,"HG_SoundVOC_attack2_1")	
--	Sound(0.06,"HG_SL_ST_Skill_016")
        Sound(0.42,"HG_GB_ST_Skill_001")
	Sound(0.14,"Wolf_L_move_004_1")	
--   GroundSound(0.21,"Sound_step")
    GroundSound(0.1,"Sound_step") 
	
	Charge(0,0.1,20,30,12) --0.1~0.29
	
    AttackStart()
			Pause(0.35,0.1,0,0.01)
--    Hurt(0.156,"hurt_62",0,2)
    Hurt(0.34,"hurt_62",0,2)	
--	DirFx(0.156,"FX_SL_ST_attack_000_hit",28,1,1,0,90,0)
	DirFx(0.337,"FX_SL_ST_attack_000_hit",28,1,1,0,90,0)	
--	HurtBoneFx(0.337,"FX_Violent_blood_Blunt_R1","Spine1",1.4,0,0,0,0,0,0)	
--    HurtSound(0.156,"BTS_CR_HIT_new_1_1")	
    HurtSound(0.337,"BTS_CR_HIT_new_2_1")


STake_Start("HG_SL_ST_skill_011","HG_SL_ST_skill_011")    --左键普攻4/真空连爆  13948       动作打击点时间0.26     (SOFT建议时间0.47秒）
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 
        Sound(0.06,"HG_SoundVOC_attack4_1")
        Sound(0.08,"HG_GB_ST_Skill_002")
        Sound(0.08,"HG_SL_ST_Skill_001")	
 
--	PlaySpeed(0,0.548,1.5)
    Charge(0.38,0.05,20,30,12)  
	
 	Fx(0.5,"SG_SL_ST_attack005_smoke","",1.5)	
    Fx(0.5,"FX_SL_ST_attack_000","",0.8,0,-5,-40,40,0,0,45,1)
	
    AttackStart()
    Pause(0.6,0.15,0,0.01) 
    Hurt(0.4,"hurt_62",0,1)	
	--DirFx(0.26,"SG_SL_ST_skill_011_hit",25,1)
	DirFx(0.4,"FX_SL_ST_attack_000_hit",28,1,1.5,-90,-45,0)	
	HurtBoneFx(0.4,"FX_blood_gun_c","Spine1",1,0,0,0,90,-45,0)	
    HurtSound(0.4,"BTS_CR_HIT_new_s1")

--    CameraShake(0.68,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2.5")	
	
	

STake_Start("HG_SL_ST_skill_010_start","HG_SL_ST_skill_010")    --打晕/JA1  13956       动作打击点时间0.2     (SOFT建议时间0.8秒）
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
    Sound(0.08,"HG_SL_ST_Skill_009")
    Sound(0.33,"SG_THD_SW_skill_010_Impact")
    Sound(0.06,"HG_SoundVOC_attack3_1")
	Fx(0.35,"SG_SL_ST_skill_010_fire_B")
    	Fx(0,"HG_SL_ST_skill_003","",1,0,0,0,0,0,0,0,1.5) 

    --CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.07","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2")
    CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.08","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 7")
	--ContralShake(0.4,0.1,0,0.8)	

	  
STake_Start("HG_SL_ST_skill_010_fire","")
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
 
    Charge(0,0.15,20,25,12) 


    AttackStart()	
    Hurt(0,"hurt_62",0,2)
	DirFx(0,"HG_SL_ST_skill_018_hit",25,1) 
	HurtBoneFx(0.0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)	
    HurtSound(0,"BTS_CR_HIT_new_s1")		
		
	
STake_Start("HG_SL_ST_skill_006","HG_SL_ST_skill_006")     -------   百烈棍/JA2	13936	动作打击点时间 0.17|0.3|0.41|0.5|0.63|0.74|0.8|0.9|   1.4
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
	
    GroundSound(1.5,"Sound_step") 
    Fx(0.0,"SG_SL_ST_skill_006_fire1")
    Fx(1.18,"SG_SL_ST_skill_007_04","",1.0,0,0,-20,0,0,0,0)
--    Fx(1.4,"SG_SL_ST_skill_006_fire2")
        Sound(0.09,"HG_SL_ST_Skill_007")
        Sound(0.85,"HG_SL_ST_Skill_005")
    Sound(0.06,"HG_SoundVOC_attack4_1")
    Sound(1.39,"HG_SoundVOC_attack3_1")
    Sound(0.9,"QZ_PA_Skill_006")
    Sound(1.12,"HG_SL_SB_Skill_004")
	
    AttackStart()
    Hurt(0.18,"hurt_62",0,1)
    Hurt(0.26,"hurt_62",0,1)
    Hurt(0.34,"hurt_62",0,1)
    Hurt(0.42,"hurt_62",0,1)
    Hurt(0.5,"hurt_62",0,1)
    Hurt(0.58,"hurt_62",0,1)
    Hurt(0.66,"hurt_62",0,1)
    Hurt(0.74,"hurt_62",0,1)
    Hurt(0.82,"hurt_62",0,1)
    Hurt(0.9,"hurt_62",0,1)
    Hurt(1.35,"hurt_81",0,1)	
	HurtBoneFx(0.135,"FX_blood_gun_c","Spine1",1,0,0,0,180,0,0)	
	DirFx(0.18,"HG_SL_ST_attack_hit",25,0.7) 	
	DirFx(0.26,"HG_SL_ST_attack_hit",25,0.7)
	DirFx(0.34,"HG_SL_ST_attack_hit",25,0.7) 	
	DirFx(0.42,"HG_SL_ST_attack_hit",25,0.7)
	DirFx(0.5,"HG_SL_ST_attack_hit",25,0.7) 	
	DirFx(0.58,"HG_SL_ST_attack_hit",25,0.7)
	DirFx(0.66,"HG_SL_ST_attack_hit",25,0.7) 	
	DirFx(0.74,"HG_SL_ST_attack_hit",25,0.7)
	DirFx(0.82,"HG_SL_ST_attack_hit",25,0.7) 	
	DirFx(0.9,"HG_SL_ST_attack_hit",25,0.7)
	DirFx(1.35,"HG_SL_ST_attack_hit",25,0.7) 	
    HurtSound(0.18,"HG_SL_ST_Hit_000")	
    HurtSound(0.26,"HG_SL_ST_Hit_000")	
    HurtSound(0.34,"HG_SL_ST_Hit_000")	
    HurtSound(0.42,"HG_SL_ST_Hit_000")	
    HurtSound(0.5,"HG_SL_ST_Hit_000")	
    HurtSound(0.58,"HG_SL_ST_Hit_000")	
    HurtSound(0.66,"HG_SL_ST_Hit_000")	
    HurtSound(0.74,"HG_SL_ST_Hit_000")	
    HurtSound(0.82,"HG_SL_ST_Hit_000")	
    HurtSound(0.9,"HG_SL_ST_Hit_000")		
    HurtSound(1.35,"BTS_CR_HIT_new_s1")	
	
    CameraShake(1.36,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 8")	
	ContralShake(0.15,0.3,0,0.7)
	ContralShake(0.25,0.3,0,0.7)
	ContralShake(0.35,0.3,0,0.7)
	ContralShake(0.45,0.3,0,0.7)
	ContralShake(0.55,0.3,0,0.7)
	ContralShake(0.65,0.3,0,0.7)
	ContralShake(0.75,0.3,0,0.7)	
	ContralShake(0.85,0.3,0,0.7)	
	ContralShake(1.4,0.55,0,0.9)	
	
	
STake_Start("HG_SL_ST_skill_012_start","HG_SL_ST_skill_012")    --伏魔棒/JA1换  13950       动作打击点时间0.7     (SOFT建议时间1秒）
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
    Sound(0.5,"QZ_SW_Skill012_Start2")
    Sound(0.19,"HG_SoundVOC_attack2_1")
    Sound(0.09,"HG_THD_QMDJ_skill_001")
    Sound(0.57,"XLJ_Down_Skill_4")
 
	Fx(0,"HG_SL_ST_attack_000_fire") 
	Fx(0,"SG_SL_ST_skill_012_01","Reference",0.6,0,0,-180,0,0,0,0)  	
	Fx(0.55,"SG_SL_ST_skill_012_02","Reference",1,0,0,-100,0,0,0,0) 
	
    --CameraShake(0.7,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.08","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2")
    CameraShake(0.55,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 8")
	ContralShake(0.55,0.55,0,0.9)
	
STake_Start("HG_SL_ST_skill_012_fire","")
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
 
    AttackStart()	
    Hurt(0,"hurt_62",0,2)
	DirFx(0,"HG_SL_ST_skill_018_hit",25,1) 
	HurtBoneFx(0,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"BTS_CR_HIT_new_s1")	



--------------------------------------------------------------------------右键---------------------------------------------------------------------


STake_Start("HG_SL_ST_attack_000","HG_SL_ST_attack_000")     -------普攻1顿  13901  (SOFT建议时间0.23秒）   动作打击点时间 0.18
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  

    Fx(0.13,"FX_SL_ST_attack_000","",1,0,5,0,20,0,0,-15)
    Fx(0.21,"SG_SL_ST_attack_000","",1.1,0,0,5,-3,0,0,0,1)
	Sound(0.15,"HG_SL_ST_attack_000")
        Sound(0.17,"HG_GB_ST_Skill_000")
        Sound(0.19,"Wolf_L_move_004_1")
        Sound(0.12,"HG_SoundVOC_attack1_1")
    GroundSound(0.16,"Sound_step")
    GroundSound(0.24,"Sound_step")
	
    Charge(0,0.085,15,30,12)  --0.1~ 0.185
    
    AttackStart()
		Pause(0.19,0.1,0,0.01)
    Hurt(0.185,"hurt_62",0,3)
	DirFx(0.185,"FX_SL_ST_attack_000_hit",28,1,1,-90,20,0)
--	HurtBoneFx(0.185,"FX_Violent_blood_Blunt_R1","Spine1",1.4,0,0,0,0,0,0)
    HurtSound(0.185,"BTS_CR_HIT_new_1_1")	

STake_Start("HG_SL_ST_attack_001","HG_SL_ST_attack_001")     -------普攻2顿  13902   (SOFT建议时间0.21秒）   动作打击点时间 0.18(0.145)
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
    PlaySpeed(0.11,0.27,2)

    Fx(0.21,"FX_SL_ST_attack_000","",1,0,-5,0,20,0,0,180)
    Fx(0.29,"SG_SL_ST_attack_001","",1.1,0,0,0,-5,0,0,0,1)
	Sound(0.15,"HG_SL_ST_attack_000")
        Sound(0.17,"HG_GB_ST_Skill_000")
        Sound(0.19,"Wolf_L_move_004_1")
        Sound(0.12,"HG_SoundVOC_attack1_1")
    GroundSound(0.11,"Sound_step")
    GroundSound(0.17,"Sound_step")
	ContralShake(0.55,0.3,0,0.9)	
    Charge(0,0.085,18,30,12) --0.07~0.18
    
    AttackStart()
			Pause(0.15,0.1,0,0.01)
    Hurt(0.145,"hurt_62",0,4)
	DirFx(0.145,"FX_SL_ST_attack_000_hit",28,1,1,90,0,0)	
	HurtBoneFx(0.145,"FX_blood_gun_b","Spine1",1,0,0,0,-90,0,0)
    HurtSound(0.145,"BTS_CR_HIT_new_1_1")	

STake_Start("HG_SL_ST_attack_002","HG_SL_ST_attack_002")     -------普攻3顿  13903  (SOFT建议时间0.6秒）   动作打击点时间 0.1|0.44  0.386
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  
  
    PlaySpeed(0.12,0.24,1.5)
    PlaySpeed(0.4,0.56,1.5)

    Fx(0.45,"FX_SL_ST_attack_000","",1,0,2,-10,25,10,0,-180)
    --Fx(0.45,"SG_SL_ST_attack_001","",1.1,0,5,-7,-5,-45,0,0,2.5)
    Fx(0.5,"SG_SL_ST_attack_002","",1.1,0,-3,5,-2,-10,0,0,1)	
	Sound(0.05,"HG_SL_ST_attack_003")
        Sound(0.3,"HG_GB_ST_Skill_001")
        Sound(0.19,"Wolf_L_move_004_1")
        Sound(0.12,"HG_SoundVOC_attack2_1")
    GroundSound(0.1,"Sound_step")
    GroundSound(0.32,"Sound_step") 
	
    Charge(0,0.286,15,30,12) --0.1~0.44
	
    AttackStart()
			Pause(0.395,0.15,0,0.01)
--    Hurt(0.1,"hurt_21",0,4)
    Hurt(0.386,"hurt_62",0,4)	
--	DirFx(0.1,"FX_SL_ST_attack_000_hit",28,1,1,90,0,0)
	DirFx(0.386,"FX_SL_ST_attack_000_hit",28,1,1,90,0,0)
--	HurtBoneFx(0.386,"FX_Violent_blood_Blunt_R1","Spine1",1.4,0,0,0,0,0,0)	
--    HurtSound(0.1,"BTS_CR_HIT_new_1_1")	
    HurtSound(0.386,"BTS_CR_HIT_new_2_1")	

STake_Start("HG_SL_ST_skill_008","HG_SL_ST_skill_008")    --雷霆/右键普攻4     13942  动作打击点时间0.54     (SOFT建议时间1.2秒）
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
 
	Fx(0,"SG_SL_ST_skill_008_fire","",0.55) 

    Charge(0,0.35,25,30,12)
	
    Sound(0.03,"QZ_PA_Skill_001_FX")
    Sound(0.08,"HG_SL_ST_Skill_002")
    Sound(0.02,"char_05_cloth_flap_01")
    Sound(0.3,"QZ_PA_Skill_004_2")
    Sound(0.35,"HG_SoundVOC_attack2_1")
    GroundSound(0,"Sound_jump")  
    GroundSound(0.54,"Sound_fall")
	
	CameraShake(0.54,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 7")
	
    AttackStart()	
			Pause(0.6,0.2,0,0.1)
    Hurt(0.55,"hurt_81",0,2)
	DirFx(0.55,"HG_SL_ST_skill_018_hit",25,0.8) 
	HurtBoneFx(0.55,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.55,"BTS_CR_HIT_new_e1")	

		
	
STake_Start("HG_SL_ST_skill_003","HG_SL_ST_skill_003")     -------三连击/JA1	13909
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  
	
	
--	PlaySpeed(0.28,0.36,2)	
--	PlaySpeed(0.37,0.49,0.8)	
--	PlaySpeed(0.5,0.62,2)	
--	PlaySpeed(0.96,0.98,0.08)	
--	PlaySpeed(0.96,1.18,2)	
	PlaySpeed(0.953,1.4,1.8)

    Fx(0.25,"FX_SL_ST_attack_000","",1.0,0,0,0,20,0,0,180)
    Fx(0.49,"FX_SL_ST_attack_000","",1.1,0,0,0,20,0,0,180)
    Fx(1.08,"FX_SL_ST_attack_000","",1.2,0,0,0,20,0,0,180)
    Fx(1.1,"FX_SL_ST_attack_000","",1.2,0,0,0,20,0,0,180)
    Fx(0,"SG_SL_ST_skill_003_fire_01")
	Fx(0.3,"SG_SL_ST_skill_003_fire_02")
	Fx(0.8,"SG_SL_ST_skill_003_fire_03")
	
    Charge(0,0.261,20,20,12) 
    Charge(0.783,0.122,20,20,12) 
	
    Sound(0.01,"QZ_SW_Skill_009")
    Sound(0.03,"HG_SoundVOC_attack4_1")
    Sound(0.973,"HG_SoundVOC_attack3_1")
    Sound(0.050,"HG_SL_ST_Skill_009")
    Sound(0.40,"HG_SL_ST_Skill_005")
    Sound(0.050,"HG_SL_ST_Skill_009")
    
    AttackStart()
    Hurt(0.18,"hurt_21",0,3)
    Hurt(0.35,"hurt_62",0,3)	
    Hurt(0.85,"hurt_62",0,3)	
	--DirFx(0.278,"HG_SL_ST_attack_hit",25,0.6) 
	--DirFx(0.554,"HG_SL_ST_attack_hit",25,0.6) 	
	--DirFx(1.115,"HG_SL_ST_attack_hit",25,0.7) 
	DirFx(0.18,"FX_SL_ST_attack_000_hit",35,1,1,90,0,0)	
	DirFx(0.35,"FX_SL_ST_attack_000_hit",30,1,1.5,90,0,0)	
	DirFx(0.85,"FX_SL_ST_attack_000_hit",30,1,1.5,90,0,0)
	HurtBoneFx(0.85,"FX_blood_gun_c","Spine1",1,0,0,0,-90,0,0)	
    HurtSound(0.18,"BTS_CR_HIT_new_2_1")		
    HurtSound(0.35,"BTS_CR_HIT_new_2_1")		
    HurtSound(0.85,"BTS_CR_HIT_new_s1")	
    Pause(0.3,0.1,1,1) 	
    Pause(0.5,0.1,1,1)
    Pause(1.08,0.2,1,0.5)	
	
	
STake_Start("HG_SL_ST_skill_001","HG_SL_ST_skill_001")     -------重劈/JA2   start   13906   (SOFT建议时间0.5秒）   动作打击点时间 0.6
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  

    PlaySpeed(0.31,0.35,0.33)	
	
    GroundSound(0,"Sound_step")	
	Fx(0,"SG_SL_ST_skill_001_all")
    Sound(0.03,"HG_SL_ST_Skill_009")
    Sound(0.2,"HG_SL_ST_Skill_005")
    Sound(0.53,"HG_SoundVOC_attack3_1")
	GroundSound(0.43,"Sound_step") 

    Charge(0,0.08,20,30,12)  
   --PlaySpeed(0.6,0.62,0.15)   
    AttackStart()
    Pause(0.6,0.15,1,0.05) 	
    Hurt(0.6,"hurt_81",0,4)
	DirFx(0.6,"FX_SL_ST_attack_000_hit",30,1,1.5,90,0,0)
	HurtBoneFx(0.6,"FX_blood_gun_c","Spine1",1,0,0,0,-90,0,0)	
    HurtSound(0.6,"BTS_CR_HIT_new_s1")		
	
--------------------------------------------------------------------------键盘---------------------------------------------------------------------


	

STake_Start("HG_SL_ST_skill_002","HG_SL_ST_skill_002")     -------回避反击/键盘2	13908
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  

    PlaySpeed(0,0.48,2)
    PlaySpeed(0.42,0.555,0.5)
    PlaySpeed(0.556,0.795,2)

	
	Fx(0,"SG_SL_ST_skill_002","",1,0,0,0,0,0,0,0,1.5)	
    --Fx(0.35,"HG_SL_ST_attack_002_fire01")
    Fx(0.45,"SG_SL_ST_skill_000_fire","",0.5,0,0,0,0,0,0,0,3)

	
    Charge(0,0.24,10,180,16) 
	Charge(0.432,0.02,200,25,12) 
	
	MotionBlur(0.05,0.3,1)
	
    GroundSound(0.32,"Sound_step")
    GroundSound(0.6,"Sound_step") 
  
        
    Sound(0.04,"char_05_cloth_flap_01")
    Sound(0.03,"HG_GB_ST_Skill_007")
    Sound(0.25,"HG_SoundVOC_attack2_1")

    AttackStart()
    Hurt(0.52,"hurt_21",0,3)
	DirFx(0.52,"HG_SL_ST_skill_016_hit",25,1) 
    HurtSound(0.52,"BTS_CR_HIT_new_s1")	
	Pause(0.5,0.12,0,0.05) 	
    CameraShake(0.86,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 8")
	ContralShake(0.86,0.3,0,0.8)

	


    
STake_Start("HG_SL_ST_skill_009_start+","HG_SL_ST_skill_016")    --月轮·改/键盘3  13945       动作打击点时间0.7|0.8     (SOFT建议时间 0.47秒）
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  

    Sound(0.03,"HG_SL_ST_Skill_012")
    Sound(0.03,"HG_SoundVOC_attack5_1")
    Sound(0.53,"HG_GB_PA_skill_005")
    Sound(0.52,"HG_SL_ST_Skill_016")

	PlaySpeed(0,0.45,4)
 
	Fx(0.3,"SG_SL_ST_skill_009_fire") 
    Fx(0.45,"FX_SL_ST_attack_000","",1.1,0,0,0,20,0,0,0)
    Fx(0.51,"FX_SL_ST_attack_000","",1.3,0,0,0,20,0,180,0)
    Fx(0,"Fox_skill_002_start","",3) 
    --Pause(0.76,0.15,1,0.005)	
    CameraShake(0.42,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 7")
	ContralShake(0.42,0.35,0,0.8)
	
STake_Start("HG_SL_ST_skill_009_fire+","")
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  


    AttackStart()	
    --Pause(0.76,0.15,1,0.005)	
    Hurt(0,"hurt_71",0,4)
	--DirFx(0,"SG_SL_ST_skill_009_hit",25) 	
	DirFx(0,"FX_SL_ST_attack_000_hit",30,1,1.5,90,0,0)	
    HurtSound(0,"BTS_CR_HIT_new_s1")
	
	
	
STake_Start("HG_SL_ST_skill_009_start++","HG_SL_ST_skill_016")    --月蚀		13987	徽章反击
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  

    Sound(0.03,"HG_SL_ST_Skill_012")
    Sound(0.03,"HG_SoundVOC_attack5_1")
    Sound(0.53,"HG_GB_PA_skill_005")
    Sound(0.52,"HG_SL_ST_Skill_016")

	PlaySpeed(0,0.45,4)
 
	Fx(0.3,"SG_SL_ST_skill_009_fire") 
    Fx(0.45,"FX_SL_ST_attack_000","",1.1,0,0,0,20,0,0,0)
    Fx(0.51,"FX_SL_ST_attack_000","",1.3,0,0,0,20,0,180,0)
    Fx(0,"Fox_skill_002_start","",3) 
    --Pause(0.76,0.15,1,0.005)	
    CameraShake(0.42,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 7")
	ContralShake(0.42,0.35,0,0.8)
	
STake_Start("HG_SL_ST_skill_009_fire++","")
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  


    AttackStart()	
    --Pause(0.76,0.15,1,0.005)	
    Hurt(0,"hurt_71",0,4)
	--DirFx(0,"SG_SL_ST_skill_009_hit",25) 	
	DirFx(0,"FX_SL_ST_attack_000_hit",30,1,1.5,90,0,0)	
    HurtSound(0,"BTS_CR_HIT_new_s1")	
    

	
STake_Start("HG_SL_ST_skill_019","HG_SL_ST_skill_019")     --大招EXP   13940
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
    Sound(0.21,"QZ_PA_Skill_007_2")
    Sound(0.02,"QZ_PA_Skill_004_2")
    Sound(0.02,"HG_SoundVOC_attack5_1")
 

  	Fx(0,"HG_SL_ST_shibatongren_fire")

    AttackStart()	
    Hurt(0.25,"hurt_81",0,4)
	DirFx(0.25,"HG_SL_ST_skill_018_hit",25,0.8) 
    HurtSound(0.25,"BTS_CR_HIT_new_s1")




STake_Start("HG_SL_ST_skill_017","HG_SL_ST_skill_017")    --后滑      13980	 打击点0.08
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 	

    Sound(0.03,"HG_SL_ST_Skill_014")
    Sound(0.04,"HG_SoundVOC_attack1_1")
    Sound(0.03,"char_05_cloth_flap_01")
    GroundSound(0.42,"Sound_brake")

	Fx(0.01,"SG_SL_ST_skill_017","Reference",1,0,0,0,0,0,0,0,1)
	
    Charge(0.25,0.2,180,180,4)
	
    AttackStart()	
    Hurt(0.08,"hurt_21",0,1)
    Hurt(0.25,"hurt_81",0,1)	
	DirFx(0.08,"HG_SL_ST_skill_018_hit",25,1) 
	DirFx(0.25,"HG_SL_ST_skill_018_hit",25,1) 	
    HurtSound(0.08,"SL_ST_Hit_1-3")	
    HurtSound(0.25,"SL_ST_Hit_1-3")		
	
	




--[[STake_Start("HG_SL_ST_skill_004","HG_SL_ST_skill_004")    --13952    小无双      打击点1.93     
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
    Sound(0.03,"HG_SL_ST_Skill_008")
    Sound(0.53,"HG_SL_ST_Skill_002")
    Sound(1.38,"HG_SL_ST_Skill_005")
    Sound(0.11,"HG_SL_ST_Skill_007")
    Sound(1.8,"HG_SoundVOC_attack2_1")

  	Fx(0,"SG_SL_ST_skill_004_01")
	Fx(1,"SG_SL_ST_skill_004_02")
	
    Charge(1.4,0.2,15,5,12) 
    Charge(1.798,0.2,25,5,12) 
    Charge(2.349,0.15,25,5,12) 
	
   --PlaySpeed(1.9,1.94,0.15)
	
    AttackStart()	
    Hurt(1.9,"hurt_81",0,3)
    Pause(1.9,0.266,1,0.15) 	
	--DirFx(1.93,"HG_SL_ST_skill_018_hit",25,0.8) 
	DirFx(1.9,"FX_SL_ST_attack_000_hit",30,1,1.5,-90,0,0)		
    HurtSound(1.9,"SL_ST_Hit_1-3")		]]--
	

	
	
	
STake_Start("HG_SL_ST_skill_020","HG_SL_ST_skill_020")    --蓄力突袭	14440      	 
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 	
	Sound(0.03,"HG_SoundVOC_attack5_1")
	Sound(0.03,"HG_SL_ST_Skill_001")
	
	Fx(0,"SG_SL_ST_skill_020","",1,1,0,0,0,0,0,0,1)
	Fx(0,"Fox_skill_002_start","",1) 
	Fx(1.5,"SG_SL_ST_skill_020","",1,1,0,0,0,0,0,0,1)
	Fx(1.5,"Fox_skill_002_start","",1) 	
	
	
STake_Start("HG_SL_ST_skill_000_fire+","HG_SL_ST_skill_000_fire+")     -------蓄力突袭   14441
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  

	Charge(0,0.13,250,30,12)	--0
	
    Fx(0.14,"SG_SL_ST_skill_000_fire","",0.7)	
    Sound(0,"HG_GB_ST_Skill_007")
    Sound(0,"HG_SL_ST_Skill_016")
    Sound(0.25,"HG_SoundVOC_attack2_1")
    GroundSound(0.14,"Sound_step")
    GroundSound(0.2,"Sound_step")	
    EffectStart(0.2, "FOV", -10)
    MotionBlur(0.14,0.255,0.5)	
	
	AttackStart()	
		Pause(0.16,0.15,0,0.01)
    Hurt(0.18,"hurt_62",0,1)	
	DirFx(0.15,"HG_SL_ST_skill_018_hit",25,1) 
	HurtBoneFx(0.18,"FX_blood_gun_c","Spine1",1,0,0,0,180,0,0)
    HurtSound(0.18,"SL_ST_Hit_1-3")	
	
	CameraShake(0.1,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.2","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 5")

		
		
STake_Start("HG_SL_ST_skill_021_KO","HG_SL_ST_skill_021_KO")    --上天入地地面Q      14436		 打击点0.25|0.58
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 		
	Sound(0.03,"HG_SL_ST_Skill_005")
    Sound(0.21,"HG_SoundVOC_attack1_1")
    Sound(0.61,"HG_SoundVOC_attack2_1")
	
	Charge(0,0.07,30,10,12)	--0.15
	Charge(0.01,0.03,30,10,12)	--0.54
	Fx(0,"SG_SL_ST_skill_021_KO")
	
    AttackStart()	
		Pause(0.7,0.15,0,0.01)
    Hurt(0.3,"hurt_81",0,1)	
	Hurt(0.6,"hurt_81",0,1)	
	DirFx(0.25,"HG_SL_ST_skill_018_hit",25,1) 	
	DirFx(0.58,"HG_SL_ST_skill_018_hit",25,1) 
    HurtSound(0.3,"SL_ST_Hit_1-3")	
	HurtSound(0.6,"SL_ST_Hit_1-3")	
	
	
	
	
STake_Start("HG_SL_ST_skill_022","HG_SL_ST_skill_022")    --锁喉功      14446		 打击点0.01|1.67
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 		
	Sound(0.4,"HG_SL_ST_Skill_007")
	Sound(1.2,"HG_SL_ST_Skill_003")
	Sound(1.64,"HG_SoundVOC_attack2_1")
	
	Fx(0,"SG_SL_ST_skill_022_fire1")
	Fx(1.65,"SG_SL_ST_skill_022_fire2")
	
    AttackStart()	
		Pause(0.1,0.15,0,0.01)
    Hurt(0.01,"hurt_62",0,1)
    Hurt(0.55,"hurt_62",0,1)
    Hurt(0.8,"hurt_62",0,1)	
	DirFx(0.1,"HG_SL_ST_skill_018_hit",25,1) 
	DirFx(0.55,"HG_SL_ST_skill_018_hit",25,1) 
	DirFx(0.8,"HG_SL_ST_skill_018_hit",25,1) 	
	HurtBoneFx(0.1,"FX_blood_gun_c","Spine1",1,0,0,0,90,0,0)
    HurtSound(0.01,"SL_ST_Hit_1-3")	
    HurtSound(0.55,"SL_ST_Hit_1-3")	
    HurtSound(0.8,"SL_ST_Hit_1-3")	
	
	CameraShake(1.7,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.3","ShakeAmplitudeX = 10","ShakeAmplitudeY = 10","ShakeAmplitudeZ = 15")
	
	


STake_Start("HG_SL_ST_skill_026_start","HG_SL_ST_skill_026")    --轮回棍舞      14461		 打击点0.1+0.25*8
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 		
	Loop()
	Sound(0.03,"HG_SL_ST_Skill_008_loop")
	Sound(0.03,"HG_SL_ST_attack_000")
	
	Fx(0,"SG_SL_ST_skill_026","",1)		
	
    AttackStart()	
		Pause(0.15,0.15,0,0.01)
		Pause(0.4,0.15,0,0.01)
		Pause(0.65,0.15,0,0.01)
		Pause(0.9,0.15,0,0.01)
		Pause(1.15,0.15,0,0.01)
		Pause(1.4,0.15,0,0.01)
		Pause(1.65,0.15,0,0.01)
		Pause(1.9,0.15,0,0.01)
		Pause(2.15,0.15,0,0.01)
	Hurt(0.15,"hurt_62",0,1)	
	Hurt(0.4,"hurt_62",0,1)	
	Hurt(0.65,"hurt_62",0,1)	
	Hurt(0.9,"hurt_62",0,1)	
	Hurt(1.15,"hurt_62",0,1)	
	Hurt(1.4,"hurt_62",0,1)	
	Hurt(1.65,"hurt_62",0,1)	
	Hurt(1.9,"hurt_62",0,1)	
	Hurt(2.15,"hurt_62",0,1)	
	DirFx(0.1,"SG_SL_ST_skill_011_hit",25,1) 
	DirFx(0.35,"SG_SL_ST_skill_011_hit",25,1) 
	DirFx(0.6,"SG_SL_ST_skill_011_hit",25,1) 
	DirFx(0.85,"SG_SL_ST_skill_011_hit",25,1) 
	DirFx(1.1,"SG_SL_ST_skill_011_hit",25,1) 
	DirFx(1.35,"SG_SL_ST_skill_011_hit",25,1) 
	DirFx(1.6,"SG_SL_ST_skill_011_hit",25,1) 
	DirFx(1.85,"SG_SL_ST_skill_011_hit",25,1) 
	DirFx(2.1,"SG_SL_ST_skill_011_hit",25,1) 
	HurtBoneFx(0.1,"FX_blood_gun_b","Spine1",1,0,0,0,90,0,0)
	HurtBoneFx(0.35,"FX_blood_gun_b","Spine1",1,0,0,0,90,0,0)
	HurtBoneFx(0.6,"FX_blood_gun_b","Spine1",1,0,0,0,90,0,0)
	HurtBoneFx(0.9,"FX_blood_gun_b","Spine1",1,0,0,0,90,0,0)
	HurtBoneFx(1.15,"FX_blood_gun_b","Spine1",1,0,0,0,90,0,0)
	HurtBoneFx(1.4,"FX_blood_gun_b","Spine1",1,0,0,0,90,0,0)
	HurtBoneFx(1.65,"FX_blood_gun_b","Spine1",1,0,0,0,90,0,0)
	HurtBoneFx(1.9,"FX_blood_gun_b","Spine1",1,0,0,0,90,0,0)
	HurtBoneFx(2.15,"FX_blood_gun_b","Spine1",1,0,0,0,90,0,0)
    HurtSound(0.15,"SL_ST_Hit_1-3")	
	HurtSound(0.4,"SL_ST_Hit_1-3")	
	HurtSound(0.65,"SL_ST_Hit_1-3")	
	HurtSound(0.9,"SL_ST_Hit_1-3")	
	HurtSound(1.15,"SL_ST_Hit_1-3")	
	HurtSound(1.4,"SL_ST_Hit_1-3")	
	HurtSound(1.65,"SL_ST_Hit_1-3")	
	HurtSound(1.9,"SL_ST_Hit_1-3")	
	HurtSound(2.15,"SL_ST_Hit_1-3")	
	
	Charge(0,0.1,5,0,21)
	
STake_Start("HG_SL_ST_skill_026_fire","HG_SL_ST_wait_000")      
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait")  


STake_Start("HG_SL_ST_skill_026_Q","HG_SL_ST_skill_026_Q")    --奈落      14463		 
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 			
    Sound(0.11,"HG_SL_ST_Skill_000")
    Sound(0.02,"QZ_PA_Skill_004_2")
    Sound(0.02,"HG_SoundVOC_attack5_1")
 
	
	PlaySpeed(0.25,0.3,0.3)
	Charge(0,0.3,30,10,12)
	Fx(0.4,"SG_SL_ST_skill_026_Q_start","Reference",1,1,0,0,0,0,0,0,1,1)
--	Fx(0.35,"SG_SL_ST_skill_032_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
--	Fx(0.5,"SG_SL_ST_skill_032_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	
	AttackStart()	
    Hurt(0.416,"hurt_21",0,1)	
	Hurt(0.59,"hurt_21",0,1)
	Hurt(0.767,"hurt_21",0,1)
	DirFx(0.416,"HG_SL_ST_skill_018_hit",25,1) 	
	DirFx(0.59,"HG_SL_ST_skill_018_hit",25,1) 	
	DirFx(0.767,"HG_SL_ST_skill_018_hit",25,1) 	
    HurtSound(0.416,"SL_ST_Hit_1-3")	
	HurtSound(0.59,"SL_ST_Hit_1-3")	
	HurtSound(0.767,"SL_ST_Hit_1-3")	
	


	

STake_Start("HG_SL_ST_skill_027+","HG_SL_ST_skill_009")    --新月轮		      14466		 打击点0.1+0.25*8
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 		
	Sound(0.03,"HG_SL_ST_Skill_007")
	
--	Fx(0.2,"SG_SL_ST_skill_016_fire","",2,1,0,0,0,0,0,0,0.5,0)	
	
	
	
STake_Start("HG_SL_ST_skill_034","HG_SL_ST_skill_034")    --净地      14483
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 		
	Sound(0.03,"HG_SL_SB_Skill_011_loop")
	Sound(0.03,"Skill_Fire_FireBall_01")
	Sound(0.03,"HG_SL_ST_Skill_009")
	
	Fx(0,"SG_SL_ST_skill_034_start")	
	
STake_Start("HG_SL_ST_skill_036","HG_SL_ST_skill_036")    --净地封锁      14487
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 		
	Sound(0.03,"HG_SL_SB_Skill_011_loop")
	Sound(0.03,"HG_SoundVOC_attack1_1")	
	
	
STake_Start("HG_SL_ST_skill_035","HG_SL_ST_skill_035")    --入定      14490
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 	
	Sound(0.03,"HG_SL_SB_Skill_011_loop")
	Sound(0.03,"HG_SoundVOC_attack1_1")	
	Sound(0.03,"HG_SL_ST_Skill_011_2")
	Sound(2.8,"HG_SL_ST_Skill_013")	
	Sound(2.8,"HG_SL_SB_Skill_013")
	Sound(3.23,"QZ_SW_QXQJ_fire")
	
	Fx(0,"SG_SL_ST_skill_035_start")		
	LoopFx(0.5,3.8,1,"SG_SL_ST_skill_035_loop1","Reference")
	LoopFx(0.5,3.8,1,"SG_SL_ST_skill_035_loop","Reference")
	Fx(3.2,"SG_SL_ST_skill_035_fire")	
	PlaySpeed(1,3,0.7)
	
	AttackStart()
    Hurt(4.1,"hurt_62",0,2)
	DirFx(4.1,"HG_SL_ST_attack_hit",25,0.7) 
    HurtSound(4.1,"HG_SL_ST_Hit_000")	
	
STake_Start("HG_SL_ST_skill_035_001","HG_SL_ST_skill_035_001")    --入定不完全      14491
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 	
	Sound(0.03,"HG_SoundVOC_attack1_1")	
	Sound(0.03,"HG_SL_ST_Skill_011_2")
	Sound(0.03,"HG_SL_SB_Skill_012_1")
		
	Fx(0,"SG_SL_ST_skill_035_fire")
	
	AttackStart()
    Hurt(0.3,"hurt_62",0,2)
	DirFx(0.3,"HG_SL_ST_attack_hit",25,0.7) 
    HurtSound(0.3,"HG_SL_ST_Hit_000")		

	
	
STake_Start("HG_SL_ST_skill_028","HG_SL_ST_skill_028")    --如来神掌      14403		 
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 		
	Sound(0.13,"HG_SL_ST_Skill_002")
	Sound(0.73,"QZ_SW_Skill012_Start2")
	Sound(0.1,"HG_SoundVOC_attack1_1")
	Sound(0.7,"HG_SoundVOC_attack2_1")
	
	PlaySpeed(0,0.9,4)
	
	Fx(0.8,"SG_SL_ST_skill_028_start")	
	CameraShake(0.85,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.2","ShakeAmplitudeX = 10","ShakeAmplitudeY = 10","ShakeAmplitudeZ = 10")
	
	
	
STake_Start("HG_SL_ST_skill_029","HG_SL_ST_skill_029")    --如来神掌拍地      14475		 
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 	
	Sound(0.03,"QZ_SW_Skill012_Start2")
	Sound(0.03,"HG_SoundVOC_attack2_1")
	
	Fx(0.1,"SG_SL_ST_skill_028_start")	
	
	
	
STake_Start("HG_SL_ST_skill_030","HG_SL_ST_skill_030")    --如来神掌画圈      14478	 
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 		
	Sound(0.03,"QZ_SW_Skill012_Start2")
	Sound(0.03,"HG_SoundVOC_attack2_1")
	
	Fx(0.1,"SG_SL_ST_skill_028_start")		
	
------------------------------------------------------------功能键技能---------------------------------------------------------------------------	
	
   	
STake_Start("HG_SL_ST_attack_006","HG_SL_ST_attack_006")     -------直升机start  13934
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  


    Fx(0.73,"SG_SL_ST_skill_010_fire")	
	Sound(0.03,"HG_SL_ST_Skill_003")
    Sound(0.6,"HG_SoundVOC_attack2_1")	
    Sound(0.76,"SG_THD_SW_skill_010_Impact")
    GroundSound(0.75,"Sound_step")
 	
    --CameraShake(0.69,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.1","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2.5")
	ContralShake(0.69,0.65,0,0.9)	
    Charge(0,0.3,60,30,12)    --Charge(0.135,0.735)
	
    AttackStart()
    Hurt(0.735,"hurt_62",0,2)
	DirFx(0.735,"HG_SL_ST_attack_hit",25,0.7) 
    HurtSound(0.735,"HG_SL_ST_Hit_000")	
	
	

STake_Start("HG_SL_ST_skill_018","HG_SL_ST_skill_018")    --受身后滑      13991	 
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"1")     
	Priority(0.15,"5")
    BlendPattern("steady") 	
    Sound(0.010,"char_05_cloth_flap_02")
    Sound(0.010,"N2_BYJQ01_dodge_001")
	Sound(0.03,"HG_SoundVOC_Jump_1")

    Fx(0.1,"SG_SL_ST_skill_move","",1.2,0,0,-45,-3,0,0,0,1.6)
	
    Charge(0,0.15,100,180,16)
	
	
STake_Start("HG_SL_ST_skill_018+","HG_SL_ST_skill_018")    --空中受身      13992
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"1")     
	Priority(0.15,"5")
    BlendPattern("steady") 	
    Sound(0.010,"char_05_cloth_flap_02")
    Sound(0.010,"N2_BYJQ01_dodge_001")
	Sound(0.03,"HG_SoundVOC_Jump_1")

    Fx(0.1,"SG_SL_ST_skill_move","",1.2,0,0,-45,-3,0,0,0,1.6)	
    Charge(0,0.15,80,180,19,0,0,5)
	
STake_Start("HG_SL_ST_attack_B_003","HG_SL_ST_attack_B_003")    --轻功衔接技		13981
    BlendMode(0)
    BlendTime(0.03)  
	Priority(0,"5")
    BlendPattern("steady") 		
    Sound(0.010,"HG_GB_PA_skill_005")
	Sound(0.03,"HG_GB_ST_Skill_008")
    Sound(0.6,"HG_SoundVOC_attack3_1")	
	
	Charge(0,0.55,120,10,12,0,0,0)
	
    AttackStart()
    Hurt(0.27,"hurt_62",0,3)
	Hurt(0.43,"hurt_62",0,3)
	Hurt(0.6,"hurt_62",0,3)
	DirFx(0.27,"FX_SL_ST_attack_000_hit",25,1,1.5,-90,30,0)	
	DirFx(0.43,"FX_SL_ST_attack_000_hit",25,1,1.5,-90,30,0)
	DirFx(0.6,"FX_SL_ST_attack_000_hit",25,1,1.5,-90,30,0)
	HurtBoneFx(0.01,"FX_blood_gun_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.27,"BTS_CR_HIT_new_1_1")		
	HurtSound(0.43,"BTS_CR_HIT_new_1_1")	
	HurtSound(0.6,"BTS_CR_HIT_new_1_1")	
	
	
STake_Start("HG_SL_ST_skill_033","HG_SL_ST_skill_033")    --终结技	13985
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 		
	Sound(0,"Skill_Fire_FireBall_01")
	Sound(0.5,"Skill_Fire_FireBall_01")
	Sound(1.0,"Skill_Fire_FireBall_01")
	Sound(1.5,"Skill_Fire_FireBall_01")
	Sound(2,"Skill_Fire_FireBall_01")
	Sound(1.6,"HG_GB_PA_Skill_015")

	
	Fx(0,"SG_SL_ST_skill_033","",1,1,0,0,0,0,0,0,1)	
	CutScene(0.4,"SG_SL_ST_skill_033_new1")
	
	
	AttackStart()	
	Hurt(0.155,"hurt_81",0,1)	
	Hurt(0.46,"hurt_81",0,1)
	Hurt(0.78,"hurt_81",0,1)
	Hurt(1,"hurt_81",0,1)
	Hurt(1.3,"hurt_81",0,1)
	Hurt(1.61,"hurt_81",0,1)
	Hurt(1.81,"hurt_81",0,1)
	Hurt(1.93,"hurt_81",0,1)
	Hurt(2.05,"hurt_81",0,1)
	Hurt(2.2,"hurt_81",0,1)	
	DirFx(0.155,"SG_SL_ST_skill_009_hit",25,1) 
	DirFx(0.46,"SG_SL_ST_skill_009_hit",25,1) 
	DirFx(0.78,"SG_SL_ST_skill_009_hit",25,1) 
	DirFx(1,"SG_SL_ST_skill_009_hit",25,1) 
	DirFx(1.3,"SG_SL_ST_skill_009_hit",25,1) 
	DirFx(1.61,"SG_SL_ST_skill_009_hit",25,1) 
	DirFx(1.81,"SG_SL_ST_skill_009_hit",25,1) 
	DirFx(1.96,"SG_SL_ST_skill_009_hit",25,1) 
	DirFx(2.05,"SG_SL_ST_skill_009_hit",25,1) 
	DirFx(2.2,"SG_SL_ST_skill_009_hit",25,1) 
    HurtSound(0.155,"SL_ST_Hit_1-3")	
	HurtSound(0.46,"SL_ST_Hit_1-3")	
	HurtSound(0.78,"SL_ST_Hit_1-3")	
	HurtSound(1,"SL_ST_Hit_1-3")	
	HurtSound(1.3,"SL_ST_Hit_1-3")	
	HurtSound(1.61,"SL_ST_Hit_1-3")	
	HurtSound(1.81,"SL_ST_Hit_1-3")	
	HurtSound(1.93,"SL_ST_Hit_1-3")	
	HurtSound(2.05,"SL_ST_Hit_1-3")	
	HurtSound(2.2,"SL_ST_Hit_1-3")	
------------------------------------------------------------空中技能----------------------------------------------------------------------------
------------------------------------------------------------空中技能----------------------------------------------------------------------------
------------------------------------------------------------空中技能----------------------------------------------------------------------------


STake_Start("HG_SL_ST_air_attack_000_start","HG_SL_ST_air_attack_000")     -------空中普攻1  13970
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  
    Priority(0.22,"6")
	
	Sound(0.05,"HG_SL_ST_attack_000")
        Sound(0.07,"HG_GB_ST_Skill_000")
        Sound(0.09,"Wolf_L_move_004_1")
        Sound(0.02,"HG_SoundVOC_attack1_1")
    Fx(0.14,"FX_SL_ST_attack_000","",0.8,0,0,-25,27,0,0,-30,1)

STake_Start("HG_SL_ST_air_attack_000_fire","")     -------空中普攻1fire  (SOFT建议时间0.22秒）   动作打击点时间 0.17
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
 
    AttackStart()
    Hurt(0,"hurt_21",0,3)
	--DirFx(0,"HG_SL_ST_attack_hit",25,0.7) 
	DirFx(0,"FX_SL_ST_attack_000_hit",25,1,1.5,-90,30,0)	
	HurtBoneFx(0.01,"FX_blood_gun_b","Spine1",1,0,0,0,0,0,0)
    HurtSound(0,"BTS_CR_HIT_new_1_1")	


STake_Start("HG_SL_ST_air_attack_001_start","HG_SL_ST_air_attack_001")     -------空中普攻2  13970
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  
    Priority(0.3,"6")	

	Sound(0.04,"HG_SL_ST_attack_000")
        Sound(0.06,"HG_GB_ST_Skill_000")
        Sound(0.09,"Wolf_L_move_004_1")
        Sound(0.02,"HG_SoundVOC_attack1_1")
    Fx(0.14,"FX_SL_ST_attack_000","",0.8,0,0,-25,34,0,0,180,1)

STake_Start("HG_SL_ST_air_attack_001_fire","")     -------空中普攻2fire  (SOFT建议时间0.3秒）   动作打击点时间 0.17
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
 
    AttackStart()
    Hurt(0,"hurt_21",0,3)
	--DirFx(0,"HG_SL_ST_attack_hit",25,0.7) 
	DirFx(0,"FX_SL_ST_attack_000_hit",25,1,1.5,90,0,0)	
	HurtBoneFx(0.01,"FX_blood_gun_b","Spine1",1,0,0,0,-90,0,0)	
    HurtSound(0,"BTS_CR_HIT_new_1_1")	


STake_Start("HG_SL_ST_air_attack_002_start","HG_SL_ST_air_attack_002")     -------空中JA2  13970
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")  
    Priority(0.35,"6")	
		
        Sound(0.02,"HG_SoundVOC_attack2_1")	
	Sound(0.06,"HG_SL_ST_Skill_016")
        Sound(0.08,"HG_GB_ST_Skill_001")
	Sound(0.04,"Wolf_L_move_004_1")	

    Fx(0.23,"FX_SL_ST_attack_000","",0.8,0,-3,-30,37,0,0,30,1)

STake_Start("HG_SL_ST_air_attack_002_fire","")     -------空中JA2  (SOFT建议时间0.35秒）   动作打击点时间 0.27
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
 
    AttackStart()
    Hurt(0,"hurt_81",0,3)
	--DirFx(0,"HG_SL_ST_attack_hit",25,0.7) 
	DirFx(0,"FX_SL_ST_attack_000_hit",25,1,1.5,-90,-60,0)		
    HurtSound(0,"BTS_CR_HIT_new_2_1")	


	
STake_Start("HG_SL_ST_air_B_attack_000","HG_SL_ST_air_B_attack_000")     -------空中重击普攻  13973
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
 
	Charge(0.4,0.1,0,0,14)
 
 
    --Fx(0,"Fox_skill_002_start","",3) 
    --Pause(0.76,0.15,1,0.005)	
    CameraShake(0.62,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 8")
    Fx(0.5,"SG_SL_ST_skill_010_fire")	
	Sound(0.03,"HG_SL_ST_Skill_003")
    Sound(0.4,"HG_SoundVOC_attack2_1")	
    Sound(0.5,"SG_THD_SW_skill_010_Impact")
    GroundSound(0.5,"Sound_step")
 	
    CameraShake(0.5,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 7")
	ContralShake(0.5,0.65,0,0.9)
 
 
 
    AttackStart()
    Hurt(0.5,"hurt_81",0,3)
	--DirFx(0,"HG_SL_ST_attack_hit",25,0.7) 
	DirFx(0.5,"FX_SL_ST_attack_000_hit",25,1,1.5,90,0,0)
	HurtBoneFx(0.01,"FX_blood_gun_c","Spine1",1,0,0,0,180,0,0)
    HurtSound(0.5,"BTS_CR_HIT_new_1_1")	
	
	
	
STake_Start("HG_SL_ST_air_B_attack_000_ground","HG_SL_ST_air_B_attack_000")     -------右键循环终结 13907
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
 
    --Fx(0,"Fox_skill_002_start","",3) 
    --Pause(0.76,0.15,1,0.005)	
    CameraShake(0.62,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 10")
    Fx(0.5,"SG_SL_ST_skill_010_fire")	
	Sound(0.03,"HG_SL_ST_Skill_003")
    Sound(0.4,"HG_SoundVOC_attack2_1")	
    Sound(0.5,"SG_THD_SW_skill_010_Impact")
    GroundSound(0.5,"Sound_step")
 	
 
	ContralShake(0.5,0.65,0,0.9)
 
 
 
    AttackStart()
    Hurt(0.5,"hurt_62",0,3)
	--DirFx(0,"HG_SL_ST_attack_hit",25,0.7) 
	DirFx(0.5,"FX_SL_ST_attack_000_hit",25,1,1.5,90,0,0)
	HurtBoneFx(0.01,"FX_blood_gun_c","Spine1",1,0,0,0,180,0,0)
    HurtSound(0.5,"BTS_CR_HIT_new_1_1")		



	
	
STake_Start("HG_SL_ST_skill_032","HG_SL_ST_skill_032")     --雷诀    14409
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
    Sound(0.03,"HG_SoundVOC_attack2_1")
    Sound(0.13,"HYHY_02_Skill02_01")
	
	Fx(0,"SG_SL_ST_skill_032_fire","Reference",1,1,0,0,0,0,0,0,1,1)
    CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.05","ShakeAmplitudeX = 10","ShakeAmplitudeY = 10","ShakeAmplitudeZ = 15")	
	
	
    AttackStart()	
    Hurt(0.22,"hurt_81")
	DirFx(0.22,"HG_SL_ST_skill_018_hit",25,0.8) 
    HurtSound(0.22,"SL_ST_Hit_1-3")

	
	
STake_Start("HG_SL_ST_skill_032_Q","HG_SL_ST_skill_032_Q")     --雷切   14410
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
	
    CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 12")
    CameraShake(0.8,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 12")
    CameraShake(1.3,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 12")
    CameraShake(1.8,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 12")
    CameraShake(2.3,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 12")
    CameraShake(2.8,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 12")
	
	Fx(0.3,"SG_SL_ST_skill_032_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	Fx(0.8,"SG_SL_ST_skill_032_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	Fx(1.3,"SG_SL_ST_skill_032_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	Fx(1.8,"SG_SL_ST_skill_032_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	Fx(2.3,"SG_SL_ST_skill_032_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	Fx(2.8,"SG_SL_ST_skill_032_Q_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	Sound(0.13,"YTGZT_1_Thunder")
	Sound(1.53,"HYHY_02_Skill02_02")
	Sound(1.53,"HYHY_02_Skill02_01")
	
    AttackStart()	
    Hurt(0.3,"hurt_81")
	Hurt(0.8,"hurt_81")
	Hurt(1.3,"hurt_81")
	Hurt(1.8,"hurt_81")
	Hurt(2.3,"hurt_81")
	Hurt(2.8,"hurt_81")
	DirFx(0.3,"HG_SL_ST_skill_018_hit",25,0.8) 
	DirFx(0.8,"HG_SL_ST_skill_018_hit",25,0.8) 
	DirFx(1.3,"HG_SL_ST_skill_018_hit",25,0.8) 
	DirFx(1.8,"HG_SL_ST_skill_018_hit",25,0.8) 
	DirFx(2.3,"HG_SL_ST_skill_018_hit",25,0.8) 
	DirFx(2.8,"HG_SL_ST_skill_018_hit",25,0.8) 
    HurtSound(0.3,"SL_ST_Hit_1-3")
	HurtSound(0.8,"SL_ST_Hit_1-3")
	HurtSound(1.3,"SL_ST_Hit_1-3")
	HurtSound(1.8,"SL_ST_Hit_1-3")
	HurtSound(2.3,"SL_ST_Hit_1-3")
	HurtSound(2.8,"SL_ST_Hit_1-3")



STake_Start("HG_SL_ST_emo_001","HG_SL_ST_emo_001")     -------阿赖耶识  14418
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
	--Fx(0.3,"SG_SL_ST_emo_001_fire","Reference",1,1,0,0,0,0,0,0,1,1)
	Sound(0.2,"HG_SL_SB_Skill_006")
    Sound(0.2,"HG_SoundVOC_attack1_1")
	Sound(0.1,"HG_SL_ST_Skill_000")
	
	
STake_Start("HG_SL_ST_def","HG_SL_ST_def")   --格挡     13996
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")
	--Fx(0,"SG_SL_ST_def_wm_loop","Reference",1,1,0,0,0,0,0,0,0.5,1)
	Loop()
	
STake_Start("HG_SL_ST_def_wm","HG_SL_ST_def_wm")   --完美格挡     13996
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")  
	Sound(0.01,"st_def_wm")
	Fx(0,"SG_SL_ST_def_wm_hit","Reference",1,1,0,0,0,0,0,0,1,1)
	
	


STake_Start("HG_SL_ST_def_pt","HG_SL_ST_def_pt")   --普通格挡     13996
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady") 
	Sound(0.01,"st_def_pt")
	Fx(0,"SG_SL_ST_def_pt_hit","Reference",1,1,0,0,0,0,0,0,1,1)
	
STake_Start("HG_SL_ST_def_air","HG_SL_ST_def_air")   --air格挡     14023
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")
	--Fx(0,"SG_SL_ST_def_wm_loop","Reference",1,1,0,0,0,0,0,0,1,1)
	Loop()
	
STake_Start("HG_SL_ST_def_air_wm","HG_SL_ST_def_air_wm")   --air完美格挡    14023
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")  
	Sound(0.01,"st_def_wm")
	Fx(0,"SG_SL_ST_def_wm_hit","Reference",1,1,0,0,0,0,0,0,1,1)

	
STake_Start("HG_SL_ST_def_air_pt","HG_SL_ST_def_air_pt")   --air普通格挡     14023
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")  	
	Sound(0.01,"st_def_pt")
	Fx(0,"SG_SL_ST_def_pt_hit","Reference",1,1,0,0,0,0,0,0,1,1)

STake_Start("HG_SL_ST_def_001","HG_SL_ST_wait_000")      
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("wait")  
    Priority(0,"5") 
    FrameTime(0,0.001)	

STake_Start("HG_SL_ST_skill_031","HG_SL_ST_skill_031")   --吸星 14030
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	Fx(0.01,"FX_xihun_SL_01","Reference",1,1,0,0,0,0,0,0,1,3)
	Loop()
	Sound(0.02,"Skill_XiXing")

STake_Start("HG_SL_ST_HX_start","HG_SL_ST_HX")   --打坐Loop 10611
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	Loop(1.3,3.3)
	Fx(0.7,"FX_tiaoxi_SL","Reference",1,1,0,0,0,0,0,0,1,3)	
	Sound(0.01,"HG_SL_ST_Skill_001")
	Sound(0.74,"Life_Recover_Loop_1")


STake_Start("HG_SL_ST_HX_fire","HG_SL_ST_HX")   --打坐fire 10612
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
    FrameTime(3.3,3.633)	
	HurtHard(3.3,3.7)	
	
	
	
	