
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	self.TargetAngleDeg = 1
	self.StartLookDir = GMhVector3:new_local()
	
end

function UnInit(self)
	self.StartLookPos = nil
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	local camera = SD.GetCamera()
	local cameraPos = camera:GetPosition()
	self.StartLookDir:Set(camera:GetLook()  - cameraPos)
	
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)

end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then
		return
	end

	local camera = SD.GetCamera()
	local cameraPos = camera:GetPosition()
	local CurLookDir   = camera:GetLook()  - cameraPos

	local angleDeg = CurLookDir:AngleDeg(StartLookDir)
	if angleDeg > self.TargetAngleDeg then
		lout('angleDeg is ' .. tostring(angleDeg))
		self.completed = true
	end

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

