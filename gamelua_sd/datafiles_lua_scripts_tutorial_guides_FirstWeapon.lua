
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local openBag = indicatorFlow.createIndicatorSpec()
	openBag.frameName     = 'openBag'
	openBag.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openBag.frameText     = '获得了新武器！打开背包装备上它吧！'
	openBag.attachFunc    = self.OnOpenBagAttached
	openBag.attachFuncParam = self
	openBag.triggerFunc   = nil
	openBag.triggerFuncParam = nil
	openBag.triggerWin    = 'Root/MainMenu/PackageBtn'
	openBag.attachWin     = 'Root/MainMenu/PackageBtn'
	openBag.attachWinRoot = 'MainMenu_FunctionAndEXP'
	openBag.triggerKey    = 'PackageUI'
	openBag.priority      = 1

	local openBagEquipPage = indicatorFlow.createIndicatorSpec()
	openBagEquipPage.frameName     = 'openBagEquipPage'
	openBagEquipPage.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openBagEquipPage.frameText     = '点击切换至装备包裹'
	openBagEquipPage.attachFunc    = self.OnOpenBagPageEquipAttached
	openBagEquipPage.attachFuncParam = self
	openBagEquipPage.triggerFunc   = nil
	openBagEquipPage.triggerFuncParam = nil
	openBagEquipPage.triggerWin    = 'Root/PackageFrame/TagContainer1/Btn2_1_0'
	openBagEquipPage.attachWin     = 'Root/PackageFrame/TagContainer1/Btn2_1_0'
	openBagEquipPage.attachWinRoot = 'Root/PackageFrame'
	openBagEquipPage.priority      = 2
	
	local placeBagItemToPos = indicatorFlow.createIndicatorSpec()
	placeBagItemToPos.frameName     = 'placeBagItemToPos'
	placeBagItemToPos.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeBagItemToPos.frameText     = '右键点击新武器将它装备上 '
	placeBagItemToPos.attachFunc    = nil
	placeBagItemToPos.attachFuncParam = nil
	placeBagItemToPos.triggerFunc   = nil
	placeBagItemToPos.triggerFuncParam = nil
	placeBagItemToPos.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeBagItemToPos.attachItemID  = nil
	placeBagItemToPos.priority      = 3
	
	self.indicatorSpecs = {
		openBag, openBagEquipPage, placeBagItemToPos
	}
	self.WeaponsItems = {
		[WM_PROF_XIANGLONG] = '5_204',
		[WM_PROF_BAIZHANG] = '5_207',
		[WM_PROF_YAOGUANG] = '5_206',	
		[WM_PROF_JIYING] = '5_205',	
	}
	self.placeBagItemToPosSpec = placeBagItemToPos
	self.indicatorFlows = nil
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	local unit = GetPlayersUnit()
	if not unit or not unit.GetProfession then
		lout('FirstWeapon:Enter - GetProfession failed')
		self.indicatorFlows = {}
		return
	end
	local eProf = unit:GetProfession()
	self.placeBagItemToPosSpec.attachItemID = self.WeaponsItems[eProf]
	-- Create Indicator
	self.downIndicator = ui.createFrame('DownIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.downIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
	
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function OnOpenBagAttached(self, indicator)
	local pWin = InitWindowPtr('Root/PackageFrame')
	if pWin and pWin:isVisible() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenBagPageEquipAttached(self, indicator)
	local pBagItemWin = SD.GetFirstBagItemButtonContainer(self.placeBagItemToPosSpec.attachItemID)
	if pBagItemWin then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end


function Update(self, fTime)
	if self.completed then
		return
	end
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

