
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local openPharmacyUI = indicatorFlow.createIndicatorSpec()
	openPharmacyUI.frameName     = 'openPharmacyUI'
	openPharmacyUI.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openPharmacyUI.frameText     = '打开界面'
	openPharmacyUI.attachFunc    = self.OnPharmacyUIAttached
	openPharmacyUI.attachFuncParam = self
	openPharmacyUI.triggerFunc   = nil
	openPharmacyUI.triggerFuncParam = nil
	openPharmacyUI.triggerWin    = 'Craft_Fabrication/Confirm'
	openPharmacyUI.attachWin     = 'Craft_Fabrication/Confirm'
	openPharmacyUI.attachWinRoot = 'Craft_Fabrication'
	openPharmacyUI.priority      = 0
	
	local placeMainItem = indicatorFlow.createIndicatorSpec()
	placeMainItem.frameName     = 'PlaceMainItem'
	placeMainItem.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeMainItem.frameText     = '点击放置主要材料'
	placeMainItem.attachFunc    = nil
	placeMainItem.attachFuncParam = nil
	placeMainItem.triggerFunc   = nil
	placeMainItem.triggerFuncParam = nil
	placeMainItem.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeMainItem.attachItemID  = '4_730'
	placeMainItem.dependWin     = 'Craft_Fabrication'
	placeMainItem.priority      = 1
	placeMainItem.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Fabrication', false)
	placeMainItem.fallbackFuncParam = self
	placeMainItem.fallbackPri   = 0
	
	local placeAssitItem = indicatorFlow.createIndicatorSpec()
	placeAssitItem.frameName     = 'PlaceAssistItem'
	placeAssitItem.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeAssitItem.frameText     = '点击放置辅助材料'
	placeAssitItem.attachFunc    = nil
	placeAssitItem.attachFuncParam = nil
	placeAssitItem.triggerFunc   = nil
	placeAssitItem.triggerFuncParam = nil
	placeAssitItem.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeAssitItem.attachItemID  = '4_729'
	placeAssitItem.dependWin     = 'Craft_Fabrication'	
	placeAssitItem.priority      = 2
	placeAssitItem.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Fabrication', false)
	placeAssitItem.fallbackFuncParam = self
	placeAssitItem.fallbackPri   = 0
	
	local confirmIdentify = indicatorFlow.createIndicatorSpec()
	confirmIdentify.frameName     = 'ConfirmIdentify'
	confirmIdentify.frameType     = indicatorFlow.INDICATOR_FRAME_UP
	confirmIdentify.frameText     = '点击开始制药'
	confirmIdentify.attachFunc    = nil
	confirmIdentify.attachFuncParam = nil
	confirmIdentify.triggerFunc   = nil
	confirmIdentify.triggerFuncParam = nil
	confirmIdentify.triggerWin    = 'Craft_Fabrication/Confirm'
	confirmIdentify.attachWin     = 'Craft_Fabrication/Confirm'
	confirmIdentify.attachWinRoot = 'Craft_Fabrication'
	confirmIdentify.priority      = 3
	confirmIdentify.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Fabrication', false)
	confirmIdentify.fallbackFuncParam = self
	confirmIdentify.fallbackPri   = 0
	
	self.indicatorSpecs = {
		openPharmacyUI, placeMainItem, placeAssitItem, confirmIdentify
	}
	
	self.indicatorFlows = nil
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnPharmacyUIAttached(self, indicator)
	indicator.bTriggered = true
	indicator.pFrame:Hide()
end	

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

