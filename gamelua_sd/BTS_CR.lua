----------------------------------CR Male------------------------------------------ 
STake_Start("HG_wait_000_BTSCR","HG_wait_000_BTSCR")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait_none")    
    
STake_Start("HG_wait_000_BTSCR_ui","HG_wait_000_BTSCR")
    BlendMode(0)
    BlendTime(0)
    Loop() 
    BlendPattern("wait_none")        

STake_Start("HG_BTS_CR_wait_000","HG_BTS_CR_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait_none")     
    
STake_Start("HG_BTS_CR_wait_000_ui","HG_BTS_CR_wait_000")
    BlendMode(0)
    BlendTime(0)
    Loop()
    BlendPattern("wait_none")    

STake_Start("HG_emo_000_BTSCR","HG_emo_000_BTSCR")
    BlendMode(0)
    BlendTime(0.2)



STake_Start("HG_emo_001_BTSCR","HG_emo_001_BTSCR")
    BlendMode(0)
    BlendTime(0.2)    
    
    
STake_Start("HG_BTS_CR_move_001","HG_BTS_CR_move_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")         
    
    --GroundFx(0.840, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.528, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.840, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.528, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      
    GroundSound(0.398,"Sound_step")
    GroundSound(0.753,"Sound_step")

STake_Start("HG_BTS_CR_move_004","HG_BTS_CR_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")  

    GroundFx(0.399, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.066, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.399, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.066, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           
    GroundSound(0.013,"Sound_step")
    GroundSound(0.366,"Sound_step")

STake_Start("HG_BTS_CR_move_004_ATT","HG_BTS_CR_move_004_new")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")  

    GroundFx(0.399, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.066, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.399, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.066, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           
    GroundSound(0.013,"Sound_step")
    GroundSound(0.366,"Sound_step")

STake_Start("HG_BTS_CR_move_005","HG_BTS_CR_move_005")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("right_left_move") 

    GroundFx(0.399, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.066, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.399, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.066, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)            
    GroundSound(0.013,"Sound_step")
    GroundSound(0.366,"Sound_step")    
    
STake_Start("HG_BTS_CR_move_006","HG_BTS_CR_move_006")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("right_left_move")      
    
    GroundFx(0.399, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.066, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.399, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.066, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)            
    GroundSound(0.013,"Sound_step")
    GroundSound(0.366,"Sound_step")    

STake_Start("HG_BTS_CR_move_009","HG_BTS_CR_move_009")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")  
    GroundSound(0.220,"Sound_step")
    GroundSound(0.670,"Sound_step") 

STake_Start("HG_BTS_CR_move_010","HG_BTS_CR_move_010")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")  
    GroundSound(0.230,"Sound_step")
    GroundSound(0.630,"Sound_step") 

STake_Start("HG_BTS_CR_PY_move_001","HG_BTS_CR_PY_move_001")
    BlendMode(0)
    BlendTime(0)
    GroundSound(0.230,"Sound_step")
    GroundSound(0.630,"Sound_step") 
    EffectStart(0.0,"MB",4)

STake_Start("HG_BTS_CR_PY_move_004","HG_BTS_CR_PY_move_004")
    BlendMode(0)
    BlendTime(0)
    GroundSound(0.230,"Sound_step")
    GroundSound(0.630,"Sound_step") 
    EffectStart(0.0,"MB",4)

STake_Start("HG_BTS_CR_PY_move_005","HG_BTS_CR_PY_move_005")
    BlendMode(0)
    BlendTime(0)
    --BlendPattern("wait")  
    GroundSound(0.230,"Sound_step")
    GroundSound(0.630,"Sound_step") 
    --EffectStart(0.0, "FOV",0)
    EffectStart(0.0,"MB",4)

STake_Start("HG_BTS_CR_PY_move_006","HG_BTS_CR_PY_move_006")
    BlendMode(0)
    BlendTime(0)
    --BlendPattern("wait")  
    GroundSound(0.230,"Sound_step")
    GroundSound(0.630,"Sound_step") 
    EffectStart(0.0,"MB",4)

STake_Start("HG_BTS_CR_dodge_000","HG_BTS_CR_dodge_000")
    BlendMode(0)
    BlendTime(0.2)
   
    --BlendPattern("wait")     
    Sound(0.017,"N1_Axe_dodge_000")
    GroundSound(0.129,"Sound_step")
    GroundSound(0.649,"Sound_step")

STake_Start("HG_BTS_CR_dodge_001","HG_BTS_CR_dodge_001")
    BlendMode(0)
    BlendTime(0.2)
   
    --BlendPattern("wait")     
    Sound(0.017,"N1_Axe_dodge_000")
    GroundSound(0.532,"Sound_jump")
    
    
    
STake_Start("HG_BTS_CR_dead","HG_BTS_CR_dead")
    BlendMode(0)
    BlendTime(0.2)
   
    --BlendPattern("wait")     
    GroundSound(0.620,"Sound_fall") 
    GroundSound(1.360,"Sound_daodi")
	StopChannel(0,2)

STake_Start("HG_BTS_CR_def_000","HG_BTS_CR_def_000")
    BlendMode(0)
    BlendTime(0.2)
   
    --BlendPattern("wait")     


STake_Start("HG_BTS_CR_hit","HG_BTS_CR_hit")
    BlendMode(0)
    BlendTime(0.2)
   
    BlendPattern("Upper_hurt")
    
    
STake_Start("HG_move_012_BTSCR","HG_move_012_BTSCR")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 


STake_Start("HG_move_013_BTSCR","HG_move_013_BTSCR")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
        
    
    
STake_Start("HG_BTS_CR_attack_000","HG_BTS_CR_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")        
    Priority(0,"5") 
    
    MustPlay(0.00,0.432)  
    Soft(0.432,1.2)     

  	AttackStart()
  	Hurt(0.348,"hurt_11")
  
	  Fx(0,"HG_BTS_CR_attack_000")
    DirFx(0.348,"HG_BTS_CR_hurt_000",25,1)        

    Sound(0.276,"BTS_CR_Attack_002")
    Sound(0.276,"BTS_HG_AttackVOC_1_1")
    HurtSound(0.348,"BTS_CR_Hit_001")
    
STake_Start("HG_BTS_CR_attack_001","HG_BTS_CR_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")        
    Priority(0,"5")     

    MustPlay(0.00,0.405)  
    Soft(0.405,1.266) 

  	AttackStart()
  	Hurt(0.329,"hurt_11")

  	Fx(0,"HG_BTS_CR_attack_001")   
    DirFx(0.329,"HG_BTS_CR_hurt_000",25,1)   

    Sound(0.189,"BTS_CR_Attack_003")
    Sound(0.300,"BTS_HG_AttackVOC_1_1")
    HurtSound(0.329,"BTS_CR_Hit_001")
        
STake_Start("HG_BTS_CR_attack_002","HG_BTS_CR_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")        
    Priority(0,"5")     

    MustPlay(0.00,0.563)  

  	AttackStart()
  	Hurt(0.422,"hurt_11") 

  	Fx(0.1,"HG_BTS_CR_attack_002")   
    DirFx(0.422,"HG_BTS_CR_hurt_001",25,1)            

    Sound(0.189,"BTS_CR_Attack_001")
    Sound(0.400,"BTS_HG_AttackVOC_1_2")
    HurtSound(0.422,"BTS_CR_Hit_002")    


STake_Start("HG_BTS_CR_skill_000_new1","HG_BTS_CR_skill_000_new")  -- תתת����
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5")   
    BlendPattern("no_sync")    
    Hard(0.000,2.3)
    Soft(2.3,2.666) 

    Pause(0,0.1,0,0.05)
    Pause(0.265,0.05,0,0.05) 	
    Pause(0.57,0.05,0,0.05) 
    Pause(0.95,0.05,0,0.05) 
    --Pause(1.433,0.1,0,0.05) 
		
  AttackStart()
    Hurt(0.165,"hurt_36")

    DirFx(0.165,"HG_BTS_CR_hurt_000",30,0.5,0.8,0,-90)
    Critical(0.165,1,"HG_BTS_CR_hurt_000_BT")

 
    HurtSound(0.165,"BTS_CR_Hit_003")


    Fx(0,"HG_BTS_CR_skill_000_new_a")
    Fx(0.25,"HG_BTS_CR_skill_000_new_a")
    Fx(0.6,"HG_BTS_CR_skill_000_new_a")
    Fx(1.33,"HG_BTS_CR_skill_000_new_b","Reference",0.8,1,5,-30,5)
    HitGroundFx(1.33,"SFX_hitground")


    StartJA(2.5,500)
    PrivateJAFx(2.3,"Fx_common_JuseAtt_01")
    PrivateJAFx(2.5,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03") 
   
    CameraShake(1.33,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.05","ShakeAmplitudeX = 0","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 0")

        Sound(0.050,"HG_BTS_CR_skill_000_new_000")
        Sound(0.159,"HG_BTS_CR_skill_000_new_001")
        Sound(0.426,"HG_BTS_CR_skill_000_new_001")
        Sound(0.746,"HG_BTS_CR_skill_000_new_002")
        Sound(1.253,"HG_BTS_CR_skill_000_new_005")
        Sound(1.786,"HG_BTS_CR_skill_000_new_004")
 
STake_Start("HG_BTS_CR_skill_000_new2","")  -- תתת˲��
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5")   
    BlendPattern("no_sync")    

  AttackStart()

  
STake_Start("HG_BTS_CR_skill_002_ground","HG_BTS_CR_skill_002_new")  --��ۼ���3
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"3")   
    BlendPattern("no_sync")    
    FrameTime(2.4,3.133)
    AttackStart()
    Hurt(0.01,"hurt_41")
    HurtSound(0.1,"BTS_CR_Hit_003")



STake_Start("HG_BTS_CR_skill_001","HG_BTS_CR_skill_001")  --��������
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    
	hardtime=1.0
		Hard(0.000,hardtime)
		Soft(hardtime,1.828)	--1.228			
  	AttackStart()
  	

	StartJA(hardtime+0.2,500)
    PrivateJAFx(hardtime,"Fx_common_JuseAtt_01")
    PrivateJAFx(hardtime+0.2,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")
  	 
 Hurt(0.604,"hurt_41")

  	Fx(0.001,"HG_BTS_CR_skill_001_fire")         
    DirFx(0.604,"HG_BTS_CR_skill_001_hit",25,1)   
    CameraShake(0.604,"ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.05","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2")	

    Sound(0.146,"BTS_CR_Skill_001")
    Sound(0.140,"BTS_HG_AttackVOC_3_1")
    GroundSound(0.623,"Sound_fall") 
    HurtSound(0.604,"QZ_PA_Skill_004_hit")        
    
  	HitGroundFx(0.604,"SFX_hitground2")   		 		
    HitGroundFx(0.604,"SFX_hitground")        
    
STake_Start("HG_BTS_CR_skill_002","HG_BTS_CR_skill_002")  --������
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,1.919)
				
  	AttackStart()
  	Hurt(0.399,"hurt_31")
  	Hurt(1.759,"hurt_41")  	

    Charge(0.346,0.133,30,0,0)
    Charge(1.572,0.214,30,0,0)

  	Fx(0.000,"HG_BTS_CR_skill_002_fire")   
  	--Fx(0.000,"HG_BTS_CR_skill_002_fire") 
    DirFx(0.399,"HG_BTS_CR_skill_002_hit1",25,1)               
    DirFx(1.759,"HG_BTS_CR_skill_002_hit2",0,1)                      

    Sound(0.283,"BTS_CR_Skill_002")
    Sound(0.312,"BTS_HG_AttackVOC_2_1")
    Sound(1.205,"BTS_HG_AttackVOC_2_2")
    GroundSound(1.759,"Sound_fall") 
    HurtSound(0.399,"BTS_CR_Skill_002_Hit")
    HurtSound(1.759,"BTS_CR_Hit_003")    
    
  	HitGroundFx(1.759,"SFX_hitground2")   		 		
    HitGroundFx(1.759,"SFX_hitground")           


STake_Start("HG_BTS_CR_skill_003","HG_BTS_CR_skill_003")  --���߶���
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.996)
				
  	AttackStart()

  	Fx(0,"HG_BTS_CR_skill_003_start") 
    
    CameraShake(0.703,"ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.05","ShakeAmplitudeX = 0","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 0")	
    
  	HitGroundFx(0.703,"SFX_hitground2")   		 		
    HitGroundFx(0.703,"SFX_hitground")           
    
    
STake_Start("HG_BTS_CR_skill_004","HG_BTS_CR_skill_004") --��ɳ��Ӱ
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.505)

				
  	AttackStart()
  	Hurt(0.332,"hurt_21")

    Charge(0.295,0.074,0,0,2)
 
  	Fx(0,"HG_BTS_CR_skill_004_fire")  
    DragFx(0.332, 0.5, "HG_BTS_CR_skill_004_tracker", "HG_BTS_CR_skill_004_hit", 0, 0, 0, "T_R", "Spine1")           

    Sound(0.050,"BTS_CR_Skill_004")
    Sound(0.426,"BTS_HG_AttackVOC_1_2")
    HurtSound(0.332,"BTS_CR_Hit_003")    
    
    
        
    
STake_Start("HG_BTS_CR_skill_005","HG_BTS_CR_skill_005")  --��ç����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,1.264)

				
  	AttackStart()
  	Hurt(0.288,"hurt_31")

    Charge(0.112,0.208,40,0,0)
    Charge(0.336,0.768,100,180,4)
 
  	Fx(0,"HG_BTS_CR_skill_005_fire")          
    DirFx(0.288,"HG_BTS_CR_skill_005_hit",25,1) 	

    Sound(0.050,"BTS_CR_Skill_005")
    Sound(0.096,"BTS_HG_AttackVOC_1_2")
    GroundSound(1.088,"Sound_fall") 
    HurtSound(0.288,"BTS_CR_Hit_003")     
    
    

STake_Start("HG_BTS_CR_skill_006_1","HG_BTS_CR_skill_006")  --��ʬ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.533)

				
  	AttackStart()
  	Hurt(0.277,"hurt_21")

  	Fx(0,"HG_BTS_skill_000_fire") 

    Charge(0.202,0.107,0,0,2)
    
    Sound(0.050,"BTS_FAN_skill_008_att02")
    HurtSound(0.277,"QZ_PA_Hit_12")
    
    
STake_Start("HG_BTS_CR_skill_006_2","HG_BTS_CR_skill_006")  --��Ѫ��ʴ
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.533)

				
  	AttackStart()
  	Hurt(0.277,"hurt_21")

  	Fx(0,"HG_BTS_skill_000_fire") 

    Charge(0.202,0.107,0,0,2)    
    
    Sound(0.050,"BTS_FAN_skill_008_att02")
    HurtSound(0.277,"QZ_PA_Hit_12")    

STake_Start("HG_BTS_CR_skill_006_3","HG_BTS_CR_skill_006")  --��������
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.533)

				
  	AttackStart()
  	Hurt(0.277,"hurt_21")

  	Fx(0,"HG_BTS_skill_000_fire") 

    Charge(0.202,0.107,0,0,2)
    
    
    Sound(0.050,"BTS_FAN_skill_008_att02")
    HurtSound(0.277,"QZ_PA_Hit_12")


STake_Start("HG_BTS_CR_skill_007","HG_BTS_CR_skill_007")  --�׼�
    BlendMode(0)
    BlendTime(0.2)

		Hard(0.000,0.671)
 
    Fx(0.1,"HG_BTS_FAN_skill_xianji")

    Sound(0.050,"BTS_FAN_skill_008_att02")

-------------------------------------�������¼���--------------------------------------------------------------------------------

STake_Start("HG_BTS_CR_skill_001_new_start","HG_BTS_CR_skill_001_new")  --����start 11109
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    FrameTime(0,2.5)
    BlendPattern("no_sync")    
    Loop(0.833,2.5)
    Fx(0.1,"Fx_common_JuseAtt_03")  
    Fx(0.01,"HG_BTS_CR_skill_001_hit01")  
     
    JAHitFx(0.01,"Fx_common_JuseAtt_03")
  	 

STake_Start("HG_BTS_CR_skill_001_new_fire1","HG_BTS_CR_skill_001_new")  --����fire�� 11111
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    FrameTime(2.5,4.533)
    BlendPattern("no_sync")    
    Hard(2.5,3.7)
    Soft(3.7,4.533)

    Charge(0.12,0.61,20,15,12)
    Fx(0.01,"HG_BTS_CR_attack_003") 

    Sound(2.646,"HG_BTS_CR_skill_000_new_000")
    Sound(0.140,"BTS_HG_AttackVOC_3_1")
    Sound(4.140,"BTS_CR_attscks_return")
				
    AttackStart()

    Pause(0.26,0.16,0,0.08) 
    Hurt(0.26,"hurt_31")
    HurtSound(0.26,"BTS_CR_HIT_new_e1")
    DirFx(0.26,"HG_BTS_CR_skill_001_hit",25,0.5)

    StartJA(3.9,500)
    PrivateJAFx(3.7,"Fx_common_JuseAtt_01")
    PrivateJAFx(3.9,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")
   

STake_Start("HG_BTS_CR_skill_001_new_fire2","HG_BTS_CR_skill_009")  --����fire�� 11110
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    
    Hard(0,1)
    Soft(1,1.833)

    Charge(0.04,0.76,40,15,12)
    Fx(0.01,"HG_BTS_CR_attack_003") 
    Fx(0.35,"HG_BTS_CR_attack_003") 
    Sound(0.06,"BTS_CR_attscks_new_1_1")
    Sound(0.42,"BTS_CR_attscks_new_3_1")
    Sound(0.140,"BTS_HG_AttackVOC_3_1")
    Sound(1.40,"BTS_CR_attscks_return")
				
    AttackStart()

    Pause(0.23,0.16,0,0.08) 
    Pause(0.74,0.17,0,0.08) 

    Hurt(0.23,"hurt_31")
    Hurt(0.74,"hurt_41")

    HurtSound(0.23,"BTS_CR_HIT_new_e1") 
    HurtSound(0.74,"BTS_CR_HIT_new_e1") 

    DirFx(0.23,"HG_BTS_CR_skill_001_hit",25,1)
    DirFx(0.74,"HG_BTS_CR_skill_001_hit",25,1)

    StartJA(1.2,500)
    PrivateJAFx(1,"Fx_common_JuseAtt_01")
    PrivateJAFx(1.2,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")




STake_Start("HG_BTS_CR_skill_008","HG_BTS_CR_skill_008")  --�ҵس��� 14602
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
	Hard(0,1.3)
	Soft(1.3,2)

    Fx(0.258,"HG_BTS_CR_skill_001_fire")  
    GroundSound(0.882,"Sound_fall") 

    HitGroundFx(0.863,"SFX_hitground2")   		 		
    HitGroundFx(0.863,"SFX_hitground")	
	
    AttackStart()  	
    Hurt(0.863,"hurt_41")
    Pause(0.863,0.17,1,0.05) 
    DirFx(0.863,"HG_BTS_CR_skill_001_hit",25,1)   
    HurtSound(0.863,"BTS_CR_HIT_new_e1")  

    Sound(0.01,"BTS_HG_AttackVOC_1_2")
    Sound(0.04,"BTS_CR_attscks_ready1")
    Sound(0.32,"HG_BTS_CR_skill_000_new_002")
    Sound(0.9,"BTS_HG_AttackVOC_1_2")
    Sound(0.8,"HG_BTS_CR_skill_000_new_005")
    Sound(1.75,"BTS_CR_attscks_return")   
        
     
    CameraShake(0.863,"ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.05","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2")

    StartJA(1.5,500)
    PrivateJAFx(1.3,"Fx_common_JuseAtt_01")
    PrivateJAFx(1.5,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")

STake_Start("HG_BTS_CR_skill_000","HG_BTS_CR_skill_000")  --��������
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
		hardtime=1.25
		Hard(0.000,hardtime)
		Soft(hardtime,hardtime+0.7)--1.332

    --Charge(0.162,0.540,28,0,0)
    Fx(0,"HG_BTS_CR_skill_000_fire")
    Fx(0.7,"Fx_common_skill_ground_stone","Reference",1,1,5,-30,5)
    Fx(0.8,"Fx_common_skill_ground_stone","Reference",1,1,-5,-60,5)
    Fx(0.9,"Fx_common_skill_ground_stone","Reference",1,1,5,-90,5) 
    Fx(1.0,"Fx_common_skill_ground_stone","Reference",1,1,-5,-120,5)

    Sound(0.140,"BTS_HG_AttackVOC_3_1")
    Sound(0.702,"BTS_HG_AttackVOC_3_2")
    Sound(0.4,"BTS_CR_attscks_new_3_1")
    Sound(0.64,"HG_BTS_CR_skill_000_new_005")
    Sound(1.45,"BTS_CR_attscks_return")

    HitGroundFx(0.702,"SFX_hitground2")   		 		
    HitGroundFx(0.702,"SFX_hitground")  
				
    AttackStart()
    Pause(0.76,0.17,1,0.05) 
    Hurt(0.918,"hurt_31")
    DirFx(0.918,"HG_BTS_CR_skill_000_hit",30,1)
    HurtSound(0.918,"BTS_CR_HIT_new_2_1") 
   
    CameraShake(0.76,"ShakeTimes = 0.05","ShakeFrequency = 0.05","ShakeMode = 3","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 2")

    StartJA(hardtime+0.2,500)
    PrivateJAFx(hardtime,"Fx_common_JuseAtt_01")
    PrivateJAFx(hardtime+0.2,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")    


STake_Start("HG_BTS_CR_skill_010","HG_BTS_CR_skill_010")  --�Ṧ��̼� 11140
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    
    Hard(0,1.1)
    Soft(1.1,1.833)

    Charge(0.01,0.733,110,15,12)
    Sound(0.140,"BTS_HG_AttackVOC_3_1")
    Sound(0.183,"BTS_CR_attscks_new_3_1")
    Sound(0.03,"HG_BTS_CR_skill_000_new_001")
    Sound(1.35,"BTS_CR_attscks_return")

    Fx(0.001,"HG_BTS_CR_attack_003") 
				
    AttackStart()

    Pause(0.29,0.16,0,0.08) 
    Hurt(0.29,"hurt_41")
    HurtSound(0.29,"BTS_CR_HIT_new_s1") 
    DirFx(0.29,"HG_BTS_CR_skill_001_hit",25,1)

    StartJA(1.3,500)
    PrivateJAFx(1.1,"Fx_common_JuseAtt_01")
    PrivateJAFx(1.3,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")

STake_Start("HG_BTS_CR_skill_000_non","HG_BTS_CR_skill_000_new")  -- תתת�������ð� 11123
    BlendMode(0)
    BlendTime(0)
    Priority(0,"5")   
    BlendPattern("no_sync")    
    Hard(0.000,2.3)
    Soft(2.3,2.666) 

    Fx(0,"HG_BTS_CR_skill_000_new_a")
    Fx(0.25,"HG_BTS_CR_skill_000_new_a")
    Fx(0.6,"HG_BTS_CR_skill_000_new_a")
    Fx(1.33,"HG_BTS_CR_skill_000_new_b","Reference",0.8,1,5,-30,5)
    HitGroundFx(1.33,"SFX_hitground")

    Sound(0.140,"BTS_HG_AttackVOC_3_1")
    Sound(0.265,"HG_BTS_CR_skill_000_new_001")
    Sound(0.47,"HG_BTS_CR_skill_000_new_002")
    Sound(0.74,"HG_BTS_CR_skill_000_new_004")
    Sound(0.8,"HG_BTS_CR_skill_000_new_001")
    Sound(1.33,"HG_BTS_CR_skill_000_new_005")
    Sound(2.25,"BTS_CR_attscks_return")

		
  AttackStart()
    Pause(0.1,0.05,0,0.05) 
    Pause(0.265,0.05,0,0.05) 	
    Pause(0.57,0.05,0,0.05) 
    Pause(0.95,0.05,0,0.05) 

    Hurt(0.215,"hurt_21")
    Hurt(0.29,"hurt_21")
    Hurt(0.35,"hurt_21")
    Hurt(0.5,"hurt_41")

    DirFx(0.215,"HG_BTS_CR_hurt_000",30,0.5,0.8,0,-90)
    DirFx(0.29,"HG_BTS_CR_hurt_000",30,0.5,0.8,0,-90)
    DirFx(0.35,"HG_BTS_CR_hurt_000",30,0.5,0.8,0,-90)
    DirFx(0.5,"HG_BTS_CR_hurt_000",30,0.5,0.8,0,-90)

    HurtSound(0.215,"BTS_CR_HIT_new_1_1")
    HurtSound(0.29,"BTS_CR_HIT_new_1_1")
    HurtSound(0.35,"BTS_CR_HIT_new_2_1")
    HurtSound(0.5,"BTS_CR_HIT_new_2_1")

    StartJA(2.5,500)
    PrivateJAFx(2.3,"Fx_common_JuseAtt_01")
    PrivateJAFx(2.5,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03") 
   
    CameraShake(1.33,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.05","ShakeAmplitudeX = 0","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 0")
 
STake_Start("HG_BTS_CR_skill_002_new1","HG_BTS_CR_skill_002_new_start")  --��ۼ���1   11125
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    
    Hard(0.000,0.5)
    Soft(0.5,1.63)

    Pause(0,0.1,0,0.05)
    Pause(0.76,0.2,1,0.35) 
		
  AttackStart()
    Hurt(0.33,"hurt_11")
    --DirFx(0.33,"HG_BTS_CR_hurt_000",25,0.5,0.8,90,0)
    HurtSound(0.33,"BTS_CR_HIT_new_2_1")
    Sound(0.04,"BTS_CR_attscks_ready1")
    Sound(0.400,"BTS_HG_AttackVOC_1_2")
    Sound(0.6,"BTS_CR_attscks_new_3_1")
    Sound(0.4,"HG_BTS_CR_skill_000_new_001")

    Fx(0.3,"HG_BTS_CR_attack_005")
    Fx(0.7,"HG_BTS_CR_attack_005")
    HitFx(0.72,0,"HG_BTS_CR_hurt_000_BT","Reference",1,1,0,-30,25,-90,-30)   

    StartJA(0.98,500)
    PrivateJAFx(0.96,"Fx_common_JuseAtt_01")
    PrivateJAFx(0.98,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03") 
   

STake_Start("HG_BTS_CR_skill_002_new2","HG_BTS_CR_skill_002_new")  --��ۼ���2  11126
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    
    FrameTime(0.76,3.133)
    Hard(0.76,2.6)
    Soft(2.6,3.133) 
    
    Hurt(0.01,"hurt_41")		

    Sound(1.84,"BTS_HG_AttackVOC_1_2")
    Sound(2.05,"BTS_CR_attscks_new_1_1")
    Sound(2.1,"HG_BTS_CR_skill_000_new_005")
    Sound(3.1,"BTS_CR_attscks_return")
    HurtSound(0.01,"BTS_CR_HIT_new_2_1")

    Fx(0.76,"HG_BTS_CR_attack_005")
    Fx(1.83,"HG_BTS_CR_skill_002_fire1")
    Fx(2.33,"HG_BTS_CR_skill_002_fire2")

    StartJA(2.8,500)
    PrivateJAFx(2.6,"Fx_common_JuseAtt_01")
    PrivateJAFx(2.8,"Fx_common_JuseAtt_02")
    --JAHitFx(0.01,"Fx_common_JuseAtt_03") 

STake_Start("HG_BTS_CR_attack_003_start","HG_BTS_CR_attack_003")    --�չ�1��start
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")  
    Priority(0,"5")   
    FrameTime(0,0.37)     
    Loop(0.369,0.37)
    Hard(0,0.25) 
    Sound(0.050,"BTS_CR_attscks_ready1")
    Fx(0.1,"HG_BTS_CR_attack_003") 
    JAHitFx(0.01,"Fx_common_JuseAtt_03")

STake_Start("HG_BTS_CR_attack_003_fire","HG_BTS_CR_attack_003")    --�չ�1��fire
    BlendMode(0)
    BlendTime(0.03)
    BlendPattern("Upper_att")  
    FrameTime(0.37,2.166)      
    Priority(0,"5") 
    Hard(0.37,0.9) 
    Soft(0.9,2)

    Charge(0.27,0.19,8,15,12)
    Fx(0.3,"HG_BTS_CR_attack_003")  
    Fx(0.41,"HG_BTS_CR_attack_003")   
    Fx(0.6,"HG_BTS_CR_attack_003")  

    Sound(0.380,"BTS_HG_AttackVOC_1_2")
    Sound(0.500,"BTS_CR_attscks_new_1_1")
    Sound(0.810,"BTS_CR_attscks_return")
    GroundSound(1.451,"Sound_step")
    GroundSound(1.600,"Sound_step")

    AttackStart()
    Pause(0.43,0.1,0,0.02) 
    Hurt(0.47,"hurt_31") 
    DirFx(0.47,"HG_BTS_CR_hurt_000",25,1,0.4,-90,0)
    Critical(0.47,0,"HG_BTS_CR_hurt_000_BT")
    HurtSound(0.470,"BTS_CR_HIT_new_1_1")

    StartJA(1.3,500)
    PrivateJAFx(1.1,"Fx_common_JuseAtt_01")
    PrivateJAFx(1.3,"Fx_common_JuseAtt_02")




STake_Start("HG_BTS_CR_attack_004_start","HG_BTS_CR_attack_004")    --�չ�2��start
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")    
    Priority(0,"5")   
    FrameTime(0,0.3)     
    Loop(0.299,0.3)
    Hard(0,0.25) 
    Sound(0.050,"BTS_CR_attscks_ready1")
    GroundSound(0.150,"Sound_step")
    Fx(0.1,"HG_BTS_CR_attack_003")  
    JAHitFx(0.01,"Fx_common_JuseAtt_03")

STake_Start("HG_BTS_CR_attack_004_fire","HG_BTS_CR_attack_004")    --�չ�2��fire
    BlendMode(0)
    BlendTime(0.03)
    BlendPattern("Upper_att")    
    Priority(0,"5") 
    FrameTime(0.3,2.5) 
    Hard(0.3,0.92) 
    Soft(0.92,2)

    Charge(0.32,0.07,10,15,12)
    Fx(0.3,"HG_BTS_CR_attack_003")  
    Fx(0.41,"HG_BTS_CR_attack_003")   
    Fx(0.6,"HG_BTS_CR_attack_003")  

    Sound(0.350,"BTS_HG_AttackVOC_1_2")
    Sound(0.420,"BTS_CR_attscks_new_1_1")
    Sound(1.170,"BTS_CR_attscks_return")
    GroundSound(1.350,"Sound_step")
    GroundSound(1.700,"Sound_step")

    AttackStart()
    Pause(0.42,0.1,0,0.02) 
    Hurt(0.4,"hurt_31") 
    DirFx(0.4,"HG_BTS_CR_hurt_000",25,1,0.4,90,0)
    Critical(0.4,0,"HG_BTS_CR_hurt_000_BT")
    HurtSound(0.4,"BTS_CR_HIT_new_1_1")

    StartJA(1.3,500)
    PrivateJAFx(1.1,"Fx_common_JuseAtt_01")
    PrivateJAFx(1.3,"Fx_common_JuseAtt_02")


    
STake_Start("HG_BTS_CR_attack_005_start","HG_BTS_CR_attack_005")   --�չ�3��start
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")  
    Priority(0,"5")   
    FrameTime(0,0.42)     
    Loop(0.419,0.42)
    Hard(0,0.25) 

    Sound(0.050,"BTS_CR_attscks_ready1")
    Sound(0.288,"BTS_CR_attscks_new_3_1")
    GroundSound(0.293,"Sound_step")
    GroundSound(0.050,"Sound_jump")

    Fx(0.1,"HG_BTS_CR_attack_003")   
    JAHitFx(0.01,"Fx_common_JuseAtt_03")

STake_Start("HG_BTS_CR_attack_005_fire","HG_BTS_CR_attack_005")   --�չ�3��fire
    BlendMode(0)
    BlendTime(0.03)
    BlendPattern("Upper_att")     
    Priority(0,"5") 
    FrameTime(0.42,1.833)  
    Hard(0.4,1.1) 
    Soft(1.1,1.6)
  
    Charge(0.075,0.385,15,15,12)
    Fx(0.3,"HG_BTS_CR_attack_003")  
    Fx(0.41,"HG_BTS_CR_attack_003")   
    Fx(0.6,"HG_BTS_CR_attack_003")  

    Sound(0.450,"BTS_HG_AttackVOC_1_2")
    Sound(1.2,"BTS_CR_attscks_return")
    Sound(1.2,"BTS_CR_attscks_return")
    GroundSound(1.356,"Sound_step")
    GroundSound(1.666,"Sound_step")

    AttackStart()
    Pause(0.29,0.1,0,0.015) 
    Hurt(0.31,"hurt_31") 
    DirFx(0.31,"HG_BTS_CR_hurt_000",25,1,0.4,-70,-30)
    Critical(0.31,0,"HG_BTS_CR_hurt_000_BT")
    HurtSound(0.31,"BTS_CR_HIT_new_2_1")


    StartJA(1.3,500)
    PrivateJAFx(1.1,"Fx_common_JuseAtt_01")
    PrivateJAFx(1.3,"Fx_common_JuseAtt_02")


STake_Start("HG_BTS_CR_skill_011","HG_BTS_CR_skill_011")   --����������--
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    


	hardtime=1.4

		Hard(0.000,hardtime)
		Soft(hardtime,1.66)
    StartJA(hardtime+0.2,500)
    PrivateJAFx(hardtime,"Fx_common_JuseAtt_01")
    PrivateJAFx(hardtime+0.2,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")
				
  	AttackStart()
  	Hurt(0.71,"hurt_36")
     
 
    Charge(0.71,0.2,30,0,3)  
  	
    CameraShake(0.893,"ShakeTimes = 0.05","ShakeFrequency = 0.05","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	




