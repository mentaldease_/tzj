local modname = ...
lout('start loading '.. modname)

module(modname, package.seeall)

function SetupConfig()
	local configMgr = SD.CLuaConfigManager:Instance();

	SetupTaskDisplayConfig(configMgr);
	SetupMapDisplayConfig(configMgr);
	SetupSceneNavConfig(configMgr);
	SetupTaskNavConfig(configMgr)

end

function SetupFormatTextConfig()
	local mgr = CFormatTextConfigManager:Instance()
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_ItemBonus.layout')
	cfg.CloseButton = 'Activity_ItemBonus/CloseBtn'
	cfg.ContentWindow = 'Activity_ItemBonus/ScriptContent'
	--cfg.TitleImage = 'TestCustom/TitleImage'
	cfg.TitleText = 'Activity_ItemBonus/TitleText'
	cfg.LabelTemplate = 'Activity_ItemBonus/Label'
	cfg.ItemTemplate = 'Activity_ItemBonus/ItemTemplate'
	--cfg.MenuItemTemplate = 'Activity_ItemBonus/ItemTemplate'
	cfg.ButtonTemplate = 'Activity_ItemBonus/Button'
	--cfg.CheckboxTemplate = 'TestCustom/NpcDialog/Frame/ContentContainer/Checkbox'
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_PVP.layout')
	cfg.CloseButton = 'Activity_PVP__auto_closebutton__'
	cfg.ContentWindow = 'Activity_PVP/ScriptContent'
	cfg.TitleImage = 'Activity_PVP/TitleImage'
	cfg.TitleText = 'Activity_PVP__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_PVP/Frame/ContentContainer/Label'
	cfg.ItemTemplate = 'Activity_PVP/Frame/ContentContainer/ItemTemplate'
	cfg.MenuItemTemplate = 'Activity_PVP/Item0'
	cfg.ButtonTemplate = 'Activity_PVP/Frame/ContentContainer/Button'
	cfg.CheckboxTemplate = 'Activity_PVP/Frame/ContentContainer/Checkbox'
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_Boss.layout')
	cfg.CloseButton = 'Activity_Boss__auto_closebutton__'
	cfg.ContentWindow = 'Activity_Boss/ScriptContent'
	cfg.TitleImage = 'Activity_Boss/TitleImage'
	cfg.TitleText = 'Activity_Boss__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_Boss/Frame/ContentContainer/Label'
	cfg.ItemTemplate = 'Activity_Boss/Frame/ContentContainer/ItemTemplate'
	cfg.MenuItemTemplate = 'Activity_Boss/Item0'
	cfg.ButtonTemplate = 'Activity_Boss/Frame/ContentContainer/Button'
	cfg.CheckboxTemplate = 'Activity_Boss/Frame/ContentContainer/Checkbox'
	cfg.ProgressBarTemplate = 'Activity_Boss/Frame/ContentContainer/ProgressBar'
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_Template1.layout')
	cfg.CloseButton = 'Activity_Template1__auto_closebutton__'
	cfg.ContentWindow = 'Activity_Template1/ScriptContent'
	cfg.TitleImage = 'Activity_Template1/TitleImage'
	cfg.TitleText = 'Activity_Template1__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_Template1/Frame/ContentContainer/Label'
	cfg.ItemTemplate = 'Activity_Template1/Frame/ContentContainer/ItemTemplate'
	cfg.MenuItemTemplate = 'Activity_Template1/Item0'
	cfg.ButtonTemplate = 'Activity_Template1/Frame/ContentContainer/Button'
	cfg.CheckboxTemplate = 'Activity_Template1/Frame/ContentContainer/Checkbox'

	local cfg = mgr:CreateWndTemplateCfg('Activity_Bonus1.layout')
	cfg.CloseButton = 'Activity_Bonus1__auto_closebutton__'
	cfg.ContentWindow = 'Activity_Bonus1/ScriptContent'
	cfg.TitleImage = 'Activity_Bonus1/TitleImage'
	cfg.TitleText = 'Activity_Bonus1__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_Bonus1/Frame/ContentContainer/Label'
	cfg.ItemTemplate = 'Activity_Bonus1/Frame/ContentContainer/ItemTemplate'
	cfg.MenuItemTemplate = 'Activity_Bonus1/Item0'
	cfg.ButtonTemplate = 'Activity_Bonus1/Frame/ContentContainer/Button'
	cfg.CheckboxTemplate = 'Activity_Bonus1/Frame/ContentContainer/Checkbox'

	local cfg = mgr:CreateWndTemplateCfg('Activity_Bonus2.layout')
	cfg.CloseButton = 'Activity_Bonus2/CloseBtn'
	cfg.ContentWindow = 'Activity_Bonus2/ScriptContent'
	--cfg.TitleImage = 'Activity_Bonus2/TitleImage'
	cfg.TitleText = 'Activity_Bonus2/TitleText'
	cfg.LabelTemplate = 'Activity_Bonus2/Label'
	cfg.ItemTemplate = 'Activity_Bonus2/ItemTemplate'
	--cfg.MenuItemTemplate = 'Activity_Bonus2/ItemTemplate'
	cfg.ButtonTemplate = 'Activity_Bonus2/Button'
	--cfg.CheckboxTemplate = 'TestCustom/NpcDialog/Frame/ContentContainer/Checkbox'
	cfg.InputTemplate = 'Activity_Bonus2/Editbox'

  local cfg = mgr:CreateWndTemplateCfg('Activity_Tip.layout')
	cfg.CloseButton = 'Activity_Tip/CloseBtn'
	cfg.ContentWindow = 'Activity_Tip/ScriptContent'
	--cfg.TitleImage = 'TestCustom/TitleImage'
	cfg.TitleText = 'Activity_Tip/TitleText'
	cfg.LabelTemplate = 'Activity_Tip/Label'
	cfg.ItemTemplate = 'Activity_Tip/ItemTemplate'
	cfg.MenuItemTemplate = 'Activity_Tip/ItemTemplate'
	cfg.ButtonTemplate = 'Activity_Tip/Button'
	--cfg.CheckboxTemplate = 'TestCustom/NpcDialog/Frame/ContentContainer/Checkbox'
	

	local cfg = mgr:CreateWndTemplateCfg('MessageBox_Activity.layout')
	cfg.CloseButton = 'MessageBox_Activity/CloseBtn'
	cfg.ContentWindow = 'MessageBox_Activity/ScriptContent'
	--cfg.TitleImage = 'MessageBox_Activity/TitleImage'
	cfg.TitleText = 'MessageBox_Activity/TitleText'
	cfg.LabelTemplate = 'MessageBox_Activity/Label'
	cfg.ItemTemplate = 'MessageBox_Activity/ItemTemplate'
	--cfg.MenuItemTemplate = 'MessageBox_Activity/ItemTemplate'
	cfg.ButtonTemplate = 'MessageBox_Activity/Button'
	--cfg.CheckboxTemplate = 'TestCustom/NpcDialog/Frame/ContentContainer/Checkbox'
	cfg.InputTemplate = 'MessageBox_Activity/Editbox'

	local cfg = mgr:CreateWndTemplateCfg('Vip.layout')
	cfg.CloseButton = 'Vip/CloseBtn'
	cfg.ContentWindow = 'Vip/ScriptContent'
	--cfg.TitleImage = 'Vip/TitleImage'
	cfg.TitleText = 'Vip/TitleText'
	cfg.LabelTemplate = 'Vip/Label'
	cfg.ItemTemplate = 'Vip/ItemTemplate'
	--cfg.MenuItemTemplate = 'Vip/ItemTemplate'
	cfg.ButtonTemplate = 'Vip/Button'
	cfg.InputTemplate = 'Vip/Editbox'
	cfg.ProgressBarTemplate = 'Vip/ProgressBar'
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_Hero.layout')
	cfg.ContentWindow = 'Activity_Hero/ScriptContent'
	cfg.CloseButton = 'Activity_Hero__auto_closebutton__'
	cfg.TitleText = 'Activity_Hero__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_Hero/Text'
	cfg.ItemTemplate = 'Activity_Hero/Item'
	cfg.ButtonTemplate = 'Activity_Hero/Button'
	cfg.ProgressBarTemplate = 'Activity_Hero/ProgressBar'

	local cfg = mgr:CreateWndTemplateCfg('Activity_BattleInfo.layout')
	cfg.ContentWindow = 'Activity_BattleInfo/ScriptContent'
	cfg.CloseButton = 'Activity_BattleInfo__auto_closebutton__'
	cfg.TitleText = 'Activity_BattleInfo__auto_titlebar__'
	
	
	local cfg = mgr:CreateWndTemplateCfg('PayReward.layout')
	cfg.CloseButton = 'PayReward__auto_closebutton__'
	cfg.ContentWindow = 'PayReward/ContentContainer/ScriptContent'
	cfg.TitleImage = 'PayReward/TitleImage'
	cfg.TitleText = 'PayReward__auto_titlebar__'
	cfg.LabelTemplate = 'PayReward/ContentContainer/Label'
	cfg.ItemTemplate = 'PayReward/ContentContainer/ItemTemplate'
	cfg.MenuItemTemplate = 'PayReward/ContentContainer/Item0'
	cfg.ButtonTemplate = 'PayReward/ContentContainer/Button'
	cfg.CheckboxTemplate = 'PayReward/ContentContainer/Checkbox'
	
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_Rili.layout')
	cfg.CloseButton = 'Activity_Rili__auto_closebutton__'
	cfg.ContentWindow = 'Activity_Rili/ScriptContent'
	cfg.TitleImage = 'Activity_Rili/TitleImage'
	cfg.TitleText = 'Activity_Rili__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_Rili/Frame/ContentContainer/Label'
	cfg.ItemTemplate = 'Activity_Rili/Frame/ContentContainer/ItemTemplate'
	cfg.MenuItemTemplate = 'Activity_Rili/Item0'
	cfg.ButtonTemplate = 'Activity_Rili/Frame/ContentContainer/Button'
	cfg.CheckboxTemplate = 'Activity_Rili/Frame/ContentContainer/Checkbox'
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_101.layout')
	cfg.CloseButton = 'Activity_101__auto_closebutton__'
	cfg.ContentWindow = 'Activity_101/ScriptContent'
	cfg.TitleImage = 'Activity_101/TitleImage'
	cfg.TitleText = 'Activity_101__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_101/Frame/ContentContainer/Label'
	cfg.ItemTemplate = 'Activity_101/Frame/ContentContainer/ItemTemplate'
	cfg.MenuItemTemplate = 'Activity_101/Item0'
	cfg.ButtonTemplate = 'Activity_101/Frame/ContentContainer/Button'
	cfg.CheckboxTemplate = 'Activity_101/Frame/ContentContainer/Checkbox'

  local cfg = mgr:CreateWndTemplateCfg('Activity_DamageCount.layout')
	cfg.CloseButton = 'Activity_DamageCount/CloseBtn'
	cfg.ContentWindow = 'Activity_DamageCount/ScriptContent'
	--cfg.TitleImage = 'TestCustom/TitleImage'
	cfg.TitleText = 'Activity_DamageCount/TitleText'
	cfg.LabelTemplate = 'Activity_DamageCount/Label'
	cfg.ItemTemplate = 'Activity_DamageCount/ItemTemplate'
	cfg.MenuItemTemplate = 'Activity_DamageCount/ItemTemplate'
	cfg.ButtonTemplate = 'Activity_DamageCount/Button'
	--cfg.CheckboxTemplate = 'TestCustom/NpcDialog/Frame/ContentContainer/Checkbox'
	
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_EmptyTemplate.layout')
	--cfg.CloseButton = 'Activity_EmptyTemplate/CloseBtn'
	cfg.ContentWindow = 'Activity_EmptyTemplate/ScriptContent'
	--cfg.TitleImage = 'Activity_EmptyTemplate/TitleImage'
	cfg.TitleText = 'Activity_EmptyTemplate/TitleText'
	cfg.LabelTemplate = 'Activity_EmptyTemplate/Label'
	cfg.ItemTemplate = 'Activity_EmptyTemplate/ItemTemplate'
	--cfg.MenuItemTemplate = 'Activity_EmptyTemplate/ItemTemplate'
	cfg.ButtonTemplate = 'Activity_EmptyTemplate/Button'
	cfg.CheckboxTemplate = 'Activity_EmptyTemplate/Checkbox'
	cfg.InputTemplate = 'Activity_EmptyTemplate/Editbox'

	local cfg = mgr:CreateWndTemplateCfg('Activity_1111.layout')
	cfg.CloseButton = 'Activity_1111__auto_closebutton__'
	cfg.ContentWindow = 'Activity_1111/ScriptContent'
	cfg.TitleImage = 'Activity_1111/TitleImage'
	cfg.TitleText = 'Activity_1111__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_1111/Frame/ContentContainer/Label'
	cfg.ItemTemplate = 'Activity_1111/Frame/ContentContainer/ItemTemplate'
	cfg.MenuItemTemplate = 'Activity_1111/Item0'
	cfg.ButtonTemplate = 'Activity_1111/Frame/ContentContainer/Button'
	cfg.CheckboxTemplate = 'Activity_1111/Frame/ContentContainer/Checkbox'
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_1212.layout')
	cfg.CloseButton = 'Activity_1212__auto_closebutton__'
	cfg.ContentWindow = 'Activity_1212/ScriptContent'
	cfg.TitleImage = 'Activity_1212/TitleImage'
	cfg.TitleText = 'Activity_1212__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_1212/Frame/ContentContainer/Label'
	cfg.ItemTemplate = 'Activity_1212/Frame/ContentContainer/ItemTemplate'
	cfg.ButtonTemplate = 'Activity_1212/Frame/ContentContainer/Button'
	
	local cfg = mgr:CreateWndTemplateCfg('Activity_Xmas.layout')
	cfg.CloseButton = 'Activity_Xmas__auto_closebutton__'
	cfg.ContentWindow = 'Activity_Xmas/ScriptContent'
	cfg.TitleImage = 'Activity_Xmas/TitleImage'
	cfg.TitleText = 'Activity_Xmas__auto_titlebar__'
	cfg.LabelTemplate = 'Activity_Xmas/Frame/ContentContainer/Label'
	cfg.ItemTemplate = 'Activity_Xmas/Frame/ContentContainer/ItemTemplate'
	cfg.ButtonTemplate = 'Activity_Xmas/Frame/ContentContainer/Button'
end

function SetupTaskDisplayConfig(configMgr)
	local displayCfg = configMgr:GetTaskDisplayCfg();
	
	local fxCfg = displayCfg.trunkFxCfg;
	fxCfg.normalAcceptFx = "fx_common_mission_yellow_tanhao.fx";
	fxCfg.easyAcceptFx = "fx_common_mission_yellow_tanhao.fx";
	fxCfg.hardAcceptFx = "fx_common_mission_yellow_tanhao.fx";
	fxCfg.doingFx = "fx_common_mission_gray_wenhao.fx";
	fxCfg.finishFx = "fx_common_mission_yellow_wenhao.fx";
	
	local fxCfg = displayCfg.branchFxCfg;
	fxCfg.normalAcceptFx = "fx_common_mission_green_tanhao.fx";
	fxCfg.easyAcceptFx = "fx_common_mission_green_tanhao.fx";
	fxCfg.hardAcceptFx = "fx_common_mission_green_tanhao.fx";
	fxCfg.doingFx = "fx_common_mission_gray_wenhao.fx";
	fxCfg.finishFx = "fx_common_mission_green_wenhao.fx";

	local fxCfg = displayCfg.pvpFxCfg;
	fxCfg.normalAcceptFx = 'fx_common_mission_purple_tanhao.fx';
	fxCfg.easyAcceptFx = 'fx_common_mission_purple_tanhao.fx';
	fxCfg.hardAcceptFx = 'fx_common_mission_purple_tanhao.fx';
	fxCfg.doingFx = 'fx_common_mission_gray_wenhao.fx';
	fxCfg.finishFx = 'fx_common_mission_purple_wenhao.fx';
	
	local fxCfg = displayCfg.dailyFxCfg;
	fxCfg.normalAcceptFx = "fx_common_mission_blue_tanhao.fx";
	fxCfg.easyAcceptFx = "fx_common_mission_blue_tanhao.fx";
	fxCfg.hardAcceptFx = "fx_common_mission_blue_tanhao.fx";
	fxCfg.doingFx = "fx_common_mission_gray_wenhao.fx";
	fxCfg.finishFx = "fx_common_mission_blue_wenhao.fx";
	
	-- Important task cfg
	local fxCfg = displayCfg.trunkImpFxCfg;
	fxCfg.normalAcceptFx = "fx_common_mission_yellow_tanhao_guang.fx";
	fxCfg.easyAcceptFx = "fx_common_mission_yellow_tanhao_guang.fx";
	fxCfg.hardAcceptFx = "fx_common_mission_yellow_tanhao_guang.fx";
	fxCfg.doingFx = "fx_common_mission_gray_wenhao_guang.fx";
	fxCfg.finishFx = "fx_common_mission_yellow_wenhao_guang.fx";
	
	local fxCfg = displayCfg.branchImpFxCfg;
	fxCfg.normalAcceptFx = "fx_common_mission_green_tanhao_guang.fx";
	fxCfg.easyAcceptFx = "fx_common_mission_green_tanhao_guang.fx";
	fxCfg.hardAcceptFx = "fx_common_mission_green_tanhao_guang.fx";
	fxCfg.doingFx = "fx_common_mission_gray_wenhao_guang.fx";
	fxCfg.finishFx = "fx_common_mission_green_wenhao_guang.fx";

	local fxCfg = displayCfg.pvpImpFxCfg;
	fxCfg.normalAcceptFx = 'fx_common_mission_purple_tanhao_guang.fx';
	fxCfg.easyAcceptFx = 'fx_common_mission_purple_tanhao_guang.fx';
	fxCfg.hardAcceptFx = 'fx_common_mission_purple_tanhao_guang.fx';
	fxCfg.doingFx = 'fx_common_mission_gray_wenhao_guang.fx';
	fxCfg.finishFx = 'fx_common_mission_purple_wenhao_guang.fx';
	
	local fxCfg = displayCfg.dailyImpFxCfg;
	fxCfg.normalAcceptFx = "fx_common_mission_blue_tanhao_guang.fx";
	fxCfg.easyAcceptFx = "fx_common_mission_blue_tanhao_guang.fx";
	fxCfg.hardAcceptFx = "fx_common_mission_blue_tanhao_guang.fx";
	fxCfg.doingFx = "fx_common_mission_gray_wenhao_guang.fx";
	fxCfg.finishFx = "fx_common_mission_blue_wenhao_guang.fx";
	
	local imgCfg = displayCfg.trunkImgCfg;
	imgCfg.acceptImg = "set:Total_MapIcon image:Quest_Accept";
	imgCfg.doingImg = "set:Total_MapIcon image:Quest_Submit_Fail";
	imgCfg.finishImg = "set:Total_MapIcon image:Quest_Submit";
	
	local imgCfg = displayCfg.branchImgCfg;
	imgCfg.acceptImg = "set:Total_MapIcon image:Quest_Accept_fenzhi";
	imgCfg.doingImg = "set:Total_MapIcon image:Quest_Submit_Fail";
	imgCfg.finishImg = "set:Total_MapIcon image:Quest_Submit_fenzhi";
	
	local imgCfg = displayCfg.dailyImgCfg;
	imgCfg.acceptImg = "set:Total_MapIcon image:Quest_Accept_Repeat";
	imgCfg.doingImg = "set:Total_MapIcon image:Quest_Submit_Fail";
	imgCfg.finishImg = "set:Total_MapIcon image:Quest_Submit_Repeat";
	
	local imgCfg = displayCfg.pvpImgCfg;
	imgCfg.acceptImg = "set:Total_MapIcon image:Quest_Accept_PVP";
	imgCfg.doingImg = "set:Total_MapIcon image:Quest_Submit_Fail";
	imgCfg.finishImg = "set:Total_MapIcon image:Quest_Submit_PVP";

end

function SetupMapDisplayConfig(configMgr)
	local displayCfg = configMgr:GetMapDisplayCfg();
	displayCfg.clientIconFlashFx = 'Fx_UI_Maptarget_X.fx';
	displayCfg.clientIconFlashTime = 3.9;
	displayCfg.scriptIconFlashFx = 'Fx_UI_Maptarget_Blue.fx';
	displayCfg.scriptIconFlashTime = 1.9;
	displayCfg.iconCollectDistance = 10;


end

function SetupSceneNavConfig(configMgr)
	local sceneNavCfg = configMgr:GetSceneNavCfg()
	sceneNavCfg.dashboardRadius = 20.0;
	sceneNavCfg.dashboardHeight = 10.0;
	sceneNavCfg.dashboardMesh   = '\\NavigateMesh\\quan.mesh'

end

function SetupTaskNavConfig(configMgr)
	local taskNavCfg = configMgr:GetTaskNavCfg()
	taskNavCfg.trunkIndicatorMesh = "\\NavigateMesh\\JianTou01.mesh"
	taskNavCfg.branchIndicatorMesh = "\\NavigateMesh\\JianTou03.mesh"
	taskNavCfg.pvpIndicatorMesh = "\\NavigateMesh\\JianTou04.mesh"
	taskNavCfg.dailyIndicatorMesh = "\\NavigateMesh\\JianTou02.mesh"

end

-------------------------------------------------------------------------------
-- init & fini callback
-------------------------------------------------------------------------------
--function Load()
--    lout(modname..'.Load()')
--end
--
--function Unload()
--    lout(modname..'.Unload()')
--end

-------------------------------------------------------------------------------
-- entry point
-------------------------------------------------------------------------------
lout('end loading '.. modname)

