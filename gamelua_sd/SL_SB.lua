---------------------------------- Male------------------------------------------ 
   
    

STake_Start("HG_SL_SB_wait_000","HG_SL_SB_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait")     
    
STake_Start("HG_SL_SB_wait_000_ui","HG_SL_SB_wait_000")
    BlendMode(0)
    BlendTime(0)
    Loop()


STake_Start("HG_SL_SB_link_000","HG_SL_SB_link_000")
    BlendMode(0)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.051, "SL_SB_2")

     Sound(0.100,"SL_link_001")

STake_Start("HG_SL_SB_link_001","HG_SL_SB_link_001")
    BlendMode(0)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.596, "SL_SB_1")
    
     Sound(0.426,"SL_link_002")    
    
STake_Start("HG_SL_SB_move_001","HG_SL_SB_move_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")         


    --GroundFx(0.840, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.528, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.840, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.528, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)  

    GroundSound(0.381,"Sound_step")   
    GroundSound(0.753,"Sound_step")  


STake_Start("HG_SL_SB_move_003","HG_SL_SB_move_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")      


    --GroundFx(0.437, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.186, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.450, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.831, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           

    GroundSound(0.000,"Sound_step")   
    GroundSound(0.389,"Sound_step")


STake_Start("HG_SL_SB_move_004","HG_SL_SB_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")      


    GroundFx(0.437, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.186, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.437, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.186, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           

    GroundSound(0.159,"Sound_step")   
    GroundSound(0.410,"Sound_step")

STake_Start("HG_SL_SB_move_005","HG_SL_SB_move_005")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")  

    GroundFx(0.437, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.186, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.437, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.186, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           

    GroundSound(0.159,"Sound_step")   
    GroundSound(0.410,"Sound_step")

STake_Start("HG_SL_SB_move_006","HG_SL_SB_move_006")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait") 

    GroundFx(0.437, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.186, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.437, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.186, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           

    GroundSound(0.159,"Sound_step")   
    GroundSound(0.410,"Sound_step")    

STake_Start("HG_SL_SB_move_009","HG_SL_SB_move_009")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait") 
    GroundSound(0.520,"Sound_step")   
    GroundSound(0.830,"Sound_step")      
    
STake_Start("HG_SL_SB_move_010","HG_SL_SB_move_010")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    --BlendPattern("wait")            
    GroundSound(0.470,"Sound_step")   
    GroundSound(0.870,"Sound_step")    
    
STake_Start("HG_SL_SB_dead","HG_SL_SB_dead")
    BlendMode(0)
    BlendTime(0.2)
   
    --BlendPattern("wait")     
    StopChannel(0,2)
    GroundSound(0.860,"Sound_fall")
    GroundSound(1.320,"Sound_daodi")
    Sound(0.080,"SL_DieVOC_000")  

STake_Start("HG_SL_SB_def_000","HG_SL_SB_def_000")
    BlendMode(0)
    BlendTime(0.01)
   
    --BlendPattern("wait")     

     Sound(0.073,"YP_def_000")  

STake_Start("HG_SL_SB_dodge_000","HG_SL_SB_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
   
    --BlendPattern("wait")   

     Sound(0.014,"N1_Axe_dodge_000")
     GroundSound(0.058,"Sound_jump")
     GroundSound(0.564,"Sound_step")

STake_Start("HG_SL_SB_hit","HG_SL_SB_hit")
    BlendMode(0)
    BlendTime(0.2)
   
    BlendPattern("Upper_hurt")

     Sound(0.010,"SL_HurtVOC_000")
    
STake_Start("HG_SL_SB_attack_000","HG_SL_SB_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")        
    Priority(0,"5") 
    
    MustPlay(0.00,0.333)  
    Soft(0.333,0.9)     

  	AttackStart()
  	Hurt(0.279,"hurt_11")
  
	  --Fx(0,"HG_SL_SB_attack_trail")
          Fx(0,"HG_SL_SB_attack_000_fire")
     
     DirFx(0.279,"HG_SL_SB_attack_hit",25,1,1,90,0)

     Sound(0.198,"SL_HG_AttackVOC_001")
     Sound(0.050,"HG_SL_SB_attack_000")
     HurtSound(0.279,"HG_Sl_SB_Hit_000")
    
STake_Start("HG_SL_SB_attack_001","HG_SL_SB_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")        
    Priority(0,"5")     

    MustPlay(0.00,0.384)  
    Soft(0.384,1.166) 

  	AttackStart()
  	Hurt(0.244,"hurt_11")

  	--Fx(0,"HG_SL_SB_attack_trail") 
        Fx(0,"HG_SL_SB_attack_000_fire")  
    
    
    DirFx(0.244,"HG_SL_SB_attack_hit",25,1,1,-90,0)

     Sound(0.198,"SL_HG_AttackVOC_001")
     Sound(0.050,"HG_SL_SB_attack_001")
     HurtSound(0.244,"HG_Sl_SB_Hit_000")
        
STake_Start("HG_SL_SB_attack_002","HG_SL_SB_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")        
    Priority(0,"5")     

    MustPlay(0.00,0.340)  

  	AttackStart()
  	Hurt(0.278,"hurt_11") 

  	--Fx(0,"HG_SL_SB_attack_trail")
        Fx(0,"HG_SL_SB_attack_000_fire")   
    
    DirFx(0.278,"HG_SL_SB_attack_hit",25,1,1.2,90,0)

     Sound(0.175,"SL_HG_AttackVOC_001")
     Sound(0.050,"HG_SL_SB_attack_002")
     HurtSound(0.278,"SL_Big_blade_Hit_002_2")

STake_Start("HG_SL_SB_skill_000","HG_SL_SB_skill_000") --无量
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.491)
		--Soft(0.576,1.6)
				
  	--AttackStart()
  	--Hurt(0.641,"hurt_11")

    Fx(0.00,"HG_SL_SB_skill_000")         

     Sound(0.080,"HG_SL_SB_Skill_000")

STake_Start("HG_SL_SB_skill_001","HG_SL_SB_skill_001")  --有量
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.470)
		--Soft(0.624,1.6)
				
  	--AttackStart()
  	--Hurt(0.641,"hurt_11")

    Fx(0.00,"HG_SL_SB_skill_001")         

     Sound(0.080,"HG_SL_SB_Skill_001")

STake_Start("HG_SL_SB_skill_002","HG_SL_SB_skill_002")  --绝量
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.470)
		--Soft(0.630,1.466)
				
  	--AttackStart()
  	--Hurt(0.641,"hurt_11")

    Fx(0.001,"HG_SL_SB_skill_002")         
    
     Sound(0.080,"HG_SL_SB_Skill_002")

STake_Start("HG_SL_SB_skill_003","HG_SL_SB_skill_003")  --退失悲心
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.567)
		--Soft(0.639,1.333)
				
  	AttackStart()
  	Hurt(0.369,"hurt_31")

    Charge(0.209,0.185,28,0,0)
 
    Fx(0,"HG_SL_SB_skill_003_fire") 
    DirFx(0.369,"HG_SL_SB_skill_003_hit",20,-3,1,0,-90) 

    --CameraShake(0.42,"ShakeTimes = 0.1","ShakeFrequency = 0.05","ShakeMode = 3","ShakeAmplitudeX = 3","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 3")           

     Sound(0.020,"HG_SL_SB_Skill_003")
     Sound(0.258,"SL_HG_AttackVOC_004")
     HurtSound(0.369,"BTS_FAN_Hit_003")

STake_Start("HG_SL_SB_skill_004","HG_SL_SB_skill_004")  --悲天悯人
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.792)
		--Soft(0.699,1.166)
				
  	AttackStart()
  	Hurt(0.519,"hurt_21")
  	Hurt(0.560,"")
  	Hurt(0.628,"")
  	Hurt(0.655,"")  	
  	

    Charge(0.000,0.319,28,0,0)
 
    Fx(0,"HG_SL_SB_skill_004_fire")
    Fx(0,"HG_SL_SB_skill_trail")  
    DirFx(0.541,"HG_SL_SB_skill_004_hit",20,0) 

    --CameraShake(0.244,"ShakeTimes = 0.1","ShakeFrequency = 0.05","ShakeMode = 3","ShakeAmplitudeX = 3","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 3")  

     Sound(0.001,"HG_SL_SB_Skill_004")
     Sound(0.001,"SL_HG_AttackVOC_005")
     Sound(0.505,"SL_HG_AttackVOC_002")
     HurtSound(0.519,"Big_blade_Hit")
     HurtSound(0.560,"Big_blade_Hit_002_1")
     HurtSound(0.628,"Big_blade_Hit_002_1")
     HurtSound(0.655,"BTS_FAN_Hit_003")

STake_Start("HG_SL_SB_skill_005","HG_SL_SB_skill_005")  --江河入海
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.665)
		--Soft(0.705,1.533)
				
  	AttackStart()
  	Hurt(0.517,"hurt_31")

    Charge(0.110,0.420,28,0,0)
 
     Fx(0,"HG_SL_SB_skill_005_fire")
     Fx(0.488,"HG_SL_SB_skill_trail")  
     --DirFx(0.517,"HG_SL_SB_skill_005_hit",20,0.5) 
     DirFx(0.517,"HG_SL_SB_skill_hit",20,0,0.6,90,0)
     

     --CameraShake(0.490,"ShakeTimes = 0.1","ShakeFrequency = 0.05","ShakeMode = 3","ShakeAmplitudeX = 3","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 3")  

     Sound(0.001,"HG_SL_SB_Skill_005")
     Sound(0.001,"SL_HG_AttackVOC_004")
     GroundSound(0.110,"Sound_jump")  
     GroundSound(0.505,"Sound_fall")
     HurtSound(0.517,"BTS_FAN_Hit_003")

STake_Start("HG_SL_SB_skill_006","HG_SL_SB_skill_006")  --手握苍生
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,1.003)
		--Soft(0.806,1.466)
				
  	AttackStart()
  	Hurt(0.47,"hurt_21")

    Charge(0.256,0.152,28,0,0)

 
      Fx(0.1,"HG_SL_SB_skill_006_fire") 
      Fx(0,"HG_SL_SB_skill_trail") 
      DirFx(0.47,"HG_SL_SB_skill_hit",20,1,1,-140,0)  

      Sound(0.001,"N1_Axe_dodge_000")
      Sound(0.329,"HG_SL_SB_Skill_006")
      Sound(0.386,"SL_HG_AttackVOC_005")
      GroundSound(0.616,"Sound_jump")  
      GroundSound(0.960,"Sound_fall")
      HurtSound(0.429,"BTS_FAN_Hit_003")

STake_Start("HG_SL_SB_skill_007","HG_SL_SB_skill_007")  --唯有我佛
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,1.213)
		--Soft(0.702,1.033)
				
  	AttackStart()
  	Hurt(0.979,"hurt_31")

    Charge(0.186,0.583,28,0,0)

 
  	Fx(0,"HG_SL_SB_skill_007_fire") 
        Fx(0,"HG_SL_SB_skill_trail") 
    DirFx(0.979,"HG_SL_SB_skill_004_hit",25,1,1,0,90) 
    --AttackHurtFx(1.1,"FX_buff_firehurt","",1.5) 
    --AttackHurtFx(2.1,"FX_buff_firehurt","",1.5) 

      Sound(0.001,"HG_SL_SB_Skill_007")
      Sound(0.001,"SL_HG_AttackVOC_004")
      Sound(0.816,"SL_HG_AttackVOC_003")
      GroundSound(0.139,"Sound_jump")  
      GroundSound(1.003,"Sound_fall")
      HurtSound(0.979,"Big_blade_Hit_002_2")

STake_Start("HG_SL_SB_skill_008","HG_SL_SB_skill_008")   --唯有修罗
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,1.147)
		--Soft(0.702,1.666)
				
  	AttackStart()
  	Hurt(0.579,"hurt_21")
  	
    Charge(0.502,0.116,28,0,0)  	
 
     
     Fx(0.000,"HG_SL_SB_skill_008_fire") 
     Fx(0.4,"HG_SL_SB_skill_trail") 
    
    DirFx(0.502,"HG_SL_SB_skill_008_hit",25,1) 
    DirFx(0.502,"HG_SL_SB_skill_hit",30,1,0.6,90,0)

      Sound(0.001,"HG_SL_SB_Skill_008")
      Sound(0.001,"SL_HG_AttackVOC_001")
      Sound(0.563,"SL_HG_AttackVOC_002")
      HurtSound(0.579,"Big_blade_Hit_002_2")

STake_Start("HG_SL_SB_skill_009","HG_SL_SB_skill_009")  --燃木飞舞
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,1.126)
		--Soft(0.702,1.666)
				
  	AttackStart()
  	Hurt(0.265,"hurt_11")
  	Hurt(0.569,"hurt_11")
  	Hurt(0.873,"hurt_11")
 
     Fx(0,"HG_SL_SB_skill_009")
     Fx(0,"HG_SL_SB_skill_trail") 
    
    DirFx(0.3,"HG_SL_SB_skill_004_hit",25,1) 
    AttackHurtFx(0.6,"FX_buff_firehurt","",1.5) 
    AttackHurtFx(0.9,"FX_buff_firehurt","",1.5) 

      Sound(0.001,"HG_SL_SB_Skill_009")
      Sound(0.113,"SL_HG_AttackVOC_005")
      HurtSound(0.265,"BTS_FAN_Hit_003")
      HurtSound(0.569,"BTS_FAN_Hit_003")
      HurtSound(0.873,"BTS_FAN_Hit_003")

STake_Start("HG_SL_SB_skill_011_loop","HG_SL_SB_skill_011_loop")  --风起云涌
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    Priority(0,"5")   
    BlendPattern("no_sync")    

		--Hard(0.000,1.666)
		--Soft(0.702,1.666)
				
  	--AttackStart()
  	--Hurt(0.349,"hurt_11")
 
  	Fx(0,"HG_SL_SB_skill_011_loop") 

      Sound(0.001,"HG_SL_SB_Skill_011_loop")

STake_Start("HG_SL_SB_skill_011_fire","HG_SL_SB_skill_011_fire")  --风起云涌
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.406)
		--Soft(0.702,2.233)
				
  	AttackStart()
  	Hurt(0.147,"hurt_32")
     
     Fx(0.00,"HG_SL_SB_skill_011_fire") 
     Fx(0,"HG_SL_SB_skill_trail") 
     DirFx(0.054,"HG_SL_SB_skill_hit",30,0.5,0.6,-90,0) 

      Sound(0.001,"HG_SL_SB_Skill_011")
      Sound(0.036,"SL_HG_AttackVOC_004")
      HurtSound(0.147,"BTS_FAN_Hit_003")

STake_Start("HG_SL_SB_skill_012","HG_SL_SB_skill_012")   --禽龙功
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,1.200)
		--Soft(0.341,1.266)
				
  	AttackStart()
  	Hurt(0.290,"hurt_21")  	
  	Hurt(0.893,"hurt_32")

     Fx(0.0,"HG_SL_SB_skill_012_fire_1") 
     Fx(0.9,"HG_SL_SB_skill_012_fire_2")
     
     LinkFx(0,"HG_SL_SB_skill_012_fire_chain",0.5,"T_L","Spine1")
     DirFx(0.01,"HG_SL_SB_skill_012_hit01",25,1) 
     DirFx(0.893,"HG_SL_SB_skill_012_hit",25,1) 
 
    Charge(0.290,0.178,7,0,3)  
  	
    CameraShake(0.893,"ShakeTimes = 0.05","ShakeFrequency = 0.05","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	

     Sound(0.001,"HG_SL_SB_Skill_012_1")
     Sound(0.290,"HG_SL_SB_Skill_012_2")
     Sound(0.803,"HG_SL_SB_Skill_012_3")
     Sound(0.022,"SL_HG_AttackVOC_001")
     Sound(0.600,"SL_HG_AttackVOC_004")
     HurtSound(0.290,"HG_SL_SB_Skill_012_hit1")
     HurtSound(0.893,"HG_SL_SB_Skill_012_hit2")

STake_Start("HG_SL_SB_skill_013","HG_SL_SB_skill_013")  --夺器鸣天
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.510)
		--Soft(0.519,1.333)
				
  	AttackStart()
  	Hurt(0.435,"hurt_21")
  	
    Charge(0.255,0.210,28,0,0)    	
 
    Fx(0,"HG_SL_SB_skill_013_fire") 
    Fx(0,"HG_SL_SB_skill_trail") 
    DirFx(0.435,"HG_SL_SB_skill_013_hit",25,1) 
    
     Sound(0.001,"HG_SL_SB_Skill_013")
     Sound(0.001,"SL_HG_AttackVOC_001")
     Sound(0.435,"SL_HG_AttackVOC_002")
     HurtSound(0.435,"HG_SL_SB_Skill_013_hit")

STake_Start("HG_SL_SB_skill_014","HG_SL_SB_skill_014")  --普渡众生
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,1.026)
		--Soft(0.29,1)
				
  	--AttackStart()
  	--Hurt(1.250,"hurt_11")
 
    Fx(0,"HG_SL_SB_skill_014") 
    --DirFx(1.250,"HG_SL_ST_skill_006_hit",25,1)

     Sound(0.001,"HG_SL_SB_Skill_014")
     Sound(0.270,"SL_HG_AttackVOC_001")

STake_Start("HG_SL_SB_skill_015","HG_SL_SB_skill_015")   --罗汉金身
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.669)

    Fx(0,"HG_SL_SB_shibatongren_fire")
			
    CameraShake(0.669,"ShakeTimes = 0.05","ShkaeSpeedFadeOut = 0.05","ShakeSpeed = 900","ShakeMode = 3","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 3")	  

    Sound(0.050,"HG_SL_SB_Skill_015")
    Sound(0.587,"SL_BianShenVOC_001")
    
    
    STake_Start("HG_SL_SB_skill_016","HG_SL_SB_skill_016")   --威震雷霆
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.86)
		
		AttackStart()
  	Hurt(0.67,"hurt_11")
  	
  	Charge(0.58,0.1,28,0,0)

    Fx(0,"HG_SL_SB_skill_016_fire")
    DirFx(0.67,"HG_SL_SB_skill_003_hit",25,1)
			
    
    Sound(0.000,"")
    HurtSound(0.67,"")
    
    
    STake_Start("HG_SL_SB_skill_017","HG_SL_SB_skill_017+")   --狂风怒啸
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		Hard(0.000,0.79)
		

    Fx(0.3,"HG_SL_SB_skill_017_fire")
    --Fx(1,"HG_SL_SB_skill_017_wind","Reference")
    --DirFx(0.67,"HG_SL_SB_skill_003_hit",25,1)
    	
    
    Sound(0.000,"")
    --HurtSound(0.67,"")
    
    STake_Start("HG_SL_SB_skill_018","HG_SL_SB_skill_018_new")   --大开杀戒
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    FrameTime(0,4.38)

		
		AttackStart()
  	Hurt(0.5,"hurt_11")
  	

    Fx(0.5,"HG_SL_SB_skill_018_fire_a")
    Fx(1.5,"HG_SL_SB_skill_018_fire_a")
    Fx(2.5,"HG_SL_SB_skill_018_fire_a")
    Fx(3.5,"HG_SL_SB_skill_018_fire_a")
    Fx(4.0,"HG_SL_SB_skill_018_fire_c")
    DirFx(0.5,"HG_SL_SB_skill_018_hit",25,1)
			
    Sound(0.000,"")
    HurtSound(0.00,"")
    
    
    STake_Start("HG_SL_SB_skill_018_end","HG_SL_SB_skill_018_new") --大开杀戒收招
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    FrameTime(4.38,5.4)
    Hard(4.38,4.96)
    

    
    STake_Start("HG_SL_SB_ylwl","HG_SL_SB_wait_000") --有量无量
    BlendMode(0)
    BlendTime(0.2)
    
    Priority(0,"6") 
      
STake_Start("HG_SL_SB_skill_012_hit","HG_SL_SB_skill_012")   --白驼禽龙功--test1---抓到人后
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

		--Hard(0.000,1.200)
		--Soft(0.341,1.266)
	hardtime=1.4
		Hard(0.000,hardtime)
		Soft(hardtime,2)
    StartJA(hardtime+0.2,500)
    PrivateJAFx(hardtime,"Fx_common_JuseAtt_01")
    PrivateJAFx(hardtime+0.2,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")
				
  	AttackStart()
  	Hurt(0.290,"hurt_21")  	
  	Hurt(0.893,"hurt_36")

     LinkFx(0.01,"HG_SL_SB_skill_012_fire_chain",0.5,"T_L","Spine1")
     Fx(0.01,"HG_SL_SB_skill_012_fire_1") 
     Fx(0.9,"HG_SL_SB_skill_012_fire_2")
     

     DirFx(0.01,"HG_SL_SB_skill_012_hit01",25,1) 
     DirFx(0.893,"HG_SL_SB_skill_012_hit",25,1,0.3) 
 
    Charge(0.290,0.178,30,0,3)  
  	
    CameraShake(0.893,"ShakeTimes = 0.05","ShakeFrequency = 0.05","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	

     Sound(0.001,"HG_SL_SB_Skill_012_1")
     Sound(0.290,"HG_SL_SB_Skill_012_2")
     Sound(0.803,"HG_SL_SB_Skill_012_3")
     Sound(0.022,"SL_HG_AttackVOC_001")
     Sound(0.600,"SL_HG_AttackVOC_004")
     HurtSound(0.290,"HG_SL_SB_Skill_012_hit1")
     HurtSound(0.893,"HG_SL_SB_Skill_012_hit2")

STake_Start("HG_SL_SB_skill_012_not_hit","HG_SL_SB_skill_012")   --白驼禽龙功--test2---没抓到人
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")  
    FrameTime(0,0.4) 
    BlendPattern("no_sync")    

		--Hard(0.000,1.200)
		--Soft(0.341,1.266)
	hardtime=0.441
		Hard(0.000,hardtime)
		Soft(hardtime,1.2)
    StartJA(hardtime+0.2,500)
    PrivateJAFx(hardtime,"Fx_common_JuseAtt_01")
    PrivateJAFx(hardtime+0.2,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")
				


     --Fx(0.0,"HG_SL_SB_skill_012_fire_1") 



     Sound(0.001,"HG_SL_SB_Skill_012_1")
     Sound(0.022,"SL_HG_AttackVOC_001")

                     