
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local openEvolutionUI = indicatorFlow.createIndicatorSpec()
	openEvolutionUI.frameName     = 'openEvolutionUI'
	openEvolutionUI.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openEvolutionUI.frameText     = '打开界面'
	openEvolutionUI.attachFunc    = self.OnEvolutionUIAttached
	openEvolutionUI.attachFuncParam = self
	openEvolutionUI.triggerFunc   = nil
	openEvolutionUI.triggerFuncParam = nil
	openEvolutionUI.triggerWin    = 'Craft_Evolution/Confirm'
	openEvolutionUI.attachWin     = 'Craft_Evolution/Confirm'
	openEvolutionUI.attachWinRoot = 'Craft_Evolution'
	openEvolutionUI.priority      = 0
	
	local placeEquipItem = indicatorFlow.createIndicatorSpec()
	placeEquipItem.frameName     = 'placeEquipItem'
	placeEquipItem.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeEquipItem.frameText     = '点击放置需要进化的装备'
	placeEquipItem.attachFunc    = nil
	placeEquipItem.attachFuncParam = nil
	placeEquipItem.triggerFunc   = nil
	placeEquipItem.triggerFuncParam = nil
	placeEquipItem.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeEquipItem.attachItemID  = nil
	placeEquipItem.dependWin     = 'Craft_Evolution'
	placeEquipItem.priority      = 1
	placeEquipItem.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Evolution', false)
	placeEquipItem.fallbackFuncParam = self
	placeEquipItem.fallbackPri   = 0
	
	local placeMaterialItem = indicatorFlow.createIndicatorSpec()
	placeMaterialItem.frameName     = 'placeMaterialItem'
	placeMaterialItem.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeMaterialItem.frameText     = '点击放置进化的辅助材料'
	placeMaterialItem.attachFunc    = nil
	placeMaterialItem.attachFuncParam = nil
	placeMaterialItem.triggerFunc   = nil
	placeMaterialItem.triggerFuncParam = nil
	placeMaterialItem.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeMaterialItem.attachItemID  = nil
	placeMaterialItem.priority      = 2
	placeMaterialItem.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Evolution', false)
	placeMaterialItem.fallbackFuncParam = self
	placeMaterialItem.fallbackPri   = 0
		
	local confirmEvolution = indicatorFlow.createIndicatorSpec()
	confirmEvolution.frameName     = 'confirmEvolution'
	confirmEvolution.frameType     = indicatorFlow.INDICATOR_FRAME_UP
	confirmEvolution.frameText     = '点击开始进化'
	confirmEvolution.attachFunc    = nil
	confirmEvolution.attachFuncParam = nil
	confirmEvolution.triggerFunc   = nil
	confirmEvolution.triggerFuncParam = nil
	confirmEvolution.triggerWin    = 'Craft_Evolution/Confirm'
	confirmEvolution.attachWin     = 'Craft_Evolution/Confirm'
	confirmEvolution.attachWinRoot = 'Craft_Evolution'
	confirmEvolution.priority      = 3
	confirmEvolution.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Evolution', false)
	confirmEvolution.fallbackFuncParam = self
	confirmEvolution.fallbackPri   = 0

	
	
	self.indicatorSpecs = {
		openEvolutionUI, placeEquipItem, placeMaterialItem, confirmEvolution
	}
	self.placeEquipItemSpec = placeEquipItem
	self.placeMaterialItemSpec = placeMaterialItem
	
	self.indicatorFlows = nil
	
	self.EquipItems = {
		[WM_PROF_XIANGLONG] = '5_71',
		[WM_PROF_BAIZHANG] = '5_72',
		[WM_PROF_YAOGUANG] = '5_69',
		[WM_PROF_JIYING] = '5_70',
	}
	
	self.MaterialItems = {
		[WM_PROF_XIANGLONG] = '4_1511',
		[WM_PROF_BAIZHANG] = '4_1512',
		[WM_PROF_YAOGUANG] = '4_1513',
		[WM_PROF_JIYING] = '4_1510',
	}
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	local unit = GetPlayersUnit()
	if not unit or not unit.GetProfession then
		lout('Evolution:Enter - GetProfession failed')
		self.indicatorFlows = {}
		return
	end
	local eProf = unit:GetProfession()
	self.placeEquipItemSpec.attachItemID = self.EquipItems[eProf]
	self.placeMaterialItemSpec.attachItemID = self.MaterialItems[eProf]
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvolutionUIAttached(self, indicator)
	indicator.bTriggered = true
	indicator.pFrame:Hide()
end	

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

