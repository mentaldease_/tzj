--The key code using "Virtual Key Code"--
---- 0 : means no key to respond this operation ----
---- 1 : mouse left button
---- 2 : mouse right button

InputItems(
				"NoKey					 =	0",
				"MouseLButton		 =  1",
				"MouseRButton		 =	2",
				"Escape					 =	27",
        "1             	 =	49",
        "2             	 =	50",
        "3           		 =	51",
        "4            	 =	52",
        "5            	 =	53",
        "6             	 =	54",
        "7           		 =	55",
        "8          	 	 =	56",
        "9            	 =	57",
        "0            	 =	48",
        ------------------------------
        "Minus           =	189",   -- '-' on main keyboard
        "Equals					 =	187",		-- '=' on main keyboard
        "Backspace			 =	8",    	-- backspace
        "Tab						 =	9",
        "Q               =	81",
        "W               =	87",
        "E               =	69",
        "R               =	82",
        "T               =	84",
        "Y               =	89",
        "U               =	85",
        "I               =	73",
        "O               =	79",
        "P               =	80",
        "LeftBracket     =	219",			-- '[' on main keyboard
        "RightBracket    =	221",			-- ']' on main keyboard
        "Return					 =	13",    	-- Enter on main keyboard
        "CapsLock				 =	20",
        "A               =	65",
        "S               =	83",
        "D               =	68",
        "F               =	70",
        "G               =	71",
        "H               =	72",
        "J               =	74",
        "K               =	75",
        "L               =	76",
        "Semicolon       =	186",			-- ';' on main keyboard
        "Apostrophe			 =	222",			-- ''' on main keyboard
        "Backslash       =	220",    	-- '\' on main keyboard
        "Shift       		 =	16",
        "Z               =	90",
        "X               =	88",
        "C               =	67",
        "V               =	86",
        "B               =	66",
        "N               =	78",
        "M               =	77",
        "Comma           =	188",			-- ',' on main keyboard
        "Period          =	190",     -- '.' on main keyboard
        "Slash           =	191",    	-- '/' on main keyboard
        "Alt        		 =	18",
        "Control			   =	17",
        "Space           =	32",
        "F1              =	112",
        "F2              =	113",
        "F3              =	114",
        "F4              =	115",
        "F5              =	116",
        "F6              =	117",
        "F7              =	118",
        "F8              =	119",
        "F9              =	120",
        "F10             =	121",
        "F11						 =  122",
        "F12						 =  123",
        "ArrowUp         =	38",    	--	UpArrow on arrow keypad
        "ArrowLeft       =	37",    	--	LeftArrow on arrow keypad
        "ArrowRight      =	39",    	--	RightArrow on arrow keypad
        "ArrowDown       =	40",    	--	DownArrow on arrow keypad
        
        "Num-Lock_0			 =  96",
        "Num-Lock_1			 =  97",
        "Num-Lock_2			 =  98",
        "Num-Lock_3			 =  99",
        "Num-Lock_4			 =  100",
        "Num-Lock_5			 =  101",
        "Num-Lock_6			 =  102",
        "Num-Lock_7			 =  103",
        "Num-Lock_8			 =  104",
        "Num-Lock_9			 =  105",
        
        "Num-Lock_Multiply			 =  106",
        "Num-Lock_Add						 =  107",
        "Num-Lock_Separator			 =  108",
        "Num-Lock_Subtract			 =  109",
        "Num-Lock_Decimal			 	 =  110",
        "Num-Lock_Divide			   =  111",
        
        "End						 =  35",
        "Home						 =  36",
        "Insert					 =  45",
        "NumLock				 =  144",
        "Tilde  				 =  192",			 --	'~' on main keyboard
        "Snapshot				 =  44",			 -- screen print
        ----------------------------------------------------------------
				"MOUSE_LB_CLICK	 = 261",
				"MOUSE_RB_CLICK	 = 262",
		
		    --------------------------------------------
				--效率问题此处值在代码中有硬编码，不要修改
				"MOUSE_LB_DRAG_X_LEFT	 = 263",
				"MOUSE_LB_DRAG_X_RIGHT	 = 264",
				"MOUSE_LB_DRAG_Y_UP	 = 265",
				"MOUSE_LB_DRAG_Y_DOWN	 = 266",
		
				"MOUSE_RB_DRAG_X_LEFT	 = 267",
				"MOUSE_RB_DRAG_X_RIGHT	 = 268",
				"MOUSE_RB_DRAG_Y_UP	 = 269",
				"MOUSE_RB_DRAG_Y_DOWN	 = 270",
				--------------------------------------------
				"MOUSE_WHEEL = 271",
				
				"MOUSE_LB_DOUBLE_CLICK	 = 272",
				"MOUSE_RB_DOUBLE_CLICK	 = 273",
				
				"MOUSE_LB_DOUBLE_BUTTON  = 274",
				"MOUSE_RB_DOUBLE_BUTTON  = 275",
				-------------------------------------------
				--JoyStick
				"JoystickAction_Jump	= 300",
				"JoystickAction_Interactive = 301",
				"JoystickAction_LB = 302",
				"JoystickAction_RB = 303",
				
				"JoystickAction_CameraBack = 304",
				"JoystickAction_FuncSkill = 305",
				"JoystickAction_Mouse = 306",
				"JoystickAction_Fn = 307",
				
				"JoystickAction_Back = 308",
				"JoystickAction_Start = 309",
				
				"JoystickAction_Lock = 310",
				
				"JoystickAction_Move = 311",
				"JoystickAction_Camera = 312",
				
				"JoystickAction_ShortCut_1 = 313",
				"JoystickAction_ShortCut_2 = 314",
				"JoystickAction_ShortCut_3 = 315",
				"JoystickAction_ShortCut_4 = 316",
				
				"JoystickAction_Skill_1 = 317",
				"JoystickAction_Skill_2 = 318",
				"JoystickAction_Skill_3 = 319",
				"JoystickAction_Skill_4 = 320",
				--"JoystickAction_ShortCut_5 = 321",
				--"JoystickAction_ShortCut_6 = 322",
				"JoystickActionQTE_Q = 321",
				"JoystickActionQTE_E = 322",
				"JoystickActionMouseSpeedDown = 323",
				"JoystickAction_UltralSkill = 324"
)