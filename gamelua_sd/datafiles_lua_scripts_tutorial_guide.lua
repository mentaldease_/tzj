local modname = ...

module(modname, package.seeall)


local _guide_list = {}
local _guide_inited = {}

function defineGuide(guide_name)
	local _module = _M
	local _guide = {}
	_guide.name = guide_name
	
	setmetatable(_guide, { __index = _G })
	_guide_list[guide_name] = _guide
	
	-- Export to guide module, thus can refer as guide.XXGuide
	_module[guide_name] = _guide
	
	setfenv(2, _guide)
	return _guide
end

function createGuide(guide_name, ...)
	local _guide = _guide_list[guide_name]
	if not _guide then
		return nil
	end
	
	_guide_inited[guide_name] = _guide
	
	_guide.name = guide_name
	_guide.completed   = false
	_guide.displaying  = false
	_guide.createdTime = 0
	_guide.displayTime = 0
	
	if _guide.Init then
		_guide:Init(...)
	end
	return _guide
end

function destroyGuide(guide_name)
	local _guide = _guide_list[guide_name]
	if not _guide then
		return
	end
	
	if _guide.UnInit then
		_guide:UnInit()
	end
	_guide_inited[guide_name] = nil
end

function getGuides()
	return _guide_inited
end
