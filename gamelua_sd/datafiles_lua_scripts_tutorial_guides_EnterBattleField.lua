
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local openBattleUI = indicatorFlow.createIndicatorSpec()
	openBattleUI.frameName     = 'openBattleUI'
	openBattleUI.frameType     = indicatorFlow.INDICATOR_FRAME_UP
	openBattleUI.frameText     = '点击红色的小旗子打开战场排队界面'
	openBattleUI.triggerWin    = 'Map_LD/PvpQueue'
	openBattleUI.attachWin     = 'Map_LD/PvpQueue'
	openBattleUI.attachWinRoot = 'Map_LD'
	openBattleUI.attachFunc    = nil
	openBattleUI.attachFuncParam = nil
	openBattleUI.priority      = 1
	
	local selectBattle = indicatorFlow.createIndicatorSpec()
	selectBattle.frameName     = 'selectBattle'
	selectBattle.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	selectBattle.frameText     = '选择要去的战场'
	selectBattle.triggerWin    = 'Npc_PVP/Tab1/BtnBG/Option1'
	selectBattle.attachWin     = 'Npc_PVP/Tab1/BtnBG/Option1'
	selectBattle.attachWinRoot = 'Npc_PVP'
	selectBattle.attachFunc    = nil
	selectBattle.attachFuncParam = nil
	selectBattle.priority      = 2
	
	local joinBattle = indicatorFlow.createIndicatorSpec()
	joinBattle.frameName     = 'joinBattle'
	joinBattle.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	joinBattle.frameText     = '点击加入战场队列'
	joinBattle.triggerWin    = 'Npc_PVP/Tab1/Enter'
	joinBattle.attachWin     = 'Npc_PVP/Tab1/Enter'
	joinBattle.attachWinRoot = 'Npc_PVP'
	joinBattle.attachFunc    = nil
	joinBattle.attachFuncParam = nil
	joinBattle.priority      = 3
	
	self.indicatorSpecs = {
		openBattleUI, selectBattle, joinBattle
	}
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

