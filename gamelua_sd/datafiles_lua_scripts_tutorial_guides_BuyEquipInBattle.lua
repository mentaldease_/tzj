
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.progressed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local buyBattleEquip = indicatorFlow.createIndicatorSpec()
	buyBattleEquip.frameName     = 'buyBattleEquip'
	buyBattleEquip.frameType     = indicatorFlow.INDICATOR_FRAME_LEFT
	buyBattleEquip.frameText     = self.guideCfg.IndicatorText
	buyBattleEquip.attachFunc    = nil
	buyBattleEquip.attachFuncParam = nil
	buyBattleEquip.triggerFunc   = nil
	buyBattleEquip.triggerFuncParam = nil
	buyBattleEquip.triggerWin    = 'BattleShop/ListFrame/Container/Template1'
	buyBattleEquip.attachWin     = 'BattleShop/ListFrame/Container/Template1'
	buyBattleEquip.attachWinRoot = 'BattleShop'
	buyBattleEquip.priority      = 0
	
	self.indicatorSpecs = {
		buyBattleEquip, 
	}
	
	self.buyActionConn = nil
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
	self.buyActionConn = gameEventMgr:subscribeEvent('Tutorial_BattleShopBuyItem', self.OnEvent, self)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	if self.buyActionConn then
		self.buyActionConn:disconnect()
		self.buyActionConn = nil
	end
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then return end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)
	self.completed = true
	
end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

