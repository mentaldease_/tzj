local modname = ...

module(modname, package.seeall)

local frame_classes = {}
local frame_instances = {}

function defineFrame(frame_name, layout_name, args)
	local _module = _M
	local _frame = {}
	_frame.layout = layout_name
	
	if args and args.export == false then
		_frame.__export__ = false
	else
		_frame.__export__ = true
	end
	
	setmetatable(_frame, { __index = _G })
	frame_classes[frame_name] = _frame
	
	setfenv(2, _frame)
	return _frame
end

function createFrame(frame_name, ...)
	local _module = _M
	local frame_class = frame_classes[frame_name]
	
	-- Create instance from class
	local frame_instance = {}
	setmetatable(frame_instance, {__index = frame_class})
	
	-- Attatch to c++ object
	local win_ptr = SD.CreateCWindowByLayout(frame_class.layout)
	tolua.setpeer(win_ptr, frame_instance)
	frame_instance = win_ptr
	
	-- Store instance's name
	if arg.n == 0 then
		frame_instance._name_ = frame_name
	elseif type(arg[1]) == 'string' then
		local new_name = frame_name..arg[1]
		frame_instance._name_ = new_name
	end
	-- Export to ui module, thus can refer like ui.XXFrame
	if frame_class.__export__ == true then
		_module[frame_instance._name_] = frame_instance
	end

	-- Store for management
	frame_instances[frame_instance._name_] = frame_instance
	lout('Create Frame `' .. frame_instance._name_ .. '\'')
	
	-- Call Init method if defined
	if frame_instance.Init then
		frame_instance:Init()
	end
	
	-- Call Subscribe if defined
	if frame_instance.Subscribe then
		frame_instance:Subscribe()
	end
	
	return frame_instance
end

-- @param frame_instance - can be frame name or frame instance
function destroyFrame(frame_instance)
	local _module = _M
	
	if type(frame_instance) == 'string' then
		local frame_name = frame_instance
		frame_instance = frame_instances[frame_name]
	end
	
	local v = frame_instance
	if v then
		lout('Destory Frame `' .. v._name_ .. '\'')
		frame_instances[v._name_] = nil
		_module[v._name_] = nil
		
		if v.Hide then v:Hide() end
		if v.UnInit then v:UnInit() end
		tolua.setpeer(v, nil)
		SD.DestroyCWindow(v)
	end
end

function createFrames()
	for k, v in pairs(frame_classes) do
		createFrame(k)
	end
end

function destroyFrames()
	local _module = _M
	for k, v in pairs(frame_instances) do
		if v then
			lout('Destory Frame `' .. v._name_ .. '\'')
			
			if v.Hide then v:Hide() end
			if v.UnInit then v:UnInit() end
			tolua.setpeer(v, nil)
			SD.DestroyCWindow(v)
			
			_module[k] = nil
			frame_instances[k] = nil
		end
	end
	frame_instances = {}
end

