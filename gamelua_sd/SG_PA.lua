GroupBegin("CPB")

--SG_PA--
STake_Start("SG_PA_attack_000","SG_PA_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")     
    Priority(0,"5")   
  
    MustPlay(0.00,0.199)  
    Soft(0.199,0.666)
    
		
  	AttackStart()
  	Hurt(0.139,"hurt_11")
  	

  	Sound(0.035,"SG_attackVoc_1")
  	Sound(0.085,"HG_GB_PA_Attack")  	  	
  	HurtSound(0.139,"PA_Hit_002") 

    DirFx(0.139,"SG_GB_PA_hit_000",25,1)

STake_Start("SG_PA_attack_001","SG_PA_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")     
    Priority(0,"5")   
  
    MustPlay(0.00,0.272)  
    Soft(0.272,0.8)
    
		
  	AttackStart()
  	Hurt(0.216,"hurt_11")


  	Sound(0.145,"SG_attackVoc_1")
  	Sound(0.195,"HG_GB_PA_Attack")  	  	
  	HurtSound(0.216,"PA_Hit_002") 

    DirFx(0.216,"SG_GB_PA_hit_000",25,1)

STake_Start("SG_PA_attack_002","SG_PA_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")     
    Priority(0,"5") 
        
    MustPlay(0.00,0.500)  
  		
  	AttackStart()
  	Hurt(0.160,"hurt_21")

  	Sound(0.050,"SG_attackVoc_1")
  	Sound(0.100,"HG_GB_PA_Attack")  	  	
  	HurtSound(0.160,"PA_Hit_001") 

    DirFx(0.160,"SG_GB_PA_hit_001",25,1)
    

STake_Start("SG_PA_def_000","SG_PA_def_000")
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("SG_PA_dodge_000","SG_PA_dodge_000")
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("SG_PA_hit","SG_PA_hit")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_hurt")
    
STake_Start("SG_PA_move_001","SG_PA_move_001")
    BlendTime(0.2)
    BlendMode(1)
    Sound(0.340,"PC_step_normal_1")
    Sound(0.900,"PC_step_normal_2")
  Loop()               
    
STake_Start("SG_PA_move_003","SG_PA_move_003")
    BlendTime(0.2)
    BlendMode(1)
    Sound(0.400,"PC_step_normal_1")
    Sound(0.900,"PC_step_normal_2")
  Loop()  
    
STake_Start("SG_PA_move_004","SG_PA_move_004")
    BlendTime(0.2)
    BlendMode(1) 
    Sound(0.337,"PC_step_normal_1")
    Sound(0.700,"PC_step_normal_2")
  Loop()
  
STake_Start("SG_PA_move_005","SG_PA_move_005")
    BlendTime(0.2)
    BlendMode(1) 
    Sound(0.337,"PC_step_normal_1")
    Sound(0.700,"PC_step_normal_2")
  Loop()
    
STake_Start("SG_PA_move_006","SG_PA_move_006")
    BlendTime(0.2)
    BlendMode(1)
    Sound(0.337,"PC_step_normal_1")
    Sound(0.700,"PC_step_normal_2")
  Loop()
    
STake_Start("SG_PA_move_009","SG_PA_move_009")
    BlendTime(0.2)
    BlendMode(0)

  Loop() 
    
STake_Start("SG_PA_move_010","SG_PA_move_010")
    BlendTime(0.2)
    BlendMode(0)
  Loop()
    
STake_Start("SG_PA_wait_000","SG_PA_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("wait") 
    Loop()