ControlItems(
    "NULL_ACTION = 0",
-------------------------------------
		"MOVE_UP = 1",
		"MOVE_DOWN = 2",
		"MOVE_LEFT = 3",
		"MOVE_RIGHT = 4",
		"TURN_LEFT = 5",
		"TURN_RIGHT = 6",
		"ATTACK = 7",
		"JUMP = 8",
		"GUARD = 9",
		"CHANGE_WEAPON = 10",
		"TOGGLE_SHOW_WEAPON = 11",

		"MINUS = 22",
		"EQUAL = 23",
		"GENERIC_ATTACK = 24",
		"RBSKILL_ATTACK = 25",
-------------------------------------
		"CAMERA_ROTATE = 26",

		"CAMERA_ROTATE_X_LEFT = 27",
		"CAMERA_ROTATE_X_RIGHT = 28",
		"CAMERA_ROTATE_Y_UP = 29",
		"CAMERA_ROTATE_Y_DOWN = 30",

-------------------------------------
		"CAMERA_FACE_ROTATE = 31",

		"CAMERA_FACE_ROTATE_X_LEFT = 32",
		"CAMERA_FACE_ROTATE_X_RIGHT = 33",
		"CAMERA_FACE_ROTATE_Y_UP = 34",
		"CAMERA_FACE_ROTATE_Y_DOWN = 35",

-------------------------------------
		"BACKTODEFAULTCAMERA = 36",

		"FRAME1_BUTTON_1 = 37",
		"FRAME1_BUTTON_2 = 38",
		"FRAME1_BUTTON_3 = 39",
		"FRAME1_BUTTON_4 = 40",
		"FRAME1_BUTTON_5 = 41",
		"FRAME1_BUTTON_6 = 42",
		"FRAME1_BUTTON_7 = 43",
		"FRAME1_BUTTON_8 = 44",
		"FRAME1_BUTTON_9 = 45",
		"FRAME1_BUTTON_10 = 46",
		"BREAK_SKILL = 47",
		
		"CHANGE_OPERATION_MODE = 48",
		"MOVE = 49",
		"COLLECT_DROPITEM = 50",

    "HEADUP_DISPLAY_TOGGLE = 53",
    "DRAWTEXT_ON_DROPITEM_TOGGLE = 54",
    "AUTO_SELECT_TARGET_2.5D = 55",
    "AUTO_SELECT_TARGET_3D = 56",
------------------UI hot keys-----------------
		"PlayerAvaterUI = 57",
		"PackageUI = 58",
		"TaskUI = 59",
		"SkillUI = 60",
		"SocialSystemUI = 61",
		"GroupsUI = 62",
		"FamilyUI = 63",
		"WorldMapUI = 64",

		"FRAME2_BUTTON_1 = 65",
		"FRAME2_BUTTON_2 = 66",
		"FRAME2_BUTTON_3 = 67",
		"FRAME2_BUTTON_4 = 68",
		"FRAME2_BUTTON_5 = 69",
		"FRAME2_BUTTON_6 = 70",
		"FRAME2_BUTTON_7 = 71",
		"FRAME2_BUTTON_8 = 72",
		"FRAME2_BUTTON_9 = 73",
		"FRAME2_BUTTON_10 = 74",
-----------------------------------------------
		"CameraZoom = 75",
		"FovAdjust = 76",
		"ChatActiveEvent = 77",
		"SWITCH_ALLOW_BILLBOARD = 78",
		"FRAME1_BUTTON_11 = 79",
		"FRAME1_BUTTON_12 = 80",
		"FRAME2_BUTTON_11 = 81",
		"FRAME2_BUTTON_12 = 82",
		
-----------------------------------------------
		"FreeCameraMoveForward = 83",
		"FreeCameraMoveBackward = 84",
		"FreeCameraMoveLeft = 85",
		"FreeCameraMoveRight = 86",
		"FreeCameraMoveUp = 87",
		"FreeCameraMoveDown = 88",
		"FreeCameraSpeedIns = 89",
		"FreeCameraSpeedDes = 90",
		"FreeCameraRotateSpeedIns = 91",
		"FreeCameraRotateSpeedDes = 92",
		"FreeCameraBufferredRotate = 93",
		"FreeLoseTarget = 94",
		"FreeRightCycle = 95",
		"FreeLeftCycle = 96",
		"FreeCameraLockedTargetRotateSpeedIns = 97",
		"FreeCameraLockedTargetRotateSpeedDes = 98",
		"FreeUpCycle = 99",
		"FreeDownCycle = 100",
		"FreeFocusLeft = 101",
		"FreeFocusRight = 102",
		"FreeFocusUp = 103",
		"FreeFocusDown = 104",
		"EagleReplay = 105",
		"FreeCameraCollisionToggleOnOff = 106",
-----------------------------------------------
		"HideAllUI = 107",
		"HideAllUIButCursor = 108",
		"ShowBuffUI = 109",

		"CameraRestoral = 110",

		"FPS_MODE_TOGGLE = 111",
		"3D_Generic_Attack = 112",

		"25D_Generic_Attack = 114",

		"Select_Entity = 113",
		
		"MOUNT = 116",
		
		"CameraZoomIn = 117",
		"CameraZoomOut = 118",
		"ChatActiveEvent_Slash = 119",
		"Interact_With_Target = 120",
		"Auto_MoveTo_Target = 121",
		
-----------------------------------------------

		"XunMa = 122",
		"BaiTan = 123",
		"YiJia = 124",
		"XunBao = 125",
		"FangWuJianShe = 126",
		"TianZhiJuan = 127",
		"DiZhiJuan = 128",
		"RenZhiJuan = 129",
		"ChangeXinFa = 130",
		
		"ScreenShot = 131",
		"HideOtherPlayers = 132",
		"WindowMinimize = 133",
		"AutoRun = 134",
		"ExitGame = 135",
		
		"BOW_BUTTON_1 = 136",
		"BOW_BUTTON_2 = 137",
		"BOW_BUTTON_3 = 138",
		"BOW_SWITCH_OPERATION = 139",
		"FPS_UICONTROL = 140",
		
		"QINGGONG = 141",

		"ChangePVPMode = 147",
		"FISHING = 148",
		"NORMAL_UICONTROL = 149",
		"NORMAL_UICONTROL_HOLD = 150",
		"FPS_UICONTROL_HOLD = 151",
		
		"LockTarget = 155",
		"ChangeTarget = 156",
		"InteractTarget = 157",
		"SHIFT_FUNCTION = 158",
		"FreeCameraMode = 159",
		"SkillSettingUI = 160",
		"QTE_FUNCTION_Q = 162",
		"QTE_FUNCTION_E = 163",
		"OneKey_QINGGONG = 164",
		
		"LeftKey_Skill = 165",
		"RightKey_Skill = 166",
		"KongfuUI = 167",
		"RollAssign = 168",
		"ZeroMode1 = 169",
		"ZeroMode2 = 170",
		"ClassicMode = 171",

---------------------------------------
		"FURN_TRY_PLACE     = 172",
		"FURN_MOVE_FORWARD  = 173",
		"FURN_MOVE_BACKWARD = 174",
		"FURN_MOVE_LEFT     = 175",
		"FURN_MOVE_RIGHT    = 176",
		"FURN_MOVE_UP       = 177",
		"FURN_MOVE_DOWN     = 178",
		"FURN_ROTATE_LEFT   = 179",
		"FURN_ROTATE_RIGHT  = 180",
		"FURN_MOVE_ANYWHERE = 181",
		"KEY_FURN_ROTATE_LEFT   = 182",
		"KEY_FURN_ROTATE_RIGHT  = 183",

-----------------------------------------------
		"JourneyUI = 184",
		"OpenGM_Panel = 185",
		"FunctionSkill = 186",
		"Fatigue = 187",
		"SheathWeapon = 188",
-----------------------------------------------
		"DOTA_SKILL_1 = 189",
		"DOTA_SKILL_2 = 190",
		"DOTA_SKILL_3 = 191",
		"DOTA_SKILL_4 = 192",
		"SPECIAL_DAZUO = 193",
-----------------------------------------------
		"MiniMap_ZoomIn  = 194",
		"MiniMap_ZoomOut = 195",
		"Polish = 196",
		"SPECIAL_DAZHAO = 197",
		"NewFriend = 198",
		"ActiveJoystick = 199",
		
		"DOTA_ITEM_1 = 200",
		"DOTA_ITEM_2 = 201",
		"DOTA_ITEM_3 = 202",
		"DOTA_ITEM_4 = 203",
		"Fx_Logic_Show = 204",
		"QTE_WAVE = 205",
		
		"DOTA_ADD_SKILL_1 = 206",
		"DOTA_ADD_SKILL_2 = 207",
		"DOTA_ADD_SKILL_3 = 208",
		"DOTA_ADD_SKILL_4 = 209",
		"DOTA_BATTLEFIELD_SHOP = 210",
		"CraftUI = 211",
		"WardrobeUI =212",
		"UIEdit = 213",
		"WhisperWindow = 214",
		"MallUI = 215",
		"BattleScore = 216",
		"ROGUELIKEITEM = 217",
		"ExpandWndUI = 218",
		"QTE_FUNCTION_F =219",
		"RightBottomChatbox = 220",
		"ScriptDebugUI = 500",
		"DataWatcher = 501"
)
