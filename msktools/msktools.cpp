#include "stdafx.h"
#include "msktools.h"
#include <stdio.h>
#include <stdarg.h>
#include <windows.h>
#include <tchar.h>
#include <exception>

#ifndef _PRINTHEAD
#define _PRINTHEAD "msk_tg:%s\n"
#endif
#ifndef _WPRINTHEAD
#define _WPRINTHEAD L"msk_tg:%s\n"
#endif

class CWin32Excetion :public std::exception
{
public:
	CWin32Excetion(DWORD);
	~CWin32Excetion();
	const char *what() const;
private:
	char* m_lpMsgBuf;
};

const char *CWin32Excetion::what() const 
{
	return m_lpMsgBuf; 
}
CWin32Excetion::CWin32Excetion(DWORD dwError)
{
	FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language 

		(char *)&m_lpMsgBuf,
		0,
		NULL
		);
}

CWin32Excetion::~CWin32Excetion()
{
	LocalFree(m_lpMsgBuf);
}

void MskThrowWin32Exception()
{
	DWORD dwError = GetLastError();
	if (0 != dwError)
		throw CWin32Excetion(dwError);
}

void MyPrint(char *msg)
{
	if (msg == 0) return;
	char newmsg[1024] = { 0 };
	sprintf_s(newmsg, _PRINTHEAD, msg);
	OutputDebugStringA(newmsg);
}

void MyDbgPrint(const char *fmt, ...)
{
	va_list	ap;
	char	sz[1024];

	va_start(ap, fmt);
	vsprintf(sz, fmt, ap);
	MyPrint(sz);
	va_end(ap);
}

void MyDbgPrint(char *fmt, ...)
{
	va_list	ap;
	char	sz[1024];

	va_start(ap, fmt);
	vsprintf(sz,fmt, ap);
	MyPrint(sz);
	va_end(ap);
}

void MyDbgPrint(wchar_t *fmt, ...)
{
	OutputDebugStringW(fmt);
	return;
	va_list	ap;
	wchar_t	sz[1024];

	va_start(ap, fmt);
	vswprintf_s(sz,1024, fmt, ap);
	wsprintfW(sz, _WPRINTHEAD, sz);
	OutputDebugStringW(sz);
	va_end(ap);
}


BOOL Inject(unsigned long pid,const char *dllpath, int noelevateprivs, int freedll, int sysdir)
{
	unsigned long len;
	void *remotelib;
	char dllname[MAX_PATH];
	HANDLE hToken, hProcess, hThread;
	HMODULE kernel32lib;
	LPTHREAD_START_ROUTINE procaddr;
	LUID luid;
	TOKEN_PRIVILEGES tp, oldtp;
	if (!noelevateprivs) {
		if (!OpenProcessToken(GetCurrentProcess(),
			TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)){
			PRT("OpenProcessToken ERROR");
			return FALSE;
		}
		PRT("process token opened, handle 0x%x\n", hToken);

		if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid)){
			PRT("LookupPrivilegeValue ERROR");
			return FALSE;
		}
		PRT("debug luid: %08x:%08x\n", luid.HighPart, luid.LowPart);

		len = sizeof(TOKEN_PRIVILEGES);

		tp.PrivilegeCount = 1;
		tp.Privileges->Luid = luid;
		tp.Privileges->Attributes = 0;
		if (!AdjustTokenPrivileges(hToken, 0, &tp,
			sizeof(TOKEN_PRIVILEGES), &oldtp, &len)){
			PRT("AdjustTokenPrivileges ERROR");
			return FALSE;
		}

		oldtp.PrivilegeCount = 1;
		oldtp.Privileges->Luid = luid;
		oldtp.Privileges->Attributes |= SE_PRIVILEGE_ENABLED;
		if (!AdjustTokenPrivileges(hToken, 0, &oldtp,
			sizeof(TOKEN_PRIVILEGES), NULL, NULL)){
			PRT("re AdjustTokenPrivileges ERROR");
			return FALSE;
		}
		PRT("elevated process privileges sucessfully!");
	}
	hProcess = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION |
		PROCESS_VM_OPERATION | PROCESS_VM_WRITE |
		PROCESS_VM_READ, 0, pid);
	if (!hProcess){
		PRT("OpenProcess EROOR");
		return FALSE;
	}
		


	if (sysdir) {
		strncpy(dllname, dllpath, sizeof(dllname));
		len = strlen(dllname) + 1;
	}
	else{
		strcpy(dllname, dllpath);
		len = strlen(dllname) + 1;
	}
	
	remotelib = VirtualAllocEx(hProcess, NULL, len, MEM_COMMIT, PAGE_READWRITE);
	if (!remotelib){
		PRT("VirtualAllocEx EROOR");
		return FALSE;
	}
	WriteProcessMemory(hProcess, remotelib, dllname, len, NULL);

	kernel32lib = GetModuleHandleA("kernel32");

	procaddr = (LPTHREAD_START_ROUTINE)GetProcAddress(kernel32lib,
		freedll ? "FreeLibraryA" : "LoadLibraryA");

	hThread = CreateRemoteThread(hProcess, NULL, 0,
		procaddr, remotelib, 0, NULL);
	if (hThread){
		WaitForSingleObject(hThread, INFINITE);
	}
	else{
		PRT("CreateRemoteThread Failed");
	}
	VirtualFreeEx(hProcess, remotelib, len, MEM_DECOMMIT);
	if (hThread)
		fCloseHandle(hThread);
	if (hProcess)
		fCloseHandle(hProcess);
	PRT("successfully %sjected %s into %d, thread handle 0x%08x.",
		freedll ? "de" : "in", dllname, pid, hThread);
	return TRUE;
}