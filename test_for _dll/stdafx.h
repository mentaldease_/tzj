// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <WinSock2.h>
#ifdef _VER_RELEASE
extern decltype(CreateFileW)* fCreateFileW;
extern decltype(ReadFile)* fReadFile;
extern decltype(WriteFile)* fWriteFile;
extern decltype(CloseHandle)* fCloseHandle;
extern decltype(GetFileSize)* fGetFileSize;
#else
#define fCreateFileW  MyCreateFileW
#define fReadFile  ReadFile
#define fWriteFile  WriteFile
#define fCloseHandle  CloseHandle
#define fGetFileSize  GetFileSize
#endif

// TODO: 在此处引用程序需要的其他头文件
