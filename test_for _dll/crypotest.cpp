/*
* MyRSA.cpp
*
*  Created on: 2013-3-7
*      Author: hust
*/
#include "stdafx.h"
#include <windows.h>
#include "../lua514/lua.hpp"
#include <math.h>
//#pragma comment(lib, "../Crypto++/cryptlib_debug.lib")
#include "MyRSA.h"
#include <time.h>
#include <fstream>
MyRSA::MyRSA()
{
	
}

MyRSA::~MyRSA()
{

}
/*
* Description: this function is used to calcuate the string 'message' 's hash value
* Input:
*  message: the init string to be hashed
* Output:
*  return the hash of the parameter
*/
string MyRSA::MD5(const char * message)
{
	string digest;
	Weak::MD5 md5;
	StringSource(message, true,
		new HashFilter(md5, new HexEncoder(new StringSink(digest))));
	return digest;
}
/*
* Description: to calculate the hash of the file and return the hash value(string)
* Input:
*  filename: the file to be calculated the hash value
* Output:
*  return the hash value of the file and its type is string
*/
string MyRSA::MD5File(const char * filename)
{
	string digest;
	Weak::MD5 md5;
	FileSource(filename, true,
		new HashFilter(md5, new HexEncoder(new StringSink(digest))));
	return digest;

}

/*
* Description: generate the RSA public key and private key in separate file
* Input:
*  KeyLength: the length of the key, such as 1024...
*  privFilename: private key file name you want to store the private key
*  pubFilename: public key file name you want to store the public key
* Output:
*  nothing
*/
void MyRSA::GenerateRSAKey(unsigned int keyLength, const char *privFilename,
	const char *pubFilename)
{
	RSAES_OAEP_SHA_Decryptor priv(_rng, keyLength);
	HexEncoder privFile(new FileSink(privFilename));
	priv.DEREncode(privFile);
	privFile.MessageEnd();

	RSAES_OAEP_SHA_Encryptor pub(priv);
	HexEncoder pubFile(new FileSink(pubFilename));
	pub.DEREncode(pubFile);
	pubFile.MessageEnd();
}

/*
* Description: this function is used to encrypt the string 'plainText' with the
*              private key, and return the cipher
* Input:
*  pubFilename: the public key
*  message: the string to be encrypted
* OutPut:
*  return the cipher
*/
string MyRSA::Encrypt(const char * pubFilename, const byte * source,size_t len)
{
	FileSource pubFile(pubFilename, true, new HexDecoder);

	RSAES_OAEP_SHA_Encryptor pub(pubFile);
	string result;
	StringSource(source,len, true,
		new PK_EncryptorFilter(_rng, pub,
		new HexEncoder(new StringSink(result))));
	return result;
}
/*
* Description: decrypt the cipher with the private key
* Input:
*  privFilename: the private key file
*  ciphertext: the string to be decrypted
* Output:
*  return the decrypted string
*/
string MyRSA::Decrypt(const char * privFilename, const char * ciphertext)
{
	//FileSource privFile(privFilename, true, new HexDecoder);
	StringSource privFile(privFilename, true, new HexDecoder);
	RSAES_OAEP_SHA_Decryptor priv(privFile);
	string result;
	StringSource(ciphertext, true,
		new HexDecoder(
		new PK_DecryptorFilter(_rng, priv,
		new StringSink(result))));
	return result;
}

/*
* Description: sign the file with the private key, and generate the signature file
* Input:
*  privFilename: the private key file
*  messageFilename: the file to be signed
*  signatureFilename: the signature file to be generated
* Output:
*  nothing
*/
void MyRSA::SignFile(const char * privFilename, const char *messageFilename,
	const char * signatureFilename)
{
	FileSource priFile(privFilename, true, new HexDecoder);
	RSASS<PKCS1v15, SHA>::Signer priv(priFile);
	FileSource f(messageFilename, true,
		new SignerFilter(_rng, priv,
		new HexEncoder(new FileSink(signatureFilename))));
}

/*
* Description: verify the file with the public key, and return the answer
* Input:
*  pubFilename: the publicFilename
*  messageFilename: the init message file, and it should be not changed
*  signatureFilename: the SignFile function generate, and it's used to verify if the message
*                      file is the original one
* Output:
*  if the message file match the signature file, return true; else return false
*/
bool MyRSA::VerifyFile(const char * pubFilename, const char * messageFilename,
	const char * signatureFilename)
{
	FileSource pubFile(pubFilename, true, new HexDecoder);
	RSASS<PKCS1v15, SHA>::Verifier pub(pubFile);

	FileSource signatureFile(signatureFilename, true, new HexDecoder);
	if (signatureFile.MaxRetrievable() != pub.SignatureLength())
		return false;
	SecByteBlock signature(pub.SignatureLength());
	signatureFile.Get(signature, signature.size());

	VerifierFilter *verifierFilter = new VerifierFilter(pub);
	verifierFilter->Put(signature, pub.SignatureLength());
	FileSource f(messageFilename, true, verifierFilter);

	return verifierFilter->GetLastResult();
}

/*
* Description: sign the string with the private key, and generate the signature
* Input:
*  privFilename: the private key file
*  message: the string to be signed
* Output:
*  return the SecByteBlock signature
*/
SecByteBlock MyRSA::SignString(const char * privFilename, const char * message)
{
	// calculate the md5(HASH) of the message
	string digest = MD5(message);
	FileSource priFile(privFilename, true, new HexDecoder);
	RSASSA_PKCS1v15_SHA_Signer priv(priFile);

	// Create signature space
	size_t length = priv.MaxSignatureLength();
	SecByteBlock signature(length);

	// sign message
	priv.SignMessage(_rng, (const byte*)digest.c_str(), digest.length(),
		signature);

	return signature;
}
/*
* Description: verify the file with the public key, and return the answer
* Input:
*  pubFilename: the publicFilename
*  message: the original message, and it should be not changed
*  signature: the SignString function returned, and it's used to verify if the message
*             is the original one
* Output:
*  if the message match the signature , return true; else return false
*/
bool MyRSA::VerifyString(const char * pubFilename, const char * message,
	const SecByteBlock & signature)
{
	// calculate the md5 of the message first
	string digest = MD5(message);
	FileSource pubFile(pubFilename, true, new HexDecoder);
	RSASSA_PKCS1v15_SHA_Verifier verifier(pubFile);

	bool result = verifier.VerifyMessage((const byte*)digest.c_str(),
		digest.length(), signature, signature.size());
	return result;
}

byte* Xor(byte *&data, size_t len, byte num)
{
	byte *newbuff = new byte[len + 1];
	newbuff[0] = num ^ 0xff;
	for (int i = 0; i < len; ++i){
		newbuff[i + 1] = data[i] ^ num;
	}
	delete[]data;
	data = newbuff;
	return newbuff;
}

byte* RXor(byte *data, size_t len, byte num)
{
	for (int i = 0; i < len; ++i){
		data[i] ^= num;
	}
	return data;
}

int _tmain2(int argc, _TCHAR* argv[]) 
{
	//AES中使用的固定参数是以类AES中定义的enum数据类型出现的，而不是成员函数或变量
	//因此需要用::符号来索引
	cout << "AES Parameters: " << endl;
	cout << "Algorithm name : " << AES::StaticAlgorithmName() << endl;

	//Crypto++库中一般用字节数来表示长度，而不是常用的字节数
	cout << "Block size     : " << AES::BLOCKSIZE * 8 << endl;
	cout << "Min key length : " << AES::MIN_KEYLENGTH * 8 << endl;
	cout << "Max key length : " << AES::MAX_KEYLENGTH * 8 << endl;
	cout << "default key length : " << AES::DEFAULT_KEYLENGTH * 8 << endl;

	//AES中只包含一些固定的数据，而加密解密的功能由AESEncryption和AESDecryption来完成
	//加密过程
	AESEncryption aesEncryptor; //加密器 

	unsigned char aesKey[AES::DEFAULT_KEYLENGTH];  //密钥
	unsigned char inBlock[AES::BLOCKSIZE] = "123456789";    //要加密的数据块
	unsigned char outBlock[AES::BLOCKSIZE]; //加密后的密文块
	unsigned char xorBlock[AES::BLOCKSIZE]; //必须设定为全零

	memset(xorBlock, 0, AES::BLOCKSIZE); //置零

	fstream fs;
	fs.open("G:\\bitbuket\\tg_dll\\script\\start.lua", ios_base::in | ios_base::binary);
	//fs.open("G:\\bitbuket\\tg_dll\\ver_release\\mskdata:msk.sp.start", ios_base::in | ios_base::binary);
	fs.seekg(0, ios::end);
	size_t length = fs.tellg();
	//length = length - 1;
	//double rnum;
	//fs.seekg(length-2, ios::beg);
	//fs.read((char*)&rnum, 2);
	fs.seekg(0, ios::beg);
	byte num;
	fs.read((char*)&num, 1);
	num = num^num;
	byte *buff = new byte[length];
	byte *buff1 = new byte[length];
	byte *buff2 = new byte[length];
	memset(buff2, 0, length); //置零
	fs.read((char *)buff, length);
	fs.close();
	Xor((byte *)buff, length, 125);
	byte bnum = *(byte *)buff;
	buff = buff + 1;
	bnum = bnum^0xff;
	RXor(buff, length, bnum);
	aesEncryptor.SetKey(aesKey, AES::DEFAULT_KEYLENGTH);  //设定加密密钥
	aesEncryptor.ProcessAndXorBlock(buff, buff2, buff1);  //加密

	//以16进制显示加密后的数据
	for (int i = 0; i < 16; i++) {
		cout << hex << (int)buff1[i] << " ";
	}
	cout << endl;

	//解密
	AESDecryption aesDecryptor;
	unsigned char plainText[AES::BLOCKSIZE];

	aesDecryptor.SetKey(aesKey, AES::DEFAULT_KEYLENGTH);
	//细心的朋友注意到这里的函数不是之前在DES中出现过的：ProcessBlock，
	//而是多了一个Xor。其实，ProcessAndXorBlock也有DES版本。用法跟AES版本差不多。
	//笔者分别在两份代码中列出这两个函数，有兴趣的朋友可以自己研究一下有何差异。
	aesDecryptor.ProcessAndXorBlock(outBlock, xorBlock, plainText);


	for (int i = 0; i < 16; i++)
	{
		cout << plainText[i];
	}
	cout << endl;
	delete[]buff;
	delete[]buff1;
	return 0;
}

int tmain(int argc, _TCHAR* argv[]) 
{

	char privFilename[128] = "prvKey", pubFilename[128] = "pubKey";
	unsigned int keyLength = 1024;
	clock_t start, finish;
	double duration;


	// cout << "Key length in bits: ";
	// cin >> keyLength;

	// cout << "\nSave private key to file: ";
	// cin >> privFilename;

	// cout << "\nSave public key to file: ";
	// cin >> pubFilename;

	MyRSA rsa;
	fstream fs;
	fs.open("G:\\bitbuket\\tg_dll\\scriptc\\start.lua", ios_base::in | ios_base::binary);
	fs.seekg(0, ios::end);
	size_t length = fs.tellg();
	fs.seekg(0, ios::beg);
	char *buff = new char[length];
	fs.read(buff, length);
	fs.close();
	string ret;
	try{
		ret = rsa.Encrypt("pubKey", (byte *)buff, length);
	}
	catch (Exception &e){
		const char *err = e.what();
		printf(err);
	}
	printf("%d", ret.size());
	delete[]buff;
	start = clock();
	cout << "============encrypt and decrypt================" << endl;
	//rsa.GenerateRSAKey(keyLength, privFilename, pubFilename);
	//string message = "hello world, i am a student from huazhong university of science and technology!";
	//string ciphertext = rsa.Encrypt(pubFilename, message.c_str());
	//cout << "The cipher is : " << ciphertext << endl;
	fs;
	fs.open("G:\\bitbuket\\tg_dll\\ver_release\\mskdata:msk.sp.start", ios_base::in );
	fs.seekg(0, ios::end);
	length = fs.tellg();
	fs.seekg(0, ios::beg);
	buff = new char[length];
	fs.read(buff, length);
	fs.close();
	fs;
	fs.open("G:\\bitbuket\\tg_dll\\ver_release\\mskdata:msk.priv", ios_base::in);
	fs.seekg(0, ios::end);
	length = fs.tellg();
	fs.seekg(0, ios::beg);
	char *buff1 = new char[length];
	fs.read(buff1, length);
	fs.close();
	//const char* privName = "G:\\bitbuket\\tg_dll\\ver_release\\mskdata:msk.priv";
	//string decrypted = rsa.Decrypt(buff1, buff);
	string decrypted;
	delete[]buff;
	delete[]buff1;
	//string decrypted = rsa.Decrypt(privFilename, ciphertext.c_str());
	cout << "The recover is : " << decrypted << endl;

	finish = clock();
	duration = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << "The cost is : " << duration << " seconds" << endl;


	cout << "==============sign file================" << endl;
	start = clock();
	string messageFilename = "signTest";
	string signatureFilename = "signature";
	rsa.SignFile(privFilename, messageFilename.c_str(),
		signatureFilename.c_str());

	if (rsa.VerifyFile(pubFilename, messageFilename.c_str(),
		signatureFilename.c_str())) {
		cout << "verify correct!" << endl;
	}
	else
		cout << "verify error!" << endl;

	finish = clock();
	duration = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << "The sign file cost is : " << duration << " seconds" << endl;


	cout << "============sign string=================" << endl;
	start = clock();
	string plainText = "Sign me, i am a student from huazhong university of science and technology!";

	SecByteBlock signature = rsa.SignString(privFilename, plainText.c_str());
	cout << "The Signature size is : " << signature.size() << endl;

	//cout << endl << "The signature is : " << signature << endl << endl;

	// save the signature to the file
	//ofstream signatureFile("signatureFile");
	//if(signatureFile.is_open() )
	//{
	// for(int i = 0; i < signature.get
	//}

	if (rsa.VerifyString(pubFilename, plainText.c_str(), signature)) {
		cout << "Verify correct!" << endl;
	}
	else {
		cout << "Verify wrong!" << endl;
	}

	finish = clock();
	duration = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << "The sign string cost is : " << duration << " seconds" << endl;
	return 0;
}

void UnPack(byte *buff, size_t len)
{
	for (int i = 0; i < len; ++i){
		buff[i] = buff[i] ^ 0xdd;
	}
}

#define PRT printf
SOCKET g_sockClient;
int _tmain1(int argc, _TCHAR* argv[])
{
	printf("%d", 10, 20);
	system("pause");
	return 0;
	WSAData data;
	int error;
	error = WSAStartup(MAKEWORD(2, 2), &data);
	SOCKET socketArray[WSA_MAXIMUM_WAIT_EVENTS];
	WSAEVENT eventArray[WSA_MAXIMUM_WAIT_EVENTS];
	DWORD dwTotal = 0;  //记录接入socket的数量
	// 创建socket操作，建立流式套接字，返回套接字号sockClient  
	g_sockClient = socket(AF_INET, SOCK_STREAM, 0);
	if (g_sockClient == INVALID_SOCKET) {
		PRT("Error at socket():%ld\n", WSAGetLastError());
		return -1;
	}
	SOCKADDR_IN addrSrv;
	addrSrv.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");      // 本地回路地址是127.0.0.1;   
	addrSrv.sin_family = AF_INET;
	addrSrv.sin_port = htons(6119);
	if (SOCKET_ERROR == connect(g_sockClient, (SOCKADDR*)&addrSrv, sizeof(SOCKADDR)))
		return -1;
	WSAEVENT newEvent = WSACreateEvent();
	WSAEventSelect(g_sockClient, newEvent, FD_READ | FD_CLOSE);
	socketArray[dwTotal] = g_sockClient;
	eventArray[dwTotal] = newEvent;
	dwTotal++;
	DWORD Index;
	char *buf[2048];
	WSANETWORKEVENTS NetworkEvents;
	while (dwTotal > 0)
	{
		Index = WSAWaitForMultipleEvents(dwTotal, eventArray, FALSE, -1, FALSE);
		if (WSA_WAIT_TIMEOUT == Index)
		{
			continue;
		}
		WSAEnumNetworkEvents(socketArray[Index - WSA_WAIT_EVENT_0], eventArray[Index - WSA_WAIT_EVENT_0], &NetworkEvents);
		WSAResetEvent(eventArray[Index - WSA_WAIT_EVENT_0]);
		if (NetworkEvents.lNetworkEvents & FD_READ)
		{
			if (NetworkEvents.iErrorCode[FD_READ_BIT] != 0)
			{
				PRT("接受客户端数据失败。。。");
				continue;
			}
			memset(buf, 0, 2048);
			recv(socketArray[Index - WSA_WAIT_EVENT_0], (char *)buf, 2048, 0);
			printf("get read data:%s", buf);

		}
		if (NetworkEvents.lNetworkEvents & FD_CLOSE)
		{
			if (NetworkEvents.iErrorCode[FD_CLOSE_BIT] != 0)
			{
				continue;
			}
			PRT("系统消息: 客户端退出。。。");
			closesocket(socketArray[Index - WSA_WAIT_EVENT_0]);
			WSACloseEvent(eventArray[Index - WSA_WAIT_EVENT_0]);
			for (int i = Index - WSA_WAIT_EVENT_0; i < dwTotal; i++)
			{
				socketArray[i] = socketArray[i + 1];
				eventArray[i] = eventArray[i + 1];
			}
			dwTotal--;
		}
	}
	PRT("客户端监听结束");
	return 0;
	system("pause");
}