#include "stdafx.h"
#include <windows.h>
DWORD OldP;
DWORD tdata;
void *dAddress;
DWORD testdata;
struct VAddress{
	DWORD address;
	int count;
};

VAddress hookaddress[7] = { 0 };

LONG CALLBACK MyVectoredHandler(
	_In_  PEXCEPTION_POINTERS ExceptionInfo
	)
{
	
	if (EXCEPTION_ACCESS_VIOLATION == ExceptionInfo->ExceptionRecord->ExceptionCode){
		DWORD flag = ExceptionInfo->ExceptionRecord->ExceptionInformation[0];
		PVOID address = (PVOID)ExceptionInfo->ExceptionRecord->ExceptionInformation[1];
		for (int i = 0; i < 3; ++i){
			if (hookaddress[i].address == (DWORD)address){
				DWORD OldP;
				switch (hookaddress[i].count){
				case 0:{
						   VirtualProtect(address, 4, PAGE_READWRITE, &OldP);
						   *(DWORD *)address = ExceptionInfo->ContextRecord->Ecx;
						   VirtualProtect(address, 1, PAGE_READONLY, &OldP);
						   break;
				}
				case 1:{
						   VirtualProtect(address, 1, PAGE_READWRITE, &OldP);
						   *(byte *)address = (byte)ExceptionInfo->ContextRecord->Ebx;
						   VirtualProtect(address, 1, PAGE_READONLY, &OldP);
						   break;
				}
				case 2:{
						   VirtualProtect(address, 1, PAGE_READWRITE, &OldP);
						   byte ret =  *(byte *)address;
						   ret ^= 0x97;
						   *(byte *)address = ret;
						   VirtualProtect(address, 4, PAGE_READONLY, &OldP);
						   break;
				}
				}
				if (hookaddress[i].count == 2){
					hookaddress[i].count = 0;
				}
				else{
					hookaddress[i].count += 1;
				}
				return EXCEPTION_CONTINUE_EXECUTION;
			}
		}
	}
	return EXCEPTION_CONTINUE_SEARCH;
}

/************************************************************************
SetHardWareBP:
设置线程硬件断点
hThread:  线程句柄
dwAddr:    断点地址
dwDrIndex:  硬件寄存器(0~3)
nType:    断点类型(0:执行,1:读取,2:写入)
nLen:    读写断点数据长度(1,2,4)
/************************************************************************/

BOOL SetHardWareBP( DWORD dwAddr, DWORD dwDrIndex = 0, UINT nType = 2, UINT nLen = 1)
{
	BOOL bResult = FALSE;
	for (int i = 0; i < 3; ++i){
		if (hookaddress[i].address == dwAddr){
			return TRUE;
		}
		if (hookaddress[i].address == 0){
			hookaddress[i].address = dwAddr;
			hookaddress[i].count = 0;
			dwDrIndex = i;
			break;
		}
	}
	HANDLE hThread = GetCurrentThread();
	CONTEXT context = { 0 };
	context.ContextFlags = CONTEXT_DEBUG_REGISTERS;
	if (::GetThreadContext(hThread, &context))
	{
		DWORD dwDrFlags = context.Dr7;


		//将断点地址复制进入对应Dr寄存器(参考CONTEXT结构)
		memcpy(((BYTE *)&context) + 4 + dwDrIndex * 4, &dwAddr, 4);

		//决定使用哪个寄存器
		dwDrFlags |= (DWORD)0x1 << (2 * dwDrIndex);

		//见OD读写断点时 这个置位了,执行没有(置位也正常-_-)
		dwDrFlags |= 0x100;


		//先将对应寄存器对应4个控制位清零(先或,再异或,还有其它好方法吗) =.= 悲催的小学生
		dwDrFlags |= (DWORD)0xF << (16 + 4 * dwDrIndex);
		dwDrFlags ^= (DWORD)0xF << (16 + 4 * dwDrIndex);


		//设置断点类型,执行:00 读取:11 写入:01
		//(不知何故,测试时发现不论是11还是01,读写数据时均会断下来)
		if (nType == 1)
			dwDrFlags |= (DWORD)0x3 << (16 + 4 * dwDrIndex);  //读取
		else if (nType == 2)
			dwDrFlags |= (DWORD)0x1 << (16 + 4 * dwDrIndex);  //写入
		//else if(nType==0) 
		//dwDrFlags=dwDrFlags            //执行


		//设置读写断点时数据长度
		if (nType != 0)
		{
			if (nLen == 2 && dwAddr % 2 == 0)
				dwDrFlags |= (DWORD)0x1 << (18 + 4 * dwDrIndex);  //2字节
			else if (nLen == 4 && dwAddr % 4 == 0)
				dwDrFlags |= (DWORD)0x3 << (18 + 4 * dwDrIndex);  //4字节
		}

		context.Dr7 = dwDrFlags;
		if (::SetThreadContext(hThread, &context)){
			bResult = TRUE;
		}	
	}
	return bResult;
}

//异常处理
//直接从工程中拷出来的
typedef ULONG(WINAPI *pfnRtlDispatchException)(PEXCEPTION_RECORD pExcptRec, CONTEXT * pContext);
static pfnRtlDispatchException m_fnRtlDispatchException = NULL;

//异常处理函数
BOOL RtlDispatchException(PEXCEPTION_RECORD ExceptionRecord, CONTEXT * ContextRecord)
{
	if (EXCEPTION_SINGLE_STEP == ExceptionRecord->ExceptionCode){
		DWORD flag = ExceptionRecord->ExceptionInformation[0];
		DWORD address = ExceptionRecord->ExceptionInformation[1];
		for (int i = 0; i < 3; ++i){
			if (hookaddress[i].address == address){
				DWORD OldP;
				switch (hookaddress[i].count){
				case 0:{
						   *(DWORD *)address = ContextRecord->Ecx;
						   break;
				}
				case 1:{
						   *(byte *)address = (byte)ContextRecord->Ebx;
						   break;
				}
				case 2:{
						   byte ret = *(byte *)address;
						   ret ^= 0x97;
						   *(byte *)address = ret;
						   break;
				}
				}
				if (hookaddress[i].count == 2){
					hookaddress[i].count = 0;
				}
				else{
					hookaddress[i].count += 1;
				}
				return TRUE;
			}
		}
	}
	return FALSE;
}

ULONG WINAPI _RtlDispatchException(PEXCEPTION_RECORD pExcptRec, CONTEXT * pContext)
{
	printf("_RtlDispatchException:%x %x\n", pExcptRec->ExceptionCode, pExcptRec->ExceptionAddress);
	if (RtlDispatchException(pExcptRec, pContext)) return 1;
	return m_fnRtlDispatchException(pExcptRec, pContext);
}

//Hook程序异常处理,当程序发生异常时,由ring0转回ring3时调用的第一个函数:KiUserExceptionDispatcher
BOOL HookSystemSEH()
{
	BOOL bResult = FALSE;
	BYTE *pAddr = (BYTE *)::GetProcAddress(::GetModuleHandleA("ntdll"), "KiUserExceptionDispatcher");
	if (pAddr)
	{
		while (*pAddr != 0xE8)pAddr++;  //XP~Win7正常,Win8尚无缘得见
		m_fnRtlDispatchException = (pfnRtlDispatchException)((*(DWORD *)(pAddr + 1)) + 5 + (DWORD)pAddr);  //得到原函数地址
		DWORD dwNewAddr = (DWORD)(_RtlDispatchException) - (DWORD)pAddr - 5;                //计算新地址
		DWORD oldpro;
		VirtualProtect((PVOID)((DWORD)pAddr + 1), 4,PAGE_READWRITE,&oldpro);
		memcpy((void*)((DWORD)pAddr + 1), (BYTE *)&dwNewAddr, 4);
		VirtualProtect((PVOID)((DWORD)pAddr + 1), 4, oldpro, &oldpro);
		bResult = TRUE;
	}
	return bResult;
}
#include <time.h>
char fname1[21] = { 0 };
char fname2[21] = { 0 };

#include <process.h>
#include "../msktools/msktools.h"
#include "../mhook-lib/mhook.h"
#define HOOKAPIPRE(fun) auto true##fun = fun
#define HOOKAPI(fun) Mhook_SetHook((LPVOID *)& true##fun, my##fun)

HOOKAPIPRE(_beginthreadex);
HOOKAPIPRE(CreateThread);

struct _tiddata {
	unsigned long   _tid;       /* thread ID */


	uintptr_t _thandle;         /* thread handle */

	int     _terrno;            /* errno value */
	unsigned long   _tdoserrno; /* _doserrno value */
	unsigned int    _fpds;      /* Floating Point data segment */
	unsigned long   _holdrand;  /* rand() seed value */
	char *      _token;         /* ptr to strtok() token */
	wchar_t *   _wtoken;        /* ptr to wcstok() token */
	unsigned char * _mtoken;    /* ptr to _mbstok() token */

	/* following pointers get malloc'd at runtime */
	char *      _errmsg;        /* ptr to strerror()/_strerror() buff */
	wchar_t *   _werrmsg;       /* ptr to _wcserror()/__wcserror() buff */
	char *      _namebuf0;      /* ptr to tmpnam() buffer */
	wchar_t *   _wnamebuf0;     /* ptr to _wtmpnam() buffer */
	char *      _namebuf1;      /* ptr to tmpfile() buffer */
	wchar_t *   _wnamebuf1;     /* ptr to _wtmpfile() buffer */
	char *      _asctimebuf;    /* ptr to asctime() buffer */
	wchar_t *   _wasctimebuf;   /* ptr to _wasctime() buffer */
	void *      _gmtimebuf;     /* ptr to gmtime() structure */
	char *      _cvtbuf;        /* ptr to ecvt()/fcvt buffer */
	unsigned char _con_ch_buf[MB_LEN_MAX];
	/* ptr to putch() buffer */
	unsigned short _ch_buf_used;   /* if the _con_ch_buf is used */

	/* following fields are needed by _beginthread code */
	void *      _initaddr;      /* initial user thread address */
	void *      _initarg;       /* initial user thread argument */
};

typedef struct _tiddata * _ptiddata;

uintptr_t __cdecl my_beginthreadex(_In_opt_ void * _Security, _In_ unsigned _StackSize,
	_In_ unsigned(__stdcall * _StartAddress) (void *), _In_opt_ void * _ArgList,
	_In_ unsigned _InitFlag, _Out_opt_ unsigned * _ThrdAddr)
{
	PRT("my_beginthreadex:%x", (DWORD)_StartAddress);
	return true_beginthreadex(_Security, _StackSize, _StartAddress, _ArgList, _InitFlag, _ThrdAddr);
}

HANDLE
WINAPI
myCreateThread(
_In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
_In_ SIZE_T dwStackSize,
_In_ LPTHREAD_START_ROUTINE lpStartAddress,
_In_opt_ __drv_aliasesMem LPVOID lpParameter,
_In_ DWORD dwCreationFlags,
_Out_opt_ LPDWORD lpThreadId
)
{
	_ptiddata ptd = (_ptiddata)lpParameter;
	DWORD trueAddress = *(DWORD *)ptd->_initaddr;
	PRT("myCreateThread:%x,%d,%x,%x,%d,%x", lpThreadAttributes, dwStackSize, (DWORD)lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);
	return trueCreateThread(lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);
}

void HookThreadMsg()
{
	//HOOKAPI(_beginthreadex);
	HOOKAPI(CreateThread);
}

DWORD WINAPI EmptyThread(LPVOID lpThreadParameter)
{
	return 0;
}

unsigned WINAPI BlockThread(LPVOID lpThreadParameter)
{
	while (true)
	{
		Sleep(30000);
	}
	return 0;
}

__declspec(naked) void SkipError()
{

}
//
//// VEH回调函数
//LONG WINAPI VectoredHandler(
//	PEXCEPTION_POINTERS ExceptionInfo
//	)
//{
//
//
//	//判断是否为我们设置的异常地址
//	if (ExceptionInfo->ExceptionRecord->ExceptionAddress == (PVOID)0X00401489)
//	{
//		MessageBox(NULL, "成功触发异常", "(^_^)", MB_ICONINFORMATION);
//
//		// 修改当前异常中断的EIP
//		ExceptionInfo->ContextRecord->Eip = 0x00401721;//指向要执行代码的内存地址
//
//		// 处理异常.
//		return EXCEPTION_CONTINUE_EXECUTION;
//	}
//
//	return EXCEPTION_CONTINUE_SEARCH;
//}
//
////设置硬件断点函数
//void SetHwBreakpoint()
//{
//
//
//	//添加VEH  参数1=1表示插入Veh链的头部，=0表示插入到VEH链的尾部
//	AddVectoredExceptionHandler(1, VectoredHandler);
//
//	//寄存器相关的结构体 
//	CONTEXT _ConText = { CONTEXT_DEBUG_REGISTERS };
//
//	//得到当前线程
//	HANDLE hThread = GetCurrentThread();
//
//
//	// 得到指定线程的环境(上下文)
//	GetThreadContext(hThread, &_ConText);
//
//
//	//给调试寄存器值
//	if (0X0 == _ConText.Dr0 && 0X0 == _ConText.Dr1 && 0X0 == _ConText.Dr2)
//	{
//		_ConText.Dr0 = 0X00401489;
//		_ConText.Dr1 = 0X0;
//		_ConText.Dr2 = 0X0;
//		_ConText.Dr3 = 0x0;
//
//		_ConText.Dr7 = 0x405;
//
//
//		// 设置线程的环境(上下文)
//		SetThreadContext(hThread, &_ConText);
//	}
//
//
//	if (0X0 != _ConText.Dr0 || 0X0 != _ConText.Dr1 || 0X0 != _ConText.Dr2 || 0X0 != _ConText.Dr3)
//	{
//		MessageBox(NULL, "设置硬件断点成功..", "(^_^)", MB_OK);
//	}
//	else
//	{
//		MessageBox(NULL, "设置失败,", "提示", MB_OK);
//	}
//
//}
//
//ALCDECL MemCode_connect(void)
//{
//
//	SetHwBreakpoint();//调用设置硬件断点
//
//	GetAddress("connect");
//	__asm JMP EAX;
//}
#include "../lua514/lua.hpp"
int _tmain1(int argc, _TCHAR* argv[])
{
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	luaL_dostring(L, "aa = \"40000000696\"");
	lua_getglobal(L, "aa");
	LONGLONG laa = lua_tonumber(L, -1);
	system("pause");
	return 0;
}