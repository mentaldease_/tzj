// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"
#include "../function/function.h"
#include "../nomodule/LoadPE.h"
#include <DbgHelp.h>
#include <time.h>
#include "../mhook-lib/mhook.h"
#include <atlconv.h>


void MyOutDstring(const char *msg)
{
	char ostr[MAX_PATH] = { 0 };
	wsprintfA(ostr, "msk_tg:%s", msg);
	OutputDebugStringA(ostr);
}

BOOL IsMutexExist(char* pstrMutex)
{
	BOOL bRet = FALSE;
	HANDLE hMutex = NULL; 

	hMutex = CreateMutexA(NULL, TRUE, pstrMutex);
	if ( hMutex )
	{
		if ( GetLastError() == ERROR_ALREADY_EXISTS )
			bRet = TRUE;
		ReleaseMutex(hMutex);
		CloseHandle(hMutex);
	}
	else
	{
		bRet = TRUE;
	}
	return bRet;

}


void LaunchNoModule()
{
	LaunchDll(dllModuleName,NO_MODULE_MARK);
}
unsigned int  __stdcall NoModuleThread(void* lpParameter)
{
	while (TRUE)
	{
		Sleep(5000);
		MyOutDstring("No Module Thread");
	}

	return TRUE;
}

void NoModuleEntryCall(HMODULE hModule, DWORD ul_reason_for_call, char* pstrModuleName)
{
	char szMutexName[MAX_PATH];
	wsprintfA(szMutexName,"mskmkt%d",GetCurrentProcessId());
	g_hMutex = CreateMutexA(NULL, TRUE, szMutexName);

	char szLog[MAX_PATH] = {0};
	wsprintfA(szLog,"NoModuleEntryCall Module Start:%p",hModule);
	MyOutDstring(szLog);
	//下面为正常Dll功能代码
	
	CloseHandle((HANDLE)_beginthreadex(0, 0, GameHackStart, NULL, 0, 0));
	Sleep(1000);//等待HOOK完成
	//CreateThread(NULL,NULL,(LPTHREAD_START_ROUTINE)NoModuleThread,NULL,NULL,NULL);


}

BOOL ChooseSub(HMODULE hModule, DWORD ul_reason_for_call, char* pstrModuleName)
{
	BOOL bRet = FALSE;
	GetModuleFileNameA(NULL, exeModuleName, MAX_PATH);
	if ( ul_reason_for_call == NO_MODULE_MARK )
		strcpy(dllModuleName, pstrModuleName);
	else
		GetModuleFileNameA(hModule, dllModuleName, MAX_PATH);

	if ( ul_reason_for_call == NO_MODULE_MARK )
	{
		MyOutDstring("run NO_MODULE_MARK");
		NoModuleEntryCall(hModule, DLL_PROCESS_ATTACH, 0);
		bRet = TRUE;
	}
	else
	{
		LaunchNoModule();
		bRet = FALSE;
	}
	return bRet;


}

LONG WINAPI MyUnhandledExceptionFilter(
	__in  struct _EXCEPTION_POINTERS* ExceptionInfo
	)
{
	HANDLE hFile = CreateFileA("msk.dmp",                // name of the write
		GENERIC_WRITE,          // open for writing
		0,                      // do not share
		NULL,                   // default security
		CREATE_ALWAYS,          // overwrite existing
		FILE_ATTRIBUTE_NORMAL,  // normal file
		NULL);                  // no attr. template


	MINIDUMP_EXCEPTION_INFORMATION expInfo;
	expInfo.ThreadId = ::GetCurrentThreadId();
	expInfo.ExceptionPointers = ExceptionInfo;
	expInfo.ClientPointers = TRUE;
	MiniDumpWriteDump(::GetCurrentProcess(), ::GetCurrentProcessId(), hFile, MiniDumpNormal
		, &expInfo, NULL, NULL);
	CloseHandle(hFile);
	MessageBoxA(NULL, "dump", "game has crash", MB_OK);
	//AfxMessageBox("dddd");
	return EXCEPTION_CONTINUE_SEARCH;

}

typedef HANDLE(WINAPI * _CreateThread)(
	_In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
	_In_ SIZE_T dwStackSize,
	_In_ LPTHREAD_START_ROUTINE lpStartAddress,
	_In_opt_ __drv_aliasesMem LPVOID lpParameter,
	_In_ DWORD dwCreationFlags,
	_Out_opt_ LPDWORD lpThreadId
	);
int trueCreateThreadAddress;
DWORD eaxTmp;
DWORD ebxTmp;
DWORD skipAddress = 0x005A7CC0;
DWORD tarAddress = 0x005A7D4F;

__declspec(naked) void FilterMultoOpen()
{
	__asm{
		mov eaxTmp, eax
			mov ebxTmp, ebx
			mov eax, [esp + 0xc]
			mov ebx, skipAddress
			cmp eax, ebx
			mov eax, eaxTmp
			mov ebx, ebxTmp
			je skiptheread
			mov edi, edi
			push ebp
			mov ebp, esp
			jmp trueCreateThreadAddress
	}
skiptheread:
	__asm{
		add esp, 0x18
		jmp tarAddress
	}
}

typedef
HANDLE
(WINAPI *
_CreateFileMappingA)(
_In_     HANDLE hFile,
_In_opt_ LPSECURITY_ATTRIBUTES lpFileMappingAttributes,
_In_     DWORD flProtect,
_In_     DWORD dwMaximumSizeHigh,
_In_     DWORD dwMaximumSizeLow,
_In_opt_ LPCSTR lpName
);
_CreateFileMappingA g_trueCreateFileMappingA = (_CreateFileMappingA)GetProcAddress(GetModuleHandleA("Kernel32.dll"), "CreateFileMappingA");

char fname1[21] = { 0 };
char fname2[21] = { 0 };
HANDLE
WINAPI
MskH_CreateFileMappingA(
_In_     HANDLE hFile,
_In_opt_ LPSECURITY_ATTRIBUTES lpFileMappingAttributes,
_In_     DWORD flProtect,
_In_     DWORD dwMaximumSizeHigh,
_In_     DWORD dwMaximumSizeLow,
_In_opt_ LPCSTR lpName
)
{
	if (lpName && strcmp("12359", lpName) == 0){
		PRT("skip mapfile:%s", lpName);
		return g_trueCreateFileMappingA(hFile, lpFileMappingAttributes, flProtect, dwMaximumSizeHigh, dwMaximumSizeLow, fname1);
	}
	if (lpName && strcmp("24611", lpName) == 0){
		PRT("skip mapfile:%s", lpName);
		HANDLE ret = g_trueCreateFileMappingA(hFile, lpFileMappingAttributes, flProtect, dwMaximumSizeHigh, dwMaximumSizeLow, fname2);
		Mhook_Unhook((LPVOID*)&g_trueCreateFileMappingA);
		return ret;
	}
	return g_trueCreateFileMappingA(hFile, lpFileMappingAttributes, flProtect, dwMaximumSizeHigh, dwMaximumSizeLow, lpName);
}

DWORD WINAPI EmptyThread(LPVOID lpThreadParameter)
{
	return 0;
}

DWORD WINAPI BlockThread(LPVOID lpThreadParameter)
{
	while (true)
	{
		Sleep(30000);
	}
	return 0;
}


HOOKAPIPRE(_beginthreadex);
HOOKAPIPRE(CreateThread);

uintptr_t __cdecl my_beginthreadex(_In_opt_ void * _Security, _In_ unsigned _StackSize,
	_In_ unsigned(__stdcall * _StartAddress) (void *), _In_opt_ void * _ArgList,
	_In_ unsigned _InitFlag, _Out_opt_ unsigned * _ThrdAddr)
{
	PRT("my_beginthreadex:%x", (DWORD)_StartAddress);
	return true_beginthreadex(_Security, _StackSize, _StartAddress, _ArgList, _InitFlag, _ThrdAddr);
}


struct _tiddata {
	unsigned long   _tid;       /* thread ID */


	uintptr_t _thandle;         /* thread handle */

	int     _terrno;            /* errno value */
	unsigned long   _tdoserrno; /* _doserrno value */
	unsigned int    _fpds;      /* Floating Point data segment */
	unsigned long   _holdrand;  /* rand() seed value */
	char *      _token;         /* ptr to strtok() token */
	wchar_t *   _wtoken;        /* ptr to wcstok() token */
	unsigned char * _mtoken;    /* ptr to _mbstok() token */

	/* following pointers get malloc'd at runtime */
	char *      _errmsg;        /* ptr to strerror()/_strerror() buff */
	wchar_t *   _werrmsg;       /* ptr to _wcserror()/__wcserror() buff */
	char *      _namebuf0;      /* ptr to tmpnam() buffer */
	wchar_t *   _wnamebuf0;     /* ptr to _wtmpnam() buffer */
	char *      _namebuf1;      /* ptr to tmpfile() buffer */
	wchar_t *   _wnamebuf1;     /* ptr to _wtmpfile() buffer */
	char *      _asctimebuf;    /* ptr to asctime() buffer */
	wchar_t *   _wasctimebuf;   /* ptr to _wasctime() buffer */
	void *      _gmtimebuf;     /* ptr to gmtime() structure */
	char *      _cvtbuf;        /* ptr to ecvt()/fcvt buffer */
	unsigned char _con_ch_buf[MB_LEN_MAX];
	/* ptr to putch() buffer */
	unsigned short _ch_buf_used;   /* if the _con_ch_buf is used */

	/* following fields are needed by _beginthread code */
	void *      _initaddr;      /* initial user thread address */
	void *      _initarg;       /* initial user thread argument */
};

typedef struct _tiddata * _ptiddata;


HANDLE
WINAPI
myCreateThread(
_In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
_In_ SIZE_T dwStackSize,
_In_ LPTHREAD_START_ROUTINE lpStartAddress,
_In_opt_ __drv_aliasesMem LPVOID lpParameter,
_In_ DWORD dwCreationFlags,
_Out_opt_ LPDWORD lpThreadId
)
{
	DWORD threadId;
	if (NULL == lpThreadId){
		lpThreadId = &threadId;
	}
	HANDLE ret = trueCreateThread(lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);
	if (NULL == ret) return ret;
	if ((DWORD)lpStartAddress == 0x70a0345e){
		_ptiddata ptd = (_ptiddata)lpParameter;
		if (ptd == NULL){
			DWORD err = GetLastError();
			PRT("my_beginthreadex tid:%x err:%d", *(DWORD*)lpThreadId, err);
		}
		else{
			PRT("my_beginthreadex tid:%x taddr:%x faddr:%x", *(DWORD*)lpThreadId,(DWORD)ptd->_initaddr, (DWORD)lpStartAddress);
		}

	}
	else{
		PRT("myCreateThread tid:%x faddr:%x", *(DWORD*)lpThreadId, (DWORD)lpStartAddress);
	}
	return ret;
}

void HookThreadMsg()
{
	//HOOKAPI(_beginthreadex);
	HOOKAPI(CreateThread);
}

void printt(const char *msk)
{
	string msg = "msk_tg:";
	msg += msk;
	OutputDebugStringA(msg.c_str());
}

DllExport void BeginGameHack(int port)
{
	g_iSocketPort = port;
	//return HookThreadMsg();
	//OutputDebugStringA("msk_dll:dll_template call !!");
#if 1
	LPTOP_LEVEL_EXCEPTION_FILTER pOdk = SetUnhandledExceptionFilter(MyUnhandledExceptionFilter);
	HANDLE hOpenFile = MyCreateFileW(L"C:\\mskd", GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hOpenFile) {
		PRT("mskd cant find");
		return;
	}
	char appPath[MAX_PATH] = { 0 };
	DWORD RSize;
	ReadFile(hOpenFile, appPath, MAX_PATH, &RSize, NULL);
	CloseHandle(hOpenFile);
	char mskPath[MAX_PATH] = { 0 };
	strcpy(mskPath, appPath);
	for (auto i = 0; i < strlen(mskPath); i++){
		if (mskPath[i] == ','){
			mskPath[i] = 0;
			break;
		}
	}

	strcat(mskPath, "mskdata");
	hOpenFile = MyCreateFileW(L"C:\\mskd:cldata", GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
//#ifndef _VER_RELEASE
//	hOpenFile = MyCreateFileW(L"C:\\mskd:cldata", GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
//#else
//	USES_CONVERSION;
//	strcat(appPath, "mskdata:cldata");
//	hOpenFile = MyCreateFileW(A2W(appPath), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
//#endif
	if (INVALID_HANDLE_VALUE == hOpenFile) {
		PRT("cldata cant find");
		return;
	}
	DWORD fileSize = 0;
	DWORD dwRead;
	fileSize = GetFileSize(hOpenFile, NULL);
	PRT("cldata size:%d", fileSize);
	char *pBuff = new char[fileSize+1];
	ReadFile(hOpenFile, pBuff, fileSize, &RSize, NULL);
	pBuff[fileSize] = 0;
	CloseHandle(hOpenFile);
	//MessageBoxA(NULL, "", "脚本信息", MB_OK);
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	luaopen_des56(L);
	lua_setglobal(L, "des56");
	luaopen_pack(L);
	lua_pushlstring(L, pBuff, fileSize);
	lua_setglobal(L, "gvClientData");
	lua_tinker::def(L, "print", printt);
	delete[]pBuff;
	using namespace lua_tinker;
	auto luaCode = R"(
		gtClientData = loadstring ("return " .. gvClientData)()
		print("dymac:"..gtClientData.dymac)
		gtClientData.sig = des56.decrypt(gtClientData.sig,gtClientData.dymac)
		gvSigData = gtClientData.sig
		_,gvMoSkip,gvMoTar = string.unpack(gtClientData.sig,'>II')
		--gvMoSkip,gvMoTar = 0,0)";
	if (0 != luaL_dostring(L, luaCode)){
		MessageBoxA(NULL, lua_tostring(L, -1), "脚本失败", MB_OK);
	}

	//if ((luaL_loadbuffer(L, luaCode, strlen(luaCode), "tmpcode") || lua_pcall(L, 0, LUA_MULTRET, 0)) != 0){
	//	MessageBoxA(NULL, lua_tostring(L, -1), "脚本失败", MB_OK);
	//}
	auto ctData = get<table>(L, "gtClientData");
	skipAddress = get<DWORD>(L, "gvMoSkip");
	tarAddress = get<DWORD>(L, "gvMoTar");
	PRT("skipAddress:%x", skipAddress);
	PRT("tarAddress:%x", tarAddress);
	InitializeCriticalSection(&g_csSafeCall);
	InitializeCriticalSection(&g_csGlobal);
	//MessageBoxA(NULL, "", "", 0);
	//auto sigdata = ctData.get<const char *>("sig");//这里是NULL
	//PRT("sigdata = %s", sigdata);
	
	size_t sz;
	lua_getglobal(L, "gvSigData");
	auto lSig =  lua_tolstring(L, -1, &sz);
	string sigdata(lSig,sz);
	setglobal("sigdata", sigdata);
	auto apath = ctData.get<const char *>("appPath");
	PRT("apppath = %s", apath);
	setglobal("apppath", apath);
	lua_close(L);
	setglobal("mskdata", mskPath);
#ifdef _VER_RELEASE
#if 1//多开及 防止检测
	time_t now; // 变量声明 
	time(&now); // 取得现在的日期时间 
	sprintf_s(fname1, 20, "%d", (int)now);
	now = (int)now + 1;
	sprintf_s(fname2, 20, "%d", (int)now);
	trueCreateThreadAddress = (int)CreateThread;
	inline_hook(trueCreateThreadAddress, (int)FilterMultoOpen, 5);
	trueCreateThreadAddress = trueCreateThreadAddress + 5;
	Mhook_SetHook((LPVOID *)&g_trueCreateFileMappingA, MskH_CreateFileMappingA);
#endif
#endif

#if 0
	mskL_setProtect();
#endif

#else
	setglobal("mskdata", R"(G:\bitbuket\tzj\ver_release_tg\mskdata)");
	time_t now; // 变量声明 
	time(&now); // 取得现在的日期时间 
	sprintf_s(fname1, 20, "%d", (int)now);
	now = (int)now + 1;
	sprintf_s(fname2, 20, "%d", (int)now);
#endif
	GameHackStart(NULL);
	//CloseHandle((HANDLE)_beginthreadex(0, 0, GameHackStart, NULL, 0, 0));

}

BOOL APIENTRY DllMain( HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
	)
{

	return TRUE;
	//*
	if (ul_reason_for_call == DLL_PROCESS_ATTACH){
		//BeginGameHack();
		//Sleep(1000);
	}
	return TRUE;
	//*/
	//MessageBoxA(0, "s", "x", MB_OK);
	BOOL  bRet = FALSE;//测试
	if ( ul_reason_for_call == DLL_PROCESS_ATTACH || ul_reason_for_call == NO_MODULE_MARK )
	{
		DisableThreadLibraryCalls(hModule);
		//return TRUE;//测试
		char szMutexName[MAX_PATH];
		wsprintfA(szMutexName,"mskmkt%d",GetCurrentProcessId());
		if ( IsMutexExist(szMutexName))
			return FALSE;

		bRet = ChooseSub(hModule, ul_reason_for_call, (char *)lpReserved);
	}
	else
	{
		//return TRUE;//测试
		if ( ul_reason_for_call ==  DLL_PROCESS_DETACH)
		{
			ReleaseMutex(g_hMutex);
			CloseHandle(g_hMutex);
			bRet = TRUE;
		}
	}
	return bRet;	
}
