
// ServerTg.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "ServerTg.h"
#include "ServerTgDlg.h"
#include "ListenSocket.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CServerTgApp

BEGIN_MESSAGE_MAP(CServerTgApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CServerTgApp 构造

CServerTgApp::CServerTgApp()
{
	// 支持重新启动管理器
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO:  在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CServerTgApp 对象

CServerTgApp theApp;


// CServerTgApp 初始化
#include <fstream>
#include <sstream>
void show_error(const char* error)
{
	using namespace std;
	stringstream ss;
	time_t t = time(0);
	char tmp[64];
	strftime(tmp, sizeof(tmp), "%Y/%m/%d %H:%M:%S", localtime(&t));
	ofstream ofs("Log.txt", ios_base::out|ios_base::app);
	ofs << tmp;
	ofs << " _ALERT ->";
	ofs.write(error, strlen(error));
	ofs << endl;
#ifdef _DEBUG
	AfxMessageBox(CString(error));
#endif
	ofs.close();
}

int NetSend(lua_State *L)
{
	SOCKET s = lua_tointeger(L, 1);
	size_t len;
	char* buffer = (char*)lua_tolstring(L, 2, &len);
	char loadbuff[4096];
	char *newbuf = loadbuff;
	if (len > 4095){
		newbuf = new char[len];
	}
	memcpy(newbuf, buffer, len);
	if (false == lua_isnil(L, 3)){
		byte key = lua_tonumber(L, 3);
		for (int i = 0; i < len; ++i){
			newbuf[i] ^= key;
		}
		for (int i = 0; i < len; ++i){
			newbuf[i] ^= 'm';
		}
	}
	WSABUF DataBuf;
	DWORD SendBytes;
	int err = 0;
	DataBuf.buf = newbuf;
	DataBuf.len = len;
	int rc = WSASend(s, &DataBuf, 1,
		&SendBytes, 0, NULL, NULL);
	if (rc == SOCKET_ERROR){
		err = WSAGetLastError();
	}
	if (WSA_IO_PENDING == err)
		err = 0;
	if (len > 4095){
		delete newbuf;
	}
	return 0;
}

int Decrypt(lua_State *L)
{
	char buff[4096];
	char *newBuff = buff;
	size_t len;
	const char *data = luaL_checklstring(L, 1,&len);
	if (len > 4095){
		newBuff = new char[len];
	}
	byte bKey = lua_tonumber(L, 2);
	memcpy(newBuff, data, len);
	for (int i = 0; i < len; ++i){
		newBuff[i] = newBuff[i] ^ 'm';
	}
	for (int i = 0; i < len; ++i){
		newBuff[i] = newBuff[i] ^ bKey;
	}
	lua_pushlstring(L, newBuff, len);
	if (len > 4095){
		delete []newBuff;
	}
	return 1;
}

void CloseSocket(int s)
{
	closesocket((SOCKET)s);
}
#include <string>
int MyPrint(lua_State *L)
{
	std::string strPrint;
	for (int i = 1; i <= lua_gettop(L); ++i){
		strPrint += lua_tostring(L, i);
		strPrint += " ";
	}
	strPrint += "\n";
	PRT(strPrint.c_str());
	printf(strPrint.c_str());
	return 0;
}

BOOL CServerTgApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。  否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}


	AfxEnableControlContainer();

	// 创建 shell 管理器，以防对话框包含
	// 任何 shell 树视图控件或 shell 列表视图控件。
	CShellManager *pShellManager = new CShellManager;

	// 激活“Windows Native”视觉管理器，以便在 MFC 控件中启用主题
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO:  应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("ServerTg"));
	m_l = luaL_newstate();
	luaL_openlibs(m_l);
#ifdef _DEBUG
	SetCurrentDirectoryA("../script_server_tg");
	lua_pushboolean(m_l, 1);
#else
	lua_pushboolean(m_l, 0);
#endif
	lua_setglobal(m_l, "_DEBUG");
	lua_register(m_l, "Decrypt", Decrypt);
	lua_register(m_l, "print", MyPrint);
	lua_register(m_l, "NetSend", NetSend);
	lt::def(m_l, "CloseSocket", CloseSocket);
	lua_tinker::def(m_l, "_ALERT", show_error);
	if (luaL_dofile(m_l, "Start.lua") != 0){
		CString strErr(_T("lua error:"));
		AfxMessageBox(strErr + lua_tostring(m_l, -1));
		return FALSE;
	}
	lt::call<void>(m_l, "_UI_Event", "evInit");
	CServerTgDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO:  在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO:  在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "警告: 对话框创建失败，应用程序将意外终止。\n");
		TRACE(traceAppMsg, 0, "警告: 如果您在对话框上使用 MFC 控件，则无法 #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS。\n");
	}

	// 删除上面创建的 shell 管理器。
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}



int CServerTgApp::ExitInstance()
{
	// TODO:  在此添加专用代码和/或调用基类
	lua_close(m_l);
	return CWinApp::ExitInstance();
}
