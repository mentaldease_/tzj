
// ServerTgDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ServerTg.h"
#include "ServerTgDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define  WM_SOCKET WM_USER+1

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CServerTgDlg 对话框



CServerTgDlg::CServerTgDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CServerTgDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CServerTgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CServerTgDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_SOCKET, &CServerTgDlg::OnSocket)
	ON_BN_CLICKED(IDC_BUTTON_TEST, &CServerTgDlg::OnBnClickedButtonTest)
END_MESSAGE_MAP()

// CServerTgDlg 消息处理程序

BOOL CServerTgDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO:  在此添加额外的初始化代码
	SOCKADDR_IN   local;
	m_sListen = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	// Bind  
	local.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	local.sin_family = AF_INET;
	local.sin_port = htons(6220);
	if (0 != bind(m_sListen, (struct sockaddr *)&local, sizeof(local))){
		AfxMessageBox(_T("bind error"));
		exit(0);
		return FALSE;
	}

	// Listen  
	listen(m_sListen, 3);
	// Associate listening socket with FD_ACCEPT event  
	WSAAsyncSelect(m_sListen, this->GetSafeHwnd(), WM_SOCKET, FD_ACCEPT);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CServerTgDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CServerTgDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CServerTgDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


#define MSGSIZE   2048  
afx_msg LRESULT CServerTgDlg::OnSocket(WPARAM wParam, LPARAM lParam)
{
	SOCKET        sClient;
	SOCKADDR_IN    client;
	int           ret, iAddrSize = sizeof(client);
	char          szMessage[MSGSIZE] = { 0 };
	SOCKET		s = wParam;
	if (WSAGETSELECTERROR(lParam))
	{
		closesocket(wParam);
	}
	else{
		if (WSAGETSELECTERROR(lParam))
		{
			closesocket(s);
			return 0;
		}

		switch (WSAGETSELECTEVENT(lParam)){
		case FD_ACCEPT:{
						   // Accept a connection from client
						   sClient = accept(s, (struct sockaddr *)&client, &iAddrSize);
						   // Associate client socket with FD_READ and FD_CLOSE event
						   WSAAsyncSelect(sClient, this->GetSafeHwnd(), WM_SOCKET, FD_READ | FD_CLOSE);
						   // 取得ip和端口号
						   lua_tinker::call<void>(theApp.m_l, "_Net_Accept", sClient, inet_ntoa(client.sin_addr), client.sin_port);
						   break;
		}
		case FD_READ:{
						 ret = recv(s, szMessage, MSGSIZE, 0);
						 if (ret == 0 || ret == SOCKET_ERROR && WSAGetLastError() == WSAECONNRESET)
						 {
							 PRT("error recv data failed");
							 //closesocket(s);
							 return 0;
						 }
						 lua_State *&L = theApp.m_l;
						 lua_getglobal(theApp.m_l, "_Net_Read");
						 lua_pushnumber(theApp.m_l, s);
						 lua_pushlstring(theApp.m_l, szMessage, ret);
						 lua_pushnumber(theApp.m_l, ret);
						 if (lua_pcall(L, 3, 0, 0) != 0)
						 {
							 lt::print_error(L, lua_tostring(L, -1));
							 lua_pop(L, 1);
							 lua_tinker::call<void>(theApp.m_l, "_Net_Close", s);
							 closesocket(s);
						 }

						 break;
		}
		case FD_CLOSE:{
						  lua_tinker::call<void>(theApp.m_l, "_Net_Close", s);
						  closesocket(s);
						  break;
		}
		}
		return 0;
	}
	return 0;
}


void CServerTgDlg::OnBnClickedButtonTest()
{
	// TODO:  在此添加控件通知处理程序代码
	lt::dofile(theApp.m_l, "Test.lua");
}
