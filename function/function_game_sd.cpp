#include "stdafx.h"
#include "function.h"

static const luaL_Reg mskgfuncs[] =
{
	{ NULL, NULL }
};

void RegisterGameApi(lua_State *L,bool bGame)
{
	if (bGame)
		g_luaL_register(L, "mskg", mskgfuncs);
	else
		luaL_register(L, "mskg", mskgfuncs);
}