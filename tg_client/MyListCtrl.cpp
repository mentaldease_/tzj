// MyListCtrl.cpp : 实现文件
//

#include "stdafx.h"
#include "MyListCtrl.h"
#include <afxconv.h>

// CMyListCtrl

IMPLEMENT_DYNAMIC(CMyListCtrl, CListCtrl)

CMyListCtrl::CMyListCtrl()
{

}

CMyListCtrl::~CMyListCtrl()
{
}


BEGIN_MESSAGE_MAP(CMyListCtrl, CListCtrl)
END_MESSAGE_MAP()



// CMyListCtrl 消息处理程序
int CMyListCtrl::MyInsertItem(int nItem,char *str)
{
	USES_CONVERSION;
	return CListCtrl::InsertItem(nItem, A2W(str));
}

int CMyListCtrl::MyInsertColumn(int nCol, char* lpszColumnHeading, int nWidth = -1)
{
	USES_CONVERSION;
	return CListCtrl::InsertColumn(nCol, A2W(lpszColumnHeading), LVCFMT_LEFT, nWidth, -1);
}

BOOL CMyListCtrl::MySetItemText(int nItem, int nSubItem, char* lpszText)
{
	USES_CONVERSION;
	return CListCtrl::SetItemText(nItem, nSubItem, A2W(lpszText));
}

int CMyListCtrl::MyGetFirstSelectedItemPosition()
{
	m_pos = CListCtrl::GetFirstSelectedItemPosition();
	return int(m_pos);
}

int CMyListCtrl::MyGetNextSelectedItem()
{
	if (NULL == m_pos) return -1;
	return CListCtrl::GetNextSelectedItem(m_pos);
}


