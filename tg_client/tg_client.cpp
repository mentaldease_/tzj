
// tg_client.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "tg_client.h"
#include "MainFrm.h"
#include "tg_clientDoc.h"
#include "tg_clientView.h"
#include <exception>
#include "DlgVerification.h"
#pragma comment(lib,"dbghelp.lib")
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Ctg_clientApp

BEGIN_MESSAGE_MAP(Ctg_clientApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &Ctg_clientApp::OnAppAbout)
	// 基于文件的标准文档命令
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()


// Ctg_clientApp 构造

Ctg_clientApp::Ctg_clientApp()
{
	// 支持重新启动管理器
	m_dwRestartManagerSupportFlags = 0;//AFX_RESTART_MANAGER_SUPPORT_ALL_ASPECTS;
#ifdef _MANAGED
	// 如果应用程序是利用公共语言运行时支持(/clr)构建的，则: 
	//     1) 必须有此附加设置，“重新启动管理器”支持才能正常工作。
	//     2) 在您的项目中，您必须按照生成顺序向 System.Windows.Forms 添加引用。
	System::Windows::Forms::Application::SetUnhandledExceptionMode(System::Windows::Forms::UnhandledExceptionMode::ThrowException);
#endif

	// TODO:  将以下应用程序 ID 字符串替换为唯一的 ID 字符串；建议的字符串格式
	//为 CompanyName.ProductName.SubProduct.VersionInformation
	CString strAppId = _T("BlackHand.Msk.Tg.");
	m_strVer = __DATE__;
	strAppId += m_strVer;
	SetAppID(strAppId);

	// TODO:  在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}

// 唯一的一个 Ctg_clientApp 对象

Ctg_clientApp theApp;


// Ctg_clientApp 初始化

LONG WINAPI  Ctg_clientApp::MyExptFilter(EXCEPTION_POINTERS *pExptInfo)
{
	if (EXCEPTION_ACCESS_VIOLATION != pExptInfo->ExceptionRecord->ExceptionCode)//ouptutdebugstring
		return EXCEPTION_CONTINUE_SEARCH;
	LONG ret = EXCEPTION_CONTINUE_SEARCH;
	TCHAR szExePath[MAX_PATH] = { 0 };
	if (::GetModuleFileName(NULL, szExePath, MAX_PATH) > 0)
	{
		int ch = _T('\\');
		*_tcsrchr(szExePath, ch) = _T('\0');
		_tcscat(szExePath, _T("\\MskDump.dmp"));
	}

	// 程序崩溃时，将写入程序目录下的MyDump.dmp文件  
	HANDLE hFile = ::CreateFile(szExePath, GENERIC_WRITE,
		FILE_SHARE_WRITE, NULL, CREATE_NEW,
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		MINIDUMP_EXCEPTION_INFORMATION exptInfo;
		exptInfo.ThreadId = ::GetCurrentThreadId();
		exptInfo.ExceptionPointers = pExptInfo;

		BOOL bOK = ::MiniDumpWriteDump(::GetCurrentProcess(),
			::GetCurrentProcessId(),
			hFile, MiniDumpNormal,
			&exptInfo, NULL, NULL);
		if (bOK)
			ret = EXCEPTION_EXECUTE_HANDLER;
	}
	CString txt;
	txt.Format( _T("程序要崩溃了:%x"), pExptInfo->ExceptionRecord->ExceptionCode);
	AfxMessageBox(txt);
	exit(0);
	return ret;
}
#include "../nomodule/MemoryModule.h"
#include <memory>
#include <functional>

void show_error(const char* error)
{
#ifdef _VER_RELEASE1
	return;
#endif
	//	MessageBoxA(NULL, error, "脚本错误", 0);
	printf("msk_clinet:error %s\n", error);
	MyDbgPrint(error);
}

int MskPrint(lua_State *L)
{
#ifdef _VER_RELEASE1
	return 1;
#endif
	string strPrint;
	char *str;
	for (int i = 1; i <= lua_gettop(L); ++i){
		str = (char *)lua_tostring(L, i);
		strPrint += str ? str : "nil";
		strPrint += " ";
	}
	strPrint += "\n";
	MyDbgPrint(strPrint.c_str());
	printf(strPrint.c_str());
	return 0;
}

int SetMskData(lua_State *L)
{
	string strconsole = "mskdata:";
	const char *fname = lua_tostring(L, 1);
	size_t sz;
	const char *data = lua_tolstring(L, 2,&sz);
	strconsole += fname;
	HANDLE hTFile = fCreateFileW(CString(strconsole.c_str()), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD dwWrite;
	fWriteFile(hTFile, data, sz,&dwWrite , NULL);
	fCloseHandle(hTFile);
	return 0;
}

int MsgBox(lua_State *L)
{
	auto msg = lua_tostring(L, 1);
	if (msg != nullptr)
	{
		MessageBoxA(NULL, msg, "警告", MB_OK);
	}
	return 0;
}

struct _pid_class{
	DWORD pid;
	const char * wndClass;
	HWND hwnd;
};
BOOL CALLBACK EnumProcessWindowsProc(HWND hwnd, LPARAM lParam)
{
	auto ppc = (_pid_class *)lParam;
	DWORD pid;
	GetWindowThreadProcessId(hwnd,&pid);
	if (pid == ppc->pid)
	{
		char classname[MAX_PATH] = { 0 };
		GetClassNameA(hwnd, classname, MAX_PATH);
		if (strcmp(ppc->wndClass, classname) == 0)
		{
			ppc->hwnd = hwnd;
			return FALSE;
		}
	}
	return TRUE;
}

int GetProcessWindow(lua_State *L)
{
	_pid_class pc;
	pc.pid = lua_tointeger(L, 1);
	pc.wndClass = lua_tostring(L, 2);
	pc.hwnd = NULL;
	EnumWindows(EnumProcessWindowsProc, (LPARAM)&pc);
	if (pc.hwnd == NULL)
	{
		lua_pushnil(L);
	}
	else
	{
		lua_pushlightuserdata(L, pc.hwnd);
	}
	return 1;
}

int GetProceesNum(lua_State *L)
{
	__try{
		auto pnum = 0;
		auto name = lua_tostring(L, 1);
		auto hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		USES_CONVERSION;
		auto wname = A2W(name);
		if (INVALID_HANDLE_VALUE != hProcessSnap)
		{
			PROCESSENTRY32 pe32;
			pe32.dwSize = sizeof(PROCESSENTRY32);

			if (Process32First(hProcessSnap, &pe32))
			{
				do
				{
					if (_tcscmp(pe32.szExeFile, wname) == 0)
						pnum++;
				} while (Process32Next(hProcessSnap, &pe32));
			}
			CloseHandle(hProcessSnap);
		}
		lua_pushinteger(L, pnum);
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
		LOG("GetProceesNum !!!!");
		lua_pushinteger(L, 99);
	}
	return 1;
}





#include <fstream>
/*
#include "../mhook-lib/mhook.h"
#define HOOKAPIPRE(fun) auto true##fun = fun
#define HOOKAPI(fun) Mhook_SetHook((LPVOID *)& true##fun, my##fun)
#define HOOKAPINEW(fun) decltype(fun) my##fun
auto trueCreateFileW = (decltype(CreateFileW) *)GetProcAddress(GetModuleHandleA("Kernel32.dll"), "CreateFileW");

HANDLE
WINAPI
myCreateFileW(
_In_ LPCWSTR lpFileName,
_In_ DWORD dwDesiredAccess,
_In_ DWORD dwShareMode,
_In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes,
_In_ DWORD dwCreationDisposition,
_In_ DWORD dwFlagsAndAttributes,
_In_opt_ HANDLE hTemplateFile
)
{
	if (lpFileName && _tcsstr(lpFileName, _T("Final\\GameGuard\\")))
	{
		SetLastError(ERROR_MOD_NOT_FOUND);
		return INVALID_HANDLE_VALUE;
	}
	return trueCreateFileW(lpFileName, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
}
HOOKAPI(CreateFileW);
*/

BOOL Ctg_clientApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。  否则，将无法创建窗口。
	AddVectoredExceptionHandler(1, MyExptFilter);
	/*
	SetUnhandledExceptionFilter(MyExptFilter);
	BYTE code[] = { 0x33, 0xC0, 0xC2, 0x04, 0x00 };
	DWORD old;
	VirtualProtect(SetUnhandledExceptionFilter, 5, PAGE_READWRITE, &old);
	memcpy(SetUnhandledExceptionFilter, code, 5);
	VirtualProtect(SetUnhandledExceptionFilter, 5, old, &old);
	*/
	char szFileFullPath[MAX_PATH], szProcessName[MAX_PATH];
	::GetModuleFileNameA(NULL, szFileFullPath, MAX_PATH);//获取文件路径
	auto nfname = theTools.GetRandomString(5) + ".exe";
	//MoveFileExA(szFileFullPath, nfname.c_str(), MOVEFILE_REPLACE_EXISTING);
	
	/*
	CStringA fp = szFileFullPath;
	int pos = fp.ReverseFind('\\');
	auto pname = fp.Right(fp.GetLength() - pos-1);
#ifdef _VER_RELEASE
	if (pname == "msk.exe"){
#else
	if (pname == "client_tg.exe"){
#endif
		auto nfname = theTools.GetRandomString(5) + ".exe";
		CopyFileA(pname, nfname.c_str(),TRUE);
		PRT("准备启动指定程序:%s", nfname.c_str());
		ShellExecuteA(0, "open", nfname.c_str(), NULL, NULL, SW_SHOWNORMAL);
		return FALSE;
	}
	else if (false){
		CStringA str(GetCommandLineA());
		PRT("命令行:%s", str.GetBuffer());
		str.ReleaseBuffer();
		if (str.Find("_first_") != -1){
			PRT("自删除");
			theTools.DeleteApplicationSelf();
			return FALSE;
		}
	}
	PRT("启动自己");
*/
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();
	
	// 初始化 OLE 库
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	// 初始化 SOCKET 库
	if (!AfxSocketInit())
	{
		AfxMessageBox(_T("网络库初始化失败"));
		return FALSE;
	}
	/*
	WSADATA wsaData;
	DWORD Ret;
	if ((Ret = WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0)  {
		AfxMessageBox(_T("WSAStartup failed with error %d\n"));
		return FALSE;
	}
	*/
	AfxEnableControlContainer();
	EnableTaskbarInteraction(FALSE);
	// 使用 RichEdit 控件需要  AfxInitRichEdit2()	
	// AfxInitRichEdit2();

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO:  应适当修改该字符串，
	// 例如修改为公司或组织名
	LoadStdProfileSettings(4);  // 加载标准 INI 文件选项(包括 MRU)
	SetRegistryKey(_T("msk_tg_client"));
#ifndef _VER_RELEASE
	if (IsDebuggerPresent()){
		AllocConsole();
		freopen("CONIN$", "r+t", stdin);
		freopen("CONOUT$", "w+t", stdout);
	}
#else
	if (IsDebuggerPresent()){
		AllocConsole();
		freopen("CONIN$", "r+t", stdin);
		freopen("CONOUT$", "w+t", stdout);
	}
#endif
#ifdef _DEBUG
	SetCurrentDirectory(_T("../Release"));
#else
	if (IsDebuggerPresent()){
#ifdef _VER_RELEASE
		SetCurrentDirectory(_T("../ver_release_tg"));
#else
		SetCurrentDirectory(_T("../Release"));
#endif
	}
#endif
	try
	{
		PRT("ver:"__DATE__""__TIME__);
		m_l = luaL_newstate();
		luaL_openlibs(m_l);
		lt::def(m_l, "_ALERT", show_error);
		lua_register(m_l, "GetProcessWindow", GetProcessWindow);
		lua_register(m_l, "GetProceesNum", GetProceesNum);
		lua_register(m_l, "MskPrint", MskPrint);
		lua_register(m_l, "print", MskPrint);
		lua_register(m_l, "SetMskData", SetMskData);
		lt::set(m_l, "PROCESS_ALL_ACCESS", PROCESS_ALL_ACCESS);
		lt::set(m_l, "WAIT_TIMEOUT", WAIT_TIMEOUT);
		lt::set(m_l, "CFG_VER", __DATE__);
#ifdef _VER_RELEASE
		lt::set(m_l, "CFG_DEF", "ver_release");
#else
#ifdef _DEBUG
		lt::set(m_l, "CFG_DEF", "debug");
#else
		lt::set(m_l, "CFG_DEF", "release");
#endif
#endif
		TCHAR szPath[MAX_PATH] = { 0 };
		//SCOPE_EXIT(MemoryFreeLibrary(hDll));//BUG 释放会出错
#ifdef _VER_RELEASE
		SHGetSpecialFolderPath(NULL, szPath, CSIDL_SYSTEM, FALSE);
		_tcscat(szPath, _T("\\Kernel32.dll"));
		CFile f(szPath, CFile::modeRead);
		unique_ptr<byte> pBuff(new byte[f.GetLength()]);
		f.Read(pBuff.get(), f.GetLength());
		f.Close();
		//SCOPE_EXIT(MemoryFreeLibrary(hDll));//BUG 释放会出错
		HMEMORYMODULE hDll = MemoryLoadLibrary(pBuff.get());
		ENSURE(hDll != NULL);
		fCreateFileW = (decltype(fCreateFileW))MemoryGetProcAddress(hDll, "CreateFileW");
		fReadFile = (decltype(fReadFile))MemoryGetProcAddress(hDll, "ReadFile");
		fWriteFile = (decltype(fWriteFile))MemoryGetProcAddress(hDll, "WriteFile");
		fCloseHandle = (decltype(fCloseHandle))MemoryGetProcAddress(hDll, "CloseHandle");
		fGetFileSize = (decltype(fGetFileSize))MemoryGetProcAddress(hDll, "GetFileSize");
		//fCreateToolhelp32Snapshot = (decltype(fCreateToolhelp32Snapshot))MemoryGetProcAddress(hDll, "CreateToolhelp32Snapshot");
		//fProcess32First = (decltype(fProcess32First))MemoryGetProcAddress(hDll, "Process32First");
		//fProcess32Next = (decltype(fProcess32Next))MemoryGetProcAddress(hDll, "Process32Next");
#endif
		memset(szPath, 0, MAX_PATH*sizeof(TCHAR));
		SHGetSpecialFolderPath(NULL, szPath, CSIDL_APPDATA, TRUE);
		CString strDir = szPath;
		strDir += "\\MskControl";
		if (FALSE == PathIsDirectory(strDir)){
			BOOL bCreate = CreateDirectory(strDir, NULL);
			strDir += "\\Config.ini";
			HANDLE hTFile = fCreateFileW(strDir, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			strDir += ":mscache";
			DWORD dWrite;
			const char *szWrite = "[配置]";
			fWriteFile(hTFile, szWrite, strlen(szWrite), &dWrite, NULL);
			fCloseHandle(hTFile);
			hTFile = fCreateFileW(strDir, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			std::string strDymac = theTools.GetRandomString(15);
			bCreate = fWriteFile(hTFile, strDymac.c_str(), strDymac.size(), &dWrite, NULL);
			fCloseHandle(hTFile);
		}
		strDir = szPath;
		strDir += "\\MskControl\\Config.ini:mscache";
		HANDLE hFile = fCreateFileW(strDir, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		ENSURE(INVALID_HANDLE_VALUE != hFile);
		char szDymac[16] = { 0 };		
		DWORD dwRead = fGetFileSize(hFile, NULL);
		fReadFile(hFile, szDymac, 15, &dwRead, NULL);
		fCloseHandle(hFile);
		char szPathA[MAX_PATH] = { 0 };
		SHGetSpecialFolderPathA(NULL, szPathA, CSIDL_APPDATA, FALSE);
		strcat(szPathA,"\\MskControl\\msk.cfg");
		lt::set(m_l, "gvConfigPath", szPathA);
		auto userData = lt::table(m_l, "gtUserData");
		userData.set("dymac", szDymac);

		memset(szPathA, 0, MAX_PATH);
		GetCurrentDirectoryA(MAX_PATH, szPathA);
		auto clData = lt::table(m_l, "gtClientData");
		clData.set("dymac", szDymac);

		//唯一编码获取结束 启动登陆框
		CDlgVerification dlg(m_l);
		if (dlg.DoModal() != IDOK){
			return FALSE;
		}
		auto srvData = lt::table(m_l, "gtServerData");
		auto srvString = lt::call<const char *>(m_l, "pickle", srvData);
		lua_getglobal(m_l, "gtClientData");
		lua_getglobal(m_l, "gvSigData");
		auto ol = lua_objlen(m_l,-1);
		lua_setfield(m_l, -2, "sig");
		lua_pop(m_l, 1);
		//auto len = strlen(srvString);
		//clData.set("sig", srvData.get<lt::table>("data").get<const char*>("sig"));

		/*HANDLE hDllP = fCreateFileW(_T("C:\\mskd"), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		fWriteFile(hDllP, szPathA, strlen(szPathA), &dWrite, NULL);;
		fCloseHandle(hDllP);*/
		HANDLE hDllP;
		DWORD dWrite;
#ifndef _VER_RELEASE
		memset(szPath, 0, MAX_PATH*sizeof(TCHAR));
		GetCurrentDirectory(MAX_PATH, szPath);
		g_strDllPath = szPath;
		g_strDllPath += _T("\\dll_loader.dll");
#else
		//登录成功
		memset(szPath, 0, MAX_PATH*sizeof(TCHAR));
		GetCurrentDirectory(MAX_PATH, szPath);
		g_strDllPath = szPath;
		g_strDllPath += _T("\\mskdata:mskl.");
#endif
		hDllP = fCreateFileW(g_strDllPath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (INVALID_HANDLE_VALUE == hDllP){
			return FALSE;
		}else{
			fCloseHandle(hDllP);
		}
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return FALSE;
	}
	catch (CFileException* e)
	{
		e->ReportError();
		e->Delete();
		return FALSE;
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
		return FALSE;
	}
	catch (exception e){
		MessageBoxA(NULL, "未知异常", e.what(), 0);
		return FALSE;
	}
	// 注册应用程序的文档模板。  文档模板
	// 将用作文档、框架窗口和视图之间的连接
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(Ctg_clientDoc),
		RUNTIME_CLASS(CMainFrame),       // 主 SDI 框架窗口
		RUNTIME_CLASS(Ctg_clientView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);


	// 分析标准 shell 命令、DDE、打开文件操作的命令行
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);



	// 调度在命令行中指定的命令。  如果
	// 用 /RegServer、/Register、/Unregserver 或 /Unregister 启动应用程序，则返回 FALSE。
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// 唯一的一个窗口已初始化，因此显示它并对其进行更新
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	return TRUE;
}

int Ctg_clientApp::ExitInstance()
{
	//TODO:  处理可能已添加的附加资源
	AfxOleTerm(FALSE);
	if (m_l) lt::call<void>(m_l, "_UI_Event", "evExit");
#ifndef _VER_RELEASE
	fclose(stdin);
	fclose(stdout);
	FreeConsole();
#else
	if (IsDebuggerPresent()){
		fclose(stdin);
		fclose(stdout);
		FreeConsole();
	}
#endif
	//*/
	return CWinApp::ExitInstance();
}

// Ctg_clientApp 消息处理程序


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// 用于运行对话框的应用程序命令
void Ctg_clientApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// Ctg_clientApp 消息处理程序



