#pragma once

#include "tg_client.h"
#include "afxpropertygridctrl.h"
#include "afxeditbrowsectrl.h"
// CCfgDlg 对话框

class CCfgDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CCfgDlg)

public:
	CCfgDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCfgDlg();

// 对话框数据
	enum { IDD = IDD_CFG };

public:
	BOOL LoadMsvcDll(CString gamedir);
	CString m_strGamDir;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	lua_State *&m_l;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedApply();
	afx_msg void OnEnChangeGamedir();
	CMFCEditBrowseCtrl m_edtGameDir;
	afx_msg void OnNMThemeChangedGamedir(NMHDR *pNMHDR, LRESULT *pResult);
};
