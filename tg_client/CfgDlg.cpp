// CfgDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "CfgDlg.h"
#include "afxdialogex.h"


// CCfgDlg 对话框

IMPLEMENT_DYNAMIC(CCfgDlg, CDialogEx)

CCfgDlg::CCfgDlg(CWnd* pParent /*=NULL*/)
: CDialogEx(CCfgDlg::IDD, pParent), m_l(theApp.m_l)
{

}

CCfgDlg::~CCfgDlg()
{
}

void CCfgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, EDB_GAMEDIR, m_edtGameDir);
}


BEGIN_MESSAGE_MAP(CCfgDlg, CDialogEx)
	ON_BN_CLICKED(IDC_APPLY, &CCfgDlg::OnBnClickedApply)
	ON_EN_CHANGE(EDB_GAMEDIR, &CCfgDlg::OnEnChangeGamedir)
END_MESSAGE_MAP()


// CCfgDlg 消息处理程序
CCfgDlg *cfgdlg;
int Ui_SetConfig(lua_State *L)
{
	auto key = lua_tostring(L, 1);
	auto val = lua_tostring(L, 2);
	if (key && strcmp(key, "交易对象") == 0){
		cfgdlg->SetDlgItemText(IDC_TRADE_PEOPLE, CString(val));
	}
	return 0;
}

BOOL CCfgDlg::OnInitDialog()
{
	cfgdlg = this;
	CDialogEx::OnInitDialog();
	lua_register(m_l, "Ui_SetConfig", Ui_SetConfig);
	// TODO:  在此添加额外的初始化
	TCHAR szPath[MAX_PATH] = { 0 };
	SHGetSpecialFolderPath(NULL, szPath, CSIDL_APPDATA, TRUE);
	CString appPath(szPath);
	try
	{
		CFile f(appPath+_T("//MskControl//basecfg.lua"), CFile::modeRead);
		auto sz = f.GetLength();
		SCOPE_EXIT(f.Close(););
		ENSURE(sz > 0);
		unique_ptr<char> pBuff(new char[sz+14]);
		memset(pBuff.get(), 0, sz + 14);
		strcpy(pBuff.get(), "g_uiConfig = ");
		auto nRead = f.Read((void *)((int)pBuff.get()+13), sz);
		lt::dostring(m_l, pBuff.get());
		//auto uiConfig = lt::call<lt::table>(m_l, "eval", pBuff.get());
		auto uiConfig = lt::table(m_l, "g_uiConfig");
		SetDlgItemInt(IDC_MULOPEN, uiConfig.get<int>("clientnum"));
		//lt::set(m_l, "g_uiConfig", uiConfig);
	}
	catch (...)
	{
		lua_newtable(m_l);
		lua_pushinteger(m_l, 3);
		lua_setfield(m_l, -2, "多开数");
		lua_pushinteger (m_l, 1);
		lua_setfield(m_l, -2, "启动后最小化");
		lua_pushinteger(m_l, 1);
		lua_setfield(m_l, -2, "封号后警告");
		lua_setglobal(m_l, "g_uiConfig");

	}
	auto uicfg = lt::table(m_l, "g_uiConfig");
	SetDlgItemInt(IDC_MULOPEN, uicfg.get<int>("多开数"));
	auto it = (CButton*)GetDlgItem(IDC_MINSIZE);
	it->SetCheck(uicfg.get<int>("启动后最小化"));
	it = (CButton*)GetDlgItem(IDC_PLAY_WARN);
	it->SetCheck(uicfg.get<int>("封号后警告"));

	CString strGameDir = theApp.GetProfileString(_T("lastset"), _T("gamedir"));
	m_edtGameDir.SetWindowText(strGameDir);

	try
	{
		CFile f(appPath + _T("//MskControl//scriptcfg.lua"), CFile::modeRead);
		auto sz = f.GetLength();
		SCOPE_EXIT(f.Close(););
		ENSURE(sz > 0);
		unique_ptr<char> pBuff(new char[sz + 18]);
		memset(pBuff.get(), 0, sz + 18);
		strcpy(pBuff.get(), "g_scriptConfig = ");
		auto nRead = f.Read((void *)((int)pBuff.get() + 17), sz);
		lt::dostring(m_l, pBuff.get());
	}
	catch (...)
	{
		lua_newtable(m_l);
		lua_pushstring(m_l, "1");
		lua_setfield(m_l, -2, "组队人数");
		lua_pushstring(m_l, "神将");
		lua_setfield(m_l, -2, "职业");
		lua_pushstring(m_l, "不交易");
		lua_setfield(m_l, -2, "交易对象");
		lua_pushstring(m_l, "五方城|433|170");
		lua_setfield(m_l, -2, "交易地点");
		lua_pushstring(m_l, "0");
		lua_setfield(m_l, -2, "保留金钱");
		lua_pushstring(m_l, "不加入");
		lua_setfield(m_l, -2, "加入帮派");
		lua_pushinteger(m_l, 0);
		lua_setfield(m_l, -2, "本机交易");
		lua_pushinteger(m_l, 0);
		lua_setfield(m_l, -2, "反击");
		lua_setglobal(m_l, "g_scriptConfig");
	}
	auto  spcfg = lt::table(m_l, "g_scriptConfig");
	auto hwnd = this->GetSafeHwnd();
#define GETCHAR(arg) spcfg.get<const char *>(arg)//
	SetDlgItemTextA(hwnd, IDC_TEAMNUM, GETCHAR("组队人数"));
	SetDlgItemTextA(hwnd, IDC_PRO, GETCHAR("职业"));
	SetDlgItemTextA(hwnd, IDC_TRADE_PEOPLE, GETCHAR("交易对象"));
	SetDlgItemTextA(hwnd, IDC_TRADE_PLACE, GETCHAR("交易地点"));
	SetDlgItemTextA(hwnd, IDC_KEEP_MONEY, GETCHAR("保留金钱"));
	SetDlgItemTextA(hwnd, IDC_ADDBOUND, GETCHAR("加入帮派"));
	it = (CButton*)GetDlgItem(IDC_TRADE_FIRST);
	it->SetCheck(spcfg.get<int>("本机交易"));
	it = (CButton*)GetDlgItem(IDC_FIGHT_BACK);
	it->SetCheck(spcfg.get<int>("反击"));
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CCfgDlg::OnBnClickedApply()
{
	// TODO:  在此添加控件通知处理程序代码
	int ltop = lua_gettop(m_l);
	try
	{
		TCHAR szPath[MAX_PATH] = { 0 };
		SHGetSpecialFolderPath(NULL, szPath, CSIDL_APPDATA, TRUE);
		CString appPath(szPath);
		auto uiConfig = lt::get<lt::table>(m_l, "g_uiConfig");
		uiConfig.set("多开数", GetDlgItemInt(IDC_MULOPEN));
		uiConfig.set("启动后最小化", ((CButton*)GetDlgItem(IDC_MINSIZE))->GetCheck());
		uiConfig.set("封号后警告", ((CButton*)GetDlgItem(IDC_PLAY_WARN))->GetCheck());

		CFile f(appPath + _T("//MskControl//basecfg.lua"), CFile::modeWrite | CFile::modeCreate);
		auto data = lt::call<const char *>(m_l, "prettytostring", uiConfig);
		lua_getglobal(m_l, "prettytostring"),
			lua_getglobal(m_l, "g_uiConfig");
		if (0 == lua_pcall(m_l, 1, 1, 0) && lua_isstring(m_l, -1)){
			auto data = lua_tostring(m_l, -1);
			f.Write(data, strlen(data));
		}
		else{
			MessageBoxA(NULL, "保存基本配置失败", "异常", MB_OK);
		}
		lua_pop(m_l, 1);
		f.Close();

		f.Open(appPath + _T("//MskControl//scriptcfg.lua"), CFile::modeWrite | CFile::modeCreate);
		auto  spcfg = lt::table(m_l, "g_scriptConfig");
		auto hwnd = this->GetSafeHwnd();
		char tmp[MAX_PATH];
#define SETCHAR(idc,key) memset(tmp,0,MAX_PATH);GetDlgItemTextA(hwnd, idc, tmp, MAX_PATH);spcfg.set(key,(char *)tmp)
		SETCHAR(IDC_TEAMNUM, "组队人数");
		SETCHAR(IDC_PRO, "职业");
		SETCHAR(IDC_TRADE_PEOPLE, "交易对象");
		SETCHAR(IDC_TRADE_PLACE, "交易地点");
		SETCHAR(IDC_KEEP_MONEY, "保留金钱");
		SETCHAR(IDC_ADDBOUND, "加入帮派");
		spcfg.set("本机交易", ((CButton*)GetDlgItem(IDC_TRADE_FIRST))->GetCheck());
		spcfg.set("反击", ((CButton*)GetDlgItem(IDC_FIGHT_BACK))->GetCheck());
		auto data1 = lt::call<const char *>(m_l, "prettytostring", spcfg);
		lua_getglobal(m_l, "prettytostring");
		lua_getglobal(m_l, "g_scriptConfig");
		if (0 == lua_pcall(m_l, 1, 1, 0) && lua_isstring(m_l, -1)){
			auto data1 = lua_tostring(m_l, -1);
			f.Write(data1, strlen(data1));
		}
		else{
			MessageBoxA(NULL, "保存脚本配置失败", "异常", MB_OK);
		}
		lua_pop(m_l, 1);
		f.Close();
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (CFileException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (exception &e){
		MessageBoxA(NULL, e.what(), "异常", MB_OK);
	}
	catch (...)
	{
		MessageBoxA(NULL, "未知错误", "异常", MB_OK);
	}
	lua_settop(m_l, ltop);
}

void CCfgDlg::OnNMThemeChangedGamedir(NMHDR *pNMHDR, LRESULT *pResult)
{
	// 该功能要求使用 Windows XP 或更高版本。
	// 符号 _WIN32_WINNT 必须 >= 0x0501。
	// TODO:  在此添加控件通知处理程序代码
	*pResult = 0;
}



BOOL CCfgDlg::LoadMsvcDll(CString gamedir)
{
	if (gamedir == "") return FALSE;
	try
	{
		LPSTR szResourceName = MAKEINTRESOURCEA(LOWORD(IDR_DLL2));
		// 资源的类型
		LPSTR szResourceType = "DLL";
		// 查找资源句柄
		HRSRC hRes = FindResourceA(NULL, szResourceName, szResourceType);
		// 获得资源数据的句柄
		HGLOBAL hResData;
		if (!hRes || !(hResData = LoadResource(NULL, hRes))){ return FALSE; };// 资源数据的大小
		DWORD dwSize = SizeofResource(NULL, hRes);
		byte* pData = new byte[dwSize];
		if (hResData == NULL){ return FALSE; }// 资源加锁
		PBYTE pSrc = (PBYTE)LockResource(hResData); if (!pSrc){ FreeResource(hResData); return -1; };// 复制数据
		CopyMemory(pData, pSrc, dwSize);
		// 释放资源
		FreeResource(hResData);
		CFile f;
		f.Open(gamedir + _T("\\msvcp120.dll"), CFile::modeWrite | CFile::typeBinary | CFile::modeCreate);
		f.Write(pData, dwSize);
		f.Close();
		delete[]pData;

		szResourceName = MAKEINTRESOURCEA(LOWORD(IDR_DLL3));
		hRes = FindResourceA(NULL, szResourceName, szResourceType);
		// 获得资源数据的句柄
		hResData;
		if (!hRes || !(hResData = LoadResource(NULL, hRes))){ return FALSE; };// 资源数据的大小
		dwSize = SizeofResource(NULL, hRes);
		pData = new byte[dwSize];
		if (hResData == NULL){ return FALSE; }// 资源加锁
		pSrc = (PBYTE)LockResource(hResData); if (!pSrc){ FreeResource(hResData); return FALSE; };// 复制数据
		CopyMemory(pData, pSrc, dwSize);
		// 释放资源
		FreeResource(hResData);
		f.Open(gamedir + _T("\\msvcr120.dll"), CFile::modeWrite | CFile::typeBinary | CFile::modeCreate);
		f.Write(pData, dwSize);
		f.Close();
		delete[]pData;
	}
	catch (CException* e)
	{
		//e->ReportError();
		e->Delete();
		return TRUE;
	}
	return TRUE;
}


void CCfgDlg::OnEnChangeGamedir()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CFormView::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
	// TODO:  在此添加控件通知处理程序代码
	if (g_bHideWindow) return;
	g_csGamDir.Lock();
	m_edtGameDir.GetWindowText(m_strGamDir);
	if (m_strGamDir.Find('\\') == -1) return;
	g_strGamDir = m_strGamDir;
	g_csGamDir.Unlock();
	theApp.WriteProfileString(_T("lastset"), _T("gamedir"), m_strGamDir);
	if (m_strGamDir != ""){
		CStringA strFullDir(m_strGamDir);
		SetGlobal("gamedir", strFullDir.GetBuffer());
		strFullDir.ReleaseBuffer();
		int pos = m_strGamDir.ReverseFind(_T('\\'));
		CString strDir = m_strGamDir.Left(pos);
		static HANDLE lockFile = INVALID_HANDLE_VALUE;
		if (lockFile != INVALID_HANDLE_VALUE){
			CloseHandle(lockFile);
		}
	//	CString logdir = strDir + "\\log";
		//lockFile = CreateFile(logdir.GetBuffer(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		//logdir.ReleaseBuffer();
		auto err = GetLastError();

		// 全部转成CStringA 防止内存错误  [4/5/2015 msk]
		CStringA oname(strDir);
		CStringA nname(strDir);
		oname += "\\BugReport.exe";
		nname += "\\BugReport_back.exe";
		rename(oname.GetBuffer(), nname.GetBuffer());
		oname.ReleaseBuffer();
		nname.ReleaseBuffer();
		CStringA dname(strDir);
		dname += "\\BugReport.exe";
		DeleteFileA(dname.GetBuffer());
		dname.ReleaseBuffer();
		if (!LoadMsvcDll(strDir)){
			AfxMessageBox(_T("软件初始化失败"));
			PostQuitMessage(0);
		}
		CStringA str(strDir);
		lt::call<void>(m_l, "_UI_Event", "evGameDirChage", str.GetBuffer());
		str.ReleaseBuffer();
	}
}
