#include "StdAfx.h"
#include "ListenSocket.h"


CListenSocket::CListenSocket(void)
{
}


CListenSocket::~CListenSocket(void)
{
}


void CListenSocket::OnAccept(int nErrorCode)
{
	// TODO: Add your specialized code here and/or call the base class
	CNewSocket *pSocket = new CNewSocket();
	if (Accept(*pSocket))
	{
		pSocket->AsyncSelect(FD_READ); //触发通信socket的Read函数读数据
		g_arrSocket.insert(pSocket);
	}
	else
	{
		delete pSocket;
	}
	CAsyncSocket::OnAccept(nErrorCode);
}

