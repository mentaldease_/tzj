// MyComboBox.cpp : 实现文件
//

#include "stdafx.h"
#include "MyComboBox.h"


// CMyComboBox

IMPLEMENT_DYNAMIC(CMyComboBox, CComboBox)

CMyComboBox::CMyComboBox()
{

}

CMyComboBox::~CMyComboBox()
{
}


BEGIN_MESSAGE_MAP(CMyComboBox, CComboBox)
END_MESSAGE_MAP()



// CMyComboBox 消息处理程序
string strret;

const char* CMyComboBox::MyGetWindowText()
{
	CString strText;
	GetWindowText(strText);
	strret =  CStringA(strText);
	return strret.c_str();
}

int CMyComboBox::MyAddString(const char* str)
{
	USES_CONVERSION;
	return CComboBox::AddString(A2W(str));
}